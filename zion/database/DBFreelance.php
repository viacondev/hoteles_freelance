<?php

class DBFreelance {
  private $dbname;
  private $dbuser = 'freelance';
  private $dbpwd = 'C#1p!0n_Ci3P0';

  private $conn;
  static private $instance = null;
  
  private function __construct() {
    $this->dbname = 'freelance';
    $conn_str = "mysql:host=localhost;dbname=" . $this->dbname . ";charset=utf8";
    $this->conn = new PDO($conn_str, $this->dbuser, $this->dbpwd);
  }
  
  public static function conn() {
    if (!isset(self::$instance)) {
      self::$instance = new DBFreelance();
    }

    return self::$instance->conn;
  }
}


?>