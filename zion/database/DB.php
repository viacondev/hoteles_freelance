<?php

class DB {
  private $dbname;
  private $dbuser = 'barrybol_zion';
  private $dbpwd = 'zionshopdbpwd';

  private $conn;
  static private $instance = null;
  
  private function __construct() {
    $this->dbname = ZionConfig::$dbname;
    $conn_str = "mysql:host=23.91.64.139;dbname=" . $this->dbname . ";charset=utf8";
    $this->conn = new PDO($conn_str, $this->dbuser, $this->dbpwd);
  }
  
  public static function conn() {
    if (!isset(self::$instance)) {
      self::$instance = new DB();
    }

    return self::$instance->conn;
  }
}


?>