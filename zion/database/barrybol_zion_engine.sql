-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 21, 2014 at 05:54 PM
-- Server version: 5.5.31
-- PHP Version: 5.3.10-1ubuntu3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `barrybol_zion_engine`
--

-- --------------------------------------------------------

--
-- Table structure for table `Admin`
--

CREATE TABLE IF NOT EXISTS `Admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role` varchar(25) NOT NULL COMMENT 'ADMIN;OPERATOR',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `Agency`
--

CREATE TABLE IF NOT EXISTS `Agency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `address` text,
  `phone` varchar(255) DEFAULT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `booking` varchar(10) NOT NULL COMMENT 'BOOK;EMIT',
  `status` int(11) NOT NULL COMMENT '1: Active; 0: Inactive',
  `commission` float NOT NULL,
  `logo` longblob,
  `extra_fee` float DEFAULT NULL,
  `extra_fee_type` varchar(25) DEFAULT NULL COMMENT 'VALUE; PERC',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Table structure for table `Booking`
--

CREATE TABLE IF NOT EXISTS `Booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `localizer` varchar(25) NOT NULL,
  `status` varchar(25) NOT NULL COMMENT 'CONFIRMED; PENDING; CANCELLED',
  `holder` varchar(55) NOT NULL,
  `price` float NOT NULL,
  `fee` float NOT NULL,
  `agency_fee` float NOT NULL,
  `agency_fee_type` varchar(15) NOT NULL COMMENT 'VALUE; PERC',
  `confirm_date` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `agency_id` int(11) NOT NULL,
  `agent` varchar(55) NOT NULL COMMENT 'In case there is no user_id',
  `agency` varchar(55) NOT NULL COMMENT 'In case there is no agency_id',
  `operator_id` int(11) NOT NULL COMMENT 'The operator that process the book',
  `remarks` text NOT NULL,
  `debit_note` varchar(25) NOT NULL,
  `operator_remarks` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=193 ;

-- --------------------------------------------------------

--
-- Table structure for table `Config`
--

CREATE TABLE IF NOT EXISTS `Config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fee` float NOT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `mail2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `Fee`
--

CREATE TABLE IF NOT EXISTS `Fee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_code` varchar(25) NOT NULL,
  `fee` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

-- --------------------------------------------------------

--
-- Table structure for table `Hotel`
--

CREATE TABLE IF NOT EXISTS `Hotel` (
  `hotel_code` varchar(8) NOT NULL,
  `hotel_name` varchar(50) NOT NULL,
  `zone_code` varchar(8) NOT NULL,
  `zone_name` varchar(50) NOT NULL,
  `destination_code` varchar(8) NOT NULL,
  `destination_name` varchar(50) NOT NULL,
  `country_code` varchar(8) NOT NULL,
  `country_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `HotelRoom`
--

CREATE TABLE IF NOT EXISTS `HotelRoom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(55) NOT NULL,
  `board` varchar(55) NOT NULL,
  `adult_count` int(11) NOT NULL,
  `child_count` int(11) NOT NULL,
  `childs_ages` varchar(55) NOT NULL,
  `price` float NOT NULL,
  `net_price` float NOT NULL,
  `service_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

-- --------------------------------------------------------

--
-- Table structure for table `Log`
--

CREATE TABLE IF NOT EXISTS `Log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `level` varchar(55) NOT NULL COMMENT 'DEBUG; INFO; WARNING; ERROR',
  `action` varchar(255) NOT NULL,
  `msg` longtext NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=215 ;

-- --------------------------------------------------------

--
-- Table structure for table `Provider`
--

CREATE TABLE IF NOT EXISTS `Provider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(55) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `Service`
--

CREATE TABLE IF NOT EXISTS `Service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(25) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category` varchar(55) DEFAULT NULL,
  `localizer` varchar(55) DEFAULT NULL,
  `status` int(25) NOT NULL,
  `destination_code` varchar(55) NOT NULL,
  `cancellation_date` datetime NOT NULL,
  `checkin` date NOT NULL,
  `checkout` date NOT NULL,
  `adult_count` int(11) NOT NULL,
  `child_count` int(11) NOT NULL,
  `child_ages` varchar(55) DEFAULT NULL,
  `time` time DEFAULT NULL,
  `price` float NOT NULL,
  `currency` varchar(5) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `net_price` float NOT NULL,
  `commission` float NOT NULL,
  `agency_fee` float NOT NULL,
  `includes` text NOT NULL,
  `is_manual` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_purchase_id` (`booking_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=254 ;

-- --------------------------------------------------------

--
-- Table structure for table `ServiceStatus`
--

CREATE TABLE IF NOT EXISTS `ServiceStatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(55) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `ServiceTransfer`
--

CREATE TABLE IF NOT EXISTS `ServiceTransfer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `origin` varchar(255) NOT NULL,
  `destination` varchar(255) NOT NULL,
  `type` varchar(10) NOT NULL COMMENT 'IN; OUT; IN_OUT',
  `time_in` time NOT NULL,
  `time_out` time DEFAULT NULL,
  `flight_in` varchar(25) DEFAULT NULL,
  `flight_out` varchar(25) DEFAULT NULL,
  `service_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `ServiceType`
--

CREATE TABLE IF NOT EXISTS `ServiceType` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(55) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `Terminal`
--

CREATE TABLE IF NOT EXISTS `Terminal` (
  `code` varchar(10) CHARACTER SET utf8 NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `destination_code` varchar(5) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE IF NOT EXISTS `User` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(55) NOT NULL,
  `password` varchar(55) NOT NULL,
  `name` varchar(55) NOT NULL,
  `lastname` varchar(55) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `agency_id` int(11) NOT NULL,
  `role` varchar(15) NOT NULL COMMENT 'ADMIN;COUNTER',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `fk_agency_id` (`agency_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `Zone`
--

CREATE TABLE IF NOT EXISTS `Zone` (
  `zone_code` varchar(10) NOT NULL,
  `destination_code` varchar(5) NOT NULL,
  `zone_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Service`
--
ALTER TABLE `Service`
  ADD CONSTRAINT `fk_purchase_id` FOREIGN KEY (`booking_id`) REFERENCES `Booking` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `User`
--
ALTER TABLE `User`
  ADD CONSTRAINT `fk_agency_id` FOREIGN KEY (`agency_id`) REFERENCES `Agency` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
