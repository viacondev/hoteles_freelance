<?php
date_default_timezone_set('America/La_Paz');

require_once("../lib/rest/rest-api/Rest.inc.php");

include_once('includes.php');
include_once('model/Profiling.php');
include_once('controller/RestController.php');
session_start();

if (isset($_GET['controller']) && isset($_GET['action'])) {
  $_GET['current_user'] = (array)$_SESSION['current_user'];
  $res = RestController::loadAction($_GET);
  echo $res;
}

if (isset($_POST['controller']) && isset($_POST['action'])) {
  $_POST['current_user'] = (array)$_SESSION['current_user'];
  $res = RestController::loadAction($_POST);
  echo $res;
}
?>
