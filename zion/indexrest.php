<?php
include('model/Model.php');
include('model/MotorConexion.php');

$_GET['origen']     = 'yc';
$_GET['agency_id']  = $_GET['current_user']['agency_id'];
// $_GET['agency_id']  = 9;
$_GET['user_id']    = $_GET['current_user']['id'];
// $_GET['user_id']    = 177;

$conexion = new MotorConexion();
$conexion->build('search', $_GET);
$resp  = $conexion->consultar();
echo json_encode($resp);

/*
ob_start();
include_once('includes.php');
include_once(__DIR__ . '/../booking/BookingEngine.php');
session_start();
date_default_timezone_set('America/La_Paz');

UserController::verifySession();

$controller_name = isset($_GET['controller']) ? $_GET['controller'] : 'accommodation';
$action_name = isset($_GET['action']) ? $_GET['action'] : 'home';
?>
<!DOCTYPE html>
<html>
<head>
  <title><?php echo TITLE; ?></title>
  <meta charset="utf-8" />
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="shortcut icon" href="assets/images/favicon.png">
  <!-- css -->
  <link href="../lib/jquery-ui/css/custom-theme/jquery-ui-1.10.0.custom.css" media="screen" rel="stylesheet">
  <link href="../lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
  <link href="assets/css/layout.css" rel="stylesheet" media="screen">
  <link href="assets/css/print.css" rel="stylesheet" media="print">
  <?php ApplicationController::loadActionCSS($controller_name, $action_name); ?>

  <!-- js -->
  <script src="../lib/jquery/jquery.min.js"></script>
  <script src="../lib/jquery-ui/assets/js/jquery-ui-1.10.0.custom.min.js"></script>
  <script src="../lib/jquery/jquery.printElement.min.js"></script>
  <script src="assets/javascript/application.js"></script>

  <!-- Overwrite jqueryui widgets to avoid collision with Bootstrap -->
  <script type="text/javascript">
    $.widget.bridge('uibutton', $.ui.button);
    $.widget.bridge('uitooltip', $.ui.tooltip);
  </script>
  <script src="../lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="../lib/bootstrap/js/jquery.bootpag.min.js"></script> <!-- Pagination -->

  <?php ApplicationController::loadActionJS($controller_name, $action_name); ?>
  <script src="assets/javascript/destinations_list.js"></script>

</head>
<body>
  <div id="header">
    <div id="top_header" class="container">
      <div class="row">
        <div class="col-xs-4">
          <div id="agency_logo_header" class="text-center">
            <? $_SESSION['current_agency']->showLogo(); ?>
          </div>
        </div>
        <div id="notifications" class="col-md-8 pull-right text-right col-xs-8">
          <?php echo $_SESSION['agency_name']; ?> |
          <b><?php echo UserController::getCurrentUserFullName(); ?></b>
          (<a href="logout.php">Salir</a>)
          <div id="header_shopping_cart">
            <i class="fa fa-shopping-cart fa-1x"></i> &nbsp;<span id="shopping_cart_status"></span>
            <span id="show-chart-link">
              | <a href="?controller=shopping_cart&amp;action=show" class="loadingAction">Ver carrito</a>
            </span>
          </div>
        </div>
      </div>
    </div>

    <div id="menu">
      <div class="container">
        <div class="col-md-12">
          <ul class="col-md-9">
            <li class="menu_logo"><? loadImage('logo_menu.png'); ?></li>
            <li class="first"><a href="./"><i class="fa fa-home"></i> Alojamientos</a></li>
            <li><a href="./?controller=ticket&amp;action=home"><i class="fa fa-ticket"></i> Entradas y Excursiones</a></li>
            <li><a href="./?controller=transfer&amp;action=home"><i class="fa fa-suitcase"></i> Traslados</a></li>
            <li><a href="./?controller=package&amp;action=home"><i class="fa fa-list-alt"></i> Paquetes</a></li>
          </ul>
          <ul class="col-md-3 text-right">
            <li><a href="indexrest.php?controller=booking&amp;action=agency_books"><i class="fa fa-gears"></i> Administraci&oacute;n</a></li>
          </ul>
        </div>
      </div>
    </div>

  </div>

  <div id="content">
    <?php

    include_once('../lib/rest/php-restclient/restclient.php');
    $params = $_GET;
    $params['current_user'] = $_SESSION['current_user'];
    $api = new RestClient(array('base_url' => 'http://book.barrybolivia.com/zion/'));
    // $api = new RestClient(array('base_url' => 'http://localhost:8080/barryzion/zion/'));
    $result = $api->post('execQuery', json_encode($params), array('Content-Type' => 'application/json'));
    $res = strpos($result->response, 'HTTP');
    if ($res !== false) {
      $res_array = explode("\n", $result->response);
      $cantidad = count($res_array)-1;
      $result->response = $res_array[$cantidad];
    }
    $response_arr = (array) json_decode($result->response);

    // __logarr($result);s
    foreach ($response_arr as $key => $value) {
      if (BookingSerializable::arrIsBookingSerializable($value)) {
        $value = BookingSerializable::deserialize($value);
      }
      else if (is_array($value)) {
        $value_buff = array();
        foreach ($value as $k => $v) {
          if (BookingSerializable::arrIsBookingSerializable($v)) {
            $value_buff[$k] = BookingSerializable::deserialize($v);
          }
          else {
            $value_buff[$k] = $v;
          }
        }
        $value = $value_buff;
      }

      if (gettype($value) == 'object' && get_class($value) == 'stdClass') {

        $value = (array) $value;
      }

      $GLOBALS[$key] = $value != '' ? $value : array();
    }
    ApplicationController::loadHelpers($controller_name, $action_name);
    ApplicationController::loadController($controller_name, $action_name);
    ApplicationController::loadView($controller_name, $action_name);

     ?>
  </div>

  <footer class="hidden-print">
    <div class="container">
      <div class="row">
        <ul class="col-md-3 col-xs-3">
          <li style="padding: 5px 10px;"><strong>Servicios</strong></li>
          <li><a href="./"><i class="fa fa-home"></i> Alojamientos</a></li>
          <li><a href="./?controller=ticket&amp;action=home"><i class="fa fa-ticket"></i> Entradas y Excursiones</a></li>
          <li><a href="./?controller=transfer&amp;action=home"><i class="fa fa-suitcase"></i> Traslados</a></li>
          <li><a href="./paquetes"><i class="fa fa-list-alt"></i> Paquetes</a></li>
        </ul>
        <ul class="col-md-3 col-xs-3">
          <li style="padding: 5px 10px;"><strong>Agencia</strong></li>
          <li><a href="?controller=booking&amp;action=agency_books">Reservas</a></li>
          <li><a href="?controller=user&amp;action=profile">Mi perfil</a></li>
          <?php if ($current_user->role == 'ADMIN') { ?>
            <li id="agency_users"><a href="?controller=agency&amp;action=users">Usuarios</a></li>
            <li id="agency_config"><a href="?controller=agency&amp;action=config">Configuraci&oacute;n</a></li>
          <? } ?>
        </ul>
        <ul class="col-md-6 col-xs-6 text-right">
          <li><strong>Barry Top Services S.R.L.</strong></li>
          <li>C/ Rene Moreno Esq. Pari Edif. Don Alcides Of. 203</li>
          <li>3367135</li>
          <li>barry@barrybolivia.com</li>
        </ul>
      </div>
    </div>
  </footer>

  <footer class="brand hidden-print">
    <div class="container">
      <div class="row">
        <div class="col-md-12 logo">
          <?php loadImage('logo_dark.png'); ?>
          Barry<strong>Bolivia</strong>
        </div>
        <div class="col-md-12"><p>&copy;2014 BarryBolivia. Todos los derechos reservados. Con el respaldo de <b>Barry Top Services S.R.L.</b></p></div>
      </div>
    </div>
  </footer>

  <script type="text/javascript">
    $(function() {
      // REST HERE
    });

    // Update shopping cart status
    $(function() {
      $('#shopping_cart_status').html('<?php echo getShoppingCartStatus() ?>');
      initLoadingMsg();
      <?php
      if (ShoppingCart::getItemsCount() == 0) {
        echo '$("#show-chart-link").hide();';
      }
      ?>
    });

    // header stick
    stickHeaderOnScroll = function() {
      var scroll = $(window).scrollTop();
      if (scroll >= 80) {
        $("#header").addClass('smaller');
      } else {
        $("#header").removeClass("smaller");
      }
    }

    $(window).scroll(stickHeaderOnScroll);
  </script>

  <!-- Chat -->
  <script type='text/javascript'>
    // (function(){ var widget_id = 'YmWkg3t3Th';
    // var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();
  </script>

  <?php include('views/tools/_mailing.php'); ?>
  <div id="spinner" class="hidden-print">Cargando...</div>
</body>
</html>
<?php ob_end_flush(); */?>
