<?php
  include('admin/BD/controladoraBD.php');
  include('admin/entidad/destino.php');
  include('admin/entidad/temporada.php');
  include('admin/entidad/tipo_paquete.php');
  include('admin/entidad/publicacion.php');
  include('admin/control/c_buscar_paquete.php');


  $c_buscar_paquete = new c_buscar_paquete();

  //$resp_destino   = destino::obtener_datos_destino();
  //$resp_temporada = temporada::obtener_datos_temporada();
  //$resp_categoria = tipo_paquete::obtener_datos_tipo_paquete();
  $resp_paquete   = publicacion::obtener_paquetes_x_00_00_00_00(0,5);//obtener ultmo 5 paquetes.


  $resp_tags_destino    = $c_buscar_paquete->traer_publicacion_activo_x_destino();
  $resp_tags_temporada  = $c_buscar_paquete->traer_publicacion_activo_x_temporada();
  $resp_tags_categoria  = $c_buscar_paquete->traer_publicacion_activo_x_categoria();
?>
  <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="col-md-12 wbox" style="padding: 15px;">
              <h3 class="title">Buscar Paquetes:</h3>
              <form class="form-horizontal" role="form" method="get" action="">
                <input type="hidden" name="controller" value="package">
                <input type="hidden" name="action" value="search">
                <div class="form-group">
                  <label for="titulo" class="col-sm-3 control-label">Titulo</label>
                  <div class="col-sm-9"><input type="text" class="form-control" id="titulo" name="titulo" placeholder="Titulo" autocomplete="off"></div>
                </div>
                <div class="form-group">
                  <label for="destino" class="col-sm-3 control-label">Destino</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="destino_nombre" onkeyUp="verificar_vacio();" onkeypress="auto_completar();" name="destino_nombre" placeholder="Destino" autocomplete="off"/>
                    <input type="hidden" id="destino" name="destino"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="temporada" class="col-sm-3 control-label">Temporada</label>
                  <div class="col-sm-9">
                    <select id="temporada" name="temporada" class="form-control">
                      <option value="">-- Temporada --</option>
                      <?php
                      foreach ($resp_tags_temporada as $key => $value)
                      {
                      ?>
                        <option value="<?php echo $key;?>"><?php echo $value['nombre_temporada'];?></option>
                      <?php
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="categoria" class="col-sm-3 control-label">Categoria</label>
                  <div class="col-sm-9">
                    <select id="categoria" name="categoria" class="form-control">
                      <option value="">-- Categoria --</option>
                      <?php
                      foreach ($resp_tags_categoria as $key => $value)
                      {
                      ?>
                        <option value="<?php echo $key;?>"><?php echo $value['descripcion'];?></option>
                      <?php
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-default">Buscar</button>
                    <input type="hidden" name="pag" value="0">
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="col-md-8 text-center">
            <div class="col-md-12 wbox" style="padding: 15px;">
              <?php
              if (count($resp_paquete) > 0)
              {
              ?>
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                  <?php
                  for ($i = 0; $i < count($resp_paquete); $i++)
                  {
                  ?>
                  <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i;?>" class="<?php echo ($i == 0)?"active":""; ?>"></li>
                  <?php
                  }
                  ?>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox" style="height:320px">
                  <?php
                  for ($i = 0; $i < count($resp_paquete); $i++)
                  {
                  ?>
                  <div class="item <?php echo ($i == 0)?"active":""; ?>">
                    <?php
                    if($resp_paquete[$i]['imagen_primera'] == "")
                    {
                      $nombre_fichero = "http://barrybolivia.com/zion/paquetes/admin/images_paquetes/no_foto_p.jpg";
                    }
                    else
                    {
                      $nombre_fichero = "http://barrybolivia.com/zion/paquetes/admin/images_paquetes/".$resp_paquete[$i]['imagen_primera'];
                    }

                    echo "<img src='".$nombre_fichero."' class='img-responsive' alt='Responsive image'>";
                    ?>
                    <input type="hidden" value="<?php echo $resp_paquete[$i]['idpublicacion'];?>">
                    <div class="carousel-caption">&nbsp;</div>
                  </div>
                  <?php
                  }
                  ?>
                </div>

                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
              <?php
              }
              else
              {
                $foto_primero = "http://barrybolivia.com/zion/admin/images_paquetes/no_foto_p.jpg";
                echo "<img src='".$foto_primero."' class='img-responsive' alt='Responsive image'>";
              }
              ?>
            </div>
          </div>
        </div>
        <div class="clearfix">&nbsp;</div>
        <div class="row"><div class="col-md-12"><h2>Sugerencias</h2></div></div>
        <div class="row">
          <div class="col-md-12">
            <?php
            if (count($resp_tags_destino) > 0)
            {
            ?>
            <div class="col-md-4">
              <div class="col-md-12 text-center"><h4>DESTINO</h4></div>
              <div class="col-md-12 wbox" style="padding: 15px;">
                <?php
                foreach ($resp_tags_destino as $key => $value)
                {
                  $link = "?controller=package&action=search&titulo=&destino_nombre=".$value['nombre_destino']."&destino=".$key."&temporada=&categoria=&pag=0";
                  echo "<a href='".$link."'><li>".$value['nombre_destino']." (".count($value['publicacion']).")</li></a>";
                }
                ?>
              </div>
            </div>
            <?php
            }

            if (count($resp_tags_temporada) > 0)
            {
            ?>
            <div class="col-md-4">
              <div class="col-md-12 text-center"><h4>TEMPORADA</h4></div>
              <div class="col-md-12 wbox" style="padding: 15px;">
                <?php
                foreach ($resp_tags_temporada as $key => $value)
                {
                  $link = "?controller=package&action=search&titulo=&destino_nombre=&destino=&temporada=".$key."&categoria=&pag=0";
                  echo "<a href='".$link."'><li>".$value['nombre_temporada']." (".count($value['publicacion']).")</li></a>";
                }
                ?>
              </div>
            </div>
            <?php
            }

            if (count($resp_tags_categoria) > 0)
            {
            ?>
              <div class="col-md-4">
              <div class="col-md-12 text-center"><h4>CATEGORIA</h4></div>
              <div class="col-md-12 wbox" style="padding: 15px;">
                <?php
                foreach ($resp_tags_categoria as $key => $value)
                {
                  $link = "?controller=package&action=search&titulo=&destino_nombre=&destino=&temporada=&categoria=".$key."&pag=0";
                  echo "<a href='".$link."'><li>".$value['descripcion']." (".count($value['publicacion']).")</li></a>";
                }
                ?>
              </div>
            </div>
            <?php
            }
            ?>
          </div>
        </div>
      </div>
  </div>
  <script>
    function verificar_vacio()
    {
      if ($('#destino_nombre').val() == "")
      {
        $('#destino').val("0");
      }
    }

    function auto_completar()
    {
      if($('#destino_nombre').val().length > 1)
      {

        $("#destino_nombre").autocomplete(
        {
          source: "paquetes/search_destino.php?texto="+$('#destino_nombre').val(),
          minLength: 2,
          select: function( event, ui )
          {
            $('#destino').val(ui.item.id);
          }
        });
      }
    }
  </script>
