<?php
  include('admin/BD/controladoraBD.php');
  include('admin/control/c_buscar_paquete.php');
  include('admin/entidad/destino.php');
  include('admin/entidad/temporada.php');
  include('admin/entidad/tipo_paquete.php');
  include('admin/entidad/publicacion.php');

  $c_buscar_paquete = new c_buscar_paquete;

  //$resp_destino   = destino::obtener_datos_destino();
  //$resp_temporada = temporada::obtener_datos_temporada();
  //$resp_categoria = tipo_paquete::obtener_datos_tipo_paquete();
  $resp_paquetes  = $c_buscar_paquete->obtener_paquetes_publicados_zion($_GET['titulo'],$_GET['destino'],$_GET['temporada'],$_GET['categoria'],$_GET['pag']);

  $resp_tags_destino    = $c_buscar_paquete->traer_publicacion_activo_x_destino();
  $resp_tags_temporada  = $c_buscar_paquete->traer_publicacion_activo_x_temporada();
  $resp_tags_categoria  = $c_buscar_paquete->traer_publicacion_activo_x_categoria();
?>
  <div class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <div class="col-md-12 wbox" style="padding: 15px;">
          <h3>Buscar Paquetes</h3>
          <form class="" role="form" method="get" action="">
            <input type="hidden" name="controller" value="package">
            <input type="hidden" name="action" value="search">
            <div class="form-group">
              <label for="titulo">Titulo</label>
              <input type="text" class="form-control" id="titulo" name="titulo" value="<?php echo $_GET['titulo'];?>" placeholder="Titulo">
            </div>
            <div class="form-group">
              <label for="destino">Destino</label>
              <input type="text" class="form-control" id="destino_nombre" value="<?php echo $_GET['destino_nombre'];?>" onkeyUp="verificar_vacio();" onkeypress="auto_completar('destino');" name="destino_nombre" placeholder="Destino" autocomplete="off"/>
              <input type="hidden" id="destino" name="destino" value="<?php echo $_GET['destino'];?>"/>
            </div>
            <div class="form-group">
              <label for="temporada">Temporada</label>
              <select id="temporada" name="temporada" class="form-control">
                      <option value="">-- Temporada --</option>
                      <?php
                      foreach ($resp_tags_temporada as $key => $value)
                      {
                      ?>
                        <option value="<?php echo $key;?>" <?php echo ($_GET['temporada'] == $key)?"selected":"";?>><?php echo $value['nombre_temporada'];?></option>
                      <?php
                      }
                      ?>
                    </select>
            </div>
            <div class="form-group">
              <label for="categoria">Categoria</label>
              <select id="categoria" name="categoria" class="form-control">
                      <option value="">-- Categoria --</option>
                      <?php
                      foreach ($resp_tags_categoria as $key => $value)
                      {
                      ?>
                        <option value="<?php echo $key;?>" <?php echo ($_GET['categoria'] == $key)?"selected":"";?>><?php echo $value['descripcion'];?></option>
                      <?php
                      }
                      ?>
                    </select>
            </div>
            <button type="submit" class="btn btn-default">Buscar</button>
            <input type="hidden" name="pag" value="<?php echo $_GET['pag']?>">
          </form>
        </div>
      </div>
      <div class="col-md-9">
        <?php
        if(count($resp_paquetes) > 0)
        {
          for ($i = 0; $i < count($resp_paquetes); $i++)
          {
            $aux  = $resp_paquetes[$i];
            $sw   = $i > 0? "service_book" : "";

            $link = "?controller=package&action=show&paquete=".$aux['idpublicacion'];
          ?>
          <div class="row">
            <div class="<?php echo $sw;?> col-md-12 wbox" style="padding: 15px;">
              <div class="row">
                <div class="col-md-4">
                  <a href="<?php echo $link;?>">
                    <?php
                    if($aux['imagen_segunda'] == "")
                    {
                      $nombre_fichero = "http://barrybolivia.com/zion/paquetes/admin/images_paquetes/no_foto_s.jpg";
                    }
                    else
                    {
                      $nombre_fichero = "http://barrybolivia.com/zion/paquetes/admin/images_paquetes/".$aux['imagen_segunda'];
                    }
                    ?>
                    <img src="<?php echo $nombre_fichero;?>" class="img-responsive" alt="Responsive image">
                  </a>
                </div>
                <div class="col-md-6">
                  <div class="row">
                    <div class="col-md-12">
                      <a href="<?php echo $link;?>"><h4><?php  echo $aux['titulo'];?></h4></a>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <?php echo (strtotime($aux['fecha_salida']))?"Desde:&nbsp;".date('d/m/Y',strtotime($aux['fecha_salida'])):$aux['fecha_salida'];?>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <?php echo (strtotime($aux['fecha_retorno']))?"Hasta:&nbsp;".date('d/m/Y',strtotime($aux['fecha_retorno'])):$aux['fecha_retorno'];?>
                    </div>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="row"><div class="col-md-12"><strong>Precio desde</strong></div></div>
                  <div class="row"><div class="col-md-12"><h1><span class="">$ <?php echo $aux['precio_desde'] ?></span></h1></div></div>
                </div>
              </div>
            </div>
          </div>
          <?php
          }
        }
        else
        {
        ?>
          <h2>No se encontraron Paquetes</h2>
          <div class="clearfix">&nbsp;</div>
          <div class="row"><div class="col-md-12"><h3>Sugerencias</h3></div></div>
          <div class="row">
            <div class="col-md-12">
              <?php
              if (count($resp_tags_destino) > 0)
              {
              ?>
              <div class="col-md-4">
                <div class="col-md-12 text-center"><h4>DESTINO</h4></div>
                <div class="col-md-12 wbox" style="padding: 15px;">
                  <?php
                  foreach ($resp_tags_destino as $key => $value)
                  {
                    $link = "?controller=package&action=search&titulo=&destino_nombre=".$value['nombre_destino']."&destino=".$key."&temporada=&categoria=&pag=0";
                    echo "<a href='".$link."'><li>".$value['nombre_destino']." (".count($value['publicacion']).")</li></a>";
                  }
                  ?>
                </div>
              </div>
              <?php
              }

              if (count($resp_tags_temporada) > 0)
              {
              ?>
              <div class="col-md-4">
                <div class="col-md-12 text-center"><h4>TEMPORADA</h4></div>
                <div class="col-md-12 wbox" style="padding: 15px;">
                  <?php
                  foreach ($resp_tags_temporada as $key => $value)
                  {
                    $link = "?controller=package&action=search&titulo=&destino_nombre=&destino=&temporada=".$key."&categoria=&pag=0";
                    echo "<a href='".$link."'><li>".$value['nombre_temporada']." (".count($value['publicacion']).")</li></a>";
                  }
                  ?>
                </div>
              </div>
              <?php
              }

              if (count($resp_tags_categoria) > 0)
              {
              ?>
                <div class="col-md-4">
                <div class="col-md-12 text-center"><h4>CATEGORIA</h4></div>
                <div class="col-md-12 wbox" style="padding: 15px;">
                  <?php
                  foreach ($resp_tags_categoria as $key => $value)
                  {
                    $link = "?controller=package&action=search&titulo=&destino_nombre=&destino=&temporada=&categoria=".$key."&pag=0";
                    echo "<a href='".$link."'><li>".$value['descripcion']." (".count($value['publicacion']).")</li></a>";
                  }
                  ?>
                </div>
              </div>
              <?php
              }
              ?>
            </div>
          </div>
        <?php
        }
        ?>
      </div>
    </div>

  </div>
  </div>
   <script>
    function verificar_vacio()
    {
      if ($('#destino_nombre').val() == "")
      {
        $('#destino').val("0");
      }
    }

    function auto_completar()
    {
      if($('#destino_nombre').val().length > 1)
      {

        $("#destino_nombre").autocomplete(
        {
          source: "paquetes/search_destino.php?texto="+$('#destino_nombre').val(),
          minLength: 2,
          select: function( event, ui )
          {
            $('#destino').val(ui.item.id);
          }
        });
      }
    }
  </script>
