<?php
/**
* 
*/
class c_buscar_promocion
{
	
	private $array_palabras = array();

	function obtener_titulos()
	{
		$paquetes = publicacion::obtener_paquetes('O');
		for ($i=0; $i < count($paquetes); $i++) 
		{ 
			if($paquetes[$i]['titulo'] != '')
				$this->array_palabras[] = $paquetes[$i]['titulo'];
		}
	}
	function cargar_palabras_clave()
	{
		$this->obtener_titulos();

		return $this->array_palabras;
	}
	function buscar_promociones_por_palabra_clave($palabra, $estado)
	{
		$tipo = 'O';

		$segun_titulos 		= publicacion::obtener_paquete_por_palabra_clave_titulo($palabra, $estado, $tipo);

		$result = $this->retornar_array_mas_fecha_salida_numerica($segun_titulos);

		usort($result, array('c_buscar_promocion','comparar_fecha'));

		return $result;
	}
	function obtener_cantidad_de_cotizaciones($idpublicacion)
	{
		$cantidad_cot = publicacion::obtener_cantidad_de_cotizaciones($idpublicacion);
		if($cantidad_cot !== false)
		{
			if(count($cantidad_cot) > 0)
				return $cantidad_cot[0]['cantidad'];
			return 0;
		}
		return 0;
	}
	function obtener_cantidad_de_envios_masivos($idpublicacion)
	{
		$cantidad_env = publicacion::obtener_cantidad_de_envios_masivos($idpublicacion);
		if($cantidad_env !== false)
		{
			if(count($cantidad_env) > 0)
				return $cantidad_env[0]['cantidad'];
			return 0;
		}
		return 0;
	}
	static function comparar($a, $b) 
	{
	    if (strlen($a) == strlen($b)) {
	        return 0;
	    }
	    return (strlen($a) < strlen($b)) ? -1 : 1;
	}
	static function comparar_fecha($a, $b) 
	{
		$fecha_a 	= $a['salida_num'];
		$fecha_b 	= $b['salida_num'];
	    if ($fecha_a == $fecha_b) {
	        return 0;
	    }
	    return ($fecha_a < $fecha_b) ? -1 : 1;
	}
	static function obtener_mes_numerico($mes)
	{
		switch ($mes) {
			case 'ENE': return '01';
			case 'FEB': return '02';
			case 'MAR': return '03';
			case 'ABR': return '04';
			case 'MAY': return '05';
			case 'JUN': return '06';
			case 'JUL': return '07';
			case 'AGO': return '08';
			case 'SEP': return '09';
			case 'OCT': return '10';
			case 'NOV': return '11';
			case 'DIC': return '12';
			default: return '00';
		}
	}
	function retornar_array_mas_fecha_salida_numerica($array_in)
	{
		$array = $array_in;
		for ($i=0; $i < count($array); $i++) 
		{ 
			$salida = $array[$i]['salida'];
			$a_salida = explode('/', $salida);
			$dia = $a_salida[0];
			$mes = $this->obtener_mes_numerico(strtoupper($a_salida[1]));
			$anio = $a_salida[2];

			$array[$i]['salida_num'] = strtotime($anio . '-' . $mes . '-' . $dia);
			if($array[$i]['salida_num'] == '')
				$array[$i]['salida_num'] = 0;
		}
		return $array;
	}
}
?>