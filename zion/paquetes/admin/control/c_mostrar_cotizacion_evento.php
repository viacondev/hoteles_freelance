<?php

/**
* 
*/
class c_mostrar_cotizacion_evento
{
	
	function obtener_cotizacion_evento($idevento_cotizado)
	{
		$evento_cotizado 	= new evento_cotizado;
		$cotizacion 		= $evento_cotizado->obtener_evento_cotizado($idevento_cotizado);

		if($cotizacion !== false)
		{
			if(count($cotizacion) > 0)
			{
				return $cotizacion[0];
			}
			return false;
		}
		return false;
	}

	function obtener_evento($idevento)
	{
		$evento 	= new evento;
		$mi_evento 	= $evento->obtener_evento($idevento);

		if($mi_evento !== false)
		{
			if(count($mi_evento) > 0)
			{
				return $mi_evento[0];
			}
			return false;
		}
		return false;
	}

	function obtener_datos_cliente($idclientes)
	{
		$clientes = new clientes;
		$cliente = $clientes->enviar_datos_basicos_cliente($idclientes);
		if($cliente!==false)
		{
			if(count($cliente)>0)
			{
				return $cliente[0];
			}
			return false;
		}
		return false;
	}

	function obtener_seguimientos($idevento_cotizado)
	{
		$seguimiento_cotizacion = new seguimiento_cotizacion_evento;
		$seguimientos = $seguimiento_cotizacion->obtener_seguimientos_por_cotizacion($idevento_cotizado);
		if($seguimientos !== false)
		{
			if(count($seguimientos) > 0)
			{
				return $seguimientos;
			}
			return false;
		}
		return false;
	}

	function obtener_telefonos_cliente($idclientes)
	{
		$telefono = new telefono;
		$telefonos = $telefono->obtener_telefonos_cliente($idclientes);
		if($telefonos !== false)
		{
			if(count($telefonos) > 0)
			{
				return $telefonos;
			}
			return false;
		}
		return false;
	}

	function obtener_mails_cliente($idclientes)
	{
		$e_mails = new e_mails;
		$correos = $e_mails->obtener_mails_cliente($idclientes);
		if($correos !== false)
		{
			if(count($correos) > 0)
			{
				return $correos;
			}
			return false;
		}
		return false;
	}

	function obtener_datos_usuario($idusuarios)
	{
		$usuarios 		= new usuarios;
		$datos_usuario 	= $usuarios->obtener_datos_usuario($idusuarios);
		if($datos_usuario !== false)
		{
			if(count($datos_usuario) > 0)
			{
				return $datos_usuario[0];
			}
			return false;
		} 
		return false;
	}

	function cerrar_cotizacion($idevento_cotizado, $estado)
	{
		$evento_cotizado 	= new evento_cotizado;
		$cambio_estado 		= $evento_cotizado->cambiar_estado_cotizacion($idevento_cotizado, $estado);

		return $cambio_estado;
	}

	function obtener_pasajeros_de_cotizacion($idevento_cotizado)
	{
		$evento_cotizado 	= new evento_cotizado;
		$pasajeros 			= $evento_cotizado->obtener_pasajeros_de_cotizacion($idevento_cotizado);

		if($pasajeros !== false)
		{
			if(count($pasajeros) > 0)
			{
				return $pasajeros;
			}
			return false;
		}
		return false;
	}

	function obtener_tarifas_hotel_por_evento($idevento)
	{
		$tarifa_hotel_evento = new tarifa_hotel;
		$tarifas_hotel = $tarifa_hotel_evento->obtener_tarifas($idevento, 'evento_cotizado');

		if($tarifas_hotel !== false)
		{
			if(count($tarifas_hotel) > 0)
			{
				for ($i=0; $i < count($tarifas_hotel); $i++) 
				{ 
					$idtarifa = $tarifas_hotel[$i]['idtarifa_hotel_evento_cotizado'];

					$items = $this->obtener_items_de_tarifa($idtarifa);

					$tarifas_hotel[$i]['opciones'] = $items;
				}
				
				return $tarifas_hotel;
			}
			return false;
		}
		return false;
	}

	function obtener_combinaciones_hoteles($idevento)
	{
		$opcion_hoteles = new opcion_hoteles;
		$opciones 		= $opcion_hoteles->obtener_opcion_hoteles($idevento, 'evento_cotizado');

		$array_hoteles  = array();

		if($opciones !== false)
		{
			if(count($opciones) > 0)
			{
				for ($i=0; $i < count($opciones); $i++) 
				{ 
					$a_opcion = array();
					$opcion = $opciones[$i];
					$idopc 	= $opcion['idopcion_hoteles_evento_cotizado'];

					$items = $opcion_hoteles->obtener_opcion_hoteles_detallada($idopc, 'evento_cotizado');
					if(count($items) > 0)
					{
						$sgl = 0;
						$dbl = 0;
						$tpl = 0;
						$cpl = 0;
						$cnn = 0;
						$inf = 0;
						$hot = "";
						$alm = "";
						$iex = "";
						//echo "ITEMS<pre>"; print_r($items); echo "</pre>";
						for ($j=0; $j < count($items); $j++) 
						{ 
							$cant_noches = $items[$j]['cant_noches'];
							if($cant_noches <= 0)
							{
								$f_in 	= strtotime($items[$j]['in'] . ' 00:00:00');
								$f_out 	= strtotime($items[$j]['out'] . ' 00:00:00');
								$cant_noches = floor( $f_out - $f_in) / 86400;
							}

							$sgl += $items[$j]['precio_single'] * $cant_noches * (1 + $items[$j]['comision']/100) * (1 + $items[$j]['factura']/100);
							$dbl += $items[$j]['precio_doble'] * $cant_noches * (1 + $items[$j]['comision']/100) * (1 + $items[$j]['factura']/100);
							$tpl += $items[$j]['precio_triple'] * $cant_noches * (1 + $items[$j]['comision']/100) * (1 + $items[$j]['factura']/100);
							$cpl += $items[$j]['precio_cuadruple'] * $cant_noches * (1 + $items[$j]['comision']/100) * (1 + $items[$j]['factura']/100);
							$cnn += $items[$j]['precio_menor'] * $cant_noches * (1 + $items[$j]['comision']/100) * (1 + $items[$j]['factura']/100);
							$inf += $items[$j]['precio_infante'] * $cant_noches * (1 + $items[$j]['comision']/100) * (1 + $items[$j]['factura']/100);

							$hot .= "<a href='http://" . $items[$j]['link'] . "' style='text-decoration:none;'>". strtoupper(nl2br($items[$j]['nombre_hotel'])) . ' - ' . $items[$j]['categoria'] . '* (' . $items[$j]['destino'] . ')</a><br/>';
							if($items[$j]['alimentacion'] != '')
								$alm .= '- ' . $items[$j]['alimentacion'] . '<br/>';
							if($items[$j]['info_extra'] != '')
								$iex .= '- ' . $items[$j]['info_extra'] . '<br/>';
						}
						$a_opcion['hotel'] 				= $hot;
						$a_opcion['precio_single'] 		= $sgl;
						$a_opcion['precio_doble'] 		= $dbl;
						$a_opcion['precio_triple'] 		= $tpl;
						$a_opcion['precio_cuadruple'] 	= $cpl;
						$a_opcion['precio_menor'] 		= $cnn;
						$a_opcion['precio_infante'] 	= $inf;
						$a_opcion['observacion'] 		= $opcion['observacion'];
						$a_opcion['alimentacion'] 		= $alm;
						$a_opcion['info_extra'] 		= $iex;

						$array_hoteles[] = $a_opcion;
					}
				}

				for ($i=0; $i < count($array_hoteles) - 1; $i++) 
				{ 
					for ($j=$i; $j < count($array_hoteles) - 1; $j++) 
					{ 
						$a_opcion1 = $array_hoteles[$j];
						$a_opcion2 = $array_hoteles[$j+1];
						$total1 = $a_opcion1['precio_single'] + $a_opcion1['precio_doble'] + $a_opcion1['precio_triple'] + $a_opcion1['precio_cuadruple']; 
						$total2 = $a_opcion2['precio_single'] + $a_opcion2['precio_doble'] + $a_opcion2['precio_triple'] + $a_opcion2['precio_cuadruple']; 
						if($total2 < $total1)
						{
							$aux = $array_hoteles[$j];
							$array_hoteles[$j] = $array_hoteles[$j+1];
							$array_hoteles[$j+1] = $aux;
						}
					}
				}
				//echo "result<pre>"; print_r($array_hoteles); echo "</pre>";
				return $array_hoteles;
			}
		}
		return false;
	}

	function evaluar_vista_hoteles($array_hoteles)
	{
		$cad_respuesta 		= "";
		$exist_single 		= false;
		$exist_doble 		= false;
		$exist_triple 		= false;
		$exist_cuadruple 	= false;
		$exist_menor 		= false;
		$exist_infante 		= false;

		for ($i=0; $i < count($array_hoteles); $i++) 
		{ 
			if($array_hoteles[$i]['precio_single'] != 0)
				$exist_single 		= true;
			if($array_hoteles[$i]['precio_doble'] != 0)
				$exist_doble 		= true;
			if($array_hoteles[$i]['precio_triple'] != 0)
				$exist_triple 		= true;
			if($array_hoteles[$i]['precio_cuadruple'] != 0)
				$exist_cuadruple 	= true;
			if($array_hoteles[$i]['precio_menor'] != 0)
				$exist_menor 		= true;
			if($array_hoteles[$i]['precio_infante'] != 0)
				$exist_infante 		= true;
		}

		if($exist_single)
			$cad_respuesta .= 'SGL/';
		if($exist_doble)
			$cad_respuesta .= 'DBL/';
		if($exist_triple)
			$cad_respuesta .= 'TPL/';
		if($exist_cuadruple)
			$cad_respuesta .= 'CPL/';
		if($exist_menor)
			$cad_respuesta .= 'CNN/';
		if($exist_infante)
			$cad_respuesta .= 'INF/';

		return $cad_respuesta;
	}

	function obtener_items_de_tarifa($idtarifa_hotel_evento)
	{
		$item_hotel_evento 	= new tarifa_hotel;
		$items 				= $item_hotel_evento->obtener_items_por_tarifa_hotel($idtarifa_hotel_evento, 'evento_cotizado');

		if($items !== false)
		{
			if(count($items) > 0)
			{
				return $items;
			}
			return false;
		}
		return false;
	}

	function obtener_paxs_del_hotel($idtarifa)
	{
		$hotel_cotizado = new hotel_cotizado;
		$nombres = $hotel_cotizado->obtener_nombres_pasajeros_por_hotel($idtarifa, 'evento');

		if($nombres === false)
			$nombres = array();
		return $nombres;
	}

	function obtener_otras_tarifas($idevento)
	{
		$tarifa_otros = new tarifa_otros;
		$tarifas = $tarifa_otros->obtener_tarifa_otros($idevento, 'evento_cotizado');

		if($tarifas !== false)
		{
			if(count($tarifas) > 0)
			{
				$total_adt = 0;
				$total_cnn = 0;
				$total_inf = 0;
				for ($i=0; $i < count($tarifas); $i++) 
				{ 
					$fee 		= $tarifas[$i]['fee'] / 100;
					$factura 	= $tarifas[$i]['factura'] / 100;
					$precio_adt = $tarifas[$i]['precio_adulto'];
					$precio_cnn = $tarifas[$i]['precio_menor'];
					$precio_inf = $tarifas[$i]['precio_infante'];

					if ($tarifas[$i]['moneda']=='B') 
					{
						$precio_adt 	= $precio_adt / 6.96;
						$precio_cnn 	= $precio_cnn / 6.96;
						$precio_inf 	= $precio_inf / 6.96;	
					}

					$subtotal_adt 	= ceil($precio_adt + ($precio_adt * $fee) + ($precio_adt * $factura));
					$subtotal_cnn   = ceil($precio_cnn + ($precio_cnn * $fee) + ($precio_cnn * $factura));
					$subtotal_inf   = ceil($precio_inf + ($precio_inf * $fee) + ($precio_inf * $factura));

					$tarifas[$i]['subtotal_adt'] = $subtotal_adt;
					$tarifas[$i]['subtotal_cnn'] = $subtotal_cnn;
					$tarifas[$i]['subtotal_inf'] = $subtotal_inf;

					if($tarifas[$i]['estado'] == '1')
					{
						$total_adt += $subtotal_adt;
						$total_cnn += $subtotal_cnn;
						$total_inf += $subtotal_inf;
					}
				}
				$tarifas['total_adt'] 	= $total_adt;
				$tarifas['total_cnn'] 	= $total_cnn;
				$tarifas['total_inf'] 	= $total_inf;
				return $tarifas;
			}
			return false;
		}
		return false;
	}

	function obtener_paxs_del_servicio($idtarifa)
	{
		$otros_cotizado = new otros_cotizado;
		$nombres = $otros_cotizado->obtener_nombres_pasajeros_por_servicio($idtarifa, 'evento');

		if($nombres === false)
			$nombres = array();
		return $nombres;
	}

	function obtener_inscripciones($idcotizacion)
	{
		$inscripcion = new inscripcion_cotizado;
		$inscripciones = $inscripcion->obtener_inscripcion_cotizados($idcotizacion);
		if($inscripciones === false)
		{
			$inscripciones = array();
		}
		return $inscripciones;
	}

	function obtener_opciones_de_vuelos($idevento)
	{
		$boleto_cotizado = new boleto_cotizado;
		$opciones_aereo = $boleto_cotizado->obtener_opciones_boletos($idevento, 'evento_cotizado');

		if($opciones_aereo !== false)
		{
			for ($i=0; $i < count($opciones_aereo); $i++) 
			{ 
				$opcion_aereo = $opciones_aereo[$i];
				$idopcion_aereo = $opcion_aereo['idopcion_boleto_evento_cotizado'];

				// CARGAR LOS BOLETOS DENTRO DE LA MISMA OPCION
				$boletos 						= $boleto_cotizado->obtener_boletos_por_opcion($idopcion_aereo, 'evento_cotizado');
				$opciones_aereo[$i]['boletos'] 	= $boletos;
				$total_adt 					= 0;
				$total_cnn 					= 0;
				$total_inf 					= 0;
				for ($j=0; $j < count($boletos); $j++) 
				{ 
					$fact = (1 + ($boletos[$j]['fee']/100)) * (1 + ($boletos[$j]['factura']/100));
					$precio_adt 	= $boletos[$j]['precio_adulto'];
					$precio_cnn 	= $boletos[$j]['precio_menor'];
					$precio_inf 	= $boletos[$j]['precio_infante'];
					$tipo_cambio 	= $boletos[$j]['tipo_cambio'];

					if($boletos[$j]['moneda'] == 'B')
					{
						$precio_adt = $precio_adt / $tipo_cambio;
						$precio_cnn = $precio_cnn / $tipo_cambio;
						$precio_inf = $precio_inf / $tipo_cambio;
					}

					if($boletos[$j]['estado'] == 1)
					{
						$total_adt += $precio_adt * $fact;
						$total_cnn += $precio_cnn * $fact;
						$total_inf += $precio_inf * $fact;	
					}
				}
				$opciones_aereo[$i]['adt'] = ceil($total_adt);
				$opciones_aereo[$i]['cnn'] = ceil($total_cnn);
				$opciones_aereo[$i]['inf'] = ceil($total_inf);

				//CARGAR EL ITINERARIO
				$itinerario = segmento_publicado::obtener_segmentos_de_opcion($idopcion_aereo, 'evento_cotizado');
				$opciones_aereo[$i]['itinerario'] = $itinerario;
			}
		}
		else
		{
			$opciones_aereo = array();	
		}
		return $opciones_aereo;
	}

	function obtener_boletos_cotizados($idevento)
	{
		$boleto_cotizado = new boleto_cotizado;
		$boletos = $boleto_cotizado->obtener_boletos_cotizados($idevento, 'evento_cotizado');

		if($boletos !== false)
		{
			if(count($boletos) > 0)
			{
				$total_adt = 0;
				$total_cnn = 0;
				$total_inf = 0;
				
				for ($i=0; $i < count($boletos); $i++) 
				{ 
					$fee 			= $boletos[$i]['fee'] / 100;
					$factura 		= $boletos[$i]['factura'] / 100;
					$fact 			= (1+$fee) * (1+$factura);
					$precio_adt 	= $boletos[$i]['precio_adulto'];
					$precio_cnn 	= $boletos[$i]['precio_menor'];
					$precio_inf 	= $boletos[$i]['precio_infante'];

					if($boletos[$i]['moneda'] == 'B')
					{
						$precio_adt = $precio_adt / $boletos[$i]['tipo_cambio'];
						$precio_cnn = $precio_cnn / $boletos[$i]['tipo_cambio'];
						$precio_inf = $precio_inf / $boletos[$i]['tipo_cambio'];
					}

					$subtotal_adt 	= ceil($precio_adt * $fact);
					$subtotal_cnn   = ceil($precio_cnn * $fact);
					$subtotal_inf   = ceil($precio_inf * $fact);

					$boletos[$i]['subtotal_adt'] = $subtotal_adt;
					$boletos[$i]['subtotal_cnn'] = $subtotal_cnn;
					$boletos[$i]['subtotal_inf'] = $subtotal_inf;

					if($boletos[$i]['estado'] == '1')
					{
						$total_adt += $subtotal_adt;
						$total_cnn += $subtotal_cnn;
						$total_inf += $subtotal_inf;	
					}
				}
				$boletos['total_adt'] 	= $total_adt;
				$boletos['total_cnn'] 	= $total_cnn;
				$boletos['total_inf'] 	= $total_inf;
				return $boletos;
			}
			return false;
		}
		return false;
	}

	function obtener_paxs_del_boleto($idboleto)
	{
		$boleto_cotizado = new boleto_cotizado;
		$nombres = $boleto_cotizado->obtener_nombres_pasajeros_por_boleto($idboleto, 'evento');

		if($nombres === false)
			$nombres = array();
		return $nombres;
	}

	function obtener_segmentos_publicados($idevento)
	{
		$segmento_publicado = new segmento_publicado;
		$segmentos = $segmento_publicado->obtener_segmentos($idevento, 'evento_cotizado');

		if($segmentos !== false)
		{
			if(count($segmentos) > 0)
			{
				return $segmentos;
			}
			return false;
		}
		return false;
	}

	private $buffer_lineas_aereas = array();
	function obtener_linea_aerea($idlinea_aerea)
	{
		if(array_key_exists($idlinea_aerea, $this->buffer_lineas_aereas))
		{
			return $this->buffer_lineas_aereas[$idlinea_aerea];
		}
		else
		{
			$linea_aerea = lineas_aereas::obtener_linea_aerea_por_id($idlinea_aerea);
			if($linea_aerea !== false)
			{
				if(count($linea_aerea) > 0)
				{
					$aerolinea = explode(',', $linea_aerea[0]['nombre_linea_aerea']);
					$this->buffer_lineas_aereas[$idlinea_aerea] = strtoupper($aerolinea[0] . '(' . $linea_aerea[0]['codigo_iata_linea_aerea'] . ')');
					return $this->buffer_lineas_aereas[$idlinea_aerea];
				}
				$this->buffer_lineas_aereas[$idlinea_aerea] = 'N/I';
				return 'N/I';
			}
			$this->buffer_lineas_aereas[$idlinea_aerea] = 'N/I';
			return 'N/I';
		}
	}

	private $buffer_aeropuertos = array();
	function obtener_aeropuerto($codigo)
	{
		if(array_key_exists($codigo, $this->buffer_aeropuertos))
		{
			return $this->buffer_aeropuertos[$codigo];
		}
		else
		{
			$aeropuerto = aeropuerto::obtener_aeropuerto_por_codigo($codigo);
			
			if($aeropuerto !== false)
			{
				if(count($aeropuerto) > 0)
				{
					$ciudad = explode(',', $aeropuerto[0]['nombre_aeropuerto']);
					$this->buffer_aeropuertos[$codigo] = strtoupper($ciudad[0] . '(' . $aeropuerto[0]['cod_aeropuerto'] . ')');
					return $this->buffer_aeropuertos[$codigo];
				}
				$this->buffer_aeropuertos[$codigo] = 'N/I';
				return 'N/I';
			}
			$this->buffer_aeropuertos[$codigo] = 'N/I';
			return 'N/I';
		}
	}
	
	function obtener_total_monto_pax($idpax)
	{
		$class 	= "evento";
		$resp 	= boleto_cotizado::obtenet_boleto_cotizado_idpax($idpax, $class);
		$total 	= 0;
		
		if(count($resp) > 0)
		{
			for($i = 0; $i < count($resp); $i++)
			{
				$precio 	= 0;
				$fee 		= 0;
				$factura	= 0;
				$r = boleto_cotizado::obtener_boleto_ruta_precio($resp[$i]['idboleto_cotizado_evento_cotizado'],$class);
				
				if($resp[$i]['tipo_precio'] == 1)
				{
					$precio 	= $r[0]['precio_adulto'];
					$factor 	= (1+$r[0]['fee']/100) * (1+$r[0]['factura']/100);
					$precio 	= $precio * $factor;
				}
				else
				{
					if($resp[$i]['tipo_precio'] == 2)
					{
						$precio 	= $r[0]['precio_menor'];
						$factor 	= (1+$r[0]['fee']/100) * (1+$r[0]['factura']/100);
					$precio 	= $precio * $factor;
					}
					else
					{
						$precio 	= $r[0]['precio_infante'];
						$factor 	= (1+$r[0]['fee']/100) * (1+$r[0]['factura']/100);
						$precio 	= $precio * $factor;
					}	
				}
				
				if($r[0]['moneda'] == "B")
				{
					$total += round($precio/ $r[0]['tipo_cambio'],2);
				}
				else
				{
					$total += round($precio,2);
				}
				
			}
		}
		return $total;
	}
	function obtener_min_time_limit_boleto($idpax)
	{
		$class 			= "evento";
		$resp 	= boleto_cotizado::obtenet_boleto_cotizado_idpax($idpax, $class);
		$min_time_limit = strtotime('+1 year');

		if(count($resp) > 0)
		{
			
			for($i = 0; $i < count($resp); $i++)
			{
				$r = boleto_cotizado::obtener_boleto_ruta_precio($resp[$i]['idboleto_cotizado_evento_cotizado'],$class);

				$cur_tlimit = strtotime($r[0]['time_limit']);
				
				if($cur_tlimit < $min_time_limit)
					$min_time_limit = $cur_tlimit;
			}
		}
		else
			return '-';
		return date('d/M/Y', $min_time_limit);
	}
	function obtener_boletos_por_pax($idpax)
	{
		$class = "evento";
		$resp = boleto_cotizado::obtener_boletos_por_pax($idpax, $class);
		if($resp === false)
		{
			$resp = array();
		}
		
		return $resp;
	}
	
	function obtener_total_monto_pax_hotel($idpax)
	{
		$class 			= "evento";
		$resp 			= hotel_cotizado::obtenet_hotel_cotizado_idpax($idpax, $class);
		$total 			= 0;
		
		if(count($resp) > 0)
		{
			
			for($i = 0; $i < count($resp); $i++)
			{
				$precio 	= 0;
				$comision 	= 0;
				$factura	= 0;
				$r 			= hotel_cotizado::obtener_item_hotel_class_cotizado_id($resp[$i]['iditem_hotel_'.$class.'_cotizado'], $class);
				
				switch($resp[$i]['tipo_precio'])
				{
					case 1:
					{
						$precio 	= $r[0]['precio_single'];
						$factor 	= (1+$r[0]['comision']/100) * (1+$r[0]['factura']/100);
						$precio 	= $precio * $factor;
						break;	
					}
					case 2:
					{
						$precio 	= $r[0]['precio_doble'];
						$factor 	= (1+$r[0]['comision']/100) * (1+$r[0]['factura']/100);
						$precio 	= $precio * $factor;
						break;	
					}
					case 3:
					{
						$precio 	= $r[0]['precio_triple'];
						$factor 	= (1+$r[0]['comision']/100) * (1+$r[0]['factura']/100);
						$precio 	= $precio * $factor;
						break;	
					}
					case 4:
					{
						$precio 	= $r[0]['precio_cuadruple'];
						$factor 	= (1+$r[0]['comision']/100) * (1+$r[0]['factura']/100);
						$precio 	= $precio * $factor;
						break;	
					}
					case 5:
					{
						$precio 	= $r[0]['precio_menor'];
						$factor 	= (1+$r[0]['comision']/100) * (1+$r[0]['factura']/100);
						$precio 	= $precio * $factor;
						break;	
					}
					case 6:
					{
						$precio 	= $r[0]['precio_infante'];
						$factor 	= (1+$r[0]['comision']/100) * (1+$r[0]['factura']/100);
						$precio 	= $precio * $factor;
						break;	
					}
				}

				$cant_noches = $r[0]['cant_noches'];
				if($cant_noches == 0)
				{
					$cant_noches = floor( (strtotime($r[0]['out'] . ' 00:00:00') - strtotime($r[0]['in'] . ' 00:00:00')) / 86400);
				}
				$precio = $precio * $cant_noches;
				
				if($r[0]['moneda'] == "B")
				{
					$total += round($precio/ $r[0]['tipo_cambio'],2);
				}
				else
				{
					$total += round($precio,2);
				}
			}
		}
		return $total;
	}

	function obtener_min_time_limit_hotel($idpax)
	{
		$class 			= "evento";
		$resp 			= hotel_cotizado::obtenet_hotel_cotizado_idpax($idpax, $class);
		$min_time_limit = strtotime('+1 year');
		
		if(count($resp) > 0)
		{
			
			for($i = 0; $i < count($resp); $i++)
			{
				$r 			= hotel_cotizado::obtener_item_hotel_class_cotizado_id($resp[$i]['iditem_hotel_'.$class.'_cotizado'], $class);

				$cur_tlimit = strtotime($r[0]['time_limit']);
				if($cur_tlimit < $min_time_limit)
					$min_time_limit = $cur_tlimit;
			}
		}
		else
			return '-';
			
		return date('d/M/Y', $min_time_limit);
	}

	function obtener_servicios_de_hotel_por_pax($idpax)
	{
		$class = "evento";
		$resp = hotel_cotizado::obtener_items_hoteles_por_pax($idpax, $class);
		if($resp === false)
		{
			$resp = array();
		}
		
		return $resp;
	}
	
	function obtener_total_monto_pax_otros($idpax)
	{
		$class = "evento";
		$resp 	= otros_cotizado::obtenet_otros_cotizado_idpax($idpax, $class);
		$total 	= 0;
		
		if(count($resp) > 0)
		{
			
			for($i = 0; $i < count($resp); $i++)
			{
				$precio 	= 0;
				$fee 		= 0;
				$factura	= 0;
				$r = otros_cotizado::obtener_otros_nombre_precio($resp[$i]['idtarifa_otros_'.$class.'_cotizado'],$class);
				
				if($resp[$i]['tipo_precio'] == 1)
				{
					$precio 	= $r[0]['precio_adulto'];
					$factor 	= (1+$r[0]['fee']/100) * (1+$r[0]['factura']/100);
					$precio 	= $precio * $factor;
				}
				else
				{
					if($resp[$i]['tipo_precio'] == 2)
					{
						$precio 	= $r[0]['precio_menor'];
						$factor 	= (1+$r[0]['fee']/100) * (1+$r[0]['factura']/100);
						$precio 	= $precio * $factor;
					}
					else
					{
						$precio 	= $r[0]['precio_infante'];
						$factor 	= (1+$r[0]['fee']/100) * (1+$r[0]['factura']/100);
						$precio 	= $precio * $factor;
					}	
				}
				if($r[0]['moneda'] == "B")
				{
					$total += round($precio/ $r[0]['tipo_cambio'],2);
				}
				else
				{
					$total += round($precio,2);
				}
			}
		}
		return $total;
	}

	function obtener_otros_servicios_por_pax($idpax)
	{
		$class 	= "evento";
		$resp 	= otros_cotizado::obtener_otros_servicios_por_pax($idpax, $class);
		if($resp === false)
		{
			$resp = array();
		}
		
		return $resp;
	}
	
	function obtener_total_monto_pax_inscripcion($idpax)
	{
		$class = "evento";
		$resp 	= inscripcion_cotizado::obtener_inscripcion_idpax($idpax);
		$total 	= 0;
		
		if(count($resp) > 0)
		{
			
			for($i = 0; $i < count($resp); $i++)
			{
				$precio 	= 0;
				$fee 		= 0;
				$factura	= 0;
				

				$precio 	= $resp[$i]['precio'];
				$factor 	= (1+$resp[$i]['fee']/100) * (1+$resp[$i]['factura']/100);
				$precio 	= $precio * $factor;

				if($resp[$i]['moneda'] == "B")
				{
					$total += round($precio/ $resp[$i]['tipo_cambio'],2);
				}
				else
				{
					$total += round($precio,2);
				}
			}
		}
		return $total;
	}

	function obtener_min_time_limit_inscripcion($idpax)
	{
		$class 			= "evento";
		$resp 	= inscripcion_cotizado::obtener_inscripcion_idpax($idpax);
		$min_time_limit = strtotime('+1 year');

		if(count($resp) > 0)
		{
			
			for($i = 0; $i < count($resp); $i++)
			{
				$cur_tlimit = strtotime($resp[$i]['time_limit']);
				if($cur_tlimit < $min_time_limit)
					$min_time_limit = $cur_tlimit;
			}
		}
		else
			return '-';
		return date('d/M/Y', $min_time_limit);
	}

	function obtener_inscripciones_por_pax($idpax)
	{
		$resp = inscripcion_cotizado::obtener_inscripciones_por_pax($idpax);
		if($resp === false)
			$resp 	= array();
		return $resp;
	}
}

?>