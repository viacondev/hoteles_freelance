<?php
    set_time_limit(0);
    include('../BD/controladoraBD.php');
    include('../entidad/archivo_adjunto.php');
    //Como no sabemos cuantos archivos van a llegar, iteramos la variable $_FILES
    $idservicio     = $_POST['servicio'];
    $type           = $_POST['type'];
    $descripcion    = $_POST['descripcion'];
    $is_visible     = $_POST['is_visible'];
    
    $ruta="../adjuntos_paquetes/";
    foreach ($_FILES as $key) 
    {
        if($key['error'] == UPLOAD_ERR_OK )
        {
            $proximo_id = archivo_adjunto::obtener_ultimo_archivo_id() + 1;

            $a_nombre   = explode('.', $key['name']);//Obtenemos el nombre del archivo
            $extension  = end($a_nombre);
            $temporal   = $key['tmp_name']; //Obtenemos el nombre del archivo temporal
            $tamano     = ($key['size'] / 1000)."Kb"; //Obtenemos el tamaño en KB
            $new_name   = 'arch' . $type . '_' . $proximo_id . '_' . $idservicio . '.' . $extension;

            move_uploaded_file($temporal, $ruta . $new_name); 
        
            $registro_archivo_adjunto       = archivo_adjunto::registrar_nuevo_archivo_adjunto($new_name, $descripcion);
            $idarchivo_adjunto              = mysql_insert_id();

            switch ($type) 
            {
                case 'h':
                    $registro_servicio_archivo   = archivo_adjunto::registrar_archivo_hotel($idarchivo_adjunto, $idservicio, $is_visible);
                    break;
                case 'b':
                    $registro_servicio_archivo   = archivo_adjunto::registrar_archivo_aereo($idarchivo_adjunto, $idservicio, $is_visible);
                    break;
                case 's':
                    $registro_servicio_archivo   = archivo_adjunto::registrar_archivo_servicio($idarchivo_adjunto, $idservicio, $is_visible);
                    break;
                case 'i':
                    $registro_servicio_archivo   = archivo_adjunto::registrar_archivo_inscripcion($idarchivo_adjunto, $idservicio, $is_visible);
                    break;
                default:
                    # code...
                    break;
            }

            if($is_visible == 0)
                $color = '#FF3300';
            else
                $color = '#999999';

            ?>
            
            <div id="adj<?php echo $type . $idarchivo_adjunto; ?>" onmouseover="$('#deladj<?php echo $type . $idarchivo_adjunto; ?>').show();" onmouseout="$('#deladj<?php echo $type . $idarchivo_adjunto; ?>').hide();" >
                <a style="color:<?php echo $color; ?>;" href="<?php echo $ruta . $new_name; ?>" target="_blank"><?php echo substr($descripcion, 0, 15); ?></a>
                <img id="deladj<?php echo $type . $idarchivo_adjunto; ?>" src="images/userminus32.png" style="width:15px; height:15px; cursor:pointer; display:none;" title="Eliminar Archivo" onclick="eliminar_adjunto_x_servicio(<?php echo $idservicio; ?>, '<?php echo $type; ?>', '<?php echo $idarchivo_adjunto; ?>');" />         
            </div>

            <?php
        }
        else
        {
            echo $key['error'];
        }
    }
?>