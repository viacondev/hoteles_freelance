<?php

/**
* 
*/
class c_crear_cotizacion_paquete
{
	
	function crear_cotizacion_desde_paquete($idpaquete, $idclientes, $observaciones, $idusuarios, $idsolicitante)
	{
		$paquete 		= new paquete;
		$cont_paquete 	= $paquete->obtener_contenido_paquete($idpaquete);
		if($cont_paquete!==false)
		{
			if(count($cont_paquete)>0)
			{
				$paquete_actual 		= $cont_paquete[0];
				$paquete_cotizado 	= new paquete_cotizado;
				$solicitante 		= 0;
				if($idsolicitante != "")
				{
					$solicitante 	= $idsolicitante;
				}
				if($paquete_actual['solo_imagen'] == 'N')
				{
					$paquete_incluye 	= $paquete_actual['paquete_incluye'];
					$importante 		= $paquete_actual['importante'];

					$r_cotizacion 		= $paquete_cotizado->registrar_paquete_cotizado($idpaquete, $paquete_incluye, $importante, $observaciones, $idclientes, $idusuarios, $solicitante);
					return $r_cotizacion;
				}
				else
				{
					$r_cotizacion 		= $paquete_cotizado->registrar_paquete_cotizado($idpaquete, "", "",$observaciones, $idclientes, $idusuarios, $solicitante);
					return $r_cotizacion;
				}
			}
			return false;
		}
		return false;
	}

	function obtener_cotizacion_paquete($idpaquete_cotizado)
	{
		$paquete_cotizado = new paquete_cotizado;
		$cotizaciones = $paquete_cotizado->obtener_paquete_cotizado($idpaquete_cotizado);
		if($cotizaciones!==false)
		{
			if(count($cotizaciones)>0)
			{
				return $cotizaciones[0];
			}
			return false;
		}
		return false;
	}

	function obtener_datos_cliente($idclientes)
	{
		$clientes = new clientes;
		$cliente = $clientes->enviar_datos_basicos_cliente($idclientes);
		if($cliente!==false)
		{
			if(count($cliente)>0)
			{
				return $cliente[0];
			}
			return false;
		}
		return false;
	}

	function obtener_datos_usuario($idusuarios)
	{
		$usuarios 	= new usuarios;
		$usuario  	= $usuarios->obtener_datos_usuario($idusuarios);

		if($usuario !== false)
		{
			if(count($usuario)>0)
			{
				return $usuario[0];
			}
			return false;
		}
		return false;
	}

	/*
	* LOS BOLETOS E ITINERARIO ESTAN AGRUPADOS POR OPCIONES OFRECIDAS AL CLIENTE
	*/

	function obtener_opciones_boletos($idpaquete, $type)
	{
		$boleto_cotizado = new boleto_cotizado;
		$boletos = $boleto_cotizado->obtener_opciones_boletos($idpaquete, $type);

		if($boletos !== false)
		{
			return $boletos;
		}
		$boletos 	= array();
		return $boletos;	
	}
	function obtener_boletos_de_opcion($idopcion, $type)
	{
		$boleto_cotizado = new boleto_cotizado;
		$boletos = $boleto_cotizado->obtener_boletos_por_opcion($idopcion, $type);

		if($boletos !== false)
		{
			return $boletos;
		}
		$boletos 	= array();
		return $boletos;
	}
	function obtener_segmentos_de_opcion($idopcion, $type)
	{
		$segmento_publicado = new segmento_publicado;
		$segmentos = $segmento_publicado->obtener_segmentos_de_opcion($idopcion, $type);

		if($segmentos !== false)
		{
			return $segmentos;
		}
		$segmentos = array();
		return $segmentos;
	}

	/***/

	/*
	* GESTION DE PRECIOS
	*/

	function obtener_tarifas_hotel_por_paquete($idpaquete, $type)
	{
		$tarifa_hotel_paquete = new tarifa_hotel;
		$tarifas_hotel = $tarifa_hotel_paquete->obtener_tarifas($idpaquete, $type);

		if($tarifas_hotel !== false)
		{
			if(count($tarifas_hotel) > 0)
			{
				return $tarifas_hotel;
			}
			return false;
		}
		return false;
	}

	function obtener_items_de_tarifa($idtarifa_hotel_paquete, $type)
	{
		$item_hotel_paquete 	= new tarifa_hotel;
		$items 				= $item_hotel_paquete->obtener_items_por_tarifa_hotel($idtarifa_hotel_paquete, $type);

		if($items !== false)
		{
			if(count($items) > 0)
			{
				return $items;
			}
			return false;
		}
		return false;
	}

	function obtener_paxs_del_hotel($idtarifa)
	{
		$hotel_cotizado = new hotel_cotizado;
		$nombres = $hotel_cotizado->obtener_nombres_pasajeros_por_hotel($idtarifa, 'paquete');

		if($nombres === false)
			$nombres = array();
		return $nombres;
	}

	function obtener_otras_tarifas($idpaquete, $type)
	{
		$tarifa_otros = new tarifa_otros;
		$tarifas = $tarifa_otros->obtener_tarifa_otros($idpaquete, $type);

		if($tarifas !== false)
		{
			if(count($tarifas) > 0)
			{
				return $tarifas;
			}
			return false;
		}
		return false;
	}

	function obtener_paxs_del_servicio($idtarifa)
	{
		$otros_cotizado = new otros_cotizado;
		$nombres = $otros_cotizado->obtener_nombres_pasajeros_por_servicio($idtarifa, 'paquete');

		if($nombres === false)
			$nombres = array();
		return $nombres;
	}

	function obtener_nombre_servicio($idotro_servicio)
	{
		$otros_servicios = new pqt_otro_servicio;
		$servicio = $otros_servicios->obtener_otro_servicio($idotro_servicio);
		if($servicio !== false && count($servicio)>0)
			return strtoupper($servicio[0]['nombre_servicio']);
		return 'NO EXISTE SERVICIO';
	}

	function obtener_boletos_cotizados($idpaquete, $type)
	{
		$boleto_cotizado = new boleto_cotizado;
		$boletos = $boleto_cotizado->obtener_boletos_cotizados($idpaquete, $type);

		if($boletos !== false)
		{
			if(count($boletos) > 0)
			{
				return $boletos;
			}
			return false;
		}
		return false;
	}

	function obtener_paxs_del_boleto($idboleto)
	{
		$boleto_cotizado = new boleto_cotizado;
		$nombres = $boleto_cotizado->obtener_nombres_pasajeros_por_boleto($idboleto, 'paquete');

		if($nombres === false)
			$nombres = array();
		return $nombres;
	}

	function obtener_segmentos_publicados($idpaquete, $type)
	{
		$segmento_publicado = new segmento_publicado;
		$segmentos = $segmento_publicado->obtener_segmentos($idpaquete, $type);

		if($segmentos !== false)
		{
			if(count($segmentos) > 0)
			{
				return $segmentos;
			}
			return false;
		}
		return false;
	}

	private $buffer_lineas_aereas = array();
	function obtener_linea_aerea($idlinea_aerea)
	{
		if(array_key_exists($idlinea_aerea, $this->buffer_lineas_aereas))
		{
			return $this->buffer_lineas_aereas[$idlinea_aerea];
		}
		else
		{
			$linea_aerea = lineas_aereas::obtener_linea_aerea_por_id($idlinea_aerea);
			if($linea_aerea !== false)
			{
				if(count($linea_aerea) > 0)
				{
					$aerolinea = explode(',',$linea_aerea[0]['nombre_linea_aerea']);
					$this->buffer_lineas_aereas[$idlinea_aerea] = strtoupper($aerolinea[0] . '(' . $linea_aerea[0]['codigo_iata_linea_aerea'] . ')');
					return $this->buffer_lineas_aereas[$idlinea_aerea];
				}
				$this->buffer_lineas_aereas[$idlinea_aerea] = 'N/I';
				return 'N/I';
			}
			$this->buffer_lineas_aereas[$idlinea_aerea] = 'N/I';
			return 'N/I';
		}
	}

	private $buffer_aeropuertos = array();
	function obtener_aeropuerto($codigo)
	{
		if(array_key_exists($codigo, $this->buffer_aeropuertos))
		{
			return $this->buffer_aeropuertos[$codigo];
		}
		else
		{
			$aeropuerto = aeropuerto::obtener_aeropuerto_por_codigo($codigo);
			
			if($aeropuerto !== false)
			{
				if(count($aeropuerto) > 0)
				{
					$ciudad = explode(',', $aeropuerto[0]['nombre_aeropuerto']);
					$this->buffer_aeropuertos[$codigo] = strtoupper($ciudad[0] . '(' . $aeropuerto[0]['cod_aeropuerto'] . ')');
					return $this->buffer_aeropuertos[$codigo];
				}
				$this->buffer_aeropuertos[$codigo] = 'N/I';
				return 'N/I';
			}
			$this->buffer_aeropuertos[$codigo] = 'N/I';
			return 'N/I';
		}
	}

	/*
	* NUEVO FORMATO
	*/
		function obtener_info_cotizacion($idcotizacion)
		{
			$cotizacion = cotizacion::obtener_info_cotizacion($idcotizacion);
			if($cotizacion === false)
				$cotizacion = array();
			return $cotizacion;
		}
		function obtener_solicitantes_de_cotizacion($idcotizacion)
		{
			$solicitantes = cotizacion::obtener_solicitantes_x_cotizacion($idcotizacion);
			if($solicitantes === false)
				$solicitantes = array();
			return $solicitantes;	
		}
		function obtener_publicacion_de_cotizacion($idcotizacion)
		{
			$publicacion = cotizacion::obtener_paquete_de_cotizacion($idcotizacion);
			if($publicacion === false)
				$publicacion = array();
			return $publicacion;
		}

		// PRECIOS DE SERVICIOS
		function obtener_ciudades_de_salida($idcotizacion)
		{
			$ciudades = cotizacion::obtener_ciudades_origen_cotizacion($idcotizacion);
			if($ciudades === false)
				$ciudades = array();
			return $ciudades;
		}

		function obtener_destinos_de_hotel($idciudad_origen)
		{
			$destinos = tarifa_hotel::obtener_tarifas_hotel_por_ciudad_origen($idciudad_origen);
			if($destinos === false)
				$destinos = array();
			return $destinos;
		}

		function obtener_hoteles_x_destino($idtarifa_hotel)
		{
			$hoteles = tarifa_hotel::obtener_hoteles_x_destino($idtarifa_hotel);
			if($hoteles === false)
				$hoteles = array();
			return $hoteles;
		}

		function obtener_otros_servicios_x_ciudad($idciudad_origen)
		{
			$otros_servicios = tarifa_otros::obtener_otros_servicios_por_ciudad($idciudad_origen);
			if($otros_servicios === false)
				$otros_servicios = array();
			return $otros_servicios;
		}

		function obtener_opcion_boletos_x_ciudad($idciudad_origen)
		{
			$opcion_boletos = boleto_cotizado::obtener_opciones_boletos_x_ciudad_origen($idciudad_origen);
			if($opcion_boletos === false)
				$opcion_boletos = array();
			return $opcion_boletos;
		}

		function obtener_boletos_x_opcion_aereo($idopcion_aereo)
		{
			$boletos = boleto_cotizado::obtener_boletos_x_opcion_boleto($idopcion_aereo);
			if($boletos === false)
				$boletos = array();
			return $boletos;
		}

		function obtener_itinerario_x_opcion_aereo($idopcion_aereo)
		{
			$itinerario = boleto_cotizado::obtener_itinerario_x_opcion_boleto($idopcion_aereo);
			if($itinerario === false)
				$itinerario = array();
			return $itinerario;
		}

		function obtener_inscripciones_evento_por_cotizacion($idcotizacion)
		{
			$inscripciones 	= inscripcion_evento::obtener_inscripciones_por_cotizacion($idcotizacion);
			if($inscripciones === false)
				$inscripciones = array();
			return $inscripciones;
		}

}

?>