<?php
/**
* 
*/
class c_buscar_evento
{
	
	private $array_palabras = array();
	function obtener_destinos()
	{
		$destinos = destino::obtener_datos_destino();
		for($p = 0;$p < count($destinos); $p++)
        {
        	$this->array_palabras[] = $destinos[$p]['nombre_destino'];
        } 
	}
	function obtener_temporadas()
	{
		$temporadas = temporada::obtener_datos_temporada();
        for($p = 0;$p < count($temporadas); $p++)
        { 
        	$this->array_palabras[] = $temporadas[$p]['nombre_temporada'];
        } 
	}
	function obtener_titulos_mas_genericos()
	{
		$eventos = evento::buscar_eventos('');
		for ($i=0; $i < count($eventos); $i++) 
		{ 
			if($eventos[$i]['nombre_evento'] != '')
				$this->array_palabras[] = $eventos[$i]['nombre_evento'];
			if($eventos[$i]['sigla_evento'] != '')
				$this->array_palabras[] = $eventos[$i]['sigla_evento'];
			if($eventos[$i]['sede'] != '')
				$this->array_palabras[] = $eventos[$i]['sede'];
		}
	}
	function obtener_especialidades()
	{
		$especialidades = especialidades::obtener_datos_especialidades();
        for($p = 0;$p < count($especialidades); $p++)
        { 
        	$this->array_palabras[] = $especialidades[$p]['nombre_especialidad'];
        } 
	}
	function obtener_alcances()
	{
		$alcances = alcance_evento::obtener_datos_alcance_evento();
		for ($i=0; $i < count($alcances); $i++) 
		{ 
			$this->array_palabras[] = $alcances[$i]['descripcion'];
		}
	}
	function obtener_tipos_de_evento()
	{
		$tipos = tipo_evento::obtener_datos_tipo_evento();
		for ($i=0; $i < count($tipos); $i++) 
		{ 
			$this->array_palabras[] = $tipos[$i]['descripcion'];
		}
	}
	function obtener_titulos()
	{
		$paquetes = publicacion::obtener_paquetes('E');
		for ($i=0; $i < count($paquetes); $i++) 
		{ 
			if($paquetes[$i]['titulo'] != '')
				$this->array_palabras[] = $paquetes[$i]['titulo'];
		}
	}
	
	function cargar_palabras_clave()
	{
		$this->obtener_destinos();
		$this->obtener_temporadas();
		$this->obtener_titulos_mas_genericos();
		$this->obtener_especialidades();
		$this->obtener_alcances();
		$this->obtener_tipos_de_evento();
		$this->obtener_titulos();

		$this->array_palabras = array_values(array_unique($this->array_palabras));
		
		usort($this->array_palabras, array('c_buscar_evento','comparar'));

		return $this->array_palabras;
	}
	function buscar_eventos_por_palabra_clave($palabra, $estado)
	{
		$tipo = 'E';

		$resultado = array();
		$segun_titulos 		= publicacion::obtener_paquete_por_palabra_clave_titulo($palabra, $estado, $tipo);
		if($segun_titulos !== false && count($segun_titulos) > 0)
			$resultado = $segun_titulos;
		$segun_tipo_evento 	= publicacion::obtener_evento_por_palabra_clave_tipo_evento($palabra, $estado, $tipo);
		if($segun_tipo_evento !== false && count($segun_tipo_evento) > 0 )
		{
			if(count($resultado) > 0)
				$resultado = array_merge($resultado, $segun_tipo_evento);
			else
				$resultado = $segun_tipo_evento;
		}
		$segun_temporada 	= publicacion::obtener_paquete_por_palabra_clave_temporada($palabra, $estado, $tipo);
		if($segun_temporada !== false && count($segun_temporada) > 0 )
		{
			if(count($resultado) > 0)
				$resultado = array_merge($resultado, $segun_temporada);
			else
				$resultado = $segun_temporada;
		}
		$segun_destino 		= publicacion::obtener_paquete_por_palabra_clave_destino($palabra, $estado, $tipo);
		if($segun_destino !== false && count($segun_destino) > 0 )
		{
			if(count($resultado) > 0)
				$resultado = array_merge($resultado, $segun_destino);
			else
				$resultado = $segun_destino;
		}
		$segun_alcance 		= publicacion::obtener_evento_por_palabra_clave_alcance($palabra, $estado, $tipo);
		if($segun_alcance !== false && count($segun_alcance) > 0 )
		{
			if(count($resultado) > 0)
				$resultado = array_merge($resultado, $segun_alcance);
			else
				$resultado = $segun_alcance;
		}
		$segun_evento 		= publicacion::obtener_evento_por_palabra_clave_evento($palabra, $estado, $tipo);
		if($segun_evento !== false && count($segun_evento) > 0 )
		{
			if(count($resultado) > 0)
				$resultado = array_merge($resultado, $segun_evento);
			else
				$resultado = $segun_evento;
		}
		/*$segun_especialidad = evento::obtener_evento_por_palabra_clave_especialidad($palabra, $estado);
		if($segun_especialidad !== false && count($segun_especialidad) > 0 )
		{
			if(count($resultado) > 0)
				$resultado = array_merge($resultado, $segun_especialidad);
			else
				$resultado = $segun_especialidad;
		}*/

		$result = array_map("unserialize", array_unique(array_map("serialize", $resultado)));

		$result = $this->retornar_array_mas_fecha_salida_numerica($result);

		usort($result, array('c_buscar_evento','comparar_fecha'));

		return $result;
	}
	function obtener_cantidad_de_cotizaciones($idpublicacion)
	{
		$cantidad_cot = publicacion::obtener_cantidad_de_cotizaciones($idpublicacion);
		if($cantidad_cot !== false)
		{
			if(count($cantidad_cot) > 0)
				return $cantidad_cot[0]['cantidad'];
			return 0;
		}
		return 0;
	}
	function obtener_cantidad_de_envios_masivos($idpublicacion)
	{
		$cantidad_env = publicacion::obtener_cantidad_de_envios_masivos($idpublicacion);
		if($cantidad_env !== false)
		{
			if(count($cantidad_env) > 0)
				return $cantidad_env[0]['cantidad'];
			return 0;
		}
		return 0;
	}
	static function comparar($a, $b) 
	{
	    if (strlen($a) == strlen($b)) {
	        return 0;
	    }
	    return (strlen($a) < strlen($b)) ? -1 : 1;
	}
	static function comparar_fecha($a, $b) 
	{
		$fecha_a 	= $a['salida_num'];
		$fecha_b 	= $b['salida_num'];
	    if ($fecha_a == $fecha_b) {
	        return 0;
	    }
	    return ($fecha_a < $fecha_b) ? -1 : 1;
	}
	static function obtener_mes_numerico($mes)
	{
		switch ($mes) {
			case 'ENE': return '01';
			case 'FEB': return '02';
			case 'MAR': return '03';
			case 'ABR': return '04';
			case 'MAY': return '05';
			case 'JUN': return '06';
			case 'JUL': return '07';
			case 'AGO': return '08';
			case 'SEP': return '09';
			case 'OCT': return '10';
			case 'NOV': return '11';
			case 'DIC': return '12';
			default: return '00';
		}
	}
	function retornar_array_mas_fecha_salida_numerica($array_in)
	{
		$array = $array_in;
		for ($i=0; $i < count($array); $i++) 
		{ 
			$salida = $array[$i]['salida'];
			$a_salida = explode('/', $salida);
			$dia = $a_salida[0];
			$mes = $this->obtener_mes_numerico(strtoupper($a_salida[1]));
			$anio = $a_salida[2];

			$array[$i]['salida_num'] = strtotime($anio . '-' . $mes . '-' . $dia);
			if($array[$i]['salida_num'] == '')
				$array[$i]['salida_num'] = 0;
		}
		return $array;
	}
}
?>