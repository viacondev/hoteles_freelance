<?php
/**
* 
*/
class c_buscar_cotizaciones_paquete
{

	/*
	* Los datos pasados por parámetro provienen de un POST
	*/
	function buscar_cotizaciones($datos)
	{
		$paquete_cotizado 	= new paquete_cotizado;

		$estado 		= $datos['estado_cotizacion'];
		$tipo_cliente   = $datos['tipo_cliente'];
		$nombre 		= $datos['nombre_cliente'];
		$apellidos 		= $datos['apellido_cliente'];
		$paquete 		= $datos['paquete'];
		$fecha_desde 	= $datos['fecha_desde'];
		$fecha_hasta 	= $datos['fecha_hasta'];
		
		$paquetes 	= $paquete_cotizado->buscar_cotizacion_paquete_por_criterios($estado, $tipo_cliente, $nombre, $apellidos, $paquete, $fecha_desde, $fecha_hasta);

		if($paquetes !== false)
		{
			if(count($paquetes) > 0)
			{
				return $paquetes;
			}
			return false;
		}
		return false;
	}
	private $array_palabras = array();
	function obtener_destinos()
	{
		$destinos = destino::obtener_datos_destino();
		for($p = 0;$p < count($destinos); $p++)
        {
        	$this->array_palabras[] = $destinos[$p]['nombre_destino'];
        } 
	}
	function obtener_temporadas()
	{
		$temporadas = temporada::obtener_datos_temporada();
        for($p = 0;$p < count($temporadas); $p++)
        { 
        	$this->array_palabras[] = $temporadas[$p]['nombre_temporada'];
        } 
	}
	function obtener_titulos_paquetes()
	{
		$paquetes = publicacion::obtener_paquetes('P');
		for ($i=0; $i < count($paquetes); $i++) 
		{ 
			if($paquetes[$i]['titulo'] != '')
				$this->array_palabras[] = $paquetes[$i]['titulo'];
		}
	}
	function obtener_titulos_evento()
	{
		$paquetes = publicacion::obtener_paquetes('E');
		for ($i=0; $i < count($paquetes); $i++) 
		{ 
			if($paquetes[$i]['titulo'] != '')
				$this->array_palabras[] = $paquetes[$i]['titulo'];
		}
	}
	function obtener_tipos_de_paquete()
	{
		$tipos = tipo_paquete::obtener_datos_tipo_paquete();
		for ($i=0; $i < count($tipos); $i++) 
		{ 
			$this->array_palabras[] = $tipos[$i]['descripcion'];
		}
	}
	function obtener_titulos_mas_genericos()
	{
		$eventos = evento::buscar_eventos('');
		for ($i=0; $i < count($eventos); $i++) 
		{ 
			if($eventos[$i]['nombre_evento'] != '')
				$this->array_palabras[] = $eventos[$i]['nombre_evento'];
			if($eventos[$i]['sigla_evento'] != '')
				$this->array_palabras[] = $eventos[$i]['sigla_evento'];
			if($eventos[$i]['sede'] != '')
				$this->array_palabras[] = $eventos[$i]['sede'];
		}
	}
	function obtener_especialidades()
	{
		$especialidades = especialidades::obtener_datos_especialidades();
        for($p = 0;$p < count($especialidades); $p++)
        { 
        	$this->array_palabras[] = $especialidades[$p]['nombre_especialidad'];
        } 
	}
	function obtener_alcances()
	{
		$alcances = alcance_evento::obtener_datos_alcance_evento();
		for ($i=0; $i < count($alcances); $i++) 
		{ 
			$this->array_palabras[] = $alcances[$i]['descripcion'];
		}
	}
	function obtener_tipos_de_evento()
	{
		$tipos = tipo_evento::obtener_datos_tipo_evento();
		for ($i=0; $i < count($tipos); $i++) 
		{ 
			$this->array_palabras[] = $tipos[$i]['descripcion'];
		}
	}
	function obtener_titulos_cotizacion()
	{
		$titulos = cotizacion::obtener_titulos_cotizacion();
		for ($i=0; $i < count($titulos); $i++) 
		{ 
			$this->array_palabras[] = $titulos[$i]['titulo'];
		}
	}
	function cargar_palabras_clave()
	{
		/*$this->obtener_destinos();
		$this->obtener_temporadas();
		$this->obtener_titulos_paquetes();
		$this->obtener_titulos_evento();
		$this->obtener_tipos_de_paquete();
		$this->obtener_titulos_mas_genericos();
		$this->obtener_especialidades();
		$this->obtener_alcances();
		$this->obtener_tipos_de_evento();
		$this->array_palabras = array_values(array_unique($this->array_palabras));*/
		$this->obtener_titulos_cotizacion();
		
		usort($this->array_palabras, array('c_buscar_cotizaciones_paquete','comparar'));
		return $this->array_palabras;
	}
	static function comparar($a, $b) 
	{
	    if (strlen($a) == strlen($b)) {
	        return 0;
	    }
	    return (strlen($a) < strlen($b)) ? -1 : 1;
	}
	function buscar_paquetes_cotizados_por_palabra_clave($palabra, $nombre, $apellido, $estado, $fecha_d, $fecha_h, $nombre_pax, $apellido_pax)
	{
		if($fecha_d != '')
		{
			$a_f_d = explode('/', $fecha_d);
			$fecha_desde = $a_f_d[2] . '-' . $a_f_d[1] . '-' . $a_f_d[0];
		}
		if($fecha_h != '')
		{
			$a_f_h = explode('/', $fecha_h);
			$fecha_hasta = $a_f_h[2] . '-' . $a_f_h[1] . '-' . $a_f_h[0];
		}
		if($estado == 'T')
			$estado = '';

		//$resultado = array();
		//$segun_titulos 		= paquete_cotizado::obtener_paquete_cotizado_por_palabra_clave_titulo($palabra, $nombre, $apellido, $estado, $fecha_desde, $fecha_hasta);
		//$segun_titulos 		= cotizacion::obtener_cotizacion_por_palabra_clave_titulo($palabra, $nombre, $apellido, $estado, $fecha_desde, $fecha_hasta);
		$segun_titulos 		= cotizacion::obtener_cotizacion_por_palabra_clave_titulo_cliente_pax($palabra, $nombre, $apellido, $estado, $fecha_desde, $fecha_hasta, $nombre_pax, $apellido_pax);
		/*if($segun_titulos !== false && count($segun_titulos) > 0)
			$resultado = $segun_titulos;
		$segun_tipo_paquete 	= paquete_cotizado::obtener_paquete_cotizado_por_palabra_clave_tipo_paquete($palabra, $nombre, $apellido, $estado, $fecha_desde, $fecha_hasta);
		if($segun_tipo_paquete !== false && count($segun_tipo_paquete) > 0 )
		{
			if(count($resultado) > 0)
				$resultado = array_merge($resultado, $segun_tipo_paquete);
			else
				$resultado = $segun_tipo_paquete;
		}
		$segun_temporada 	= paquete_cotizado::obtener_paquete_cotizado_por_palabra_clave_temporada($palabra, $nombre, $apellido, $estado, $fecha_desde, $fecha_hasta);
		if($segun_temporada !== false && count($segun_temporada) > 0 )
		{
			if(count($resultado) > 0)
				$resultado = array_merge($resultado, $segun_temporada);
			else
				$resultado = $segun_temporada;
		}
		$segun_destino 		= paquete_cotizado::obtener_paquete_cotizado_por_palabra_clave_destino($palabra, $nombre, $apellido, $estado, $fecha_desde, $fecha_hasta);
		if($segun_destino !== false && count($segun_destino) > 0 )
		{
			if(count($resultado) > 0)
				$resultado = array_merge($resultado, $segun_destino);
			else
				$resultado = $segun_destino;
		}*/
		//$result = array_map("unserialize", array_unique(array_map("serialize", $resultado)));

		//return $result;
		return $segun_titulos;
	}
	function obtener_cantidad_paxs($idcotizacion)
	{
		$cantidad_paxs = cotizacion::obtener_cantidad_paxs($idcotizacion);
		if($cantidad_paxs === false)
			return 0;
		return $cantidad_paxs[0][0];
	}
}
?>