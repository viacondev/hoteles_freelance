<?php
/**
* 
*/
class c_buscar_paquete
{
	
	private $array_palabras = array();
	function obtener_destinos()
	{
		$destinos = destino::obtener_datos_destino();
		for($p = 0;$p < count($destinos); $p++)
        {
        	$this->array_palabras[] = $destinos[$p]['nombre_destino'];
        } 
	}
	function obtener_temporadas()
	{
		$temporadas = temporada::obtener_datos_temporada();
        for($p = 0;$p < count($temporadas); $p++)
        { 
        	$this->array_palabras[] = $temporadas[$p]['nombre_temporada'];
        } 
	}
	function obtener_titulos()
	{
		$paquetes = publicacion::obtener_paquetes('P');
		for ($i=0; $i < count($paquetes); $i++) 
		{ 
			if($paquetes[$i]['titulo'] != '')
				$this->array_palabras[] = $paquetes[$i]['titulo'];
		}
	}
	function obtener_tipos_de_paquete()
	{
		$tipos = tipo_paquete::obtener_datos_tipo_paquete();
		for ($i=0; $i < count($tipos); $i++) 
		{ 
			$this->array_palabras[] = $tipos[$i]['descripcion'];
		}
	}
	function cargar_palabras_clave()
	{
		$this->obtener_destinos();
		$this->obtener_temporadas();
		$this->obtener_titulos();
		$this->obtener_tipos_de_paquete();
		
		$this->array_palabras = array_values(array_unique($this->array_palabras));
		
		usort($this->array_palabras, array('c_buscar_paquete','comparar'));
        

		return $this->array_palabras;
	}
	function buscar_paquetes_por_palabra_clave($palabra, $estado)
	{
		$tipo = 'P';

		$resultado = array();
		$segun_titulos 		= publicacion::obtener_paquete_por_palabra_clave_titulo($palabra, $estado, $tipo);
		if($segun_titulos !== false && count($segun_titulos) > 0)
			$resultado = $segun_titulos;
		$segun_tipo_paquete 	= publicacion::obtener_paquete_por_palabra_clave_tipo_paquete($palabra, $estado, $tipo);
		if($segun_tipo_paquete !== false && count($segun_tipo_paquete) > 0 )
		{
			if(count($resultado) > 0)
				$resultado = array_merge($resultado, $segun_tipo_paquete);
			else
				$resultado = $segun_tipo_paquete;
		}
		$segun_temporada 	= publicacion::obtener_paquete_por_palabra_clave_temporada($palabra, $estado, $tipo);
		if($segun_temporada !== false && count($segun_temporada) > 0 )
		{
			if(count($resultado) > 0)
				$resultado = array_merge($resultado, $segun_temporada);
			else
				$resultado = $segun_temporada;
		}
		$segun_destino 		= publicacion::obtener_paquete_por_palabra_clave_destino($palabra, $estado, $tipo);
		if($segun_destino !== false && count($segun_destino) > 0 )
		{
			if(count($resultado) > 0)
				$resultado = array_merge($resultado, $segun_destino);
			else
				$resultado = $segun_destino;
		}

		$result = array_map("unserialize", array_unique(array_map("serialize", $resultado)));

		$result = $this->retornar_array_mas_fecha_salida_numerica($result);

		usort($result, array('c_buscar_paquete','comparar_fecha'));

		return $result;
	}
	function buscar_paquetes_por_palabra_clave_vista_previa($palabra, $estado)
	{
		$resultado = array();
		$segun_titulos 		= paquete::obtener_paquete_por_palabra_clave_titulo_vista_previa($palabra, $estado);
		if($segun_titulos !== false && count($segun_titulos) > 0)
			$resultado = $segun_titulos;
		$segun_tipo_paquete 	= paquete::obtener_paquete_por_palabra_clave_tipo_paquete_vista_previa($palabra, $estado);
		if($segun_tipo_paquete !== false && count($segun_tipo_paquete) > 0 )
		{
			if(count($resultado) > 0)
				$resultado = array_merge($resultado, $segun_tipo_paquete);
			else
				$resultado = $segun_tipo_paquete;
		}
		$segun_temporada 	= paquete::obtener_paquete_por_palabra_clave_temporada_vista_previa($palabra, $estado);
		if($segun_temporada !== false && count($segun_temporada) > 0 )
		{
			if(count($resultado) > 0)
				$resultado = array_merge($resultado, $segun_temporada);
			else
				$resultado = $segun_temporada;
		}
		$segun_destino 		= paquete::obtener_paquete_por_palabra_clave_destino_vista_previa($palabra, $estado);
		if($segun_destino !== false && count($segun_destino) > 0 )
		{
			if(count($resultado) > 0)
				$resultado = array_merge($resultado, $segun_destino);
			else
				$resultado = $segun_destino;
		}

		$result = array_map("unserialize", array_unique(array_map("serialize", $resultado)));

		$result = $this->retornar_array_mas_fecha_salida_numerica($result);

		usort($result, array('c_buscar_paquete','comparar_fecha'));

		return $result;
	}
	function obtener_cantidad_de_cotizaciones($idpublicacion)
	{
		$cantidad_cot = publicacion::obtener_cantidad_de_cotizaciones($idpublicacion);
		if($cantidad_cot !== false)
		{
			if(count($cantidad_cot) > 0)
				return $cantidad_cot[0]['cantidad'];
			return 0;
		}
		return 0;
	}
	function obtener_cantidad_de_envios_masivos($idpublicacion)
	{
		$cantidad_env = publicacion::obtener_cantidad_de_envios_masivos($idpublicacion);
		if($cantidad_env !== false)
		{
			if(count($cantidad_env) > 0)
				return $cantidad_env[0]['cantidad'];
			return 0;
		}
		return 0;
	}
	static function comparar($a, $b) 
	{
	    if (strlen($a) == strlen($b)) {
	        return 0;
	    }
	    return (strlen($a) < strlen($b)) ? -1 : 1;
	}
	static function comparar_fecha($a, $b) 
	{
		$fecha_a 	= $a['salida_num'];
		$fecha_b 	= $b['salida_num'];
	    if ($fecha_a == $fecha_b) {
	        return 0;
	    }
	    return ($fecha_a < $fecha_b) ? -1 : 1;
	}
	static function obtener_mes_numerico($mes)
	{
		switch ($mes) {
			case 'ENE': return '01';
			case 'FEB': return '02';
			case 'MAR': return '03';
			case 'ABR': return '04';
			case 'MAY': return '05';
			case 'JUN': return '06';
			case 'JUL': return '07';
			case 'AGO': return '08';
			case 'SEP': return '09';
			case 'OCT': return '10';
			case 'NOV': return '11';
			case 'DIC': return '12';
			default: return '00';
		}
	}
	function retornar_array_mas_fecha_salida_numerica($array_in)
	{
		$array = $array_in;
		for ($i=0; $i < count($array); $i++) 
		{ 
			$salida = $array[$i]['salida'];
			$a_salida = explode('/', $salida);
			$dia = $a_salida[0];
			$mes = $this->obtener_mes_numerico(strtoupper($a_salida[1]));
			$anio = $a_salida[2];

			$array[$i]['salida_num'] = strtotime($anio . '-' . $mes . '-' . $dia);
			if($array[$i]['salida_num'] == '')
				$array[$i]['salida_num'] = 0;
		}
		return $array;
	}

	function obtener_paquetes_publicados_zion($titulo,$destino,$temporada,$categoria,$pag)
	{
		$max 	= 10;
		$ti 	= $titulo;
		$de 	= $destino;
		$te 	= $temporada;
		$ca 	= $categoria;

		if($ti == "" && $de == 	0 && $te == 0 && $ca == 0){ return publicacion::obtener_paquetes_x_00_00_00_00($pag,$max);					}//0000
		if($ti == "" && $de == 	0 && $te == 0 && $ca > 	0){ return publicacion::obtener_paquetes_x_00_00_00_ca($ca,$pag,$max);				}//0001
		if($ti == "" && $de == 	0 && $te > 	0 && $ca == 0){ return publicacion::obtener_paquetes_x_00_00_te_00($te,$pag,$max);				}//0010
		if($ti == "" && $de == 	0 && $te > 	0 && $ca > 	0){ return publicacion::obtener_paquetes_x_00_00_te_ca($te,$ca,$pag,$max);			}//0011
		if($ti == "" && $de > 	0 && $te == 0 && $ca == 0){ return publicacion::obtener_paquetes_x_00_de_00_00($de,$pag,$max);				}//0100
		if($ti == "" && $de > 	0 && $te == 0 && $ca > 	0){ return publicacion::obtener_paquetes_x_00_de_00_ca($de,$ca,$pag,$max);			}//0101
		if($ti == "" && $de > 	0 && $te > 	0 && $ca == 0){ return publicacion::obtener_paquetes_x_00_de_te_00($de,$te,$pag,$max);			}//0110
		if($ti == "" && $de > 	0 && $te > 	0 && $ca > 	0){ return publicacion::obtener_paquetes_x_00_de_te_ca($de,$te,$ca,$pag,$max);		}//0111
		if($ti != "" && $de == 	0 && $te == 0 && $ca == 0){ return publicacion::obtener_paquetes_x_ti_00_00_00($ti,$pag,$max);				}//1000
		if($ti != "" && $de == 	0 && $te == 0 && $ca > 	0){ return publicacion::obtener_paquetes_x_ti_00_00_ca($ti,$ca,$pag,$max);			}//1001
		if($ti != "" && $de == 	0 && $te > 	0 && $ca == 0){ return publicacion::obtener_paquetes_x_ti_00_te_00($ti,$te,$pag,$max);		  	}//1010
		if($ti != "" && $de == 	0 && $te > 	0 && $ca > 	0){ return publicacion::obtener_paquetes_x_ti_00_te_ca($ti,$te,$ca,$pag,$max);    	}//1011
		if($ti != "" && $de > 	0 && $te == 0 && $ca == 0){ return publicacion::obtener_paquetes_x_ti_de_00_00($ti,$de,$pag,$max);		  	}//1100
		if($ti != "" && $de > 	0 && $te == 0 && $ca > 	0){ return publicacion::obtener_paquetes_x_ti_de_00_ca($ti,$de,$ca,$pag,$max);    	}//1101
		if($ti != "" && $de > 	0 && $te > 	0 && $ca == 0){ return publicacion::obtener_paquetes_x_ti_de_te_00($ti,$de,$te,$pag,$max);	  	}//1110
		if($ti != "" && $de > 	0 && $te > 	0 && $ca > 	0){ return publicacion::obtener_paquetes_x_ti_de_te_ca($ti,$de,$te,$ca,$pag,$max);	}//1111
	}

	function traer_publicacion_activo_x_destino()
	{
		$resp 	= publicacion::traer_publicacion_activo_x_destino();
		$vec 	= array();

		for ($i = 0; $i < count($resp); $i++) 
		{ 
		 	$vec[$resp[$i]['iddestino']]['nombre_destino'] 	= $resp[$i]['nombre_destino'];
		 	$vec[$resp[$i]['iddestino']]['publicacion'][] 	= $resp[$i]['idpublicacion']; 
		} 

		return $vec;
	}

	function traer_publicacion_activo_x_temporada()
	{
		$resp 	= publicacion::traer_publicacion_activo_x_temporada();
		$vec 	= array();

		for ($i = 0; $i < count($resp); $i++) 
		{ 
		 	$vec[$resp[$i]['idtemporada']]['nombre_temporada'] 	= $resp[$i]['nombre_temporada'];
		 	$vec[$resp[$i]['idtemporada']]['publicacion'][] 	= $resp[$i]['idpublicacion']; 
		} 

		return $vec;
	}

	function traer_publicacion_activo_x_categoria()
	{
		$resp 	= publicacion::traer_publicacion_activo_x_categoria();
		$vec 	= array();

		for ($i = 0; $i < count($resp); $i++) 
		{ 
		 	$vec[$resp[$i]['idtipo_paquete']]['descripcion'] 	= $resp[$i]['descripcion'];
		 	$vec[$resp[$i]['idtipo_paquete']]['publicacion'][] 	= $resp[$i]['idpublicacion']; 
		} 

		return $vec;
	}
}
?>