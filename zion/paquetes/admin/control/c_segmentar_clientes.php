<?php
/**
* 
*/
class c_segmentar_clientes
{
	
	function obtener_especialidades()
	{
		$especialidades = clientes::obtener_especialidades();
		if($especialidades !== false)
		{
			if(count($especialidades) > 0)
			{
				return $especialidades;
			}
			return false;
		}
		return false;
	}
	function obtener_rubros()
	{
		$rubros = clientes::obtener_rubros();
		if($rubros !== false)
		{
			if(count($rubros) > 0)
			{
				return $rubros;
			}
			return false;
		}
		return false;
	}
	function obtener_profesiones()
	{
		$profesiones = clientes::obtener_profesiones();
		if($profesiones !== false)
		{
			if(count($profesiones) > 0)
			{
				return $profesiones;
			}
			return false;
		}
		return false;
	}
	function obtener_todos_clientes()
	{
		$t_clientes		= clientes::buscar_todos_clientes();
		return $t_clientes;
	}
	function segmentar_clientes($rbr, $pro, $esp)
	{
		$resultado = array();
		if($rbr != 0)
		{
			$segun_rubro 		= clientes::buscar_clientes_por_rubro($rbr);
			if($segun_rubro !== false && count($segun_rubro) > 0)
				$resultado = $segun_rubro;	
		}
		if($pro != 0 && $esp == 0)
		{
			$segun_profesion 	= clientes::buscar_clientes_por_profesion($pro);
			if($segun_profesion !== false && count($segun_profesion) > 0 )
			{
				if(count($resultado) > 0)
					$resultado = array_merge($resultado, $segun_profesion);
				else
					$resultado = $segun_profesion;
			}	
		}
		if($esp != 0)
		{
			$segun_especialidad = clientes::buscar_clientes_por_especialidad($esp);
			if($segun_especialidad !== false && count($segun_especialidad) > 0 )
			{
				if(count($resultado) > 0)
					$resultado = array_merge($resultado, $segun_especialidad);
				else
					$resultado = $segun_especialidad;
			}	
		}
		/*if($_POST['pre'] != 0)
		{
			$segun_preferencia 	= clientes::buscar_clientes_por_preferencia($_POST['pre']);
			if($segun_preferencia !== false && count($segun_preferencia) > 0 )
			{
				if(count($resultado) > 0)
					$resultado = array_merge($resultado, $segun_preferencia);
				else
					$resultado = $segun_preferencia;
			}	
		}*/

		$result = array_map("unserialize", array_unique(array_map("serialize", $resultado)));
		return $result;
	}
	function obtener_especialidad($id)
	{
		$especialidad = clientes::obtener_especialidad($id);
		if($especialidad !== false)
		{
			if(count($especialidad) > 0)
			{
				return $especialidad[0];
			}
			return false;
		}
		return false;
	}
	function obtener_rubro($id)
	{
		$rubro = clientes::obtener_rubro($id);
		if($rubro !== false)
		{
			if(count($rubro) > 0)
			{
				return $rubro[0];
			}
			return false;
		}
		return false;
	}
	function obtener_profesion($id)
	{
		$profesion = clientes::obtener_profesion($id);
		if($profesion !== false)
		{
			if(count($profesion) > 0)
			{
				return $profesion[0];
			}
			return false;
		}
		return false;
	}
	function buscar_cliente($idclientes)
	{
		$clientes = clientes::buscar_cliente($idclientes);
		if($clientes !== false)
		{
			if(count($clientes) > 0)
				return $clientes[0];
			else
				return false;
		}
		return false;
	}
	function obtener_titulo_evento($idevento)
	{
		$evento = evento::obtener_titulo_evento($idevento);
		if($evento !== false)
		{
			if(count($evento) > 0)
				return $evento[0];
			return false;
		}
		return false;
	}
	function obtener_correos_cliente($idclientes)
	{
		$correos = clientes::obtener_correos_de_cliente($idclientes);
		if($correos !== false)
		{
			if(count($correos) > 0)
				return $correos;
			return false;
		}
		return false;
	}
}
?>