<?php
    include('../BD/controladoraBD.php');
    include('../entidad/archivo_adjunto.php');
    //Como no sabemos cuantos archivos van a llegar, iteramos la variable $_FILES
    $idarchivo  = $_POST['archivo'];
    $idpublicacion = $_POST['publicacion'];
        
    $eliminar_archivo = archivo_adjunto::desvincular_archivo($idarchivo, $idpublicacion);
    
    if($eliminar_archivo)
        echo "CORRECTO";
    else
        echo "ERROR";
?>