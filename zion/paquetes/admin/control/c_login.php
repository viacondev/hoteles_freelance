<?php

class c_login {

    function verificar_credenciales($usuario, $passwd) {
        $usuarios = new usuarios;
        $usuarios = $usuarios->existe_usuario($usuario, $passwd);
        return $usuarios[0];
    }

    function verificar_session() {
        if (!isset($_SESSION['idusuarios'])) {
            header('location: login.php?return=' . urlencode(c_login::selfURL()));
        }
    }

    function iniciar_session($usuario, $passwd, $url) {
        $valido = false;
        if (($usuario != "") && ($passwd != "")) {
            $datos_usuario = c_login::verificar_credenciales($usuario, md5($passwd));
            
            if ($datos_usuario != 0) 
            {
                $admin = usuarios::obtener_admin($datos_usuario['id']);

                if(count($admin) > 0)
                {
                    $valido                             = true;
                    $_SESSION['idusuarios']             = $datos_usuario['id'];
                    $_SESSION['nombre_usuario']         = $datos_usuario['username'];
                    $_SESSION['clave_usuario_md5']      = $datos_usuario['password'];
                    $_SESSION['nivel']                  = "1";
                    $_SESSION['nombre_usuario_sesion']  = $datos_usuario['name'] . ' ' . $datos_usuario['lastname'];
                }
            }
        } 

        if ($valido) {
            if (!$url || $url == '') {
                $url = 'index.php';
            }
            header('location:' . $url);
        }
        else {
            session_unset();
            session_destroy();
            return false;
        }
    }

    /**
     * Obtiene la URL actual, sirve para redireccionar a la URL en la que el usuario estaba
     * antes de que la sesion se pierda.
     */
    function selfURL() {
        $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
        $protocol = c_login::strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/") . $s;
        $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":" . $_SERVER["SERVER_PORT"]);
        return $protocol . "://" . $_SERVER['SERVER_NAME'] . $port . $_SERVER['REQUEST_URI'];
    }

    function strleft($s1, $s2) {
        return substr($s1, 0, strpos($s1, $s2));
    }
}
?>