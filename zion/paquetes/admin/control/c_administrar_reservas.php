<?php

class c_administrar_reservas {
    function enviar_reservas($pagina) {
        $online_reserva = new online_reserva;
        $max = 60;
        $pagina = ($pagina - 1) * $max;
        return $online_reserva->enviar_reservas($pagina, $max);
    }

    function enviar_clientes_reserva($pnr) {
        $online_reserva = new online_reserva;
        return $online_reserva->enviar_datos_clientes_reserva($pnr);
    }

}

?>