<?php

/**
* 
*/
class c_crear_cotizacion_evento
{
	
	function crear_cotizacion_desde_evento($idevento, $idclientes, $observaciones, $idusuarios, $idsolicitante)
	{
		$evento 		= new evento;
		$cont_evento 	= $evento->obtener_contenido_evento($idevento);
		if($cont_evento!==false)
		{
			if(count($cont_evento)>0)
			{
				$evento_actual 		= $cont_evento[0];
				$evento_cotizado 	= new evento_cotizado;
				$solicitante 		= 0;
				if($idsolicitante != "")
				{
					$solicitante 	= $idsolicitante;
				}
				if($evento_actual['solo_imagen'] == 'N')
				{
					$paquete_incluye 	= $evento_actual['paquete_incluye'];
					$importante 		= $evento_actual['importante'];

					$r_cotizacion 		= $evento_cotizado->registrar_evento_cotizado($idevento, $paquete_incluye, $importante, $observaciones, $idclientes, $idusuarios, $solicitante);
					return $r_cotizacion;
				}
				else
				{
					$r_cotizacion 		= $evento_cotizado->registrar_evento_cotizado($idevento, "", "",$observaciones, $idclientes, $idusuarios, $solicitante);
					return $r_cotizacion;
				}
			}
			return false;
		}
		return false;
	}

	function obtener_cotizacion_evento($idevento_cotizado)
	{
		$evento_cotizado = new evento_cotizado;
		$cotizaciones = $evento_cotizado->obtener_evento_cotizado($idevento_cotizado);
		if($cotizaciones!==false)
		{
			if(count($cotizaciones)>0)
			{
				return $cotizaciones[0];
			}
			return false;
		}
		return false;
	}

	function obtener_datos_cliente($idclientes)
	{
		$clientes = new clientes;
		$cliente = $clientes->enviar_datos_basicos_cliente($idclientes);
		if($cliente!==false)
		{
			if(count($cliente)>0)
			{
				return $cliente[0];
			}
			return false;
		}
		return false;
	}

	function obtener_datos_usuario($idusuarios)
	{
		$usuarios 	= new usuarios;
		$usuario  	= $usuarios->obtener_datos_usuario($idusuarios);

		if($usuario !== false)
		{
			if(count($usuario)>0)
			{
				return $usuario[0];
			}
			return false;
		}
		return false;
	}

	/*
	* LOS BOLETOS E ITINERARIO ESTAN AGRUPADOS POR OPCIONES OFRECIDAS AL CLIENTE
	*/

	function obtener_opciones_boletos($idevento, $type)
	{
		$boleto_cotizado = new boleto_cotizado;
		$boletos = $boleto_cotizado->obtener_opciones_boletos($idevento, $type);

		if($boletos !== false)
		{
			return $boletos;
		}
		$boletos 	= array();
		return $boletos;	
	}
	function obtener_boletos_de_opcion($idopcion, $type)
	{
		$boleto_cotizado = new boleto_cotizado;
		$boletos = $boleto_cotizado->obtener_boletos_por_opcion($idopcion, $type);

		if($boletos !== false)
		{
			return $boletos;
		}
		$boletos 	= array();
		return $boletos;
	}
	function obtener_segmentos_de_opcion($idopcion, $type)
	{
		$segmento_publicado = new segmento_publicado;
		$segmentos = $segmento_publicado->obtener_segmentos_de_opcion($idopcion, $type);

		if($segmentos !== false)
		{
			return $segmentos;
		}
		$segmentos = array();
		return $segmentos;
	}

	/***/

	/*
	* GESTION DE PRECIOS
	*/

	function obtener_tarifas_hotel_por_evento($idevento, $type)
	{
		$tarifa_hotel_evento = new tarifa_hotel;
		$tarifas_hotel = $tarifa_hotel_evento->obtener_tarifas($idevento, $type);

		if($tarifas_hotel !== false)
		{
			if(count($tarifas_hotel) > 0)
			{
				return $tarifas_hotel;
			}
			return false;
		}
		return false;
	}

	function obtener_items_de_tarifa($idtarifa_hotel_evento, $type)
	{
		$item_hotel_evento 	= new tarifa_hotel;
		$items 				= $item_hotel_evento->obtener_items_por_tarifa_hotel($idtarifa_hotel_evento, $type);

		if($items !== false)
		{
			if(count($items) > 0)
			{
				return $items;
			}
			return false;
		}
		return false;
	}

	function obtener_paxs_del_hotel($idtarifa)
	{
		$hotel_cotizado = new hotel_cotizado;
		$nombres = $hotel_cotizado->obtener_nombres_pasajeros_por_hotel($idtarifa, 'evento');

		if($nombres === false)
			$nombres = array();
		return $nombres;
	}

	function obtener_otras_tarifas($idevento, $type)
	{
		$tarifa_otros = new tarifa_otros;
		$tarifas = $tarifa_otros->obtener_tarifa_otros($idevento, $type);

		if($tarifas !== false)
		{
			if(count($tarifas) > 0)
			{
				return $tarifas;
			}
			return false;
		}
		return false;
	}

	function obtener_paxs_del_servicio($idtarifa)
	{
		$otros_cotizado = new otros_cotizado;
		$nombres = $otros_cotizado->obtener_nombres_pasajeros_por_servicio($idtarifa, 'evento');

		if($nombres === false)
			$nombres = array();
		return $nombres;
	}

	function obtener_nombre_servicio($idotro_servicio)
	{
		$otros_servicios = new pqt_otro_servicio;
		$servicio = $otros_servicios->obtener_otro_servicio($idotro_servicio);
		if($servicio !== false && count($servicio)>0)
			return strtoupper($servicio[0]['nombre_servicio']);
		return 'NO EXISTE SERVICIO';
	}

	function obtener_boletos_cotizados($idevento, $type)
	{
		$boleto_cotizado = new boleto_cotizado;
		$boletos = $boleto_cotizado->obtener_boletos_cotizados($idevento, $type);

		if($boletos !== false)
		{
			if(count($boletos) > 0)
			{
				return $boletos;
			}
			return false;
		}
		return false;
	}

	function obtener_paxs_del_boleto($idboleto)
	{
		$boleto_cotizado = new boleto_cotizado;
		$nombres = $boleto_cotizado->obtener_nombres_pasajeros_por_boleto($idboleto, 'evento');

		if($nombres === false)
			$nombres = array();
		return $nombres;
	}

	function obtener_segmentos_publicados($idevento, $type)
	{
		$segmento_publicado = new segmento_publicado;
		$segmentos = $segmento_publicado->obtener_segmentos($idevento, $type);

		if($segmentos !== false)
		{
			if(count($segmentos) > 0)
			{
				return $segmentos;
			}
			return false;
		}
		return false;
	}

	private $buffer_lineas_aereas = array();
	function obtener_linea_aerea($idlinea_aerea)
	{
		if(array_key_exists($idlinea_aerea, $this->buffer_lineas_aereas))
		{
			return $this->buffer_lineas_aereas[$idlinea_aerea];
		}
		else
		{
			$linea_aerea = lineas_aereas::obtener_linea_aerea_por_id($idlinea_aerea);
			if($linea_aerea !== false)
			{
				if(count($linea_aerea) > 0)
				{
					$aerolinea = explode(',',$linea_aerea[0]['nombre_linea_aerea']);
					$this->buffer_lineas_aereas[$idlinea_aerea] = strtoupper($aerolinea[0] . '(' . $linea_aerea[0]['codigo_iata_linea_aerea'] . ')');
					return $this->buffer_lineas_aereas[$idlinea_aerea];
				}
				$this->buffer_lineas_aereas[$idlinea_aerea] = 'N/I';
				return 'N/I';
			}
			$this->buffer_lineas_aereas[$idlinea_aerea] = 'N/I';
			return 'N/I';
		}
	}

	private $buffer_aeropuertos = array();
	function obtener_aeropuerto($codigo)
	{
		if(array_key_exists($codigo, $this->buffer_aeropuertos))
		{
			return $this->buffer_aeropuertos[$codigo];
		}
		else
		{
			$aeropuerto = aeropuerto::obtener_aeropuerto_por_codigo($codigo);
			
			if($aeropuerto !== false)
			{
				if(count($aeropuerto) > 0)
				{
					$ciudad = explode(',', $aeropuerto[0]['nombre_aeropuerto']);
					$this->buffer_aeropuertos[$codigo] = strtoupper($ciudad[0] . '(' . $aeropuerto[0]['cod_aeropuerto'] . ')');
					return $this->buffer_aeropuertos[$codigo];
				}
				$this->buffer_aeropuertos[$codigo] = 'N/I';
				return 'N/I';
			}
			$this->buffer_aeropuertos[$codigo] = 'N/I';
			return 'N/I';
		}
	}
	
	function obtener_inscripcion_cotizado($idcotizacion)
	{
		$inscripcion_cotizado = new inscripcion_cotizado;
		$inscripcion = $inscripcion_cotizado->obtener_inscripcion_cotizados($idcotizacion);

		if($inscripcion !== false)
		{
			if(count($inscripcion) > 0)
			{
				return $inscripcion;
			}
			return false;
		}
		return false;
	}
	
	function obtener_tipo_cambio($moneda) 
	{
        $evento_cotizado = new evento_cotizado;
        $idgestion = $this->obtener_idgestion();
        $tipo_cambio = 1;
       
	    if ($moneda != "BOB") 
		{
            $tb_tpcambio = $evento_cotizado->enviar_ultimo_tipo_cambio($idgestion, $moneda);
            $tipo_cambio = (float) $tb_tpcambio[0]['factor_tipo_cambio'];
        }
		
        return $tipo_cambio;
    }
	
	function obtener_idgestion() 
	{
		$evento_cotizado = new evento_cotizado;
		$anio = date("Y");
		$tb_gestion = $evento_cotizado->enviar_idgestion($anio);
		return $tb_gestion[0]['idgestion'];
	}
	
}

?>