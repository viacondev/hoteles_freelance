<?
	class c_pax_paquete
	{
		function obtener_pax_paquete_idcotizacion($idpaquete_cotizado)
		{
			$resp = pax_paquete::obtener_pax_paquete_idcotizacion($idpaquete_cotizado);
			return $resp;
		}
		
		function eliminar_pax_paquete_cotizado($idpax)
		{
			$class 	= "paquete";
			
			$resp 	= boleto_cotizado::obtenet_boleto_cotizado_idpax($idpax,$class);
			for($i = 0; $i < count($resp); $i++)
			{
				boleto_cotizado::eliminar_boleto_cotizado_class_cotizado_has_pax_class($resp[$i]['idboleto_cotizado_paquete_cotizado'],$idpax, $class);
			}
			
			
			$resp_hotel = hotel_cotizado::obtenet_hotel_cotizado_idpax($idpax,$class);
			for($i = 0; $i < count($resp_hotel); $i++)
			{
				hotel_cotizado::eliminar_hotel_cotizado_class_cotizado_has_pax_class($resp_hotel[$i]['iditem_hotel_paquete_cotizado'],$idpax, $class);
			}
			
			
			$resp_otros = otros_cotizado::obtenet_otros_cotizado_idpax($idpax,$class);
			for($i = 0; $i < count($resp_otros); $i++)
			{
				otros_cotizado::eliminar_otros_cotizado_class_cotizado_has_pax_class($resp_otros[$i]['idtarifa_otros_paquete_cotizado'],$idpax, $class);
			}
			
			boleto_cotizado::eliminar_pax_class($idpax, $class);
		}
	}
?>