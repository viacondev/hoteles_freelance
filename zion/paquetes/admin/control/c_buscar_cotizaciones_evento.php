<?php
/**
* 
*/
class c_buscar_cotizaciones_evento
{

	/*
	* Los datos pasados por parámetro provienen de un POST
	*/
	function buscar_cotizaciones($datos)
	{
		$evento_cotizado 	= new evento_cotizado;

		$estado 		= $datos['estado_cotizacion'];
		$tipo_cliente   = $datos['tipo_cliente'];
		$nombre 		= $datos['nombre_cliente'];
		$apellidos 		= $datos['apellido_cliente'];
		$evento 		= $datos['evento'];
		$fecha_desde 	= $datos['fecha_desde'];
		$fecha_hasta 	= $datos['fecha_hasta'];
		
		$eventos 	= $evento_cotizado->buscar_cotizacion_evento_por_criterios($estado, $tipo_cliente, $nombre, $apellidos, $evento, $fecha_desde, $fecha_hasta);

		if($eventos !== false)
		{
			if(count($eventos) > 0)
			{
				return $eventos;
			}
			return false;
		}
		return false;
	}
	private $array_palabras = array();
	function obtener_destinos()
	{
		$destinos = destino::obtener_datos_destino();
		for($p = 0;$p < count($destinos); $p++)
        {
        	$this->array_palabras[] = $destinos[$p]['nombre_destino'];
        } 
	}
	function obtener_temporadas()
	{
		$temporadas = temporada::obtener_datos_temporada();
        for($p = 0;$p < count($temporadas); $p++)
        { 
        	$this->array_palabras[] = $temporadas[$p]['nombre'];
        } 
	}
	function obtener_titulos_mas_genericos()
	{
		$eventos = evento::obtener_eventos();
		for ($i=0; $i < count($eventos); $i++) 
		{ 
			if($eventos[$i]['titulo'] != '')
				$this->array_palabras[] = $eventos[$i]['titulo'];
			if($eventos[$i]['nombre_generico'] != '')
				$this->array_palabras[] = $eventos[$i]['nombre_generico'];
			if($eventos[$i]['sede'] != '')
				$this->array_palabras[] = $eventos[$i]['sede'];
		}
	}
	function obtener_especialidades()
	{
		$especialidades = especialidades::obtener_datos_especialidades();
        for($p = 0;$p < count($especialidades); $p++)
        { 
        	$this->array_palabras[] = $especialidades[$p]['nombre_especialidad'];
        } 
	}
	function obtener_alcances()
	{
		$alcances = alcance_evento::obtener_datos_alcance_evento();
		for ($i=0; $i < count($alcances); $i++) 
		{ 
			$this->array_palabras[] = $alcances[$i]['descripcion'];
		}
	}
	function obtener_tipos_de_evento()
	{
		$tipos = tipo_evento::obtener_datos_tipo_evento();
		for ($i=0; $i < count($tipos); $i++) 
		{ 
			$this->array_palabras[] = $tipos[$i]['descripcion'];
		}
	}
	function cargar_palabras_clave()
	{
		$this->obtener_destinos();
		$this->obtener_temporadas();
		$this->obtener_titulos_mas_genericos();
		$this->obtener_especialidades();
		$this->obtener_alcances();
		$this->obtener_tipos_de_evento();
		asort($this->array_palabras);
		//echo "<pre>"; print_r($this->array_palabras); echo "</pre>";
		return $this->array_palabras;
	}
	function buscar_eventos_cotizados_por_palabra_clave($palabra, $nombre, $apellido, $estado, $fecha_d, $fecha_h)
	{
		if($fecha_d != '')
		{
			$a_f_d = explode('/', $fecha_d);
			$fecha_desde = $a_f_d[2] . '-' . $a_f_d[1] . '-' . $a_f_d[0];
		}
		if($fecha_h != '')
		{
			$a_f_h = explode('/', $fecha_h);
			$fecha_hasta = $a_f_h[2] . '-' . $a_f_h[1] . '-' . $a_f_h[0];
		}
		if($estado == 'T')
			$estado = '';

		$resultado = array();
		$segun_titulos 		= evento_cotizado::obtener_evento_cotizado_por_palabra_clave_titulo($palabra, $nombre, $apellido, $estado, $fecha_desde, $fecha_hasta);
		if($segun_titulos !== false && count($segun_titulos) > 0)
			$resultado = $segun_titulos;
		$segun_tipo_evento 	= evento_cotizado::obtener_evento_cotizado_por_palabra_clave_tipo_evento($palabra, $nombre, $apellido, $estado, $fecha_desde, $fecha_hasta);
		if($segun_tipo_evento !== false && count($segun_tipo_evento) > 0 )
		{
			if(count($resultado) > 0)
				$resultado = array_merge($resultado, $segun_tipo_evento);
			else
				$resultado = $segun_tipo_evento;
		}
		$segun_temporada 	= evento_cotizado::obtener_evento_cotizado_por_palabra_clave_temporada($palabra, $nombre, $apellido, $estado, $fecha_desde, $fecha_hasta);
		if($segun_temporada !== false && count($segun_temporada) > 0 )
		{
			if(count($resultado) > 0)
				$resultado = array_merge($resultado, $segun_temporada);
			else
				$resultado = $segun_temporada;
		}
		$segun_destino 		= evento_cotizado::obtener_evento_cotizado_por_palabra_clave_destino($palabra, $nombre, $apellido, $estado, $fecha_desde, $fecha_hasta);
		if($segun_destino !== false && count($segun_destino) > 0 )
		{
			if(count($resultado) > 0)
				$resultado = array_merge($resultado, $segun_destino);
			else
				$resultado = $segun_destino;
		}
		$segun_alcance 		= evento_cotizado::obtener_evento_cotizado_por_palabra_clave_alcance($palabra, $nombre, $apellido, $estado, $fecha_desde, $fecha_hasta);
		if($segun_alcance !== false && count($segun_alcance) > 0 )
		{
			if(count($resultado) > 0)
				$resultado = array_merge($resultado, $segun_alcance);
			else
				$resultado = $segun_alcance;
		}
		$segun_especialidad = evento_cotizado::obtener_evento_cotizado_por_palabra_clave_especialidad($palabra, $nombre, $apellido, $estado, $fecha_desde, $fecha_hasta);
		if($segun_especialidad !== false && count($segun_especialidad) > 0 )
		{
			if(count($resultado) > 0)
				$resultado = array_merge($resultado, $segun_especialidad);
			else
				$resultado = $segun_especialidad;
		}
		$result = array_map("unserialize", array_unique(array_map("serialize", $resultado)));

		return $result;
	}
}
?>