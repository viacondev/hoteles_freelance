<?php 
/**
* 
*/
class c_mostrar_cotizacion
{
	
	function obtener_paxs($idcotizacion)
	{
		$paxs = pax::obtener_pax_idcotizacion($idcotizacion);
		if($paxs === false)
			$paxs = array();
		return $paxs;
	}
	function obtener_nombre_del_cliente($idclientes)
	{
		$datos_cliente = clientes::enviar_datos_basicos_cliente($idclientes);
		if($datos_cliente === false || count($datos_cliente)==0)
			return "- NO ENCONTRADO -";
		return strtoupper($datos_cliente[0]['nombre_cliente'] . ' ' . $datos_cliente[0]['apellido_cliente']);
	}

	/*
	* OBTIENE COTIZACIONES POR PUBLICACION
	*/

		function obtener_cotizaciones_por_publicacion($idpublicacion)
		{
			$cotizaciones = publicacion::obtener_cotizaciones_de_publicacion($idpublicacion);
			if($cotizaciones === false)
				$cotizaciones = array();
			return $cotizaciones;
		}

	/*
	* MUESTRA LOS SERVICIOS POR PASAJERO
	*/

		function obtener_detalle_hotel_por_pax($idpax)
		{
			$hoteles = pax::obtener_items_hotel_por_pax($idpax);
			if($hoteles === false)
				$hoteles = array();

			$total_costo_hotel = 0;
			for ($i=0; $i < count($hoteles); $i++) 
			{ 
				$hotel = $hoteles[$i];

				// COLOCAR EL COSTO SEGUN EL TIPO DE HABITACION TOMADO
				switch ($hotel['tipo_precio']) 
				{
					case 1: 	{ $costo_hotel = $hotel['precio_single']; 		$hoteles[$i]['habitacion'] = 'SINGLE'; 	} break;
					case 2: 	{ $costo_hotel = $hotel['precio_doble']; 		$hoteles[$i]['habitacion'] = 'DOBLE';  	} break;
					case 3: 	{ $costo_hotel = $hotel['precio_triple']; 		$hoteles[$i]['habitacion'] = 'TRIPLE';  	} break;
					case 4: 	{ $costo_hotel = $hotel['precio_cuadruple']; 	$hoteles[$i]['habitacion'] = 'CUADRUPLE'; } break;
					case 5: 	{ $costo_hotel = $hotel['precio_menor']; 		$hoteles[$i]['habitacion'] = 'MENOR';  	} break;
					case 6: 	{ $costo_hotel = $hotel['precio_infante']; 		$hoteles[$i]['habitacion'] = 'INFANTE';  	} break;
					default: 	{ $costo_hotel = 0; $hotel['habitacion'] = 'N/N'; }	break;
				}

				// MULTIPLICAR POR LA CANTIDAD DE NOCHES
				$cant_noches 	= floor((strtotime($hotel['fecha_out'] . ' 00:00:00') - strtotime($hotel['fecha_in'] . ' 00:00:00')) / 86400);
				$costo_hotel    *= $cant_noches;

				// TRANSFORMAR A DOLARES SI ESTA EN BOLIVIANOS
				if($hotel['moneda'] == 'B')
				{
					$costo_hotel /= $hotel['tipo_cambio'];
				}

				// SUMAR FEE
				if($hotel['increment_fee'] == 'P')
				{
					$fee 			= 1 + ($hotel['fee']/100);
					$costo_hotel 	*= $fee;
				}
				else
				{
					$fee 		 	= $hotel['fee'];
					$costo_hotel 	+= $fee;
				}

				// SUMAR IVA
				if($hotel['increment_factura'] == 'P')
				{
					$iva 			= 1 + ($hotel['factura']/100);
					$costo_hotel 	*= $iva;
				}
				else
				{
					$iva 			= $hotel['factura'];
					$costo_hotel 	+= $iva;
				}

				// AÑADIMOS AL ARRAY EL PRECIO DE SERVICIO DE HOTEL DEL PASAJERO MAS FEE MAS IVA AL ENTERO INMEDIATO SUPERIOR
				$costo_hotel 					= ceil($costo_hotel);
				$hoteles[$i]['precio_hotel'] 	= $costo_hotel;
				
				$total_costo_hotel 		+= $costo_hotel;
			}
			
			$hoteles['precio_total_pax'] = $total_costo_hotel;
			return $hoteles;
		}
		function obtener_detalle_boletos_por_pax($idpax)
		{
			$boletos = pax::obtener_boletos_por_pax($idpax);
			if($boletos === false)
				$boletos = array();

			$total_costo_boletos = 0;
			for ($i=0; $i < count($boletos); $i++) 
			{ 
				$boleto = $boletos[$i];

				switch($boleto['tipo_precio'])
				{
					case 1: { $costo_boleto = $boleto['precio_adulto']; 	$boletos[$i]['tarifa'] = 'ADT'; } break;
					case 2: { $costo_boleto = $boleto['precio_menor']; 		$boletos[$i]['tarifa'] = 'CNN'; } break;
					case 3: { $costo_boleto = $boleto['precio_infante'];	$boletos[$i]['tarifa'] = 'INF'; } break;
					default: { $costo_boleto = 0; $boletos[$i]['tarifa'] = 'N/N'; } break;
				}

				// TRANSFORMAR A DOLARES SI ESTA EN BOLIVIANOS
				if($boleto['moneda'] == 'B')
				{
					$costo_boleto /= $boleto['tipo_cambio'];
				}

				// SUMAR FEE
				if($boleto['increment_fee'] == 'P')
				{
					$fee 			= 1 + ($boleto['fee']/100);
					$costo_boleto 	*= $fee;
				}
				else
				{
					$fee 		 	= $boleto['fee'];
					$costo_boleto 	+= $fee;
				}

				// SUMAR IVA
				if($boleto['increment_factura'] == 'P')
				{
					$iva 			= 1 + ($boleto['factura']/100);
					$costo_boleto 	*= $iva;
				}
				else
				{
					$iva 			= $boleto['factura'];
					$costo_boleto 	+= $iva;
				}

				// AÑADIMOS AL ARRAY EL PRECIO DE SERVICIO DE BOLETO AEREO DEL PASAJERO MAS FEE MAS IVA AL ENTERO INMEDIATO SUPERIOR
				$costo_boleto 					= ceil($costo_boleto);
				$boletos[$i]['precio_boleto'] 	= $costo_boleto;
				
				$total_costo_boletos 	+= $costo_boleto;
			}
			$boletos['precio_total_pax'] 	= $total_costo_boletos;
			return $boletos;
		}
		function obtener_detalle_servicios_por_pax($idpax)
		{
			$servicios = pax::obtener_servicios_por_pax($idpax);
			if($servicios === false)
				$servicios = array();

			$total_costo_servicios = 0;
			for ($i=0; $i < count($servicios); $i++) 
			{ 
				$servicio = $servicios[$i];

				switch($servicio['tipo_precio'])
				{
					case 1: { $costo_servicio = $servicio['precio_adulto']; 	$servicios[$i]['tarifa'] = 'ADT'; } break;
					case 2: { $costo_servicio = $servicio['precio_menor']; 		$servicios[$i]['tarifa'] = 'CNN'; } break;
					case 3: { $costo_servicio = $servicio['precio_infante'];	$servicios[$i]['tarifa'] = 'INF'; } break;
					default: { $costo_servicio = 0; $servicios[$i]['tarifa'] = 'N/N'; } break;
				}

				// TRANSFORMAR A DOLARES SI ESTA EN BOLIVIANOS
				if($servicio['moneda'] == 'B')
				{
					$costo_servicio /= $servicio['tipo_cambio'];
				}

				// SUMAR FEE
				if($servicio['increment_fee'] == 'P')
				{
					$fee 			= 1 + ($servicio['fee']/100);
					$costo_servicio *= $fee;
				}
				else
				{
					$fee 		 	= $servicio['fee'];
					$costo_servicio += $fee;
				}

				// SUMAR IVA
				if($servicio['increment_factura'] == 'P')
				{
					$iva 			= 1 + ($servicio['factura']/100);
					$costo_servicio *= $iva;
				}
				else
				{
					$iva 			= $servicio['factura'];
					$costo_servicio += $iva;
				}

				// AÑADIMOS AL ARRAY EL PRECIO DE SERVICIO DE BOLETO AEREO DEL PASAJERO MAS FEE MAS IVA AL ENTERO INMEDIATO SUPERIOR
				$costo_servicio 					= ceil($costo_servicio);
				$servicios[$i]['precio_servicio'] 	= $costo_servicio;
				
				$total_costo_servicios 	+= $costo_servicio;
			}
			$servicios['precio_total_pax'] 	= $total_costo_servicios;
			return $servicios;
		}
		function obtener_detalle_inscripciones_por_pax($idpax)
		{
			$inscripciones = pax::obtener_inscripciones_por_pax($idpax);
			if($inscripciones === false)
				$inscripciones = array();

			$total_costo_inscripciones = 0;
			for ($i=0; $i < count($inscripciones); $i++) 
			{ 
				$inscripcion = $inscripciones[$i];

				$costo_inscripcion = $inscripcion['precio'];

				// TRANSFORMAR A DOLARES SI ESTA EN BOLIVIANOS
				if($inscripcion['moneda'] == 'B')
				{
					$costo_inscripcion /= $inscripcion['tipo_cambio'];
				}

				// SUMAR FEE
				if($inscripcion['increment_fee'] == 'P')
				{
					$fee 				= 1 + ($inscripcion['fee']/100);
					$costo_inscripcion 	*= $fee;
				}
				else
				{
					$fee 		 		= $inscripcion['fee'];
					$costo_inscripcion 	+= $fee;
				}

				// SUMAR IVA
				if($inscripcion['increment_factura'] == 'P')
				{
					$iva 				= 1 + ($inscripcion['factura']/100);
					$costo_inscripcion 	*= $iva;
				}
				else
				{
					$iva 				= $inscripcion['factura'];
					$costo_inscripcion 	+= $iva;
				}

				// AÑADIMOS AL ARRAY EL PRECIO DE INSCRIPCION AEREO DEL PASAJERO MAS FEE MAS IVA AL ENTERO INMEDIATO SUPERIOR
				$costo_inscripcion 							= ceil($costo_inscripcion);
				$inscripciones[$i]['precio_inscripcion'] 	= $costo_inscripcion;
				
				$total_costo_inscripciones 	+= $costo_inscripcion;
			}
			$inscripciones['precio_total_pax'] 	= $total_costo_inscripciones;
			return $inscripciones;
		}

	/*
	* MUESTRA LOS PAXS SEGUN EL SERVICIO
	*/

		function obtener_paxs_segun_hotel($iditem_hotel)
		{
			$paxs = pax::obtener_paxs_por_item_hotel($iditem_hotel);
			if($paxs === false)
				$paxs = array();
			return $paxs;
		}
		function obtener_paxs_segun_servicio($idtarifa_otros)
		{
			$paxs = pax::obtener_paxs_segun_servicio($idtarifa_otros);
			if($paxs === false)
				$paxs = array();
			return $paxs;
		}
		function obtener_paxs_segun_boleto($idboleto_cotizado)
		{
			$paxs = pax::obtener_paxs_segun_boleto($idboleto_cotizado);
			if($paxs === false)
				$paxs = array();
			return $paxs;
		}
		function obtener_paxs_segun_inscripcion($idinscripcion_evento)
		{
			$paxs = pax::obtener_paxs_segun_inscripcion($idinscripcion_evento);
			if($paxs === false)
				$paxs = array();
			return $paxs;
		}

	/*
	* ARCHIVOS ADJUNTOS POR SERVICIO
	*/

		function mostrar_adjuntos_por_hotel($iditem_hotel)
		{
			$adjuntos = archivo_adjunto::obtener_archivos_x_hotel($iditem_hotel);
			if($adjuntos === false)
				$adjuntos = array();
			return $adjuntos;
		}
		function mostrar_adjuntos_por_servicio($idtarifa_otros)
		{
			$adjuntos = archivo_adjunto::obtener_archivos_x_servicio($idtarifa_otros);
			if($adjuntos === false)
				$adjuntos = array();
			return $adjuntos;
		}
		function mostrar_adjuntos_por_boleto($idboleto_cotizado)
		{
			$adjuntos = archivo_adjunto::obtener_archivos_x_aereo($idboleto_cotizado);
			if($adjuntos === false)
				$adjuntos = array();
			return $adjuntos;
		}
		function mostrar_adjuntos_por_inscripcion($idinscripcion_evento)
		{
			$adjuntos = archivo_adjunto::obtener_archivos_x_inscripcion($idinscripcion_evento);
			if($adjuntos === false)
				$adjuntos = array();
			return $adjuntos;
		}

	/*
	* REPORTES GENERALES
	*/

		function mostrar_destinos_de_hotel_por_cotizacion($idcotizacion)
		{
			$destinos = tarifa_hotel::obtener_destinos_por_cotizacion($idcotizacion);
			if($destinos === false)
				$destinos = array();
			return $destinos;
		}
		function obtener_hoteles_por_rango_destinos($destinos, $is_real)
		{
			$hoteles = tarifa_hotel::obtener_hoteles_por_rango_destinos($destinos, $is_real);
			if($hoteles === false)
				$hoteles = array();
			return $hoteles;
		}

		/*
		* OBTENER SERVICIOS POR PAX
		*/

			function obtener_id_servicios_hoteles($idpax)
			{
				$iditems = tarifa_hotel::obtener_id_hoteles_por_pax($idpax);
				if($iditems === false)
					$iditems = array();

				$items 		= array();
				$detalles 	= array();
				for ($i=0; $i < count($iditems); $i++) 
				{ 
					$items[] = $iditems[$i]['iditem_hotel'];
					$detalles[$iditems[$i]['iditem_hotel']] = array('t' => $iditems[$i]['tipo_precio'], 'h' => $iditems[$i]['habitacion']);
				}

				$detalles['items'] = implode(',', $items);
				
				return $detalles;
			}
			function obtener_id_otros_servicios($idpax)
			{
				$iditems = tarifa_otros::obtener_id_servicios_por_pax($idpax);
				if($iditems === false)
					$iditems = array();

				$items 		= array();
				$detalles 	= array();
				for ($i=0; $i < count($iditems); $i++) 
				{ 
					$items[] = $iditems[$i]['idtarifa_otros'];
					$detalles[$iditems[$i]['idtarifa_otros']] = array('t' => $iditems[$i]['tipo_precio']);
				}

				$detalles['items'] = implode(',', $items);
				
				return $detalles;
			}
			function obtener_id_servicios_aereo($idpax)
			{
				$idboletos = boleto_cotizado::obtener_id_boletos_por_pax($idpax);
				if($idboletos === false)
					$idboletos = array();

				$boletos 		= array();
				$detalles 		= array();
				for ($i=0; $i < count($idboletos); $i++) 
				{ 
					$boletos[] = $idboletos[$i]['idboleto_cotizado'];
					$detalles[$idboletos[$i]['idboleto_cotizado']] = array('t' => $idboletos[$i]['tipo_precio'], 'b' => $idboletos[$i]['numero_ticket']);
				}

				$detalles['items'] = implode(',', $boletos);
				
				return $detalles;
			}
			function obtener_id_servicios_inscripcion($idpax)
			{
				$idinscripciones = inscripcion_evento::obtener_id_inscripciones_por_pax($idpax);
				if($idinscripciones === false)
					$idinscripciones = array();

				$inscripciones 		= array();
				$detalles 		= array();
				for ($i=0; $i < count($idinscripciones); $i++) 
				{ 
					$inscripciones[] = $idinscripciones[$i]['idinscripcion_evento'];
					$detalles[$idinscripciones[$i]['idinscripcion_evento']] = 0;
				}

				$detalles['items'] = implode(',', $inscripciones);
				
				return $detalles;
			}
		/*
		* RESUMEN DE HOTELES
		*/

			function obtener_destinos_hotel_por_rango_cotizaciones($cotizaciones)
			{
				$dhoteles = tarifa_hotel::obtener_destinos_x_rango_cotizaciones($cotizaciones);
				if($dhoteles === false)
					$dhoteles = array();
				return $dhoteles;	
			}
			function obtener_items_hotel_x_destino($destino, $cotizaciones)
			{
				$ihoteles = tarifa_hotel::obtener_items_hotel_x_destino($destino, $cotizaciones);
				if($ihoteles === false)
					$ihoteles = array();
				return $ihoteles;
			}

		/*
		* RESUMEN DE OTROS SERVICIOS
		*/

			function obtener_servicios_por_rango_cotizaciones($cotizaciones)
			{
				$servicios = tarifa_otros::obtener_otros_servicios_segun_cotizaciones($cotizaciones);
				if($servicios === false)
					$servicios = array();
				return $servicios;
			}
			function obtener_tarifas_otros_servicios_por_servicio($idotro_servicio, $cotizaciones)
			{
				$tarifas = tarifa_otros::obtener_tarifas_otros_servicios_x_servicio($idotro_servicio, $cotizaciones);
				if($tarifas === false)
					$tarifas = array();
				return $tarifas;
			}

		/*
		* RESUMEN DE BOLETOS AEREOS
		*/

			function obtener_lineas_aereas_por_rango_cotizaciones($cotizaciones)
			{
				$lineas_aereas = boleto_cotizado::obtener_lineas_aereas_segun_cotizaciones($cotizaciones);
				if($lineas_aereas === false)
					$lineas_aereas = array();
				return $lineas_aereas;
			}
			function obtener_boletos_por_linea_aerea($idlineas_aereas, $cotizaciones)
			{
				$boletos = boleto_cotizado::obtener_boletos_x_linea_aerea($idlineas_aereas, $cotizaciones);
				if($boletos === false)
					$boletos = array();
				return $boletos;
			}

		/*
		* RESUMEN DE INSCRIPCIONES A EVENTOS
		*/

			function obtener_eventos_por_rango_cotizaciones($cotizaciones)
			{
				$eventos = inscripcion_evento::obtener_eventos_segun_cotizaciones($cotizaciones);
				if($eventos === false)
					$eventos = array();
				return $eventos;
			}
			function obtener_inscripciones_por_evento($idevento, $cotizaciones)
			{
				$inscripciones = inscripcion_evento::obtener_inscripciones_por_evento($idevento, $cotizaciones);
				if($inscripciones === false)
					$inscripciones = array();
				return $inscripciones;
			}

		/*
		* REPORTE POR SERVICIOS
		*/

			function obtener_pasajeros_por_servicio($idtarifa_otros)
			{
				$pasajeros = tarifa_otros::obtener_pasajeros_por_servicio($idtarifa_otros);
				if($pasajeros === false)
					$pasajeros = array();
				return $pasajeros;
			}
			function obtener_pasajeros_por_servicio_hotel($iditem_hotel)
			{
				$pasajeros = tarifa_hotel::obtener_pasajeros_por_item_hotel($iditem_hotel);
				if($pasajeros === false)
					$pasajeros = array();
				return $pasajeros;
			}
			function obtener_pasajeros_por_servicio_aereo($idboleto_cotizado)
			{
				$pasajeros = boleto_cotizado::obtener_pasajeros_por_boleto_cotizado($idboleto_cotizado);
				if($pasajeros === false)
					$pasajeros = array();
				return $pasajeros;
			}
			function obtener_pasajeros_por_servicio_inscripcion($idinscripcion_evento)
			{
				$pasajeros = inscripcion_evento::obtener_pasajeros_por_inscripcion_evento($idinscripcion_evento);
				if($pasajeros === false)
					$pasajeros = array();
				return $pasajeros;	
			}

		/*
		* CONTACTO POR PAX
		*/

			function obtener_telefonos_x_pax($idclientes)
			{
				$telefonos 	= clientes::obtener_telefonos_de_cliente($idclientes);
				$telfs = '';
				for ($i=0; $i < count($telefonos); $i++) 
				{ 
					$telfs .= $telefonos[$i]['numero_telefono'] . '<br/>';
				}

				return $telfs;
			}
			function obtener_correos_x_pax($idclientes)
			{
				$correos 	= clientes::obtener_correos_de_cliente($idclientes);
				
				$mails = '';
				for ($i=0; $i < count($correos); $i++) 
				{ 
					$mails .= $correos[$i]['e_mail'] . '<br/>';
				}

				return $mails;
			}

	/*
	* DATOS PARA EL LLENADO DEL CHECKLIST
	*/

		function obtener_paxs_mas_datos_checklist($idcotizacion)
		{
			$paxs = pax::obtener_pax_detalles_idcotizacion($idcotizacion);
			if($paxs === false)
				$paxs = array();
			return $paxs;
		}

	/*
	* MOSTRAR REQUISITOS DE VIAJE
	*/

		private $buffer_requisitos = array();
		function obtener_requisitos_x_pais_nacionalidad($idpais, $idnacionalidad)
		{
			$index 	= $idpais . '_' . $idnacionalidad;
			if(array_key_exists($index, $this->buffer_requisitos))
			{
				return $this->buffer_requisitos[$index];
			}
			else
			{
				$requisitos = requisito_viaje::obtener_requisitos_viaje_x_pais_nacionalidad($idpais, $idnacionalidad);

				if ($requisitos !== false) 
				{
					if(count($requisitos) > 0)
						$this->buffer_requisitos[$index] = $requisitos[0];
					else
						$this->buffer_requisitos[$index] = "No Ingresado";
				}
				else
					$this->buffer_requisitos[$index] = "No Ingresado";
				
				return	$this->buffer_requisitos[$index];
			}
		}

		private $buffer_paises = array();
		function obtener_pais_x_id($idpais)
		{
			if(array_key_exists($idpais, $this->buffer_paises))
			{
				return $this->buffer_paises[$idpais];
			}
			else
			{
				$pais = pais::obtener_pais($idpais);
				
				if($pais !== false)
				{
					if(count($pais) > 0)
					{
						$n_pais = $pais[0]['nombre_pais'];
						$this->buffer_paises[$idpais] = $n_pais;
					}
					else
						$this->buffer_paises[$idpais] = 'No Ingresado';
					return $this->buffer_paises[$idpais];
				}
				$this->buffer_paises[$idpais] = 'No Ingresado';
				return $this->buffer_paises[$idpais];
			}	
		}
}
?>