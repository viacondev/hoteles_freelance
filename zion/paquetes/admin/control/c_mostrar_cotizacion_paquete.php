<?php

/**
* 
*/
class c_mostrar_cotizacion_paquete
{
	
	function obtener_cotizacion_paquete($idpaquete_cotizado)
	{
		$paquete_cotizado 	= new paquete_cotizado;
		$cotizacion 		= $paquete_cotizado->obtener_paquete_cotizado($idpaquete_cotizado);

		if($cotizacion !== false)
		{
			if(count($cotizacion) > 0)
			{
				return $cotizacion[0];
			}
			return false;
		}
		return false;
	}

	function obtener_paquete($idpaquete)
	{
		$paquete 	= new paquete;
		$mi_paquete 	= $paquete->obtener_paquete($idpaquete);

		if($mi_paquete !== false)
		{
			if(count($mi_paquete) > 0)
			{
				return $mi_paquete[0];
			}
			return false;
		}
		return false;
	}

	function obtener_datos_cliente($idclientes)
	{
		$clientes = new clientes;
		$cliente = $clientes->enviar_datos_basicos_cliente($idclientes);
		if($cliente!==false)
		{
			if(count($cliente)>0)
			{
				return $cliente[0];
			}
			return false;
		}
		return false;
	}

	function obtener_seguimientos($idpaquete_cotizado)
	{
		$seguimiento_cotizacion = new seguimiento_cotizacion_paquete;
		$seguimientos = $seguimiento_cotizacion->obtener_seguimientos_por_cotizacion($idpaquete_cotizado);
		if($seguimientos !== false)
		{
			if(count($seguimientos) > 0)
			{
				return $seguimientos;
			}
			return false;
		}
		return false;
	}

	function obtener_telefonos_cliente($idclientes)
	{
		$telefono = new telefono;
		$telefonos = $telefono->obtener_telefonos_cliente($idclientes);
		if($telefonos !== false)
		{
			if(count($telefonos) > 0)
			{
				return $telefonos;
			}
			return false;
		}
		return false;
	}

	function obtener_mails_cliente($idclientes)
	{
		$e_mails = new e_mails;
		$correos = $e_mails->obtener_mails_cliente($idclientes);
		if($correos !== false)
		{
			if(count($correos) > 0)
			{
				return $correos;
			}
			return false;
		}
		return false;
	}

	function obtener_datos_usuario($idusuarios)
	{
		$usuarios 		= new usuarios;
		$datos_usuario 	= $usuarios->obtener_datos_usuario($idusuarios);
		if($datos_usuario !== false)
		{
			if(count($datos_usuario) > 0)
			{
				return $datos_usuario[0];
			}
			return false;
		} 
		return false;
	}

	function cerrar_cotizacion($idpaquete_cotizado, $estado)
	{
		$paquete_cotizado 	= new paquete_cotizado;
		$cambio_estado 		= $paquete_cotizado->cambiar_estado_cotizacion($idpaquete_cotizado, $estado);

		return $cambio_estado;
	}

	function obtener_pasajeros_de_cotizacion($idpaquete_cotizado)
	{
		$paquete_cotizado 	= new paquete_cotizado;
		$pasajeros 			= $paquete_cotizado->obtener_pasajeros_de_cotizacion($idpaquete_cotizado);

		if($pasajeros !== false)
		{
			if(count($pasajeros) > 0)
			{
				return $pasajeros;
			}
			return false;
		}
		return false;
	}

	function obtener_tarifas_hotel_por_paquete($idpaquete)
	{
		$tarifa_hotel_paquete = new tarifa_hotel;
		$tarifas_hotel = $tarifa_hotel_paquete->obtener_tarifas($idpaquete, 'paquete_cotizado');

		if($tarifas_hotel !== false)
		{
			if(count($tarifas_hotel) > 0)
			{
				for ($i=0; $i < count($tarifas_hotel); $i++) 
				{ 
					$idtarifa = $tarifas_hotel[$i]['idtarifa_hotel_paquete_cotizado'];

					$items = $this->obtener_items_de_tarifa($idtarifa);

					$tarifas_hotel[$i]['opciones'] = $items;
				}
				
				return $tarifas_hotel;
			}
			return false;
		}
		return false;
	}

	function obtener_combinaciones_hoteles($idpaquete)
	{
		$opcion_hoteles = new opcion_hoteles;
		$opciones 		= $opcion_hoteles->obtener_opcion_hoteles($idpaquete, 'paquete_cotizado');

		$array_hoteles  = array();

		if($opciones !== false)
		{
			if(count($opciones) > 0)
			{
				for ($i=0; $i < count($opciones); $i++) 
				{ 
					$a_opcion = array();
					$opcion = $opciones[$i];
					$idopc 	= $opcion['idopcion_hoteles_paquete_cotizado'];

					$items = $opcion_hoteles->obtener_opcion_hoteles_detallada($idopc, 'paquete_cotizado');
					
					if(count($items) > 0)
					{
						$sgl = 0;
						$dbl = 0;
						$tpl = 0;
						$cpl = 0;
						$cnn = 0;
						$inf = 0;
						$hot = "";
						$alm = "";
						$iex = "";
						//echo "ITEMS<pre>"; print_r($items); echo "</pre>";
						for ($j=0; $j < count($items); $j++) 
						{ 
							$cant_noches = $items[$j]['cant_noches'];
							if($cant_noches <= 0)
							{
								$f_in 	= strtotime($items[$j]['in'] . ' 00:00:00');
								$f_out 	= strtotime($items[$j]['out'] . ' 00:00:00');
								$cant_noches = floor( $f_out - $f_in) / 86400;
							}

							$sgl += $items[$j]['precio_single'] * $cant_noches * (1 + $items[$j]['comision']/100) * (1 + $items[$j]['factura']/100);
							$dbl += $items[$j]['precio_doble'] * $cant_noches * (1 + $items[$j]['comision']/100) * (1 + $items[$j]['factura']/100);
							$tpl += $items[$j]['precio_triple'] * $cant_noches * (1 + $items[$j]['comision']/100) * (1 + $items[$j]['factura']/100);
							$cpl += $items[$j]['precio_cuadruple'] * $cant_noches * (1 + $items[$j]['comision']/100) * (1 + $items[$j]['factura']/100);
							$cnn += $items[$j]['precio_menor'] * $cant_noches * (1 + $items[$j]['comision']/100) * (1 + $items[$j]['factura']/100);
							$inf += $items[$j]['precio_infante'] * $cant_noches * (1 + $items[$j]['comision']/100) * (1 + $items[$j]['factura']/100);

							$hot .= "<a href='http://" . $items[$j]['link'] . "' style='text-decoration:none;'>". strtoupper(nl2br($items[$j]['nombre_hotel'])) . ' - ' . $items[$j]['categoria'] . '* (' . $items[$j]['destino'] . ')</a><br/>';
							if($items[$j]['alimentacion'] != '')
								$alm .= '- ' . $items[$j]['alimentacion'] . '<br/>';
							if($items[$j]['info_extra'] != '')
								$iex .= '- ' . $items[$j]['info_extra'] . '<br/>';
						}
						$a_opcion['hotel'] 				= $hot;
						$a_opcion['precio_single'] 		= ceil($sgl);
						$a_opcion['precio_doble'] 		= ceil($dbl);
						$a_opcion['precio_triple'] 		= ceil($tpl);
						$a_opcion['precio_cuadruple'] 	= ceil($cpl);
						$a_opcion['precio_menor'] 		= ceil($cnn);
						$a_opcion['precio_infante'] 	= ceil($inf);
						$a_opcion['observacion'] 		= $opcion['observacion'];
						$a_opcion['alimentacion'] 		= $alm;
						$a_opcion['info_extra'] 		= $iex;

						$array_hoteles[] = $a_opcion;
					}
				}

				for ($i=0; $i < count($array_hoteles) - 1; $i++) 
				{ 
					for ($j=$i; $j < count($array_hoteles) - 1; $j++) 
					{ 
						$a_opcion1 = $array_hoteles[$j];
						$a_opcion2 = $array_hoteles[$j+1];
						$total1 = $a_opcion1['precio_single'] + $a_opcion1['precio_doble'] + $a_opcion1['precio_triple'] + $a_opcion1['precio_cuadruple']; 
						$total2 = $a_opcion2['precio_single'] + $a_opcion2['precio_doble'] + $a_opcion2['precio_triple'] + $a_opcion2['precio_cuadruple']; 
						if($total2 < $total1)
						{
							$aux = $array_hoteles[$j];
							$array_hoteles[$j] = $array_hoteles[$j+1];
							$array_hoteles[$j+1] = $aux;
						}
					}
				}
				
				return $array_hoteles;
			}
		}
		return false;
	}

	function evaluar_vista_hoteles($array_hoteles)
	{
		$cad_respuesta 		= "";
		$exist_single 		= false;
		$exist_doble 		= false;
		$exist_triple 		= false;
		$exist_cuadruple 	= false;
		$exist_menor 		= false;
		$exist_infante 		= false;

		for ($i=0; $i < count($array_hoteles); $i++) 
		{ 
			if($array_hoteles[$i]['precio_single'] != 0)
				$exist_single 		= true;
			if($array_hoteles[$i]['precio_doble'] != 0)
				$exist_doble 		= true;
			if($array_hoteles[$i]['precio_triple'] != 0)
				$exist_triple 		= true;
			if($array_hoteles[$i]['precio_cuadruple'] != 0)
				$exist_cuadruple 	= true;
			if($array_hoteles[$i]['precio_menor'] != 0)
				$exist_menor 		= true;
			if($array_hoteles[$i]['precio_infante'] != 0)
				$exist_infante 		= true;
		}

		if($exist_single)
			$cad_respuesta .= 'SGL/';
		if($exist_doble)
			$cad_respuesta .= 'DBL/';
		if($exist_triple)
			$cad_respuesta .= 'TPL/';
		if($exist_cuadruple)
			$cad_respuesta .= 'CPL/';
		if($exist_menor)
			$cad_respuesta .= 'CNN/';
		if($exist_infante)
			$cad_respuesta .= 'INF/';

		return $cad_respuesta;
	}

	function obtener_items_de_tarifa($idtarifa_hotel_paquete)
	{
		$item_hotel_paquete 	= new tarifa_hotel;
		$items 				= $item_hotel_paquete->obtener_items_por_tarifa_hotel($idtarifa_hotel_paquete, 'paquete_cotizado');

		if($items !== false)
		{
			if(count($items) > 0)
			{
				return $items;
			}
			return false;
		}
		return false;
	}

	function obtener_paxs_del_hotel($idtarifa)
	{
		$hotel_cotizado = new hotel_cotizado;
		$nombres = $hotel_cotizado->obtener_nombres_pasajeros_por_hotel($idtarifa, 'paquete');

		if($nombres === false)
			$nombres = array();
		return $nombres;
	}

	function obtener_otras_tarifas($idpaquete)
	{
		$tarifa_otros = new tarifa_otros;
		$tarifas = $tarifa_otros->obtener_tarifa_otros($idpaquete, 'paquete_cotizado');

		if($tarifas !== false)
		{
			if(count($tarifas) > 0)
			{
				$total_adt = 0;
				$total_cnn = 0;
				$total_inf = 0;
				for ($i=0; $i < count($tarifas); $i++) 
				{ 
					$fee 		= $tarifas[$i]['fee'] / 100;
					$factura 	= $tarifas[$i]['factura'] / 100;
					$fact 		= (1+$fee) * (1+$factura);
					$precio_adt = $tarifas[$i]['precio_adulto'];
					$precio_cnn = $tarifas[$i]['precio_menor'];
					$precio_inf = $tarifas[$i]['precio_infante'];
					$tipo_cambio= $tarifas[$i]['tipo_cambio'];

					if ($tarifas[$i]['moneda']=='B') 
					{
						$precio_adt 	= $precio_adt / $tipo_cambio;
						$precio_cnn 	= $precio_cnn / $tipo_cambio;
						$precio_inf 	= $precio_inf / $tipo_cambio;	
					}

					$subtotal_adt 	= ceil($precio_adt * $fact);
					$subtotal_cnn   = ceil($precio_cnn * $fact);
					$subtotal_inf   = ceil($precio_inf * $fact);

					$tarifas[$i]['subtotal_adt'] = $subtotal_adt;
					$tarifas[$i]['subtotal_cnn'] = $subtotal_cnn;
					$tarifas[$i]['subtotal_inf'] = $subtotal_inf;

					if($tarifas[$i]['estado'] == '1')
					{
						$total_adt += $subtotal_adt;
						$total_cnn += $subtotal_cnn;
						$total_inf += $subtotal_inf;
					}
				}
				$tarifas['total_adt'] 	= $total_adt;
				$tarifas['total_cnn'] 	= $total_cnn;
				$tarifas['total_inf'] 	= $total_inf;
				return $tarifas;
			}
			return false;
		}
		return false;
	}

	function obtener_paxs_del_servicio($idtarifa)
	{
		$otros_cotizado = new otros_cotizado;
		$nombres = $otros_cotizado->obtener_nombres_pasajeros_por_servicio($idtarifa, 'paquete');

		if($nombres === false)
			$nombres = array();
		return $nombres;
	}

	function obtener_opciones_de_vuelos($idpaquete)
	{
		$boleto_cotizado = new boleto_cotizado;
		$opciones_aereo = $boleto_cotizado->obtener_opciones_boletos($idpaquete, 'paquete_cotizado');

		if($opciones_aereo !== false)
		{
			for ($i=0; $i < count($opciones_aereo); $i++) 
			{ 
				$opcion_aereo = $opciones_aereo[$i];
				$idopcion_aereo = $opcion_aereo['idopcion_boleto_paquete_cotizado'];

				// CARGAR LOS BOLETOS DENTRO DE LA MISMA OPCION
				$boletos 						= $boleto_cotizado->obtener_boletos_por_opcion($idopcion_aereo, 'paquete_cotizado');
				$opciones_aereo[$i]['boletos'] 	= $boletos;
				$total_adt 					= 0;
				$total_cnn 					= 0;
				$total_inf 					= 0;
				for ($j=0; $j < count($boletos); $j++) 
				{ 
					$fact = (1 + ($boletos[$j]['fee']/100)) * (1 + ($boletos[$j]['factura']/100));
					$precio_adt 	= $boletos[$j]['precio_adulto'];
					$precio_cnn 	= $boletos[$j]['precio_menor'];
					$precio_inf 	= $boletos[$j]['precio_infante'];
					$tipo_cambio 	= $boletos[$j]['tipo_cambio'];

					if($boletos[$j]['moneda'] == 'B')
					{
						$precio_adt = $precio_adt / $tipo_cambio;
						$precio_cnn = $precio_cnn / $tipo_cambio;
						$precio_inf = $precio_inf / $tipo_cambio;
					}

					if($boletos[$j]['estado'] == 1)
					{
						$total_adt += $precio_adt * $fact;
						$total_cnn += $precio_cnn * $fact;
						$total_inf += $precio_inf * $fact;	
					}
				}
				$opciones_aereo[$i]['adt'] = ceil($total_adt);
				$opciones_aereo[$i]['cnn'] = ceil($total_cnn);
				$opciones_aereo[$i]['inf'] = ceil($total_inf);

				//CARGAR EL ITINERARIO
				$itinerario = segmento_publicado::obtener_segmentos_de_opcion($idopcion_aereo, 'paquete_cotizado');
				$opciones_aereo[$i]['itinerario'] = $itinerario;
			}
		}
		else
		{
			$opciones_aereo = array();	
		}
		return $opciones_aereo;
	}

	function obtener_boletos_cotizados($idpaquete)
	{
		$boleto_cotizado = new boleto_cotizado;
		$boletos = $boleto_cotizado->obtener_boletos_cotizados($idpaquete, 'paquete_cotizado');

		if($boletos !== false)
		{
			if(count($boletos) > 0)
			{
				$total_adt = 0;
				$total_cnn = 0;
				$total_inf = 0;
				
				for ($i=0; $i < count($boletos); $i++) 
				{ 
					$fee 			= $boletos[$i]['fee'] / 100;
					$factura 		= $boletos[$i]['factura'] / 100;
					$precio_adt 	= $boletos[$i]['precio_adulto'];
					$precio_cnn 	= $boletos[$i]['precio_menor'];
					$precio_inf 	= $boletos[$i]['precio_infante'];

					if($boletos[$i]['moneda'] == 'B')
					{
						$precio_adt = $precio_adt / 6.96;
						$precio_cnn = $precio_cnn / 6.96;
						$precio_inf = $precio_inf / 6.96;
					}

					$subtotal_adt 	= ceil($precio_adt + ($precio_adt * $fee) + ($precio_adt * $factura));
					$subtotal_cnn   = ceil($precio_cnn + ($precio_cnn * $fee) + ($precio_cnn * $factura));
					$subtotal_inf   = ceil($precio_inf + ($precio_inf * $fee) + ($precio_inf * $factura));

					$boletos[$i]['subtotal_adt'] = $subtotal_adt;
					$boletos[$i]['subtotal_cnn'] = $subtotal_cnn;
					$boletos[$i]['subtotal_inf'] = $subtotal_inf;

					if($boletos[$i]['estado'] == '1')
					{
						$total_adt += $subtotal_adt;
						$total_cnn += $subtotal_cnn;
						$total_inf += $subtotal_inf;	
					}
				}
				$boletos['total_adt'] 	= $total_adt;
				$boletos['total_cnn'] 	= $total_cnn;
				$boletos['total_inf'] 	= $total_inf;
				return $boletos;
			}
			return false;
		}
		return false;
	}

	function obtener_paxs_del_boleto($idboleto)
	{
		$boleto_cotizado = new boleto_cotizado;
		$nombres = $boleto_cotizado->obtener_nombres_pasajeros_por_boleto($idboleto, 'paquete');

		if($nombres === false)
			$nombres = array();
		return $nombres;
	}

	function obtener_segmentos_publicados($idpaquete)
	{
		$segmento_publicado = new segmento_publicado;
		$segmentos = $segmento_publicado->obtener_segmentos($idpaquete, 'paquete_cotizado');

		if($segmentos !== false)
		{
			if(count($segmentos) > 0)
			{
				return $segmentos;
			}
			return false;
		}
		return false;
	}

	private $buffer_lineas_aereas = array();
	function obtener_linea_aerea($idlinea_aerea)
	{
		if(array_key_exists($idlinea_aerea, $this->buffer_lineas_aereas))
		{
			return $this->buffer_lineas_aereas[$idlinea_aerea];
		}
		else
		{
			$linea_aerea = lineas_aereas::obtener_linea_aerea_por_id($idlinea_aerea);
			if($linea_aerea !== false)
			{
				if(count($linea_aerea) > 0)
				{
					$aerolinea = explode(',', $linea_aerea[0]['nombre_linea_aerea']);
					$this->buffer_lineas_aereas[$idlinea_aerea] = strtoupper($aerolinea[0] . '(' . $linea_aerea[0]['codigo_iata_linea_aerea'] . ')');
					return $this->buffer_lineas_aereas[$idlinea_aerea];
				}
				$this->buffer_lineas_aereas[$idlinea_aerea] = 'N/I';
				return 'N/I';
			}
			$this->buffer_lineas_aereas[$idlinea_aerea] = 'N/I';
			return 'N/I';
		}
	}

	private $buffer_aeropuertos = array();
	function obtener_aeropuerto($codigo)
	{
		if(array_key_exists($codigo, $this->buffer_aeropuertos))
		{
			return $this->buffer_aeropuertos[$codigo];
		}
		else
		{
			$aeropuerto = aeropuerto::obtener_aeropuerto_por_codigo($codigo);
			
			if($aeropuerto !== false)
			{
				if(count($aeropuerto) > 0)
				{
					$ciudad = explode(',', $aeropuerto[0]['nombre_aeropuerto']);
					$this->buffer_aeropuertos[$codigo] = strtoupper($ciudad[0] . '(' . $aeropuerto[0]['cod_aeropuerto'] . ')');
					return $this->buffer_aeropuertos[$codigo];
				}
				$this->buffer_aeropuertos[$codigo] = 'N/I';
				return 'N/I';
			}
			$this->buffer_aeropuertos[$codigo] = 'N/I';
			return 'N/I';
		}
	}

	private $buffer_paises = array();
	function obtener_pais($codigo)
	{
		if(array_key_exists($codigo, $this->buffer_paises))
		{
			return $this->buffer_paises[$codigo];
		}
		else
		{
			$pais = aeropuerto::obtener_pais_desde_aeropuerto($codigo);
			
			if($pais !== false)
			{
				if(count($pais) > 0)
				{
					$n_pais = $pais[0]['nombre_pais'];
					$idpais = $pais[0]['idpais'];
					$this->buffer_paises[$codigo] = array('idpais' => $idpais, 'nombre_pais' => $n_pais);
				}
				else
					$this->buffer_paises[$codigo] = array('idpais' => 0, 'nombre_pais' => 'No Ingresado');
				return $this->buffer_paises[$codigo];
			}
			$this->buffer_paises[$codigo] = array('idpais' => 0, 'nombre_pais' => 'No Ingresado');
			return $this->buffer_paises[$codigo];
		}	
	}

	function paises_por_cotizacion($idcotizacion)
	{
		$a_paises = array();
		$idspaises = cotizacion_has_pais::obtener_paises_en_cotizacion($idcotizacion);
		for ($i=0; $i < count($idspaises); $i++) 
		{ 
			$a_paises[] = $idspaises[$i]['idpais'];
		}
		return $a_paises;
	}
	
	function obtener_total_monto_pax($idpax)
	{
		$class = "paquete";
		$resp 	= boleto_cotizado::obtenet_boleto_cotizado_idpax($idpax, $class);
		$total 	= 0;
		
		if(count($resp) > 0)
		{
			
			for($i = 0; $i < count($resp); $i++)
			{
				$precio 	= 0;
				$fee 		= 0;
				$factura	= 0;
				$r = boleto_cotizado::obtener_boleto_ruta_precio($resp[$i]['idboleto_cotizado_paquete_cotizado'],$class);
				
				if($resp[$i]['tipo_precio'] == 1)
				{
					$precio 	= $r[0]['precio_adulto'];
					$factor 	= (1+$r[0]['fee']/100) * (1+$r[0]['factura']/100);
						$precio 	= $precio * $factor;
				}
				else
				{
					if($resp[$i]['tipo_precio'] == 2)
					{
						$precio 	= $r[0]['precio_menor'];
						$factor 	= (1+$r[0]['fee']/100) * (1+$r[0]['factura']/100);
						$precio 	= $precio * $factor;
					}
					else
					{
						$precio 	= $r[0]['precio_infante'];
						$factor 	= (1+$r[0]['fee']/100) * (1+$r[0]['factura']/100);
						$precio 	= $precio * $factor;
					}	
				}
				if($r[0]['moneda'] == "B")
				{
					$total += round($precio/ $r[0]['tipo_cambio'],2);
				}
				else
				{
					$total += round($precio,2);
				}
			}
		}
		return $total;
	}
	function obtener_min_time_limit_boleto($idpax)
	{
		$class 			= "paquete";
		$resp 	= boleto_cotizado::obtenet_boleto_cotizado_idpax($idpax, $class);
		$min_time_limit = strtotime('+1 year');

		if(count($resp) > 0)
		{
			
			for($i = 0; $i < count($resp); $i++)
			{
				$r = boleto_cotizado::obtener_boleto_ruta_precio($resp[$i]['idboleto_cotizado_paquete_cotizado'],$class);

				$cur_tlimit = strtotime($r[0]['time_limit']);
				
				if($cur_tlimit < $min_time_limit)
					$min_time_limit = $cur_tlimit;
			}
		}
		else
			return '-';
		return date('d/M/Y', $min_time_limit);
	}

	function obtener_boletos_por_pax($idpax)
	{
		$class = "paquete";
		$resp = boleto_cotizado::obtener_boletos_por_pax($idpax, $class);
		if($resp === false)
		{
			$resp = array();
		}
		
		return $resp;
	}
	
	function obtener_total_monto_pax_hotel($idpax)
	{
		$class 	= "paquete";
		//$resp 	= boleto_cotizado::obtenet_boleto_cotizado_idpax($idpax, $class);
		$resp 	= hotel_cotizado::obtenet_hotel_cotizado_idpax($idpax, $class);
		$total 	= 0;
		
		if(count($resp) > 0)
		{
			
			for($i = 0; $i < count($resp); $i++)
			{
				$precio 	= 0;
				$comision 	= 0;
				$factura	= 0;
				$r 			= hotel_cotizado::obtener_item_hotel_class_cotizado_id($resp[$i]['iditem_hotel_'.$class.'_cotizado'], $class);
				
				switch($resp[$i]['tipo_precio'])
				{
					case 1:
					{
						$precio 	= $r[0]['precio_single'];
						$factor 	= (1+$r[0]['comision']/100) * (1+$r[0]['factura']/100);
						$precio 	= $precio * $factor;
						break;	
					}
					case 2:
					{
						$precio 	= $r[0]['precio_doble'];
						$factor 	= (1+$r[0]['comision']/100) * (1+$r[0]['factura']/100);
						$precio 	= $precio * $factor;
						break;	
					}
					case 3:
					{
						$precio 	= $r[0]['precio_triple'];
						$factor 	= (1+$r[0]['comision']/100) * (1+$r[0]['factura']/100);
						$precio 	= $precio * $factor;
						break;	
					}
					case 4:
					{
						$precio 	= $r[0]['precio_cuadruple'];
						$factor 	= (1+$r[0]['comision']/100) * (1+$r[0]['factura']/100);
						$precio 	= $precio * $factor;
						break;	
					}
					case 5:
					{
						$precio 	= $r[0]['precio_menor'];
						$factor 	= (1+$r[0]['comision']/100) * (1+$r[0]['factura']/100);
						$precio 	= $precio * $factor;
						break;	
					}
					case 6:
					{
						$precio 	= $r[0]['precio_infante'];
						$factor 	= (1+$r[0]['comision']/100) * (1+$r[0]['factura']/100);
						$precio 	= $precio * $factor;
						break;	
					}
				}

				$cant_noches = $r[0]['cant_noches'];
				if($cant_noches == 0)
				{
					$cant_noches = floor( (strtotime($r[0]['out'] . ' 00:00:00') - strtotime($r[0]['in'] . ' 00:00:00')) / 86400);
				}
				$precio = $precio * $cant_noches;
				
				if($r[0]['moneda'] == "B")
				{
					$total += round($precio/ $r[0]['tipo_cambio'],2);
				}
				else
				{
					$total += round($precio,2);
				}
			}
		}
		return $total;
	}

	function obtener_min_time_limit_hotel($idpax)
	{
		$class 			= "paquete";
		$resp 			= hotel_cotizado::obtenet_hotel_cotizado_idpax($idpax, $class);
		$min_time_limit = strtotime('+1 year');
		
		if(count($resp) > 0)
		{
			
			for($i = 0; $i < count($resp); $i++)
			{
				$r 			= hotel_cotizado::obtener_item_hotel_class_cotizado_id($resp[$i]['iditem_hotel_'.$class.'_cotizado'], $class);

				$cur_tlimit = strtotime($r[0]['time_limit']);
				if($cur_tlimit < $min_time_limit)
					$min_time_limit = $cur_tlimit;
			}
		}
		else
			return '-';
			
		return date('d/M/Y', $min_time_limit);
	}

	function obtener_servicios_de_hotel_por_pax($idpax)
	{
		$class = "paquete";
		$resp = hotel_cotizado::obtener_items_hoteles_por_pax($idpax, $class);
		if($resp === false)
		{
			$resp = array();
		}
		
		return $resp;
	}
	
	function obtener_total_monto_pax_otros($idpax)
	{
		$class = "paquete";
		$resp 	= otros_cotizado::obtenet_otros_cotizado_idpax($idpax, $class);
		$total 	= 0;
		
		if(count($resp) > 0)
		{
			
			for($i = 0; $i < count($resp); $i++)
			{
				$precio 	= 0;
				$fee 		= 0;
				$factura	= 0;
				$r = otros_cotizado::obtener_otros_nombre_precio($resp[$i]['idtarifa_otros_'.$class.'_cotizado'],$class);
				
				if($resp[$i]['tipo_precio'] == 1)
				{
					$precio 	= $r[0]['precio_adulto'];
					$factor 	= (1+$r[0]['fee']/100) * (1+$r[0]['factura']/100);
						$precio 	= $precio * $factor;
				}
				else
				{
					if($resp[$i]['tipo_precio'] == 2)
					{
						$precio 	= $r[0]['precio_menor'];
						$factor 	= (1+$r[0]['fee']/100) * (1+$r[0]['factura']/100);
						$precio 	= $precio * $factor;
					}
					else
					{
						$precio 	= $r[0]['precio_infante'];
						$factor 	= (1+$r[0]['fee']/100) * (1+$r[0]['factura']/100);
						$precio 	= $precio * $factor;
					}	
				}
				if($r[0]['moneda'] == "B")
				{
					$total += round($precio/ $r[0]['tipo_cambio'],2);
				}
				else
				{
					$total += round($precio,2);
				}
			}
		}
		return $total;
	}

	function obtener_otros_servicios_por_pax($idpax)
	{
		$class 	= "paquete";
		$resp 	= otros_cotizado::obtener_otros_servicios_por_pax($idpax, $class);
		if($resp === false)
		{
			$resp = array();
		}
		
		return $resp;
	}

	/*
	* NUEVO FORMATO
	*/
	function obtener_info_cotizacion($idcotizacion)
	{
		$cotizacion = cotizacion::obtener_info_cotizacion($idcotizacion);
		if($cotizacion === false)
			$cotizacion = array();
		return $cotizacion;
	}
	function obtener_solicitantes_de_cotizacion($idcotizacion)
	{
		$solicitantes = cotizacion::obtener_solicitantes_x_cotizacion($idcotizacion);
		if($solicitantes === false)
			$solicitantes = array();
		return $solicitantes;	
	}
	function obtener_publicacion_de_cotizacion($idcotizacion)
	{
		$publicacion = cotizacion::obtener_paquete_de_cotizacion($idcotizacion);
		if($publicacion === false)
			$publicacion = array();
		return $publicacion;
	}

	// PRECIOS DE SERVICIOS
	function obtener_ciudades_de_salida($idcotizacion)
	{
		$ciudades = cotizacion::obtener_ciudades_origen_cotizacion($idcotizacion);
		if($ciudades === false)
			$ciudades = array();
		return $ciudades;
	}

	function obtener_destinos_de_hotel($idciudad_origen)
	{
		$destinos = tarifa_hotel::obtener_tarifas_hotel_por_ciudad_origen($idciudad_origen);
		if($destinos === false)
			$destinos = array();
		return $destinos;
	}

	function obtener_hoteles_x_destino($idtarifa_hotel)
	{
		$hoteles = tarifa_hotel::obtener_hoteles_x_destino($idtarifa_hotel);
		if($hoteles === false)
			$hoteles = array();
		return $hoteles;
	}

	function obtener_hoteles_x_destino_real_cotizacion($idtarifa_hotel, $is_real)
	{
		$hoteles = tarifa_hotel::obtener_hoteles_x_destino_real_cotizacion($idtarifa_hotel, $is_real);
		if($hoteles === false)
			$hoteles = array();
		return $hoteles;
	}

	function obtener_otros_servicios_x_ciudad($idciudad_origen)
	{
		$otros_servicios = tarifa_otros::obtener_otros_servicios_por_ciudad($idciudad_origen);
		if($otros_servicios === false)
			$otros_servicios = array();
		return $otros_servicios;
	}

	function obtener_otros_servicios_x_ciudad_real_cotizacion($idciudad_origen, $is_real)
	{
		$otros_servicios = tarifa_otros::obtener_otros_servicios_por_ciudad_real_cotizacion($idciudad_origen, $is_real);
		if($otros_servicios === false)
			$otros_servicios = array();
		return $otros_servicios;
	}

	function obtener_opcion_boletos_x_ciudad($idciudad_origen)
	{
		$opcion_boletos = boleto_cotizado::obtener_opciones_boletos_x_ciudad_origen($idciudad_origen);
		if($opcion_boletos === false)
			$opcion_boletos = array();
		return $opcion_boletos;
	}

	function obtener_boletos_x_opcion_aereo($idopcion_aereo)
	{
		$boletos = boleto_cotizado::obtener_boletos_x_opcion_boleto($idopcion_aereo);
		if($boletos === false)
			$boletos = array();
		return $boletos;
	}

	function obtener_boletos_x_opcion_aereo_real_cotizacion($idopcion_aereo, $is_real)
	{
		$boletos = boleto_cotizado::obtener_boletos_x_opcion_boleto_real_cotizacion($idopcion_aereo, $is_real);
		if($boletos === false)
			$boletos = array();
		return $boletos;
	}

	function obtener_itinerario_x_opcion_aereo($idopcion_aereo)
	{
		$itinerario = boleto_cotizado::obtener_itinerario_x_opcion_boleto($idopcion_aereo);
		if($itinerario === false)
			$itinerario = array();
		return $itinerario;
	}

	function obtener_telefonos_de_cliente($idclientes)
	{
		$telefonos = clientes::obtener_telefonos_de_cliente($idclientes);
		if($telefonos === false)
			$telefonos = array();
		return $telefonos;
	}

	function obtener_correos_de_cliente($idclientes)
	{
		$correos = clientes::obtener_correos_de_cliente($idclientes);
		if($correos === false)
			$correos = array();
		return $correos;
	}

	function evaluar_precios_hotel($precios)
	{
		$sgl = 0;
		$dbl = 0;
		$tpl = 0;
		$cpl = 0;
		$cnn = 0;
		$inf = 0;

		for ($i=0; $i < count($precios); $i++) 
		{ 
			$sgl += $precios[$i]['precio_single'];
			$dbl += $precios[$i]['precio_doble'];
			$tpl += $precios[$i]['precio_triple'];
			$cpl += $precios[$i]['precio_cuadruple'];
			$cnn += $precios[$i]['precio_menor'];
			$inf += $precios[$i]['precio_infante'];	
		}

		$columnas_ocultar = "";
		if($sgl <= 0)
			$columnas_ocultar .= 'SGL/';
		if($dbl <= 0)
			$columnas_ocultar .= 'DBL/';
		if($tpl <= 0)
			$columnas_ocultar .= 'TPL/';
		if($cpl <= 0)
			$columnas_ocultar .= 'CPL/';
		if($cnn <= 0)
			$columnas_ocultar .= 'CNN/';
		if($inf <= 0)
			$columnas_ocultar .= 'INF/';
		//echo $columnas_ocultar;
		return $columnas_ocultar;
	}

	function obtener_nombre_servicio($idotro_servicio)
	{
		$otros_servicios = new pqt_otro_servicio;
		$servicio = $otros_servicios->obtener_otro_servicio($idotro_servicio);
		if($servicio !== false && count($servicio)>0)
			return strtoupper($servicio[0]['nombre_servicio']);
		return 'NO EXISTE SERVICIO';
	}

	function obtener_inscripciones_de_evento($idcotizacion)
	{
		$inscripciones = inscripcion_evento::obtener_inscripciones_por_cotizacion($idcotizacion);
		if($inscripciones === false)
			$inscripciones = array();
		return $inscripciones;
	}

	function obtener_opciones_hotel_combinados($idciudad_origen)
	{
		$opciones = tarifa_hotel::obtener_opcion_hoteles($idciudad_origen);
		if($opciones === false)
			$opciones = array();
		//echo "<pre>"; print_r($opciones); echo "</pre>";
		$mostrar_cols 	= '';
		$cant_cols 		= 0;

		$a_sgl = array();
		$a_dbl = array();
		$a_tpl = array();
		$a_cpl = array();

		for ($i=0; $i < count($opciones); $i++)
		{ 
			$opcion = $opciones[$i];

			$sgl = 0; $is_sgl = 1;
			$dbl = 0; $is_dbl = 1;
			$tpl = 0; $is_tpl = 1;
			$cpl = 0; $is_cpl = 1;
			$cnn = 0; $is_cnn = 1;
			$inf = 0; $is_inf = 1;

			$hoteles = tarifa_hotel::obtener_detalle_opcion_hoteles($opcion['idopcion_hoteles']);
			for ($j=0; $j < count($hoteles); $j++) 
			{ 
				$hotel = $hoteles[$j];

				$cant_noches 	= floor( (strtotime($hotel['fecha_out'] . ' 00:00:00') - strtotime($hotel['fecha_in'] . ' 00:00:00')) / 86400);

				if($is_sgl == 0 || $hotel['precio_single'] <= 0) { $is_sgl=0; $hotel['precio_single'] = 0; }
				if($is_dbl == 0 || $hotel['precio_doble'] <= 0) { $is_dbl=0; $hotel['precio_doble'] = 0; }
				if($is_tpl == 0 || $hotel['precio_triple'] <= 0) { $is_tpl=0; $hotel['precio_triple'] = 0; }
				if($is_cpl == 0 || $hotel['precio_cuadruple'] <= 0) { $is_cpl=0; $hotel['precio_cuadruple'] = 0; }
				if($is_cnn == 0 || $hotel['precio_menor'] <= 0) { $is_cnn=0; $hotel['precio_menor'] = 0; }
				if($is_inf == 0 || $hotel['precio_infante'] <= 0) { $is_inf=0; $hotel['precio_infante'] = 0; }

				$total_sgl 	= $hotel['precio_single'] * $cant_noches;
				$total_dbl 	= $hotel['precio_doble'] * $cant_noches;
				$total_tpl 	= $hotel['precio_triple'] * $cant_noches;
				$total_cpl 	= $hotel['precio_cuadruple'] * $cant_noches;
				$total_cnn 	= $hotel['precio_menor'] * $cant_noches;
				$total_inf 	= $hotel['precio_infante'] * $cant_noches;

				if($hotel['increment_fee'] == 'P')
				{
					$fee 		= 1 + ($hotel['fee']/100);
					$total_sgl 	*= $fee;
					$total_dbl 	*= $fee;
					$total_tpl 	*= $fee;
					$total_cpl 	*= $fee;
					$total_cnn 	*= $fee;
					$total_inf 	*= $fee;
				}
				else
				{
					$fee 		= $hotel['fee'];
					if($hotel['precio_single'] > 0)
						$total_sgl 	+= $fee;
					else
						$total_sgl 	= 0;
					if($hotel['precio_doble'] > 0)
						$total_dbl 	+= $fee;
					else
						$total_dbl 	= 0;
					if($hotel['precio_triple'] > 0)
						$total_tpl 	+= $fee;
					else
						$total_tpl 	= 0;
					if($hotel['precio_cuadruple'] > 0)
						$total_cpl 	+= $fee;
					else
						$total_cpl 	= 0;
					if($hotel['precio_menor'] > 0)
						$total_cnn 	+= $fee;
					else
						$total_cnn 	= 0;
					if($hotel['precio_infante'] > 0)
						$total_inf 	+= $fee;
					else
						$total_inf 	= 0;
				}

				if($hotel['increment_factura'] == 'P')
				{
					$iva 		= 1 + ($hotel['factura']/100);
					$total_sgl 	*= $iva;
					$total_dbl 	*= $iva;
					$total_tpl 	*= $iva;
					$total_cpl 	*= $iva;
					$total_cnn 	*= $iva;
					$total_inf 	*= $iva;
				}
				else
				{
					$iva 		= $hotel['factura'];
					if($hotel['precio_single'] > 0)
						$total_sgl 	+= $iva;
					else
						$total_sgl 	= 0;
					if($hotel['precio_doble'] > 0)
						$total_dbl 	+= $iva;
					else
						$total_dbl 	= 0;
					if($hotel['precio_triple'] > 0)
						$total_tpl 	+= $iva;
					else
						$total_tpl 	= 0;
					if($hotel['precio_cuadruple'] > 0)
						$total_cpl 	+= $iva;
					else
						$total_cpl 	= 0;
					if($hotel['precio_menor'] > 0)
						$total_cnn 	+= $iva;
					else
						$total_cnn 	= 0;
					if($hotel['precio_infante'] > 0)
						$total_inf 	+= $iva;
					else
						$total_inf 	= 0;
				}

				if($hotel['moneda'] == 'B')
				{
					$tc 		= $hotel['tipo_cambio'];
					$total_sgl /= $tc;
					$total_dbl /= $tc;
					$total_tpl /= $tc;
					$total_cpl /= $tc;
					$total_cnn /= $tc;
					$total_inf /= $tc;
				}

				$sgl += ceil($total_sgl);
				$dbl += ceil($total_dbl);
				$tpl += ceil($total_tpl);
				$cpl += ceil($total_cpl);
				$cnn += ceil($total_cnn);
				$inf += ceil($total_inf);

			}

			$a_sgl[$i] = 999999; if($is_sgl == 1) { $opciones[$i]['precio_sgl'] = $sgl; $a_sgl[$i] = $sgl; if(strpos($mostrar_cols, 'sgl') === false) { $mostrar_cols .= 'sgl;'; $cant_cols++; } }
			$a_dbl[$i] = 999999; if($is_dbl == 1) { $opciones[$i]['precio_dbl'] = $dbl; $a_dbl[$i] = $dbl; if(strpos($mostrar_cols, 'dbl') === false) { $mostrar_cols .= 'dbl;'; $cant_cols++; } }
			$a_tpl[$i] = 999999; if($is_tpl == 1) { $opciones[$i]['precio_tpl'] = $tpl; $a_tpl[$i] = $tpl; if(strpos($mostrar_cols, 'tpl') === false) { $mostrar_cols .= 'tpl;'; $cant_cols++; } }
			$a_cpl[$i] = 999999; if($is_cpl == 1) { $opciones[$i]['precio_cpl'] = $cpl; $a_cpl[$i] = $cpl; if(strpos($mostrar_cols, 'cpl') === false) { $mostrar_cols .= 'cpl;'; $cant_cols++; } }
			if($is_cnn == 1) { $opciones[$i]['precio_cnn'] = $cnn; if(strpos($mostrar_cols, 'cnn') === false) { $mostrar_cols .= 'cnn;'; $cant_cols++; } }
			if($is_inf == 1) { $opciones[$i]['precio_inf'] = $inf; if(strpos($mostrar_cols, 'inf') === false) { $mostrar_cols .= 'inf;'; $cant_cols++; } }

			$opciones[$i]['hoteles'] = $hoteles;
		}

		array_multisort($a_sgl, SORT_ASC, $a_dbl, SORT_ASC, $a_tpl, SORT_ASC, $a_cpl, SORT_ASC, $opciones);

		$opciones['col_mostrar'] 	= $mostrar_cols;
		$opciones['cant_cols'] 		= $cant_cols;
		
		return $opciones;
	}

}

?>