<?php

class c_agenteb_reserva {
    function obtener_reservas_agencia($pagina) {
        $agenteb_reserva = new agenteb_reserva;
        $max_pagina = 30;
        return $agenteb_reserva->enviar_reservas_agencia($pagina, $max_pagina);
    }

    function buscar_reservas($tag_busqueda, $pagina) {
        $agenteb_reserva = new agenteb_reserva;
        $max_pagina = 30;
        return $agenteb_reserva->buscar_reservas($tag_busqueda, $pagina, $max_pagina);
    }

    function es_reserva_vendida($idreserva) {
        $agenteb_venta = new agenteb_venta;
        $venta = $agenteb_venta->buscar_venta_por_reserva($idreserva);
        return $venta != 0;
    }

    function obtener_reserva_por_idreserva($idreserva) 
	{
    	$datos_segmento = agenteb_reserva::obtener_segmento_por_idreserva($idreserva);
        $datos_pasajero = agenteb_reserva::obtener_pasajero_por_idreserva($idreserva);
        
        $dato['segmento'] = $datos_segmento;
        $dato['pasajero'] = $datos_pasajero;
        
        return $dato;
    }

    function enviar_datos_agenteb_cliente($idreserva) {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT agenteb_clientes.idagenteb_clientes, nombre, apellido, pasaporte FROM agenteb_clientes, agenteb_reserva
                        WHERE idreserva = '$idreserva' AND agenteb_clientes.idagenteb_clientes = agenteb_reserva.idagenteb_clientes";
        return $controladoraBD->ejecutarconsulta($consulta);
    }
}

?>