<?
	class c_pax_evento
	{
		function obtener_pax_evento_idcotizacion($idevento_cotizado)
		{
			$resp = pax_evento::obtener_pax_evento_idcotizacion($idevento_cotizado);
			return $resp;
		}
		
		function eliminar_pax_evento_cotizado($idpax)
		{
			$class 	= "evento";
			
			$resp 	= boleto_cotizado::obtenet_boleto_cotizado_idpax($idpax,$class);
			for($i = 0; $i < count($resp); $i++)
			{
				boleto_cotizado::eliminar_boleto_cotizado_class_cotizado_has_pax_class($resp[$i]['idboleto_cotizado_evento_cotizado'],$idpax, $class);
			}
			
			$resp_hotel = hotel_cotizado::obtenet_hotel_cotizado_idpax($idpax,$class);
			for($i = 0; $i < count($resp_hotel); $i++)
			{
				hotel_cotizado::eliminar_hotel_cotizado_class_cotizado_has_pax_class($resp_hotel[$i]['iditem_hotel_evento_cotizado'],$idpax, $class);
			}
			
			$resp_otros = otros_cotizado::obtenet_otros_cotizado_idpax($idpax,$class);
			for($i = 0; $i < count($resp_otros); $i++)
			{
				otros_cotizado::eliminar_otros_cotizado_class_cotizado_has_pax_class($resp_otros[$i]['idtarifa_otros_evento_cotizado'],$idpax, $class);
			}
			
			$resp_inscripcion = inscripcion_cotizado::obtenet_inscripcion_cotizado_idpax($idpax);
			for($i = 0; $i < count($resp_inscripcion); $i++)
			{
				inscripcion_cotizado::eliminar_inscripcion_cotizado_has_pax_evento($resp_inscripcion[$i]['idinscripcion_cotizado'],$idpax);
			}
			
			boleto_cotizado::eliminar_pax_class($idpax, $class);
		}
	}
?>