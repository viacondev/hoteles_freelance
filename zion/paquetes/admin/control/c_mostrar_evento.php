<?php

/**
* 
*/
class c_mostrar_evento
{

	function obtener_evento($idevento)
	{
		$evento 	= new evento;
		$mi_evento 	= $evento->obtener_evento($idevento);

		if($mi_evento !== false)
		{
			if(count($mi_evento) > 0)
			{
				return $cotizacion[0];
			}
			return false;
		}
		return false;
	}

	function obtener_tarifas_hotel_por_evento($idevento)
	{
		$tarifa_hotel_evento = new tarifa_hotel;
		$tarifas_hotel = $tarifa_hotel_evento->obtener_tarifas($idevento, 'evento');

		if($tarifas_hotel !== false)
		{
			if(count($tarifas_hotel) > 0)
			{
				for ($i=0; $i < count($tarifas_hotel); $i++) 
				{ 
					$idtarifa = $tarifas_hotel[$i]['idtarifa_hotel_evento'];

					$items = $this->obtener_items_de_tarifa($idtarifa);

					$tarifas_hotel[$i]['opciones'] = $items;
				}
				
				return $tarifas_hotel;
			}
			return false;
		}
		return false;
	}

	function obtener_combinaciones_hoteles($idevento)
	{
		$opcion_hoteles = new opcion_hoteles;
		$opciones 		= $opcion_hoteles->obtener_opcion_hoteles($idevento, 'evento');
		$array_hoteles  = array();

		if($opciones !== false)
		{
			if(count($opciones) > 0)
			{
				for ($i=0; $i < count($opciones); $i++) 
				{ 
					$a_opcion = array();
					$opcion = $opciones[$i];
					$idopc 	= $opcion['idopcion_hoteles_evento'];

					$items = $opcion_hoteles->obtener_opcion_hoteles_detallada($idopc, 'evento');
					$sgl = 0;
					$dbl = 0;
					$tpl = 0;
					$cpl = 0;
					$cnn = 0;
					$inf = 0;
					$hot = "";
					$alm = "";
					$iex = "";
					//echo "<pre>"; print_r($items); echo "</pre>";
					for ($j=0; $j < count($items); $j++) 
					{ 
						$cant_noches = $items[$j]['cant_noches'];
						if($cant_noches <= 0)
						{
							$f_in 	= strtotime($items[$j]['in'] . ' 00:00:00');
							$f_out 	= strtotime($items[$j]['out'] . ' 00:00:00');
							$cant_noches = floor( $f_out - $f_in) / 86400;
						}

						$sgl += ceil($items[$j]['precio_single'] * $cant_noches * (1 + $items[$j]['comision']/100) * (1 + $items[$j]['factura']/100));
						$dbl += ceil($items[$j]['precio_doble'] * $cant_noches * (1 + $items[$j]['comision']/100) * (1 + $items[$j]['factura']/100));
						$tpl += ceil($items[$j]['precio_triple'] * $cant_noches * (1 + $items[$j]['comision']/100) * (1 + $items[$j]['factura']/100));
						$cpl += ceil($items[$j]['precio_cuadruple'] * $cant_noches * (1 + $items[$j]['comision']/100) * (1 + $items[$j]['factura']/100));
						$cnn += ceil($items[$j]['precio_menor'] * $cant_noches * (1 + $items[$j]['comision']/100) * (1 + $items[$j]['factura']/100));
						$inf += ceil($items[$j]['precio_infante'] * $cant_noches * (1 + $items[$j]['comision']/100) * (1 + $items[$j]['factura']/100));

						$hot .= "<a href='http://" . $items[$j]['link'] . "' style='text-decoration:none;'>". strtoupper(nl2br($items[$j]['nombre_hotel'])) . ' - ' . $items[$j]['categoria'] . '* (' . $items[$j]['destino'] . ')</a><br/>';
						if($items[$j]['alimentacion'] != '')
							$alm .= '- ' . $items[$j]['alimentacion'] . '<br/>';
						if($items[$j]['info_extra'] != '')
							$iex .= '- ' . $items[$j]['info_extra'] . '<br/>';
					}
					$a_opcion['hotel'] 				= $hot;
					$a_opcion['precio_single'] 		= $sgl;
					$a_opcion['precio_doble'] 		= $dbl;
					$a_opcion['precio_triple'] 		= $tpl;
					$a_opcion['precio_cuadruple'] 	= $cpl;
					$a_opcion['precio_menor'] 		= $cnn;
					$a_opcion['precio_infante'] 	= $inf;
					$a_opcion['observacion'] 		= $opcion['observacion'];
					$a_opcion['alimentacion'] 		= $alm;
					$a_opcion['info_extra'] 		= $iex;

					$array_hoteles[] = $a_opcion;
				}

				for ($i=0; $i < count($array_hoteles) - 1; $i++) 
				{ 
					for ($j=$i; $j < count($array_hoteles) - 1; $j++) 
					{ 
						$a_opcion1 = $array_hoteles[$j];
						$a_opcion2 = $array_hoteles[$j+1];
						$total1 = $a_opcion1['precio_single'] + $a_opcion1['precio_doble'] + $a_opcion1['precio_triple'] + $a_opcion1['precio_cuadruple']; 
						$total2 = $a_opcion2['precio_single'] + $a_opcion2['precio_doble'] + $a_opcion2['precio_triple'] + $a_opcion2['precio_cuadruple']; 
						if($total2 < $total1)
						{
							$aux = $array_hoteles[$j];
							$array_hoteles[$j] = $array_hoteles[$j+1];
							$array_hoteles[$j+1] = $aux;
						}
					}
				}
				//echo "<pre>"; print_r($array_hoteles); echo "</pre>";
				return $array_hoteles;
			}
		}
		return false;
	}

	function evaluar_vista_hoteles($array_hoteles)
	{
		$cad_respuesta 		= "";
		$exist_single 		= false;
		$exist_doble 		= false;
		$exist_triple 		= false;
		$exist_cuadruple 	= false;
		$exist_menor 		= false;
		$exist_infante 		= false;

		for ($i=0; $i < count($array_hoteles); $i++) 
		{ 
			if($array_hoteles[$i]['precio_single'] != 0)
				$exist_single 		= true;
			if($array_hoteles[$i]['precio_doble'] != 0)
				$exist_doble 		= true;
			if($array_hoteles[$i]['precio_triple'] != 0)
				$exist_triple 		= true;
			if($array_hoteles[$i]['precio_cuadruple'] != 0)
				$exist_cuadruple 	= true;
			if($array_hoteles[$i]['precio_menor'] != 0)
				$exist_menor 		= true;
			if($array_hoteles[$i]['precio_infante'] != 0)
				$exist_infante 		= true;
		}

		if($exist_single)
			$cad_respuesta .= 'SGL/';
		if($exist_doble)
			$cad_respuesta .= 'DBL/';
		if($exist_triple)
			$cad_respuesta .= 'TPL/';
		if($exist_cuadruple)
			$cad_respuesta .= 'CPL/';
		if($exist_menor)
			$cad_respuesta .= 'CNN/';
		if($exist_infante)
			$cad_respuesta .= 'INF/';

		return $cad_respuesta;
	}

	function obtener_items_de_tarifa($idtarifa_hotel_evento)
	{
		$item_hotel_evento 	= new tarifa_hotel;
		$items 				= $item_hotel_evento->obtener_items_por_tarifa_hotel($idtarifa_hotel_evento, 'evento');

		if($items !== false)
		{
			if(count($items) > 0)
			{
				return $items;
			}
			return false;
		}
		return false;
	}

	function obtener_otras_tarifas($idevento)
	{
		$tarifa_otros = new tarifa_otros;
		$tarifas = $tarifa_otros->obtener_tarifa_otros($idevento, 'evento');

		if($tarifas !== false)
		{
			if(count($tarifas) > 0)
			{
				$total_adt = 0;
				$total_cnn = 0;
				$total_inf = 0;
				for ($i=0; $i < count($tarifas); $i++) 
				{ 
					$fee 		= $tarifas[$i]['fee'] / 100;
					$factura 	= $tarifas[$i]['factura'] / 100;
					$precio_adt = $tarifas[$i]['precio_adulto'];
					$precio_cnn = $tarifas[$i]['precio_menor'];
					$precio_inf = $tarifas[$i]['precio_infante'];

					if ($tarifas[$i]['moneda']=='B') 
					{
						$precio_adt 	= $precio_adt / 6.96;
						$precio_cnn 	= $precio_cnn / 6.96;
						$precio_inf 	= $precio_inf / 6.96;	
					}

					$subtotal_adt 	= ceil($precio_adt + ($precio_adt * $fee) + ($precio_adt * $factura));
					$subtotal_cnn   = ceil($precio_cnn + ($precio_cnn * $fee) + ($precio_cnn * $factura));
					$subtotal_inf   = ceil($precio_inf + ($precio_inf * $fee) + ($precio_inf * $factura));

					$tarifas[$i]['subtotal_adt'] = $subtotal_adt;
					$tarifas[$i]['subtotal_cnn'] = $subtotal_cnn;
					$tarifas[$i]['subtotal_inf'] = $subtotal_inf;

					$total_adt += $subtotal_adt;
					$total_cnn += $subtotal_cnn;
					$total_inf += $subtotal_inf;
				}
				$tarifas['total_adt'] 	= $total_adt;
				$tarifas['total_cnn'] 	= $total_cnn;
				$tarifas['total_inf'] 	= $total_inf;
				return $tarifas;
			}
			return false;
		}
		return false;
	}

	function obtener_boletos_cotizados($idevento)
	{
		$boleto_cotizado = new boleto_cotizado;
		$boletos = $boleto_cotizado->obtener_boletos_cotizados($idevento, 'evento');

		if($boletos !== false)
		{
			if(count($boletos) > 0)
			{
				$total_adt = 0;
				$total_cnn = 0;
				$total_inf = 0;
				
				for ($i=0; $i < count($boletos); $i++) 
				{ 
					$fee 			= $boletos[$i]['fee'] / 100;
					$factura 		= $boletos[$i]['factura'] / 100;
					$precio_adt 	= $boletos[$i]['precio_adulto'];
					$precio_cnn 	= $boletos[$i]['precio_menor'];
					$precio_inf 	= $boletos[$i]['precio_infante'];

					if($boletos[$i]['moneda'] == 'B')
					{
						$precio_adt = $precio_adt / 6.96;
						$precio_cnn = $precio_cnn / 6.96;
						$precio_inf = $precio_inf / 6.96;
					}

					$subtotal_adt 	= ceil($precio_adt + ($precio_adt * $fee) + ($precio_adt * $factura));
					$subtotal_cnn   = ceil($precio_cnn + ($precio_cnn * $fee) + ($precio_cnn * $factura));
					$subtotal_inf   = ceil($precio_inf + ($precio_inf * $fee) + ($precio_inf * $factura));

					$boletos[$i]['subtotal_adt'] = $subtotal_adt;
					$boletos[$i]['subtotal_cnn'] = $subtotal_cnn;
					$boletos[$i]['subtotal_inf'] = $subtotal_inf;

					$total_adt += $subtotal_adt;
					$total_cnn += $subtotal_cnn;
					$total_inf += $subtotal_inf;
				}
				$boletos['total_adt'] 	= $total_adt;
				$boletos['total_cnn'] 	= $total_cnn;
				$boletos['total_inf'] 	= $total_inf;
				return $boletos;
			}
			return false;
		}
		return false;
	}

	function obtener_segmentos_publicados($idevento)
	{
		$segmento_publicado = new segmento_publicado;
		$segmentos = $segmento_publicado->obtener_segmentos($idevento, 'evento');

		if($segmentos !== false)
		{
			if(count($segmentos) > 0)
			{
				return $segmentos;
			}
			return false;
		}
		return false;
	}

	private $buffer_lineas_aereas = array();
	function obtener_linea_aerea($idlinea_aerea)
	{
		if(array_key_exists($idlinea_aerea, $this->buffer_lineas_aereas))
		{
			return $this->buffer_lineas_aereas[$idlinea_aerea];
		}
		else
		{
			$linea_aerea = lineas_aereas::obtener_linea_aerea_por_id($idlinea_aerea);
			if($linea_aerea !== false)
			{
				if(count($linea_aerea) > 0)
				{
					$aerolinea = explode(',', $linea_aerea[0]['nombre_linea_aerea']);
					$this->buffer_lineas_aereas[$idlinea_aerea] = strtoupper($aerolinea[0] . '(' . $linea_aerea[0]['codigo_iata_linea_aerea'] . ')');
					return $this->buffer_lineas_aereas[$idlinea_aerea];
				}
				$this->buffer_lineas_aereas[$idlinea_aerea] = 'N/I';
				return 'N/I';
			}
			$this->buffer_lineas_aereas[$idlinea_aerea] = 'N/I';
			return 'N/I';
		}
	}

	private $buffer_aeropuertos = array();
	function obtener_aeropuerto($codigo)
	{
		if(array_key_exists($codigo, $this->buffer_aeropuertos))
		{
			return $this->buffer_aeropuertos[$codigo];
		}
		else
		{
			$aeropuerto = aeropuerto::obtener_aeropuerto_por_codigo($codigo);
			
			if($aeropuerto !== false)
			{
				if(count($aeropuerto) > 0)
				{
					$ciudad = explode(',', $aeropuerto[0]['nombre_aeropuerto']);
					$this->buffer_aeropuertos[$codigo] = strtoupper($ciudad[0] . '(' . $aeropuerto[0]['cod_aeropuerto'] . ')');
					return $this->buffer_aeropuertos[$codigo];
				}
				$this->buffer_aeropuertos[$codigo] = 'N/I';
				return 'N/I';
			}
			$this->buffer_aeropuertos[$codigo] = 'N/I';
			return 'N/I';
		}
	}

	/*
	* BUSCAR SEGUIMIENTOS DEL EVENTO
	*/
	function obtener_seguimientos($codigo)
	{
		$seguimientos = pqt_seguimiento::obtener_seguimientos('E', $codigo);
		if($seguimientos === false)
			$seguimientos = array();
		
		return $seguimientos;
	}
	function obtener_datos_usuario($idusuarios)
	{
		$usuario = usuarios::obtener_datos_usuario($idusuarios);
		if($usuario === false)
			return false;
		if(count($usuario) > 0)
			return $usuario[0];
		else
			return false;
	}

	/*
	* NUEVO FORMATO
	*/
	function obtener_ciudades_de_salida($idpublicacion)
	{
		$ciudades = publicacion::obtener_ciudades_origen_publicacion($idpublicacion);
		if($ciudades === false)
			$ciudades = array();
		return $ciudades;
	}

	function obtener_destinos_de_hotel($idciudad_origen)
	{
		$destinos = tarifa_hotel::obtener_tarifas_hotel_por_ciudad_origen($idciudad_origen);
		if($destinos === false)
			$destinos = array();
		return $destinos;
	}

	function obtener_hoteles_x_destino($idtarifa_hotel)
	{
		$hoteles = tarifa_hotel::obtener_hoteles_x_destino($idtarifa_hotel);
		if($hoteles === false)
			$hoteles = array();
		return $hoteles;
	}

	function evaluar_precios_hotel($precios)
	{
		$sgl = 0;
		$dbl = 0;
		$tpl = 0;
		$cpl = 0;
		$cnn = 0;
		$inf = 0;

		for ($i=0; $i < count($precios); $i++) 
		{ 
			$sgl += $precios[$i]['precio_single'];
			$dbl += $precios[$i]['precio_doble'];
			$tpl += $precios[$i]['precio_triple'];
			$cpl += $precios[$i]['precio_cuadruple'];
			$cnn += $precios[$i]['precio_menor'];
			$inf += $precios[$i]['precio_infante'];	
		}

		$columnas_ocultar = "";
		if($sgl <= 0)
			$columnas_ocultar .= 'SGL/';
		if($dbl <= 0)
			$columnas_ocultar .= 'DBL/';
		if($tpl <= 0)
			$columnas_ocultar .= 'TPL/';
		if($cpl <= 0)
			$columnas_ocultar .= 'CPL/';
		if($cnn <= 0)
			$columnas_ocultar .= 'CNN/';
		if($inf <= 0)
			$columnas_ocultar .= 'INF/';
//echo $columnas_ocultar;
		return $columnas_ocultar;
	}

	function obtener_otros_servicios_x_ciudad($idciudad_origen)
	{
		$otros_servicios = tarifa_otros::obtener_otros_servicios_por_ciudad($idciudad_origen);
		if($otros_servicios === false)
			$otros_servicios = array();
		return $otros_servicios;
	}

	function obtener_nombre_servicio($idotro_servicio)
	{
		$otros_servicios = new pqt_otro_servicio;
		$servicio = $otros_servicios->obtener_otro_servicio($idotro_servicio);
		if($servicio !== false && count($servicio)>0)
			return strtoupper($servicio[0]['nombre_servicio']);
		return 'NO EXISTE SERVICIO';
	}

	function obtener_opcion_boletos_x_ciudad($idciudad_origen)
	{
		$opcion_boletos = boleto_cotizado::obtener_opciones_boletos_x_ciudad_origen($idciudad_origen);
		if($opcion_boletos === false)
			$opcion_boletos = array();
		return $opcion_boletos;
	}

	function obtener_boletos_x_opcion_aereo($idopcion_aereo)
	{
		$boletos = boleto_cotizado::obtener_boletos_x_opcion_boleto($idopcion_aereo);
		if($boletos === false)
			$boletos = array();
		return $boletos;
	}

	function obtener_itinerario_x_opcion_aereo($idopcion_aereo)
	{
		$itinerario = boleto_cotizado::obtener_itinerario_x_opcion_boleto($idopcion_aereo);
		if($itinerario === false)
			$itinerario = array();
		return $itinerario;
	}

	function obtener_evento_por_publicacion($idpublicacion)
	{
		$evento = publicacion::obtener_evento_x_publicacion($idpublicacion);
		if($evento === false)
			$evento = array();
		if(count($evento) == 0)
			$evento[0] = array();
		return $evento[0];
	}

	function obtener_opciones_hotel_combinados($idciudad_origen)
	{
		$opciones = tarifa_hotel::obtener_opcion_hoteles($idciudad_origen);
		if($opciones === false)
			$opciones = array();
		//echo "<pre>"; print_r($opciones); echo "</pre>";
		$mostrar_cols 	= '';
		$cant_cols 		= 0;

		$a_sgl = array();
		$a_dbl = array();
		$a_tpl = array();
		$a_cpl = array();

		for ($i=0; $i < count($opciones); $i++)
		{ 
			$opcion = $opciones[$i];

			$sgl = 0; $is_sgl = 1;
			$dbl = 0; $is_dbl = 1;
			$tpl = 0; $is_tpl = 1;
			$cpl = 0; $is_cpl = 1;
			$cnn = 0; $is_cnn = 1;
			$inf = 0; $is_inf = 1;

			$hoteles = tarifa_hotel::obtener_detalle_opcion_hoteles($opcion['idopcion_hoteles']);
			for ($j=0; $j < count($hoteles); $j++) 
			{ 
				$hotel = $hoteles[$j];

				$cant_noches 	= floor( (strtotime($hotel['fecha_out'] . ' 00:00:00') - strtotime($hotel['fecha_in'] . ' 00:00:00')) / 86400);

				if($is_sgl == 0 || $hotel['precio_single'] <= 0) { $is_sgl=0; $hotel['precio_single'] = 0; }
				if($is_dbl == 0 || $hotel['precio_doble'] <= 0) { $is_dbl=0; $hotel['precio_doble'] = 0; }
				if($is_tpl == 0 || $hotel['precio_triple'] <= 0) { $is_tpl=0; $hotel['precio_triple'] = 0; }
				if($is_cpl == 0 || $hotel['precio_cuadruple'] <= 0) { $is_cpl=0; $hotel['precio_cuadruple'] = 0; }
				if($is_cnn == 0 || $hotel['precio_menor'] <= 0) { $is_cnn=0; $hotel['precio_menor'] = 0; }
				if($is_inf == 0 || $hotel['precio_infante'] <= 0) { $is_inf=0; $hotel['precio_infante'] = 0; }

				$total_sgl 	= $hotel['precio_single'] * $cant_noches;
				$total_dbl 	= $hotel['precio_doble'] * $cant_noches;
				$total_tpl 	= $hotel['precio_triple'] * $cant_noches;
				$total_cpl 	= $hotel['precio_cuadruple'] * $cant_noches;
				$total_cnn 	= $hotel['precio_menor'] * $cant_noches;
				$total_inf 	= $hotel['precio_infante'] * $cant_noches;

				if($hotel['increment_fee'] == 'P')
				{
					$fee 		= 1 + ($hotel['fee']/100);
					$total_sgl 	*= $fee;
					$total_dbl 	*= $fee;
					$total_tpl 	*= $fee;
					$total_cpl 	*= $fee;
					$total_cnn 	*= $fee;
					$total_inf 	*= $fee;
				}
				else
				{
					$fee 		= $hotel['fee'];
					if($hotel['precio_single'] > 0)
						$total_sgl 	+= $fee;
					else
						$total_sgl 	= 0;
					if($hotel['precio_doble'] > 0)
						$total_dbl 	+= $fee;
					else
						$total_dbl 	= 0;
					if($hotel['precio_triple'] > 0)
						$total_tpl 	+= $fee;
					else
						$total_tpl 	= 0;
					if($hotel['precio_cuadruple'] > 0)
						$total_cpl 	+= $fee;
					else
						$total_cpl 	= 0;
					if($hotel['precio_menor'] > 0)
						$total_cnn 	+= $fee;
					else
						$total_cnn 	= 0;
					if($hotel['precio_infante'] > 0)
						$total_inf 	+= $fee;
					else
						$total_inf 	= 0;
				}

				if($hotel['increment_factura'] == 'P')
				{
					$iva 		= 1 + ($hotel['factura']/100);
					$total_sgl 	*= $iva;
					$total_dbl 	*= $iva;
					$total_tpl 	*= $iva;
					$total_cpl 	*= $iva;
					$total_cnn 	*= $iva;
					$total_inf 	*= $iva;
				}
				else
				{
					$iva 		= $hotel['factura'];
					if($hotel['precio_single'] > 0)
						$total_sgl 	+= $iva;
					else
						$total_sgl 	= 0;
					if($hotel['precio_doble'] > 0)
						$total_dbl 	+= $iva;
					else
						$total_dbl 	= 0;
					if($hotel['precio_triple'] > 0)
						$total_tpl 	+= $iva;
					else
						$total_tpl 	= 0;
					if($hotel['precio_cuadruple'] > 0)
						$total_cpl 	+= $iva;
					else
						$total_cpl 	= 0;
					if($hotel['precio_menor'] > 0)
						$total_cnn 	+= $iva;
					else
						$total_cnn 	= 0;
					if($hotel['precio_infante'] > 0)
						$total_inf 	+= $iva;
					else
						$total_inf 	= 0;
				}

				if($hotel['moneda'] == 'B')
				{
					$tc 		= $hotel['tipo_cambio'];
					$total_sgl /= $tc;
					$total_dbl /= $tc;
					$total_tpl /= $tc;
					$total_cpl /= $tc;
					$total_cnn /= $tc;
					$total_inf /= $tc;
				}

				$sgl += ceil($total_sgl);
				$dbl += ceil($total_dbl);
				$tpl += ceil($total_tpl);
				$cpl += ceil($total_cpl);
				$cnn += ceil($total_cnn);
				$inf += ceil($total_inf);

			}

			$a_sgl[$i] = 999999; if($is_sgl == 1) { $opciones[$i]['precio_sgl'] = $sgl; $a_sgl[$i] = $sgl; if(strpos($mostrar_cols, 'sgl') === false) { $mostrar_cols .= 'sgl;'; $cant_cols++; } }
			$a_dbl[$i] = 999999; if($is_dbl == 1) { $opciones[$i]['precio_dbl'] = $dbl; $a_dbl[$i] = $dbl; if(strpos($mostrar_cols, 'dbl') === false) { $mostrar_cols .= 'dbl;'; $cant_cols++; } }
			$a_tpl[$i] = 999999; if($is_tpl == 1) { $opciones[$i]['precio_tpl'] = $tpl; $a_tpl[$i] = $tpl; if(strpos($mostrar_cols, 'tpl') === false) { $mostrar_cols .= 'tpl;'; $cant_cols++; } }
			$a_cpl[$i] = 999999; if($is_cpl == 1) { $opciones[$i]['precio_cpl'] = $cpl; $a_cpl[$i] = $cpl; if(strpos($mostrar_cols, 'cpl') === false) { $mostrar_cols .= 'cpl;'; $cant_cols++; } }
			if($is_cnn == 1) { $opciones[$i]['precio_cnn'] = $cnn; if(strpos($mostrar_cols, 'cnn') === false) { $mostrar_cols .= 'cnn;'; $cant_cols++; } }
			if($is_inf == 1) { $opciones[$i]['precio_inf'] = $inf; if(strpos($mostrar_cols, 'inf') === false) { $mostrar_cols .= 'inf;'; $cant_cols++; } }

			$opciones[$i]['hoteles'] = $hoteles;
		}

		array_multisort($a_sgl, SORT_ASC, $a_dbl, SORT_ASC, $a_tpl, SORT_ASC, $a_cpl, SORT_ASC, $opciones);

		$opciones['col_mostrar'] 	= $mostrar_cols;
		$opciones['cant_cols'] 		= $cant_cols;
		
		return $opciones;
	}

}

?>