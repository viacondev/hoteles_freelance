<?php
/**
* 
*/
class c_crear_evento
{
	
	function obtener_tarifas_hotel_por_evento($idevento)
	{
		$tarifa_hotel_evento = new tarifa_hotel;
		$tarifas_hotel = $tarifa_hotel_evento->obtener_tarifas($idevento, 'evento');

		if($tarifas_hotel !== false)
		{
			if(count($tarifas_hotel) > 0)
			{
				return $tarifas_hotel;
			}
			return false;
		}
		return false;
	}

	function obtener_items_de_tarifa($idtarifa_hotel_evento)
	{
		$item_hotel_evento 	= new tarifa_hotel;
		$items 				= $item_hotel_evento->obtener_items_por_tarifa_hotel($idtarifa_hotel_evento, 'evento');

		if($items !== false)
		{
			if(count($items) > 0)
			{
				return $items;
			}
			return false;
		}
		return false;
	}

	function obtener_otras_tarifas($idevento)
	{
		$tarifa_otros = new tarifa_otros;
		$tarifas = $tarifa_otros->obtener_tarifa_otros($idevento, 'evento');

		if($tarifas !== false)
		{
			if(count($tarifas) > 0)
			{
				return $tarifas;
			}
			return false;
		}
		return false;
	}

	function obtener_nombre_servicio($idotro_servicio)
	{
		$otros_servicios = new pqt_otro_servicio;
		$servicio = $otros_servicios->obtener_otro_servicio($idotro_servicio);
		if($servicio !== false && count($servicio)>0)
			return strtoupper($servicio[0]['nombre_servicio']);
		return 'NO EXISTE SERVICIO';
	}

	/*
	* LOS BOLETOS E ITINERARIO ESTAN AGRUPADOS POR OPCIONES OFRECIDAS AL CLIENTE
	*/

	function obtener_opciones_boletos($idevento)
	{
		$boleto_cotizado = new boleto_cotizado;
		$boletos = $boleto_cotizado->obtener_opciones_boletos($idevento, 'evento');

		if($boletos !== false)
		{
			return $boletos;
		}
		$boletos 	= array();
		return $boletos;	
	}
	function obtener_boletos_de_opcion($idopcion)
	{
		$boleto_cotizado = new boleto_cotizado;
		$boletos = $boleto_cotizado->obtener_boletos_por_opcion($idopcion, 'evento');

		if($boletos !== false)
		{
			return $boletos;
		}
		$boletos 	= array();
		return $boletos;
	}
	function obtener_segmentos_de_opcion($idopcion)
	{
		$segmento_publicado = new segmento_publicado;
		$segmentos = $segmento_publicado->obtener_segmentos_de_opcion($idopcion, 'evento');

		if($segmentos !== false)
		{
			return $segmentos;
		}
		$segmentos = array();
		return $segmentos;
	}

	/***/

	function obtener_boletos_cotizados($idevento)
	{
		$boleto_cotizado = new boleto_cotizado;
		$boletos = $boleto_cotizado->obtener_boletos_cotizados($idevento, 'evento');

		if($boletos !== false)
		{
			if(count($boletos) > 0)
			{
				return $boletos;
			}
			return false;
		}
		return false;
	}

	function obtener_segmentos_publicados($idevento)
	{
		$segmento_publicado = new segmento_publicado;
		$segmentos = $segmento_publicado->obtener_segmentos($idevento, 'evento');

		if($segmentos !== false)
		{
			if(count($segmentos) > 0)
			{
				return $segmentos;
			}
			return false;
		}
		return false;
	}

	private $buffer_lineas_aereas = array();
	function obtener_linea_aerea($idlinea_aerea)
	{
		if(array_key_exists($idlinea_aerea, $this->buffer_lineas_aereas))
		{
			return $this->buffer_lineas_aereas[$idlinea_aerea];
		}
		else
		{
			$linea_aerea = lineas_aereas::obtener_linea_aerea_por_id($idlinea_aerea);
			if($linea_aerea !== false)
			{
				if(count($linea_aerea) > 0)
				{
					$aerolinea = explode(',',$linea_aerea[0]['nombre_linea_aerea']);
					$this->buffer_lineas_aereas[$idlinea_aerea] = strtoupper($aerolinea[0] . '(' . $linea_aerea[0]['codigo_iata_linea_aerea'] . ')');
					return $this->buffer_lineas_aereas[$idlinea_aerea];
				}
				$this->buffer_lineas_aereas[$idlinea_aerea] = 'N/I';
				return 'N/I';
			}
			$this->buffer_lineas_aereas[$idlinea_aerea] = 'N/I';
			return 'N/I';
		}
	}

	private $buffer_aeropuertos = array();
	function obtener_aeropuerto($codigo)
	{
		if(array_key_exists($codigo, $this->buffer_aeropuertos))
		{
			return $this->buffer_aeropuertos[$codigo];
		}
		else
		{
			$aeropuerto = aeropuerto::obtener_aeropuerto_por_codigo($codigo);
			
			if($aeropuerto !== false)
			{
				if(count($aeropuerto) > 0)
				{
					$ciudad = explode(',', $aeropuerto[0]['nombre_aeropuerto']);
					$this->buffer_aeropuertos[$codigo] = strtoupper($ciudad[0] . '(' . $aeropuerto[0]['cod_aeropuerto'] . ')');
					return $this->buffer_aeropuertos[$codigo];
				}
				$this->buffer_aeropuertos[$codigo] = 'N/I';
				return 'N/I';
			}
			$this->buffer_aeropuertos[$codigo] = 'N/I';
			return 'N/I';
		}
	}

	/*
	* TOMANDO EN CUENTA LAS CIUDADES DE SALIDA
	*/

		function obtener_evento_por_publicacion($idpublicacion)
		{
			$evento = publicacion::obtener_evento_x_publicacion($idpublicacion);
			if($evento === false)
				$evento = array();
			if(count($evento) == 0)
				$evento[0] = array();
			return $evento[0];
		}

		function obtener_ciudades_de_salida($idpublicacion)
		{
			$ciudades = publicacion::obtener_ciudades_origen_publicacion($idpublicacion);
			if($ciudades === false)
				$ciudades = array();
			return $ciudades;
		}

		function obtener_destinos_de_hotel($idciudad_origen)
		{
			$destinos = tarifa_hotel::obtener_tarifas_hotel_por_ciudad_origen($idciudad_origen);
			if($destinos === false)
				$destinos = array();
			return $destinos;
		}

		function obtener_hoteles_x_destino($idtarifa_hotel)
		{
			$hoteles = tarifa_hotel::obtener_hoteles_x_destino($idtarifa_hotel);
			if($hoteles === false)
				$hoteles = array();
			return $hoteles;
		}

		function obtener_otros_servicios_x_ciudad($idciudad_origen)
		{
			$otros_servicios = tarifa_otros::obtener_otros_servicios_por_ciudad($idciudad_origen);
			if($otros_servicios === false)
				$otros_servicios = array();
			return $otros_servicios;
		}

		function obtener_opcion_boletos_x_ciudad($idciudad_origen)
		{
			$opcion_boletos = boleto_cotizado::obtener_opciones_boletos_x_ciudad_origen($idciudad_origen);
			if($opcion_boletos === false)
				$opcion_boletos = array();
			return $opcion_boletos;
		}

		function obtener_boletos_x_opcion_aereo($idopcion_aereo)
		{
			$boletos = boleto_cotizado::obtener_boletos_x_opcion_boleto($idopcion_aereo);
			if($boletos === false)
				$boletos = array();
			return $boletos;
		}

		function obtener_itinerario_x_opcion_aereo($idopcion_aereo)
		{
			$itinerario = boleto_cotizado::obtener_itinerario_x_opcion_boleto($idopcion_aereo);
			if($itinerario === false)
				$itinerario = array();
			return $itinerario;
		}
}
?>