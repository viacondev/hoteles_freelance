<?php
    include('../BD/controladoraBD.php');
    include('../entidad/imagen.php');
    //Como no sabemos cuantos archivos van a llegar, iteramos la variable $_FILES
    $idpublicacion  = $_POST['publicacion'];
    
    $ruta   = "../images_paquetes/";
    foreach ($_FILES as $key) 
    {
        if($key['error'] == UPLOAD_ERR_OK )
        {
            $proximo_id = imagen::obtener_ultima_imagen_id() + 1;

            $a_nombre   = explode('.', $key['name']);//Obtenemos el nombre del archivo
            $extension  = end($a_nombre);
            $temporal   = $key['tmp_name']; //Obtenemos el nombre del archivo temporal
            $tamano     = ($key['size'] / 1000)."Kb"; //Obtenemos el tamaño en KB
            $new_name   = 'img_c_' . $proximo_id . '_' . $idpublicacion . '.' . $extension;

            move_uploaded_file($temporal, $ruta . $new_name); 
        
            $registro_imagen = imagen::registrar_nueva_imagen($new_name);
            $idimagen = mysql_insert_id();

            $registro_publicacion = imagen::registrar_publicacion_has_imagen($idpublicacion ,$idimagen);

            echo    "<div class='columns three' id='imagen_contenido_" . $idimagen . "' style='float:left; margin-left:0px; margin-right:10px; position:relative;'>
                        <img src='" . $ruta . $new_name . "' style='width:60px; height: 50px;' /><br/>
                        <img src='images/delete.png' class='icon_delete_image' onclick='eliminar_imagen_multiple(" . $idimagen . ");' >
                        <strong>$new_name</strong>
                    </div>";
        }
        else
        {
            echo $key['error'];
        }
    }
?>