<?php
/**
* 
*/
class c_editar_tarifas_hotel_combinadas
{
	
	function obtener_tarifas_hotel($idciudad_origen)
	{
		$tarifa_hotel = new tarifa_hotel;
		$tarifas_hotel = $tarifa_hotel->obtener_tarifas_hotel_por_ciudad_origen($idciudad_origen);

		if($tarifas_hotel !== false)
		{
			if(count($tarifas_hotel) > 0)
			{
				return $tarifas_hotel;
			}
			return false;
		}
		return false;
	}

	function obtener_items_de_tarifa($idtarifa_hotel)
	{
		$item_hotel 	= new tarifa_hotel;
		$items 			= $item_hotel->obtener_hoteles_x_destino_real_cotizacion($idtarifa_hotel, 0);

		if($items !== false)
		{
			if(count($items) > 0)
			{
				return $items;
			}
			return false;
		}
		return false;
	}

	function obtener_opciones_de_hotel($idciudad_origen)
	{
		$opc_hoteles 	= new tarifa_hotel;
		$opciones 		= $opc_hoteles->obtener_opcion_hoteles($idciudad_origen);

		if($opciones !== false)
		{
			if(count($opciones) > 0)
			{
				return $opciones;
			}
			return false;
		}
		return false;
	}

	function obtener_hoteles_ofertados($idopcion_hoteles)
	{
		$opc_hoteles 	= new tarifa_hotel;
		$opciones 		= $opc_hoteles->obtener_detalle_opcion_hoteles($idopcion_hoteles);

		if($opciones !== false)
		{
			if(count($opciones) > 0)
			{
				return $opciones;
			}
			return false;
		}
		return false;	
	}
}
?>