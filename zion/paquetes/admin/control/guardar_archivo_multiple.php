<?php
    set_time_limit(0);
    include('../BD/controladoraBD.php');
    include('../entidad/archivo_adjunto.php');
    //Como no sabemos cuantos archivos van a llegar, iteramos la variable $_FILES
    $idpublicacion  = $_POST['publicacion'];
    $descripcion    = $_POST['descripcion'];
    $publicar       = $_POST['publicar'];

    if($publicar == '1')
        $pub = 'Pub';
    else
        $pub = 'Int';
    
    $ruta="../adjuntos_paquetes/";
    foreach ($_FILES as $key) 
    {
        if($key['error'] == UPLOAD_ERR_OK )
        {
            $proximo_id = archivo_adjunto::obtener_ultimo_archivo_id() + 1;

            $a_nombre   = explode('.', $key['name']);//Obtenemos el nombre del archivo
            $extension  = end($a_nombre);
            $temporal   = $key['tmp_name']; //Obtenemos el nombre del archivo temporal
            $tamano     = ($key['size'] / 1000)."Kb"; //Obtenemos el tamaño en KB
            $new_name   = 'arch_' . $proximo_id . '_' . $idpublicacion . '.' . $extension;

            move_uploaded_file($temporal, $ruta . $new_name); 
        
            $registro_archivo_adjunto       = archivo_adjunto::registrar_nuevo_archivo_adjunto($new_name, $descripcion);
            $idarchivo_adjunto              = mysql_insert_id();

            $registro_publicacion_archivo   = archivo_adjunto::registrar_publicacion_adjunto($idpublicacion, $idarchivo_adjunto, $publicar);

            echo    "<div class='columns five' id='archivo_adjunto_" . $idarchivo_adjunto . "' >
                        <strong>" . $new_name . " - </strong>
                        " . $descripcion . " - " . $pub . "
                        <img src='images/delete.png' class='icon_delete_image' onclick='eliminar_archivo_multiple(" . $idarchivo_adjunto . "," . $idpublicacion . ");' style='position:relative;' >
                    </div>";
        }
        else
        {
            echo $key['error'];
        }
    }
?>