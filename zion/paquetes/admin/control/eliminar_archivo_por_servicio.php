<?php
	include('../BD/controladoraBD.php');
	include('../entidad/archivo_adjunto.php');

	$ruta  			         = "../adjuntos_paquetes/";
	$idarchivo_adjunto       = $_POST['a'];
    $idservicio              = $_POST['s'];
    $type                    = $_POST['t'];
    
    $dato_adjunto   = archivo_adjunto::obtener_info_adjunto($idarchivo_adjunto);
    
    if($dato_adjunto !== false && count($dato_adjunto)>0)
    {
        $nombre = $dato_adjunto[0]['nombre'];    
    }

    switch ($type) 
    {
        case 'h':
            $eliminacion   = archivo_adjunto::eliminar_archivo_hotel($idarchivo_adjunto, $idservicio);
            break;
        case 'b':
            $eliminacion   = archivo_adjunto::eliminar_archivo_aereo($idarchivo_adjunto, $idservicio);
            break;
        case 's':
            $eliminacion   = archivo_adjunto::eliminar_archivo_servicio($idarchivo_adjunto, $idservicio);
            break;
        case 'i':
            $eliminacion   = archivo_adjunto::eliminar_archivo_inscripcion($idarchivo_adjunto, $idservicio);
            break;
        default:
            # code...
            break;
    }

    $eliminacion_adjunto = archivo_adjunto::eliminar_archivo_adjunto($idarchivo_adjunto);
    
    if($eliminacion !== false && $eliminacion_adjunto !== false)
    {
        if(isset($nombre))
        {
            unlink($ruta . $nombre);    
        }
        
        echo 'CORRECTO';
    }
?>