<?php
    include('../BD/controladoraBD.php');
    include('../entidad/registro_general.php');
    //Como no sabemos cuantos archivos van a llegar, iteramos la variable $_FILES
    $atributo       = $_POST['id'];
    $idpublicacion  = $_POST['publicacion'];

    if($atributo == 'imagen_primera')
        $indicador = "p";
    if($atributo == 'imagen_segunda')
        $indicador = "s";
    
    $ruta="../images_paquetes/";
    foreach ($_FILES as $key) 
    {
        if($key['error'] == UPLOAD_ERR_OK )
        {
            $a_nombre   = explode('.', $key['name']);//Obtenemos el nombre del archivo
            $extension  = end($a_nombre);
            $temporal   = $key['tmp_name']; //Obtenemos el nombre del archivo temporal
            $tamano     = ($key['size'] / 1000)."Kb"; //Obtenemos el tamaño en KB
            $new_name   = 'img_' . $indicador . '_' . $idpublicacion . '.' . $extension;

            move_uploaded_file($temporal, $ruta . $new_name); 

            echo "<img src='" . $ruta . $new_name . "' style='width:50px; height: 50px;' />
            <strong>$new_name</strong>";

            $registro_imagen = registro_general::modificar_atributo_en_tabla('pqt_publicacion', $atributo, $new_name, 'idpublicacion', $idpublicacion);
        }
        else
        {
            echo $key['error'];
        }
    }
?>