<?php
	/**
	* 
	*/
	class pax
	{
		/*
		* REGISTRO DE PASAJEROS
		*/
			function registrar_pax($cotizacion, $idclientes, $idciudad_origen)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= "INSERT INTO pqt_pax(idcotizacion, idclientes, idciudad_origen_paquete) 
									VALUES('$cotizacion', '$idclientes', '$idciudad_origen')";
				
				return $controladoraBD->ejecutarconsulta($consulta);
			}

			function eliminar_pax($idpax)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= "DELETE FROM pqt_pax WHERE idpax=$idpax";
				
				return $controladoraBD->ejecutarconsulta($consulta);	
			}

			function verificar_si_existe_pax($idcotizacion, $idclientes)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= "SELECT * FROM pqt_pax 
									WHERE idcotizacion=$idcotizacion AND idclientes=$idclientes";

				return $controladoraBD->ejecutarconsulta($consulta);
			}	
			
			function obtener_pax_idcotizacion($idcotizacion)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= 	"SELECT P.idpax, P.idclientes, P.idciudad_origen_paquete, O.ciudad, C.trato_cliente, C.nombre_cliente, C.apellido_cliente 
									FROM pqt_pax P, clientes C, pqt_ciudad_origen_paquete O   
									WHERE P.idclientes=C.idclientes AND P.idcotizacion=$idcotizacion AND P.idciudad_origen_paquete=O.idciudad_origen_paquete 
									ORDER BY P.idciudad_origen_paquete";

				return $controladoraBD->ejecutarconsulta($consulta);
			}

			function obtener_pax_detalles_idcotizacion($idcotizacion)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= 	"SELECT P.idpax, 
											P.idclientes, 
											P.idciudad_origen_paquete, 
											O.ciudad, C.trato_cliente, 
											C.nombre_cliente, 
											C.apellido_cliente, 
											C.numero_pasaporte_cliente, 
											C.fecha_vence_pasaporte_cliente, 
											C.fecha_nacimiento_cliente,
											N.nacionalidad, 
											C.nacionalidad_idnacionalidad 
									FROM pqt_pax P, clientes C, pqt_ciudad_origen_paquete O, nacionalidad N   
									WHERE 	P.idclientes=C.idclientes AND 
											P.idcotizacion=$idcotizacion AND 
											P.idciudad_origen_paquete=O.idciudad_origen_paquete AND 
											N.idnacionalidad=C.nacionalidad_idnacionalidad 
									ORDER BY P.idciudad_origen_paquete";

				return $controladoraBD->ejecutarconsulta($consulta);
			}

		/*
		* OBTENCION DE PRECIOS POR PASAJERO
		*/
			function obtener_items_hotel_por_pax($idpax)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= 	"SELECT * 
									FROM pqt_item_hotel_has_pax P, pqt_item_hotel I, pqt_tarifa_hotel T, pqt_hotel H  
									WHERE P.iditem_hotel=I.iditem_hotel AND I.idtarifa_hotel=T.idtarifa_hotel AND I.idhotel=H.idhotel AND P.idpax=$idpax";
								
				return $controladoraBD->ejecutarconsulta($consulta);
			}
			function obtener_servicios_por_pax($idpax)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= 	"SELECT * 
									FROM pqt_tarifa_otros_has_pax P, pqt_tarifa_otros O, pqt_otro_servicio S  
									WHERE S.idotro_servicio=O.idotro_servicio AND O.idtarifa_otros=P.idtarifa_otros AND P.idpax=$idpax";
								
				return $controladoraBD->ejecutarconsulta($consulta);	
			}
			function obtener_boletos_por_pax($idpax)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= 	"SELECT L.nombre_linea_aerea, L.codigo_iata_linea_aerea,P.*, B.* 
									FROM pqt_boleto_cotizado_has_pax P, pqt_boleto_cotizado B, lineas_aereas L  
									WHERE P.idboleto_cotizado=B.idboleto_cotizado AND L.idlineas_aereas=B.idlineas_aereas AND P.idpax=$idpax";
								
				return $controladoraBD->ejecutarconsulta($consulta);	
			}
			function obtener_inscripciones_por_pax($idpax)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= 	"SELECT * 
									FROM pqt_inscripcion_evento_has_pax P, pqt_inscripcion_evento I, pqt_evento E 
									WHERE E.idevento=I.idevento AND P.idinscripcion_evento=I.idinscripcion_evento AND P.idpax=$idpax";
								
				return $controladoraBD->ejecutarconsulta($consulta);	
			}

		/*
		* OBTENCION DE PASAJEROS POR SERVICIO
		*/
			function obtener_paxs_por_item_hotel($iditem_hotel)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= 	"SELECT Px.idpax ,C.nombre_cliente, C.apellido_cliente, P.* 
									FROM pqt_pax Px, pqt_item_hotel_has_pax P, pqt_item_hotel I, clientes C  
									WHERE Px.idclientes=C.idclientes AND Px.idpax=P.idpax AND P.iditem_hotel=I.iditem_hotel AND I.iditem_hotel=$iditem_hotel  
									ORDER BY P.habitacion";
				
				return $controladoraBD->ejecutarconsulta($consulta);	
			}
			function obtener_paxs_segun_servicio($idtarifa_otros)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= 	"SELECT Px.idpax ,C.nombre_cliente, C.apellido_cliente, P.* 
									FROM pqt_pax Px, pqt_tarifa_otros_has_pax P, pqt_tarifa_otros O, clientes C  
									WHERE Px.idclientes=C.idclientes AND Px.idpax=P.idpax AND P.idtarifa_otros=O.idtarifa_otros AND O.idtarifa_otros=$idtarifa_otros";
				
				return $controladoraBD->ejecutarconsulta($consulta);		
			}
			function obtener_paxs_segun_boleto($idboleto_cotizado)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= 	"SELECT Px.idpax ,C.nombre_cliente, C.apellido_cliente, P.* 
									FROM pqt_pax Px, pqt_boleto_cotizado_has_pax P, pqt_boleto_cotizado B, clientes C  
									WHERE Px.idclientes=C.idclientes AND Px.idpax=P.idpax AND P.idboleto_cotizado=B.idboleto_cotizado AND B.idboleto_cotizado=$idboleto_cotizado";
				
				return $controladoraBD->ejecutarconsulta($consulta);		
			}
			function obtener_paxs_segun_boleto_sin_ticket($idboleto_cotizado)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= 	"SELECT Px.idpax ,C.nombre_cliente, C.apellido_cliente, P.* 
									FROM pqt_pax Px, pqt_boleto_cotizado_has_pax P, pqt_boleto_cotizado B, clientes C  
									WHERE Px.idclientes=C.idclientes AND 
											Px.idpax=P.idpax AND 
											P.idboleto_cotizado=B.idboleto_cotizado AND 
											B.idboleto_cotizado=$idboleto_cotizado AND 
											P.numero_ticket IS NULL";
				
				return $controladoraBD->ejecutarconsulta($consulta);
			}
			function obtener_paxs_segun_inscripcion($idinscripcion_evento)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= 	"SELECT Px.idpax ,C.nombre_cliente, C.apellido_cliente, P.* 
									FROM pqt_pax Px, pqt_inscripcion_evento_has_pax P, pqt_inscripcion_evento I, clientes C  
									WHERE Px.idclientes=C.idclientes AND Px.idpax=P.idpax AND P.idinscripcion_evento=I.idinscripcion_evento AND I.idinscripcion_evento=$idinscripcion_evento";
				
				return $controladoraBD->ejecutarconsulta($consulta);
			}

		/*
		* OBTIENE SERVICIOS INDIVIDUALMENTE 
		*/
			function obtener_item_hotel($iditem_hotel)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= 	"SELECT * FROM pqt_item_hotel  
									WHERE iditem_hotel=$iditem_hotel";
				
				return $controladoraBD->ejecutarconsulta($consulta);	
			}
			function obtener_tarifa_otros($idtarifa_otros)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= 	"SELECT * FROM pqt_tarifa_otros  
									WHERE idtarifa_otros=$idtarifa_otros";
				
				return $controladoraBD->ejecutarconsulta($consulta);	
			}
			function obtener_boleto_cotizado($idboleto_cotizado)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= 	"SELECT * FROM pqt_boleto_cotizado  
									WHERE idboleto_cotizado=$idboleto_cotizado";
				
				return $controladoraBD->ejecutarconsulta($consulta);	
			}

		/*
		* REGISTRAR PASAJERO POR SERVICIO
		*/
			function registrar_pax_hotel($iditem_hotel, $idpax, $tipo_precio, $habitacion)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= 	"INSERT INTO pqt_item_hotel_has_pax(iditem_hotel, idpax, tipo_precio, habitacion) 
										VALUES('$iditem_hotel', '$idpax', '$tipo_precio', '$habitacion')";
				
				return $controladoraBD->ejecutarconsulta($consulta);
			}
			function registrar_pax_boleto($idboleto_cotizado, $idpax, $tipo_precio)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= 	"INSERT INTO pqt_boleto_cotizado_has_pax(idboleto_cotizado, idpax, tipo_precio) 
										VALUES('$idboleto_cotizado', '$idpax', '$tipo_precio')";
				
				return $controladoraBD->ejecutarconsulta($consulta);
			}
			function registrar_tkt_pax_boleto($idboleto_cotizado, $idpax, $tkt)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= 	"UPDATE pqt_boleto_cotizado_has_pax 
										SET numero_ticket='$tkt' 
										WHERE idboleto_cotizado=$idboleto_cotizado AND idpax=$idpax";
				
				return $controladoraBD->ejecutarconsulta($consulta);	
			}
			function registrar_pax_servicio($idtarifa_otros, $idpax, $tipo_precio)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= 	"INSERT INTO pqt_tarifa_otros_has_pax(idtarifa_otros, idpax, tipo_precio) 
										VALUES('$idtarifa_otros', '$idpax', '$tipo_precio')";
				
				return $controladoraBD->ejecutarconsulta($consulta);
			}
			function registrar_pax_inscripcion($idinscripcion_evento, $idpax, $tipo_precio)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= 	"INSERT INTO pqt_inscripcion_evento_has_pax(idinscripcion_evento, idpax, tipo_precio) 
										VALUES('$idinscripcion_evento', '$idpax', '$tipo_precio')";
				
				return $controladoraBD->ejecutarconsulta($consulta);
			}

		/*
		* QUITAR PAXS DE LOS SERVICIOS
		*/
			function quitar_pax_hotel($iditem_hotel, $idpax)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= 	"DELETE FROM pqt_item_hotel_has_pax 
										WHERE iditem_hotel=$iditem_hotel AND idpax=$idpax";
				
				return $controladoraBD->ejecutarconsulta($consulta);
			}
			function quitar_pax_boleto($idboleto_cotizado, $idpax)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= 	"DELETE FROM pqt_boleto_cotizado_has_pax 
										WHERE idboleto_cotizado=$idboleto_cotizado AND idpax=$idpax";
				
				return $controladoraBD->ejecutarconsulta($consulta);
			}
			function quitar_pax_servicio($idtarifa_otros, $idpax)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= 	"DELETE FROM pqt_tarifa_otros_has_pax 
										WHERE idtarifa_otros=$idtarifa_otros AND idpax=$idpax";
				
				return $controladoraBD->ejecutarconsulta($consulta);
			}
			function quitar_pax_inscripcion($idinscripcion_evento, $idpax)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= 	"DELETE FROM pqt_inscripcion_evento_has_pax 
										WHERE idinscripcion_evento=$idinscripcion_evento AND idpax=$idpax";
				
				return $controladoraBD->ejecutarconsulta($consulta);
			}

		/*
		* EDICION DE PAXS TICKET
		*/

	}
?>