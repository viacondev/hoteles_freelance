<?php
/**
* 
*/
class paquete_cotizado
{

	function registrar_paquete_cotizado($idpaquete, $paquete_incluye, $importante, $observaciones, $idclientes, $idusuarios, $idsolicitante)
	{
		$controladoraBD = new controladoraBD;
		$consulta = "INSERT INTO paquete_cotizado(idpaquete, fecha_cotizacion, paquete_incluye, importante, observaciones, idclientes, idusuarios, idsolicitante) 
					VALUES('$idpaquete', '" . date('Y-m-d H:i:s') . "', '$paquete_incluye', '$importante', '$observaciones', '$idclientes', '$idusuarios', '$idsolicitante')";

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function obtener_paquete_cotizado($idpaquete_cotizado)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT * FROM paquete_cotizado WHERE idpaquete_cotizado='$idpaquete_cotizado'";

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function buscar_cotizacion_paquete_por_criterios($estado, $tipo_cliente, $nombre, $apellidos, $paquete, $fecha_desde, $fecha_hasta)
	{
		$controladoraBD = new controladoraBD;

		$consulta 		= "SELECT Ec.idpaquete_cotizado, P.titulo, C.nombre_cliente, C.apellido_cliente, Ec.fecha_cotizacion, U.nombre_usuario, Ec.estado_cotizacion 
							FROM publicacion P, paquete E, paquete_cotizado Ec, clientes C, usuarios U 
							WHERE P.idpublicacion=E.idpublicacion AND E.idpublicacion=Ec.idpaquete AND Ec.idclientes=C.idclientes AND Ec.idusuarios=U.idusuarios";

		$consulta       .= " AND P.titulo LIKE('%$paquete%') ";

		if($tipo_cliente == 'natural')
		{
			$consulta 	.= " AND C.tipo_cliente='1' AND C.nombre_cliente LIKE('%$nombre%') AND C.apellido_cliente LIKE('%$apellidos%') "; 
		}
		else
		{
			$consulta 	.= " AND C.tipo_cliente='2' AND C.nombre_cliente LIKE('%$nombre%') ";
		}

		if($estado != 'T')
		{
			$consulta 	.= " AND Ec.estado_cotizacion='$estado' ";
		}

		$consulta .= " ORDER BY Ec.fecha_cotizacion ASC";

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function cambiar_estado_cotizacion($idpaquete_cotizado, $estado)
	{
		$controladoraBD = new controladoraBD;
		$consulta = "UPDATE paquete_cotizado SET estado_cotizacion='$estado' WHERE idpaquete_cotizado=$idpaquete_cotizado";

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function obtener_pasajeros_de_cotizacion($idpaquete_cotizado)
	{
		$controladoraBD = new controladoraBD;
		$consulta		= "SELECT C.idclientes, C.trato_cliente, C.nombre_cliente, C.apellido_cliente, C.numero_pasaporte_cliente, C.fecha_vence_pasaporte_cliente, C.fecha_nacimiento_cliente, C.sexo_cliente, Ep.* 
							FROM paquete_cotizado_has_pasajeros Ep, clientes C 
							WHERE Ep.idclientes=C.idclientes AND Ep.idpaquete_cotizado=$idpaquete_cotizado ";

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function obtener_paquete_cotizado_por_palabra_clave_titulo($palabra, $nombre, $apellido, $estado, $fecha_desde, $fecha_hasta)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.titulo, Pc.idpaquete_cotizado, Pc.fecha_cotizacion, Pc.estado_cotizacion, C.trato_cliente, C.nombre_cliente, C.apellido_cliente, U.nombre_usuario   
                        FROM publicacion Pb, paquete Pq, paquete_cotizado Pc, clientes C, 
                            usuarios U 
                        WHERE Pb.idpublicacion=Pq.idpublicacion AND Pq.idpublicacion=Pc.idpaquete AND Pc.idclientes=C.idclientes AND Pc.idusuarios=U.idusuarios AND 
                                Pb.titulo LIKE('%$palabra%')";
        if($nombre != '')
        	$consulta .= " AND C.nombre_cliente LIKE('%$nombre%')";
        if($apellido != '')
        	$consulta .= " AND C.apellido_cliente LIKE('%$apellido%')";	
        if($estado != '')
        	$consulta .= " AND Pc.estado_cotizacion='$estado'";	
        if($fecha_desde != '' && $fecha_hasta != '')
        	$consulta .= " AND Pc.fecha_cotizacion >= '$fecha_desde' AND Pc.fecha_cotizacion <= '$fecha_hasta'";
        $consulta .= " GROUP BY Pc.idpaquete_cotizado";
        //echo $consulta . "<br/>";
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_paquete_cotizado_por_palabra_clave_tipo_paquete($palabra, $nombre, $apellido, $estado, $fecha_desde, $fecha_hasta)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.titulo, Pc.idpaquete_cotizado, Pc.fecha_cotizacion, Pc.estado_cotizacion, C.trato_cliente, C.nombre_cliente, C.apellido_cliente, U.nombre_usuario   
                        FROM publicacion Pb, paquete Pq, paquete_cotizado Pc, clientes C, 
                             paquete_has_tipo_paquete Ptp, tipo_paquete Tp, 
                            usuarios U 
                        WHERE Pb.idpublicacion=Pq.idpublicacion AND Pq.idpublicacion=Pc.idpaquete AND Pc.idclientes=C.idclientes AND Pc.idusuarios=U.idusuarios
                        		 AND Ptp.idpaquete=Pq.idpublicacion AND Ptp.idtipo_paquete=Tp.idtipo_paquete AND Tp.nombre_tipo LIKE('%$palabra%')";
        if($nombre != '')
        	$consulta .= " AND C.nombre_cliente LIKE('%$nombre%')";
        if($apellido != '')
        	$consulta .= " AND C.apellido_cliente LIKE('%$apellido%')";	
        if($estado != '')
        	$consulta .= " AND Pc.estado_cotizacion='$estado'";	
        if($fecha_desde != '' && $fecha_hasta != '')
        	$consulta .= " AND Pc.fecha_cotizacion >= '$fecha_desde' AND Pc.fecha_cotizacion <= '$fecha_hasta'";
        $consulta .= " GROUP BY Pc.idpaquete_cotizado";
        //echo $consulta . "<br/>";
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_paquete_cotizado_por_palabra_clave_temporada($palabra, $nombre, $apellido, $estado, $fecha_desde, $fecha_hasta)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.titulo, Pc.idpaquete_cotizado, Pc.fecha_cotizacion, Pc.estado_cotizacion, C.trato_cliente, C.nombre_cliente, C.apellido_cliente, U.nombre_usuario   
                        FROM publicacion Pb, paquete Pq, paquete_cotizado Pc, clientes C, 
                            paquete_has_temporada Pht, temporada T, 
                            usuarios U 
                        WHERE Pb.idpublicacion=Pq.idpublicacion AND Pq.idpublicacion=Pc.idpaquete AND Pc.idclientes=C.idclientes AND Pc.idusuarios=U.idusuarios
                        		 AND Pht.idpaquete=Pq.idpublicacion AND Pht.idtemporada=T.idtemporada AND T.nombre LIKE('%$palabra%')";
        if($nombre != '')
        	$consulta .= " AND C.nombre_cliente LIKE('%$nombre%')";
        if($apellido != '')
        	$consulta .= " AND C.apellido_cliente LIKE('%$apellido%')";	
        if($estado != '')
        	$consulta .= " AND Pc.estado_cotizacion='$estado'";	
        if($fecha_desde != '' && $fecha_hasta != '')
        	$consulta .= " AND Pc.fecha_cotizacion >= '$fecha_desde' AND Pc.fecha_cotizacion <= '$fecha_hasta'";
        $consulta .= " GROUP BY Pc.idpaquete_cotizado";
        //echo $consulta . "<br/>";
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_paquete_cotizado_por_palabra_clave_destino($palabra, $nombre, $apellido, $estado, $fecha_desde, $fecha_hasta)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.titulo, Pc.idpaquete_cotizado, Pc.fecha_cotizacion, Pc.estado_cotizacion, C.trato_cliente, C.nombre_cliente, C.apellido_cliente, U.nombre_usuario   
                        FROM publicacion Pb, paquete Pq, paquete_cotizado Pc, clientes C, 
                            paquete_has_destino Phd, destino D, 
                            usuarios U 
                        WHERE Pb.idpublicacion=Pq.idpublicacion AND Pq.idpublicacion=Pc.idpaquete AND Pc.idclientes=C.idclientes AND Pc.idusuarios=U.idusuarios
                        		 AND Phd.idpaquete=Pq.idpublicacion AND Phd.iddestino=D.iddestino AND D.nombre_destino LIKE('%$palabra%')";
        if($nombre != '')
        	$consulta .= " AND C.nombre_cliente LIKE('%$nombre%')";
        if($apellido != '')
        	$consulta .= " AND C.apellido_cliente LIKE('%$apellido%')";	
        if($estado != '')
        	$consulta .= " AND Pc.estado_cotizacion='$estado'";	
        if($fecha_desde != '' && $fecha_hasta != '')
        	$consulta .= " AND Pc.fecha_cotizacion >= '$fecha_desde' AND Pc.fecha_cotizacion <= '$fecha_hasta'";
        $consulta .= " GROUP BY Pc.idpaquete_cotizado";
        //echo $consulta . "<br/>";
        return $controladoraBD->ejecutarconsulta($consulta);
    }

}

?>