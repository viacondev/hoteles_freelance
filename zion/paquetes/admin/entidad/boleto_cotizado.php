<?php
/**
* 
*/
class boleto_cotizado
{
	
	function obtener_boletos_cotizados($id, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT idboleto_cotizado_" . $type . ", 
								CASE moneda 
		 						WHEN 'B' THEN 'BOB' 
		 						WHEN 'U' THEN 'USD' 
		 						END AS nmoneda, moneda, id" . $type . ", orden, ruta, precio_adulto, precio_menor, precio_infante, fee, factura, observacion, linea_aerea, tipo_cambio, estado, codigo_reserva, time_limit 
							FROM boleto_cotizado_" . $type . " T WHERE T.id" . $type . "=$id ORDER BY T.orden, T.idboleto_cotizado_" . $type . " ASC" ;

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function registrar_boleto_cotizado($id, $idopcion_boleto, $orden, $codigo_reserva, $linea_aerea, $ruta, $precio_adulto, $precio_menor, $precio_infante, $fee, $factura, $moneda, $tipo_cambio, $time_limit, $observacion, $estado, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "INSERT INTO boleto_cotizado_" . $type . "(id" . $type . ", idopcion_boleto_" . $type . ", orden, codigo_reserva, linea_aerea, ruta, precio_adulto, precio_menor, precio_infante, fee, factura, moneda, tipo_cambio, time_limit, observacion, estado) 
							VALUES('$id', '$idopcion_boleto', '$orden', '$codigo_reserva', '$linea_aerea', '$ruta', '$precio_adulto', '$precio_menor', '$precio_infante', '$fee', '$factura', '$moneda', '$tipo_cambio', '$time_limit', '$observacion', '$estado')";

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function modificar_boleto_cotizado($id, $orden, $codigo_reserva, $linea_aerea, $ruta, $precio_adulto, $precio_menor, $precio_infante, $fee, $factura, $moneda, $tipo_cambio, $time_limit, $observacion, $estado, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "UPDATE boleto_cotizado_" . $type . " 
							SET orden='$orden', codigo_reserva='$codigo_reserva', linea_aerea='$linea_aerea', ruta='$ruta', precio_adulto='$precio_adulto', precio_menor='$precio_menor', precio_infante='$precio_infante', fee='$fee', factura='$factura', moneda='$moneda', tipo_cambio='$tipo_cambio', time_limit='$time_limit', observacion='$observacion', estado='$estado'  
							WHERE idboleto_cotizado_" . $type . "='$id' ";

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function eliminar_boleto_cotizado($id, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "DELETE FROM boleto_cotizado_" . $type . " 
							WHERE idboleto_cotizado_" . $type . "='$id'";

		return $controladoraBD->ejecutarconsulta($consulta);	
	}
	
	function obtener_boleto_cotizado_idcotizacion($idcotizacion, $class)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT * FROM boleto_cotizado_".$class."_cotizado
							WHERE id".$class."_cotizado = $idcotizacion ORDER BY orden ASC";
		return $controladoraBD->ejecutarconsulta($consulta);	
	}
	
	function obtenet_boleto_cotizado_idpax($idpax, $class)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT pqt_boleto_cotizado_".$class."_cotizado_has_pax_".$class.".* FROM pqt_boleto_cotizado_".$class."_cotizado_has_pax_".$class."
							WHERE pqt_boleto_cotizado_".$class."_cotizado_has_pax_".$class.".idpax_".$class." = $idpax";
		return $controladoraBD->ejecutarconsulta($consulta);
	}
	
	function obtener_boleto_ruta_precio($idboleto_cotizado, $class)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT boleto_cotizado_".$class."_cotizado.* FROM boleto_cotizado_".$class."_cotizado
						WHERE boleto_cotizado_".$class."_cotizado.idboleto_cotizado_".$class."_cotizado = $idboleto_cotizado";
		return $controladoraBD->ejecutarconsulta($consulta);
	}
	
	function registrar_boleto_cotizado_class_cotizado_has_pax_class($idboleto_cotizado, $idpax, $tipo_precio, $class)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "INSERT INTO pqt_boleto_cotizado_".$class."_cotizado_has_pax_".$class."(idboleto_cotizado_".$class."_cotizado, idpax_".$class.", tipo_precio) 
							VALUES('$idboleto_cotizado', '$idpax', '$tipo_precio')";
		return $controladoraBD->ejecutarconsulta($consulta);
	}
	
	function eliminar_boleto_cotizado_class_cotizado_has_pax_class($idboleto_cotizado, $idpax, $class)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "DELETE FROM pqt_boleto_cotizado_".$class."_cotizado_has_pax_".$class." 
							WHERE idboleto_cotizado_".$class."_cotizado='$idboleto_cotizado' and  idpax_".$class."='$idpax'";

		return $controladoraBD->ejecutarconsulta($consulta);	
	}
	
	function eliminar_pax_class($idpax, $class)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "DELETE FROM pqt_pax_".$class." 
							WHERE idpax_".$class."='$idpax'";
		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function obtener_nombres_pasajeros_por_boleto($idboleto, $class)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT C.nombre_cliente, C.apellido_cliente, O.tipo_precio FROM pqt_boleto_cotizado_" . $class . "_cotizado_has_pax_" . $class . " O , pqt_pax_" . $class . " P, clientes C 
							WHERE O.idpax_" . $class . "=P.idpax_" . $class . " AND P.idclientes=C.idclientes AND O.idboleto_cotizado_" . $class . "_cotizado=" . $idboleto;

		return $controladoraBD->ejecutarconsulta($consulta);	
	}
	function obtener_boletos_por_pax($idpax, $class)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT Bp.*, B.*, L.nombre_linea_aerea, L.codigo_iata_linea_aerea FROM pqt_boleto_cotizado_" . $class . "_cotizado_has_pax_" . $class . " Bp, boleto_cotizado_" . $class . "_cotizado B, lineas_aereas L 
							WHERE Bp.idboleto_cotizado_" . $class . "_cotizado=B.idboleto_cotizado_" . $class . "_cotizado AND B.linea_aerea=L.idlineas_aereas AND Bp.idpax_" . $class . "=$idpax";

		return $controladoraBD->ejecutarconsulta($consulta);	
	}
	function registrar_opcion_boleto($id, $salida, $descripcion, $estado, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "INSERT INTO pqt_opcion_boleto_" . $type . "(id" . $type . ", salida, descripcion, estado) VALUES('$id', '$salida', '$descripcion', '$estado')";

		return $controladoraBD->ejecutarconsulta($consulta);
	}
	function modificar_opcion_boleto($id, $salida, $descripcion, $estado, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "UPDATE pqt_opcion_boleto_" . $type . " SET salida='$salida', descripcion='$descripcion', estado='$estado' WHERE idopcion_boleto_" . $type . "='$id'";
//echo $consulta;
		return $controladoraBD->ejecutarconsulta($consulta);
	}
	function eliminar_opcion_boleto($idtarifa, $type)
	{
		/*
		* ELIMINAR BOLETOS
		*/
		$controladoraBD = new controladoraBD;
		$consulta 		= "DELETE FROM boleto_cotizado_" . $type . " WHERE idopcion_boleto_" . $type . "='$idtarifa'";

		$res = $controladoraBD->ejecutarconsulta($consulta);

		if($res !== false)
		{
			/*
			* ELIMINAR ITINERARIO
			*/
			$controladoraBD = new controladoraBD;
			$consulta 		= "DELETE FROM segmento_" . $type . " WHERE idopcion_boleto_" . $type . "='$idtarifa'";

			$res = $controladoraBD->ejecutarconsulta($consulta);

			if($res !== false)
			{
				/*
				* ELIMINAR LA OPCION DE TARIFA BOLETO
				*/
				$controladoraBD = new controladoraBD;
				$consulta 		= "DELETE FROM pqt_opcion_boleto_" . $type . " WHERE idopcion_boleto_" . $type . "='$idtarifa'";

				return $controladoraBD->ejecutarconsulta($consulta);			
			}
			return $res;
		}
		return $res;
	}
	function obtener_opciones_boletos($id, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT * FROM pqt_opcion_boleto_" . $type . " WHERE id" . $type . "='$id'";

		return $controladoraBD->ejecutarconsulta($consulta);
	}
	function obtener_boletos_por_opcion($idopcion, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT idboleto_cotizado_" . $type . ", 
								CASE moneda 
		 						WHEN 'B' THEN 'BOB' 
		 						WHEN 'U' THEN 'USD' 
		 						END AS nmoneda, moneda, id" . $type . ", orden, ruta, precio_adulto, precio_menor, precio_infante, fee, factura, observacion, linea_aerea, tipo_cambio, estado, codigo_reserva, time_limit 
							FROM boleto_cotizado_" . $type . " T WHERE T.idopcion_boleto_" . $type . "='$idopcion' ORDER BY T.orden, T.idboleto_cotizado_" . $type . " ASC" ;

		return $controladoraBD->ejecutarconsulta($consulta);	
	}

	/*
	* NUEVO FORMATO
	*/
	function obtener_opciones_boletos_x_ciudad_origen($idciudad_origen)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT * FROM pqt_opcion_boleto 
							WHERE idciudad_origen_paquete=$idciudad_origen";

		return $controladoraBD->ejecutarconsulta($consulta);
	}
	function obtener_boletos_x_opcion_boleto($idopcion_boleto)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT * FROM pqt_boleto_cotizado 
							WHERE idopcion_boleto=$idopcion_boleto";

		return $controladoraBD->ejecutarconsulta($consulta);	
	}
	function obtener_boletos_x_opcion_boleto_real_cotizacion($idopcion_boleto, $is_real)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT * FROM pqt_boleto_cotizado 
							WHERE idopcion_boleto=$idopcion_boleto AND is_real=$is_real";

		return $controladoraBD->ejecutarconsulta($consulta);	
	}
	function obtener_itinerario_x_opcion_boleto($idopcion_boleto)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT * FROM pqt_segmento 
							WHERE idopcion_boleto=$idopcion_boleto";

		return $controladoraBD->ejecutarconsulta($consulta);
	}
	function registrar_nueva_opcion_boleto($idciudad_origen_paquete, $salida, $descripcion, $estado)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "INSERT INTO pqt_opcion_boleto(idciudad_origen_paquete, salida, descripcion, estado) 
							VALUES('$idciudad_origen_paquete', '$salida', '$descripcion', '$estado')";

		return $controladoraBD->ejecutarconsulta($consulta);
	}
	function modificar_opcion_boleto_x_id($idopcion_boleto, $salida, $descripcion, $estado)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "UPDATE pqt_opcion_boleto SET salida='$salida', descripcion='$descripcion', estado='$estado'  
							WHERE idopcion_boleto='$idopcion_boleto'";

		return $controladoraBD->ejecutarconsulta($consulta);	
	}
	function eliminar_boleto_x_id($idboleto_cotizado)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "DELETE FROM pqt_boleto_cotizado 
							WHERE idboleto_cotizado=$idboleto_cotizado";

		return $controladoraBD->ejecutarconsulta($consulta);
	}
	function registrar_nuevo_boleto_cotizado($idopcion_boleto, $idlineas_aereas, $orden, $codigo_reserva, $ruta, $precio_adulto, $precio_menor, $precio_infante, $fee, $increment_fee, $factura, $increment_factura, $moneda, $tipo_cambio, $time_limit, $observacion, $is_real, $estado)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "INSERT INTO pqt_boleto_cotizado(idopcion_boleto, idlineas_aereas, orden, codigo_reserva, ruta, precio_adulto, precio_menor, precio_infante, fee, increment_fee, factura, increment_factura, moneda, tipo_cambio, time_limit, observacion, is_real, estado) 
							VALUES('$idopcion_boleto', '$idlineas_aereas', '$orden', '$codigo_reserva', '$ruta', '$precio_adulto', '$precio_menor', '$precio_infante', '$fee', '$increment_fee', '$factura', '$increment_factura', '$moneda', '$tipo_cambio', '$time_limit', '$observacion', '$is_real', '$estado')";

		return $controladoraBD->ejecutarconsulta($consulta);
	}
	function modificar_boleto_cotizado_x_id($idboleto_cotizado, $idlineas_aereas, $orden, $codigo_reserva, $ruta, $precio_adulto, $precio_menor, $precio_infante, $fee, $increment_fee, $factura, $increment_factura, $moneda, $tipo_cambio, $time_limit, $observacion, $is_real, $estado)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "UPDATE pqt_boleto_cotizado 
							SET idlineas_aereas='$idlineas_aereas', orden='$orden', codigo_reserva='$codigo_reserva', ruta='$ruta', precio_adulto='$precio_adulto', precio_menor='$precio_menor', precio_infante='$precio_infante', fee='$fee', increment_fee='$increment_fee', factura='$factura', increment_factura='$increment_factura', moneda='$moneda', tipo_cambio='$tipo_cambio', time_limit='$time_limit', observacion='$observacion', is_real='$is_real', estado='$estado' 
							WHERE idboleto_cotizado=$idboleto_cotizado";

		return $controladoraBD->ejecutarconsulta($consulta);
	}
	function obtener_boleto_cotizado_x_id($idboleto_cotizado)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT * FROM pqt_boleto_cotizado B WHERE B.idboleto_cotizado=$idboleto_cotizado";

		return $controladoraBD->ejecutarconsulta($consulta);	
	}
	function eliminar_opcion_boleto_aereo($idopcion_boleto)
	{
		/*
		* ELIMINAR BOLETOS
		*/
		$controladoraBD = new controladoraBD;
		$consulta 		= "DELETE FROM pqt_boleto_cotizado WHERE idopcion_boleto='$idopcion_boleto'";

		$res = $controladoraBD->ejecutarconsulta($consulta);

		if($res !== false)
		{
			/*
			* ELIMINAR ITINERARIO
			*/
			$controladoraBD = new controladoraBD;
			$consulta 		= "DELETE FROM pqt_segmento WHERE idopcion_boleto='$idopcion_boleto'";

			$res = $controladoraBD->ejecutarconsulta($consulta);

			if($res !== false)
			{
				/*
				* ELIMINAR LA OPCION DE TARIFA BOLETO
				*/
				$controladoraBD = new controladoraBD;
				$consulta 		= "DELETE FROM pqt_opcion_boleto WHERE idopcion_boleto='$idopcion_boleto'";

				return $controladoraBD->ejecutarconsulta($consulta);			
			}
			return $res;
		}
		return $res;
	}

	/*
	* REPORTES PARA COTIZACIONES
	*/

		function obtener_lineas_aereas_segun_cotizaciones($idcotizaciones)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "SELECT La.idlineas_aereas, La.codigo_iata_linea_aerea, La.nombre_linea_aerea, Bc.idboleto_cotizado 

								FROM 	lineas_aereas La, 
										pqt_boleto_cotizado Bc, 
										pqt_opcion_boleto Ob, 
										pqt_ciudad_origen_paquete Cop, 
										pqt_cotizacion_has_ciudad_origen Chco 

								WHERE 	La.idlineas_aereas = Bc.idlineas_aereas AND 
										Bc.idopcion_boleto = Ob.idopcion_boleto AND 
										Ob.idciudad_origen_paquete = Cop.idciudad_origen_paquete AND 
										Cop.idciudad_origen_paquete = Chco.idciudad_origen_paquete AND 
										Bc.is_real = 1 AND 
										Chco.idcotizacion IN($idcotizaciones) 

								GROUP BY La.idlineas_aereas 
								ORDER BY La.codigo_iata_linea_aerea";

			return $controladoraBD->ejecutarconsulta($consulta);
		}
		function obtener_boletos_x_linea_aerea($idlineas_aereas, $idcotizaciones)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "SELECT Bc.* 

								FROM 	lineas_aereas La, 
										pqt_boleto_cotizado Bc, 
										pqt_opcion_boleto Ob, 
										pqt_ciudad_origen_paquete Cop, 
										pqt_cotizacion_has_ciudad_origen Chco 

								WHERE 	La.idlineas_aereas = Bc.idlineas_aereas AND 
										Bc.idopcion_boleto = Ob.idopcion_boleto AND 
										Ob.idciudad_origen_paquete = Cop.idciudad_origen_paquete AND 
										Cop.idciudad_origen_paquete = Chco.idciudad_origen_paquete AND 
										Chco.idcotizacion IN($idcotizaciones) AND 
										Bc.is_real = 1 AND 
										La.idlineas_aereas = $idlineas_aereas";

			return $controladoraBD->ejecutarconsulta($consulta);	
		}

		function obtener_id_boletos_por_pax($idpax)
		{
			$controladoraBD = new controladoraBD;
			$consulta = "SELECT B.* 

							FROM pqt_boleto_cotizado_has_pax B  

							WHERE B.idpax='$idpax'";
			//echo $consulta;
			return $controladoraBD->ejecutarconsulta($consulta);
		}
		function obtener_pasajeros_por_boleto_cotizado($idboleto_cotizado)
		{
			$controladoraBD = new controladoraBD;
			$consulta = "SELECT P.idcotizacion, C.nombre_cliente, C.apellido_cliente, C.numero_pasaporte_cliente, C.fecha_nacimiento_cliente, Bchp.tipo_precio 

							FROM pqt_pax P, pqt_boleto_cotizado_has_pax Bchp, clientes C 

							WHERE 	P.idclientes = C.idclientes AND 
									P.idpax = Bchp.idpax AND 
									Bchp.idboleto_cotizado='$idboleto_cotizado'";
			//echo $consulta;
			return $controladoraBD->ejecutarconsulta($consulta);
		}
	
}

?>