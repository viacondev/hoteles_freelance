<?php

class clientes 
{

    function enviar_datos_basicos_cliente($idclientes) 
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT trato_cliente, nombre_cliente, apellido_cliente FROM clientes
                        WHERE idclientes = '$idclientes'";
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_clientes_por_nombre_apellido_pasaporte($nombre_cliente, $apellido_cliente, $pasaporte, $tipo_cliente)
    {
    	$controladoraBD = new controladoraBD;
    	if($tipo_cliente == 1)
    	{
    		$consulta 		=  "SELECT idclientes ,trato_cliente, nombre_cliente, apellido_cliente, numero_pasaporte_cliente FROM clientes
                        	WHERE nombre_cliente LIKE('%$nombre_cliente%') AND apellido_cliente LIKE('%$apellido_cliente%') AND tipo_cliente=1";
                            
            if($pasaporte != '')
                $consulta .= " AND numero_pasaporte_cliente LIKE('%$pasaporte%')";
    	}
    	else
    	{
    		$consulta 		=  "SELECT idclientes ,trato_cliente, nombre_cliente, apellido_cliente, numero_pasaporte_cliente FROM clientes
                        	WHERE nombre_cliente LIKE('%$nombre_cliente%') AND (tipo_cliente=2 OR tipo_cliente=3)";	
    	}
                        	//echo $consulta;
        return $controladoraBD->ejecutarconsulta($consulta);	
    }
    function obtener_paxs_por_nombre_apellido_pasaporte($nombre_cliente, $apellido_cliente, $pasaporte)
    {
        $controladoraBD = new controladoraBD;
        $consulta       =  "SELECT idclientes ,trato_cliente, nombre_cliente, apellido_cliente, numero_pasaporte_cliente, fecha_vence_pasaporte_cliente, fecha_nacimiento_cliente, sexo_cliente FROM clientes
                                WHERE nombre_cliente LIKE('%$nombre_cliente%') AND apellido_cliente LIKE('%$apellido_cliente%') AND tipo_cliente=1";

        if($pasaporte != '')
            $consulta .= " AND numero_pasaporte_cliente LIKE('%$pasaporte%')";

        $consulta .=  " LIMIT 20";

        return $controladoraBD->ejecutarconsulta($consulta);       
    }
    function obtener_clientes_relacionados_con_cliente($idclientes)
    {
        $controladoraBD = new controladoraBD;
        $consulta       = "SELECT C.idclientes ,C.trato_cliente, C.nombre_cliente, C.apellido_cliente, C.numero_pasaporte_cliente 
                            FROM clientes C, clientes_has_clientes Cc 
                            WHERE C.idclientes=Cc.idclientes AND Cc.idclientes_relacion=$idclientes";

        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_empleados_relacionados_con_empresa($idempresa)
    {
        $controladoraBD = new controladoraBD;
        $consulta       = "SELECT C.idclientes ,C.trato_cliente, C.nombre_cliente, C.apellido_cliente, C.numero_pasaporte_cliente 
                            FROM clientes C, clientes_has_clientes_contacto Cc 
                            WHERE Cc.idclientes_relacion=C.idclientes AND Cc.idclientes=$idempresa";

        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function buscar_clientes_por_rubro($idrubro)
    {
        $controladoraBD = new controladoraBD;
        $consulta       = "SELECT C.idclientes ,C.trato_cliente, C.nombre_cliente, C.apellido_cliente, C.codigo_cliente FROM clientes C, clientes_has_rubro Cr WHERE C.idclientes=Cr.clientes_idclientes AND Cr.rubro_idrubro='$idrubro'";

        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function buscar_clientes_por_profesion($idprofesiones)
    {
        $controladoraBD = new controladoraBD;
        $consulta       = "SELECT C.idclientes ,C.trato_cliente, C.nombre_cliente, C.apellido_cliente, C.codigo_cliente FROM clientes C, clientes_has_profesion Cp WHERE C.idclientes=Cp.clientes_idclientes AND Cp.profesion_idprofesiones='$idprofesiones'";

        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function buscar_clientes_por_especialidad($idespecialidades)
    {
        $controladoraBD = new controladoraBD;
        $consulta       = "SELECT C.idclientes ,C.trato_cliente, C.nombre_cliente, C.apellido_cliente, C.codigo_cliente FROM clientes C, clientes_has_profesion Cp WHERE C.idclientes=Cp.clientes_idclientes AND Cp.especialidades_idespecialidades='$idespecialidades'";

        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function buscar_clientes_por_preferencia($idpreferencias)
    {
        $controladoraBD = new controladoraBD;
        $consulta       = "SELECT C.idclientes ,C.trato_cliente, C.nombre_cliente, C.apellido_cliente, C.codigo_cliente FROM clientes C, clientes_has_preferencias Cp WHERE C.idclientes=Cp.clientes_idclientes AND Cp.preferencias_idpreferencias='$idpreferencias'";

        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function buscar_todos_clientes()
    {
        $controladoraBD = new controladoraBD;
        $consulta       = "SELECT C.idclientes ,C.trato_cliente, C.nombre_cliente, C.apellido_cliente, C.codigo_cliente FROM clientes C";

        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function buscar_clientes_por_nombre($nombre, $apellido)
    {
        $controladoraBD = new controladoraBD;
        $consulta       = "SELECT C.idclientes ,C.trato_cliente, C.nombre_cliente, C.apellido_cliente, C.codigo_cliente FROM clientes C WHERE C.nombre_cliente LIKE('%$nombre%') ";
        if($apellido != '')
        {
            $consulta .= " AND C.apellido_cliente LIKE('%$apellido%')";
        }

        return $controladoraBD->ejecutarconsulta($consulta);   
    }

    /*
    * CONSULTAS DE ALGUNAS TABLAS PARA SELECCION MULTIPLE
    */
    function obtener_especialidades()
    {
        $controladoraBD = new controladoraBD;
        $consulta       = "SELECT * FROM especialidades";

        return $controladoraBD->ejecutarconsulta($consulta);   
    }
    function obtener_especialidad($idespecialidades)
    {
        $controladoraBD = new controladoraBD;
        $consulta       = "SELECT * FROM especialidades WHERE idespecialidades='$idespecialidades'";

        return $controladoraBD->ejecutarconsulta($consulta);      
    }
    function obtener_rubros()
    {
        $controladoraBD = new controladoraBD;
        $consulta       = "SELECT * FROM rubro";

        return $controladoraBD->ejecutarconsulta($consulta);   
    }
    function obtener_rubro($idrubro)
    {
        $controladoraBD = new controladoraBD;
        $consulta       = "SELECT * FROM rubro WHERE idrubro='$idrubro'";

        return $controladoraBD->ejecutarconsulta($consulta);      
    }
    function obtener_profesiones()
    {
        $controladoraBD = new controladoraBD;
        $consulta       = "SELECT * FROM profesiones";

        return $controladoraBD->ejecutarconsulta($consulta);      
    }
    function obtener_profesion($idprofesion)
    {
        $controladoraBD = new controladoraBD;
        $consulta       = "SELECT * FROM profesiones WHERE idprofesiones='$idprofesion'";

        return $controladoraBD->ejecutarconsulta($consulta);      
    }
    function buscar_cliente($idclientes)
    {
        $controladoraBD = new controladoraBD;
        $consulta       = "SELECT C.idclientes ,C.trato_cliente, C.nombre_cliente, C.apellido_cliente, C.codigo_cliente FROM clientes C WHERE C.idclientes='$idclientes'";

        return $controladoraBD->ejecutarconsulta($consulta);         
    }
    function obtener_correos_de_cliente($idclientes)
    {
        $controladoraBD = new controladoraBD;
        $consulta       = "SELECT * FROM e_mails WHERE clientes_idclientes='$idclientes'";

        return $controladoraBD->ejecutarconsulta($consulta);            
    }
    function obtener_telefonos_de_cliente($idclientes)
    {
        $controladoraBD = new controladoraBD;
        $consulta       = "SELECT * FROM telefono WHERE clientes_idclientes='$idclientes'";

        return $controladoraBD->ejecutarconsulta($consulta);   
    }

}

?>