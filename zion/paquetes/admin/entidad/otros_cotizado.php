<?
	class otros_cotizado
	{
		function obtener_otros_cotizado_idcotizacion($idcotizacion,$class)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "SELECT tarifa_otros_".$class."_cotizado.*, pqt_otro_servicio.nombre_servicio FROM tarifa_otros_".$class."_cotizado, pqt_otro_servicio 
								WHERE tarifa_otros_".$class."_cotizado.id".$class."_cotizado = $idcotizacion AND tarifa_otros_".$class."_cotizado.idotro_servicio = pqt_otro_servicio.idotro_servicio";
			return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		
		function registrar_otros_cotizado_class_cotizado_has_pax_class($idotros_cotizado, $idpax, $tipo_precio, $class)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "INSERT INTO pqt_tarifa_otros_".$class."_cotizado_has_pax_".$class."(idtarifa_otros_".$class."_cotizado,idpax_".$class.",tipo_precio) 
								VALUES('$idotros_cotizado', '$idpax', '$tipo_precio')";
			return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function obtenet_otros_cotizado_idpax($idpax, $class)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "SELECT pqt_tarifa_otros_".$class."_cotizado_has_pax_".$class.".* FROM pqt_tarifa_otros_".$class."_cotizado_has_pax_".$class."
								WHERE pqt_tarifa_otros_".$class."_cotizado_has_pax_".$class.".idpax_".$class." = $idpax";
			return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function obtener_otros_nombre_precio($idotros_cotizado, $class)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "SELECT tarifa_otros_".$class."_cotizado.* , pqt_otro_servicio.nombre_servicio FROM tarifa_otros_".$class."_cotizado, pqt_otro_servicio 
								WHERE tarifa_otros_".$class."_cotizado.idtarifa_otros_".$class."_cotizado = $idotros_cotizado AND tarifa_otros_".$class."_cotizado.idotro_servicio=pqt_otro_servicio.idotro_servicio";
								//echo $consulta;
			return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function eliminar_otros_cotizado_class_cotizado_has_pax_class($idotros_cotizado, $idpax, $class)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "DELETE FROM pqt_tarifa_otros_".$class."_cotizado_has_pax_".$class."
								WHERE idtarifa_otros_".$class."_cotizado = $idotros_cotizado AND idpax_".$class." = $idpax";
	
			return $controladoraBD->ejecutarconsulta($consulta);	
		}	
		function obtener_nombres_pasajeros_por_servicio($idotros_cotizado, $class)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "SELECT C.nombre_cliente, C.apellido_cliente, O.tipo_precio FROM pqt_tarifa_otros_" . $class . "_cotizado_has_pax_" . $class . " O , pqt_pax_" . $class . " P, clientes C 
								WHERE O.idpax_" . $class . "=P.idpax_" . $class . " AND P.idclientes=C.idclientes AND O.idtarifa_otros_" . $class . "_cotizado=" . $idotros_cotizado;
	
			return $controladoraBD->ejecutarconsulta($consulta);	
		}
		function obtener_otros_servicios_por_pax($idpax, $class)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "SELECT Po.*, T.*, O.* FROM pqt_tarifa_otros_" . $class . "_cotizado_has_pax_" . $class . " Po , tarifa_otros_" . $class . "_cotizado T, pqt_otro_servicio O 
								WHERE Po.idtarifa_otros_" . $class . "_cotizado=T.idtarifa_otros_" . $class . "_cotizado AND T.idotro_servicio=O.idotro_servicio AND Po.idpax_" . $class . "=$idpax";
	
			return $controladoraBD->ejecutarconsulta($consulta);		
		}
	}
?>