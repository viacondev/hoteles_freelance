<?php
/**
* 
*/
class inscripcion_evento
{
	
	function obtener_inscripciones_por_cotizacion($idcotizacion)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT * 
							FROM pqt_cotizacion_has_inscripcion_evento Ie, pqt_inscripcion_evento I, pqt_evento E 
							WHERE I.idevento=E.idevento AND I.idinscripcion_evento=Ie.idinscripcion_evento AND Ie.idcotizacion=$idcotizacion" ;

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function registrar_inscripcion_evento($idevento, $categoria, $precio, $moneda, $fee, $increment_fee, $factura, $increment_factura, $observacion, $time_limit, $estado, $tipo_cambio)
	{
		$controladoraBD = new controladoraBD;
		$consulta		= "INSERT INTO pqt_inscripcion_evento(idevento, categoria, precio, moneda, fee, increment_fee, factura, increment_factura, observacion, time_limit, estado, tipo_cambio) 
							VALUES('$idevento', '$categoria', '$precio', '$moneda', '$fee', '$increment_fee', '$factura', '$increment_factura', '$observacion', '$time_limit', '$estado', '$tipo_cambio')";
		
		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function registrar_inscripcion_en_cotizacion($idcotizacion, $idinscripcion_evento)
	{
		$controladoraBD = new controladoraBD;
		$consulta		= "INSERT INTO pqt_cotizacion_has_inscripcion_evento(idcotizacion, idinscripcion_evento) 
							VALUES('$idcotizacion', '$idinscripcion_evento')";
		
		return $controladoraBD->ejecutarconsulta($consulta);	
	}

	function modificar_inscripcion_evento_por_id($idinscripcion_evento ,$idevento, $categoria, $precio, $moneda, $fee, $increment_fee, $factura, $increment_factura, $observacion, $time_limit, $estado, $tipo_cambio)
	{
		$controladoraBD = new controladoraBD;
		$consulta		= "UPDATE pqt_inscripcion_evento 
							SET idevento='$idevento', categoria='$categoria', precio='$precio', moneda='$moneda', fee='$fee', increment_fee='$increment_fee', factura='$factura', increment_factura='$increment_factura', observacion='$observacion', time_limit='$time_limit', estado='$estado', tipo_cambio='$tipo_cambio'  
							WHERE idinscripcion_evento=$idinscripcion_evento";
		
		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function quitar_inscripcion_de_cotizacion($idcotizacion, $idinscripcion_evento)
	{
		$controladoraBD = new controladoraBD;
		$consulta		= "DELETE FROM pqt_cotizacion_has_inscripcion_evento 
							WHERE idcotizacion=$idcotizacion AND idinscripcion_evento=$idinscripcion_evento";
		
		return $controladoraBD->ejecutarconsulta($consulta);		
	}

	function eliminar_inscripcion_evento($idinscripcion_evento)
	{
		$controladoraBD = new controladoraBD;
		$consulta		= "DELETE FROM pqt_inscripcion_evento 
							WHERE idinscripcion_evento=$idinscripcion_evento";
		
		return $controladoraBD->ejecutarconsulta($consulta);
	}

	/*
	* REPORTES PARA COTIZACIONES
	*/
		function obtener_eventos_segun_cotizaciones($cotizaciones)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "SELECT E.idevento, E.nombre_evento, E.sigla_evento

								FROM 	pqt_evento E, 
										pqt_inscripcion_evento Ie, 
										pqt_cotizacion_has_inscripcion_evento Chie 

								WHERE 	E.idevento=Ie.idevento AND 
										Ie.idinscripcion_evento=Chie.idinscripcion_evento AND 
										Chie.idcotizacion IN($cotizaciones)

								GROUP BY E.idevento, E.nombre_evento, E.sigla_evento";

			return $controladoraBD->ejecutarconsulta($consulta);
		}
		function obtener_inscripciones_por_evento($idevento, $cotizaciones)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "SELECT Ie.* 

								FROM 	pqt_inscripcion_evento Ie, 
										pqt_cotizacion_has_inscripcion_evento Chie 

								WHERE 	Ie.idinscripcion_evento=Chie.idinscripcion_evento AND 
										Chie.idcotizacion IN($cotizaciones) AND 
										Ie.idevento=$idevento";

			return $controladoraBD->ejecutarconsulta($consulta);	
		}

		function obtener_id_inscripciones_por_pax($idpax)
		{
			$controladoraBD = new controladoraBD;
			$consulta = "SELECT Ie.* 

							FROM pqt_inscripcion_evento_has_pax Ie  

							WHERE Ie.idpax='$idpax'";
			//echo $consulta;
			return $controladoraBD->ejecutarconsulta($consulta);
		}

		function obtener_pasajeros_por_inscripcion_evento($idinscripcion_evento)
		{
			$controladoraBD = new controladoraBD;
			$consulta = "SELECT P.idcotizacion, C.idclientes, C.nombre_cliente, C.apellido_cliente, C.numero_pasaporte_cliente, C.fecha_nacimiento_cliente 

							FROM pqt_pax P, pqt_inscripcion_evento_has_pax Iehp, clientes C 

							WHERE 	P.idclientes = C.idclientes AND 
									P.idpax = Iehp.idpax AND 
									Iehp.idinscripcion_evento = '$idinscripcion_evento'";
			//echo $consulta;
			return $controladoraBD->ejecutarconsulta($consulta);
		}

}	
?>