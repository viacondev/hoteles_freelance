<?php
/**
* 
*/
class ciudad_origen_paquete
{
	function registrar_ciudad_origen_paquete($ciudad, $fecha_salida, $fecha_retorno)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "INSERT INTO pqt_ciudad_origen_paquete(ciudad, fecha_salida, fecha_retorno, paquete_incluye, importante, datos_agente) 
							VALUES('$ciudad', '$fecha_salida', '$fecha_retorno', '', '', '')";

		return $controladoraBD->ejecutarconsulta($consulta);
	}
	
	function registrar_ciudad_origen_paquete_generico($idciudad_origen, $publicacion)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "INSERT INTO pqt_publicacion_has_ciudad_origen(idpublicacion, idciudad_origen_paquete) 
							VALUES('$publicacion', '$idciudad_origen')";	

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function registrar_ciudad_origen_paquete_cotizacion($idciudad_origen, $cotizacion)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "INSERT INTO pqt_cotizacion_has_ciudad_origen(idcotizacion, idciudad_origen_paquete) 
							VALUES('$cotizacion', '$idciudad_origen')";	

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function eliminar_ciudad_origen_paquete_generico($idciudad_origen, $idpublicacion)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "DELETE FROM pqt_publicacion_has_ciudad_origen 
		 					WHERE idpublicacion='$idpublicacion' AND idciudad_origen_paquete='$idciudad_origen'";	

		return $controladoraBD->ejecutarconsulta($consulta);	
	}

	function eliminar_ciudad_origen_paquete_cotizacion($idciudad_origen, $cotizacion)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "DELETE FROM pqt_cotizacion_has_ciudad_origen 
		 					WHERE idcotizacion='$cotizacion' AND idciudad_origen_paquete='$idciudad_origen'";	

		return $controladoraBD->ejecutarconsulta($consulta);	
	}

	function eliminar_ciudad_origen_paquete($idciudad_origen)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "DELETE FROM pqt_ciudad_origen_paquete 
		 					WHERE idciudad_origen_paquete='$idciudad_origen'";	

		return $controladoraBD->ejecutarconsulta($consulta);	
	}

	function obtener_ciudad_origen_paquete_de_cotizacion($idcotizacion)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT Cot.idciudad_origen_paquete, Cd.ciudad 
							FROM pqt_ciudad_origen_paquete Cd, pqt_cotizacion_has_ciudad_origen Cot 
							WHERE Cot.idciudad_origen_paquete=Cd.idciudad_origen_paquete AND Cot.idcotizacion=$idcotizacion";

		return $controladoraBD->ejecutarconsulta($consulta);	
	}

	function obtener_ciudad_origen_paquete_de_idpublicacion($idpublicacion)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT pqt_ciudad_origen_paquete.* FROM pqt_ciudad_origen_paquete
							INNER JOIN pqt_publicacion_has_ciudad_origen ON pqt_ciudad_origen_paquete.idciudad_origen_paquete = pqt_publicacion_has_ciudad_origen.idciudad_origen_paquete
							WHERE pqt_publicacion_has_ciudad_origen.idpublicacion = '$idpublicacion'";
		return $controladoraBD->ejecutarconsulta($consulta);	
	}
}
?>