<?php
/**
* 
*/
class evento_cotizado
{

	function registrar_evento_cotizado($idevento, $paquete_incluye, $importante, $observaciones, $idclientes, $idusuarios, $idsolicitante)
	{
		$controladoraBD = new controladoraBD;
		$consulta = "INSERT INTO evento_cotizado(idevento, fecha_cotizacion, paquete_incluye, importante, observaciones, idclientes, idusuarios, idsolicitante) 
					VALUES('$idevento', '" . date('Y-m-d H:i:s') . "', '$paquete_incluye', '$importante', '$observaciones', '$idclientes', '$idusuarios', '$idsolicitante')";

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function obtener_evento_cotizado($idevento_cotizado)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT * FROM evento_cotizado WHERE idevento_cotizado='$idevento_cotizado'";

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function buscar_cotizacion_evento_por_criterios($estado, $tipo_cliente, $nombre, $apellidos, $evento, $fecha_desde, $fecha_hasta)
	{
		$controladoraBD = new controladoraBD;

		$consulta 		= "SELECT Ec.idevento_cotizado, P.titulo, C.nombre_cliente, C.apellido_cliente, Ec.fecha_cotizacion, U.nombre_usuario, Ec.estado_cotizacion 
							FROM publicacion P, evento E, evento_cotizado Ec, clientes C, usuarios U 
							WHERE P.idpublicacion=E.idpublicacion AND E.idpublicacion=Ec.idevento AND Ec.idclientes=C.idclientes AND Ec.idusuarios=U.idusuarios";

		$consulta       .= " AND (P.titulo LIKE('%$evento%') OR E.nombre_generico LIKE('%$evento%') )";

		if($tipo_cliente == 'natural')
		{
			$consulta 	.= " AND C.tipo_cliente='1' AND C.nombre_cliente LIKE('%$nombre%') AND C.apellido_cliente LIKE('%$apellidos%') "; 
		}
		else
		{
			$consulta 	.= " AND C.tipo_cliente='2' AND C.nombre_cliente LIKE('%$nombre%') ";
		}

		if($estado != 'T')
		{
			$consulta 	.= " AND Ec.estado_cotizacion='$estado' ";
		}

		$consulta .= " ORDER BY Ec.fecha_cotizacion ASC";

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function cambiar_estado_cotizacion($idevento_cotizado, $estado)
	{
		$controladoraBD = new controladoraBD;
		$consulta = "UPDATE evento_cotizado SET estado_cotizacion='$estado' WHERE idevento_cotizado=$idevento_cotizado";

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function obtener_pasajeros_de_cotizacion($idevento_cotizado)
	{
		$controladoraBD = new controladoraBD;
		$consulta		= "SELECT C.idclientes, C.trato_cliente, C.nombre_cliente, C.apellido_cliente, C.numero_pasaporte_cliente, C.fecha_vence_pasaporte_cliente, C.fecha_nacimiento_cliente, C.sexo_cliente, Ep.* 
							FROM evento_cotizado_has_pasajeros Ep, clientes C 
							WHERE Ep.idclientes=C.idclientes AND Ep.idevento_cotizado=$idevento_cotizado ";

		return $controladoraBD->ejecutarconsulta($consulta);
	}
	function obtener_evento_cotizado_por_palabra_clave_titulo($palabra, $nombre, $apellido, $estado, $fecha_desde, $fecha_hasta)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.titulo, Ev.nombre_generico, Ec.idevento_cotizado, Ec.fecha_cotizacion, Ec.estado_cotizacion, C.trato_cliente, C.nombre_cliente, C.apellido_cliente, U.nombre_usuario 
                        FROM publicacion Pb, evento Ev, evento_cotizado Ec, clientes C, 
                            usuarios U 
                        WHERE Pb.idpublicacion=Ev.idpublicacion AND Ev.idpublicacion=Ec.idevento AND Ec.idclientes=C.idclientes AND Ec.idusuarios=U.idusuarios AND (
                                Pb.titulo LIKE('%$palabra%') OR Ev.nombre_generico LIKE('%$palabra%') OR Ev.sede LIKE('%$palabra%')
                            )";
        if($nombre != '')
        	$consulta .= " AND C.nombre_cliente LIKE('%$nombre%')";
        if($apellido != '')
        	$consulta .= " AND C.apellido_cliente LIKE('%$apellido%')";	
        if($estado != '')
        	$consulta .= " AND Ec.estado_cotizacion='$estado'";	
        if($fecha_desde != '' && $fecha_hasta != '')
        	$consulta .= " AND Ec.fecha_cotizacion >= '$fecha_desde' AND Ec.fecha_cotizacion <= '$fecha_hasta'";
        $consulta .= " GROUP BY Ec.idevento_cotizado";
        //echo $consulta . "<br/>";
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_evento_cotizado_por_palabra_clave_tipo_evento($palabra, $nombre, $apellido, $estado, $fecha_desde, $fecha_hasta)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.titulo, Ev.nombre_generico, Ec.idevento_cotizado, Ec.fecha_cotizacion, Ec.estado_cotizacion, C.trato_cliente, C.nombre_cliente, C.apellido_cliente, U.nombre_usuario 
                        FROM publicacion Pb, evento Ev, evento_cotizado Ec, clientes C, 
                             evento_has_tipo_evento Ete, tipo_evento Te, 
                            usuarios U 
                        WHERE Pb.idpublicacion=Ev.idpublicacion AND Ev.idpublicacion=Ec.idevento AND Ec.idclientes=C.idclientes AND Ec.idusuarios=U.idusuarios AND Ete.idevento=Ev.idpublicacion AND Ete.idtipo_evento=Te.idtipo_evento AND Te.descripcion LIKE('%$palabra%')";
        if($nombre != '')
        	$consulta .= " AND C.nombre_cliente LIKE('%$nombre%')";
        if($apellido != '')
        	$consulta .= " AND C.apellido_cliente LIKE('%$apellido%')";	
        if($estado != '')
        	$consulta .= " AND Ec.estado_cotizacion='$estado'";	
        if($fecha_desde != '' && $fecha_hasta != '')
        	$consulta .= " AND Ec.fecha_cotizacion >= '$fecha_desde' AND Ec.fecha_cotizacion <= '$fecha_hasta'";
        $consulta .= " GROUP BY Ec.idevento_cotizado";
        //echo $consulta . "<br/>";
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_evento_cotizado_por_palabra_clave_temporada($palabra, $nombre, $apellido, $estado, $fecha_desde, $fecha_hasta)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.titulo, Ev.nombre_generico, Ec.idevento_cotizado, Ec.fecha_cotizacion, Ec.estado_cotizacion, C.trato_cliente, C.nombre_cliente, C.apellido_cliente, U.nombre_usuario 
                        FROM publicacion Pb, evento Ev, evento_cotizado Ec, clientes C, 
                            evento_has_temporada Eht, temporada T, 
                            usuarios U 
                        WHERE Pb.idpublicacion=Ev.idpublicacion AND Ev.idpublicacion=Ec.idevento AND Ec.idclientes=C.idclientes AND Ec.idusuarios=U.idusuarios AND Eht.idevento=Ev.idpublicacion AND Eht.idtemporada=T.idtemporada AND T.nombre LIKE('%$palabra%')";
        if($nombre != '')
        	$consulta .= " AND C.nombre_cliente LIKE('%$nombre%')";
        if($apellido != '')
        	$consulta .= " AND C.apellido_cliente LIKE('%$apellido%')";	
        if($estado != '')
        	$consulta .= " AND Ec.estado_cotizacion='$estado'";	
        if($fecha_desde != '' && $fecha_hasta != '')
        	$consulta .= " AND Ec.fecha_cotizacion >= '$fecha_desde' AND Ec.fecha_cotizacion <= '$fecha_hasta'";
        $consulta .= " GROUP BY Ec.idevento_cotizado";
        //echo $consulta . "<br/>";
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_evento_cotizado_por_palabra_clave_destino($palabra, $nombre, $apellido, $estado, $fecha_desde, $fecha_hasta)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.titulo, Ev.nombre_generico, Ec.idevento_cotizado, Ec.fecha_cotizacion, Ec.estado_cotizacion, C.trato_cliente, C.nombre_cliente, C.apellido_cliente, U.nombre_usuario 
                        FROM publicacion Pb, evento Ev, evento_cotizado Ec, clientes C, 
                            evento_has_destino Ehd, destino D, 
                            usuarios U 
                        WHERE Pb.idpublicacion=Ev.idpublicacion AND Ev.idpublicacion=Ec.idevento AND Ec.idclientes=C.idclientes AND Ec.idusuarios=U.idusuarios AND Ehd.idevento=Ev.idpublicacion AND Ehd.iddestino=D.iddestino AND D.nombre_destino LIKE('%$palabra%')";
        if($nombre != '')
        	$consulta .= " AND C.nombre_cliente LIKE('%$nombre%')";
        if($apellido != '')
        	$consulta .= " AND C.apellido_cliente LIKE('%$apellido%')";	
        if($estado != '')
        	$consulta .= " AND Ec.estado_cotizacion='$estado'";	
        if($fecha_desde != '' && $fecha_hasta != '')
        	$consulta .= " AND Ec.fecha_cotizacion >= '$fecha_desde' AND Ec.fecha_cotizacion <= '$fecha_hasta'";
        $consulta .= " GROUP BY Ec.idevento_cotizado";
        //echo $consulta . "<br/>";
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_evento_cotizado_por_palabra_clave_alcance($palabra, $nombre, $apellido, $estado, $fecha_desde, $fecha_hasta)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.titulo, Ev.nombre_generico, Ec.idevento_cotizado, Ec.fecha_cotizacion, Ec.estado_cotizacion, C.trato_cliente, C.nombre_cliente, C.apellido_cliente, U.nombre_usuario 
                        FROM publicacion Pb, evento Ev, evento_cotizado Ec, clientes C, 
                            evento_has_alcance_evento Eha, alcance_evento Ae, 
                            usuarios U 
                        WHERE Pb.idpublicacion=Ev.idpublicacion AND Ev.idpublicacion=Ec.idevento AND Ec.idclientes=C.idclientes AND Ec.idusuarios=U.idusuarios AND Eha.idevento=Ev.idpublicacion AND Eha.idalcance_evento=Ae.idalcance_evento AND Ae.descripcion LIKE('%$palabra%')";
        if($nombre != '')
        	$consulta .= " AND C.nombre_cliente LIKE('%$nombre%')";
        if($apellido != '')
        	$consulta .= " AND C.apellido_cliente LIKE('%$apellido%')";	
        if($estado != '')
        	$consulta .= " AND Ec.estado_cotizacion='$estado'";	
        if($fecha_desde != '' && $fecha_hasta != '')
        	$consulta .= " AND Ec.fecha_cotizacion >= '$fecha_desde' AND Ec.fecha_cotizacion <= '$fecha_hasta'";
        $consulta .= " GROUP BY Ec.idevento_cotizado";
        //echo $consulta . "<br/>";
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_evento_cotizado_por_palabra_clave_especialidad($palabra, $nombre, $apellido, $estado, $fecha_desde, $fecha_hasta)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.titulo, Ev.nombre_generico, Ec.idevento_cotizado, Ec.fecha_cotizacion, Ec.estado_cotizacion, C.trato_cliente, C.nombre_cliente, C.apellido_cliente, U.nombre_usuario 
                        FROM publicacion Pb, evento Ev, evento_cotizado Ec, clientes C, 
                            evento_has_especialidades Ehe, especialidades E, 
                            usuarios U 
                        WHERE Pb.idpublicacion=Ev.idpublicacion AND Ev.idpublicacion=Ec.idevento AND Ec.idclientes=C.idclientes AND Ec.idusuarios=U.idusuarios AND Ehe.idevento=Ev.idpublicacion AND Ehe.idespecialidades=E.idespecialidades AND E.nombre_especialidad LIKE('%$palabra%')";
        if($nombre != '')
        	$consulta .= " AND C.nombre_cliente LIKE('%$nombre%')";
        if($apellido != '')
        	$consulta .= " AND C.apellido_cliente LIKE('%$apellido%')";	
        if($estado != '')
        	$consulta .= " AND Ec.estado_cotizacion='$estado'";	
        if($fecha_desde != '' && $fecha_hasta != '')
        	$consulta .= " AND Ec.fecha_cotizacion >= '$fecha_desde' AND Ec.fecha_cotizacion <= '$fecha_hasta'";
        $consulta .= " GROUP BY Ec.idevento_cotizado";
        //echo $consulta . "<br/>";
        return $controladoraBD->ejecutarconsulta($consulta);
    }
	
	function enviar_idgestion($anio)
	{
		$controladoraBD = new controladoraBD;
		$consulta = "SELECT idgestion,gestion FROM gestion WHERE estado_gestion = 1 AND gestion.gestion=(SELECT MAX(gestion) from gestion)";

		return $controladoraBD->ejecutarconsulta($consulta);
	}
	
	function enviar_ultimo_tipo_cambio($idgestion, $moneda)
	{
		$controladoraBD = new controladoraBD;
		$consulta = "SELECT factor_tipo_cambio,fecha,moneda FROM tipo_cambio WHERE gestion_idgestion='$idgestion' AND moneda='$moneda' ORDER BY fecha DESC";

		return $controladoraBD->ejecutarconsulta($consulta);
	}
}

?>