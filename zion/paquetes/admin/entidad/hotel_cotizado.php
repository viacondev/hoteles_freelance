<?
	class hotel_cotizado
	{
		function obtener_combinacion_hotel_paquete_cotizado($idpaquete_cotizado)
		{
			$controladoraBD = new controladoraBD;
			$consulta		= "SELECT opcion_hoteles_paquete_cotizado.idopcion_hoteles_paquete_cotizado, hotel.idhotel, hotel.nombre_hotel, hotel.categoria FROM hotel
								INNER JOIN item_hotel_paquete_cotizado ON hotel.idhotel = item_hotel_paquete_cotizado.idhotel
								INNER JOIN opcion_hoteles_has_item_paquete_cotizado ON item_hotel_paquete_cotizado.iditem_hotel_paquete_cotizado = opcion_hoteles_has_item_paquete_cotizado.iditem_hotel_paquete_cotizado
								INNER JOIN opcion_hoteles_paquete_cotizado ON opcion_hoteles_has_item_paquete_cotizado.idopcion_hoteles_paquete_cotizado = opcion_hoteles_paquete_cotizado.idopcion_hoteles_paquete_cotizado
								WHERE opcion_hoteles_paquete_cotizado.idpaquete_cotizado = $idpaquete_cotizado";
			echo $consulta;
			return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function obtener_hotel_class_cotizado($idcotizacion,$class)
		{
			$controladoraBD = new controladoraBD;
			$consulta		= "SELECT item_hotel_".$class."_cotizado.*,tarifa_hotel_".$class."_cotizado.destino, hotel.nombre_hotel, hotel.categoria FROM item_hotel_".$class."_cotizado
								INNER JOIN tarifa_hotel_".$class."_cotizado ON item_hotel_".$class."_cotizado.idtarifa_hotel_".$class."_cotizado = tarifa_hotel_".$class."_cotizado.idtarifa_hotel_".$class."_cotizado
								INNER JOIN hotel ON item_hotel_".$class."_cotizado.idhotel = hotel.idhotel
								WHERE tarifa_hotel_".$class."_cotizado.id".$class."_cotizado = $idcotizacion";
	
			return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function registrar_hotel_cotizado_class_cotizado_has_pax_class($idhotel_cotizado, $idpax, $tipo_precio, $class)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "INSERT INTO pqt_item_hotel_".$class."_cotizado_has_pax_".$class."(iditem_hotel_".$class."_cotizado, idpax_".$class.", tipo_precio) 
								VALUES('$idhotel_cotizado', '$idpax', '$tipo_precio')";
			return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function obtenet_hotel_cotizado_idpax($idpax, $class)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "SELECT pqt_item_hotel_".$class."_cotizado_has_pax_".$class.".* FROM pqt_item_hotel_".$class."_cotizado_has_pax_".$class."
								WHERE pqt_item_hotel_".$class."_cotizado_has_pax_".$class.".idpax_".$class." = $idpax";
								//echo $consulta;
			return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function obtener_item_hotel_class_cotizado_id($iditem_hotel, $class)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "SELECT item_hotel_".$class."_cotizado.*, hotel.nombre_hotel, hotel.categoria,tarifa_hotel_".$class."_cotizado.destino, tarifa_hotel_".$class."_cotizado.in, tarifa_hotel_".$class."_cotizado.out, tarifa_hotel_".$class."_cotizado.cant_noches FROM item_hotel_".$class."_cotizado
								INNER JOIN hotel on item_hotel_".$class."_cotizado.idhotel = hotel.idhotel
								INNER JOIN tarifa_hotel_".$class."_cotizado on item_hotel_".$class."_cotizado.idtarifa_hotel_".$class."_cotizado = tarifa_hotel_".$class."_cotizado.idtarifa_hotel_".$class."_cotizado
								WHERE item_hotel_".$class."_cotizado.iditem_hotel_".$class."_cotizado= $iditem_hotel";
			return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function eliminar_hotel_cotizado_class_cotizado_has_pax_class($idhotel_cotizado, $idpax, $class)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "DELETE FROM pqt_item_hotel_".$class."_cotizado_has_pax_".$class."
								WHERE pqt_item_hotel_".$class."_cotizado_has_pax_".$class.".iditem_hotel_".$class."_cotizado = $idhotel_cotizado
								AND pqt_item_hotel_".$class."_cotizado_has_pax_".$class.".idpax_".$class." = $idpax";
			return $controladoraBD->ejecutarconsulta($consulta);
		}

		function obtener_nombres_pasajeros_por_hotel($iditem, $class)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "SELECT C.nombre_cliente, C.apellido_cliente, O.tipo_precio FROM pqt_item_hotel_" . $class . "_cotizado_has_pax_" . $class . " O , pqt_pax_" . $class . " P, clientes C 
								WHERE O.idpax_" . $class . "=P.idpax_" . $class . " AND P.idclientes=C.idclientes AND O.iditem_hotel_" . $class . "_cotizado=" . $iditem;
	
			return $controladoraBD->ejecutarconsulta($consulta);	
		}

		function obtener_items_hoteles_por_pax($idpax, $class)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "SELECT Ph.*, I.*, T.*, H.nombre_hotel, H.categoria FROM pqt_item_hotel_" . $class . "_cotizado_has_pax_" . $class . " Ph, item_hotel_" . $class . "_cotizado I, tarifa_hotel_" . $class . "_cotizado T , hotel H 
								WHERE Ph.iditem_hotel_" . $class . "_cotizado=I.iditem_hotel_" . $class . "_cotizado AND I.idtarifa_hotel_" . $class . "_cotizado=T.idtarifa_hotel_" . $class . "_cotizado AND I.idhotel=H.idhotel AND Ph.idpax_" . $class . "=$idpax";
	
			return $controladoraBD->ejecutarconsulta($consulta);		
		}
	}
?>