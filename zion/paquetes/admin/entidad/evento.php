<?php

class evento
{   
	function registrar_evento($idpublicacion, $precio_desde, $salida, $retorno, $cant_noches, $nombre_generico, $sede)
	{
		$controladoraBD= new controladoraBD;
        $consulta = "INSERT INTO evento(idpublicacion, precio_desde, salida, retorno, cant_noches, nombre_generico, sede) VALUES ($idpublicacion, $precio_desde, '$salida', '$retorno', $cant_noches, '$nombre_generico', '$sede')";
        
        return $controladoraBD->ejecutarconsulta($consulta);	
	}
    function registrar_contenido_evento($idevento, $paquete_incluye, $importante, $datos_agente, $paginas_web)
    {
        $controladoraBD     = new controladoraBD;
        $consulta           = "INSERT INTO contenido_evento(idevento, paquete_incluye, importante, datos_agente, paginas_web) VALUES($idevento, '$paquete_incluye', '$importante', '$datos_agente', '$paginas_web')";
        return $controladoraBD->ejecutarconsulta($consulta);        
    }	
    function obtener_evento($idpublicacion)
    {
        $controladoraBD= new controladoraBD;
        $consulta = "SELECT * FROM publicacion Pb, evento Ev, contenido_evento Ce, usuarios U WHERE Pb.idpublicacion=Ev.idpublicacion AND Ev.idpublicacion=Ce.idevento AND Pb.idusuarios=U.idusuarios AND Pb.idpublicacion=$idpublicacion";
        
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_tipos_de_evento_por_evento($idevento)
    {
        $controladoraBD= new controladoraBD;
        $consulta = "SELECT Te.idtipo_evento, Te.descripcion FROM evento E, evento_has_tipo_evento Et, tipo_evento Te WHERE E.idpublicacion=Et.idevento AND Et.idtipo_evento=Te.idtipo_evento AND E.idpublicacion=$idevento";
        //echo $consulta;
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_destinos_por_evento($idevento)
    {
        $controladoraBD= new controladoraBD;
        $consulta = "SELECT D.iddestino, D.nombre_destino FROM evento Ev, evento_has_destino Ed, destino D WHERE Ev.idpublicacion=Ed.idevento AND Ed.iddestino=D.iddestino AND Ev.idpublicacion=$idevento";
        //echo $consulta;
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_temporadas_por_evento($idevento)
    {
        $controladoraBD= new controladoraBD;
        $consulta = "SELECT Tp.idtemporada, Tp.nombre FROM evento Ev, evento_has_temporada Et, temporada Tp WHERE Ev.idpublicacion=Et.idevento AND Et.idtemporada=Tp.idtemporada AND Ev.idpublicacion=$idevento";
        //echo $consulta;
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_alcances_de_evento_por_evento($idevento)
    {
        $controladoraBD= new controladoraBD;
        $consulta = "SELECT Ae.idalcance_evento, Ae.descripcion FROM evento Ev, evento_has_alcance_evento Ea, alcance_evento Ae WHERE Ev.idpublicacion=Ea.idevento AND Ea.idalcance_evento=Ae.idalcance_evento AND Ev.idpublicacion=$idevento";
        //echo $consulta;
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_especialidades_por_evento($idevento)
    {
        $controladoraBD= new controladoraBD;
        $consulta = "SELECT Es.idespecialidades, Es.nombre_especialidad FROM evento Ev, evento_has_especialidades Ee, especialidades Es WHERE Ev.idpublicacion=Ee.idevento AND Ee.idespecialidades=Es.idespecialidades AND Ev.idpublicacion=$idevento";
        //echo $consulta;
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_evento_por_criterios($tipo, $temporada, $destino, $precio, $estado)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.idpublicacion, Pb.titulo, Ev.cant_noches, Ev.salida, Ev.precio_desde, Pb.fecha_publicacion, Pb.estado, U.nombre_usuario, Pb.imagen_chica FROM publicacion Pb, evento Ev, usuarios U ";
        if ($tipo != 0) 
        {
            $consulta .= ",evento_has_tipo_evento Ete ";
        }
        if($temporada != "")
        {
            $consulta .= ", evento_has_temporada Eht, temporada T " ;
        }
        if($destino != "")
        {
            $consulta .= ", evento_has_destino Ehd, destino D ";       
        }
        $consulta .= " WHERE Pb.idusuarios=U.idusuarios AND Pb.idpublicacion=Ev.idpublicacion AND Pb.estado='$estado' AND Pb.tipo_publicacion='E' ";
        if ($tipo != 0) 
        {
            $consulta .= "AND ( Ete.idevento=Ev.idpublicacion AND Ete.idtipo_evento=$tipo ) ";
        }
        if($temporada != "")
        {
            $consulta .= "AND ( Eht.idevento=Ev.idpublicacion AND Eht.idtemporada=T.idtemporada AND T.nombre LIKE('%$temporada%') ) ";
        }
        if($destino != "")
        {
            $consulta .= "AND ( Ehd.idevento=Ev.idpublicacion AND Ehd.iddestino=D.iddestino AND D.nombre_destino LIKE('%$destino%') ) ";
        }
        
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_evento_por_palabra_clave($palabra, $estado)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.idpublicacion, Pb.titulo, Pb.vista_previa, Pb.portada, Ev.nombre_generico, Ev.cant_noches, Ev.salida, Ev.retorno, Ev.precio_desde, Pb.fecha_publicacion, Pb.estado, Ev.armado, U.nombre_usuario 
                        FROM publicacion Pb, evento Ev, 
                            evento_has_tipo_evento Ete, tipo_evento Te, 
                            evento_has_temporada Eht, temporada T, 
                            evento_has_destino Ehd, destino D, 
                            evento_has_alcance_evento Eha, alcance_evento Ae, 
                            evento_has_especialidades Ehe, especialidades E, 
                            usuarios U 
                        WHERE Pb.idpublicacion=Ev.idpublicacion AND Pb.estado='$estado' AND Pb.tipo_publicacion='E' AND Pb.vista_previa='S' AND Pb.idusuarios=U.idusuarios AND (
                                ( Ete.idevento=Ev.idpublicacion AND Ete.idtipo_evento=Te.idtipo_evento AND Te.descripcion LIKE('%$palabra%') ) 
                                OR ( Eht.idevento=Ev.idpublicacion AND Eht.idtemporada=T.idtemporada AND T.nombre LIKE('%$palabra%') ) 
                                OR ( Ehd.idevento=Ev.idpublicacion AND Ehd.iddestino=D.iddestino AND D.nombre_destino LIKE('%$palabra%') ) 
                                OR ( Eha.idevento=Ev.idpublicacion AND Eha.idalcance_evento=Ae.idalcance_evento AND Ae.descripcion LIKE('%$palabra%') ) 
                                OR ( Ehe.idevento=Ev.idpublicacion AND Ehe.idespecialidades=E.idespecialidades AND E.nombre_especialidad LIKE('%$palabra%') ) 
                                OR Pb.titulo LIKE('%$palabra%') 
                            ) GROUP BY Pb.idpublicacion";
        
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_evento_por_palabra_clave_titulo($palabra, $estado)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.idpublicacion, Pb.titulo, Pb.vista_previa, Pb.portada, Ev.nombre_generico, Ev.cant_noches, Ev.salida, Ev.retorno, Ev.precio_desde, Pb.fecha_publicacion, Pb.estado, Ev.armado, U.nombre_usuario 
                        FROM publicacion Pb, evento Ev, 
                            usuarios U 
                        WHERE Pb.idpublicacion=Ev.idpublicacion AND Pb.estado='$estado' AND Pb.tipo_publicacion='E' AND Pb.idusuarios=U.idusuarios AND (
                                Pb.titulo LIKE('%$palabra%') OR Ev.nombre_generico LIKE('%$palabra%') OR Ev.sede LIKE('%$palabra%')
                            ) GROUP BY Pb.idpublicacion";
        //echo $consulta . "<br/>";
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_evento_por_palabra_clave_tipo_evento($palabra, $estado)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.idpublicacion, Pb.titulo, Pb.vista_previa, Pb.portada, Ev.nombre_generico, Ev.cant_noches, Ev.salida, Ev.retorno, Ev.precio_desde, Pb.fecha_publicacion, Pb.estado, Ev.armado, U.nombre_usuario 
                        FROM publicacion Pb, evento Ev, 
                             evento_has_tipo_evento Ete, tipo_evento Te, 
                            usuarios U 
                        WHERE Pb.idpublicacion=Ev.idpublicacion AND Pb.estado='$estado' AND Pb.tipo_publicacion='E' AND Pb.idusuarios=U.idusuarios AND Ete.idevento=Ev.idpublicacion AND Ete.idtipo_evento=Te.idtipo_evento AND Te.descripcion LIKE('%$palabra%') GROUP BY Pb.idpublicacion";
        //echo $consulta . "<br/>";
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_evento_por_palabra_clave_temporada($palabra, $estado)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.idpublicacion, Pb.titulo, Pb.vista_previa, Pb.portada, Ev.nombre_generico, Ev.cant_noches, Ev.salida, Ev.retorno, Ev.precio_desde, Pb.fecha_publicacion, Pb.estado, Ev.armado, U.nombre_usuario 
                        FROM publicacion Pb, evento Ev, 
                            evento_has_temporada Eht, temporada T, 
                            usuarios U 
                        WHERE Pb.idpublicacion=Ev.idpublicacion AND Pb.estado='$estado' AND Pb.tipo_publicacion='E' AND Pb.idusuarios=U.idusuarios AND Eht.idevento=Ev.idpublicacion AND Eht.idtemporada=T.idtemporada AND T.nombre LIKE('%$palabra%') GROUP BY Pb.idpublicacion";
        //echo $consulta . "<br/>";
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_evento_por_palabra_clave_destino($palabra, $estado)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.idpublicacion, Pb.titulo, Pb.vista_previa, Pb.portada, Ev.nombre_generico, Ev.cant_noches, Ev.salida, Ev.retorno, Ev.precio_desde, Pb.fecha_publicacion, Pb.estado, Ev.armado, U.nombre_usuario 
                        FROM publicacion Pb, evento Ev, 
                            evento_has_destino Ehd, destino D, 
                            usuarios U 
                        WHERE Pb.idpublicacion=Ev.idpublicacion AND Pb.estado='$estado' AND Pb.tipo_publicacion='E' AND Pb.idusuarios=U.idusuarios AND Ehd.idevento=Ev.idpublicacion AND Ehd.iddestino=D.iddestino AND D.nombre_destino LIKE('%$palabra%') GROUP BY Pb.idpublicacion";
        //echo $consulta . "<br/>";
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_evento_por_palabra_clave_alcance($palabra, $estado)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.idpublicacion, Pb.titulo, Pb.vista_previa, Pb.portada, Ev.nombre_generico, Ev.cant_noches, Ev.salida, Ev.retorno, Ev.precio_desde, Pb.fecha_publicacion, Pb.estado, Ev.armado, U.nombre_usuario 
                        FROM publicacion Pb, evento Ev, 
                            evento_has_alcance_evento Eha, alcance_evento Ae, 
                            usuarios U 
                        WHERE Pb.idpublicacion=Ev.idpublicacion AND Pb.estado='$estado' AND Pb.tipo_publicacion='E' AND Pb.idusuarios=U.idusuarios AND Eha.idevento=Ev.idpublicacion AND Eha.idalcance_evento=Ae.idalcance_evento AND Ae.descripcion LIKE('%$palabra%') GROUP BY Pb.idpublicacion";
        //echo $consulta . "<br/>";
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_evento_por_palabra_clave_especialidad($palabra, $estado)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.idpublicacion, Pb.titulo, Pb.vista_previa, Pb.portada, Ev.nombre_generico, Ev.cant_noches, Ev.salida, Ev.retorno, Ev.precio_desde, Pb.fecha_publicacion, Pb.estado, Ev.armado, U.nombre_usuario 
                        FROM publicacion Pb, evento Ev, 
                            evento_has_especialidades Ehe, especialidades E, 
                            usuarios U 
                        WHERE Pb.idpublicacion=Ev.idpublicacion AND Pb.estado='$estado' AND Pb.tipo_publicacion='E' AND Pb.idusuarios=U.idusuarios AND Ehe.idevento=Ev.idpublicacion AND Ehe.idespecialidades=E.idespecialidades AND E.nombre_especialidad LIKE('%$palabra%') GROUP BY Pb.idpublicacion";
        //echo $consulta . "<br/>";
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_contenido_evento($idevento)
    {
        $controladoraBD= new controladoraBD;
        $consulta = "SELECT * FROM contenido_evento Ce WHERE Ce.idevento=$idevento";
        //echo $consulta;
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_eventos()
    {
        $controladoraBD= new controladoraBD;
        $consulta = "SELECT * FROM evento E, publicacion P WHERE E.idpublicacion=P.idpublicacion AND P.estado='V' ORDER BY P.titulo";
        //echo $consulta;
        return $controladoraBD->ejecutarconsulta($consulta);   
    }
    function obtener_cantidad_cotizaciones($idevento)
    {
        $controladoraBD= new controladoraBD;
        $consulta = "SELECT COUNT(idevento) AS cantidad FROM evento_cotizado E WHERE E.idevento='$idevento' GROUP BY idevento";
        //echo $consulta;
        return $controladoraBD->ejecutarconsulta($consulta);      
    }
    function obtener_titulo_evento($idevento)
    {
        $controladoraBD= new controladoraBD;
        $consulta = "SELECT P.titulo, E.nombre_generico FROM publicacion P, evento E WHERE P.idpublicacion=E.idpublicacion AND E.idpublicacion='$idevento'";
        //echo $consulta;
        return $controladoraBD->ejecutarconsulta($consulta);      
    }
    function obtener_cantidad_de_envios_de_evento($idevento)
    {
        $controladoraBD = new controladoraBD;
        $consulta       = "SELECT COUNT(codigo) AS cantidad FROM pqt_envio_masivo WHERE tipo_envio='E' AND codigo='$idevento' GROUP BY codigo";
        
        return $controladoraBD->ejecutarconsulta($consulta);    
    }
    /*
    *  EVENTO COMO EL NUEVO FORMATO
    */
    function registrar_nuevo_evento($nombre_evento, $sigla_evento, $sede, $fecha_inicio, $fecha_fin)
    {
        $controladoraBD = new controladoraBD;
        $consulta       = "INSERT INTO pqt_evento(nombre_evento, sigla_evento, sede, fecha_inicio, fecha_fin) 
                            VALUES ('$nombre_evento', '$sigla_evento', '$sede', '$fecha_inicio', '$fecha_fin')";
        
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function registrar_publicacion_has_evento($idpublicacion, $idevento)
    {
        $controladoraBD = new controladoraBD;
        $consulta       = "INSERT INTO pqt_publicacion_has_evento(idpublicacion, idevento) 
                            VALUES ('$idpublicacion', '$idevento')";
        
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function buscar_eventos($palabra)
    {
        $controladoraBD = new controladoraBD;
        $consulta       = "SELECT * FROM pqt_evento 
                            WHERE nombre_evento LIKE('%$palabra%') OR sigla_evento LIKE('%$palabra')";
        
        return $controladoraBD->ejecutarconsulta($consulta);
    }
}

?>