<?php
/**
* 
*/
class tarifa_otros
{
	
	function obtener_tarifa_otros($id, $type)
	{
		$controladoraBD = new controladoraBD;

		$consulta = "SELECT * FROM tarifa_otros_" . $type . " T, pqt_otro_servicio S  
						WHERE T.idotro_servicio=S.idotro_servicio AND id" . $type . "=$id ORDER BY idtarifa_otros_" . $type . " ASC";

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	/*function registrar_tarifa_otros($id, $concepto, $precio_adulto, $precio_menor, $precio_infante, $fee, $factura, $moneda, $observacion, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "INSERT INTO tarifa_otros_" . $type . "(id" . $type . ", concepto, precio_adulto, precio_menor, precio_infante, fee, factura, moneda, observacion) 
							VALUES('$id', '$concepto', '$precio_adulto', '$precio_menor', '$precio_infante', '$fee', '$factura', '$moneda', '$observacion')";

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function modificar_tarifa_otros($id, $concepto, $precio_adulto, $precio_menor, $precio_infante, $fee, $factura, $moneda, $observacion, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "UPDATE tarifa_otros_" . $type . " 
							SET concepto='$concepto', precio_adulto='$precio_adulto', precio_menor='$precio_menor', precio_infante='$precio_infante', fee='$fee', factura='$factura', moneda='$moneda', observacion='$observacion' 
							WHERE idtarifa_otros_" . $type . "='$id' ";
		return $controladoraBD->ejecutarconsulta($consulta);
	}*/
	function registrar_tarifa_otros($id, $codigo, $idotro_servicio, $descripcion, $precio_adulto, $precio_menor, $precio_infante, $fee, $factura, $moneda, $tipo_cambio, $time_limit, $observacion, $estado, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "INSERT INTO tarifa_otros_" . $type . "(id" . $type . ", codigo, idotro_servicio, descripcion, precio_adulto, precio_menor, precio_infante, fee, factura, moneda, tipo_cambio, time_limit, observacion, estado) 
							VALUES('$id', '$codigo', '$idotro_servicio', '$descripcion', '$precio_adulto', '$precio_menor', '$precio_infante', '$fee', '$factura', '$moneda', '$tipo_cambio', '$time_limit', '$observacion', '$estado')";

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function modificar_tarifa_otros($id, $codigo, $idotro_servicio, $descripcion, $precio_adulto, $precio_menor, $precio_infante, $fee, $factura, $moneda, $tipo_cambio, $time_limit, $observacion, $estado, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "UPDATE tarifa_otros_" . $type . " 
							SET codigo='$codigo', idotro_servicio='$idotro_servicio', descripcion='$descripcion', precio_adulto='$precio_adulto', precio_menor='$precio_menor', precio_infante='$precio_infante', fee='$fee', factura='$factura', moneda='$moneda', tipo_cambio='$tipo_cambio', time_limit='$time_limit', observacion='$observacion', estado='$estado' 
							WHERE idtarifa_otros_" . $type . "='$id' ";
		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function eliminar_tarifa_otros($id, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "DELETE FROM tarifa_otros_" . $type . " 
							WHERE idtarifa_otros_" . $type . "='$id'";

		return $controladoraBD->ejecutarconsulta($consulta);	
	}

	/*
	* NUEVO FORMATO
	*/
	function obtener_otros_servicios_por_ciudad($idciudad_origen_paquete)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT * FROM pqt_tarifa_otros T, pqt_otro_servicio O 
							WHERE T.idotro_servicio=O.idotro_servicio AND T.idciudad_origen_paquete=$idciudad_origen_paquete";

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function obtener_otros_servicios_por_ciudad_real_cotizacion($idciudad_origen_paquete, $is_real)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT * FROM pqt_tarifa_otros T, pqt_otro_servicio O 
							WHERE T.idotro_servicio=O.idotro_servicio AND T.idciudad_origen_paquete=$idciudad_origen_paquete AND T.is_real=$is_real";

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function registrar_nuevo_servicio($idciudad_origen_paquete, $idotro_servicio, $codigo, $descripcion, $ciudad, $fecha_desde, $fecha_hasta, $precio_adulto, $precio_menor, $precio_infante, $fee, $increment_fee, $factura, $increment_factura, $moneda, $tipo_cambio, $time_limit, $observacion, $is_real, $estado)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "INSERT INTO pqt_tarifa_otros(idciudad_origen_paquete, idotro_servicio, codigo, descripcion, ciudad, fecha_desde, fecha_hasta, precio_adulto, precio_menor, precio_infante, fee, increment_fee, factura, increment_factura, moneda, tipo_cambio, time_limit, observacion, is_real, estado) 
							VALUES('$idciudad_origen_paquete', '$idotro_servicio', '$codigo', '$descripcion', '$ciudad', '$fecha_desde', '$fecha_hasta', '$precio_adulto', '$precio_menor', '$precio_infante', '$fee', '$increment_fee', '$factura', '$increment_factura', '$moneda', '$tipo_cambio', '$time_limit', '$observacion', '$is_real', '$estado')";

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function modificar_servicio_x_id($idtarifa_otros, $idotro_servicio, $codigo, $descripcion, $ciudad, $fecha_desde, $fecha_hasta, $precio_adulto, $precio_menor, $precio_infante, $fee, $increment_fee, $factura, $increment_factura, $moneda, $tipo_cambio, $time_limit, $observacion, $is_real, $estado)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "UPDATE pqt_tarifa_otros 
							SET idotro_servicio='$idotro_servicio', codigo='$codigo', descripcion='$descripcion', ciudad='$ciudad', fecha_desde='$fecha_desde', fecha_hasta='$fecha_hasta', precio_adulto='$precio_adulto', precio_menor='$precio_menor', precio_infante='$precio_infante', fee='$fee', increment_fee='$increment_fee', factura='$factura', increment_factura='$increment_factura', moneda='$moneda', tipo_cambio='$tipo_cambio', time_limit='$time_limit', observacion='$observacion', is_real='$is_real', estado='$estado' 
							WHERE idtarifa_otros=$idtarifa_otros";

		return $controladoraBD->ejecutarconsulta($consulta);	
	}

	function eliminar_otro_servicio_x_id($idtarifa_otros)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "DELETE FROM pqt_tarifa_otros 
							WHERE idtarifa_otros=$idtarifa_otros";

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	/*
	* REPORTES PARA COTIZACIONES
	*/

		function obtener_otros_servicios_segun_cotizaciones($idservicios)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "SELECT Os.* 

								FROM 	pqt_otro_servicio Os, 
										pqt_tarifa_otros Tot, 
										pqt_ciudad_origen_paquete Cop, 
										pqt_cotizacion_has_ciudad_origen Chco 

								WHERE 	Os.idotro_servicio = Tot.idotro_servicio AND 
										Tot.idciudad_origen_paquete = Cop.idciudad_origen_paquete AND 
										Cop.idciudad_origen_paquete = Chco.idciudad_origen_paquete AND 
										Tot.is_real=1 AND 
										Chco.idcotizacion IN($idservicios) 

								GROUP BY Os.idotro_servicio";

			return $controladoraBD->ejecutarconsulta($consulta);
		}

		function obtener_tarifas_otros_servicios_x_servicio($idotro_servicio, $idcotizaciones)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "SELECT Tot.* 

								FROM 	pqt_tarifa_otros Tot, 
										pqt_ciudad_origen_paquete Cop, 
										pqt_cotizacion_has_ciudad_origen Chco 

								WHERE 	Tot.idciudad_origen_paquete = Cop.idciudad_origen_paquete AND 
										Cop.idciudad_origen_paquete = Chco.idciudad_origen_paquete AND 
										Tot.idotro_servicio=$idotro_servicio AND 
										Tot.is_real=1 AND 
										Chco.idcotizacion IN($idcotizaciones) 
								ORDER BY Tot.fecha_desde";

			return $controladoraBD->ejecutarconsulta($consulta);	
		}

		function obtener_id_servicios_por_pax($idpax)
		{
			$controladoraBD = new controladoraBD;
			$consulta = "SELECT T.* 

							FROM pqt_tarifa_otros_has_pax T 

							WHERE T.idpax='$idpax'";
			//echo $consulta;
			return $controladoraBD->ejecutarconsulta($consulta);
		}

		function obtener_pasajeros_por_servicio($idservicio)
		{
			$controladoraBD = new controladoraBD;
			$consulta = "SELECT C.nombre_cliente, C.apellido_cliente, C.numero_pasaporte_cliente, C.fecha_nacimiento_cliente, Tohp.tipo_precio 

							FROM pqt_pax P, pqt_tarifa_otros_has_pax Tohp, clientes C 

							WHERE 	P.idclientes = C.idclientes AND 
									P.idpax = Tohp.idpax AND 
									Tohp.idtarifa_otros='$idservicio'";
			//echo $consulta;
			return $controladoraBD->ejecutarconsulta($consulta);
		}
}
?>