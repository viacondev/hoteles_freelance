<?php

class archivo_adjunto
{   
	function registrar_archivo_adjunto($nombre, $descripcion)
	{
		$controladoraBD = new controladoraBD;
        $consulta 		= "INSERT INTO archivo_adjunto(nombre, descripcion) VALUES ('$nombre', '$descripcion') ";
        
        return $controladoraBD->ejecutarconsulta($consulta);			
	}
	function registrar_publicacion_has_archivo_adjunto($idarchivo_adjunto, $idpublicacion)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "INSERT INTO publicacion_has_archivo_adjunto(idpublicacion, idarchivo_adjunto) VALUES ($idpublicacion, $idarchivo_adjunto)";

		return $controladoraBD->ejecutarconsulta($consulta);
	}
	function obtener_adjuntos_de_publicacion($idpublicacion)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT A.idarchivo_adjunto, A.descripcion, A.nombre FROM archivo_adjunto A, publicacion_has_archivo_adjunto P WHERE P.idarchivo_adjunto=A.idarchivo_adjunto AND P.idpublicacion='$idpublicacion' ";

		return $controladoraBD->ejecutarconsulta($consulta);
	}
	function desvincular_archivo($idarchivo, $idpublicacion)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "DELETE FROM publicacion_has_archivo_adjunto WHERE idarchivo_adjunto='$idarchivo' AND idpublicacion='$idpublicacion' ";
		
		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function registrar_item_has_adjunto($idarchivo_adjunto, $codigo_item, $idpax, $tipo, $vista)
	{
		$controladoraBD = new controladoraBD;
        $consulta 		= "INSERT INTO pqt_item_has_adjunto(idarchivo_adjunto ,codigo_item, idpax, tipo, vista) VALUES ('$idarchivo_adjunto', '$codigo_item', '$idpax', '$tipo', '$vista') ";
        
        return $controladoraBD->ejecutarconsulta($consulta);			
	}

	function obtener_adjuntos_x_item($codigo_item, $idpax, $tipo)
	{
		$controladoraBD = new controladoraBD;
        $consulta 		= "SELECT * FROM pqt_item_has_adjunto Ia, archivo_adjunto A WHERE Ia.idarchivo_adjunto=A.idarchivo_adjunto AND Ia.codigo_item='$codigo_item' AND Ia.idpax='$idpax' AND Ia.tipo='$tipo'";
        
        return $controladoraBD->ejecutarconsulta($consulta);
	}

	function obtener_adjunto($idarchivo_adjunto)
	{
		$controladoraBD = new controladoraBD;
        $consulta 		= "SELECT * FROM archivo_adjunto WHERE idarchivo_adjunto='$idarchivo_adjunto'";
        
        return $controladoraBD->ejecutarconsulta($consulta);
	}

	function eliminar_archivo_adjunto_x_pax($id, $idadjunto)
	{
		$controladoraBD = new controladoraBD;
        $consulta 		= "DELETE FROM pqt_item_has_adjunto WHERE iditem_has_adjunto='$id'";
        
        $res = $controladoraBD->ejecutarconsulta($consulta);
        
        if($res)
        {
        	$controladoraBD = new controladoraBD;
	        $consulta 		= "DELETE FROM archivo_adjunto WHERE idarchivo_adjunto='$idadjunto'";
	        
	        return $controladoraBD->ejecutarconsulta($consulta);		
        }
        else
        	return false;
	}

	/*
	* NUEVO FORMATO
	*/

	function obtener_ultimo_archivo_id()
	{
		$controladoraBD = new controladoraBD;
        $consulta 		= "SELECT MAX(idarchivo_adjunto) AS next FROM pqt_archivo_adjunto";
        
        $resultado 		= $controladoraBD->ejecutarconsulta($consulta);	
        return $resultado[0]['next'];
	}
	function registrar_nuevo_archivo_adjunto($nombre, $descripcion)
	{
		$controladoraBD = new controladoraBD;
        $consulta 		= "INSERT INTO pqt_archivo_adjunto(nombre, descripcion) 
        					VALUES('$nombre', '$descripcion')";
        
        return $controladoraBD->ejecutarconsulta($consulta);
	}
	function registrar_publicacion_adjunto($idpublicacion, $idarchivo_adjunto, $publicar)
	{
		$controladoraBD = new controladoraBD;
        $consulta 		= "INSERT INTO pqt_publicacion_has_adjunto(idpublicacion, idarchivo_adjunto, publicar) 
        					VALUES('$idpublicacion', '$idarchivo_adjunto', '$publicar')";
        
        return $controladoraBD->ejecutarconsulta($consulta);
	}
	function obtener_info_adjunto($idarchivo_adjunto)
	{
		$controladoraBD = new controladoraBD;
        $consulta 		= "SELECT * FROM pqt_archivo_adjunto WHERE idarchivo_adjunto=$idarchivo_adjunto";
        
        return $controladoraBD->ejecutarconsulta($consulta);	
	}
	function eliminar_archivo_adjunto($idarchivo_adjunto)
	{
		$controladoraBD = new controladoraBD;
        $consulta 		= "DELETE FROM pqt_archivo_adjunto WHERE idarchivo_adjunto=$idarchivo_adjunto";
        
        return $controladoraBD->ejecutarconsulta($consulta);		
	}

		/*
		* ARCHIVOS POR SERVICIO
		*/

		// REGISTRAR
			function registrar_archivo_hotel($idarchivo_adjunto, $iditem_hotel, $is_visible)
			{
				$controladoraBD = new controladoraBD;
		        $consulta 		= "INSERT INTO pqt_item_hotel_has_archivo_adjunto(idarchivo_adjunto, iditem_hotel, is_visible) 
		        					VALUES('$idarchivo_adjunto', '$iditem_hotel', '$is_visible')";
		        
		        return $controladoraBD->ejecutarconsulta($consulta);	
			}
			function registrar_archivo_servicio($idarchivo_adjunto, $idtarifa_otros, $is_visible)
			{
				$controladoraBD = new controladoraBD;
		        $consulta 		= "INSERT INTO pqt_tarifa_otros_has_archivo_adjunto(idarchivo_adjunto, idtarifa_otros, is_visible) 
		        					VALUES('$idarchivo_adjunto', '$idtarifa_otros', '$is_visible')";
		        
		        return $controladoraBD->ejecutarconsulta($consulta);	
			}
			function registrar_archivo_aereo($idarchivo_adjunto, $idboleto_cotizado, $is_visible)
			{
				$controladoraBD = new controladoraBD;
		        $consulta 		= "INSERT INTO pqt_boleto_cotizado_has_archivo_adjunto(idarchivo_adjunto, idboleto_cotizado, is_visible) 
		        					VALUES('$idarchivo_adjunto', '$idboleto_cotizado', '$is_visible')";
		        
		        return $controladoraBD->ejecutarconsulta($consulta);	
			}
			function registrar_archivo_inscripcion($idarchivo_adjunto, $idinscripcion_evento, $is_visible)
			{
				$controladoraBD = new controladoraBD;
		        $consulta 		= "INSERT INTO pqt_inscripcion_evento_has_archivo_adjunto(idarchivo_adjunto, idinscripcion_evento, is_visible) 
		        					VALUES('$idarchivo_adjunto', '$idinscripcion_evento', '$is_visible')";
		        
		        return $controladoraBD->ejecutarconsulta($consulta);	
			}

		// ELIMINAR
			function eliminar_archivo_hotel($idarchivo_adjunto, $iditem_hotel)
			{
				$controladoraBD = new controladoraBD;
		        $consulta 		= "DELETE FROM pqt_item_hotel_has_archivo_adjunto 
		        					WHERE idarchivo_adjunto=$idarchivo_adjunto AND iditem_hotel=$iditem_hotel";
		        
		        return $controladoraBD->ejecutarconsulta($consulta);	
			}
			function eliminar_archivo_servicio($idarchivo_adjunto, $idtarifa_otros)
			{
				$controladoraBD = new controladoraBD;
		        $consulta 		= "DELETE FROM pqt_tarifa_otros_has_archivo_adjunto 
		        					WHERE idarchivo_adjunto=$idarchivo_adjunto AND idtarifa_otros=$idtarifa_otros";
		        
		        return $controladoraBD->ejecutarconsulta($consulta);	
			}
			function eliminar_archivo_aereo($idarchivo_adjunto, $idboleto_cotizado)
			{
				$controladoraBD = new controladoraBD;
		        $consulta 		= "DELETE FROM pqt_boleto_cotizado_has_archivo_adjunto 
		        					WHERE idarchivo_adjunto=$idarchivo_adjunto AND idboleto_cotizado=$idboleto_cotizado";
		        
		        return $controladoraBD->ejecutarconsulta($consulta);	
			}
			function eliminar_archivo_inscripcion($idarchivo_adjunto, $idinscripcion_evento)
			{
				$controladoraBD = new controladoraBD;
		        $consulta 		= "DELETE FROM pqt_inscripcion_evento_has_archivo_adjunto 
		        					WHERE idarchivo_adjunto=$idarchivo_adjunto AND idinscripcion_evento=$idinscripcion_evento";
		        
		        return $controladoraBD->ejecutarconsulta($consulta);	
			}

		// OBTENER
			function obtener_archivos_x_hotel($iditem_hotel)
			{
				$controladoraBD = new controladoraBD;
		        $consulta 		= "SELECT * FROM pqt_item_hotel_has_archivo_adjunto Ha, pqt_archivo_adjunto A  
		        					WHERE Ha.idarchivo_adjunto=A.idarchivo_adjunto AND Ha.iditem_hotel=$iditem_hotel";
		        
		        return $controladoraBD->ejecutarconsulta($consulta);
			}
			function obtener_archivos_x_servicio($idtarifa_otros)
			{
				$controladoraBD = new controladoraBD;
		        $consulta 		= "SELECT * FROM pqt_tarifa_otros_has_archivo_adjunto Oa, pqt_archivo_adjunto A  
		        					WHERE Oa.idarchivo_adjunto=A.idarchivo_adjunto AND Oa.idtarifa_otros=$idtarifa_otros";
		        
		        return $controladoraBD->ejecutarconsulta($consulta);
			}
			function obtener_archivos_x_aereo($idboleto_cotizado)
			{
				$controladoraBD = new controladoraBD;
		        $consulta 		= "SELECT * FROM pqt_boleto_cotizado_has_archivo_adjunto Ba, pqt_archivo_adjunto A  
		        					WHERE Ba.idarchivo_adjunto=A.idarchivo_adjunto AND Ba.idboleto_cotizado=$idboleto_cotizado";
		        
		        return $controladoraBD->ejecutarconsulta($consulta);
			}
			function obtener_archivos_x_inscripcion($idinscripcion_evento)
			{
				$controladoraBD = new controladoraBD;
		        $consulta 		= "SELECT * FROM pqt_inscripcion_evento_has_archivo_adjunto Ia, pqt_archivo_adjunto A  
		        					WHERE Ia.idarchivo_adjunto=A.idarchivo_adjunto AND Ia.idinscripcion_evento=$idinscripcion_evento";
		        
		        return $controladoraBD->ejecutarconsulta($consulta);
			}
}

?>