<?php

class publicacion
{   
	function registrar_publicacion($titulo, $tipo, $idusuarios)
	{
		$controladoraBD= new controladoraBD;
        $consulta = "INSERT INTO publicacion(titulo, fecha_publicacion, tipo_publicacion, idusuarios) VALUES ('$titulo', CURRENT_DATE(), '$tipo', '$idusuarios') ";
        //echo $consulta;
        return $controladoraBD->ejecutarconsulta($consulta);			
	}
	function obtener_datos_publicacion()
	{
		/*$controladoraBD= new controladoraBD;
        $consulta = "SELECT * FROM tipo_paquete";
        return $controladoraBD->ejecutarconsulta($consulta);*/
	}
	/*
	* NUEVO FORMATO
	*/
	function registrar_nueva_publicacion($idusuarios, $tipo_publicacion, $titulo, $fecha_caducidad, $fecha_salida, $fecha_retorno, $precio_desde, $publicado, $solo_imagen)
	{
		$controladoraBD	= new controladoraBD;
        $consulta 		= "INSERT INTO pqt_publicacion(idusuarios, tipo_publicacion, titulo, fecha_publicacion, fecha_caducidad, fecha_salida, fecha_retorno, precio_desde, publicado, solo_imagen) 
        					VALUES ('$idusuarios', '$tipo_publicacion', '$titulo', CURRENT_DATE(), '$fecha_caducidad', '$fecha_salida', '$fecha_retorno', '$precio_desde', '$publicado', '$solo_imagen') ";
        //echo $consulta;
        return $controladoraBD->ejecutarconsulta($consulta);				
	}
	function obtener_info_paquete($idpublicacion)
	{
		$controladoraBD= new controladoraBD;
        $consulta = "SELECT * FROM pqt_publicacion Pb, User U WHERE Pb.idusuarios=U.id AND Pb.idpublicacion=$idpublicacion";
        
        return $controladoraBD->ejecutarconsulta($consulta);
	}
		/*
		* ANULA Y MUESTRA PUBLICACIONES AUTOMATICAMENTE
		*/
		function anular_publicaciones_a_vencerse()
		{
			$controladoraBD= new controladoraBD;
	        $consulta = "UPDATE pqt_publicacion Pb SET estado=0 WHERE Pb.fecha_caducidad < CURRENT_DATE() AND estado=1";

	        return $controladoraBD->ejecutarconsulta($consulta);
		}
		function buscar_paquetes_vacacionales_a_vencerse()
		{
			$controladoraBD	= new controladoraBD;
	        $consulta 		= "SELECT * FROM pqt_publicacion Pb WHERE Pb.tipo_publicacion='P' AND Pb.fecha_caducidad = DATE_ADD( CURRENT_DATE( ) , INTERVAL -1 DAY )";
	        
	        return $controladoraBD->ejecutarconsulta($consulta);
		}
		function buscar_paquetes_eventos_a_vencerse()
		{
			$controladoraBD	= new controladoraBD;
	        $consulta 		= "SELECT * FROM pqt_publicacion Pb WHERE Pb.tipo_publicacion='E' AND Pb.fecha_caducidad = DATE_ADD( CURRENT_DATE( ) , INTERVAL -1 DAY )";
	        
	        return $controladoraBD->ejecutarconsulta($consulta);	
		}
		function buscar_promociones_a_vencerse()
		{
			$controladoraBD	= new controladoraBD;
	        $consulta 		= "SELECT * FROM pqt_publicacion Pb WHERE Pb.tipo_publicacion='O' AND Pb.fecha_caducidad = DATE_ADD( CURRENT_DATE( ) , INTERVAL -1 DAY )";
	        
	        return $controladoraBD->ejecutarconsulta($consulta);
		}
		function buscar_paquetes_vacacionales_a_vencerse_en_siete_dias()
		{
			$controladoraBD	= new controladoraBD;
	        $consulta 		= "SELECT Pb.* FROM pqt_publicacion Pb 
	        					WHERE Pb.tipo_publicacion='P' AND Pb.fecha_caducidad < DATE_ADD( CURRENT_DATE( ) , INTERVAL 7 DAY ) AND Pb.estado='1'";
	        
	        return $controladoraBD->ejecutarconsulta($consulta);
		}
		function buscar_paquetes_eventos_a_vencerse_en_siete_dias()
		{
			$controladoraBD	= new controladoraBD;
	        $consulta 		= "SELECT Pb.* FROM pqt_publicacion Pb 
	        					WHERE Pb.tipo_publicacion='E' AND Pb.fecha_caducidad < DATE_ADD( CURRENT_DATE( ) , INTERVAL 7 DAY ) AND Pb.estado='1'";
	        
	        return $controladoraBD->ejecutarconsulta($consulta);	
		}
		function buscar_promociones_a_vencerse_en_siete_dias()
		{
			$controladoraBD	= new controladoraBD;
	        $consulta 		= "SELECT Pb.* FROM pqt_publicacion Pb 
	        					WHERE Pb.tipo_publicacion='O' AND Pb.fecha_caducidad < DATE_ADD( CURRENT_DATE( ) , INTERVAL 7 DAY ) AND Pb.estado='1'";
	        
	        return $controladoraBD->ejecutarconsulta($consulta);
		}
	/*
	* OBTENER RELACIONES DE PUBLICACION CON CRITERIOS DE BUSQUEDA Y OPERADORAS
	*/
	function obtener_operadoras_por_publicacion($idpublicacion)
	{
		$controladoraBD= new controladoraBD;
        $consulta = "SELECT * FROM pqt_publicacion_has_operadora Po, pqt_operadora O 
        			WHERE Po.idoperadora=O.idoperadora AND Po.idpublicacion=$idpublicacion";
        
        return $controladoraBD->ejecutarconsulta($consulta);	
	}
	function obtener_alcances_evento_por_publicacion($idpublicacion)
	{
		$controladoraBD= new controladoraBD;
        $consulta = "SELECT * FROM pqt_publicacion_has_alcance_evento Pa, pqt_alcance_evento A 
        			WHERE Pa.idalcance_evento=A.idalcance_evento AND Pa.idpublicacion=$idpublicacion";
        
        return $controladoraBD->ejecutarconsulta($consulta);	
	}
	function obtener_especialidades_por_publicacion($idpublicacion)
	{
		$controladoraBD= new controladoraBD;
        $consulta = "SELECT * FROM pqt_publicacion_has_especialidades Pe, especialidades E 
        			WHERE Pe.idespecialidades=E.idespecialidades AND Pe.idpublicacion=$idpublicacion";
        
        return $controladoraBD->ejecutarconsulta($consulta);	
	}
	function obtener_destinos_por_publicacion($idpublicacion)
	{
		$controladoraBD= new controladoraBD;
        $consulta = "SELECT * FROM pqt_publicacion_has_destino Pd, pqt_destino D 
        			WHERE Pd.iddestino=D.iddestino AND Pd.idpublicacion=$idpublicacion";
        
        return $controladoraBD->ejecutarconsulta($consulta);	
	}
	function obtener_temporadas_por_publicacion($idpublicacion)
	{
		$controladoraBD= new controladoraBD;
        $consulta = "SELECT * FROM pqt_publicacion_has_temporada Pt, pqt_temporada T 
        			WHERE Pt.idtemporada=T.idtemporada AND Pt.idpublicacion=$idpublicacion";
        
        return $controladoraBD->ejecutarconsulta($consulta);	
	}
	function obtener_tipos_evento_por_publicacion($idpublicacion)
	{
		$controladoraBD= new controladoraBD;
        $consulta = "SELECT * FROM pqt_publicacion_has_tipo_evento Pt, pqt_tipo_evento T 
        			WHERE Pt.idtipo_evento=T.idtipo_evento AND Pt.idpublicacion=$idpublicacion";
        
        return $controladoraBD->ejecutarconsulta($consulta);	
	}
	function obtener_tipos_paquete_por_publicacion($idpublicacion)
	{
		$controladoraBD= new controladoraBD;
        $consulta = "SELECT * FROM pqt_publicacion_has_tipo_paquete Pt, pqt_tipo_paquete T 
        			WHERE Pt.idtipo_paquete=T.idtipo_paquete AND Pt.idpublicacion=$idpublicacion";
        
        return $controladoraBD->ejecutarconsulta($consulta);	
	}
	/*
	* OBTENER IMAGENES DE LA PUBLICACION
	*/
	function obtener_imagenes_por_publicacion($idpublicacion)
	{
		$controladoraBD= new controladoraBD;
        $consulta = "SELECT * FROM pqt_publicacion_has_imagen Pi, pqt_imagen I 
        			WHERE Pi.idimagen=I.idimagen AND Pi.idpublicacion=$idpublicacion";
        
        return $controladoraBD->ejecutarconsulta($consulta);
	}
	/*
	* OBTENER ARCHIVOS ADJUNTOS DE UNA PUBLICACION
	*/
	function obtener_archivos_por_publicacion($idpublicacion)
	{
		$controladoraBD= new controladoraBD;
        $consulta = "SELECT * FROM pqt_publicacion_has_adjunto Pa, pqt_archivo_adjunto A 
        			WHERE Pa.idarchivo_adjunto=A.idarchivo_adjunto AND Pa.idpublicacion=$idpublicacion";
        
        return $controladoraBD->ejecutarconsulta($consulta);	
	}
	/*
	* OBTENER CIUDADES DE EL PAQUETE
	*/
	function obtener_ciudades_origen_publicacion($idpublicacion)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT * FROM pqt_ciudad_origen_paquete C, pqt_publicacion_has_ciudad_origen P 
							WHERE C.idciudad_origen_paquete=P.idciudad_origen_paquete AND P.idpublicacion=$idpublicacion";

		return $controladoraBD->ejecutarconsulta($consulta);	
	}
	/*
	* OBTENER EVENTOS DEL PAQUETE
	*/
	function obtener_evento_x_publicacion($idpublicacion)
	{
		$controladoraBD = new controladoraBD;
		$consulta		= "SELECT * FROM pqt_publicacion_has_evento P, pqt_evento E 
							WHERE P.idevento=E.idevento AND P.idpublicacion=$idpublicacion";

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	/*
	* BUSQUEDA DE PUBLICACION POR PALABRAS
	*/

		/*
		* CARGAR PALABRAS
		*/
		function obtener_paquetes($tipo)
	    {
	        $controladoraBD= new controladoraBD;
	        $consulta = "SELECT * FROM pqt_publicacion P WHERE P.tipo_publicacion='$tipo' AND P.estado='1' ORDER BY P.titulo";
	        //echo $consulta;
	        return $controladoraBD->ejecutarconsulta($consulta);   
	    }

		/*
		* BUSQUEDAS POR PALABRA CLAVE
		*/		
		function obtener_paquete_por_palabra_clave_titulo($palabra, $estado, $tipo)
	    {
	        $controladoraBD = new controladoraBD;
	        $consulta = "SELECT Pb.*, U.username  
	                        FROM pqt_publicacion Pb, 
	                            User U 
	                        WHERE Pb.estado='$estado' AND Pb.tipo_publicacion='$tipo' AND Pb.idusuarios=U.id AND (
	                                Pb.titulo LIKE('%$palabra%') 
	                            ) GROUP BY Pb.idpublicacion";
	        //echo $consulta . "<br/>";
	        return $controladoraBD->ejecutarconsulta($consulta);
	    }
	    function obtener_paquete_por_palabra_clave_tipo_paquete($palabra, $estado, $tipo)
	    {
	        $controladoraBD = new controladoraBD;
	        $consulta = "SELECT Pb.*, U.username  
	                        FROM pqt_publicacion Pb, 
	                             pqt_publicacion_has_tipo_paquete Ptp, pqt_tipo_paquete Tp, 
	                            User U 
	                        WHERE Pb.estado='$estado' AND Pb.tipo_publicacion='$tipo' AND Pb.idusuarios=U.id AND Ptp.idpublicacion=Pb.idpublicacion AND Ptp.idtipo_paquete=Tp.idtipo_paquete AND Tp.descripcion LIKE('%$palabra%') GROUP BY Pb.idpublicacion";
	        //echo $consulta . "<br/>";
	        return $controladoraBD->ejecutarconsulta($consulta);
	    }
	    function obtener_paquete_por_palabra_clave_temporada($palabra, $estado, $tipo)
	    {
	        $controladoraBD = new controladoraBD;
	        $consulta = "SELECT Pb.*, U.username  
	                        FROM pqt_publicacion Pb, 
	                            pqt_publicacion_has_temporada Pht, pqt_temporada T, 
	                            User U 
	                        WHERE Pb.estado='$estado' AND Pb.tipo_publicacion='$tipo' AND Pb.idusuarios=U.id AND Pht.idpublicacion=Pb.idpublicacion AND Pht.idtemporada=T.idtemporada AND T.nombre_temporada LIKE('%$palabra%') 
	                        GROUP BY Pb.idpublicacion";
	        //echo $consulta . "<br/>";
	        return $controladoraBD->ejecutarconsulta($consulta);
	    }
	    function obtener_paquete_por_palabra_clave_destino($palabra, $estado, $tipo)
	    {
	        $controladoraBD = new controladoraBD;
	        $consulta = "SELECT Pb.*, U.username  
	                        FROM pqt_publicacion Pb, 
	                            pqt_publicacion_has_destino Phd, pqt_destino D, 
	                            User U 
	                        WHERE Pb.estado='$estado' AND Pb.tipo_publicacion='$tipo' AND Pb.idusuarios=U.id AND Phd.idpublicacion=Pb.idpublicacion AND Phd.iddestino=D.iddestino AND D.nombre_destino LIKE('%$palabra%') 
	                        GROUP BY Pb.idpublicacion";
	        //echo $consulta . "<br/>";
	        return $controladoraBD->ejecutarconsulta($consulta);
	    }
	    function obtener_evento_por_palabra_clave_tipo_evento($palabra, $estado, $tipo)
	    {
	        $controladoraBD = new controladoraBD;
	        $consulta = "SELECT Pb.*, U.username 
	                        FROM pqt_publicacion Pb, 
	                             pqt_publicacion_has_tipo_evento Ete, pqt_tipo_evento Te, 
	                            User U 
	                        WHERE Pb.estado='$estado' AND Pb.tipo_publicacion='$tipo' AND Pb.idusuarios=U.id AND Ete.idpublicacion=Pb.idpublicacion AND Ete.idtipo_evento=Te.idtipo_evento AND Te.descripcion LIKE('%$palabra%') GROUP BY Pb.idpublicacion";
	        //echo $consulta . "<br/>";
	        return $controladoraBD->ejecutarconsulta($consulta);
	    }
	    function obtener_evento_por_palabra_clave_alcance($palabra, $estado, $tipo)
	    {
	        $controladoraBD = new controladoraBD;
	        $consulta = "SELECT Pb.*, U.username 
	                        FROM pqt_publicacion Pb, 
	                            pqt_publicacion_has_alcance_evento Eha, pqt_alcance_evento Ae, 
	                            User U 
	                        WHERE Pb.estado='$estado' AND Pb.tipo_publicacion='$tipo' AND Pb.idusuarios=U.id AND Eha.idpublicacion=Pb.idpublicacion AND Eha.idalcance_evento=Ae.idalcance_evento AND Ae.descripcion LIKE('%$palabra%') GROUP BY Pb.idpublicacion";
	        //echo $consulta . "<br/>";
	        return $controladoraBD->ejecutarconsulta($consulta);
	    }
	    function obtener_evento_por_palabra_clave_evento($palabra, $estado, $tipo)
	    {
	        $controladoraBD = new controladoraBD;
	        $consulta = "SELECT Pb.*, U.username 
	                        FROM pqt_publicacion Pb, 
	                            pqt_publicacion_has_evento Ehe, pqt_evento E, 
	                            User U 
	                        WHERE Pb.estado='$estado' AND Pb.tipo_publicacion='$tipo' AND Pb.idusuarios=U.id AND Ehe.idpublicacion=Pb.idpublicacion AND Ehe.idevento=E.idevento AND (E.nombre_evento LIKE('%$palabra%') OR E.sigla_evento LIKE('%$palabra%') OR E.sede LIKE('%$palabra%')) GROUP BY Pb.idpublicacion";
	        //echo $consulta . "<br/>";
	        return $controladoraBD->ejecutarconsulta($consulta);
	    }
	    /*function obtener_evento_por_palabra_clave_especialidad($palabra, $estado, $tipo)
	    {
	        $controladoraBD = new controladoraBD;
	        $consulta = "SELECT Pb.idpublicacion, Pb.titulo, Pb.publicado, Pb.resaltar, Pb.fecha_salida, Pb.fecha_retorno, Pb.fecha_publicacion, Pb.fecha_caducidad, U.nombre_usuario 
	                        FROM publicacion Pb, 
	                            evento_has_especialidades Ehe, especialidades E, 
	                            usuarios U 
	                        WHERE Pb.idpublicacion=Ev.idpublicacion AND Pb.estado='$estado' AND Pb.tipo_publicacion='E' AND Pb.idusuarios=U.idusuarios AND Ehe.idevento=Ev.idpublicacion AND Ehe.idespecialidades=E.idespecialidades AND E.nombre_especialidad LIKE('%$palabra%') GROUP BY Pb.idpublicacion";
	        //echo $consulta . "<br/>";
	        return $controladoraBD->ejecutarconsulta($consulta);
	    }*/
	    /*
	    * RESUMENES DE PAQUETES
	    */
	    function obtener_cantidad_de_envios_masivos($idpublicacion)
	    {
	    	$controladoraBD = new controladoraBD;
	        $consulta = "SELECT COUNT(E.idenvio_masivo) AS cantidad 
	                        FROM pqt_envio_masivo_paquetes E, pqt_envio_masivo_has_publicacion P  
	                        WHERE E.idenvio_masivo=P.idenvio_masivo AND P.idpublicacion=$idpublicacion";
	        //echo $consulta . "<br/>";
	        return $controladoraBD->ejecutarconsulta($consulta);
	    }
	    function obtener_cantidad_de_cotizaciones($idpublicacion)
	    {
	    	$controladoraBD = new controladoraBD;
	        $consulta = "SELECT COUNT(C.idcotizacion) AS cantidad 
	                        FROM pqt_cotizacion C, pqt_cotizacion_has_publicacion P  
	                        WHERE C.idcotizacion=P.idcotizacion AND P.idpublicacion=$idpublicacion";
	        //echo $consulta . "<br/>";
	        return $controladoraBD->ejecutarconsulta($consulta);	
	    }
	    /*
	    * OBTENER COTIZACIONES DE PUBLICACION
	    */
	    function obtener_cotizaciones_de_publicacion($idpublicacion)
	    {
	    	$controladoraBD = new controladoraBD;
	        $consulta = "SELECT * 
	                        FROM pqt_cotizacion C, pqt_cotizacion_has_publicacion P  
	                        WHERE C.idcotizacion=P.idcotizacion AND P.idpublicacion=$idpublicacion";
	        //echo $consulta . "<br/>";
	        return $controladoraBD->ejecutarconsulta($consulta);		
	    }

	    /*FUNCTION PARA BUSQUEDA PAQUETE ZION*/
	    function obtener_paquetes_x_00_00_00_00($pag,$max)//0000
		{
			$controladoraBD= new controladoraBD;
	        $consulta = "SELECT * FROM pqt_publicacion 
	        			WHERE pqt_publicacion.estado = '1'
	        			ORDER BY pqt_publicacion.idpublicacion DESC
	        			LIMIT $pag,$max";
	        return $controladoraBD->ejecutarconsulta($consulta);
		}

		function obtener_paquetes_x_00_00_00_ca($ca,$pag,$max)//0001
		{
			$controladoraBD= new controladoraBD;
	        $consulta = "SELECT * FROM pqt_publicacion 
						INNER JOIN pqt_publicacion_has_tipo_paquete ON pqt_publicacion.idpublicacion = pqt_publicacion_has_tipo_paquete.idpublicacion
						WHERE pqt_publicacion.estado = '1' 
						AND pqt_publicacion_has_tipo_paquete.idtipo_paquete = '$ca'
						ORDER BY pqt_publicacion.idpublicacion DESC 
						LIMIT $pag,$max";
	        return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function obtener_paquetes_x_00_00_te_00($te,$pag,$max)//0010
		{
			$controladoraBD= new controladoraBD;
	        $consulta = "SELECT * FROM pqt_publicacion
	        			INNER JOIN pqt_publicacion_has_temporada ON pqt_publicacion.idpublicacion = pqt_publicacion_has_temporada.idpublicacion
	        			WHERE pqt_publicacion.estado = '1'
	        			AND pqt_publicacion_has_temporada.idtemporada = '$te'
	        			ORDER BY pqt_publicacion.idpublicacion DESC
	        			LIMIT $pag,$max";
	        return $controladoraBD->ejecutarconsulta($consulta);
		}

		function obtener_paquetes_x_00_00_te_ca($te,$ca,$pag,$max)//0011
		{
			$controladoraBD= new controladoraBD;
	        $consulta = "SELECT * FROM pqt_publicacion
	        			INNER JOIN pqt_publicacion_has_temporada ON pqt_publicacion.idpublicacion = pqt_publicacion_has_temporada.idpublicacion
	        			INNER JOIN pqt_publicacion_has_tipo_paquete ON pqt_publicacion.idpublicacion = pqt_publicacion_has_tipo_paquete.idpublicacion
	        			WHERE pqt_publicacion.estado = '1'
	        			AND pqt_publicacion_has_temporada.idtemporada = '$te'
	        			AND pqt_publicacion_has_tipo_paquete.idtipo_paquete = '$ca'
	        			ORDER BY pqt_publicacion.idpublicacion DESC
	        			LIMIT $pag,$max";
	        return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function obtener_paquetes_x_00_de_00_00($de,$pag,$max)//0100
		{
			$controladoraBD= new controladoraBD;
	        $consulta = "SELECT * FROM pqt_publicacion
	        			INNER JOIN pqt_publicacion_has_destino ON pqt_publicacion.idpublicacion = pqt_publicacion_has_destino.idpublicacion 
	        			WHERE pqt_publicacion.estado = '1'
	        			AND pqt_publicacion_has_destino.iddestino= '$de'
	        			ORDER BY pqt_publicacion.idpublicacion DESC
	        			LIMIT $pag,$max";
	        return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function obtener_paquetes_x_00_de_00_ca($de,$ca,$pag,$max)//0101
		{
			$controladoraBD= new controladoraBD;
	        $consulta = "SELECT * FROM pqt_publicacion
	        			INNER JOIN pqt_publicacion_has_destino ON pqt_publicacion.idpublicacion = pqt_publicacion_has_destino.idpublicacion 
	        			INNER JOIN pqt_publicacion_has_tipo_paquete ON pqt_publicacion.idpublicacion = pqt_publicacion_has_tipo_paquete.idpublicacion
	        			WHERE pqt_publicacion.estado = '1'
	        			AND pqt_publicacion_has_destino.iddestino= '$de'
	        			AND pqt_publicacion_has_tipo_paquete.idtipo_paquete = '$ca'
	        			ORDER BY pqt_publicacion.idpublicacion DESC
	        			LIMIT $pag,$max";
	        return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function obtener_paquetes_x_00_de_te_00($de,$te,$pag,$max)//0110
		{
			$controladoraBD= new controladoraBD;
	        $consulta = "SELECT * FROM pqt_publicacion
	        			INNER JOIN pqt_publicacion_has_destino ON pqt_publicacion.idpublicacion = pqt_publicacion_has_destino.idpublicacion 
	        			INNER JOIN pqt_publicacion_has_temporada ON pqt_publicacion.idpublicacion = pqt_publicacion_has_temporada.idpublicacion
	        			WHERE pqt_publicacion.estado = '1'
	        			AND pqt_publicacion_has_destino.iddestino= '$de'
	        			AND pqt_publicacion_has_temporada.idtemporada = '$te'
	        			ORDER BY pqt_publicacion.idpublicacion DESC
	        			LIMIT $pag,$max";
	        return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function obtener_paquetes_x_00_de_te_ca($de,$te,$ca,$pag,$max)//0111
		{
			$controladoraBD= new controladoraBD;
	        $consulta = "SELECT * FROM pqt_publicacion
	        			INNER JOIN pqt_publicacion_has_destino ON pqt_publicacion.idpublicacion = pqt_publicacion_has_destino.idpublicacion 
	        			INNER JOIN pqt_publicacion_has_temporada ON pqt_publicacion.idpublicacion = pqt_publicacion_has_temporada.idpublicacion
	        			INNER JOIN pqt_publicacion_has_tipo_paquete ON pqt_publicacion.idpublicacion = pqt_publicacion_has_tipo_paquete.idpublicacion
	        			WHERE pqt_publicacion.estado = '1'
	        			AND pqt_publicacion_has_destino.iddestino= '$de'
	        			AND pqt_publicacion_has_temporada.idtemporada = '$te'
	        			AND pqt_publicacion_has_tipo_paquete.idtipo_paquete = '$ca'
	        			ORDER BY pqt_publicacion.idpublicacion DESC
	        			LIMIT $pag,$max";
	        return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function obtener_paquetes_x_ti_00_00_00($ti,$pag,$max)//1000
		{
			$controladoraBD= new controladoraBD;
	        $consulta = "SELECT * FROM pqt_publicacion
	        			WHERE pqt_publicacion.estado = '1'
	        			AND pqt_publicacion.titulo LIKE('%$ti%') 
	        			ORDER BY pqt_publicacion.idpublicacion DESC
	        			LIMIT $pag,$max";
	        return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function obtener_paquetes_x_ti_00_00_ca($ti,$ca,$pag,$max)//1001
		{
			$controladoraBD= new controladoraBD;
	        $consulta = "SELECT * FROM pqt_publicacion
	        			INNER JOIN pqt_publicacion_has_tipo_paquete ON pqt_publicacion.idpublicacion = pqt_publicacion_has_tipo_paquete.idpublicacion
	        			WHERE pqt_publicacion.estado = '1'
	        			AND pqt_publicacion.titulo LIKE('%$ti%')
	        			AND pqt_publicacion_has_tipo_paquete.idtipo_paquete = '$ca'
	        			ORDER BY pqt_publicacion.idpublicacion DESC
	        			LIMIT $pag,$max";
	        return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function obtener_paquetes_x_ti_00_te_00($ti,$te,$pag,$max)//1010
		{
			$controladoraBD= new controladoraBD;
	        $consulta = "SELECT * FROM pqt_publicacion
	        			INNER JOIN pqt_publicacion_has_temporada ON pqt_publicacion.idpublicacion = pqt_publicacion_has_temporada.idpublicacion
	        			WHERE pqt_publicacion.estado = '1'
	        			AND pqt_publicacion.titulo LIKE('%$ti%')
	        			AND pqt_publicacion_has_temporada.idtemporada = '$te'
	        			ORDER BY pqt_publicacion.idpublicacion DESC
	        			LIMIT $pag,$max";
	        return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function obtener_paquetes_x_ti_00_te_ca($ti,$te,$ca,$pag,$max)//1011
		{
			$controladoraBD= new controladoraBD;
	        $consulta = "SELECT * FROM pqt_publicacion
	        			INNER JOIN pqt_publicacion_has_temporada ON pqt_publicacion.idpublicacion = pqt_publicacion_has_temporada.idpublicacion
	        			INNER JOIN pqt_publicacion_has_tipo_paquete ON pqt_publicacion.idpublicacion = pqt_publicacion_has_tipo_paquete.idpublicacion
	        			WHERE pqt_publicacion.estado = '1'
	        			AND pqt_publicacion.titulo LIKE('%$ti%')
	        			AND pqt_publicacion_has_temporada.idtemporada = '$te'
	        			AND pqt_publicacion_has_tipo_paquete.idtipo_paquete = '$ca'
	        			ORDER BY pqt_publicacion.idpublicacion DESC
	        			LIMIT $pag,$max";
	        return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function obtener_paquetes_x_ti_de_00_00($ti,$de,$pag,$max)//1100
		{
			$controladoraBD= new controladoraBD;
	        $consulta = "SELECT * FROM pqt_publicacion
	        			INNER JOIN pqt_publicacion_has_destino ON pqt_publicacion.idpublicacion = pqt_publicacion_has_destino.idpublicacion 
	        			WHERE pqt_publicacion.estado = '1'
	        			AND pqt_publicacion.titulo LIKE('%$ti%')
	        			AND pqt_publicacion_has_destino.iddestino= '$de'
	        			ORDER BY pqt_publicacion.idpublicacion DESC
	        			LIMIT $pag,$max";
	        return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function obtener_paquetes_x_ti_de_00_ca($ti,$de,$ca,$pag,$max)//1101
		{
			$controladoraBD= new controladoraBD;
	        $consulta = "SELECT * FROM pqt_publicacion
	        			INNER JOIN pqt_publicacion_has_destino ON pqt_publicacion.idpublicacion = pqt_publicacion_has_destino.idpublicacion 
	        			INNER JOIN pqt_publicacion_has_tipo_paquete ON pqt_publicacion.idpublicacion = pqt_publicacion_has_tipo_paquete.idpublicacion
	        			WHERE pqt_publicacion.estado = '1'
	        			AND pqt_publicacion.titulo LIKE('%$ti%')
	        			AND pqt_publicacion_has_destino.iddestino= '$de'
	        			AND pqt_publicacion_has_tipo_paquete.idtipo_paquete = '$ca'
	        			ORDER BY pqt_publicacion.idpublicacion DESC
	        			LIMIT $pag,$max";
	        return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function obtener_paquetes_x_ti_de_te_00($ti,$de,$te,$pag,$max)//1110
		{
			$controladoraBD= new controladoraBD;
	        $consulta = "SELECT * FROM pqt_publicacion
	        			INNER JOIN pqt_publicacion_has_destino ON pqt_publicacion.idpublicacion = pqt_publicacion_has_destino.idpublicacion 
	        			INNER JOIN pqt_publicacion_has_temporada ON pqt_publicacion.idpublicacion = pqt_publicacion_has_temporada.idpublicacion
	        			WHERE pqt_publicacion.estado = '1'
	        			AND pqt_publicacion.titulo LIKE('%$ti%')
	        			AND pqt_publicacion_has_destino.iddestino= '$de'
	        			AND pqt_publicacion_has_temporada.idtemporada = '$te'
	        			ORDER BY pqt_publicacion.idpublicacion DESC
	        			LIMIT $pag,$max";
	        return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function obtener_paquetes_x_ti_de_te_ca($ti,$de,$te,$ca,$pag,$max)//1111
		{
			$controladoraBD= new controladoraBD;
	        $consulta = "SELECT * FROM pqt_publicacion
	        			INNER JOIN pqt_publicacion_has_destino ON pqt_publicacion.idpublicacion = pqt_publicacion_has_destino.idpublicacion 
	        			INNER JOIN pqt_publicacion_has_temporada ON pqt_publicacion.idpublicacion = pqt_publicacion_has_temporada.idpublicacion
	        			INNER JOIN pqt_publicacion_has_tipo_paquete ON pqt_publicacion.idpublicacion = pqt_publicacion_has_tipo_paquete.idpublicacion
	        			WHERE pqt_publicacion.estado = '1'
	        			AND pqt_publicacion.titulo LIKE('%$ti%')
	        			AND pqt_publicacion_has_destino.iddestino= '$de'
	        			AND pqt_publicacion_has_temporada.idtemporada = '$te'
	        			AND pqt_publicacion_has_tipo_paquete.idtipo_paquete = '$ca'
	        			ORDER BY pqt_publicacion.idpublicacion DESC
	        			LIMIT $pag,$max";
	        return $controladoraBD->ejecutarconsulta($consulta);
		}



		/*COUNTAR EL RESULTADO DE LA CONSULTA DE LA BUSQUEDA ZION*/

		function contar_paquetes_x_($pag,$max)
		{
			$controladoraBD= new controladoraBD;
	        $consulta = "SELECT COUNT(pqt_publicacion.idpublicacion) as cantidad FROM pqt_publicacion 
	        			WHERE pqt_publicacion.estado = '1'";
	        return $controladoraBD->ejecutarconsulta($consulta);
		}

		function traer_publicacion_activo_x_destino()
		{
			$controladoraBD= new controladoraBD;
	        $consulta = "SELECT pqt_publicacion_has_destino.*,pqt_destino.nombre_destino FROM pqt_publicacion_has_destino
						INNER JOIN pqt_destino ON pqt_publicacion_has_destino.iddestino = pqt_destino.iddestino
						INNER JOIN pqt_publicacion ON pqt_publicacion_has_destino.idpublicacion = pqt_publicacion.idpublicacion
						WHERE pqt_publicacion.estado = 1
						ORDER BY pqt_publicacion_has_destino.idpublicacion DESC";
	        return $controladoraBD->ejecutarconsulta($consulta);
		}

		function traer_publicacion_activo_x_temporada()
		{
			$controladoraBD= new controladoraBD;
	        $consulta = "SELECT pqt_publicacion_has_temporada.*,pqt_temporada.nombre_temporada FROM pqt_publicacion_has_temporada
						INNER JOIN pqt_temporada ON pqt_publicacion_has_temporada.idtemporada = pqt_temporada.idtemporada
						INNER JOIN pqt_publicacion ON pqt_publicacion_has_temporada.idpublicacion = pqt_publicacion.idpublicacion
						WHERE pqt_publicacion.estado = 1
						ORDER BY pqt_publicacion_has_temporada.idpublicacion DESC";
	        return $controladoraBD->ejecutarconsulta($consulta);
		}

		function traer_publicacion_activo_x_categoria()
		{
			$controladoraBD= new controladoraBD;
	        $consulta = "SELECT pqt_publicacion_has_tipo_paquete.*,pqt_tipo_paquete.descripcion FROM pqt_publicacion_has_tipo_paquete
						INNER JOIN pqt_tipo_paquete ON pqt_publicacion_has_tipo_paquete.idtipo_paquete= pqt_tipo_paquete.idtipo_paquete
						INNER JOIN pqt_publicacion ON pqt_publicacion_has_tipo_paquete.idpublicacion = pqt_publicacion.idpublicacion
						WHERE pqt_publicacion.estado = 1
						ORDER BY pqt_publicacion_has_tipo_paquete.idpublicacion DESC";
	        return $controladoraBD->ejecutarconsulta($consulta);
		}

}

?>