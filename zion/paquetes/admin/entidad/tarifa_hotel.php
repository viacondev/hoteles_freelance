<?php
/**
* 
*/
class tarifa_hotel
{
	function obtener_tarifas($id, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT * FROM tarifa_hotel_" . $type . " T WHERE T.id" . $type . "=$id ORDER BY T.idtarifa_hotel_" . $type . " ASC" ;

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function registrar_tarifa_hotel($id, $destino, $in, $out, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "INSERT INTO tarifa_hotel_" . $type . "(id" . $type . ", destino, `in`, `out`) 
							VALUES('$id', '$destino', '$in', '$out')";
//echo $consulta;
		return $controladoraBD->ejecutarconsulta($consulta);	
	}
	function modificar_tarifa_hotel($id, $destino, $in, $out, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "UPDATE tarifa_hotel_" . $type . " 
							SET destino='$destino', `in`='$in', `out`='$out' 
							WHERE idtarifa_hotel_" . $type . "='$id'";
//echo $consulta;
		return $controladoraBD->ejecutarconsulta($consulta);	
	}

	/*function registrar_tarifa_hotel($id, $cant_noches, $destino, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "INSERT INTO tarifa_hotel_" . $type . "(id" . $type . ", cant_noches, destino) 
							VALUES('$id', '$cant_noches', '$destino')";
//echo $consulta;
		return $controladoraBD->ejecutarconsulta($consulta);	
	}
	function modificar_tarifa_hotel($id, $cant_noches, $destino, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "UPDATE tarifa_hotel_" . $type . " 
							SET cant_noches='$cant_noches', destino='$destino' 
							WHERE idtarifa_hotel_" . $type . "='$id'";

		return $controladoraBD->ejecutarconsulta($consulta);	
	}*/
	function eliminar_tarifa_hotel($id, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "DELETE FROM tarifa_hotel_" . $type . " 
							WHERE idtarifa_hotel_" . $type . "='$id'";

		return $controladoraBD->ejecutarconsulta($consulta);	
	}

	function obtener_items_por_tarifa_hotel($idtarifa_hotel, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT * FROM item_hotel_" . $type . " I, hotel H WHERE I.idhotel=H.idhotel AND I.idtarifa_hotel_" . $type . "=$idtarifa_hotel";

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function registrar_item_hotel($idtarifa_hotel, $identificador, $idhotel, $link, $precio_single, $precio_doble, $precio_triple, $precio_cuadruple, $precio_menor, $precio_infante, $observacion, $alimentacion, $comision, $factura, $tipo_cambio, $time_limit, $info_extra, $estado, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "INSERT INTO item_hotel_" . $type . "(idtarifa_hotel_" . $type . ", identificador, idhotel, link, precio_single, precio_doble, precio_triple, precio_cuadruple, precio_menor, precio_infante, observacion, alimentacion, comision, factura, tipo_cambio, time_limit, info_extra, estado) 
							VALUES('$idtarifa_hotel', '$identificador', '$idhotel', '$link', '$precio_single', '$precio_doble', '$precio_triple', '$precio_cuadruple', '$precio_menor', '$precio_infante', '$observacion', '$alimentacion', '$comision', '$factura', '$tipo_cambio', '$time_limit', '$info_extra', '$estado')";
							//echo $consulta;
		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function modificar_item_hotel($iditem_hotel, $idtarifa_hotel, $identificador, $idhotel, $link, $precio_single, $precio_doble, $precio_triple, $precio_cuadruple, $precio_menor, $precio_infante, $observacion, $alimentacion, $comision, $factura, $tipo_cambio, $time_limit, $info_extra, $estado, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "UPDATE item_hotel_" . $type . " 
							SET identificador='$identificador', idhotel='$idhotel', link='$link', precio_single='$precio_single', precio_doble='$precio_doble', precio_triple='$precio_triple', precio_cuadruple='$precio_cuadruple', precio_menor='$precio_menor', precio_infante='$precio_infante', observacion='$observacion', alimentacion='$alimentacion', comision='$comision', factura='$factura', tipo_cambio='$tipo_cambio', time_limit='$time_limit', info_extra='$info_extra', estado='$estado'  
							WHERE iditem_hotel_" . $type . "='$iditem_hotel'";
//echo $consulta;
		return $controladoraBD->ejecutarconsulta($consulta);
	}

	/*function registrar_item_hotel($idtarifa_hotel, $idhotel, $link, $precio_single, $precio_doble, $precio_triple, $precio_cuadruple, $precio_menor, $precio_infante, $observacion, $alimentacion, $comision, $factura, $info_extra, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "INSERT INTO item_hotel_" . $type . "(idtarifa_hotel_" . $type . ", idhotel, link, precio_single, precio_doble, precio_triple, precio_cuadruple, precio_menor, precio_infante, observacion, alimentacion, comision, factura, info_extra) 
							VALUES('$idtarifa_hotel', '$idhotel', '$link', '$precio_single', '$precio_doble', '$precio_triple', '$precio_cuadruple', '$precio_menor', '$precio_infante', '$observacion', '$alimentacion', '$comision', '$factura', '$info_extra')";
							
		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function modificar_item_hotel($iditem_hotel, $idtarifa_hotel, $idhotel, $link, $precio_single, $precio_doble, $precio_triple, $precio_cuadruple, $precio_menor, $precio_infante, $observacion, $alimentacion, $comision, $factura, $info_extra, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "UPDATE item_hotel_" . $type . " 
							SET idhotel='$idhotel', link='$link', precio_single='$precio_single', precio_doble='$precio_doble', precio_triple='$precio_triple', precio_cuadruple='$precio_cuadruple', precio_menor='$precio_menor', precio_infante='$precio_infante', observacion='$observacion', alimentacion='$alimentacion', comision='$comision', factura='$factura', info_extra='$info_extra'  
							WHERE iditem_hotel_" . $type . "='$iditem_hotel'";

		return $controladoraBD->ejecutarconsulta($consulta);
	}*/

	function eliminar_items_hotel_por_tarifa($idtarifa_hotel, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "DELETE FROM item_hotel_" . $type . " 
							WHERE idtarifa_hotel_" . $type . "='$idtarifa_hotel'";

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function eliminar_item_hotel($iditem_hotel, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "DELETE FROM item_hotel_" . $type . " 
							WHERE iditem_hotel_" . $type . "='$iditem_hotel'";

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	/*
	* TARIFAS DE HOTEL NUEVO FORMATO
	*/

		function obtener_tarifas_hotel_por_ciudad_origen($idciudad_origen)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "SELECT * FROM pqt_tarifa_hotel T WHERE T.idciudad_origen_paquete=$idciudad_origen ORDER BY T.orden, T.idtarifa_hotel";

			return $controladoraBD->ejecutarconsulta($consulta);
		}
		function obtener_hoteles_x_destino($idtarifa_hotel)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "SELECT * FROM pqt_item_hotel I, pqt_hotel H WHERE I.idhotel=H.idhotel AND I.idtarifa_hotel=$idtarifa_hotel ORDER BY I.precio_single, I.precio_doble, I.precio_triple, I.precio_cuadruple";

			return $controladoraBD->ejecutarconsulta($consulta);	
		}
		function obtener_hoteles_x_destino_real_cotizacion($idtarifa_hotel, $is_real)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "SELECT * FROM pqt_item_hotel I, pqt_hotel H 
								WHERE I.idhotel=H.idhotel AND I.idtarifa_hotel=$idtarifa_hotel AND I.is_real=$is_real ORDER BY I.precio_single, I.precio_doble, I.precio_triple, I.precio_cuadruple";

			return $controladoraBD->ejecutarconsulta($consulta);	
		}
		
		// TARIFAS POR DESTINO

			function registrar_nueva_tarifa_hotel($destino, $orden, $idciudad_origen)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= "INSERT INTO pqt_tarifa_hotel(destino, orden, idciudad_origen_paquete) 
									VALUES('$destino', '$orden', '$idciudad_origen')";
//echo $consulta;
				return $controladoraBD->ejecutarconsulta($consulta);
			}
			function modificar_tarifa_hotel_x_id($destino, $orden, $idciudad_origen, $idtarifa_hotel)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= "UPDATE pqt_tarifa_hotel SET destino='$destino', orden='$orden', idciudad_origen_paquete=$idciudad_origen 
									WHERE idtarifa_hotel=$idtarifa_hotel";

				return $controladoraBD->ejecutarconsulta($consulta);	
			}
			function eliminar_tarifa_hotel_x_id($idtarifa_hotel)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= "DELETE FROM pqt_tarifa_hotel  
									WHERE idtarifa_hotel=$idtarifa_hotel";

				return $controladoraBD->ejecutarconsulta($consulta);		
			}
		
		// ITEMS HOTELES

			function registrar_nuevo_item_hotel($idtarifa_hotel, $idhotel, $identificador, $link, $fecha_in, $fecha_out, $precio_single, $precio_doble, $precio_triple, $precio_cuadruple, $precio_menor, $precio_infante, $observacion, $alimentacion, $fee, $increment_fee, $factura, $increment_factura, $moneda, $tipo_cambio, $time_limit, $info_extra, $is_real, $estado)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= "INSERT INTO pqt_item_hotel(idtarifa_hotel, idhotel, identificador, link, fecha_in, fecha_out, precio_single, precio_doble, precio_triple, precio_cuadruple, precio_menor, precio_infante, observacion, alimentacion, fee, increment_fee, factura, increment_factura, moneda, tipo_cambio, time_limit, info_extra, is_real, estado) 
									VALUES('$idtarifa_hotel', '$idhotel', '$identificador', '$link', '$fecha_in', '$fecha_out', '$precio_single', '$precio_doble', '$precio_triple', '$precio_cuadruple', '$precio_menor', '$precio_infante', '$observacion', '$alimentacion', '$fee', '$increment_fee', '$factura', '$increment_factura', '$moneda', '$tipo_cambio', '$time_limit', '$info_extra', '$is_real', '$estado')";

				return $controladoraBD->ejecutarconsulta($consulta);	
			}
			function modificar_item_hotel_x_id($iditem_hotel, $idhotel, $identificador, $link, $fecha_in, $fecha_out, $precio_single, $precio_doble, $precio_triple, $precio_cuadruple, $precio_menor, $precio_infante, $observacion, $alimentacion, $fee, $increment_fee, $factura, $increment_factura, $moneda, $tipo_cambio, $time_limit, $info_extra, $is_real, $estado)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= "UPDATE pqt_item_hotel SET idhotel='$idhotel', identificador='$identificador', link='$link', fecha_in='$fecha_in', fecha_out='$fecha_out', precio_single='$precio_single', precio_doble='$precio_doble', precio_triple='$precio_triple', precio_cuadruple='$precio_cuadruple', precio_menor='$precio_menor', precio_infante='$precio_infante', observacion='$observacion', alimentacion='$alimentacion', fee='$fee', increment_fee='$increment_fee', factura='$factura', increment_factura='$increment_factura', moneda='$moneda', tipo_cambio='$tipo_cambio', time_limit='$time_limit', info_extra='$info_extra', is_real='$is_real', estado='$estado' 
									WHERE iditem_hotel='$iditem_hotel'";

				return $controladoraBD->ejecutarconsulta($consulta);	
			}
			function eliminar_item_hotel_x_destino($idtarifa_hotel)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= "DELETE FROM pqt_item_hotel  
									WHERE idtarifa_hotel=$idtarifa_hotel";

				return $controladoraBD->ejecutarconsulta($consulta);
			}
			function eliminar_item_hotel_x_id($iditem_hotel)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= "DELETE FROM pqt_item_hotel  
									WHERE iditem_hotel=$iditem_hotel";

				return $controladoraBD->ejecutarconsulta($consulta);
			}

		// COMBINACIONES DE HOTELES

			function registrar_opcion_hoteles($idciudad_origen, $observacion)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= "INSERT INTO pqt_opcion_hoteles(idciudad_origen_paquete, observacion) 
									VALUES('$idciudad_origen', '$observacion')";

				return $controladoraBD->ejecutarconsulta($consulta);
			}
			function registrar_opcion_hoteles_has_item($idopcion_hoteles, $iditem_hotel)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= "INSERT INTO pqt_opcion_hoteles_has_item(idopcion_hoteles, iditem_hotel) 
									VALUES('$idopcion_hoteles', '$iditem_hotel')";

				return $controladoraBD->ejecutarconsulta($consulta);
			}
			function modificar_opcion_hoteles($idopcion_hoteles, $observacion)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= "UPDATE pqt_opcion_hoteles SET observacion='$observacion' WHERE idopcion_hoteles=$idopcion_hoteles";

				return $controladoraBD->ejecutarconsulta($consulta);	
			}
			function eliminar_opcion_hoteles($idopcion_hoteles)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= "DELETE FROM pqt_opcion_hoteles_has_item WHERE idopcion_hoteles=$idopcion_hoteles";

				$controladoraBD->ejecutarconsulta($consulta);

				$controladoraBD = new controladoraBD;
				$consulta 		= "DELETE FROM pqt_opcion_hoteles WHERE idopcion_hoteles=$idopcion_hoteles";

				return $controladoraBD->ejecutarconsulta($consulta);
			}
			function obtener_opcion_hoteles($idciudad_origen)
			{
				$controladoraBD = new controladoraBD;
				$consulta = "SELECT * FROM pqt_opcion_hoteles WHERE idciudad_origen_paquete=$idciudad_origen ORDER BY idopcion_hoteles";
				//echo $consulta;
				return $controladoraBD->ejecutarconsulta($consulta);
			}
			function obtener_detalle_opcion_hoteles($idopcion_hoteles)
			{
				$controladoraBD = new controladoraBD;
				$consulta 		= "SELECT H.*, I.*, T.destino 
									FROM pqt_item_hotel I, pqt_hotel H, pqt_opcion_hoteles_has_item O, pqt_tarifa_hotel T 
									WHERE O.iditem_hotel=I.iditem_hotel AND I.idhotel=H.idhotel AND I.idtarifa_hotel=T.idtarifa_hotel AND O.idopcion_hoteles='$idopcion_hoteles' ORDER BY T.orden";

				return $controladoraBD->ejecutarconsulta($consulta);
			}

		// CONSULTAS PARA REPORTES

			function obtener_destinos_por_cotizacion($idcotizacion)
			{
				$controladoraBD = new controladoraBD;
				$consulta = "SELECT Th.destino, Th.orden, Th.idtarifa_hotel

								FROM 	pqt_cotizacion_has_ciudad_origen Chco, 
										pqt_ciudad_origen_paquete Cop, 
										pqt_tarifa_hotel Th 

								WHERE 	Chco.idciudad_origen_paquete = Cop.idciudad_origen_paquete AND 
										Cop.idciudad_origen_paquete = Th.idciudad_origen_paquete AND 
										Chco.idcotizacion = '$idcotizacion'";
				//echo $consulta;
				return $controladoraBD->ejecutarconsulta($consulta);	
			}
			function obtener_hoteles_por_rango_destinos($destinos, $is_real)
			{
				$controladoraBD = new controladoraBD;
				$consulta = "SELECT Ih.*, H.* 

								FROM pqt_item_hotel Ih, pqt_hotel H 

								WHERE 	Ih.idhotel=H.idhotel AND 
										Ih.idtarifa_hotel IN($destinos) AND 
										Ih.is_real='$is_real'";
				//echo $consulta;
				return $controladoraBD->ejecutarconsulta($consulta);	
			}
			function obtener_id_hoteles_por_pax($idpax)
			{
				$controladoraBD = new controladoraBD;
				$consulta = "SELECT I.* 

								FROM pqt_item_hotel_has_pax I 

								WHERE I.idpax='$idpax'";
				//echo $consulta;
				return $controladoraBD->ejecutarconsulta($consulta);
			}
			function obtener_destinos_x_rango_cotizaciones($cotizaciones)
			{
				$controladoraBD = new controladoraBD;
				$consulta = "SELECT Th.destino   

								FROM 	pqt_item_hotel Ih, 
										pqt_tarifa_hotel Th, 
										pqt_ciudad_origen_paquete Cop, 
										pqt_cotizacion_has_ciudad_origen Chco 

								WHERE 	Ih.idtarifa_hotel=Th.idtarifa_hotel AND 
										Th.idciudad_origen_paquete=Cop.idciudad_origen_paquete AND 
										Cop.idciudad_origen_paquete = Chco.idciudad_origen_paquete AND 
										Chco.idcotizacion IN($cotizaciones) AND 
										Ih.is_real=1 

								GROUP BY Th.destino";
				
				return $controladoraBD->ejecutarconsulta($consulta);
			}
			function obtener_items_hotel_x_destino($destino, $cotizaciones)
			{
				$controladoraBD = new controladoraBD;
				$consulta = "SELECT Ih.*, Th.destino, Th.orden, H.* 

								FROM 	pqt_hotel H, 
										pqt_item_hotel Ih, 
										pqt_tarifa_hotel Th, 
										pqt_ciudad_origen_paquete Cop, 
										pqt_cotizacion_has_ciudad_origen Chco 

								WHERE 	H.idhotel = Ih.idhotel AND 
										Ih.idtarifa_hotel = Th.idtarifa_hotel AND 
										Th.idciudad_origen_paquete = Cop.idciudad_origen_paquete AND 
										Cop.idciudad_origen_paquete = Chco.idciudad_origen_paquete AND 
										Chco.idcotizacion IN($cotizaciones) AND 
										Th.destino = '$destino' AND  
										Ih.is_real = 1 

								ORDER BY Ih.fecha_in";
				//echo $consulta;
				return $controladoraBD->ejecutarconsulta($consulta);
			}
			function obtener_pasajeros_por_item_hotel($iditem_hotel)
			{
				$controladoraBD = new controladoraBD;
				$consulta = "SELECT P.idcotizacion, C.nombre_cliente, C.apellido_cliente, C.numero_pasaporte_cliente, C.fecha_nacimiento_cliente, Ihhp.tipo_precio, Ihhp.habitacion 

								FROM pqt_pax P, pqt_item_hotel_has_pax Ihhp, clientes C 

								WHERE 	P.idclientes = C.idclientes AND 
										P.idpax = Ihhp.idpax AND 
										Ihhp.iditem_hotel='$iditem_hotel' 
								ORDER BY Ihhp.habitacion, Ihhp.tipo_precio";
				//echo $consulta;
				return $controladoraBD->ejecutarconsulta($consulta);
			}

}
?>