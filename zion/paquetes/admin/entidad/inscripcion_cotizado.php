<?
	class inscripcion_cotizado
	{
		function obtener_inscripcion_cotizados($idcotizacion)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "select pqt_inscripcion_cotizado.* from pqt_inscripcion_cotizado
								where pqt_inscripcion_cotizado.idevento_cotizado = $idcotizacion";
			return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function eliminar_inscripcion($idcotizacion)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "DELETE FROM pqt_inscripcion_cotizado 
								WHERE idinscripcion_cotizado = $idcotizacion";
			return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function registrar_inscripcion($id, $cat, $pre, $mon, $fee, $fac, $obs, $tim, $est, $ide, $t_c)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "INSERT INTO pqt_inscripcion_cotizado (idinscripcion_cotizado, categoria, precio, moneda, fee, factura, observacion, time_limit, estado, idevento_cotizado, tipo_cambio)
								VALUES('$id','$cat','$pre','$mon','$fee','$fac','$obs','$tim','$est','$ide','$t_c')";
			return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function modificar_inscripcion($id, $cat, $pre, $mon, $fee, $fac, $obs, $tim, $est, $ide, $t_c)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "UPDATE pqt_inscripcion_cotizado 
								SET categoria = '$cat', precio = '$pre', moneda = '$mon', fee = '$fee', factura = '$fac', observacion = '$obs', time_limit = '$tim', estado = '$est', idevento_cotizado = '$ide', tipo_cambio = '$t_c'  
								WHERE idinscripcion_cotizado = '$id'";
			return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function obtener_inscripcion_idpax($idpax)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "SELECT pqt_inscripcion_cotizado.* FROM pqt_inscripcion_cotizado
								INNER JOIN pqt_inscripcion_cotizado_has_pax_evento ON pqt_inscripcion_cotizado.idinscripcion_cotizado = pqt_inscripcion_cotizado_has_pax_evento.idinscripcion_cotizado
								WHERE pqt_inscripcion_cotizado_has_pax_evento.idpax_evento = $idpax";
			return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function registrar_inscripcion_cotizado_has_pax_evento($idinscripcion_cotizado, $idpax, $tipo_precio)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "INSERT INTO pqt_inscripcion_cotizado_has_pax_evento (idinscripcion_cotizado, idpax_evento, tipo_precio)
								VALUES('$idinscripcion_cotizado', '$idpax', '$tipo_precio')";
			return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function eliminar_inscripcion_cotizado_has_pax_evento($idinscripcion_cotizado, $idpax)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "DELETE FROM pqt_inscripcion_cotizado_has_pax_evento
								WHERE idinscripcion_cotizado = $idinscripcion_cotizado AND idpax_evento = $idpax";
			return $controladoraBD->ejecutarconsulta($consulta);
		}
		
		function obtenet_inscripcion_cotizado_idpax($idpax)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "SELECT pqt_inscripcion_cotizado_has_pax_evento.* FROM pqt_inscripcion_cotizado_has_pax_evento
								WHERE pqt_inscripcion_cotizado_has_pax_evento.idpax_evento = $idpax";
			return $controladoraBD->ejecutarconsulta($consulta);
		}

		function obtener_inscripciones_por_pax($idpax)
		{
			$controladoraBD = new controladoraBD;
			$consulta 		= "SELECT Ip.*, I.* FROM pqt_inscripcion_cotizado_has_pax_evento Ip, pqt_inscripcion_cotizado I 
								WHERE Ip.idinscripcion_cotizado=I.idinscripcion_cotizado AND Ip.idpax_evento = $idpax";
			return $controladoraBD->ejecutarconsulta($consulta);	
		}
	}
?>