<?php
/**
* 
*/
class cotizacion
{
	
	function registrar_nueva_cotizacion($idclientes, $idusuarios, $titulo, $observacion)
	{
		$controladoraBD = new controladoraBD;
        $consulta 		= "INSERT INTO pqt_cotizacion(idclientes, idusuarios, titulo, fecha_creacion, observacion) 
        					VALUES('$idclientes', '$idusuarios', '$titulo', '" . date('Y-m-d H:i:s') . "', '$observacion')";
        return $controladoraBD->ejecutarconsulta($consulta);
	}

	function registrar_relacion_publicacion_cotizacion($idpublicacion, $idcotizacion)
	{
		$controladoraBD = new controladoraBD;
        $consulta 		= "INSERT INTO pqt_cotizacion_has_publicacion(idpublicacion, idcotizacion) 
        					VALUES('$idpublicacion', '$idcotizacion')";
        return $controladoraBD->ejecutarconsulta($consulta);	
	}

	function registrar_solicitante_de_cotizacion($idcotizacion, $idclientes)
	{
		$controladoraBD = new controladoraBD;
        $consulta 		= "INSERT INTO pqt_cotizacion_has_solicitante(idcotizacion, idclientes) 
        					VALUES('$idcotizacion', '$idclientes')";
        return $controladoraBD->ejecutarconsulta($consulta);		
	}

	function obtener_info_cotizacion($idcotizacion)
	{
		$controladoraBD = new controladoraBD;
        $consulta 		= "SELECT Co.*, Co.estado AS estado_cotizacion, Cl.nombre_cliente, Cl.apellido_cliente, U.* FROM pqt_cotizacion Co, clientes Cl, user U 
        					WHERE Co.idclientes=Cl.idclientes AND Co.idusuarios=U.id AND Co.idcotizacion=$idcotizacion";
        					
        return $controladoraBD->ejecutarconsulta($consulta);			
	}

	function obtener_solicitantes_x_cotizacion($idcotizacion)
	{
		$controladoraBD = new controladoraBD;
        $consulta 		= "SELECT Cl.idclientes, Cl.nombre_cliente, Cl.apellido_cliente 
        					FROM pqt_cotizacion_has_solicitante Cs, clientes Cl 
        					WHERE  Cs.idclientes=Cl.idclientes AND Cs.idcotizacion=$idcotizacion";
        
        return $controladoraBD->ejecutarconsulta($consulta);			
	}

	/*
	* OBTENER CIUDADES DE EL PAQUETE
	*/
	function obtener_ciudades_origen_cotizacion($idcotizacion)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT * FROM pqt_ciudad_origen_paquete Co, pqt_cotizacion_has_ciudad_origen Cc 
							WHERE Co.idciudad_origen_paquete=Cc.idciudad_origen_paquete AND Cc.idcotizacion=$idcotizacion";

		return $controladoraBD->ejecutarconsulta($consulta);	
	}
	function obtener_paquete_de_cotizacion($idcotizacion)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT * FROM pqt_publicacion P, pqt_cotizacion_has_publicacion C 
							WHERE C.idpublicacion=P.idpublicacion AND C.idcotizacion=$idcotizacion";

		return $controladoraBD->ejecutarconsulta($consulta);		
	}

	/*
	* BUSQUEDA DE COTIZACIONES
	*/
	function obtener_titulos_cotizacion()
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT * FROM pqt_cotizacion ORDER BY titulo";

		return $controladoraBD->ejecutarconsulta($consulta);
	}
	function obtener_cotizacion_por_palabra_clave_titulo($palabra, $nombre, $apellido, $estado, $fecha_desde, $fecha_hasta)
	{
		$controladoraBD = new controladoraBD;
        $consulta = "SELECT Cot.*, Cli.nombre_cliente, Cli.apellido_cliente, U.nombre_usuario    
                        FROM pqt_cotizacion Cot, clientes Cli, 
                            usuarios U 
                        WHERE Cot.idclientes=Cli.idclientes AND Cot.idusuarios=U.idusuarios AND Cot.titulo LIKE('%$palabra%')";
        if($nombre != '')
        	$consulta .= " AND Cli.nombre_cliente LIKE('%$nombre%')";
        if($apellido != '')
        	$consulta .= " AND Cli.apellido_cliente LIKE('%$apellido%')";	
        if($estado != '' && $estado != '3')
        	$consulta .= " AND Cot.estado='$estado'";	
        if($fecha_desde != '' && $fecha_hasta != '')
        	$consulta .= " AND Cot.fecha_creacion >= '$fecha_desde' AND Cot.fecha_creacion <= '$fecha_hasta'";
        $consulta .= " GROUP BY Cot.idcotizacion";
 
echo $consulta;

        return $controladoraBD->ejecutarconsulta($consulta);
	}
	function obtener_cotizacion_por_palabra_clave_titulo_cliente_pax($palabra, $nombre, $apellido, $estado, $fecha_desde, $fecha_hasta, $nombre_pax, $apellido_pax)
	{
		$controladoraBD = new controladoraBD;
        $consulta = "SELECT Cot.*, Cli.nombre_cliente, Cli.apellido_cliente, U.nombre_usuario    
                        FROM pqt_cotizacion Cot, clientes Cli, 
                            usuarios U ";

        // SI SE INGRESO NOMBRE Y/O APELLIDO DEL PASAJERO CONSULTAR ESTAS TABLAS
        if($nombre_pax != '' || $apellido_pax != '')
        	$consulta .= ", pqt_pax Pax, clientes Cpax ";

        // FILTRO PARA LA CONSULTA RESPECTO DEL TITULO DE LA COTIZACION
        $consulta 	.=  " WHERE Cot.idclientes=Cli.idclientes AND Cot.idusuarios=U.idusuarios AND Cot.titulo LIKE('%$palabra%')";
        
        // FILTRO RESPECTO DEL NOMBRE Y/O APELLIDO DEL CLIENTE
        if($nombre != '')
        	$consulta .= " AND Cli.nombre_cliente LIKE('%$nombre%')";
        if($apellido != '')
        	$consulta .= " AND Cli.apellido_cliente LIKE('%$apellido%')";

        // FILTRA POR ESTADO	
        if($estado != '' && $estado != '3')
        	$consulta .= " AND Cot.estado='$estado'";

        // FILTRO ENTRE FECHAS SEGUN FECHA DE CREACION DE LA COTIZACION	
        if($fecha_desde != '' && $fecha_hasta != '')
        	$consulta .= " AND Cot.fecha_creacion >= '$fecha_desde' AND Cot.fecha_creacion <= '$fecha_hasta'";

        // FILTRO RESPECTO DEL NOMBRE Y APELLIDO DEL PASAJERO
        if($nombre_pax != '' || $apellido_pax != '')
        	$consulta .= " AND Pax.idcotizacion = Cot.idcotizacion AND Pax.idclientes = Cpax.idclientes AND Cpax.nombre_cliente LIKE('%$nombre_pax%') AND Cpax.apellido_cliente LIKE('%$apellido_pax%')";

        // AGRUPAR POR COTIZACION
        $consulta .= " GROUP BY Cot.idcotizacion";
 
        return $controladoraBD->ejecutarconsulta($consulta);
	}
	function obtener_cantidad_paxs($idcotizacion)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT COUNT(idpax) FROM pqt_pax WHERE idcotizacion=$idcotizacion";

		return $controladoraBD->ejecutarconsulta($consulta);	
	}
	
	/*
	* CIERRE O REAPERTURA DE UNA COTIZACION
	*/
	function cierre_de_cotizacion($idcotizacion, $estado)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "UPDATE pqt_cotizacion 
							SET estado=$estado, fecha_cierre='" . date('Y-m-d H:i:s') . "' 
							WHERE idcotizacion=$idcotizacion";

		return $controladoraBD->ejecutarconsulta($consulta);
	}


}
?>