<?php

class paquete
{   
	function registrar_paquete($idpublicacion, $destino, $precio_desde, $salida, $retorno, $cant_noches)
	{
		$controladoraBD= new controladoraBD;
        $consulta = "INSERT INTO paquete (idpublicacion ,destino, precio_desde, salida, retorno, cant_noches) VALUES ($idpublicacion, '$destino', '$precio_desde', '$salida', '$retorno', '$cant_noches')";
        //echo $consulta;
        return $controladoraBD->ejecutarconsulta($consulta);	
	}
	function registrar_operadora_en_paquete($idoperadora, $idpaquete)
	{
		$controladoraBD= new controladoraBD;
        $consulta = "UPDATE paquete SET idoperadora=$idoperadora WHERE idpublicacion=$idpaquete";
        return $controladoraBD->ejecutarconsulta($consulta);		
	}
	function registrar_temporada_en_paquete($idtemporada, $idpaquete)
	{
			$controladoraBD	= new controladoraBD;
        	$consulta 		= "INSERT INTO paquete_has_temporada(idpaquete, idtemporada) VALUES ($idpaquete, $idtemporada)";
        	return $controladoraBD->ejecutarconsulta($consulta);		
	}
	function registrar_tipo_paquete_en_paquete($idtipo_paquete, $idpaquete)
	{
			$controladoraBD		= new controladoraBD;
        	$consulta 			= "INSERT INTO paquete_has_tipo_paquete(idpaquete, idtipo_paquete) VALUES ($idpaquete, $idtipo_paquete)";
        	return $controladoraBD->ejecutarconsulta($consulta);		
	}
	function registrar_contenido_paquete($idpaquete, $paquete_incluye, $importante, $datos_agente, $solo_imagen)
	{
		$controladoraBD		= new controladoraBD;
    	$consulta 			= "INSERT INTO contenido_paquete(idpaquete, paquete_incluye, importante, datos_agente, solo_imagen) VALUES ('$idpaquete', '$paquete_incluye', '$importante', '$datos_agente', '$solo_imagen')";
    	return $controladoraBD->ejecutarconsulta($consulta);		
	}
	function obtener_paquete($idpublicacion)
	{
		$controladoraBD= new controladoraBD;
        $consulta = "SELECT * FROM publicacion Pb, paquete Pq, contenido_paquete Cp, usuarios U WHERE Pb.idpublicacion=Pq.idpublicacion AND Pq.idpublicacion=Cp.idpaquete AND Pb.idusuarios=U.idusuarios AND Pb.idpublicacion=$idpublicacion";
        //echo($consulta);
        return $controladoraBD->ejecutarconsulta($consulta);
	}
	function obtener_tipos_de_paquete_por_paquete($idpaquete)
	{
		$controladoraBD= new controladoraBD;
        $consulta = "SELECT Tp.idtipo_paquete, Tp.nombre_tipo FROM paquete Pq, paquete_has_tipo_paquete Pt, tipo_paquete Tp WHERE Pq.idpublicacion=Pt.idpaquete AND Pt.idtipo_paquete=Tp.idtipo_paquete AND Pq.idpublicacion=$idpaquete";
        //echo $consulta;
        return $controladoraBD->ejecutarconsulta($consulta);
	}
	function obtener_destinos_por_paquete($idpaquete)
	{
		$controladoraBD= new controladoraBD;
        $consulta = "SELECT D.iddestino, D.nombre_destino FROM paquete Pq, paquete_has_destino Pd, destino D WHERE Pq.idpublicacion=Pd.idpaquete AND Pd.iddestino=D.iddestino AND Pq.idpublicacion=$idpaquete";
        //echo $consulta;
        return $controladoraBD->ejecutarconsulta($consulta);
	}
	function obtener_temporadas_por_paquete($idpaquete)
	{
		$controladoraBD= new controladoraBD;
        $consulta = "SELECT Tp.idtemporada, Tp.nombre FROM paquete Pq, paquete_has_temporada Pt, temporada Tp WHERE Pq.idpublicacion=Pt.idpaquete AND Pt.idtemporada=Tp.idtemporada AND Pq.idpublicacion=$idpaquete";
        //echo $consulta;
        return $controladoraBD->ejecutarconsulta($consulta);
	}
	function obtener_paquete_por_codigo($idpublicacion, $estado)
	{
		$controladoraBD= new controladoraBD;
        $consulta = "SELECT Pb.idpublicacion, Pb.titulo, Pq.destino, Pb.fecha_publicacion, Pb.estado, U.nombre_usuario FROM publicacion Pb, usuarios U, paquete Pq WHERE Pb.idusuarios=U.idusuarios AND Pb.idpublicacion=Pq.idpublicacion  AND Pb.idpublicacion=$idpublicacion AND Pb.estado='$estado' AND Pb.tipo_publicacion='P'";
        //echo $consulta;
        return $controladoraBD->ejecutarconsulta($consulta);
	}
	function obtener_paquete_por_criterios($tipo, $temporada, $destino, $precio, $estado)
	{
		$controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.idpublicacion, Pb.titulo, Pq.destino, Pq.cant_noches, Pq.salida, Pq.precio_desde, Pb.fecha_publicacion, Pb.estado, U.nombre_usuario, Pb.imagen_chica FROM publicacion Pb, paquete Pq, usuarios U ";
        if ($tipo != 0) 
        {
        	$consulta .= ",paquete_has_tipo_paquete Ptp ";
        }
        if($temporada != "")
        {
        	$consulta .= ",paquete_has_temporada Ptm, temporada T "	;
        }
        if($destino != "")
        {
			$consulta .= ", paquete_has_destino Phd, destino D ";     	
        }
        $consulta .= " WHERE Pb.idusuarios=U.idusuarios AND Pb.idpublicacion=Pq.idpublicacion AND Pb.estado='$estado' AND Pb.tipo_publicacion='P' ";
        if($precio != "")
		{
			$consulta .= "AND Pq.precio_desde<=$precio ";
		}
        if ($tipo != 0) 
        {
        	$consulta .= "AND ( Ptp.idpaquete=Pq.idpublicacion AND Ptp.idtipo_paquete=$tipo ) ";
        }
        if($temporada != "")
        {
        	$consulta .= "AND ( Ptm.idpaquete=Pq.idpublicacion AND Ptm.idtemporada=T.idtemporada AND T.nombre LIKE('%$temporada%') ) ";
        }
        if($destino != "")
        {
        	$consulta .= "AND ( Phd.idpaquete=Pq.idpublicacion AND Phd.iddestino=D.iddestino AND D.nombre_destino LIKE('%$destino%') ) ";
        }
        //echo $consulta;
        return $controladoraBD->ejecutarconsulta($consulta);
	}
	function obtener_operadora_por_paquete($idpaquete)
	{
		$controladoraBD= new controladoraBD;
        $consulta = "SELECT Op.nombre_operadora, Op.idoperadora FROM paquete Pq, Operadora Op WHERE Pq.idoperadora=Op.idoperadora AND Pq.idpublicacion=$idpaquete";
        //echo $consulta;
        return $controladoraBD->ejecutarconsulta($consulta);	
	}
    function obtener_contenido_paquete($idpaquete)
    {
        $controladoraBD= new controladoraBD;
        $consulta = "SELECT * FROM contenido_paquete Ce WHERE Ce.idpaquete=$idpaquete";
        //echo $consulta;
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_paquetes()
    {
        $controladoraBD= new controladoraBD;
        $consulta = "SELECT * FROM publicacion P WHERE P.tipo_publicacion='P' AND P.estado='V' ORDER BY P.titulo";
        //echo $consulta;
        return $controladoraBD->ejecutarconsulta($consulta);   
    }
    function obtener_paquete_por_palabra_clave($palabra, $estado)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.idpublicacion, Pb.titulo, Pq.cant_noches, Pq.salida, Pq.retorno, Pq.precio_desde, Pb.fecha_publicacion, Pb.estado, U.nombre_usuario 
                        FROM publicacion Pb, paquete Pq, 
                            paquete_has_tipo_paquete Ptp, tipo_paquete Tp, 
                            paquete_has_temporada Pht, temporada T, 
                            paquete_has_destino Phd, destino D, 
                            usuarios U 
                        WHERE Pb.idpublicacion=Pq.idpublicacion AND Pb.estado='$estado' AND Pb.tipo_publicacion='P' AND Pb.idusuarios=U.idusuarios AND (
                                ( Ptp.idpaquete=Pq.idpublicacion AND Ptp.idtipo_paquete=Tp.idtipo_paquete AND Tp.nombre_tipo LIKE('%$palabra%') )
                                OR ( Pht.idpaquete=Pq.idpublicacion AND Pht.idtemporada=T.idtemporada AND T.nombre LIKE('%$palabra%') )
                                OR ( Phd.idpaquete=Pq.idpublicacion AND Phd.iddestino=D.iddestino AND D.nombre_destino LIKE('%$palabra%') )
                                OR  Pb.titulo LIKE('%$palabra%') 
                            ) GROUP BY Pb.idpublicacion";
        
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_paquete_por_palabra_clave_titulo($palabra, $estado)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.idpublicacion, Pb.titulo, Pb.vista_previa,Pb.portada, Pq.salida, Pq.retorno, Pb.fecha_publicacion, Pb.estado, U.nombre_usuario  
                        FROM publicacion Pb, paquete Pq, 
                            usuarios U 
                        WHERE Pb.idpublicacion=Pq.idpublicacion AND Pb.estado='$estado' AND Pb.tipo_publicacion='P' AND Pb.idusuarios=U.idusuarios AND (
                                Pb.titulo LIKE('%$palabra%') 
                            ) GROUP BY Pb.idpublicacion";
        //echo $consulta . "<br/>";
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_paquete_por_palabra_clave_tipo_paquete($palabra, $estado)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.idpublicacion, Pb.titulo, Pb.vista_previa,Pb.portada, Pq.salida, Pq.retorno, Pb.fecha_publicacion, Pb.estado, U.nombre_usuario  
                        FROM publicacion Pb, paquete Pq, 
                             paquete_has_tipo_paquete Ptp, tipo_paquete Tp, 
                            usuarios U 
                        WHERE Pb.idpublicacion=Pq.idpublicacion AND Pb.estado='$estado' AND Pb.tipo_publicacion='P' AND Pb.idusuarios=U.idusuarios AND Ptp.idpaquete=Pq.idpublicacion AND Ptp.idtipo_paquete=Tp.idtipo_paquete AND Tp.nombre_tipo LIKE('%$palabra%') GROUP BY Pb.idpublicacion";
        //echo $consulta . "<br/>";
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_paquete_por_palabra_clave_temporada($palabra, $estado)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.idpublicacion, Pb.titulo, Pb.vista_previa,Pb.portada, Pq.salida, Pq.retorno, Pb.fecha_publicacion, Pb.estado, U.nombre_usuario  
                        FROM publicacion Pb, paquete Pq, 
                            paquete_has_temporada Pht, temporada T, 
                            usuarios U 
                        WHERE Pb.idpublicacion=Pq.idpublicacion AND Pb.estado='$estado' AND Pb.tipo_publicacion='P' AND Pb.idusuarios=U.idusuarios AND Pht.idpaquete=Pq.idpublicacion AND Pht.idtemporada=T.idtemporada AND T.nombre LIKE('%$palabra%') GROUP BY Pb.idpublicacion";
        //echo $consulta . "<br/>";
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_paquete_por_palabra_clave_destino($palabra, $estado)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.idpublicacion, Pb.titulo, Pb.vista_previa,Pb.portada, Pq.salida, Pq.retorno, Pb.fecha_publicacion, Pb.estado, U.nombre_usuario  
                        FROM publicacion Pb, paquete Pq, 
                            paquete_has_destino Phd, destino D, 
                            usuarios U 
                        WHERE Pb.idpublicacion=Pq.idpublicacion AND Pb.estado='$estado' AND Pb.tipo_publicacion='P' AND Pb.idusuarios=U.idusuarios AND Phd.idpaquete=Pq.idpublicacion AND Phd.iddestino=D.iddestino AND D.nombre_destino LIKE('%$palabra%') GROUP BY Pb.idpublicacion";
        //echo $consulta . "<br/>";
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_cantidad_cotizaciones($idpaquete)
    {
        $controladoraBD= new controladoraBD;
        $consulta = "SELECT COUNT(idpaquete) AS cantidad FROM paquete_cotizado P WHERE P.idpaquete='$idpaquete' GROUP BY idpaquete";
        
        return $controladoraBD->ejecutarconsulta($consulta);      
    }

    /*
    * BUSQUEDA DE PAQUETES SEGUN CRITERIOS PARA VISTA PREVIA
    */
    function obtener_paquete_por_palabra_clave_titulo_vista_previa($palabra, $estado)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.idpublicacion, Pb.titulo, Pb.imagen_chica, Pb.imagen_grande, Pq.precio_desde, Pq.cant_noches, Pq.salida, Pq.retorno  
                        FROM publicacion Pb, paquete Pq 
                        WHERE Pb.idpublicacion=Pq.idpublicacion AND Pb.estado='$estado' AND Pb.tipo_publicacion='P' AND (
                                Pb.titulo LIKE('%$palabra%') 
                            ) GROUP BY Pb.idpublicacion";
        //echo $consulta . "<br/>";
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_paquete_por_palabra_clave_tipo_paquete_vista_previa($palabra, $estado)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.idpublicacion, Pb.titulo, Pb.imagen_chica, Pb.imagen_grande, Pq.precio_desde, Pq.cant_noches, Pq.salida, Pq.retorno  
                        FROM publicacion Pb, paquete Pq, 
                             paquete_has_tipo_paquete Ptp, tipo_paquete Tp  
                        WHERE Pb.idpublicacion=Pq.idpublicacion AND Pb.estado='$estado' AND Pb.tipo_publicacion='P' AND Ptp.idpaquete=Pq.idpublicacion AND Ptp.idtipo_paquete=Tp.idtipo_paquete AND Tp.nombre_tipo LIKE('%$palabra%') GROUP BY Pb.idpublicacion";
        //echo $consulta . "<br/>";
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_paquete_por_palabra_clave_temporada_vista_previa($palabra, $estado)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.idpublicacion, Pb.titulo, Pb.imagen_chica, Pb.imagen_grande, Pq.precio_desde, Pq.cant_noches, Pq.salida, Pq.retorno  
                        FROM publicacion Pb, paquete Pq, 
                            paquete_has_temporada Pht, temporada T 
                        WHERE Pb.idpublicacion=Pq.idpublicacion AND Pb.estado='$estado' AND Pb.tipo_publicacion='P' AND Pht.idpaquete=Pq.idpublicacion AND Pht.idtemporada=T.idtemporada AND T.nombre LIKE('%$palabra%') GROUP BY Pb.idpublicacion";
        //echo $consulta . "<br/>";
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_paquete_por_palabra_clave_destino_vista_previa($palabra, $estado)
    {
        $controladoraBD = new controladoraBD;
        $consulta = "SELECT Pb.idpublicacion, Pb.titulo, Pb.imagen_chica, Pb.imagen_grande, Pq.precio_desde, Pq.cant_noches, Pq.salida, Pq.retorno  
                        FROM publicacion Pb, paquete Pq, 
                            paquete_has_destino Phd, destino D 
                        WHERE Pb.idpublicacion=Pq.idpublicacion AND Pb.estado='$estado' AND Pb.tipo_publicacion='P' AND Phd.idpaquete=Pq.idpublicacion AND Phd.iddestino=D.iddestino AND D.nombre_destino LIKE('%$palabra%') GROUP BY Pb.idpublicacion";
        //echo $consulta . "<br/>";
        return $controladoraBD->ejecutarconsulta($consulta);
    }
    function obtener_cantidad_de_envios_de_paquete($idpaquete)
    {
        $controladoraBD = new controladoraBD;
        $consulta       = "SELECT COUNT(codigo) AS cantidad FROM pqt_envio_masivo WHERE tipo_envio='P' AND codigo='$idpaquete' GROUP BY codigo";
        
        return $controladoraBD->ejecutarconsulta($consulta);    
    }
}

?>