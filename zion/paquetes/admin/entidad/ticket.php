<?php
/**
* 
*/
class ticket
{
	
	function obtener_tickets_por_codigo_reserva($codigo_pnr)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= 	"SELECT T.numero_ticket, T.precio_total_ticket, T.moneda_ticket, Px.idpasajeros, Px.nombre_pasajero, Px.apellido_pasajero, Pn.idpnr, L.codigo_iata_linea_aerea 
								FROM ticket T, pasajeros Px, pnr Pn, lineas_aereas L 
								WHERE Pn.cod_reserva_aerea='$codigo_pnr' AND 
								T.lineas_aereas_idlineas_aereas=L.idlineas_aereas AND 
								T.pnr_has_pasajeros_pasajeros_idpasajeros = Px.idpasajeros AND 
								T.pnr_has_pasajeros_pnr_idpnr = Pn.idpnr AND 
								T.anulado='F' AND 
								NOT EXISTS (SELECT * FROM pqt_boleto_cotizado_has_pax Pb WHERE Pb.numero_ticket=T.numero_ticket)";
		
		return $controladoraBD->ejecutarconsulta($consulta);	
	}
}
?>