<?php
/**
* 
*/
class requisito_viaje
{
	
	function obtener_requisitos_viaje_x_pais_nacionalidad($idpais, $idnacionalidad)
	{
		$controladoraBD = new controladoraBD;
		$consulta = "SELECT Rv.*, Vac.desc_vacuna, Vis.nombre_visa, P.nombre_pais 
						FROM pqt_requisito_viaje Rv, vacuna Vac, visa Vis, pais P 
						WHERE Rv.idpais=$idpais AND Rv.idnacionalidad=$idnacionalidad AND 
						 		Rv.idvacuna=Vac.idvacuna AND Rv.idvisa=Vis.idvisa AND P.idpais=Rv.idpais";

		return $controladoraBD->ejecutarconsulta($consulta);
	}
}
?>