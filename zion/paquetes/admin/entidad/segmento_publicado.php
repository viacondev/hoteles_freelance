<?php
/**
* 
*/
class segmento_publicado
{
	
	function obtener_segmentos($id, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT * FROM segmento_" . $type . " T WHERE T.id" . $type . "=$id ORDER BY T.orden, T.idsegmento_" . $type . " ASC" ;

		return $controladoraBD->ejecutarconsulta($consulta);
	}
	function registrar_segmento_publicado($id, $idopcion_boleto, $codigo, $orden, $idlineas_aereas, $nro_vuelo, $fecha, $origen, $hora_sale, $destino, $hora_llega, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "INSERT INTO segmento_" . $type . "(id" . $type . ", idopcion_boleto_" . $type . ", codigo, orden, idlineas_aereas, nro_vuelo, fecha, origen, hora_sale, destino, hora_llega) 
							VALUES('$id', '$idopcion_boleto', '$codigo', '$orden', '$idlineas_aereas', '$nro_vuelo', '$fecha', '$origen', '$hora_sale', '$destino', '$hora_llega')";

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function modificar_segmento_publicado($id, $codigo, $orden, $idlineas_aereas, $nro_vuelo, $fecha, $origen, $hora_sale, $destino, $hora_llega, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "UPDATE segmento_" . $type . " 
							SET codigo='$codigo', orden='$orden', idlineas_aereas='$idlineas_aereas', nro_vuelo='$nro_vuelo', fecha='$fecha', origen='$origen', hora_sale='$hora_sale', destino='$destino', hora_llega='$hora_llega' 
							WHERE idsegmento_" . $type . "='$id' ";

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function eliminar_segmento_publicado($id, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "DELETE FROM segmento_" . $type . " 
							WHERE idsegmento_" . $type . "='$id'";

		return $controladoraBD->ejecutarconsulta($consulta);	
	}
	function obtener_segmentos_de_opcion($id, $type)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT * FROM segmento_" . $type . " T WHERE T.idopcion_boleto_" . $type . "=$id ORDER BY T.orden, T.idsegmento_" . $type . " ASC" ;

		return $controladoraBD->ejecutarconsulta($consulta);
	}
	/*
	* NUEVO FORMATO
	*/
	function eliminar_segmento_x_id($idsegmento)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "DELETE FROM pqt_segmento 
							WHERE idsegmento=$idsegmento";

		return $controladoraBD->ejecutarconsulta($consulta);	
	}
	function registrar_nuevo_segmento_paquete($idopcion_boleto, $idlineas_aereas, $codigo, $orden, $nro_vuelo, $fecha, $origen, $hora_sale, $destino, $hora_llega)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "INSERT INTO pqt_segmento(idopcion_boleto, idlineas_aereas, codigo, orden, nro_vuelo, fecha, origen, hora_sale, destino, hora_llega) 
							VALUES('$idopcion_boleto', '$idlineas_aereas', '$codigo', '$orden', '$nro_vuelo', '$fecha', '$origen', '$hora_sale', '$destino', '$hora_llega')";
//echo $consulta;
		return $controladoraBD->ejecutarconsulta($consulta);
	}
	function modificar_segmento_paquete_x_id($idsegmento, $idlineas_aereas, $codigo, $orden, $nro_vuelo, $fecha, $origen, $hora_sale, $destino, $hora_llega)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "UPDATE pqt_segmento SET idlineas_aereas='$idlineas_aereas', codigo='$codigo', orden='$orden', nro_vuelo='$nro_vuelo', fecha='$fecha', origen='$origen', hora_sale='$hora_sale', destino='$destino', hora_llega='$hora_llega' 
							WHERE idsegmento=$idsegmento";
//echo $consulta;
		return $controladoraBD->ejecutarconsulta($consulta);
	}
}
?>