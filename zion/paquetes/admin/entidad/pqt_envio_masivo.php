<?php
/**
* 
*/
class pqt_envio_masivo
{
	
	function insertar_envio_masivo($idusuarios, $tipo_envio, $contenido, $observacion, $cantidad_envios_correctos, $cantidad_envios_incorrectos, $cantidad_clientes_con_mail, $cantidad_clientes_sin_mail, $codigo)
	{
		$controladoraBD = new controladoraBD;
        $consulta 		= "INSERT INTO pqt_envio_masivo(fecha_envio, idusuarios, tipo_envio, contenido, observacion, cantidad_envios_correctos, cantidad_envios_incorrectos, cantidad_clientes_con_mail, cantidad_clientes_sin_mail, codigo) 
        					VALUES ('" . date('Y-m-d H:i:s') . "' ,'$idusuarios', '$tipo_envio', '$contenido', '$observacion', '$cantidad_envios_correctos', '$cantidad_envios_incorrectos', '$cantidad_clientes_con_mail', '$cantidad_clientes_sin_mail', '$codigo')";
        
        return $controladoraBD->ejecutarconsulta($consulta);
	}
	function obtener_envios($codigo, $type)
	{
		$controladoraBD = new controladoraBD;
        $consulta 		= "SELECT P.*, U.nombre_usuario FROM pqt_envio_masivo P, usuarios U WHERE P.tipo_envio='$type' AND P.codigo='$codigo' AND P.idusuarios=U.idusuarios";
        
        return $controladoraBD->ejecutarconsulta($consulta);
	}

	/*
	* NUEVO FORMATO
	*/
		function insertar_nuevo_envio_masivo($idusuarios, $tipo_envio, $contenido, $observacion, $cantidad_envios_correctos, $cantidad_envios_incorrectos, $cantidad_clientes_con_mail, $cantidad_clientes_sin_mail)
		{
			$controladoraBD = new controladoraBD;
	        $consulta 		= "INSERT INTO pqt_envio_masivo_paquetes(fecha_envio, idusuarios, tipo_envio, contenido, observacion, cantidad_envios_correctos, cantidad_envios_incorrectos, cantidad_clientes_con_mail, cantidad_clientes_sin_mail) 
	        					VALUES ('" . date('Y-m-d H:i:s') . "' ,'$idusuarios', '$tipo_envio', '$contenido', '$observacion', '$cantidad_envios_correctos', '$cantidad_envios_incorrectos', '$cantidad_clientes_con_mail', '$cantidad_clientes_sin_mail')";
	        
	        return $controladoraBD->ejecutarconsulta($consulta);
		}
		function insertar_envio_has_publicacion($idenvio_masivo, $idpublicacion)
		{
			$controladoraBD = new controladoraBD;
	        $consulta 		= "INSERT INTO pqt_envio_masivo_has_publicacion(idenvio_masivo, idpublicacion) 
	        					VALUES ($idenvio_masivo, $idpublicacion)";
	        
	        return $controladoraBD->ejecutarconsulta($consulta);
		}
		function obtener_envios_por_publicacion($idpublicacion)
		{
			$controladoraBD = new controladoraBD;
	        $consulta 		= "SELECT E.*, U.nombre_usuario FROM pqt_envio_masivo_paquetes E, usuarios U, pqt_envio_masivo_has_publicacion P WHERE E.idenvio_masivo=P.idenvio_masivo AND P.idpublicacion='$idpublicacion' AND E.idusuarios=U.idusuarios";
	        
	        return $controladoraBD->ejecutarconsulta($consulta);
		}

		
}
?>