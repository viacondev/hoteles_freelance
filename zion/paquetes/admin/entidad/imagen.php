<?php

class imagen
{   
	function registrar_imagen($directorio, $idpublicacion)
	{
		$controladoraBD= new controladoraBD;
        $consulta = "INSERT INTO imagen(directorio, idpublicacion) VALUES ('$directorio', $idpublicacion) ";
        //echo $consulta;
        return $controladoraBD->ejecutarconsulta($consulta);			
	}
	function eliminar_imagen($idimagen)
	{
		$controladoraBD= new controladoraBD;
        $consulta = "DELETE FROM imagen WHERE idimagen=$idimagen";
        return $controladoraBD->ejecutarconsulta($consulta);
	}
	function obtener_imagenes_de_publicacion($idpublicacion)
	{
		$controladoraBD= new controladoraBD;
        $consulta = "SELECT * FROM imagen WHERE idpublicacion=$idpublicacion";
        return $controladoraBD->ejecutarconsulta($consulta);	
	}

	/*
	* IMAGENES NUEVO FORMATO
	*/
	function obtener_ultima_imagen_id()
	{
		$controladoraBD = new controladoraBD;
        $consulta 		= "SELECT MAX(idimagen) AS next FROM pqt_imagen";
        
        $resultado 		= $controladoraBD->ejecutarconsulta($consulta);
        return $resultado[0]['next'];
	}
	function registrar_nueva_imagen($nombre)
	{
		$controladoraBD = new controladoraBD;
        $consulta 		= "INSERT INTO pqt_imagen(nombre) VALUES ('$nombre') ";
        
        return $controladoraBD->ejecutarconsulta($consulta);				
	}
	function registrar_publicacion_has_imagen($idpublicacion ,$idimagen)
	{
		$controladoraBD = new controladoraBD;
        $consulta 		= "INSERT INTO pqt_publicacion_has_imagen(idpublicacion, idimagen) 
        					VALUES ('$idpublicacion', '$idimagen')";
        
        return $controladoraBD->ejecutarconsulta($consulta);
	}
	
}

?>