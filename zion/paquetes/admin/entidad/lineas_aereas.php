<?php
/**
* 
*/
class lineas_aereas
{
	
	function obtener_lineas_aereas()
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT * FROM lineas_aereas" ;

		return $controladoraBD->ejecutarconsulta($consulta);
	}

	function obtener_linea_aerea_por_id($idlineas_aereas)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT * FROM lineas_aereas WHERE idlineas_aereas='$idlineas_aereas'" ;

		return $controladoraBD->ejecutarconsulta($consulta);	
	}

	function obtener_linea_aerea_por_codigo_iata($codigo)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT * FROM lineas_aereas WHERE codigo_iata_linea_aerea='$codigo'" ;

		return $controladoraBD->ejecutarconsulta($consulta);	
	}

	function buscar_lineas_aereas($nombre)
	{
		$controladoraBD = new controladoraBD;
		$consulta 		= "SELECT * FROM lineas_aereas WHERE nombre_linea_aerea LIKE('%$nombre%') OR codigo_iata_linea_aerea LIKE('%$nombre%')" ;

		return $controladoraBD->ejecutarconsulta($consulta);		
	}
}
?>