
<form action="" method="post" enctype="multipart/form-data">
    <label for="file">Sube un imagenes:</label>
    <input type="file" name="imagenes" id="imagenes" />
    <input type="submit" name="boton" value="Subir" />
</form>
<div class="resultado">
<?php
if(isset($_POST['boton'])){
    // Hacemos una condicion en la que solo permitiremos que se suban imagenes y que sean menores a 2000 KB
    if ((($_FILES["imagenes"]["type"] == "image/gif") ||
    ($_FILES["imagenes"]["type"] == "image/jpeg") ||
    ($_FILES["imagenes"]["type"] == "image/pjpeg")) &&
    ($_FILES["imagenes"]["size"] < 2000000)) {
 
    //Si es que hubo un error en la subida, mostrarlo, de la variable $_FILES podemos extraer el valor de [error], que almacena un valor booleano (1 o 0).
      if ($_FILES["imagenes"]["error"] > 0) {
        echo $_FILES["imagenes"]["error"] . "";
      } else {
        // Si no hubo ningun error, hacemos otra condicion para asegurarnos que el imagenes no sea repetido
        if (file_exists("imagenes/" . $_FILES["imagenes"]["name"])) {
          echo $_FILES["imagenes"]["name"] . " ya existe. ";
        } else {
         // Si no es un imagenes repetido y no hubo ningun error, procedemos a subir a la carpeta /imageness, seguido de eso mostramos la imagen subida
          move_uploaded_file($_FILES["imagenes"]["tmp_name"],
          "imagenes/" . $_FILES["imagenes"]["name"]);
          echo "Archivo Subido " . $_FILES["imagenes"]["name"];
          //echo "<img src='imagenes/".$_FILES["imagenes"]["name"]."'>";
        }
      }
    } else {
        // Si el usuario intenta subir algo que no es una imagen o una imagen que pesa mas de 20 KB mostramos este mensaje
        echo "archivo no permitido";
    }
}
?>
</div>
