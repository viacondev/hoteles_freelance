<div style="display:none;">
<?php

if($paquete['solo_imagen'] == 'S')
{
?>
    <div style="border:3px solid #000000; background:#ffffff; width:21cm; height:29.7cm;" class="myPrintArea" >
        <table style="width:21cm; font-family:Courier New;" >
            <tr style="border-bottom:1px solid #aaa;">
                <td>
                    <img src="images\logo.png" style="width:350px; height:70px;" />    
                </td>
                <td style="vertical-align:top; text-align:right;">
                    Calle Velasco Esq. Suarez de Figueroa<br/>
                    Telf 591-3-363610<br/>
                    Santa Cruz - Bolivia<br/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <?php 
                        for ($i=0; $i < count($imagenes); $i++) 
                        { 
                            $imagen = $imagenes[$i];
                    ?>
                    <img src="<?php echo $ruta_imagen . $imagen["directorio"]; ?>" style="width:100%;" />
                    <?php
                        }
                    ?>
                </td>
            </tr>
        </table>
    </div>
<?php
}
else
{
?>
    <div style="background:#ffffff; width:21cm; border:1px solid #AAA; padding-bottom:15px;" class="myPrintArea" >
        <table style="width:21cm; font-family:Courier New;" class="sin-borde">
            <tr style="border-bottom:1px solid #aaa;">
                <td style="vertical-align:middle;">
                    <div  class="one columns" style="padding:15px">
                        <img src="../../../assets/images/logo.png"/>    
                    </div>
                </td>
                <td>
                    <div id="logo" class="five columns" style="padding:15px">
                        <h2>BarryBolivia</h2>    
                    </div>
                </td>
                <td style="vertical-align:top; text-align:right;">
                    Calle Warnes Esq. Rene Moreno<br/>
                    Telf 591-3-368514<br/>
                    Santa Cruz - Bolivia<br/>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="font-size: 25pt; color: #ff3300;font-weight: bold; text-align:center; padding: 10px 0px;">
                    <?php echo strtoupper($paquete["titulo"]); ?><br/>
                    <?php 
                        for ($i=0; $i < count($imagenes); $i++) 
                        { 
                            $imagen = $imagenes[$i];
                    ?>
                            <img src="<?php echo $ruta_imagen . $imagen["nombre"]; ?>" style="width:170px; height:150px; border:3px solid #d0d0d0; margin:10px 10px;" />
                    <?php
                        }
                    ?>
                </td>
            </tr>
        </table>
        <?php
            for ($z=0; $z < count($ciudades); $z++) 
            { 
                $ciudad             = $ciudades[$z];
                $idciudad_origen    = $ciudad['idciudad_origen_paquete'];
        ?>
            <table style="width:21cm; font-family:Courier New;<?php if($z > 0) echo ' display:none;'; ?>" class="tab_ciudad" id="tab_ciudad_print_<?php echo $z; ?>" >
                
                <!-- CIUDAD DE SALIDA -->
                <tr>
                    <td colspan="2" style="padding-top:10px; text-align:center;">
                        <font style="font-size:14pt; font-weight:bold; color:#ff3300;">SALE DESDE <?php echo $c_mostrar_paquete->obtener_aeropuerto($ciudad['ciudad']); ?></font>
                    </td>
                </tr>

                <!-- SALIDA Y RETORNO -->
                <tr>
                    <td colspan="2" style="text-align:center; padding:10px 10px 0px 0px;">
                        <font style="font-size:12pt; font-weight:bold; color:#ff3300;">SALIDA&nbsp;:&nbsp;</font>
                        <font style="font-size:12pt; font-weight:bold; color:#000;">
                            <?php 
                                $fecha_salida = strtotime($ciudad["fecha_salida"]);
                                if($fecha_salida)
                                    $fecha_salida = date('d/m/Y', $fecha_salida);
                                else
                                    $fecha_salida = $ciudad["fecha_salida"];
                                if($fecha_salida != '')
                                    echo $fecha_salida;
                                else
                                    echo 'NO INGRESADO';
                            ?>
                        </font>
                        <font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font>
                        <font style="font-size:12pt; font-weight:bold; color:#ff3300;">RETORNO&nbsp;:&nbsp;</font>
                        <font style="font-size:12pt; font-weight:bold; color:#000;">
                            <?php 
                                $fecha_retorno = strtotime($ciudad["fecha_retorno"]);
                                if($fecha_retorno)
                                    $fecha_retorno = date('d/m/Y', $fecha_retorno);
                                else
                                    $fecha_retorno = $ciudad["fecha_retorno"];
                                if($fecha_retorno != '')
                                    echo $fecha_retorno;
                                else
                                    echo 'NO INGRESADO';
                            ?>
                        </font>
                    </td>
                </tr>

                <!-- PAQUETE INCLUYE -->
                <?php
                    if($ciudad['paquete_incluye'] != '')
                    {
                    ?>
                        <tr>
                            <td colspan="2" style="padding:10px 0px 0px 30px;">
                                <font style="font-size:13pt; font-weight:bold; color:#ff3300;">PAQUETE INCLUYE</font>
                                <ul style="list-style: disc inside; margin:10px 0px 0px 20px; line-height:1;">
                                    <?php
                                        $paquete_incluye = explode("\n", $ciudad['paquete_incluye']);
                                        for($i=0; $i<count($paquete_incluye); $i++)
                                        {
                                            if($paquete_incluye[$i] != "")
                                            {
                                    ?>
                                                <li <?php if($paquete_incluye[$i]{0} == '@') { ?> style="font-weight:bold; list-style:square inside; margin:0;" <?php } else { ?> style="font-size:7pt; margin-left:30px; margin:0;" <?php } ?> >
                                                    <font <?php if($paquete_incluye[$i]{0} != '@') {  ?> style="font-size:9pt; margin:0;" <?php } else { $paquete_incluye[$i] = substr($paquete_incluye[$i], 1) ; } ?> ><?php echo $paquete_incluye[$i]; ?></font>
                                                </li>
                                    <?php
                                            }
                                        }
                                    ?>      
                                </ul>
                            </td>
                        </tr>
                    <?php
                    }
                ?>
                
                <!-- HOTELES -->
                <?php
                    $tarifas_por_destino =  $c_mostrar_paquete->obtener_destinos_de_hotel($idciudad_origen);
                    if(count($tarifas_por_destino) > 0)
                    {
                    ?>
                        <tr class="todo_precio solo_hotel" style="display:none;">
                            <td colspan="2" style="padding:10px 0px 0px 30px;">
                                <font style="font-size:13pt; font-weight:bold; color:#ff3300;">PRECIOS POR PERSONA EN DOLARES AMERICANOS</font>
                                <br/><font style="font-size:11pt; font-weight:bold; color:#ff3300; padding:15px;">HOTELES</font>
                                <?php 
                                    for ($i=0; $i < count($tarifas_por_destino); $i++) 
                                    { 
                                        $tarifa_hotel       = $tarifas_por_destino[$i];
                                        $idth               = $tarifa_hotel['idtarifa_hotel'];
                                        $items_tarifa       = $c_mostrar_paquete->obtener_hoteles_x_destino($idth);

                                        $hide_columns       = $c_mostrar_paquete->evaluar_precios_hotel($items_tarifa);
                                        $cant_col           = count(explode('/', $hide_columns));
                                    ?>
                                        <table style="width:50%; margin-left:3%;">
                                            <tr>
                                                <td style="width:5%;">
                                                    <strong>DESTINO :</strong>
                                                </td>
                                                <td style="width:20%;">
                                                    <?php echo $c_mostrar_paquete->obtener_aeropuerto(strtoupper($tarifa_hotel['destino'])); ?>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                                        <table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios">
                                            <tr style="background-color:#10689b; color:#FFFFFF;">
                                                <th>HOTEL</th>
                                                <?php if(strpos($hide_columns, 'SGL')===false) { ?><th>SINGLE</th><?php } ?>
                                                <?php if(strpos($hide_columns, 'DBL')===false) { ?><th>DOBLE</th><?php } ?>
                                                <?php if(strpos($hide_columns, 'TPL')===false) { ?><th>TRIPLE</th><?php } ?>
                                                <?php if(strpos($hide_columns, 'CPL')===false) { ?><th>CUADRUPLE</th><?php } ?>
                                                <?php if(strpos($hide_columns, 'CNN')===false) { ?><th>MENOR</th><?php } ?>
                                                <?php if(strpos($hide_columns, 'INF')===false) { ?><th>INFANTE</th><?php } ?>
                                                <th>ALIMENTACION</th>
                                                <th>OBS</th>
                                            </tr>
                                    <?php
                                        for ($j=0; $j < count($items_tarifa); $j++) 
                                        { 
                                            $item_tarifa    = $items_tarifa[$j];
                                            $iditem         = $item_tarifa['iditem_hotel'];

                                            $cant_noches    = floor( (strtotime($item_tarifa['fecha_out'] . ' 00:00:00') - strtotime($item_tarifa['fecha_in'] . ' 00:00:00')) / 86400);
                                            
                                            $total_sgl  = $item_tarifa['precio_single'] * $cant_noches;
                                            $total_dbl  = $item_tarifa['precio_doble'] * $cant_noches;
                                            $total_tpl  = $item_tarifa['precio_triple'] * $cant_noches;
                                            $total_cpl  = $item_tarifa['precio_cuadruple'] * $cant_noches;
                                            $total_cnn  = $item_tarifa['precio_menor'] * $cant_noches;
                                            $total_inf  = $item_tarifa['precio_infante'] * $cant_noches;

                                            if($item_tarifa['increment_fee'] == 'P')
                                            {
                                                $fee        = 1 + ($item_tarifa['fee']/100);
                                                $total_sgl  *= $fee;
                                                $total_dbl  *= $fee;
                                                $total_tpl  *= $fee;
                                                $total_cpl  *= $fee;
                                                $total_cnn  *= $fee;
                                                $total_inf  *= $fee;
                                            }
                                            else
                                            {
                                                $fee        = $item_tarifa['fee'];
                                                if($item_tarifa['precio_single'] > 0)
                                                    $total_sgl  += $fee;
                                                else
                                                    $total_sgl  = 0;
                                                if($item_tarifa['precio_doble'] > 0)
                                                    $total_dbl  += $fee;
                                                else
                                                    $total_dbl  = 0;
                                                if($item_tarifa['precio_triple'] > 0)
                                                    $total_tpl  += $fee;
                                                else
                                                    $total_tpl  = 0;
                                                if($item_tarifa['precio_cuadruple'] > 0)
                                                    $total_cpl  += $fee;
                                                else
                                                    $total_cpl  = 0;
                                                if($item_tarifa['precio_menor'] > 0)
                                                    $total_cnn  += $fee;
                                                else
                                                    $total_cnn  = 0;
                                                if($item_tarifa['precio_infante'] > 0)
                                                    $total_inf  += $fee;
                                                else
                                                    $total_inf  = 0;
                                            }

                                            if($item_tarifa['increment_factura'] == 'P')
                                            {
                                                $iva        = 1 + ($item_tarifa['factura']/100);
                                                $total_sgl  *= $iva;
                                                $total_dbl  *= $iva;
                                                $total_tpl  *= $iva;
                                                $total_cpl  *= $iva;
                                                $total_cnn  *= $iva;
                                                $total_inf  *= $iva;
                                            }
                                            else
                                            {
                                                $iva        = $item_tarifa['factura'];
                                                if($item_tarifa['precio_single'] > 0)
                                                    $total_sgl  += $iva;
                                                else
                                                    $total_sgl  = 0;
                                                if($item_tarifa['precio_doble'] > 0)
                                                    $total_dbl  += $iva;
                                                else
                                                    $total_dbl  = 0;
                                                if($item_tarifa['precio_triple'] > 0)
                                                    $total_tpl  += $iva;
                                                else
                                                    $total_tpl  = 0;
                                                if($item_tarifa['precio_cuadruple'] > 0)
                                                    $total_cpl  += $iva;
                                                else
                                                    $total_cpl  = 0;
                                                if($item_tarifa['precio_menor'] > 0)
                                                    $total_cnn  += $iva;
                                                else
                                                    $total_cnn  = 0;
                                                if($item_tarifa['precio_infante'] > 0)
                                                    $total_inf  += $iva;
                                                else
                                                    $total_inf  = 0;
                                            }

                                            if($item_tarifa['moneda'] == 'B')
                                            {
                                                $tc         = $item_tarifa['tipo_cambio'];
                                                $total_sgl /= $tc;
                                                $total_dbl /= $tc;
                                                $total_tpl /= $tc;
                                                $total_cpl /= $tc;
                                                $total_cnn /= $tc;
                                                $total_inf /= $tc;
                                            }
                                        ?>
                                            <tr>
                                                <td><?php echo $item_tarifa['nombre_hotel'] . ' - ' . $item_tarifa['categoria'] . '*'; ?></td>
                                                <?php if(strpos($hide_columns, 'SGL')===false) { ?><td style="text-align:right;"><?php if($total_sgl>0) echo ceil($total_sgl); else echo '-'; ?></td><?php } ?>
                                                <?php if(strpos($hide_columns, 'DBL')===false) { ?><td style="text-align:right;"><?php if($total_dbl>0) echo ceil($total_dbl); else echo '-'; ?></td><?php } ?>
                                                <?php if(strpos($hide_columns, 'TPL')===false) { ?><td style="text-align:right;"><?php if($total_tpl>0) echo ceil($total_tpl); else echo '-'; ?></td><?php } ?>
                                                <?php if(strpos($hide_columns, 'CPL')===false) { ?><td style="text-align:right;"><?php if($total_cpl>0) echo ceil($total_cpl); else echo '-'; ?></td><?php } ?>
                                                <?php if(strpos($hide_columns, 'CNN')===false) { ?><td style="text-align:right;"><?php if($total_cnn>0) echo ceil($total_cnn); else echo '-'; ?></td><?php } ?>
                                                <?php if(strpos($hide_columns, 'INF')===false) { ?><td style="text-align:right;"><?php if($total_inf>0) echo ceil($total_inf); else echo '-'; ?></td><?php } ?>
                                                <td><?php echo strtoupper($item_tarifa['alimentacion']); ?></td>
                                                <td><?php echo strtoupper($item_tarifa['info_extra']); ?></td>
                                            </tr>
                                        <?php
                                        }
                                    ?>
                                        </table>
                                    <?
                                    }               
                                ?>
                            </td>
                        </tr>
                    <?php
                    }
                ?>

                <!-- OTROS SERVICIOS -->
                <?php
                    $otras_tarifas = $c_mostrar_paquete->obtener_otros_servicios_x_ciudad($idciudad_origen);
                    $serv_x_dest    = array();
                    $total_serv_adt = 0;
                    $total_serv_cnn = 0;
                    $total_serv_inf = 0;
                    if(count($otras_tarifas) > 0)
                    {
                    ?>
                        <tr class="todo_precio solo_servicio" style="display:none;">
                            <td colspan="2" style="padding:10px 0px 0px 30px;">
                                <font style="font-size:11pt; font-weight:bold; color:#ff3300; padding:15px;">OTROS SERVICIOS</font>
                                <table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios" >
                                    <tr style="background-color:#10689b; color:#FFFFFF;">
                                        <th rowspan="2">DETALLE</th>
                                        <th colspan="3">PRECIOS</th>
                                    </tr>
                                    <tr style="background-color:#10689b; color:#FFFFFF;">
                                        <th>ADULTO</th>
                                        <th>MENOR</th>
                                        <th>INFANTE</th>
                                    </tr>
                                    <?php
                                        for ($i=0; $i < count($otras_tarifas); $i++) 
                                        { 
                                            $tarifa_otros   = $otras_tarifas[$i];
                                            $idotra_tarifa  = $tarifa_otros['idtarifa_otros'];

                                            $ciudad_serv    = $tarifa_otros['ciudad'];
                                            
                                            $total_adt = $tarifa_otros['precio_adulto'];
                                            $total_cnn = $tarifa_otros['precio_menor'];
                                            $total_inf = $tarifa_otros['precio_infante'];

                                            if($tarifa_otros['increment_fee'] == 'P')
                                            {
                                                $fee        = 1+($tarifa_otros['fee'] / 100);
                                                $total_adt  *= $fee;
                                                $total_cnn  *= $fee;
                                                $total_inf  *= $fee;
                                            }
                                            else
                                            {
                                                $fee = $tarifa_otros['fee'];
                                                if($total_adt > 0)
                                                    $total_adt += $fee;
                                                else
                                                    $total_adt = 0;
                                                if($total_cnn > 0)
                                                    $total_cnn += $fee;
                                                else
                                                    $total_cnn = 0;
                                                if($total_inf > 0)
                                                    $total_inf += $fee;
                                                else
                                                    $total_inf = 0;
                                            }

                                            if($tarifa_otros['increment_factura'] == 'P')
                                            {
                                                $iva        = 1+($tarifa_otros['factura'] / 100);
                                                $total_adt  *= $iva;
                                                $total_cnn  *= $iva;
                                                $total_inf  *= $iva;
                                            }
                                            else
                                            {
                                                $iva = $tarifa_otros['factura'];
                                                if($total_adt > 0)
                                                    $total_adt += $iva;
                                                else
                                                    $total_adt = 0;
                                                if($total_cnn > 0)
                                                    $total_cnn += $iva;
                                                else
                                                    $total_cnn = 0;
                                                if($total_inf > 0)
                                                    $total_inf += $iva;
                                                else
                                                    $total_inf = 0;
                                            }

                                            if($tarifa_otros['moneda'] == 'B')
                                            {
                                                $tc         = $tarifa_otros['tipo_cambio'];
                                                $total_adt /= $tc;
                                                $total_cnn /= $tc;
                                                $total_inf /= $tc;
                                            }

                                            $total_serv_adt += ceil($total_adt);
                                            $total_serv_cnn += ceil($total_cnn);
                                            $total_serv_inf += ceil($total_inf);

                                            if(!array_key_exists($ciudad_serv, $serv_x_dest))
                                            {
                                                $serv_x_dest[$ciudad_serv] = array();
                                                $serv_x_dest[$ciudad_serv]['adt'] = 0;
                                                $serv_x_dest[$ciudad_serv]['cnn'] = 0;
                                                $serv_x_dest[$ciudad_serv]['inf'] = 0;
                                            }

                                            $serv_x_dest[$ciudad_serv]['adt'] += ceil($total_adt);
                                            $serv_x_dest[$ciudad_serv]['cnn'] += ceil($total_cnn);
                                            $serv_x_dest[$ciudad_serv]['inf'] += ceil($total_inf);

                                            $nombre_servicio = $c_mostrar_paquete->obtener_nombre_servicio($tarifa_otros['idotro_servicio']);
                                        ?>
                                            <tr>
                                                <td><?php echo strtoupper($nombre_servicio . ' ' . $tarifa_otros['descripcion']); ?></td>
                                                <td style="text-align:right;">
                                                    <?php if($total_adt>0) echo ceil($total_adt); else echo '-'; ?>
                                                </td>
                                                <td style="text-align:right;">
                                                    <?php if($total_cnn>0) echo ceil($total_cnn); else echo '-'; ?>
                                                </td>
                                                <td style="text-align:right;">
                                                    <?php if($total_inf>0) echo ceil($total_inf); else echo '-'; ?>
                                                </td>
                                            </tr>
                                        <?php
                                        }
                                    ?>
                                </table>
                            </td>
                        </tr>
                    <?php
                    }
                ?>

                <!-- MUESTRA HOTEL MAS SERVICIOS -->
                <?php
                    if(count($tarifas_por_destino) > 0)
                    {
                    ?>
                        <tr class="todo_precio hotel_mas_servicio" style="display:none;" >
                            <td colspan="2" style="padding:10px 0px 0px 30px;">
                                <font style="font-size:13pt; font-weight:bold; color:#ff3300;">PRECIOS POR PERSONA EN DOLARES AMERICANOS</font>
                                <br/><font style="font-size:11pt; font-weight:bold; color:#ff3300; padding:15px;">PAQUETE TERRESTRE</font>
                                <?php 
                                    for ($i=0; $i < count($tarifas_por_destino); $i++) 
                                    { 
                                        $tarifa_hotel       = $tarifas_por_destino[$i];
                                        $idth               = $tarifa_hotel['idtarifa_hotel'];
                                        $dest_hotel         = $tarifa_hotel['destino'];
                                        $items_tarifa       = $c_mostrar_paquete->obtener_hoteles_x_destino($idth);

                                        $hide_columns       = $c_mostrar_paquete->evaluar_precios_hotel($items_tarifa);
                                        $cant_col           = count(explode('/', $hide_columns));
                                    ?>
                                        <table style="width:50%; margin-left:3%;">
                                            <tr>
                                                <td style="width:5%;">
                                                    <strong>DESTINO :</strong>
                                                </td>
                                                <td style="width:20%;">
                                                    <?php echo $c_mostrar_paquete->obtener_aeropuerto(strtoupper($tarifa_hotel['destino'])); ?>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                                        <table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios">
                                            <tr style="background-color:#10689b; color:#FFFFFF;">
                                                <th>HOTEL</th>
                                                <?php if(strpos($hide_columns, 'SGL')===false) { ?><th>SINGLE</th><?php } ?>
                                                <?php if(strpos($hide_columns, 'DBL')===false) { ?><th>DOBLE</th><?php } ?>
                                                <?php if(strpos($hide_columns, 'TPL')===false) { ?><th>TRIPLE</th><?php } ?>
                                                <?php if(strpos($hide_columns, 'CPL')===false) { ?><th>CUADRUPLE</th><?php } ?>
                                                <?php if(strpos($hide_columns, 'CNN')===false) { ?><th>MENOR</th><?php } ?>
                                                <?php if(strpos($hide_columns, 'INF')===false) { ?><th>INFANTE</th><?php } ?>
                                                <th>ALIMENTACION</th>
                                                <th>OBS</th>
                                            </tr>
                                    <?php
                                        for ($j=0; $j < count($items_tarifa); $j++) 
                                        { 
                                            $item_tarifa    = $items_tarifa[$j];
                                            $iditem         = $item_tarifa['iditem_hotel'];

                                            $cant_noches    = floor( (strtotime($item_tarifa['fecha_out'] . ' 00:00:00') - strtotime($item_tarifa['fecha_in'] . ' 00:00:00')) / 86400);
                                            
                                            $total_sgl  = $item_tarifa['precio_single'] * $cant_noches;
                                            $total_dbl  = $item_tarifa['precio_doble'] * $cant_noches;
                                            $total_tpl  = $item_tarifa['precio_triple'] * $cant_noches;
                                            $total_cpl  = $item_tarifa['precio_cuadruple'] * $cant_noches;
                                            $total_cnn  = $item_tarifa['precio_menor'] * $cant_noches;
                                            $total_inf  = $item_tarifa['precio_infante'] * $cant_noches;

                                            if($item_tarifa['increment_fee'] == 'P')
                                            {
                                                $fee        = 1 + ($item_tarifa['fee']/100);
                                                $total_sgl  *= $fee;
                                                $total_dbl  *= $fee;
                                                $total_tpl  *= $fee;
                                                $total_cpl  *= $fee;
                                                $total_cnn  *= $fee;
                                                $total_inf  *= $fee;
                                            }
                                            else
                                            {
                                                $fee        = $item_tarifa['fee'];
                                                if($item_tarifa['precio_single'] > 0)
                                                    $total_sgl  += $fee;
                                                else
                                                    $total_sgl  = 0;
                                                if($item_tarifa['precio_doble'] > 0)
                                                    $total_dbl  += $fee;
                                                else
                                                    $total_dbl  = 0;
                                                if($item_tarifa['precio_triple'] > 0)
                                                    $total_tpl  += $fee;
                                                else
                                                    $total_tpl  = 0;
                                                if($item_tarifa['precio_cuadruple'] > 0)
                                                    $total_cpl  += $fee;
                                                else
                                                    $total_cpl  = 0;
                                                if($item_tarifa['precio_menor'] > 0)
                                                    $total_cnn  += $fee;
                                                else
                                                    $total_cnn  = 0;
                                                if($item_tarifa['precio_infante'] > 0)
                                                    $total_inf  += $fee;
                                                else
                                                    $total_inf  = 0;
                                            }

                                            if($item_tarifa['increment_factura'] == 'P')
                                            {
                                                $iva        = 1 + ($item_tarifa['factura']/100);
                                                $total_sgl  *= $iva;
                                                $total_dbl  *= $iva;
                                                $total_tpl  *= $iva;
                                                $total_cpl  *= $iva;
                                                $total_cnn  *= $iva;
                                                $total_inf  *= $iva;
                                            }
                                            else
                                            {
                                                $iva        = $item_tarifa['factura'];
                                                if($item_tarifa['precio_single'] > 0)
                                                    $total_sgl  += $iva;
                                                else
                                                    $total_sgl  = 0;
                                                if($item_tarifa['precio_doble'] > 0)
                                                    $total_dbl  += $iva;
                                                else
                                                    $total_dbl  = 0;
                                                if($item_tarifa['precio_triple'] > 0)
                                                    $total_tpl  += $iva;
                                                else
                                                    $total_tpl  = 0;
                                                if($item_tarifa['precio_cuadruple'] > 0)
                                                    $total_cpl  += $iva;
                                                else
                                                    $total_cpl  = 0;
                                                if($item_tarifa['precio_menor'] > 0)
                                                    $total_cnn  += $iva;
                                                else
                                                    $total_cnn  = 0;
                                                if($item_tarifa['precio_infante'] > 0)
                                                    $total_inf  += $iva;
                                                else
                                                    $total_inf  = 0;
                                            }

                                            if($item_tarifa['moneda'] == 'B')
                                            {
                                                $tc         = $item_tarifa['tipo_cambio'];
                                                $total_sgl /= $tc;
                                                $total_dbl /= $tc;
                                                $total_tpl /= $tc;
                                                $total_cpl /= $tc;
                                                $total_cnn /= $tc;
                                                $total_inf /= $tc;
                                            }

                                            // SUMAR PRECIOS CON LOS OTROS SERVICIOS
                                            if($total_sgl > 0)
                                                $total_sgl += $serv_x_dest[$dest_hotel]['adt'];
                                            if($total_dbl > 0)
                                                $total_dbl += $serv_x_dest[$dest_hotel]['adt'];
                                            if($total_tpl > 0)
                                                $total_tpl += $serv_x_dest[$dest_hotel]['adt'];
                                            if($total_cpl > 0)
                                                $total_cpl += $serv_x_dest[$dest_hotel]['adt'];
                                            if($total_cnn > 0)
                                                $total_cnn += $serv_x_dest[$dest_hotel]['cnn'];
                                            if($total_inf > 0)
                                                $total_inf += $serv_x_dest[$dest_hotel]['inf'];
                                        ?>
                                            <tr>
                                                <td><?php echo $item_tarifa['nombre_hotel'] . ' - ' . $item_tarifa['categoria'] . '*'; ?></td>
                                                <?php if(strpos($hide_columns, 'SGL')===false) { ?><td style="text-align:right;"><?php if($total_sgl>0) echo ceil($total_sgl); else echo '-'; ?></td><?php } ?>
                                                <?php if(strpos($hide_columns, 'DBL')===false) { ?><td style="text-align:right;"><?php if($total_dbl>0) echo ceil($total_dbl); else echo '-'; ?></td><?php } ?>
                                                <?php if(strpos($hide_columns, 'TPL')===false) { ?><td style="text-align:right;"><?php if($total_tpl>0) echo ceil($total_tpl); else echo '-'; ?></td><?php } ?>
                                                <?php if(strpos($hide_columns, 'CPL')===false) { ?><td style="text-align:right;"><?php if($total_cpl>0) echo ceil($total_cpl); else echo '-'; ?></td><?php } ?>
                                                <?php if(strpos($hide_columns, 'CNN')===false) { ?><td style="text-align:right;"><?php if($total_cnn>0) echo ceil($total_cnn); else echo '-'; ?></td><?php } ?>
                                                <?php if(strpos($hide_columns, 'INF')===false) { ?><td style="text-align:right;"><?php if($total_inf>0) echo ceil($total_inf); else echo '-'; ?></td><?php } ?>
                                                <td><?php echo strtoupper($item_tarifa['alimentacion']); ?></td>
                                                <td><?php echo strtoupper($item_tarifa['info_extra']); ?></td>
                                            </tr>
                                        <?php
                                        }
                                    ?>
                                        </table>
                                    <?
                                    }               
                                ?>
                            </td>
                        </tr>
                    <?php
                    }
                ?>

                <!-- HOTELES COMBINADO MAS SERVICIOS -->
                <?php
                    $opciones_de_hoteles =  $c_mostrar_paquete->obtener_opciones_hotel_combinados($idciudad_origen);
                    if(array_key_exists(0, $opciones_de_hoteles))
                    {

                    ?>
                        <tr class="todo_precio hotel_comb_mas_servicio" style="display:none;" >
                            <td colspan="2" style="padding:10px 0px 0px 30px;">
                                <font style="font-size:13pt; font-weight:bold; color:#ff3300;">PRECIOS POR PERSONA EN DOLARES AMERICANOS</font>
                                <br/><font style="font-size:11pt; font-weight:bold; color:#ff3300; padding:15px;">PAQUETE TERRESTRE</font>
                            <?php 
                                $cant_col = $opciones_de_hoteles['cant_cols'];
                                $mostrar_cols = $opciones_de_hoteles['col_mostrar'];
                                ?>
                                    <table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios">
                                        <tr style="background-color:#10689b; color:#FFFFFF;">
                                            <th>HOTEL</th>
                                            <?php if(strpos($mostrar_cols, 'sgl')!==false) { ?><th>SINGLE</th><?php } ?>
                                            <?php if(strpos($mostrar_cols, 'dbl')!==false) { ?><th>DOBLE</th><?php } ?>
                                            <?php if(strpos($mostrar_cols, 'tpl')!==false) { ?><th>TRIPLE</th><?php } ?>
                                            <?php if(strpos($mostrar_cols, 'cpl')!==false) { ?><th>CUADRUPLE</th><?php } ?>
                                            <?php if(strpos($mostrar_cols, 'cnn')!==false) { ?><th>MENOR</th><?php } ?>
                                            <?php if(strpos($mostrar_cols, 'inf')!==false) { ?><th>INFANTE</th><?php } ?>
                                            <th>ALIMENTACION</th>
                                            <th>OBS</th>
                                        </tr>
                            <?php
                                for ($i=0; array_key_exists($i, $opciones_de_hoteles); $i++) 
                                { 
                                    $opcion_hotel       = $opciones_de_hoteles[$i];
                                    if(array_key_exists('precio_sgl', $opcion_hotel))
                                        $opcion_hotel['precio_sgl'] += $total_serv_adt;
                                    if(array_key_exists('precio_dbl', $opcion_hotel))
                                        $opcion_hotel['precio_dbl'] += $total_serv_adt;
                                    if(array_key_exists('precio_tpl', $opcion_hotel))
                                        $opcion_hotel['precio_tpl'] += $total_serv_adt;
                                    if(array_key_exists('precio_cpl', $opcion_hotel))
                                        $opcion_hotel['precio_cpl'] += $total_serv_adt;
                                    if(array_key_exists('precio_cnn', $opcion_hotel))
                                        $opcion_hotel['precio_cnn'] += $total_serv_cnn;
                                    if(array_key_exists('precio_inf', $opcion_hotel))
                                        $opcion_hotel['precio_inf'] += $total_serv_inf;
                                ?>
                                        <tr>
                                            <td style="line-height:1.5;">
                                                <?php
                                                    $hoteles = $opcion_hotel['hoteles'];
                                                    for ($j=0; $j < count($hoteles); $j++) 
                                                    { 
                                                        $item_tarifa = $hoteles[$j];
                                                        echo $item_tarifa['nombre_hotel'] . ' - ' . $item_tarifa['categoria'] . '* (' . $item_tarifa['destino'] . ')<br/>'; 
                                                    }
                                                ?>
                                            </td>
                                            <?php if(strpos($mostrar_cols, 'sgl')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_sgl']; ?></td><?php } ?>
                                            <?php if(strpos($mostrar_cols, 'dbl')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_dbl']; ?></td><?php } ?>
                                            <?php if(strpos($mostrar_cols, 'tpl')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_tpl']; ?></td><?php } ?>
                                            <?php if(strpos($mostrar_cols, 'cpl')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_cpl']; ?></td><?php } ?>
                                            <?php if(strpos($mostrar_cols, 'cnn')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_cnn']; ?></td><?php } ?>
                                            <?php if(strpos($mostrar_cols, 'inf')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_inf']; ?></td><?php } ?>
                                            <td style="line-height:1.5;">
                                                <?php
                                                    $hoteles = $opcion_hotel['hoteles'];
                                                    for ($j=0; $j < count($hoteles); $j++) 
                                                    { 
                                                        $item_tarifa = $hoteles[$j];
                                                        echo strtoupper($item_tarifa['alimentacion']) . '<br/>';
                                                    }
                                                ?>
                                            </td>
                                            <td style="line-height:1.5;">
                                                <?php
                                                    $hoteles = $opcion_hotel['hoteles'];
                                                    for ($j=0; $j < count($hoteles); $j++) 
                                                    { 
                                                        $item_tarifa = $hoteles[$j];
                                                        echo strtoupper($item_tarifa['info_extra']) . '<br/>';
                                                    }
                                                ?>
                                            </td>
                                        </tr>                                   
                                <?php
                                }               
                            ?>
                                    </table>
                            </td>
                        </tr>
                    <?php
                    }
                ?>

                <!-- BOLETOS AEREOS -->
                <?php
                    $opciones_aereo = $c_mostrar_paquete->obtener_opcion_boletos_x_ciudad($idciudad_origen);
                    if(count($opciones_aereo) > 0)
                    {
                    ?>
                        <tr class="todo_precio solo_aereo" style="display:none;">
                            <td colspan="2" style="padding:10px 0px 0px 30px;">
                                <font style="font-size:11pt; font-weight:bold; color:#ff3300; padding:15px;">BOLETOS AEREOS</font>
                                <?php
                                    for ($i=0; $i < count($opciones_aereo); $i++) 
                                    { 
                                        $opcion_aereo   = $opciones_aereo[$i];
                                        $idopaereo      = $opcion_aereo['idopcion_boleto'];
                                    ?>
                                        <div class="opc<?php echo $z . '_' . $i; ?> opc_aereo<?php echo $z; ?>" >
                                            <table style="width:70%; margin-left:3%;">
                                                <tr>
                                                    <td style="width:10%;">
                                                        <strong>OPCION :</strong>
                                                    </td>
                                                    <td style="width:30%;">
                                                        <?php echo strtoupper($opcion_aereo['descripcion']); ?>
                                                    </td>
                                                    <td style="width:10%;">
                                                        <strong>SALE :</strong>
                                                    </td>
                                                    <td style="width:30%;">
                                                        <?php echo strtoupper($opcion_aereo['salida']); ?>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                            <table style="width:90%; font-size:7pt; margin-left:3%;" class="tabla-precios" >
                                                <tr style="background-color:#10689b; color:#FFFFFF;">
                                                    <th rowspan="2">LINEA AEREA</th>
                                                    <th rowspan="2">RUTA</th>
                                                    <th colspan="3">PRECIOS</th>
                                                </tr>
                                                <tr style="background-color:#10689b; color:#FFFFFF;">
                                                    <th>ADULTO</th>
                                                    <th>MENOR</th>
                                                    <th>INFANTE</th>
                                                </tr>
                                                <?php
                                                    $boletos = $c_mostrar_paquete->obtener_boletos_x_opcion_aereo($idopaereo);
                                                    for ($j=0; $j < count($boletos); $j++) 
                                                    { 
                                                        $boleto_cotizado    = $boletos[$j];
                                                        $idboleto           = $boleto_cotizado['idboleto_cotizado'];
                                                        
                                                        $total_adt = $boleto_cotizado['precio_adulto'];
                                                        $total_cnn = $boleto_cotizado['precio_menor'];
                                                        $total_inf = $boleto_cotizado['precio_infante'];

                                                        if($boleto_cotizado['increment_fee'] == 'P')
                                                        {
                                                            $fee        = 1+($boleto_cotizado['fee'] / 100);
                                                            $total_adt  *= $fee;
                                                            $total_cnn  *= $fee;
                                                            $total_inf  *= $fee;
                                                        }
                                                        else
                                                        {
                                                            $fee = $boleto_cotizado['fee'];
                                                            if($total_adt > 0)
                                                                $total_adt += $fee;
                                                            else
                                                                $total_adt = 0;
                                                            if($total_cnn > 0)
                                                                $total_cnn += $fee;
                                                            else
                                                                $total_cnn = 0;
                                                            if($total_inf > 0)
                                                                $total_inf += $fee;
                                                            else
                                                                $total_inf = 0;
                                                        }

                                                        if($boleto_cotizado['increment_factura'] == 'P')
                                                        {
                                                            $iva        = 1+($boleto_cotizado['factura'] / 100);
                                                            $total_adt  *= $iva;
                                                            $total_cnn  *= $iva;
                                                            $total_inf  *= $iva;
                                                        }
                                                        else
                                                        {
                                                            $iva = $boleto_cotizado['factura'];
                                                            if($total_adt > 0)
                                                                $total_adt += $iva;
                                                            else
                                                                $total_adt = 0;
                                                            if($total_cnn > 0)
                                                                $total_cnn += $iva;
                                                            else
                                                                $total_cnn = 0;
                                                            if($total_inf > 0)
                                                                $total_inf += $iva;
                                                            else
                                                                $total_inf = 0;
                                                        }

                                                        if($boleto_cotizado['moneda'] == 'B')
                                                        {
                                                            $tc         = $boleto_cotizado['tipo_cambio'];
                                                            $total_adt /= $tc;
                                                            $total_cnn /= $tc;
                                                            $total_inf /= $tc;
                                                        }

                                                        $total_bol_adt += $total_adt;
                                                        $total_bol_cnn += $total_cnn;
                                                        $total_bol_inf += $total_inf;
                                                    ?>
                                                        <tr>
                                                            <td><?php echo strtoupper($c_mostrar_paquete->obtener_linea_aerea($boleto_cotizado['idlineas_aereas'])); ?></td>
                                                            <td><?php echo strtoupper($boleto_cotizado['ruta']); ?></td>
                                                            <td style="text-align:right;"><?php if($total_adt>0) echo ceil($total_adt); else echo '-'; ?></td>
                                                            <td style="text-align:right;"><?php if($total_cnn>0) echo ceil($total_cnn); else echo '-'; ?></td>
                                                            <td style="text-align:right;"><?php if($total_inf>0) echo ceil($total_inf); else echo '-'; ?></td>
                                                        </tr>
                                                    <?php
                                                    }
                                                ?>
                                            </table>
                                            <br/>
                                            <font style="font-size:10pt; font-weight:bold; margin-left:3%;">ITINERARIO AEREO DETALLADO</font><br/>
                                            <table style="width:90%; font-size:7pt; margin-left:3%;" class="tabla-precios" >
                                                <tr style="background-color:#10689b; color:#FFFFFF;">
                                                    <th>LINEA AEREA</th>
                                                    <th>VUELO</th>
                                                    <th>FECHA</th>
                                                    <th>SALE</th>
                                                    <th>HRS</th>
                                                    <th>LLEGA</th>
                                                    <th>HRS</th>
                                                </tr>
                                                <?php
                                                    $itinerario = $c_mostrar_paquete->obtener_itinerario_x_opcion_aereo($idopaereo);
                                                    for ($k=0; $k < count($itinerario); $k++) 
                                                    { 
                                                        $segmento   = $itinerario[$k];
                                                        $idsegmento = $segmento['idsegmento'];

                                                        $linea_aerea        = $c_mostrar_paquete->obtener_linea_aerea($segmento['idlineas_aereas']);
                                                        $aeropuerto_sale    = $c_mostrar_paquete->obtener_aeropuerto($segmento['origen']);
                                                        $aeropuerto_llega   = $c_mostrar_paquete->obtener_aeropuerto($segmento['destino']);
                                                ?>
                                                        <tr>
                                                            <td><?php echo strtoupper($linea_aerea); ?></td>
                                                            <td><?php echo $segmento['nro_vuelo']; ?></td>
                                                            <td><?php echo date('d/m/Y', strtotime($segmento['fecha'])); ?></td>
                                                            <td><?php echo strtoupper($aeropuerto_sale); ?></td>
                                                            <td><?php echo substr($segmento['hora_sale'], 0, 5); ?></td>
                                                            <td><?php echo strtoupper($aeropuerto_llega); ?></td>
                                                            <td><?php echo substr($segmento['hora_llega'], 0, 5); ?></td>
                                                        </tr>
                                                <?php
                                                    }
                                                ?>
                                            </table>
                                        </div>
                                    <?php
                                    }
                                ?>
                            </tr>
                        </td>
                    <?php
                    }
                ?>

                <!-- MUESTRA HOTEL MAS SERVICIOS MAS AEREO -->
                <?php
                    if(count($opciones_aereo) > 0)
                    {
                    ?>
                        <tr class="todo_precio hotel_mas_servicio_mas_aereo" >
                            <td colspan="2" style="padding:10px 0px 0px 30px;">
                                <font style="font-size:13pt; font-weight:bold; color:#ff3300;">PRECIOS POR PERSONA EN DOLARES AMERICANOS</font>
                                <?php
                                    for ($g=0; $g < count($opciones_aereo); $g++) 
                                    { 
                                        $opcion_aereo   = $opciones_aereo[$g];
                                        $idopaereo      = $opcion_aereo['idopcion_boleto'];
                                    ?>
                                        <div class="opc<?php echo $z . '_' . $g; ?> opc_aereo<?php echo $z; ?>" >
                                            <table style="width:70%; margin-left:3%;">
                                                <tr>
                                                    <td style="width:15%;">
                                                        <strong>OPCION :</strong>
                                                    </td>
                                                    <td style="width:30%;">
                                                        <?php echo strtoupper($opcion_aereo['descripcion']); ?>
                                                    </td>
                                                    <td style="width:15%;">
                                                        <strong>SALE :</strong>
                                                    </td>
                                                    <td style="width:30%;">
                                                        <?php echo strtoupper($opcion_aereo['salida']); ?>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                            <?php
                                                $boletos = $c_mostrar_paquete->obtener_boletos_x_opcion_aereo($idopaereo);
                                                $total_bol_adt = 0;
                                                $total_bol_cnn = 0;
                                                $total_bol_inf = 0;
                                                for ($j=0; $j < count($boletos); $j++) 
                                                { 
                                                    $boleto_cotizado    = $boletos[$j];
                                                    $idboleto           = $boleto_cotizado['idboleto_cotizado'];
                                                    
                                                    $total_adt = $boleto_cotizado['precio_adulto'];
                                                    $total_cnn = $boleto_cotizado['precio_menor'];
                                                    $total_inf = $boleto_cotizado['precio_infante'];

                                                    if($boleto_cotizado['increment_fee'] == 'P')
                                                    {
                                                        $fee        = 1+($boleto_cotizado['fee'] / 100);
                                                        $total_adt  *= $fee;
                                                        $total_cnn  *= $fee;
                                                        $total_inf  *= $fee;
                                                    }
                                                    else
                                                    {
                                                        $fee = $boleto_cotizado['fee'];
                                                        if($total_adt > 0)
                                                            $total_adt += $fee;
                                                        else
                                                            $total_adt = 0;
                                                        if($total_cnn > 0)
                                                            $total_cnn += $fee;
                                                        else
                                                            $total_cnn = 0;
                                                        if($total_inf > 0)
                                                            $total_inf += $fee;
                                                        else
                                                            $total_inf = 0;
                                                    }

                                                    if($boleto_cotizado['increment_factura'] == 'P')
                                                    {
                                                        $iva        = 1+($boleto_cotizado['factura'] / 100);
                                                        $total_adt  *= $iva;
                                                        $total_cnn  *= $iva;
                                                        $total_inf  *= $iva;
                                                    }
                                                    else
                                                    {
                                                        $iva = $boleto_cotizado['factura'];
                                                        if($total_adt > 0)
                                                            $total_adt += $iva;
                                                        else
                                                            $total_adt = 0;
                                                        if($total_cnn > 0)
                                                            $total_cnn += $iva;
                                                        else
                                                            $total_cnn = 0;
                                                        if($total_inf > 0)
                                                            $total_inf += $iva;
                                                        else
                                                            $total_inf = 0;
                                                    }

                                                    if($boleto_cotizado['moneda'] == 'B')
                                                    {
                                                        $tc         = $boleto_cotizado['tipo_cambio'];
                                                        $total_adt /= $tc;
                                                        $total_cnn /= $tc;
                                                        $total_inf /= $tc;
                                                    }

                                                    $total_bol_adt += ceil($total_adt);
                                                    $total_bol_cnn += ceil($total_cnn);
                                                    $total_bol_inf += ceil($total_inf);
                                                }
                                            ?>
                                            <?php
                                                if(count($tarifas_por_destino) > 0)
                                                {
                                                ?>
                                                    <div class="clear"></div>
                                                    <?php 
                                                        for ($i=0; $i < count($tarifas_por_destino); $i++) 
                                                        { 
                                                            $tarifa_hotel       = $tarifas_por_destino[$i];
                                                            $idth               = $tarifa_hotel['idtarifa_hotel'];
                                                            $items_tarifa       = $c_mostrar_paquete->obtener_hoteles_x_destino($idth);
                                                            $dest_hotel         = $tarifa_hotel['destino'];

                                                            $hide_columns       = $c_mostrar_paquete->evaluar_precios_hotel($items_tarifa);
                                                            $cant_col           = count(explode('/', $hide_columns));
                                                        ?>
                                                            <strong style="margin-left:3%;">HOTEL(ES) EN <?php echo $c_mostrar_paquete->obtener_aeropuerto(strtoupper($tarifa_hotel['destino'])); ?></strong>
                                                            <table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios">
                                                                <tr style="background-color:#10689b; color:#FFFFFF;">
                                                                    <th>HOTEL</th>
                                                                    <?php if(strpos($hide_columns, 'SGL')===false) { ?><th>SINGLE</th><?php } ?>
                                                                    <?php if(strpos($hide_columns, 'DBL')===false) { ?><th>DOBLE</th><?php } ?>
                                                                    <?php if(strpos($hide_columns, 'TPL')===false) { ?><th>TRIPLE</th><?php } ?>
                                                                    <?php if(strpos($hide_columns, 'CPL')===false) { ?><th>CUADRUPLE</th><?php } ?>
                                                                    <?php if(strpos($hide_columns, 'CNN')===false) { ?><th>MENOR</th><?php } ?>
                                                                    <?php if(strpos($hide_columns, 'INF')===false) { ?><th>INFANTE</th><?php } ?>
                                                                    <th>ALIMENTACION</th>
                                                                    <th>OBS</th>
                                                                </tr>
                                                        <?php
                                                            for ($j=0; $j < count($items_tarifa); $j++) 
                                                            { 
                                                                $item_tarifa    = $items_tarifa[$j];
                                                                $iditem         = $item_tarifa['iditem_hotel'];

                                                                $cant_noches    = floor( (strtotime($item_tarifa['fecha_out'] . ' 00:00:00') - strtotime($item_tarifa['fecha_in'] . ' 00:00:00')) / 86400);
                                                                
                                                                $total_sgl  = $item_tarifa['precio_single'] * $cant_noches;
                                                                $total_dbl  = $item_tarifa['precio_doble'] * $cant_noches;
                                                                $total_tpl  = $item_tarifa['precio_triple'] * $cant_noches;
                                                                $total_cpl  = $item_tarifa['precio_cuadruple'] * $cant_noches;
                                                                $total_cnn  = $item_tarifa['precio_menor'] * $cant_noches;
                                                                $total_inf  = $item_tarifa['precio_infante'] * $cant_noches;

                                                                if($item_tarifa['increment_fee'] == 'P')
                                                                {
                                                                    $fee        = 1 + ($item_tarifa['fee']/100);
                                                                    $total_sgl  *= $fee;
                                                                    $total_dbl  *= $fee;
                                                                    $total_tpl  *= $fee;
                                                                    $total_cpl  *= $fee;
                                                                    $total_cnn  *= $fee;
                                                                    $total_inf  *= $fee;
                                                                }
                                                                else
                                                                {
                                                                    $fee        = $item_tarifa['fee'];
                                                                    if($item_tarifa['precio_single'] > 0)
                                                                        $total_sgl  += $fee;
                                                                    else
                                                                        $total_sgl  = 0;
                                                                    if($item_tarifa['precio_doble'] > 0)
                                                                        $total_dbl  += $fee;
                                                                    else
                                                                        $total_dbl  = 0;
                                                                    if($item_tarifa['precio_triple'] > 0)
                                                                        $total_tpl  += $fee;
                                                                    else
                                                                        $total_tpl  = 0;
                                                                    if($item_tarifa['precio_cuadruple'] > 0)
                                                                        $total_cpl  += $fee;
                                                                    else
                                                                        $total_cpl  = 0;
                                                                    if($item_tarifa['precio_menor'] > 0)
                                                                        $total_cnn  += $fee;
                                                                    else
                                                                        $total_cnn  = 0;
                                                                    if($item_tarifa['precio_infante'] > 0)
                                                                        $total_inf  += $fee;
                                                                    else
                                                                        $total_inf  = 0;
                                                                }

                                                                if($item_tarifa['increment_factura'] == 'P')
                                                                {
                                                                    $iva        = 1 + ($item_tarifa['factura']/100);
                                                                    $total_sgl  *= $iva;
                                                                    $total_dbl  *= $iva;
                                                                    $total_tpl  *= $iva;
                                                                    $total_cpl  *= $iva;
                                                                    $total_cnn  *= $iva;
                                                                    $total_inf  *= $iva;
                                                                }
                                                                else
                                                                {
                                                                    $iva        = $item_tarifa['factura'];
                                                                    if($item_tarifa['precio_single'] > 0)
                                                                        $total_sgl  += $iva;
                                                                    else
                                                                        $total_sgl  = 0;
                                                                    if($item_tarifa['precio_doble'] > 0)
                                                                        $total_dbl  += $iva;
                                                                    else
                                                                        $total_dbl  = 0;
                                                                    if($item_tarifa['precio_triple'] > 0)
                                                                        $total_tpl  += $iva;
                                                                    else
                                                                        $total_tpl  = 0;
                                                                    if($item_tarifa['precio_cuadruple'] > 0)
                                                                        $total_cpl  += $iva;
                                                                    else
                                                                        $total_cpl  = 0;
                                                                    if($item_tarifa['precio_menor'] > 0)
                                                                        $total_cnn  += $iva;
                                                                    else
                                                                        $total_cnn  = 0;
                                                                    if($item_tarifa['precio_infante'] > 0)
                                                                        $total_inf  += $iva;
                                                                    else
                                                                        $total_inf  = 0;
                                                                }

                                                                if($item_tarifa['moneda'] == 'B')
                                                                {
                                                                    $tc         = $item_tarifa['tipo_cambio'];
                                                                    $total_sgl /= $tc;
                                                                    $total_dbl /= $tc;
                                                                    $total_tpl /= $tc;
                                                                    $total_cpl /= $tc;
                                                                    $total_cnn /= $tc;
                                                                    $total_inf /= $tc;
                                                                }

                                                                // SUMAR PRECIOS CON LOS OTROS SERVICIOS
                                                                if($total_sgl > 0)
                                                                    $total_sgl += $total_serv_adt + $total_bol_adt;
                                                                if($total_dbl > 0)
                                                                    $total_dbl += $total_serv_adt + $total_bol_adt;
                                                                if($total_tpl > 0)
                                                                    $total_tpl += $total_serv_adt + $total_bol_adt;
                                                                if($total_cpl > 0)
                                                                    $total_cpl += $total_serv_adt + $total_bol_adt;
                                                                if($total_cnn > 0)
                                                                    $total_cnn += $total_serv_cnn + $total_bol_cnn;
                                                                if($total_inf > 0)
                                                                    $total_inf += $total_serv_inf + $total_bol_inf;
                                                            ?>
                                                                <tr>
                                                                    <td><?php echo $item_tarifa['nombre_hotel'] . ' - ' . $item_tarifa['categoria'] . '*'; ?></td>
                                                                    <?php if(strpos($hide_columns, 'SGL')===false) { ?><td style="text-align:right;"><?php if($total_sgl>0) echo ceil($total_sgl); else echo '-'; ?></td><?php } ?>
                                                                    <?php if(strpos($hide_columns, 'DBL')===false) { ?><td style="text-align:right;"><?php if($total_dbl>0) echo ceil($total_dbl); else echo '-'; ?></td><?php } ?>
                                                                    <?php if(strpos($hide_columns, 'TPL')===false) { ?><td style="text-align:right;"><?php if($total_tpl>0) echo ceil($total_tpl); else echo '-'; ?></td><?php } ?>
                                                                    <?php if(strpos($hide_columns, 'CPL')===false) { ?><td style="text-align:right;"><?php if($total_cpl>0) echo ceil($total_cpl); else echo '-'; ?></td><?php } ?>
                                                                    <?php if(strpos($hide_columns, 'CNN')===false) { ?><td style="text-align:right;"><?php if($total_cnn>0) echo ceil($total_cnn); else echo '-'; ?></td><?php } ?>
                                                                    <?php if(strpos($hide_columns, 'INF')===false) { ?><td style="text-align:right;"><?php if($total_inf>0) echo ceil($total_inf); else echo '-'; ?></td><?php } ?>
                                                                    <td><?php echo strtoupper($item_tarifa['alimentacion']); ?></td>
                                                                    <td><?php echo strtoupper($item_tarifa['info_extra']); ?></td>
                                                                </tr>
                                                            <?php
                                                            }
                                                        ?>
                                                            </table>
                                                        <?
                                                        }               
                                                    ?>
                                                    <div class="clear_mayor"></div>
                                                <?php
                                                }
                                            ?>
                                            <br/>
                                            <font style="font-size:10pt; font-weight:bold; margin-left:3%;">ITINERARIO AEREO DETALLADO</font><br/>
                                            <table style="width:90%; font-size:7pt; margin-left:3%;" class="tabla-precios" >
                                                <tr style="background-color:#10689b; color:#FFFFFF;">
                                                    <th>LINEA AEREA</th>
                                                    <th>VUELO</th>
                                                    <th>FECHA</th>
                                                    <th>SALE</th>
                                                    <th>HRS</th>
                                                    <th>LLEGA</th>
                                                    <th>HRS</th>
                                                </tr>
                                                <?php
                                                    $itinerario = $c_mostrar_paquete->obtener_itinerario_x_opcion_aereo($idopaereo);
                                                    for ($k=0; $k < count($itinerario); $k++) 
                                                    { 
                                                        $segmento   = $itinerario[$k];
                                                        $idsegmento = $segmento['idsegmento'];

                                                        $linea_aerea        = $c_mostrar_paquete->obtener_linea_aerea($segmento['idlineas_aereas']);
                                                        $aeropuerto_sale    = $c_mostrar_paquete->obtener_aeropuerto($segmento['origen']);
                                                        $aeropuerto_llega   = $c_mostrar_paquete->obtener_aeropuerto($segmento['destino']);
                                                    ?>
                                                        <tr>
                                                            <td><?php echo strtoupper($linea_aerea); ?></td>
                                                            <td><?php echo $segmento['nro_vuelo']; ?></td>
                                                            <td><?php echo date('d/m/Y', strtotime($segmento['fecha'])); ?></td>
                                                            <td><?php echo strtoupper($aeropuerto_sale); ?></td>
                                                            <td><?php echo substr($segmento['hora_sale'], 0, 5); ?></td>
                                                            <td><?php echo strtoupper($aeropuerto_llega); ?></td>
                                                            <td><?php echo substr($segmento['hora_llega'], 0, 5); ?></td>
                                                        </tr>
                                                    <?php
                                                    }
                                                ?>
                                            </table>
                                        </div>
                                    <?php
                                    }
                                ?>
                            </td>
                        </tr>
                    <?php
                    }
                ?>

                <!-- MUESTRA HOTEL COMBINADO MAS SERVICIOS MAS AEREO -->
                <?php
                    //$opciones_aereo = $c_mostrar_paquete->obtener_opcion_boletos_x_ciudad($idciudad_origen);
                    if(count($opciones_aereo) > 0)
                    {
                    ?>
                        <tr class="todo_precio hotel_comb_mas_servicio_mas_aereo" style="display:none;" >
                            <td colspan="2" style="padding:10px 0px 0px 30px;">
                                <font style="font-size:13pt; font-weight:bold; color:#ff3300;">PRECIOS POR PERSONA EN DOLARES AMERICANOS</font>
                            <?php
                                for ($g=0; $g < count($opciones_aereo); $g++) 
                                { 
                                    $opcion_aereo   = $opciones_aereo[$g];
                                    $idopaereo      = $opcion_aereo['idopcion_boleto'];
                                ?>
                                    <div class="opc<?php echo $z . '_' . $g; ?> opc_aereo<?php echo $z; ?>" >
                                        <table style="width:70%; margin-left:3%;">
                                            <tr>
                                                <td style="width:5%;">
                                                    <strong>OPCION :</strong>
                                                </td>
                                                <td style="width:30%;">
                                                    <?php echo strtoupper($opcion_aereo['descripcion']); ?>
                                                </td>
                                                <td style="width:5%;">
                                                    <strong>SALE :</strong>
                                                </td>
                                                <td style="width:30%;">
                                                    <?php echo strtoupper($opcion_aereo['salida']); ?>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                        <?php
                                            $boletos = $c_mostrar_paquete->obtener_boletos_x_opcion_aereo($idopaereo);
                                            $total_bol_adt = 0;
                                            $total_bol_cnn = 0;
                                            $total_bol_inf = 0;
                                            for ($j=0; $j < count($boletos); $j++) 
                                            { 
                                                $boleto_cotizado    = $boletos[$j];
                                                $idboleto           = $boleto_cotizado['idboleto_cotizado'];
                                                
                                                $total_adt = $boleto_cotizado['precio_adulto'];
                                                $total_cnn = $boleto_cotizado['precio_menor'];
                                                $total_inf = $boleto_cotizado['precio_infante'];

                                                if($boleto_cotizado['increment_fee'] == 'P')
                                                {
                                                    $fee        = 1+($boleto_cotizado['fee'] / 100);
                                                    $total_adt  *= $fee;
                                                    $total_cnn  *= $fee;
                                                    $total_inf  *= $fee;
                                                }
                                                else
                                                {
                                                    $fee = $boleto_cotizado['fee'];
                                                    if($total_adt > 0)
                                                        $total_adt += $fee;
                                                    else
                                                        $total_adt = 0;
                                                    if($total_cnn > 0)
                                                        $total_cnn += $fee;
                                                    else
                                                        $total_cnn = 0;
                                                    if($total_inf > 0)
                                                        $total_inf += $fee;
                                                    else
                                                        $total_inf = 0;
                                                }

                                                if($boleto_cotizado['increment_factura'] == 'P')
                                                {
                                                    $iva        = 1+($boleto_cotizado['factura'] / 100);
                                                    $total_adt  *= $iva;
                                                    $total_cnn  *= $iva;
                                                    $total_inf  *= $iva;
                                                }
                                                else
                                                {
                                                    $iva = $boleto_cotizado['factura'];
                                                    if($total_adt > 0)
                                                        $total_adt += $iva;
                                                    else
                                                        $total_adt = 0;
                                                    if($total_cnn > 0)
                                                        $total_cnn += $iva;
                                                    else
                                                        $total_cnn = 0;
                                                    if($total_inf > 0)
                                                        $total_inf += $iva;
                                                    else
                                                        $total_inf = 0;
                                                }

                                                if($boleto_cotizado['moneda'] == 'B')
                                                {
                                                    $tc         = $boleto_cotizado['tipo_cambio'];
                                                    $total_adt /= $tc;
                                                    $total_cnn /= $tc;
                                                    $total_inf /= $tc;
                                                }

                                                $total_bol_adt += ceil($total_adt);
                                                $total_bol_cnn += ceil($total_cnn);
                                                $total_bol_inf += ceil($total_inf);
                                            }
                                        ?>
                                        <?php
                                            if(array_key_exists(0, $opciones_de_hoteles))
                                            {
                                            ?>
                                                <div class="clear"></div>
                                                <?php 
                                                    $cant_col = $opciones_de_hoteles['cant_cols'];
                                                    $mostrar_cols = $opciones_de_hoteles['col_mostrar'];
                                                    ?>
                                                        <strong style="margin-left:3%;">HOTELES POR PERSONA EN DOLARES AMERICANOS</strong>
                                                        <table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios">
                                                            <tr style="background-color:#10689b; color:#FFFFFF;">
                                                                <th>HOTEL</th>
                                                                <?php if(strpos($mostrar_cols, 'sgl')!==false) { ?><th>SINGLE</th><?php } ?>
                                                                <?php if(strpos($mostrar_cols, 'dbl')!==false) { ?><th>DOBLE</th><?php } ?>
                                                                <?php if(strpos($mostrar_cols, 'tpl')!==false) { ?><th>TRIPLE</th><?php } ?>
                                                                <?php if(strpos($mostrar_cols, 'cpl')!==false) { ?><th>CUADRUPLE</th><?php } ?>
                                                                <?php if(strpos($mostrar_cols, 'cnn')!==false) { ?><th>MENOR</th><?php } ?>
                                                                <?php if(strpos($mostrar_cols, 'inf')!==false) { ?><th>INFANTE</th><?php } ?>
                                                                <th>ALIMENTACION</th>
                                                                <th>OBS</th>
                                                            </tr>
                                                <?php
                                                    for ($i=0; array_key_exists($i, $opciones_de_hoteles); $i++) 
                                                    { 
                                                        $opcion_hotel       = $opciones_de_hoteles[$i];
                                                        if(array_key_exists('precio_sgl', $opcion_hotel))
                                                            $opcion_hotel['precio_sgl'] += $total_serv_adt + $total_bol_adt;
                                                        if(array_key_exists('precio_dbl', $opcion_hotel))
                                                            $opcion_hotel['precio_dbl'] += $total_serv_adt + $total_bol_adt;
                                                        if(array_key_exists('precio_tpl', $opcion_hotel))
                                                            $opcion_hotel['precio_tpl'] += $total_serv_adt + $total_bol_adt;
                                                        if(array_key_exists('precio_cpl', $opcion_hotel))
                                                            $opcion_hotel['precio_cpl'] += $total_serv_adt + $total_bol_adt;
                                                        if(array_key_exists('precio_cnn', $opcion_hotel))
                                                            $opcion_hotel['precio_cnn'] += $total_serv_cnn + $total_bol_cnn;
                                                        if(array_key_exists('precio_inf', $opcion_hotel))
                                                            $opcion_hotel['precio_inf'] += $total_serv_inf + $total_bol_inf;
                                                    ?>
                                                            <tr>
                                                                <td style="line-height:1.5;">
                                                                    <?php
                                                                        $hoteles = $opcion_hotel['hoteles'];
                                                                        for ($j=0; $j < count($hoteles); $j++) 
                                                                        { 
                                                                            $item_tarifa = $hoteles[$j];
                                                                            echo $item_tarifa['nombre_hotel'] . ' - ' . $item_tarifa['categoria'] . '* (' . $item_tarifa['destino'] . ')<br/>'; 
                                                                        }
                                                                    ?>
                                                                </td>
                                                                <?php if(strpos($mostrar_cols, 'sgl')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_sgl']; ?></td><?php } ?>
                                                                <?php if(strpos($mostrar_cols, 'dbl')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_dbl']; ?></td><?php } ?>
                                                                <?php if(strpos($mostrar_cols, 'tpl')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_tpl']; ?></td><?php } ?>
                                                                <?php if(strpos($mostrar_cols, 'cpl')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_cpl']; ?></td><?php } ?>
                                                                <?php if(strpos($mostrar_cols, 'cnn')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_cnn']; ?></td><?php } ?>
                                                                <?php if(strpos($mostrar_cols, 'inf')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_inf']; ?></td><?php } ?>
                                                                <td style="line-height:1.5;">
                                                                    <?php
                                                                        $hoteles = $opcion_hotel['hoteles'];
                                                                        for ($j=0; $j < count($hoteles); $j++) 
                                                                        { 
                                                                            $item_tarifa = $hoteles[$j];
                                                                            echo strtoupper($item_tarifa['alimentacion']) . '<br/>';
                                                                        }
                                                                    ?>
                                                                </td>
                                                                <td style="line-height:1.5;">
                                                                    <?php
                                                                        $hoteles = $opcion_hotel['hoteles'];
                                                                        for ($j=0; $j < count($hoteles); $j++) 
                                                                        { 
                                                                            $item_tarifa = $hoteles[$j];
                                                                            echo strtoupper($item_tarifa['info_extra']) . '<br/>';
                                                                        }
                                                                    ?>
                                                                </td>
                                                            </tr>                                   
                                                    <?php
                                                    }               
                                                ?>
                                                        </table>
                                            <?php
                                            }
                                        ?>
                                        <br/>
                                        <font style="font-size:10pt; font-weight:bold; margin-left:3%;">ITINERARIO AEREO DETALLADO</font><br/>
                                        <table style="width:90%; font-size:7pt; margin-left:3%;" class="tabla-precios" >
                                            <tr style="background-color:#10689b; color:#FFFFFF;">
                                                <th>LINEA AEREA</th>
                                                <th>VUELO</th>
                                                <th>FECHA</th>
                                                <th>SALE</th>
                                                <th>HRS</th>
                                                <th>LLEGA</th>
                                                <th>HRS</th>
                                            </tr>
                                            <?php
                                                $itinerario = $c_mostrar_paquete->obtener_itinerario_x_opcion_aereo($idopaereo);
                                                for ($k=0; $k < count($itinerario); $k++) 
                                                { 
                                                    $segmento   = $itinerario[$k];
                                                    $idsegmento = $segmento['idsegmento'];

                                                    $linea_aerea        = $c_mostrar_paquete->obtener_linea_aerea($segmento['idlineas_aereas']);
                                                    $aeropuerto_sale    = $c_mostrar_paquete->obtener_aeropuerto($segmento['origen']);
                                                    $aeropuerto_llega   = $c_mostrar_paquete->obtener_aeropuerto($segmento['destino']);
                                                ?>
                                                    <tr>
                                                        <td><?php echo strtoupper($linea_aerea); ?></td>
                                                        <td><?php echo $segmento['nro_vuelo']; ?></td>
                                                        <td><?php echo date('d/m/Y', strtotime($segmento['fecha'])); ?></td>
                                                        <td><?php echo strtoupper($aeropuerto_sale); ?></td>
                                                        <td><?php echo substr($segmento['hora_sale'], 0, 5); ?></td>
                                                        <td><?php echo strtoupper($aeropuerto_llega); ?></td>
                                                        <td><?php echo substr($segmento['hora_llega'], 0, 5); ?></td>
                                                    </tr>
                                                <?php
                                                }
                                            ?>
                                        </table>
                                    </div>
                                <?php
                                }
                            ?>
                            </td>
                        </tr>
                    <?php
                    }
                ?>
                
                <!-- IMPORTANTE -->
                <?php
                    if($ciudad['importante'] != '')
                    {
                    ?>
                        <tr>
                            <td colspan="2" style="padding:10px 0px 0px 30px;">
                                <font style="font-size:13pt; font-weight:bold; color:#ff3300;">IMPORTANTE</font>
                                <ul style="list-style: disc inside; margin:10px 0px 0px 20px; font-size:9pt;">
                                    <?php
                                        $importante = explode("\n", $ciudad['importante']);
                                        for($i=0; $i<count($importante); $i++)
                                        {
                                            if($importante[$i] != "")
                                            {
                                    ?>
                                                <li style="margin: 2px 2px 2px 2px;"><?php echo $importante[$i]; ?></li>
                                    <?php
                                            }
                                        }
                                    ?>      
                                </ul>
                            </td>
                        </tr>
                    <?php
                    }
                ?>
                
            </table>
        <?php
            }
        ?>
    </div>
    <?php
}
?>
</div>