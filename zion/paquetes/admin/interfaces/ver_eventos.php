<?php
	include 'header.php';
	include('../entidad/tipo_evento.php');
	include('../entidad/temporada.php');
	include('../entidad/publicacion.php');
	include('../entidad/evento.php');
	include('../entidad/destino.php');
	include('../entidad/alcance_evento.php');
	include('../entidad/especialidades.php');
	include('../entidad/usuarios_paquetes.php');
	include('../control/c_buscar_evento.php');

	$c_buscar_evento = new c_buscar_evento;
	$array_palabras = $c_buscar_evento->cargar_palabras_clave();

?>
<script type="text/javascript">
	function Mostrar_Ocultar()
    {
        if($("input[name='buscar_por']:checked").val() == 1)
        {
            $("#busqueda_codigo").hide();
            $("#otros").show();
        }    
        else
        {
            $("#busqueda_codigo").show();
            $("#otros").hide();
        }
    }
    $(function(){
        var autocompletar = new Array();
        <?php 
            for($p = 0;$p < count($array_palabras); $p++)
            { 
        ?>
                autocompletar.push('<?php echo strtoupper($array_palabras[$p]); ?>');
        <?php 
            } 
        ?>
         $("#busqueda").autocomplete({ 
           source: autocompletar,
           minLength: 3
        });

    });	
</script>

<div class="eigth columns">
	<h3>BUSCAR EVENTOS</h3>
</div>
<div class="eigth columns">
	<img src="images/event.png" style="height:40px; float:left;" />	
</div>
<form class="sixteen columns" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#F1F1F1; padding-top:10px;" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
	<div class="columns five">
        <input type="checkbox" name="anulados" <?php if(isset($_POST['anulados'])) echo "Checked"; ?>/>Buscar entre los anulados
    </div>
	<div class="sixteen columns">
		<input type="text" id="busqueda" name="busqueda" value="<?php echo $_POST['busqueda']; ?>" style="width:90%; padding:5px;" placeholder="Introduzca Evento, Temporada, Especialidad o Destino" />
	</div>
    <div class="clear"></div>
    
    <div class="fifteen columns">
    	<input type="submit" name="buscar_evento" value="Buscar" style="font-size:15pt; padding: 5px 15px; margin: 10px 20px;" />
    </div>
</form>
<div class="sixteen columns" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#F1F1F1; <?php if(!isset($_POST["buscar_evento"])) { ?>display:none;<?php } ?>" >
	
	<?php
		if(isset($_POST["buscar_evento"]))
		{
			if(isset($_POST['anulados']))
			{
				$estado = '0';
				$estado_descripcion = 'Anulados';
				$color = '#ff3300';
			}
			else
			{
				$estado = '1';
				$estado_descripcion = 'Vigentes';
				$color = '#10689b';
			}
				$eventos = $c_buscar_evento->buscar_eventos_por_palabra_clave($_POST['busqueda'], $estado);
				$usuarios_paquetes = usuarios_paquetes::enviar_datos_usuario($_SESSION["idusuarios"]);
				$autorizado = 1;
	?>
				<div class="columns fifteen" style="color:<?php echo $color; ?>; font-weight:bold;">
					B&Uacute;SQUEDA DE EVENTOS: Se encontraron <?php echo count($eventos); ?> resultados entre los eventos <?php echo $estado_descripcion; ?>.
				</div>
	<?php
				if(count($eventos) > 0)
				{

	?>
					<center><div class="estilo_tabla">
					<table>
						<th></th>
						<th>PAQUETE</th>
						<th>SALIDA</th>
						<th>RETORNO</th>
						<th>CREACION</th>
						<th>CADUCIDAD</th>
						<!--<th>ESTADO</th>-->
						<th>COT</th>
						<th>MAILS</th>
						<th>EST</th>
						<th>ACCION</th>
	<?php
					for ($i=0; $i < count($eventos); $i++) 
					{ 
						$evento = $eventos[$i];
	?>
						<tr id="publicacion_<?php echo $evento['idpublicacion']; ?>" <?php //if($evento['vista_previa'] != 'S') echo 'style="color:#FF3300;"'; ?> >
							<td><strong><?php echo ($i+1); ?></strong></td>
							<td style="line-height:1.3;"><?php echo strtoupper($evento['titulo']); ?></td>
							<td>
								<?php 
									$fecha_salida = strtotime($evento["fecha_salida"]);
									if($fecha_salida)
										$fecha_salida = date('d/M/Y', $fecha_salida);
									else
										$fecha_salida = $evento["fecha_salida"];
									echo $fecha_salida;
								?>
							</td>
							<td>
								<?php 
									$fecha_retorno = strtotime($evento["fecha_retorno"]);
									if($fecha_retorno)
										$fecha_retorno = date('d/M/Y', $fecha_retorno);
									else
										$fecha_retorno = $evento["fecha_retorno"];
									echo $fecha_retorno;
								?>	
							</td>
							<!--<td>
								<?php
									if($evento['armado'] == 'S')
										echo "ARM";
									else if($evento['armado'] == 'N')
										echo "A SOL";
									else
										echo "S/MOV";
								?>
							</td>-->
							<td><?php echo strtoupper(date('d/M/Y', strtotime($evento['fecha_publicacion'] . ' 00:00:00')) . '(' . substr($evento['nombre_usuario'], 0, 5)) . ')'; ?></td>
							<td><?php echo strtoupper(date('d/M/Y', strtotime($evento['fecha_caducidad'] . ' 00:00:00'))); ?></td>
							<td>
								<a href="mostrar_cotizaciones_por_generico.php?p=<?php echo $evento['idpublicacion']; ?>" target="_blank">
									<?php
										echo $c_buscar_evento->obtener_cantidad_de_cotizaciones($evento['idpublicacion']);
									?>
								</a>
							</td>
							<td>
								<a onclick="Mostrar_Envios_Realizados(<?php echo $evento['idpublicacion']; ?>);" style="cursor:pointer;">
								<?php
									echo $c_buscar_evento->obtener_cantidad_de_envios_masivos($evento['idpublicacion']);
								?>
								</a>
							</td>
							<td>
								<?php
									if($evento['estado_paquete'] == 0)
									{
									?>
										<font style="color:#FF0000;">A SOL</font>
									<?php
									}
									else if($evento['estado_paquete'] == 1)
									{
									?>
										ARM
									<?php
									}
								?>
							</td>
							<td>
								<a href="visualizar_evento.php?idevento=<?php echo $evento['idpublicacion']; ?>"><img src="images/icon-view.png" class="icon" title="Visualizar" /></a>
		<?php 
							if($autorizado == 1)
							{
		?>
								<a href="editar_evento.php?idevento=<?php echo $evento['idpublicacion']; ?>"><img src="images/pencil.png" class="icon" title="Editar" /></a>
								<img src="images/cross.png" class="icon" title="Anular" onclick="anular_publicacion(<?php echo $evento['idpublicacion']; ?>);" />
		<?php
							}
		?>
							</td>
						</tr>
	<?php
					}
	?>
					</table>
					</div></center>
					<span style="color:#FF3300; font-weight:bold; font-size:7pt; margin-left:20px;"><font style="font-size:5em;">&#46;</font>&nbsp;SIN PUBLICAR</span>
	<?php
				}		
	?>
					
	<?php
		}
	?>
</div>
<?php include("footer.php"); ?>