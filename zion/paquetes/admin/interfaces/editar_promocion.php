<?php
	include 'header.php';
	include('../entidad/publicacion.php');
	include('../entidad/paquete.php');
	include('../entidad/imagen.php');
	include('../entidad/archivo_adjunto.php');
	include('../entidad/operadora.php');
	include('../entidad/usuarios_paquetes.php');

	$usuarios_paquetes = usuarios_paquetes::enviar_datos_usuario($_SESSION["idusuarios"]);
	if(count($usuarios_paquetes) > 0)
	{
		if($usuarios_paquetes[0]["acceso"] != 'E')
		{
			echo "USTED NO ESTA AUTORIZADO PARA REALIZAR ESTA OPERACION";
			exit(0);
		}
	}

	$idpublicacion = $_GET['idpromo'];

	$paquete = publicacion::obtener_info_paquete($idpublicacion);
	$paquete = $paquete[0];

?>

<style type="text/css">
	.is_real1 
	{
		display: none !important; 
	}
</style>

<input type="hidden" value="<?php echo $idpublicacion; ?>" id="id_publicacion" />
<input type="hidden" value="<?php echo $idpublicacion; ?>" id="publicacion" />
<input type="hidden" value="pqt" id="type_publicacion" />
<input type="hidden" value="G" id="type" />

<div class="eight columns">
	<h3>EDITAR PROMOCION</h3>
</div>
<div class="five columns">
	<img src="images/travel.png" style="height:40px; float:left; display:inline;" />	
</div>
<div class="clear"></div>
<div class="columns eight" style="margin-left:8px; border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1; padding-top:10px; padding-bottom:10px;">
	<h4>Datos Generales</h4>
	<div class="columns two" style="font-weight:bold; color:#10689b;">
		TITULO
	</div>
	<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
	<div class="columns four" onmouseover="revelar_icono_edicion('titulo');" onmouseout="ocultar_icono_edicion('titulo');">
		<span id="titulo">
			<span id="datos_titulo"><?php echo $paquete["titulo"]; ?></span>
			<img id="icon_editar_titulo" src="images/pencil.png" onclick="mostrar_edicion('titulo');" class="icon_edit">
		</span>
		<span id="editar_titulo" style="display:none;">
			<input type="text" name="titulo" id="valor_titulo" value="<?php echo $paquete["titulo"]; ?>" />
			<img src="images/cross.png" onclick="ocultar_edicion('titulo');" class="icon_close">
			<img src="images/disk.png" onclick="guardar_atributo('pqt_publicacion', 'titulo', 'idpublicacion', '<?php echo $idpublicacion; ?>');" class="icon_save">
		</span>
	</div>

	<div class="clear_mayor"></div>

	<div class="columns two" style="font-weight:bold; color:#10689b;">
		VALIDO HASTA
	</div>
	<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
	<div class="columns four" onmouseover="revelar_icono_edicion('fecha_caducidad');" onmouseout="ocultar_icono_edicion('fecha_caducidad');">
		<span id="fecha_caducidad">
			<span id="datos_fecha_caducidad">&nbsp;
				<?php 
					$fecha_caducidad = date('d/m/Y',strtotime($paquete["fecha_caducidad"]));
					echo $fecha_caducidad;
				?>	
			</span>
			<img id="icon_editar_fecha_caducidad" src="images/pencil.png" onclick="mostrar_edicion('fecha_caducidad');" class="icon_edit">
		</span>
		<span id="editar_fecha_caducidad" style="display:none;">
			<input type="text" class="time_limit" name="fecha_caducidad" id="valor_fecha_caducidad" value="<?php echo $fecha_caducidad; ?>" style="width:150px;" />
			<img src="images/cross.png" onclick="ocultar_edicion('fecha_caducidad');" class="icon_close">
			<img src="images/disk.png" onclick="guardar_atributo('pqt_publicacion', 'fecha_caducidad', 'idpublicacion', '<?php echo $idpublicacion; ?>');" class="icon_save">
		</span>
	</div>

	<div class="clear_mayor"></div>

	<div class="columns two" style="font-weight:bold; color:#10689b;">
		OPERADORA
	</div>
	<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
	<div class="columns four"  onmouseover="revelar_icono_edicion_multiple('operadora');" onmouseout="ocultar_icono_edicion_multiple('operadora');">
		<div class="columns three" style="margin-left:0px; margin-right:10px;" id="contenedor_operadora">
			<?php 
				$operadoras = publicacion::obtener_operadoras_por_publicacion($idpublicacion);
				if(count($operadoras) == 0)
					echo "- No hay ninguno -";
				for($i = 0; $i < count($operadoras); $i++)
				{
					$nombre 		= $operadoras[$i]["nombre_operadora"];
					$idoperadora 	= $operadoras[$i]["idoperadora"];
			?>
				<div id="operadora_<?php echo $idoperadora; ?>">
					<?php echo $nombre; ?>
					<img src="images/delete.png" class="delete_operadora icon_delete" onclick="quitar_asociacion('pqt_publicacion_has_operadora', 'operadora', 'idpublicacion', '<?php echo $idpublicacion; ?>', 'idoperadora', '<?php echo $idoperadora; ?>');" >
				</div>
			<?php
				}
			?>
		</div>
		<div class="columns one" style="margin-right:0px;" style="margin-left:0px; margin-right:10px;">
			<img id="icon_editar_operadora" src="images/pencil.png" onclick="mostrar_edicion_multiple('operadora');" class="icon_edit">			
		</div>
		<div class="columns four" style="float:left; display:none; margin-left:0px;" id="editar_operadora">
			<select id="valor_operadora" style="width:150px;">
				<?php
					$operadoras = operadora::obtener_datos_operadora();
					for($i = 0; $i < count($operadoras); $i++)
					{
				?>
				<option value="<?php echo $operadoras[$i]['idoperadora']; ?>">
					<?php echo $operadoras[$i]['nombre_operadora']; ?>
				</option>
				<?php
					}
				?>
			</select>
			<img src="images/cross.png" onclick="ocultar_edicion_multiple('operadora');" class="icon_close">
			<img src="images/disk.png" onclick="guardar_asociacion('pqt_publicacion_has_operadora', 'operadora', 'idpublicacion', '<?php echo $idpublicacion; ?>', 'idoperadora');" class="icon_save">
		</div>
	</div>
	
	<div class="clear_mayor"></div>

	<div class="columns two" style="font-weight:bold; color:#10689b;">
		DESDE USD
	</div>
	<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
	<div class="columns four" onmouseover="revelar_icono_edicion('precio_desde');" onmouseout="ocultar_icono_edicion('precio_desde');">
		<span id="precio_desde">
			<span id="datos_precio_desde"><?php echo $paquete["precio_desde"]; ?></span>
			<img id="icon_editar_precio_desde" src="images/pencil.png" onclick="mostrar_edicion('precio_desde');" class="icon_edit">
		</span>
		<span id="editar_precio_desde" style="display:none;">
			<input type="text" name="precio_desde" id="valor_precio_desde" value="<?php echo $paquete["precio_desde"]; ?>" />
			<img src="images/cross.png" onclick="ocultar_edicion('precio_desde');" class="icon_close">
			<img src="images/disk.png" onclick="guardar_atributo('pqt_publicacion', 'precio_desde', 'idpublicacion', '<?php echo $idpublicacion; ?>');" class="icon_save">
		</span>
	</div>

	<div class="clear_mayor"></div>

	<div class="columns two" style="font-weight:bold; color:#10689b;">
		SALIDA
	</div>
	<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
	<div class="columns four" onmouseover="revelar_icono_edicion('fecha_salida');" onmouseout="ocultar_icono_edicion('fecha_salida');">
		<span id="fecha_salida">
			<span id="datos_fecha_salida">&nbsp;
				<?php 
					$fecha_salida = strtotime($paquete["fecha_salida"]);
					if($fecha_salida)
						$fecha_salida = date('d/m/Y', $fecha_salida);
					else
						$fecha_salida = $paquete["fecha_salida"];
					echo $fecha_salida;
				?>	
			</span>
			<img id="icon_editar_fecha_salida" src="images/pencil.png" onclick="mostrar_edicion('fecha_salida');" class="icon_edit">
		</span>
		<span id="editar_fecha_salida" style="display:none;">
			<input type="text" class="time_limit" name="fecha_salida" id="valor_fecha_salida" value="<?php echo $fecha_salida; ?>"  style="width:150px;" />
			<img src="images/cross.png" onclick="ocultar_edicion('fecha_salida');" class="icon_close">
			<img src="images/disk.png" onclick="guardar_atributo('pqt_publicacion', 'fecha_salida', 'idpublicacion', '<?php echo $idpublicacion; ?>');" class="icon_save">
		</span>
	</div>

	<div class="clear_mayor"></div>

	<div class="columns two" style="font-weight:bold; color:#10689b;">
		RETORNO
	</div>
	<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
	<div class="columns four" onmouseover="revelar_icono_edicion('fecha_retorno');" onmouseout="ocultar_icono_edicion('fecha_retorno');">
		<span id="fecha_retorno">
			<span id="datos_fecha_retorno">&nbsp;
				<?php 
					$fecha_retorno = strtotime($paquete["fecha_retorno"]);
					if($fecha_retorno)
						$fecha_retorno = date('d/m/Y', $fecha_retorno);
					else
						$fecha_retorno = $paquete["fecha_retorno"];
					echo $fecha_retorno;
				?>	
			</span>
			<img id="icon_editar_fecha_retorno" src="images/pencil.png" onclick="mostrar_edicion('fecha_retorno');" class="icon_edit">
		</span>
		<span id="editar_fecha_retorno" style="display:none;">
			<input type="text" class="time_limit" name="fecha_retorno" id="valor_fecha_retorno" value="<?php echo $fecha_retorno; ?>" style="width:150px;" />
			<img src="images/cross.png" onclick="ocultar_edicion('fecha_retorno');" class="icon_close">
			<img src="images/disk.png" onclick="guardar_atributo('pqt_publicacion', 'fecha_retorno', 'idpublicacion', '<?php echo $idpublicacion; ?>');" class="icon_save">
		</span>
	</div>

	<div class="clear_mayor"></div>
	<div class="clear_mayor"></div>

	<div class="columns seven" style="font-weight:bold; color:#10689b;">
		DESCRIPCION DE LA PROMOCION :
	</div>
	<div class="columns seven" onmouseover="revelar_icono_edicion('descr_publicacion');" onmouseout="ocultar_icono_edicion('descr_publicacion');">
		<span id="descr_publicacion">
			<span id="datos_descr_publicacion">
				<ul class="lista_descripcion" id="lista_descr_publicacion">
					<?php
						$descr_publicacion = explode("\n", $paquete['descr_publicacion']);
						for($i=0; $i<count($descr_publicacion); $i++)
						{
							if($descr_publicacion[$i] != "")
							{
					?>
								<li>
									<a><?php echo $descr_publicacion[$i]; ?></a>
								</li>
					<?php
							}
						}
					?>		
				</ul>
			</span>
			<img id="icon_editar_descr_publicacion" src="images/pencil.png" onclick="mostrar_edicion('descr_publicacion');" class="icon_edit">
		</span>
		<span id="editar_descr_publicacion" style="display:none;">
			<textarea name="descr_publicacion" id="valor_descr_publicacion" style="width:350px;" ><?php echo $paquete['descr_publicacion']; ?></textarea>
			<img src="images/cross.png" onclick="ocultar_edicion('descr_publicacion');" class="icon_close">
			<img src="images/disk.png" onclick="guardar_atributo('pqt_publicacion', 'descr_publicacion', 'idpublicacion', '<?php echo $idpublicacion; ?>');" class="icon_save">
		</span>
	</div>

</div>

<div class="columns eight" style="margin-left:8px; border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1; padding-top:10px; padding-bottom:10px;">
	
	<h4>Im&aacute;genes</h4>

	<div class="columns seven" >
		<div class="columns four" style="font-weight:bold; color:#10689b;">
			IMAGEN PREVIEW :
		</div>
		<div class="columns two" style="font-size:10px; color:#aaa; line-height: 1em;">(como vista previa) 356 X 191;</div>
		<div id="imagen_primera_cargados" class="columns six" style="margin-left:50px;">
			<?php 
				if($paquete["imagen_primera"] != "") 
				{
				?>
					<img src='<?php echo $ruta_imagen . $paquete["imagen_primera"]; ?>' style='width:50px; height: 50px;' />
		            <strong><?php echo $paquete["imagen_primera"]; ?></strong>
				<?php
				}
				else
				{
					echo "No se a&ntilde;adio ninguna imagen.";
				}
			?>
		</div>
		<div class="six columns" style="margin-left:50px; display:none;" id="imagen_primera_loading">
			<img src="images/cargando.gif" />
		</div>
		<div class="columns two" style="margin-left:50px;">
	  		<input id="archivo_imagen_primera" type="file" name="archivos[]" onchange="seleccionar_y_guardar_imagen('imagen_primera','<?php echo $idpublicacion; ?>');" />
		</div>
	</div>

	<div class="clear_mayor"></div>
	<div class="clear_mayor"></div>

	<div class="columns seven">
		<div class="columns four" style="font-weight:bold; color:#10689b;">
			PUBLICAR PAQUETE:
		</div>
		<div class="columns two" style="font-size:7pt;">
			<input type="checkbox" id="publicado" value="P" <?php if($paquete['publicado'] == 1) echo 'Checked'; ?> />
			<input type="hidden" id="valor_publicado" />
			<input type="button" value="Guardar" style="float:right;" onclick="GuardarPublicado(<?php echo $idpublicacion; ?>);" />
		</div>
	</div>

	<div class="columns seven">
		<div class="columns four" style="font-weight:bold; color:#10689b;">
			RESALTAR PAQUETE:
		</div>
		<div class="columns two" style="font-size:7pt;">
			<input type="checkbox" id="resaltar" value="P" <?php if($paquete['resaltar'] == 1) echo 'Checked'; ?> />
			<input type="hidden" id="valor_resaltar" />
			<input type="button" value="Guardar" style="float:right;" onclick="GuardarResaltado(<?php echo $idpublicacion; ?>);" />
		</div>
	</div>

	<div class="columns seven">
		<div class="columns two" style="font-weight:bold; color:#10689b;">
			ESTADO:
		</div>
		<div class="columns three" style="font-size:7pt;">
			<select name="estado_paquete" id="valor_estado_paquete" >
				<option value="0" <?php if($paquete['estado_paquete'] == 0) echo 'selected'; ?> >A Solicitud</option>
				<option value="1" <?php if($paquete['estado_paquete'] == 1) echo 'selected'; ?> >Armado</option>
			</select>
		</div>
		<div class="columns one">
			<input type="button" value="Guardar" style="float:right;" onclick="guardar_atributo('pqt_publicacion', 'estado_paquete', 'idpublicacion', '<?php echo $idpublicacion; ?>');" />
		</div>
	</div>

	<div class="clear_mayor"></div>

	<div class="columns seven">
		<div class="columns five" style="font-weight:bold; color:#10689b;">
			ARCHIVOS ADJUNTOS:
		</div>
		<div class="columns six" style="font-size:7pt;" id="archivos_cargados">
			<?php 
				$archivos = publicacion::obtener_archivos_por_publicacion($idpublicacion);
				if(count($archivos) > 0) 
				{
					for($i=0; $i<count($archivos); $i++)
					{
						$archivo = $archivos[$i];
						if($archivo['publicar'] == 1)
					        $pub = 'Pub';
					    else
					        $pub = 'Int';
			?>
					<div class="columns six" id='archivo_adjunto_<?php echo $archivo['idarchivo_adjunto']; ?>' >
						<strong><?php echo $archivo['nombre']; ?> - </strong>
						<?php echo $archivo["descripcion"]; ?> - <?php echo $pub; ?>
                        <img src='images/delete.png' class='icon_delete_image' onclick='eliminar_archivo_multiple(<?php echo $archivo['idarchivo_adjunto']; ?>, <?php echo $idpublicacion; ?>);' style="position:relative;">
                    </div>
			<?php
					}
				}
				else
				{
					echo "No se a&ntilde;adio ning&uacute;n archivo.";
				}
			?>
		</div>
		<div class="six columns" style="display:none;" id="adjunto_loading">
			<img src="images/cargando.gif" />
		</div>
		<div class="columns two" style="margin-left:50px;">
	  		<input id="archivo_adjunto" type="file" name="archivos[]" onchange="seleccionar_y_guardar_archivo_multiple('<?php echo $idpublicacion; ?>');" />
	  		<textarea id="descripcion_archivo" rows="2" style="min-height:20px; min-width:300px; margin:0px;" placeholder="Descripcion del Contenido"></textarea>
	  		<label style="font-weight:normal; margin:0px; font-size:8pt;">
	  			<input type="checkbox" id="archivo_publicar" />Publicar
	  		</label>
		</div>
	</div>	

</div>
	
<div class="clear_mayor"></div>

<h4>Datos Agente</h4>
<div class="columns sixteen" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1; padding-top:10px; padding-bottom:10px;">
	<div class="columns fifteen" onmouseover="revelar_icono_edicion('datos_agente');" onmouseout="ocultar_icono_edicion('datos_agente');">
		<span id="datos_agente">
			<span id="datos_datos_agente">
				<ul class="lista_descripcion" id="lista_datos_agente">
					<?php
						$datos_agente = explode("\n", $paquete['datos_agente']);
						for($i=0; $i<count($datos_agente); $i++)
						{
							if($datos_agente[$i] != "")
							{
					?>
								<li>
									<a><?php echo $datos_agente[$i]; ?></a>
								</li>
					<?php
							}
						}
					?>		
				</ul>
			</span>
			<img id="icon_editar_datos_agente" src="images/pencil.png" onclick="mostrar_edicion('datos_agente');" class="icon_edit">
		</span>
		<span id="editar_datos_agente" style="display:none;">
			<textarea name="datos_agente" id="valor_datos_agente" style="width:500px;"><?php echo $paquete["datos_agente"]; ?></textarea>
			<img src="images/cross.png" onclick="ocultar_edicion('datos_agente');" class="icon_close">
			<img src="images/disk.png" onclick="guardar_atributo('pqt_publicacion', 'datos_agente', 'idpublicacion', '<?php echo $idpublicacion; ?>');" class="icon_save">
		</span>
	</div>
</div>

<div class="clear_mayor"></div>

<div class="sixteen columns">
	<a href="visualizar_promocion.php?idpromo=<?php echo $idpublicacion; ?>">
		<input type="button" value="Ir a Vista de la Promocion" />
	</a>
</div>

<?php include('footer.php'); ?>

<script type="text/javascript">
	$(function() {

		$('.vista_pagina').html('EDICION DE PAQUETE GENERICO');

	$.datepicker.regional['es'] =
  	{
	  	closeText: 'Cerrar',
	  	prevText: 'Previo',
	  	nextText: 'Próximo',
	  	monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
	  	monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
	  	monthStatus: 'Ver otro mes', yearStatus: 'Ver otro año',
	  	dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
	  	dayNamesShort: ['Dom','Lun','Mar','Mie','Jue','Vie','Sáb'],
	  	dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sa'],
	  	dateFormat: 'dd/mm/yy', firstDay: 0,
	  	initStatus: 'Selecciona la fecha', isRTL: false
	 };
	 	$.datepicker.setDefaults($.datepicker.regional['es']);

  	crear_calendario_cumpleanios('valor_fecha_caducidad');
  	crear_calendario_cumpleanios('valor_fecha_salida');
  	crear_calendario_cumpleanios('valor_fecha_retorno');
  	crear_calendario_time_limit();

	});

	function crear_calendario_cumpleanios(id) {
	    anioActual = new Date().getFullYear();
	    anioInicio = anioActual - 90;
	    $("#" + id).datepicker({
	        showOn: "button",
	        buttonImage: "../images/calendar.jpg",
	        buttonImageOnly: true,
	        numberOfMonths: 3,
	        yearRange: anioInicio + ":2025",
	        constrainInput: false
	    });  
	    $("#" + id).datepicker("option", "dateFormat", 'dd/mm/yy');
	}
</script>