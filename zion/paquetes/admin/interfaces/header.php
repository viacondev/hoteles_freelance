<?php
session_start();
ini_set('display_errors', 0);

$ruta_imagen    = "../images_paquetes/";
$ruta_adjuntos  = "../adjuntos_paquetes/";

include '../control/c_login.php';
include '../entidad/usuarios.php';
include '../BD/controladoraBD.php';

c_login::verificar_session();

 
$developer =  isset($_GET['developer'])?$_GET["developer"]:0;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
    <title>BarryBolivia</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <!-- JQuery -->
    <script type="text/javascript" src="lib/jquery-ui/js/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="lib/jquery-ui/js/jquery-ui-1.10.1.custom.js"></script>
    <link href="lib/jquery-ui/css/flick/jquery-ui-1.10.1.custom.css" rel="stylesheet">
   

    <!-- Skeleton -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <link rel="stylesheet" href="lib/Skeleton/stylesheets/base.css">
    <link rel="stylesheet" href="lib/Skeleton/stylesheets/skeleton.css">
    <link rel="stylesheet" href="lib/Skeleton/stylesheets/layout.css">

    <!--PrintThisMaster-->
    <script type="text/javascript"  src="lib/printThis-master/printThis.js"></script>

    <!-- Personal files -->
    <link href="styles.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="js/actions.js"></script>
    
    <!--TinyBox-->
    <script type="text/javascript" src="lib/tinybox2/tinybox.js"></script>
    
</head>
<body>
<div align="left" class="div_sistema"><br /><br /></div>
<div style="position:fixed; background-color:#009900; color:#fff; display:none; padding:10px; margin-top:0px; margin-left:20%; text-align:center; width:60%; z-index:100; opacity:0.9;" id="mensaje_correcto"> 
</div>
<div style="position:fixed; background-color:#ff0000; color:#fff; display:none; padding:10px; margin-top:0px; margin-left:20%; text-align:center; width:60%; z-index:100; opacity:0.9;" id="mensaje_error">    
</div>
<?php
    if(strpos($_SERVER['PHP_SELF'], 'visualizar_paquete.php') !== false || strpos($_SERVER['PHP_SELF'], 'visualizar_evento.php') !== false || strpos($_SERVER['PHP_SELF'], 'ver_cotizacion_paquete.php') !== false)
    {
    ?>
        <?php 
        //if ($developer> 0) 
        //{
        ?>
        <div class="vista_paquete">
            <input type="radio" name="vista_precios" value="1" onchange="validar_vista_tarifas_paquete();" />Separar Todo<br/>
            <input type="radio" name="vista_precios" value="2" onchange="validar_vista_tarifas_paquete();" />Hotel + Servicios<br/>
            <input type="radio" name="vista_precios" value="3" checked onchange="validar_vista_tarifas_paquete();" />Hotel + Serv. + Aereo<br/>
            <input type="radio" name="vista_precios" value="4" onchange="validar_vista_tarifas_paquete();" />Hotel Comb. + Serv.<br/>
            <input type="radio" name="vista_precios" value="5" onchange="validar_vista_tarifas_paquete();" />Hotel Comb. + Serv. + Aereo<br/>
        </div>
        <div class="vista_opciones_aereo"></div>
        <?php 
        //}
        ?>
        
    <?php
    }
    if(strpos($_SERVER['PHP_SELF'], 'editar_cotizacion_paquete.php') !== false || strpos($_SERVER['PHP_SELF'], 'ver_cotizacion_paquete.php') !== false)
    {
    ?>
        <div class="vista_de_precios">
            <input type="radio" name="vista_cotizacion_edicion" value="1" <?php if($_GET['vista'] == 0) { ?>checked <?php } ?> onchange="validar_vista_cotizacion();" />Cotizacion<br/>
            <input type="radio" name="vista_cotizacion_edicion" value="2" <?php if($_GET['vista'] == 1 || !isset($_GET['vista'])) { ?>checked <?php } ?> onchange="validar_vista_cotizacion();" />Reservas Reales<br/>
        </div>
        <?php
            if($_GET['vista'] == 1 || !isset($_GET['vista']))
            {
            ?>
                <script type="text/javascript">
                    $(function() { validar_vista_cotizacion(); });
                </script>
            <?php
            }
        ?>
    <?php
    } 
    if(strpos($_SERVER['PHP_SELF'], 'ver_detalle_pasajeros_de_paquete.php') !== false)
    {
    ?>
        <div class="vista_de_precios vista_detalle_pax">
            <input type="checkbox" onchange="$('.encabezado').toggle();" />Encabezado
            <hr class="min-spc" />
            <input type="checkbox" onchange="$('.view_cotizacion').toggle();" />Vista Impresion
            <hr class="min-spc" />
            <input type="checkbox" onchange="$('.is_real').toggle();" checked />Ver Reservas
            <hr class="min-spc" />
            <input type="checkbox" checked onchange="$('.costos_resumido').toggle();" />RESUMEN DE COSTOS<br/>
            <input type="checkbox" checked onchange="$('.descripcion_servicios_por_pax').toggle();" />DESCRIPCION X PAX<br/>
            <input type="checkbox" checked onchange="$('.vista_hoteles').toggle();" />HOTEL
            <div style="padding-left:10px; line-height:1;">
                <input type="checkbox" onchange="$('.col_sgl').toggle();" />Simple<br/>
                <input type="checkbox" onchange="$('.col_dbl').toggle();" />Doble<br/>
                <input type="checkbox" onchange="$('.col_tpl').toggle();" />Triple<br/>
                <input type="checkbox" onchange="$('.col_cpl').toggle();" />Cuadruple<br/>
                <input type="checkbox" onchange="$('.col_cnn').toggle();" />Menor<br/>
                <input type="checkbox" onchange="$('.col_inf').toggle();" />Infante<br/>
                <input type="checkbox" checked onchange="$('.col_paxs').toggle();" />Pasajeros<br/>
            </div>
            <input type="checkbox" checked onchange="$('.inscripciones_a_evento').toggle();" />INSCRIPCIONES<br/>
            <div style="padding-left:10px; line-height:1;">
                <input type="checkbox" checked onchange="$('.inscripciones_pax').toggle();" />Pasajeros<br/>
                <input type="checkbox" onchange="$('.inscripcion_imagen').toggle();" />Tabla<br/>
            </div>
            <input type="checkbox" checked onchange="$('.vista_aereo').toggle();" />AEREO
        </div>
    <?php
    } 
?>

<div id="wrapper" class="container">
    <div id="header">
        <div id="logo" class="one columns">
            <img src="../../../assets/images/logo.png" style="" />    
        </div>
        <div id="logo" class="five columns">
            <h2>BarryBolivia</h2>    
        </div>
        <div id="login_box" class="ten columns">
            <?php echo $_SESSION['nombre_usuario_sesion']; ?> (<a href="logout.php" />Salir</a>)
        </div>
        <div class="clear"></div>
    </div>
    <br />
    <div id="menu" class="columns sixteen">
        <div id="menu_items">
            <ul>
                <li><a href="index.php" id="menu_inicio">Inicio</a></li>
                <li>
                    <a href="ver_paquetes.php" id="menu_clientes">Paquetes</a>
                    <ul class="submenu">
                        <li><a href="ver_paquetes.php">Buscar</a></li>
                        <li><a href="insertar_paquete.php">Insertar</a></li>
                        <?php 
                        if ($developer > 0) 
                        {
                        ?>
                        <li><a href="segmentar_paquetes.php?i=1">Envio Masivo</a></li>
                        <?php 
                        }
                        ?>
                    </ul>
                </li>
                <?php 
                if ($developer> 0) 
                {
                ?>
                <li>
                    <a href="#" id="menu_configuracion">Promociones</a>
                    <ul class="submenu">
                        <li><a href="ver_promociones.php">Buscar</a></li>
                        <li><a href="insertar_promocion.php">Insertar</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" id="menu_eventos">Eventos</a>
                    <ul class="submenu">
                        <li><a href="ver_eventos.php">Buscar</a></li>
                        <li><a href="insertar_evento.php">Insertar</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" id="menu_eventos">Cotizaciones</a>
                    <ul class="submenu">
                        <li><a href="insertar_cotizacion.php">Nueva Cotizacion</a></li>
                        <li><a href="buscar_cotizacion.php">Buscar Cotizacion</a></li>
                    </ul>
                </li>
                <?php 
                }
                ?>
                
                
                
                <li>
                    <a href="registro_administracion.php" id="menu_configuracion">Administraci&oacute;n</a>
                </li>
            </ul>
        </div>
    </div>