<?php
	include 'header.php';
	include('../entidad/tipo_paquete.php');
	include('../entidad/temporada.php');
	include('../entidad/operadora.php');
	include('../entidad/destino.php');
	include('../entidad/hotel.php');
	include('../entidad/tipo_evento.php');
	include('../entidad/alcance_evento.php');
	include('../entidad/pqt_otro_servicio.php');
	include('../entidad/usuarios_paquetes.php');

	$usuarios_paquetes = usuarios_paquetes::enviar_datos_usuario($_SESSION["idusuarios"]);
	if(count($usuarios_paquetes) > 0)
	{
		if($usuarios_paquetes[0]["acceso"] != 'E')
		{
			echo "USTED NO ESTA AUTORIZADO PARA ACCEDER A ESTA SECCION";
			exit(0);
		}
	}

	if(isset($_POST['Registrar_Destino']))
	{
		$nombre_destino = $_POST["nombre_destino"];
		$registro_destino = destino::registrar_destino($nombre_destino);
	}
	if(isset($_POST['Registrar_Temporada']))
	{
		$nombre = $_POST["nombre_temporada"];
		$registro_temporada = temporada::registrar_temporada($nombre);
	}
	if(isset($_POST['Registrar_Hotel']))
	{
		$codigo_hotel 	= $_POST["codigo_hotel"];
		$nombre_hotel 	= $_POST["nombre_hotel"];
		$categoria 		= $_POST["categoria_hotel"];
		$registro_hotel = hotel::registrar_nuevo_hotel($nombre_hotel, $categoria);
	}
	if(isset($_POST['Registrar_Tipo_Paquete']))
	{
		$nombre_tipo = $_POST["nombre_tipo_paquete"];
		$imagen      = $_POST["imagen_tipo_paquete"];
		$registro_tipo_paquete = tipo_paquete::registrar_tipo_paquete($nombre_tipo, $imagen);
	}
	if(isset($_POST['Registrar_Operadora']))
	{
		$nombre_operadora 	= $_POST["nombre_operadora"];
		$direccion 			= $_POST["correo_operadora"];
		$telefonos 			= $_POST["telefonos_operadora"];
		$observaciones 		= $_POST["observaciones_operadora"];
		$registro_operadora = operadora::registrar_operadora($nombre_operadora, $direccion, $telefonos, $observaciones);
	}
	if(isset($_POST['Registrar_Tipo_Evento']))
	{
		$nombre = $_POST["nombre_tipo_evento"];
		$registro_tipo_evento = tipo_evento::registrar_tipo_evento($nombre);
	}
	if(isset($_POST['Registrar_Alcance_Evento']))
	{
		$nombre = $_POST["nombre_alcance_evento"];
		$registro_alcance_evento = alcance_evento::registrar_alcance_evento($nombre);
	}
	if(isset($_POST['Registrar_Otro_Servicio']))
	{
		$nombre_servicio = $_POST["nombre_otro_servicio"];
		$registro_otro_servicio = pqt_otro_servicio::registrar_otro_servicio($nombre_servicio);
	}

?>
<div class="columns sixteen" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
	<div class="clear_mayor"></div>
	<div class="columns two">
		<input type="radio" name="panel_admin" value="destino" onchange="mostrar_panel();" <?php if(isset($_POST['Registrar_Destino']) || count($_POST) <= 0) { echo "checked"; } ?> />DESTINOS
	</div>
	<div class="columns three">
		<input type="radio" name="panel_admin" value="temporada" onchange="mostrar_panel();" <?php if(isset($_POST['Registrar_Temporada'])) { echo "checked"; } ?> />TEMPORADAS
	</div>
	<div class="columns three">
		<input type="radio" name="panel_admin" value="hotel" onchange="mostrar_panel();" <?php if(isset($_POST['Registrar_Hotel'])) { echo "checked"; } ?> />HOTELES
	</div>
	<div class="columns two">
		<input type="radio" name="panel_admin" value="tipo_paquete" onchange="mostrar_panel();" <?php if(isset($_POST['Registrar_Tipo_Paquete'])) { echo "checked"; } ?> />TIPOS DE PAQUETE
	</div>
	<div class="columns three">
		<input type="radio" name="panel_admin" value="operadora" onchange="mostrar_panel();" <?php if(isset($_POST['Registrar_Operadora'])) { echo "checked"; } ?> />OPERADORAS
	</div>
	<div class="columns two">
		<input type="radio" name="panel_admin" value="tipo_evento" onchange="mostrar_panel();" <?php if(isset($_POST['Registrar_Tipo_Evento'])) { echo "checked"; } ?> />TIPO DE EVENTO
	</div>
	<div class="columns two">
		<input type="radio" name="panel_admin" value="alcance_evento" onchange="mostrar_panel();" <?php if(isset($_POST['Registrar_Alcance_Evento'])) { echo "checked"; } ?> />ALCANCE DE EVENTO
	</div>
	<div class="columns two">
		<input type="radio" name="panel_admin" value="otro_servicio" onchange="mostrar_panel();" <?php if(isset($_POST['Registrar_Otro_Servicio'])) { echo "checked"; } ?> />OTROS SERVICIOS
	</div>

	<div class="clear_mayor"></div>
</div>

<div class="clear_mayor"></div>

<div class="columns sixteen admin" id="destino" <?php if(!isset($_POST['Registrar_Destino']) && count($_POST) > 0) { echo "style='display:none;'"; } ?> >
	<div class="columns seven" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
		<center><h4>Registro de Destinos</h4></center>
		<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" class="miform" >
			<div class="columns two" style="font-weight:bold; color:#10689b;">
				DESTINO
			</div>
			<div class="columns four">
				<input type="text" name="nombre_destino" />
			</div>
			<div class="columns six" style="float:right;">
				<input type="submit" name="Registrar_Destino" value="Registrar" style="float:right" />
			</div>
		</form>
	</div>
	<div class="columns eight" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
		<center><h4>Destinos Registrados</h4></center>
			<div class="columns seven" style="background-color:#10689b; ">
				<div class="columns one" style="color:#fff;">&nbsp;</div>
				<div class="columns four" style="color:#fff;">NOMBRE DESTINO</div>
				<div class="columns one" style="color:#fff;">&nbsp;</div>
			</div>
		<?php
			$destinos = destino::obtener_datos_destino();
			for ($i=0; $i < count($destinos); $i++) { 
		?>
			<div class="columns seven" id="pqt_destino<?php echo $destinos[$i]["iddestino"]; ?>" style="border-bottom: 1px solid #10689b;">
				<div class="columns one"><?php echo ($i+1) . " ."; ?></div>
				<div class="columns four"><?php echo $destinos[$i]["nombre_destino"]; ?></div>
				<div class="columns one"><img src="images/delete.png" class="icon_delete_visible" title="Eliminar" onclick="eliminar_registro_adm('pqt_destino', 'iddestino', '<?php echo $destinos[$i]['iddestino']; ?>');" /></div>
			</div>
		<?php
			}
		?>
		<div class="clear_mayor"></div>
	</div>
</div>

<div class="columns sixteen admin" id="temporada" <?php if(!isset($_POST['Registrar_Temporada'])) { echo "style='display:none;'"; } ?> >
	<div class="columns seven" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
		<center><h4>Registro de Temporadas</h4></center>
		<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" class="miform" >
			<div class="columns two" style="font-weight:bold; color:#10689b;">
				TEMPORADA
			</div>
			<div class="columns four">
				<input type="text" name="nombre_temporada" />
			</div>
			<div class="columns six" style="float:right;">
				<input type="submit" name="Registrar_Temporada" value="Registrar" style="float:right" />
			</div>
		</form>
	</div>
	<div class="columns eight" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
		<center><h4>Temporadas Registradas</h4></center>
			<div class="columns seven" style="background-color:#10689b; ">
				<div class="columns one" style="color:#fff;">&nbsp;</div>
				<div class="columns four" style="color:#fff;">NOMBRE TEMPORADA</div>
				<div class="columns one" style="color:#fff;">&nbsp;</div>
			</div>
		<?php
			$temporadas = temporada::obtener_datos_temporada();
			for ($i=0; $i < count($temporadas); $i++) { 
		?>
			<div class="columns seven" id="pqt_temporada<?php echo $temporadas[$i]["idtemporada"]; ?>" style="border-bottom: 1px solid #10689b;">
				<div class="columns one"><?php echo ($i+1) . " ."; ?></div>
				<div class="columns four"><?php echo $temporadas[$i]["nombre_temporada"]; ?></div>
				<div class="columns one"><img src="images/delete.png" class="icon_delete_visible" title="Eliminar" onclick="eliminar_registro_adm('pqt_temporada', 'idtemporada', '<?php echo $temporadas[$i]["idtemporada"]; ?>');" /></div>
			</div>
		<?php
			}
		?>
		<div class="clear_mayor"></div>
	</div>
</div>

<div class="columns sixteen admin" id="hotel" <?php if(!isset($_POST['Registrar_Hotel'])) { echo "style='display:none;'"; } ?> >
	<div class="columns seven" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
		<center><h4>Registro de Hoteles</h4></center>
		<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" class="miform" >
			<div class="columns two" style="font-weight:bold; color:#10689b;">
				COD HOTEL
			</div>
			<div class="columns four">
				<input type="text" name="codigo_hotel" />
			</div>
			<div class="columns two" style="font-weight:bold; color:#10689b;">
				NOMBRE
			</div>
			<div class="columns four">
				<input type="text" name="nombre_hotel" />
			</div>
			<div class="columns two" style="font-weight:bold; color:#10689b;">
				CATEGORIA
			</div>
			<div class="columns four">
				<input type="text" name="categoria_hotel" />
			</div>
			<div class="columns six" style="float:right;">
				<input type="submit" name="Registrar_Hotel" value="Registrar" style="float:right" />
			</div>
		</form>
	</div>
	<div class="columns eight" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
		<center><h4>Hoteles Registrados</h4></center>
			<div class="columns seven" style="background-color:#10689b; ">
				<div class="columns one" style="color:#fff;">&nbsp;</div>
				<div class="columns four" style="color:#fff;">NOMBRE</div>
				<div class="columns one" style="color:#fff;">&nbsp;</div>
			</div>
		<?php
			$hoteles = hotel::obtener_hoteles();
			for ($i=0; $i < count($hoteles); $i++) { 
		?>
			<div class="columns seven" id="pqt_hotel<?php echo $hoteles[$i]["idhotel"]; ?>" style="border-bottom: 1px solid #10689b;">
				<div class="columns one"><?php echo ($i+1) . " ."; ?></div>
				<div class="columns four"><?php echo $hoteles[$i]["nombre_hotel"] . ' (' . $hoteles[$i]["codigo_hotel"] . ') - ' . $hoteles[$i]["categoria"] . '*'; ?></div>
				<div class="columns one"><img src="images/delete.png" onclick="eliminar_registro_adm('pqt_hotel', 'idhotel', '<?php echo $hoteles[$i]["idhotel"]; ?>');" class="icon_delete_visible" title="Eliminar" /></div>
			</div>
		<?php
			}
		?>
		<div class="clear_mayor"></div>
	</div>
</div>

<div class="columns sixteen admin" id="tipo_paquete" <?php if(!isset($_POST['Registrar_Tipo_Paquete'])) { echo "style='display:none;'"; } ?> >
	<div class="columns seven" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
		<center><h4>Registro de Tipos de Paquete</h4></center>
		<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" class="miform" >
			<div class="columns two" style="font-weight:bold; color:#10689b;">
				TIPO DE PAQ
			</div>
			<div class="columns four">
				<input type="text" name="nombre_tipo_paquete" />
			</div>
			<div class="columns two" style="font-weight:bold; color:#10689b;">
				IMAGEN
			</div>
			<div class="columns six">
				<input id="archivo_imagen" type="file" name="archivos[]" onchange="seleccionar_imagen_tipo_paquete();" />
				<div class="columns five" id="imagen_contenido">
				</div>
			</div>
			<div class="columns six" style="float:right;">
				<input type="submit" name="Registrar_Tipo_Paquete" value="Registrar" style="float:right" />
			</div>
		</form>
	</div>
	<div class="columns eight" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
		<center><h4>Tipos de Paquete Registrados</h4></center>
			<div class="columns seven" style="background-color:#10689b; ">
				<div class="columns one" style="color:#fff;">&nbsp;</div>
				<div class="columns four" style="color:#fff;">NOMBRE</div>
				<div class="columns one" style="color:#fff;">&nbsp;</div>
			</div>
		<?php
			$tipos_de_paquete = tipo_paquete::obtener_datos_tipo_paquete();
			for ($i=0; $i < count($tipos_de_paquete); $i++) { 
		?>
			<div class="columns seven" id="pqt_tipo_paquete<?php echo $tipos_de_paquete[$i]["idtipo_paquete"]; ?>" style="border-bottom: 1px solid #10689b;">
				<div class="columns one"><?php echo ($i+1) . " ."; ?></div>
				<div class="columns four"><?php echo $tipos_de_paquete[$i]["descripcion"]; ?></div>
				<div class="columns one"><img src="images/delete.png" onclick="eliminar_registro_adm('pqt_tipo_paquete', 'idtipo_paquete', '<?php echo $tipos_de_paquete[$i]["idtipo_paquete"]; ?>');" class="icon_delete_visible" title="Eliminar" /></div>
			</div>
		<?php
			}
		?>
		<div class="clear_mayor"></div>
	</div>
</div>

<div class="columns sixteen admin" id="operadora" <?php if(!isset($_POST['Registrar_Operadora'])) { echo "style='display:none;'"; } ?> >
	<div class="columns seven" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
		<center><h4>Registro de Operadoras</h4></center>
		<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" class="miform" >
			<div class="columns two" style="font-weight:bold; color:#10689b;">
				OPERADORA
			</div>
			<div class="columns four">
				<input type="text" name="nombre_operadora" />
			</div>
			<div class="columns two" style="font-weight:bold; color:#10689b;">
				CORREO
			</div>
			<div class="columns four">
				<input type="text" name="direccion_operadora" />
			</div>
			<div class="columns two" style="font-weight:bold; color:#10689b;">
				TELEFONOS
			</div>
			<div class="columns four">
				<input type="text" name="telefonos_operadora" />
			</div>
			<div class="columns two" style="font-weight:bold; color:#10689b;">
				OBSERVACIONES
			</div>
			<div class="columns four">
				<textarea name="observaciones_operadora" ></textarea>
			</div>
			<div class="columns six" style="float:right;">
				<input type="submit" name="Registrar_Operadora" value="Registrar" style="float:right" />
			</div>
		</form>
	</div>
	<div class="columns eight" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
			<center><h4>Operadoras Registradas</h4></center>
			<div class="columns seven" style="background-color:#10689b; ">
				<div class="columns one" style="color:#fff;">&nbsp;</div>
				<div class="columns four" style="color:#fff;">NOMBRE</div>
				<div class="columns one" style="color:#fff;">&nbsp;</div>
			</div>
		<?php
			$operadoras = operadora::obtener_datos_operadora();
			for ($i=0; $i < count($operadoras); $i++) { 
		?>
			<div class="columns seven" id="pqt_operadora<?php echo $operadoras[$i]["idoperadora"]; ?>" style="border-bottom: 1px solid #10689b;">
				<div class="columns one"><?php echo ($i+1) . " ."; ?></div>
				<div class="columns four"><?php echo $operadoras[$i]["nombre_operadora"]; ?></div>
				<div class="columns one"><img src="images/delete.png" onclick="eliminar_registro_adm('pqt_operadora', 'idoperadora', '<?php echo $operadoras[$i]["idoperadora"]; ?>');" class="icon_delete_visible" title="Eliminar" /></div>
			</div>
		<?php
			}
		?>
		<div class="clear_mayor"></div>
	</div>
</div>

<div class="columns sixteen admin" id="tipo_evento" <?php if(!isset($_POST['Registrar_Tipo_Evento'])) { echo "style='display:none;'"; } ?> >
	<div class="columns seven" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
		<center><h4>Registro de Tipos de Evento</h4></center>
		<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" class="miform" >
			<div class="columns two" style="font-weight:bold; color:#10689b;">
				TIPO DE EVENTO
			</div>
			<div class="columns four">
				<input type="text" name="nombre_tipo_evento" />
			</div>
			<div class="columns six" style="float:right;">
				<input type="submit" name="Registrar_Tipo_Evento" value="Registrar" style="float:right" />
			</div>
		</form>
	</div>
	<div class="columns eight" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
			<center><h4>Tipos de Evento Registrados</h4></center>
			<div class="columns seven" style="background-color:#10689b; ">
				<div class="columns one" style="color:#fff;">&nbsp;</div>
				<div class="columns four" style="color:#fff;">NOMBRE</div>
				<div class="columns one" style="color:#fff;">&nbsp;</div>
			</div>
		<?php
			$tipos_evento = tipo_evento::obtener_datos_tipo_evento();
			for ($i=0; $i < count($tipos_evento); $i++) { 
		?>
			<div class="columns seven" id="pqt_tipo_evento<?php echo $tipos_evento[$i]["idtipo_evento"]; ?>" style="border-bottom: 1px solid #10689b;">
				<div class="columns one"><?php echo ($i+1) . " ."; ?></div>
				<div class="columns four"><?php echo $tipos_evento[$i]["descripcion"]; ?></div>
				<div class="columns one"><img src="images/delete.png" onclick="eliminar_registro_adm('pqt_tipo_evento', 'idtipo_evento', '<?php echo $tipos_evento[$i]["idtipo_evento"]; ?>');" class="icon_delete_visible" title="Eliminar" /></div>
			</div>
		<?php
			}
		?>
		<div class="clear_mayor"></div>
	</div>
</div>

<div class="columns sixteen admin" id="alcance_evento" <?php if(!isset($_POST['Registrar_Alcance_Evento'])) { echo "style='display:none;'"; } ?> >
	<div class="columns seven" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
		<center><h4>Registro de Alcances de Evento</h4></center>
		<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" class="miform" >
			<div class="columns two" style="font-weight:bold; color:#10689b;">
				ALCANCE EVENTO
			</div>
			<div class="columns four">
				<input type="text" name="nombre_alcance_evento" />
			</div>
			<div class="columns six" style="float:right;">
				<input type="submit" name="Registrar_Alcance_Evento" value="Registrar" style="float:right" />
			</div>
		</form>
	</div>
	<div class="columns eight" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
			<center><h4>Registrados</h4></center>
			<div class="columns seven" style="background-color:#10689b; ">
				<div class="columns one" style="color:#fff;">&nbsp;</div>
				<div class="columns four" style="color:#fff;">ALCANCE EVENTO</div>
				<div class="columns one" style="color:#fff;">&nbsp;</div>
			</div>
		<?php
			$alcances_evento = alcance_evento::obtener_datos_alcance_evento();
			for ($i=0; $i < count($alcances_evento); $i++) { 
		?>
			<div class="columns seven" id="pqt_alcance_evento<?php echo $alcances_evento[$i]["idalcance_evento"]; ?>" style="border-bottom: 1px solid #10689b;">
				<div class="columns one"><?php echo ($i+1) . " ."; ?></div>
				<div class="columns four"><?php echo $alcances_evento[$i]["descripcion"]; ?></div>
				<div class="columns one"><img src="images/delete.png" onclick="eliminar_registro_adm('pqt_alcance_evento', 'idalcance_evento', '<?php echo $alcances_evento[$i]["idalcance_evento"]; ?>');" class="icon_delete_visible" title="Eliminar" /></div>
			</div>
		<?php
			}
		?>
		<div class="clear_mayor"></div>
	</div>
</div>

<div class="columns sixteen admin" id="otro_servicio" <?php if(!isset($_POST['Registrar_Otro_Servicio'])) { echo "style='display:none;'"; } ?> >
	<div class="columns seven" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
		<center><h4>Registro de Otros Servicios</h4></center>
		<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" class="miform" >
			<div class="columns two" style="font-weight:bold; color:#10689b;">
				OTRO SERVICIO
			</div>
			<div class="columns four">
				<input type="text" name="nombre_otro_servicio" />
			</div>
			<div class="columns six" style="float:right;">
				<input type="submit" name="Registrar_Otro_Servicio" value="Registrar" style="float:right" />
			</div>
		</form>
	</div>
	<div class="columns eight" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
		<center><h4>Registrados</h4></center>
			<div class="columns seven" style="background-color:#10689b; ">
				<div class="columns one" style="color:#fff;">&nbsp;</div>
				<div class="columns four" style="color:#fff;">OTRO SERVICIO</div>
				<div class="columns one" style="color:#fff;">&nbsp;</div>
			</div>
		<?php
			$otros_servicios = pqt_otro_servicio::obtener_otros_servicios();
			for ($i=0; $i < count($otros_servicios); $i++) { 
		?>
			<div class="columns seven" id="pqt_otro_servicio<?php echo $otros_servicios[$i]["idotro_servicio"]; ?>" style="border-bottom: 1px solid #10689b;">
				<div class="columns one"><?php echo ($i+1) . " ."; ?></div>
				<div class="columns four"><?php echo $otros_servicios[$i]["nombre_servicio"]; ?></div>
				<div class="columns one"><img src="images/delete.png" onclick="eliminar_registro_adm('pqt_otro_servicio', 'idotro_servicio', '<?php echo $otros_servicios[$i]["idotro_servicio"]; ?>');" class="icon_delete_visible" title="Eliminar" /></div>
			</div>
		<?php
			}
		?>
		<div class="clear_mayor"></div>
	</div>
</div>

<script type="text/javascript">
	function mostrar_panel()
	{
		mostrar = $("input[name='panel_admin']:checked").val();
		$(".admin").hide();
		$("#" + mostrar).show();
	}
</script>

<?php
	include("footer.php");
?>