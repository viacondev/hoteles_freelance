<?php
	include("../BD/controladoraBD.php");
	include("../entidad/pax.php");

	$idcotizacion   = $_GET['c'];
	$idservicio  	= $_GET['s'];
	$type 			= $_GET['t'];
	$descripcion 	= $_GET['d'];

	switch ($type) 
	{
		case 'h':
			$nservicio = 'HOTEL';
			break;
		case 'b':
			$nservicio = 'BOLETO';
			break;
		case 's':
			$nservicio = 'OTRO SERVICIO';
			break;
		case 'i':
			$nservicio = 'INSCRIPCION';
			break;
		default:
			$nservicio = 'N/N';
			break;
	}

	$paxs = pax::obtener_pax_idcotizacion($idcotizacion);

	if($type == 'h')
	{
		$paxs_en_servicio 	= pax::obtener_paxs_por_item_hotel($idservicio);
		
		$cad_paxs = '';
		for ($i=0; $i < count($paxs_en_servicio); $i++) 
		{ 
			$cad_paxs .= $paxs_en_servicio[$i]['idpax'] . ';';
		}

		$servicio 			= pax::obtener_item_hotel($idservicio);
		$precios = array();
		if($servicio[0]['precio_single'] > 0)
			$precios[] = array(0 => 1, 1=> 'SGL - ' . $servicio[0]['precio_single']);
		if($servicio[0]['precio_doble'] > 0)
			$precios[] = array(0 => 2, 1=> 'DBL - ' . $servicio[0]['precio_doble']);
		if($servicio[0]['precio_triple'] > 0)
			$precios[] = array(0 => 3, 1=> 'TPL - ' . $servicio[0]['precio_triple']);
		if($servicio[0]['precio_cuadruple'] > 0)
			$precios[] = array(0 => 4, 1=> 'CPL - ' . $servicio[0]['precio_cuadruple']);
		/*if($servicio[0]['precio_menor'] > 0)
			$precios[] = array(0 => 5, 1=> 'CNN - ' . $servicio[0]['precio_menor']);
		if($servicio[0]['precio_infante'] > 0)
			$precios[] = array(0 => 6, 1=> 'INF - ' . $servicio[0]['precio_infante']);*/
	}
	else if($type == 'b')
	{
		$paxs_en_servicio 	= pax::obtener_paxs_segun_boleto($idservicio);

		$cad_paxs = '';
		for ($i=0; $i < count($paxs_en_servicio); $i++) 
		{ 
			$cad_paxs .= $paxs_en_servicio[$i]['idpax'] . ';';
		}

		$servicio 			= pax::obtener_boleto_cotizado($idservicio);
		$precios = array();
		if($servicio[0]['precio_adulto'] > 0)
			$precios[] = array(0 => 1, 1=> 'ADT - ' . $servicio[0]['precio_adulto']);
		if($servicio[0]['precio_menor'] > 0)
			$precios[] = array(0 => 2, 1=> 'CNN - ' . $servicio[0]['precio_menor']);
		if($servicio[0]['precio_infante'] > 0)
			$precios[] = array(0 => 3, 1=> 'INF - ' . $servicio[0]['precio_infante']);
	}
	else if($type == 's')
	{
		$paxs_en_servicio 	= pax::obtener_paxs_segun_servicio($idservicio);
		
		$cad_paxs = '';
		for ($i=0; $i < count($paxs_en_servicio); $i++) 
		{ 
			$cad_paxs .= $paxs_en_servicio[$i]['idpax'] . ';';
		}

		$servicio 			= pax::obtener_tarifa_otros($idservicio);
		$precios = array();
		if($servicio[0]['precio_adulto'] > 0)
			$precios[] = array(0 => 1, 1=> 'ADT - ' . $servicio[0]['precio_adulto']);
		if($servicio[0]['precio_menor'] > 0)
			$precios[] = array(0 => 2, 1=> 'CNN - ' . $servicio[0]['precio_menor']);
		if($servicio[0]['precio_infante'] > 0)
			$precios[] = array(0 => 3, 1=> 'INF - ' . $servicio[0]['precio_infante']);
	}
	else if($type == 'i')
	{
		$paxs_en_servicio 	= pax::obtener_paxs_segun_inscripcion($idservicio);
		
		$cad_paxs = '';
		for ($i=0; $i < count($paxs_en_servicio); $i++) 
		{ 
			$cad_paxs .= $paxs_en_servicio[$i]['idpax'] . ';';
		}

		$precios = array();
		$precios[] = array(0 => 1, 1=> 'Precio Unico');
	}
	else
	{
		$paxs_en_servicio = array();
	}
	//echo "<pre>"; print_r($precios); echo "</pre>";
?>
	<link href="lib/tinybox2/style.css" rel="stylesheet">
	<div class="hidden plantilla_pax_hab">
		<div class="four columns hab_ind_" style="border-bottom:2px solid #aaa; margin-bottom:10px;">
			_contenido_
			<img src="images/cross.png" class="mini" onclick="QuitarPaxServicioAsignado(_ind_);" />
		</div>
	</div>
	<div class="container" style="max-height:550px !important; overflow-y: auto; overflow-x: hidden;" >
		<div class="twelve columns">
			<div class="twelve columns" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">AÑADIR PASAJEROS </div>
			<div class="nine columns">
				<strong>SERVICIO DE <?php echo $nservicio . ' (' . $descripcion . ')'; ?></strong>
			</div>
			<div class="seven columns">
				<select id="tipo_precio">
					<?php
						for ($i=0; $i < count($precios); $i++) 
						{ 
						?>
							<option value="<?php echo $precios[$i][0]; ?>"><?php echo $precios[$i][1]; ?></option>
						<?php
						}
					?>
				</select>
			</div>
			<div class="clear_mayor"></div>
			<?php
				$ciudad_actual = '';
				for ($i=0; $i < count($paxs); $i++) 
				{ 
					$pax = $paxs[$i];

					if($ciudad_actual != $pax['idciudad_origen_paquete'] || $ciudad_actual == '')
					{
					?>
						<div class="sixteen columns" style="font-weight:bold; color:#10689b; ">SALE DESDE <?php echo $pax['ciudad']; ?></div>
					<?php
						$ciudad_actual = $pax['idciudad_origen_paquete'];
					}
				?>
					<div class="four columns" class="line-height:1;" style="margin:0px 5px 0px 5px;">
						<input type="checkbox" value="<?php echo $pax['idpax']; ?>" class="pax_seleccionado pax_<?php echo $pax['idpax']; ?>" id="chk_pax_adt_<?php echo $pax['idpax']; ?>" onchange="SeleccionarPax(<?php echo $pax['idpax']; ?>, 'adt');"   
						<?php 
							if(strpos($cad_paxs, $pax['idpax']) > -1)
							{
						?>
								title="Este Pasajero Ya cuenta con el servicio" disabled 
						<?php
							}
						?>
						 />
						<font id="pax<?php echo $pax['idpax']; ?>"><?php echo substr($pax['nombre_cliente'] . ' ' . $pax['apellido_cliente'], 0, 25); ?></font>
					</div>
				<?php
				}
			?>
			<div class="clear_mayor"></div>
			<div class="clear_mayor"></div>
			<?php
				if($type == 'h')
				{
				?>
					<div class="nine columns">
						<strong> MENORES :</strong>
					</div>
					<div class="seven columns">
						<select id="tipo_precio">
							<option value="5"><?php echo 'CNN - ' . $servicio[0]['precio_menor']; ?></option>
						</select>
					</div>
					<?php
						$ciudad_actual = '';
						for ($i=0; $i < count($paxs); $i++) 
						{ 
							$pax = $paxs[$i];

							if($ciudad_actual != $pax['idciudad_origen_paquete'] || $ciudad_actual == '')
							{
							?>
								<div class="sixteen columns" style="font-weight:bold; color:#10689b; ">SALE DESDE <?php echo $pax['ciudad']; ?></div>
							<?php
								$ciudad_actual = $pax['idciudad_origen_paquete'];
							}
						?>
							<div class="four columns">
								<input type="checkbox" value="<?php echo $pax['idpax']; ?>" class="pax_seleccionado_cnn pax_<?php echo $pax['idpax']; ?>" id="chk_pax_cnn_<?php echo $pax['idpax']; ?>" onchange="SeleccionarPax(<?php echo $pax['idpax']; ?>, 'cnn');"   
								<?php 
									if(strpos($cad_paxs, $pax['idpax']) > -1)
									{
								?>
										title="Este Pasajero Ya cuenta con el servicio" disabled 
								<?php
									}
								?>
								 />
								<font id="pax<?php echo $pax['idpax']; ?>"><?php echo substr($pax['nombre_cliente'] . ' ' . $pax['apellido_cliente'], 0, 25); ?></font>
							</div>
						<?php
						}
					?>
					<div class="clear_mayor"></div>
					<div class="clear_mayor"></div>
					<div class="nine columns">
						<strong> INFANTES :</strong>
					</div>
					<div class="seven columns">
						<select id="tipo_precio">
							<option value="6"><?php echo 'INF - ' . $servicio[0]['precio_infante']; ?></option>
						</select>
					</div>
					<?php
						$ciudad_actual = '';
						for ($i=0; $i < count($paxs); $i++) 
						{ 
							$pax = $paxs[$i];
							
							if($ciudad_actual != $pax['idciudad_origen_paquete'] || $ciudad_actual == '')
							{
							?>
								<div class="sixteen columns" style="font-weight:bold; color:#10689b; ">SALE DESDE <?php echo $pax['ciudad']; ?></div>
							<?php
								$ciudad_actual = $pax['idciudad_origen_paquete'];
							}
						?>
							<div class="four columns">
								<input type="checkbox" value="<?php echo $pax['idpax']; ?>" class="pax_seleccionado_inf pax_<?php echo $pax['idpax']; ?>" id="chk_pax_inf_<?php echo $pax['idpax']; ?>" onchange="SeleccionarPax(<?php echo $pax['idpax']; ?>, 'inf');"   
								<?php 
									if(strpos($cad_paxs, $pax['idpax']) > -1)
									{
								?>
										title="Este Pasajero Ya cuenta con el servicio" disabled 
								<?php
									}
								?>
								 />
								<font id="pax<?php echo $pax['idpax']; ?>"><?php echo substr($pax['nombre_cliente'] . ' ' . $pax['apellido_cliente'], 0, 25); ?></font>
							</div>
						<?php
						}
					?>
				<?php
				}
			?>
			<div class="clear_mayor"></div>
			<div class="four columns">
				<input type="hidden" value="<?php echo $idservicio; ?>" id="idserviciopax" />
				<input type="hidden" value="<?php echo $type; ?>" id="typeserviciopax" />
				<input type="button" value="Añadir Seleccionados" onclick="AgregarPaxsServicio();" />
			</div>
		</div>

		<div class="four columns pasajeros_x_hab" >
			<div class="four columns" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">PASAJEROS X HABITACION</div>
		</div>
		<div class="four columns">
			<input type="button" value="Registrar" onclick="AgregarPaxServicio();" />			
		</div>
	</div>