<?php

	$tipo = "";

	switch ($_GET['tp']) {
		case 1:
			$tipo = 'evento';
			break;
		
		default:
			$tipo = 'undefined';
			break;
	}

	if($_GET['ex'] == 2)
	{
		$resultado 	= '(Anulación)';
		$estado    	= 'A';
	}
	else
	{
		$resultado 	= '';
		$estado		= 'R';
	}

?>
<link href="lib/tinybox2/style.css" rel="stylesheet">
<div class="container" style="width:600px;">
	<div class="four columns">
		<h4>Atender Seguimiento<br/><?php echo $resultado; ?></h4>
	</div>
	<div class="four columns">
		<textarea style="height:20px;" placeholder="Comentario" id="cm"></textarea>
	</div>
	<div class="one columns">
		<input type="hidden" value="<?php echo $_GET['us']; ?>" id="us" />
		<input type="hidden" value="<?php echo $_GET['seg']; ?>" id="sg" />
		<input type="hidden" value="<?php echo $tipo; ?>" id="tp" />
		<input type="hidden" value="<?php echo $estado; ?>" id="es" />
		<input type="button" value="Aceptar" style="float:right;" onclick="atender_seguimiento();" />
	</div>
</div>