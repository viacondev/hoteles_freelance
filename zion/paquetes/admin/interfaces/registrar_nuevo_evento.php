<?php
	session_start();
	include('../BD/controladoraBD.php');
	include('../entidad/publicacion.php');
	include('../entidad/evento.php');

	if(isset($_POST['registrar_evento']))
	{
		//Registro de la publicacion
		$titulo 			= $_POST['titulo'];

		$tipo_publicacion 	= 'E';

		$idusuarios 		= $_SESSION['idusuarios'];

		$a_fecha_caducidad 	= explode('/', $_POST['vencimiento']);
		$fecha_caducidad 	= $a_fecha_caducidad[2] . '-' . $a_fecha_caducidad[1] . '-' . $a_fecha_caducidad[0];

		$a_fecha_salida 	= explode('/', $_POST['salida']);
		if(count($a_fecha_salida) == 3)
			$fecha_salida 	= $a_fecha_salida[2] . '-' . $a_fecha_salida[1] . '-' . $a_fecha_salida[0];
		else
			$fecha_salida 	= $_POST['salida'];

		$a_fecha_retorno 	= explode('/', $_POST['retorno']);
		if(count($a_fecha_retorno) == 3)
			$fecha_retorno 	= $a_fecha_retorno[2] . '-' . $a_fecha_retorno[1] . '-' . $a_fecha_retorno[0];
		else
			$fecha_retorno 	= $_POST['retorno'];

		$precio_desde 		= $_POST['precio_desde'];

		$publicado			= 0;

		$solo_imagen 		= $_POST['solo_imagen'];
			
		$publicacion 	= publicacion::registrar_nueva_publicacion($idusuarios, $tipo_publicacion, $titulo, $fecha_caducidad, $fecha_salida, $fecha_retorno, $precio_desde, $publicado, $solo_imagen);
		$idpublicacion  = mysql_insert_id();

		// Registrar el evento
		$nombre_evento 	= $_POST['nombre_evento'];

		$sigla_evento 	= $_POST['sigla_evento'];

		$sede 			= $_POST['sede'];

		$a_fecha_inicio = explode('/', $_POST['fecha_inicio']);
		$fecha_inicio 	= $a_fecha_inicio[2] . '-' . $a_fecha_inicio[1] . '-' . $a_fecha_inicio[0];

		$a_fecha_fin 	= explode('/', $_POST['fecha_fin']);
		$fecha_fin 	= $a_fecha_fin[2] . '-' . $a_fecha_fin[1] . '-' . $a_fecha_fin[0];

		$evento 	= evento::registrar_nuevo_evento($nombre_evento, $sigla_evento, $sede, $fecha_inicio, $fecha_fin);
		$idevento 	= mysql_insert_id();

		//Registro de la union entre publicacion y evento
		$publicacion_evento = evento::registrar_publicacion_has_evento($idpublicacion, $idevento);

		header("location:editar_evento.php?idevento=" . $idpublicacion);

	}

?>