<link href="lib/tinybox2/style.css" rel="stylesheet">
<?php
	include('../BD/controladoraBD.php');
	//include('../entidad/usuarios_paquetes.php');
	include('../entidad/tarifa_hotel.php');
	include('../entidad/tarifa_otros.php');
	include('../entidad/opcion_hoteles.php');
	include('../control/c_editar_tarifas_hotel_combinadas.php');

	$c_edit_tarifas = new c_editar_tarifas_hotel_combinadas;

	$idciudad = $_GET['c'];
?>
<div class="container">
	<div class="sixteen columns" style="color:#FFFFFF; position:absolute; padding:10px; opacity:0.9; display:none;" id="mensaje_pax">
	</div>
	<div class="eight columns" style="overflow:auto; height:500px;">
		<?php
			$tarifas 	= $c_edit_tarifas->obtener_tarifas_hotel($idciudad);
			if($tarifas !== false)
			{
				for($i=0; $i<count($tarifas); $i++)
				{
					$idtarifa 	= $tarifas[$i]['idtarifa_hotel'];
					$items 		= $c_edit_tarifas->obtener_items_de_tarifa($idtarifa);
					if($items !== false)
					{
		?>
						<table style="width:95%;">
							<tr style="border:1px solid #AAAAAA; background-color:#10689b; color:#FFFFFF;">
								<th>
									DESTINO: 
									<input type="hidden" value="<?php echo $idtarifa; ?>" class="mis_destinos" />
								</th>
								<th id="dest_<?php echo $idtarifa; ?>"><?php echo $tarifas[$i]['destino']; ?></th>
							</tr>
		<?php
						for ($j=0; $j < count($items); $j++) 
						{ 
							$iditem = $items[$j]['iditem_hotel'];
		?>
							<tr>
								<td style="border:1px solid #AAAAAA; padding:2px;" id="hot_<?php echo $iditem; ?>">
									<?php echo strtoupper(nl2br($items[$j]['nombre_hotel'])); ?>
								</td>
								<td style="border:1px solid #AAAAAA; padding:5px;">
									<input type="radio" name="opcion_<?php echo $idtarifa; ?>" value="<?php echo $iditem; ?>"/>
								</td>
							</tr>
		<?php
						}
		?>
						</table>
						<hr/>
		<?php
					}
				}
			}
		?>
		<input type="button" value="Añadir" onclick="agrupar_hoteles_por_destino();" />	
	</div>

	<div class="eight columns" style="overflow:auto; height:550px;">
		<table style="width:95%;" id="opciones">
			<tr style="border:1px solid #AAAAAA; background-color:#10689b; color:#FFFFFF;">
				<th colspan="3">
					OPCIONES DE HOTELES
				</th>
			</tr>
			<?php 
				$opciones_hotel = $c_edit_tarifas->obtener_opciones_de_hotel($idciudad);
				if($opciones_hotel === false)
				{
					$opciones_hotel = array();
				}
				
				for ($i=0; $i < count($opciones_hotel); $i++) 
				{ 
					$idopc 	= $opciones_hotel[$i]['idopcion_hoteles'];
					$observ = $opciones_hotel[$i]['observacion'];
					
			?>
					<tr id="opc_<?php echo $i; ?>" style="border:1px solid #AAAAAA;">
						<td>
							<?php
								$detalle = $c_edit_tarifas->obtener_hoteles_ofertados($idopc);
								if($detalle === false)
								{
									$detalle = array();
								}
								
								for ($j=0; $j < count($detalle); $j++) 
								{ 
									$hotel 		= $detalle[$j]['nombre_hotel'];
									$item 		= $detalle[$j]['iditem_hotel'];
									$destino 	= $detalle[$j]['destino'];
									echo strtoupper(nl2br($hotel)) . '(' . $destino . ')';
							?>
									<input type="hidden" value="<?php echo $item; ?>" class="myop_<?php echo $i; ?>" /><br/>
							<?php
								}
							?>
						</td>
						<td>
							<textarea id="obs_<?php echo $i; ?>" cols="10" rows="2" style="min-height:20px; margin-botton:0px; display:block;" placeholder="Observacion"><?php echo $observ; ?></textarea>
						</td>
						<td>
							<img src="images/cancel.png" style="width:10px; height:10px; cursor:pointer;" title="Eliminar" onclick="quitar_combinacion(<?php echo $i; ?>);" />
							<input type="hidden" value="<?php echo $idopc; ?>" name="<?php echo $i; ?>" class="opciones_hotel" />
						</td>
					</tr>
			<?php
				}
			?>
		</table>
		<input type="hidden" id="c_opciones" value="<?php echo count($opciones_hotel); ?>" />
		<input type="hidden" id="city" value="<?php echo $idciudad; ?>" />
		<span id="combi_eliminar" style="display:none;"></span>
		<input type="button" value="Guardar Cambios" onclick="guardar_tabla_hoteles();" />
	</div>
	<table style="display:none;" id="opcion_hotel">
		<tr id="opc__i_" style="border:1px solid #AAAAAA;">
			<td>
				_x_
			</td>
			<td>
				<textarea id="obs__i_" cols="10" rows="2" style="min-height:20px; margin-botton:0px; display:block;" placeholder="Observacion"></textarea>
			</td>
			<td>
				<img src="images/cancel.png" style="width:10px; height:10px; cursor:pointer;" title="Eliminar" onclick="quitar_combinacion(_i_);" />
				<input type="hidden" value="0" name="_i_" class="_cls_" />
			</td>
		</tr>
	</table>
	<div style="display:none;" id="cd_item">
		<input type="hidden" value="_v_" class="myop__i_" />
	</div>
</div>