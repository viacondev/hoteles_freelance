<?php
	include 'header.php';
	include('../entidad/publicacion.php');
	include('../entidad/paquete.php');
	include('../entidad/usuarios_paquetes.php');
	include('../control/c_buscar_promocion.php');

	$c_buscar_promocion = new c_buscar_promocion;
	$array_palabras 	= $c_buscar_promocion->cargar_palabras_clave();

?>
<script type="text/javascript">
	function Mostrar_Ocultar()
    {
        if($("input[name='buscar_por']:checked").val() == 1)
        {
            $("#busqueda_codigo").hide();
            $("#otros").show();
        }    
        else
        {
            $("#busqueda_codigo").show();
            $("#otros").hide();
        }
    }	
    $(function(){
        var autocompletar = new Array();
        <?php 
            for($p = 0;$p < count($array_palabras); $p++)
            { 
        ?>
                autocompletar.push('<?php echo strtoupper($array_palabras[$p]); ?>');
        <?php 
            } 
        ?>
         $("#busqueda").autocomplete({ 
           source: autocompletar,
           minLength: 3
        });

    });	
</script>
<div class="eigth columns">
	<h3>BUSCAR PROMOCIONES</h3>
</div>
<div class="eigth columns">
	<img src="images/travel.png" style="height:40px; float:left; display:inline;" />	
</div>
<form class="sixteen columns" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#F1F1F1; padding-top:10px;" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <div class="columns four">
        <input type="checkbox" name="anulados" <?php if(isset($_POST['anulados'])) echo "Checked"; ?>/>Buscar entre los anulados
    </div>
    <div class="clear_mayor"></div>
    <div class="fifteen columns">
    	<input type="text" name="busqueda" id="busqueda" value="<?php echo $_POST['busqueda']; ?>" style="width:90%; padding:5px;" placeholder="Introduzca Promocion" />
    </div>
    <div class="clear"></div>
    <div class="fifteen columns">
    	<input type="submit" name="buscar_promocion" value="Buscar" style="font-size:15pt; padding: 5px 15px; margin: 10px 20px;" />
    </div>
</form>
<div class="sixteen columns" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#F1F1F1; <?php if(!isset($_POST["buscar_promocion"])) { ?>display:none;<?php } ?>" >
	
	<?php
		if(isset($_POST["buscar_promocion"]))
		{
			if(isset($_POST['anulados']))
			{
				$estado = '0';
				$estado_descripcion = 'Anulados';
				$color = '#ff3300';
			}
			else
			{
				$estado = '1';
				$estado_descripcion = 'Vigentes';
				$color = '#10689b';
			}
			
			$promociones 			= $c_buscar_promocion->buscar_promociones_por_palabra_clave($_POST['busqueda'], $estado);
			$usuarios_paquetes 	= usuarios_paquetes::enviar_datos_usuario($_SESSION["idusuarios"]);
			$autorizado 		= 0;
			if(count($usuarios_paquetes) > 0)
			{
				if($usuarios_paquetes[0]["acceso"] == 'E')
				{
					$autorizado = 1;
				}
			}
	?>
			<div class="columns fifteen" style="color:<?php echo $color; ?>; font-weight:bold;">
				B&Uacute;SQUEDA DE PAQUETES: Se encontraron <?php echo count($promociones); ?> resultados entre las promociones <?php echo $estado_descripcion; ?>.
			</div>
	<?php
			if(count($promociones) > 0)
			{
	?>
				<center><div class="estilo_tabla">
				<table>
					<tr>
						<th></th>
						<th>PROMO</th>
						<th>SALIDA</th>
						<th>RETORNO</th>
						<th>CREACION</th>
						<th>CADUCIDAD</th>
						<th>COT</th>
						<th>MAILS</th>
						<th>EST</th>
						<th>ACCION</th>
					</tr>
	<?php
				for ($i=0; $i < count($promociones); $i++) 
				{ 
					$promocion = $promociones[$i];
	?>
					<tr id="publicacion_<?php echo $promocion['idpublicacion']; ?>">
						<td><strong><?php echo ($i+1); ?></strong></td>
						<td><?php echo strtoupper($promocion['titulo']); ?></td>
						<td>
							<?php 
								$fecha_salida = strtotime($promocion["fecha_salida"]);
								if($fecha_salida)
									$fecha_salida = date('d/M/Y', $fecha_salida);
								else
									$fecha_salida = $promocion["fecha_salida"];
								echo $fecha_salida;
							?>	
						</td>
						<td>
							<?php 
								$fecha_retorno = strtotime($promocion["fecha_retorno"]);
								if($fecha_retorno)
									$fecha_retorno = date('d/M/Y', $fecha_retorno);
								else
									$fecha_retorno = $promocion["fecha_retorno"];
								echo $fecha_retorno;
							?>	
						</td>
						<td><?php echo strtoupper(date('d/M/Y', strtotime($promocion['fecha_publicacion'] . ' 00:00:00')) . '(' . substr($promocion['nombre_usuario'], 0, 5)) . ')'; ?></td>
						<td><?php echo strtoupper(date('d/M/Y', strtotime($promocion['fecha_caducidad'] . ' 00:00:00'))); ?></td>
						<td>
							<a href="mostrar_cotizaciones_por_generico.php?p=<?php echo $promocion['idpublicacion']; ?>" target="_blank">
								<?php
									echo $c_buscar_promocion->obtener_cantidad_de_cotizaciones($promocion['idpublicacion']);
								?>
							</a>
						</td>
						<td>
							<a onclick="Mostrar_Envios_Realizados(<?php echo $promocion['idpublicacion']; ?>);" style="cursor:pointer;">
								<?php
									echo $c_buscar_promocion->obtener_cantidad_de_envios_masivos($promocion['idpublicacion']);
								?>
							</a>
						</td>
						<td>
							<?php
								if($promocion['estado_paquete'] == 0)
								{
								?>
									<font style="color:#FF0000;">A SOL</font>
								<?php
								}
								else if($promocion['estado_paquete'] == 1)
								{
								?>
									ARM
								<?php
								}
							?>
						</td>
						<td>
							<a href="visualizar_promocion.php?idpromo=<?php echo $promocion['idpublicacion']; ?>"><img src="images/icon-view.png" class="icon" title="Visualizar" /></a>
	<?php 
						if($autorizado == 1)
						{
	?>
							<a href="editar_promocion.php?idpromo=<?php echo $promocion['idpublicacion']; ?>"><img src="images/pencil.png" class="icon" title="Editar" /></a>
							<img src="images/cross.png" class="icon" title="Anular" onclick="anular_publicacion(<?php echo $promocion['idpublicacion']; ?>);" />
	<?php
						}
	?>
						</td>
					</tr>
					</div>
	<?php
				}
	?>
				</table>
				</div></center>
	<?php
			}
		}
	?>
</div>
<?php include("footer.php"); ?>