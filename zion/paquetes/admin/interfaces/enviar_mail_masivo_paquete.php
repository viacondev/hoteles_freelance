<?php
ini_set('memory_limit', '-1');
set_time_limit(0);
session_start();
require('../../crm/interfaces/xajax/xajax.inc.php');
include('../../crm/interfaces/lib/swift_required.php');
include '../BD/controladoraBD.php';
include('../entidad/pqt_envio_masivo.php');

	function comprobar_email($email){ 
	    $mail_correcto = 0; 
	    //compruebo unas cosas primeras 
	    if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@")){ 
	       if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," "))) { 
	          //miro si tiene caracter . 
	          if (substr_count($email,".")>= 1){ 
	             //obtengo la terminacion del dominio 
	             $term_dom = substr(strrchr ($email, '.'),1); 
	             //compruebo que la terminación del dominio sea correcta 
	             if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ){ 
	                //compruebo que lo de antes del dominio sea correcto 
	                $antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1); 
	                $caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1); 
	                if ($caracter_ult != "@" && $caracter_ult != "."){ 
	                   $mail_correcto = 1; 
	                } 
	             } 
	          } 
	       } 
	    } 
	    if ($mail_correcto) 
	       return 1; 
	    else 
	       return 0; 
	} 

	/*
	* PREPARANDO CONTENIDO PARA ENVIAR POR CORREO
	*/

	$RUTA_IMG 		= "http://viacontours.com/pqt/images_paquetes/";
	//$RUTA_IMG 		= "http://localhost/viacon/paquetes/images_paquetes/";
	
	$paquetes_seleccionados = $_SESSION['paquetes_segmentados'];
	$cantidad 	= (count($paquetes_seleccionados)/2) + 6;
	$alto 		= $cantidad * 80 + 300;

	$html = '<table width="100%" height="100%" style="width:100%; height:'. $alto . 'px;">
				<tr>
					<td style="background-color:#E1E1E1;">
						<center>
							<table style="width:700px; border:1px solid #345e8f; background-color:#FFFFFF;">
								<tr>
									<td colspan="2" style="background-color:#345e8f; color:#FFFFFF;">
										<center><h3 style="font-family:Lucida Sans, Sans serif; color:#ffffff; margin:0;">PAQUETES PROMOCIONALES</h3></center>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<center><table>
											<tr>
												<td style="font-family:Lucida Sans, Sans serif; padding-left:20px; padding-right:30px; font-size:7pt; line-height:1;">
													<center><img src="http://viacontours.com/online/images/logo_print.png"/></center>
													<hr/>
													<center><table style="font-family:Lucida Sans, Sans serif; font-size:8pt;">
														<tr>
															<td style="padding:10px;">
																<center><img src="http://viacontours.com/paquetes/images/facebook.png" /><br/>
																<a href="http://www.facebook.com/viacon.tours">Facebook</a></center>
															</td>
															<td style="padding:10px;">
																<center><img src="http://viacontours.com/paquetes/images/user.png" /><br/>
																<a href="http://www.viacontours.com/clientes">Clientes</a></center>
															</td>
														</tr>
													</table>
													</center>
													<hr/>
													<center>CALLE VELASCO NRO.232 ESQ. SUAREZ DE FIGUEROA<br/>
													TELEFONO 3-363610<br/>
													PAGINA WEB <a href="http://viacontours.com">www.viacontours.com</a></center>
												</td>
												<td style="padding-left:30px; padding-right:20px;">';
										
	$pqt_grande = $paquetes_seleccionados[0];
												if($pqt_grande['imagen_primera'] == '')
													$imagen_grande = 'imagen_generico.JPG';
												else
													$imagen_grande = $pqt_grande['imagen_primera'];

				$html .=							'<table style="width:90%; border:1px solid #319941; background-color:#F1F1F1;">
														<tr>
															<td style="padding:0px;">
																<img src="' . $RUTA_IMG . $imagen_grande. '" style="width:300px;">
															</td>
														</tr>
														<tr>
															<td style="font-family:Lucida Sans, Sans serif;">
																<center><strong style="font-size:12pt; color:#345e8f;">' . strtoupper($pqt_grande['titulo']) . '</strong></center>
																<table style="width:100%; font-size:8pt;">
																	<tr>
																		<td>';
																			if($pqt_grande['precio_desde'] > 0)
																			$html .= '<strong style="font-family:Lucida Sans, Sans serif;">DESDE ' . $pqt_grande['precio_desde'] . ' USD</strong><br/>';

												                            $salida     = strtotime($pqt_grande["fecha_salida"]);
												                            $retorno    = strtotime($pqt_grande["fecha_retorno"]);
												                            if($salida && $retorno)
																				$html .= '<strong style="font-family:Lucida Sans, Sans serif;">' . floor( ($retorno - $salida) / 86400) . ' NOCHES</strong>';
							$html.=										'</td>
																		<td>
																		</td>
																	</tr>
																	<tr>
																		<td>
																		</td>
																		<td>
																			<a href="http://viacontours.com/trve/paquetes.php?paquete=' . $pqt_grande['idpublicacion'] . '" style="color:#FF3300; font-weight:bold; style="font-family:Lucida Sans, Sans serif;"">VER DETALLES</a>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table></center>
									</td>
								</tr>
								<tr>
									<td colspan="2" style="font-family:Lucida Sans, Sans serif; font-size:12pt; color:#345e8f; padding:10px;">
										/nombre_cli/..
									</td>
								</tr>
								<tr>
									<td colspan="2" style="font-family:Lucida Sans, Sans serif; padding-left:10px; padding-right:10px; font-size:9pt;">
										' . strtoupper(nl2br($_POST['comentario'])) . '
									</td>
								</tr>';
								if(count($paquetes_seleccionados) > 1)
								{
			$html .=			'<tr>
									<td colspan="2" style="font-family:Lucida Sans, Sans serif; padding:5px; font-size:10pt; color:#FFFFFF; background-color:#319941; text-align:center; font-weight:bold;">
										PAQUETES TURISTICOS EN OFERTA
									</td>
								</tr>';
								}
			$html .=			'<tr>
									<td colspan="2">
										<table style="width:100%;">';
								for ($i=1; $i < count($paquetes_seleccionados); $i=$i+2) 
								{ 
									$paquete1 = $paquetes_seleccionados[$i];

									$paquete2 = $paquetes_seleccionados[$i+1];

				$html .=					'<tr>
												<td>';
										if($i < count($paquetes_seleccionados))
										{
			$html .=								'<table style="border:1px solid #AAA; width:100%; background-color:#F1F1F1;">
														<tr>
															<td style="width:75px;">';

																if($paquete1['imagen_segunda'] == '')
																	$imagen_chica = 'mini_generico.jpg';
																else
																	$imagen_chica = $paquete1['imagen_segunda'];

																$html .= '<img src="' . $RUTA_IMG . $imagen_chica . '" >
															</td>
															<td style="padding-left:10px; padding-right:10px;">
																<strong style="font-family:Lucida Sans, Sans serif; color:#319941; font-size:10pt;">' . strtoupper($paquete1['titulo']) . '</strong><br/>
																<table style="font-family:Lucida Sans, Sans serif; font-size:8pt; width:100%;">
																	<tr>
																		<td>';

																		if($paquete1['precio_desde'] > 0)
																			$html .='DESDE ' . $paquete1['precio_desde'] . ' USD<br/>';

																		$salida     = strtotime($paquete1["fecha_salida"]);
										                                $retorno    = strtotime($paquete1["fecha_retorno"]);
										                                if($salida && $retorno)
																			$html .= floor( ($retorno - $salida) / 86400) . ' NOCHES';

																$html .=		'</td>
																		<td></td>
																	</tr>
																	<tr>
																		<td></td>
																		<td><a href="http://viacontours.com/trve/paquetes.php?paquete=' . $paquete1['idpublicacion'] . '" style="cursor:pointer; font-weight:bold; padding:3px;  font-family:Lucida Sans, Sans serif; font-size:8pt; color:#FF3300; text-decoration:underline;">VER DETALLES</a></td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>';
										}
	$html .=									'</td>
												<td>';
										if($i+1<count($paquetes_seleccionados))
										{
				$html .=							'<table style="border:1px solid #AAA; width:100%; background-color:#F1F1F1;">
														<tr>
															<td style="width:75px;">';

															if($paquete2['imagen_segunda'] == '')
																$imagen_chica = 'mini_generico.jpg';
															else
																$imagen_chica = $paquete2['imagen_segunda'];

															$html .=	'<img src="' . $RUTA_IMG . $imagen_chica . '" >
															</td>
															<td style="padding-left:10px; padding-right:10px;">
																<strong style="font-family:Lucida Sans, Sans serif; color:#319941; font-size:10pt;">' . strtoupper($paquete2['titulo']) . '</strong><br/>
																<table style="font-family:Lucida Sans, Sans serif; font-size:8pt; width:100%;">
																	<tr>
																		<td>';

																		if($paquete2['precio_desde'] > 0)
																			$html .='DESDE ' . $paquete2['precio_desde'] . ' USD<br/>';

																		$salida     = strtotime($paquete2["fecha_salida"]);
										                                $retorno    = strtotime($paquete2["fecha_retorno"]);
										                                if($salida && $retorno)
																			$html .= floor( ($retorno - $salida) / 86400) . ' NOCHES';

																$html .=		'</td>
																		<td></td>
																	</tr>
																	<tr>
																		<td></td>
																		<td><a href="http://viacontours.com/trve/paquetes.php?paquete=' . $paquete2['idpublicacion'] . '" style="cursor:pointer; font-weight:bold; padding:3px;  font-family:Lucida Sans, Sans serif; font-size:8pt; color:#FF3300; text-decoration:underline;">VER DETALLES</a></td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>';
										}
	$html .=									'</td>	
											</tr>';
								}
	$html .=							'</table>
									</td>
								</tr>
							</table>
						</center>
					</td>
				</tr>
			</table>';

	$contador_clientes_con_mail 	= $_SESSION['clientes_con_mail'] + 0;
	$contador_clientes_sin_mail 	= $_SESSION['clientes_sin_mail'] + 0;
	$contador_envios_correctos 		= $_SESSION['envios_correctos'] + 0;
	$contador_envios_incorrectos 	= $_SESSION['envios_incorrectos'] + 0;

	$titulo 		= strtoupper($_POST['titulo']);
	$de     		= 'vacaciones@viacontours.com';

	$array_clientes 	= $_SESSION['segmentados'];

	if(!isset($_SESSION['clientes_total']))
	{
		$_SESSION['clientes_total'] = count($array_clientes);
	}

	$cantidad_envios 	= 0;
	$i 					= 0;

	$MAX_ENVIOS			= 995;

	while ($i < count($array_clientes) && $cantidad_envios <= $MAX_ENVIOS) 
	{
		$correos_cliente = $array_clientes[$i]['mails'];
		echo "<strong>CLIENTE " . $array_clientes[$i]['trato_cliente'] . " " . $array_clientes[$i]['nombre_cliente'] . " " . $array_clientes[$i]['apellido_cliente'] . "</strong><br/>";
		$nombre_completo  	= strtoupper($array_clientes[$i]['trato_cliente'] . " " . $array_clientes[$i]['nombre_cliente'] . " " . $array_clientes[$i]['apellido_cliente']);
		$codigo_cliente  	= $array_clientes[$i]['codigo_cliente'];

		if(count($correos_cliente) > 0)
		{
			for ($j=0; $j < count($correos_cliente); $j++) 
			{ 
				try 
			    {
			        $transport 	= Swift_SmtpTransport::newInstance('mail.viacontours.com', 25)
			                ->setUsername('vacaciones@viacontours.com')
			                ->setPassword('P@$$w0rd')
			                ;
			        
			        $mailer 	= Swift_Mailer::newInstance($transport);
			        
			        $para 		= $correos_cliente[$j]['e_mail'];
			        
			        $contenido 	= str_replace('/nombre_cli/', $nombre_completo, $html);
					$contenido 	= str_replace('/poner_codigo/', $codigo_cliente, $contenido);

					$mensaje_masivo 	= $contenido;
			        
			        //incluir aqui el mensaje

			        $message 	= Swift_Message::newInstance();
			        
			        $message->setSubject($titulo);
			        $message->setFrom($de);
			        $arregloTo 	= explode(',',$para);
			        
			        if(!comprobar_email($arregloTo[0])&&($arregloTo[0]!=""))
			        {
			        	//mail incorrecto
			        	mail($de,"correo incorrecto",$arregloTo[0]);
			        }
			        if(!comprobar_email($arregloTo[1])&&($arregloTo[1]!=""))
			        {
			        	//mail incorrecto
			        	mail($de,"correo incorrecto",$arregloTo[1]);
			        }
			        if(!comprobar_email($arregloTo[2])&&($arregloTo[2]!=""))
			        {
			        	//mail incorrecto
			        	mail($de,"correo incorrecto",$arregloTo[2]);
			        }
			        if(!comprobar_email($arregloTo[3])&&($arregloTo[3]!=""))
			        {
			        	//mail incorrecto
			        	mail($de,"correo incorrecto",$arregloTo[3]);
			        }
			        
			        
			        
			        
			        if(($arregloTo[0]!="")&&($arregloTo[1]=="")&&($arregloTo[2]=="")&&($arregloTo[3]==""))
			        {
			        	$message->setTo(array($arregloTo[0] => $arregloTo[0]));
			        }
			        if(($arregloTo[0]!="")&&($arregloTo[1]!="")&&($arregloTo[2]=="")&&($arregloTo[3]==""))
			        {
			        	$message->setTo(array($arregloTo[0] , $arregloTo[1] => $arregloTo[0]));
			        }
			        if(($arregloTo[0]!="")&&($arregloTo[1]!="")&&($arregloTo[2]!="")&&($arregloTo[3]==""))
			        {
			        	$message->setTo(array($arregloTo[0] , $arregloTo[1], $arregloTo[2] => $arregloTo[0]));
			        }
			        if(($arregloTo[0]!="")&&($arregloTo[1]!="")&&($arregloTo[2]!="")&&($arregloTo[3]!=""))
			        {
			        	$message->setTo(array($arregloTo[0] , $arregloTo[1], $arregloTo[2],$arregloTo[3] => $arregloTo[0]));
			        }
			        
			        $message->setBody($mensaje_masivo, 'text/html');
			        // convertir a texto plano.
			        $texto_mensaje 	= strip_tags($mensaje_masivo); 
			        $message->addPart($texto_mensaje, 'text/plain');
			        
			        $result 	= $mailer->send($message);
			        
			        if($result == 1)
			        {
			            echo "<font style='color:#00AA00;'>Se envio mail a " . $para . "</font><br />";
			            $contador_envios_correctos++;
			        }
			        else
			        {
			            echo "<font style='color:#FF0000;'>No se pudo enviar mail a " . $para . "</font><br />";
			            $contador_envios_incorrectos++;
			        }

			        $cantidad_envios++;
			    } 
			    catch(Exception $e) 
			    {
			        echo '<font style="$FF0000;">Error en el envio: ' . $e->getMessage() . "</font><br />";
			    }
			}
			$contador_clientes_con_mail++;
		}
		else
		{
			echo "<font style='color:#FF2FE1;'>Este cliente no tiene ningun correo</font><br />";
			$contador_clientes_sin_mail++;
		}
		$i++;
	}

	$_SESSION['clientes_con_mail'] = $contador_clientes_con_mail;
	$_SESSION['clientes_sin_mail'] = $contador_clientes_sin_mail;
	$_SESSION['envios_correctos'] = $contador_envios_correctos;
	$_SESSION['envios_incorrectos'] = $contador_envios_incorrectos;

	$array_clientes = array_slice($array_clientes, $i);

	if(count($array_clientes) > 0)
	{
		$_SESSION['segmentados'] = $array_clientes;	
		$cantidad_clientes = count($array_clientes);
		echo "<strong>AUN QUEDAN " . $cantidad_clientes . " CLIENTES PARA ENVIAR POR MAIL.</strong><br/>";
?>
		<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" id="form_segmentar" name="form_segmentar">
			TITULO: <input type="text" name="titulo" value="<?php echo strtoupper($_POST['titulo']); ?>"><br/>
			COMENTARIO: <textarea name="comentario"><?php echo strtoupper($_POST['comentario']); ?></textarea>
			<input type="submit" value="Enviar a los Restantes" name="enviar" />
		</form>
<?php
	}
	else
	{
?>
		<p/><strong>SE TERMINO EL ENVIO DE CORREOS</strong><p/>
		Se realizo el envio a <?php echo $_SESSION['clientes_total']; ?> clientes.<br/>
		<?php echo $_SESSION['clientes_sin_mail']; ?>&nbsp;clientes no tienen correo.<br/>
		<?php echo $_SESSION['clientes_con_mail']; ?>&nbsp;clientes si tienen al menos un correo registrado.<br/>
		&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;<?php echo $_SESSION['envios_correctos']; ?>&nbsp;mensajes se enviaron correctamente.<br/>
		&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;<?php echo $_SESSION['envios_incorrectos']; ?>&nbsp;mensajes no se lograron enviar.<br/>
<?php	
		$headers  		= 'MIME-Version: 1.0' . "\r\n";
		$headers 		.= 'Content-type: text/html; charset=utf-8' . "\r\n";    
		$headers 		.= "From: VIACONTOURS <vacaciones@viacontours.com>" . "\r\n";

		$contenido_agente =  $contenido . '<center>
												<table>
													<tr>
														<td style="background-color:#FFFFFF; border:1px solid #d1d1d1; padding:5px;" >
															<table>
																<tr>
																	<td style="padding:10px; background-color:#e1e1e1;">
																		<table style="font-family:Lucida Sans, Sans serif; width:700px; font-size:10pt; color:#333333;">
																			<tr>
																				<td style="font-family:Lucida Sans, Sans serif;">
																					SE ENVIO CORREOS A ' . $_SESSION['clientes_total'] . ' CLIENTES DE LOS CUALES :
																				</td>
																			</tr>
																			<tr>
																				<td style="font-family:Lucida Sans, Sans serif; padding-left:20px;">
																					* ' . $_SESSION['clientes_con_mail'] . ' TIENEN AL MENOS UN CORREO REGISTRADO.<br/>
																					* ' . $_SESSION['clientes_sin_mail'] . ' NO TIENEN CORREO.
																				</td>
																			</tr>
																			<tr>
																				<td style="font-family:Lucida Sans, Sans serif;">
																					EN EL ENVIO DE MENSAJES POR MAIL ..
																				</td>
																			</tr>
																			<tr>
																				<td style="font-family:Lucida Sans, Sans serif; padding-left:20px;">
																					* ' . $_SESSION['envios_correctos'] . ' SE ENVIARON CORRECTAMENTE.<br/>
																					* ' . $_SESSION['envios_incorrectos'] . ' NO LOGRARON ENVIAR.
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>			
														</td>
													</tr>
												</table>
											</center>';

		$enviado 	= mail('vacaciones@viacontours.com',$titulo,$contenido_agente, $headers);
		if($enviado !== false)
			echo "<p/>SE ENVIO UN MAIL CON EL ULTIMO MENSAJE, A vacaciones@viacontours.com.<p/>";

		/*
		* GUARDAMOS EN LA BD EL ENVIO REALIZADO
		*/

		$mail_idusuarios 	= $_SESSION['idusuarios'];
		$mail_tipo_envio 	= 'P';
		$mail_contenido 	= $contenido_agente;

		$mail_observacion   = '';
		$a_segmentacion 	= $_SESSION['criterios'];
		for ($i=0; $i < count($a_segmentacion); $i++) 
		{
			$mail_observacion .= $a_segmentacion[$i] . "\n";
		}

		$mail_correctos 	= $_SESSION['envios_correctos'];
		$mail_incorrectos 	= $_SESSION['envios_incorrectos'];
		$mail_con_mail     	= $_SESSION['clientes_con_mail'];
		$mail_sin_mail 		= $_SESSION['clientes_sin_mail'];

		$guardar_envio_de_correo 	= pqt_envio_masivo::insertar_nuevo_envio_masivo($mail_idusuarios, $mail_tipo_envio, $mail_contenido, $mail_observacion, $mail_correctos, $mail_incorrectos, $mail_con_mail, $mail_sin_mail);
		$idenvio_masivo 			= mysql_insert_id();

		$cantidad_pqt = count($paquetes_seleccionados);
		for ($i=0; $i < $cantidad_pqt; $i++) 
		{ 
			$paquete 		= $paquetes_seleccionados[$i];
			$mail_codigo 	= $paquete['idpublicacion'];

			$guardar_envio_publicacion = pqt_envio_masivo::insertar_envio_has_publicacion($idenvio_masivo, $mail_codigo);
		}

		unset($_SESSION['evento']);
	    unset($_SESSION['title_ev']);
	    unset($_SESSION['sigla_ev']);
	    unset($_SESSION['mis_imagenes']);
	    unset($_SESSION['segmentados']);
	    unset($_SESSION['paquetes_segmentados']);
	    unset($_SESSION['palabras_clave']);
	    unset($_SESSION['criterios']);
	    unset($_SESSION['clientes_total']);
	    unset($_SESSION['clientes_con_mail']);
	    unset($_SESSION['clientes_sin_mail']);
	    unset($_SESSION['envios_correctos']);
	    unset($_SESSION['envios_incorrectos']);
	    unset($_SESSION['paquetes_temporal'])
?>
	<a href="index.php">Volver al inicio</a>
<?php
	}
?>