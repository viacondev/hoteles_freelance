<?php
	ini_set('memory_limit', '-1');
	set_time_limit(0);
	include 'header.php';
	ini_set('display_errors', 1);
	include('../entidad/clientes.php');
	include('../entidad/imagen.php');
	include('../entidad/publicacion.php');
	include('../control/c_segmentar_clientes.php');
	
	$c_segmentar_clientes = new c_segmentar_clientes;	

	if(isset($_GET['p']))
	{
		$_SESSION['promocion'] 		= $_GET['p'];
		$datos_promocion 			= publicacion::obtener_info_paquete($_SESSION['promocion']);
		$datos_promocion 			= $datos_promocion[0];
		$_SESSION['datos_promo']	= $datos_promocion;
		$_SESSION['segmentados'] 	= array();
		$_SESSION['criterios'] 		= array();
	}

	if(!isset($_SESSION['segmentados']))
	{
		$_SESSION['segmentados'] 	= array();
		$_SESSION['criterios'] 		= array();	
	}

	/*
	* REALIZAMOS LA BUSQUEDA EN CASO DE NECESITARSE
	*/
	if(isset($_POST['rubro']))
	{
		$clientes_seleccionados 	= $_SESSION['segmentados'];
		$clientes_segmentados = $c_segmentar_clientes->segmentar_clientes($_POST['rubro'], $_POST['profesion'], $_POST['especialidad']);
		if(count($clientes_seleccionados) > 0)
		{
			if(count($clientes_segmentados) > 0 && $clientes_segmentados !== false)
				$nueva_segmentacion 	= array_merge($clientes_seleccionados, $clientes_segmentados);
			else
				$nueva_segmentacion 	= $clientes_seleccionados;
		}
		else
		{
			$nueva_segmentacion 	= $clientes_segmentados;
		}
		
		$resultado_actual 	= array_map("unserialize", array_unique(array_map("serialize", $nueva_segmentacion)));

		$_SESSION['segmentados'] 	= $resultado_actual;

		$criterios_segmentacion 	= $_SESSION['criterios'];
		if(isset($_POST['rubro']) && $_POST['rubro'] != 0)
		{
			$rubro = $c_segmentar_clientes->obtener_rubro($_POST['rubro']);
			$criterios_segmentacion[] = 'SEGMENTADO POR RUBRO (' . $rubro['nombre_rubro'] . ')';
		}
		if(isset($_POST['especialidad']) && $_POST['especialidad'] != 0)
		{
			$especialidad = $c_segmentar_clientes->obtener_especialidad($_POST['especialidad']);
			$criterios_segmentacion[] = 'SEGMENTADO POR ESPECIALIDAD (' . $especialidad['nombre_especialidad'] . ')';
		}
		if(isset($_POST['profesion']) && $_POST['profesion'] != 0)
		{
			$profesion = $c_segmentar_clientes->obtener_profesion($_POST['profesion']);
			$criterios_segmentacion[] = 'SEGMENTADO POR PROFESION (' . $profesion['nombre_profesion'] . ')';
		}

		$_SESSION['criterios'] = $criterios_segmentacion;
	}
	/*
	* SE VERIFICA SI SE NECESITA VACIAR LOS CLIENTES Y CRITERIOS
	*/
	if(isset($_POST['new_seg']))
	{
		$_SESSION['segmentados'] 	= array();
		$_SESSION['criterios'] 		= array();	
	}

	/*
	* VERIFICAMOS SI SE NECESITA QUITAR ALGUN CLIENTE
	*/
	if(isset($_POST['quitar_clientes']))
	{
		$clientes_seleccionados 	= $_SESSION['segmentados'];
		$cant_cli = $_POST['cant_select'];
		for ($i=0; $i < $cant_cli; $i++) 
		{ 
			if(isset($_POST['chk_cliente_' . $i]))
			{
				unset($clientes_seleccionados[$i]);
			}
		}
		$clientes_seleccionados = array_values($clientes_seleccionados);
		$_SESSION['segmentados'] = $clientes_seleccionados;
	}
	/*
	* VERIFICAMOS SI AGREGA ALGUN/OS CLIENTE/S
	*/
	if(isset($_POST['add_cliente']))
	{
		$clientes_seleccionados = $_SESSION['segmentados'];
		$cant_cli = $_POST['cant_clientes'];
		for ($i=0; $i < $cant_cli; $i++) 
		{ 
			if(isset($_POST['chk_cliente_' . $i]))
			{
				$client = $c_segmentar_clientes->buscar_cliente($_POST['chk_cliente_' . $i]);
				if($client !== false)
					$clientes_seleccionados[] 	= $client;			
			}
		}
		$clientes_seleccionados = array_values($clientes_seleccionados);
		$_SESSION['segmentados'] = $clientes_seleccionados;	
	}

?>
<center><h2><?php echo $_SESSION['title_promo']; ?></h2></center>

<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" id="form_segmentar" name="form_segmentar">
	<div class="four columns">
		<label>Rubro</label>
		<select id="rubro" name="rubro">
			<option value="0">Ninguno</option>
			<?php
				$rubros = $c_segmentar_clientes->obtener_rubros();
				for ($i=0; $i < count($rubros); $i++) 
				{ 
			?>
					<option value="<?php echo $rubros[$i]['idrubro']; ?>"><?php echo strtoupper($rubros[$i]['nombre_rubro']); ?></option>
			<?php
				}
			?>
		</select>
	</div>
	<div class="four columns">
		<label>Profesion</label>
		<select id="profesion" name="profesion" onchange="Mostrar_Especialidades();">
			<option value="0">Ninguno</option>
			<?php
				$profesiones = $c_segmentar_clientes->obtener_profesiones();
				for ($i=0; $i < count($profesiones); $i++) 
				{ 
			?>
					<option value="<?php echo $profesiones[$i]['idprofesiones']; ?>"><?php echo strtoupper($profesiones[$i]['nombre_profesion']); ?></option>
			<?php
				}
			?>
		</select>
	</div>
	<div class="four columns">
		<label>Especialidad</label>
		<select id="especialidad" name="especialidad" id="combo_especialidad">
			<option value="0">Ninguno</option>
		</select>
	</div>
</form>
<!--<div class="four columns">
	<label>Preferencias</label>
	<select id="preferencia">
		<option>Todos</option>
	</select>
</div>-->
<div class="four columns">
	<a class="mylinkleft" onclick="segmentar_clientes_por_criterios();">Segmentar y Añadir</a>
	<br/>
	<a class="mylinkleft" onclick="mostrar_busqueda_ind_cliente();">Agregar Cliente Individual</a>
</div>

<form class="sixteen columns" id="clientes" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
	<table>
		<?php
			$criterios_segmentacion = $_SESSION['criterios'];
			for ($i=0; $i < count($criterios_segmentacion); $i++) 
			{ 
		?>
			<tr>
				<td><?php echo $criterios_segmentacion[$i]; ?></td>
			</tr>
		<?
			}
		?>
	</table>
	<center><div class="estilo_tabla">
	<table id="lista_clientes">
		<tr>
			<th></th>
			<th>CLIENTES EN LISTA</th>
			<th>SELEC</th>
		</tr>
	<?php
		$clientes_seleccionados = $_SESSION['segmentados'];
		
		for ($i=0; $i < count($clientes_seleccionados); $i++) 
		{ 
	?>
		<tr>
			<td><?php echo ($i+1); ?></td>
			<td><?php echo strtoupper($clientes_seleccionados[$i]['trato_cliente'] . ' ' . $clientes_seleccionados[$i]['nombre_cliente'] . ' ' . $clientes_seleccionados[$i]['apellido_cliente']); ?></td>
			<td>
				<input type="checkbox" value="<?php echo $clientes_seleccionados[$i]['idclientes']; ?>" name="chk_cliente_<?php echo $i; ?>" />
				<input type="hidden" value="<?php echo $clientes_seleccionados[$i]['idclientes']; ?>" name="cliente_enviar_<?php echo $i; ?>" />
			</td>
		</tr>
	<?php
		}
	?>
	</table>
	<hr/>
	<input type="submit" value="Quitar Seleccionados" name="quitar_clientes" />
	<input type="submit" value="Reiniciar Segmentación" name="new_seg" />
	<input type="hidden" value="<?php echo count($clientes_seleccionados); ?>" name="cant_select">
	</div></center>
</form>

<form class="sixteen columns" method="post" action="cargar_e_mails_para_envio_promocion.php">
	<input type="checkbox" id="todos_clientes" name="todos_clientes" onchange="listado();" />Enviar a Todos los Clientes
	<hr/>
	<center><input type="submit" value="Enviar Mail a Listado" name="enviar" /></center>
</form>

<script type="text/javascript">
	function listado()
	{
		if($('#todos_clientes').is(':checked'))
		{
			$('#lista_clientes').hide();	
		}
		else
		{
			$('#lista_clientes').show();
		}
	}
</script>

<?php include('footer.php'); ?>


