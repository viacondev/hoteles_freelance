<?php
	include 'header.php';
	include('../entidad/tipo_paquete.php');
	include('../entidad/temporada.php');
	include('../entidad/publicacion.php');
	include('../entidad/paquete.php');
	include('../entidad/destino.php');
	include('../entidad/usuarios_paquetes.php');
	include('../control/c_buscar_paquete.php');

	$c_buscar_paquete = new c_buscar_paquete;
	$array_palabras = $c_buscar_paquete->cargar_palabras_clave();

?>
<script type="text/javascript">
	function Mostrar_Ocultar()
    {
        if($("input[name='buscar_por']:checked").val() == 1)
        {
            $("#busqueda_codigo").hide();
            $("#otros").show();
        }    
        else
        {
            $("#busqueda_codigo").show();
            $("#otros").hide();
        }
    }	
    $(function(){
        var autocompletar = new Array();
        <?php 
            for($p = 0;$p < count($array_palabras); $p++)
            { 
        ?>
                autocompletar.push('<?php echo strtoupper($array_palabras[$p]); ?>');
        <?php 
            } 
        ?>
         $("#busqueda").autocomplete({ 
           source: autocompletar,
           minLength: 3
        });

    });	
</script>
<div class="eigth columns">
	<h3>BUSCAR PAQUETES</h3>
</div>
<div class="eigth columns">
	<img src="images/travel.png" style="height:40px; float:left; display:inline;" />	
</div>
<form class="sixteen columns" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#F1F1F1; padding-top:10px;" method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <div class="columns four">
        <input type="checkbox" name="anulados" <?php if(isset($_GET['anulados'])) echo "Checked"; ?>/>Buscar entre los anulados
    </div>
    <div class="clear_mayor"></div>
    <div class="fifteen columns">
    	<input type="text" name="busqueda" id="busqueda" value="<?php echo $_GET['busqueda']; ?>" style="width:90%; padding:5px;" placeholder="Introduzca Paquete, Temporada o Destino" />
    </div>
    <div class="clear"></div>
    <div class="fifteen columns">
    	<input type="submit" name="buscar_paquete" value="Buscar" style="font-size:15pt; padding: 5px 15px; margin: 10px 20px;" />
    </div>
</form>
<div class="sixteen columns" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#F1F1F1; <?php if(!isset($_GET["buscar_paquete"])) { ?>display:none;<?php } ?>" >
	
	<?php
		if(isset($_GET["buscar_paquete"]))
		{
			if(isset($_GET['anulados']))
			{
				$estado = '0';
				$estado_descripcion = 'Anulados';
				$color = '#ff3300';
			}
			else
			{
				$estado = '1';
				$estado_descripcion = 'Vigentes';
				$color = '#10689b';
			}
			
			$paquetes 			= $c_buscar_paquete->buscar_paquetes_por_palabra_clave($_GET['busqueda'], $estado);
			$usuarios_paquetes 	= usuarios_paquetes::enviar_datos_usuario($_SESSION["idusuarios"]);
			$autorizado 		= 0;
			if(count($usuarios_paquetes) > 0)
			{
				if($usuarios_paquetes[0]["acceso"] == 'E')
				{
					$autorizado = 1;
				}
			}
	?>
			<div class="columns fifteen" style="color:<?php echo $color; ?>; font-weight:bold;">
				B&Uacute;SQUEDA DE PAQUETES: Se encontraron <?php echo count($paquetes); ?> resultados entre los paquetes <?php echo $estado_descripcion; ?>.
			</div>
	<?php
			if(count($paquetes) > 0)
			{
	?>
				<center><div class="estilo_tabla">
				<table>
					<tr>
						<th></th>
						<th>PAQUETE</th>
						<th>SALIDA</th>
						<th>RETORNO</th>
						<th>CREACION</th>
						<th>CADUCIDAD</th>
						<th>COT</th>
						<th>MAILS</th>
						<th>EST</th>
						<th>ACCION</th>
					</tr>
	<?php
				for ($i=0; $i < count($paquetes); $i++) 
				{ 
					$paquete = $paquetes[$i];
	?>
					<tr id="publicacion_<?php echo $paquete['idpublicacion']; ?>">
						<td><strong><?php echo ($i+1); ?></strong></td>
						<td><?php echo strtoupper($paquete['titulo']); ?></td>
						<td>
							<?php 
								$fecha_salida = strtotime($paquete["fecha_salida"]);
								if($fecha_salida)
									$fecha_salida = date('d/M/Y', $fecha_salida);
								else
									$fecha_salida = $paquete["fecha_salida"];
								echo $fecha_salida;
							?>	
						</td>
						<td>
							<?php 
								$fecha_retorno = strtotime($paquete["fecha_retorno"]);
								if($fecha_retorno)
									$fecha_retorno = date('d/M/Y', $fecha_retorno);
								else
									$fecha_retorno = $paquete["fecha_retorno"];
								echo $fecha_retorno;
							?>	
						</td>
						<td><?php echo strtoupper(date('d/M/Y', strtotime($paquete['fecha_publicacion'] . ' 00:00:00')) . '(' . substr($paquete['username'], 0, 6)) . ')'; ?></td>
						<td><?php echo strtoupper(date('d/M/Y', strtotime($paquete['fecha_caducidad'] . ' 00:00:00'))); ?></td>
						<td>
							<a href="mostrar_cotizaciones_por_generico.php?p=<?php echo $paquete['idpublicacion']; ?>" target="_blank">
								<?php
									echo $c_buscar_paquete->obtener_cantidad_de_cotizaciones($paquete['idpublicacion']);
								?>
							</a>
						</td>
						<td>
							<a onclick="Mostrar_Envios_Realizados(<?php echo $paquete['idpublicacion']; ?>);" style="cursor:pointer;">
								<?php
									echo $c_buscar_paquete->obtener_cantidad_de_envios_masivos($paquete['idpublicacion']);
								?>
							</a>
						</td>
						<td>
							<?php
								if($paquete['estado_paquete'] == 0)
								{
								?>
									<font style="color:#FF0000;">A SOL</font>
								<?php
								}
								else if($paquete['estado_paquete'] == 1)
								{
								?>
									ARM
								<?php
								}
							?>
						</td>
						<td>
							<a href="visualizar_paquete.php?idpaquete=<?php echo $paquete['idpublicacion']; ?>"><img src="images/icon-view.png" class="icon" title="Visualizar" /></a>
	<?php 
						if($autorizado == 1)
						{
	?>
							<a href="editar_paquete.php?idpaquete=<?php echo $paquete['idpublicacion']; ?>"><img src="images/pencil.png" class="icon" title="Editar" /></a>
							<img src="images/cross.png" class="icon" title="Anular" onclick="anular_publicacion(<?php echo $paquete['idpublicacion']; ?>);" />
	<?php
						}
	?>
						</td>
					</tr>
					</div>
	<?php
				}
	?>
				</table>
				</div></center>
	<?php
			}
		}
	?>
</div>
<?php include("footer.php"); ?>