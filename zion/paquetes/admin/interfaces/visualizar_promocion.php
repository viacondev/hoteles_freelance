<?php
	include 'header.php';
	include('../entidad/publicacion.php');
	include('../entidad/paquete.php');
	include('../entidad/imagen.php');
	include('../entidad/archivo_adjunto.php');
	include('../entidad/operadora.php');
	include('../control/c_mostrar_paquete.php');

	$c_mostrar_paquete 	= new c_mostrar_paquete;

	$idpublicacion = $_GET['idpromo'];

	$paquete 	= publicacion::obtener_info_paquete($idpublicacion);
	$paquete 	= $paquete[0];

	$imagenes 	= publicacion::obtener_imagenes_por_publicacion($idpublicacion);

?>
<?php
	if($paquete['solo_imagen'] == 'S')
	{
	?>
	<div class="columns fifteen">
		<div class="columns two" style="font-weight:bold; color:#10689b;">
			CODIGO
		</div>
		<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
		<div class="columns four">
			<?php echo $paquete["idpublicacion"]; ?>
		</div>
		<img src="<?php echo $ruta_imagen . $imagenes[0]['nombre']; ?>" />
	</div>	
<?php
	}
	else
	{
	?>
		<div class="columns sixteen">
			<?php
				if($paquete["imagen_primera"] != '')
				{
					$imagen = $paquete["imagen_primera"];
				}
				else
				{
					$imagen = 'imagen_generico.JPG';
				}
			?>
			<div class="columns seven">
				<img src="<?php echo $ruta_imagen . $imagen; ?>" style="width:100%;" />
			</div>
			<div class="columns eight" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">	
				<h4 style="padding:10px;"><?php echo strtoupper($paquete['titulo']); ?></h4>
				<div class="columns two" style="font-weight:bold; color:#10689b;">
					CODIGO
				</div>
				<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
				<div class="columns four">
					<?php echo $paquete["idpublicacion"]; ?>
				</div>
				<div class="clear_mayor"></div>
				<div class="columns two" style="font-weight:bold; color:#10689b;">
					SALIDA
				</div>
				<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
				<div class="columns four">
					<?php 
						$fecha_salida = strtotime($paquete["fecha_salida"]);
						if($fecha_salida)
							$fecha_salida = date('d/m/Y', $fecha_salida);
						else
							$fecha_salida = $paquete["fecha_salida"];
						echo $fecha_salida;
					?>
				</div>
				<div class="columns two" style="font-weight:bold; color:#10689b;">
					RETORNO
				</div>
				<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
				<div class="columns four">
					<?php 
						$fecha_retorno = strtotime($paquete["fecha_retorno"]);
						if($fecha_retorno)
							$fecha_retorno = date('d/m/Y', $fecha_retorno);
						else
							$fecha_retorno = $paquete["fecha_retorno"];
						echo $fecha_retorno;
					?>
				</div>
				<div class="clear_mayor"></div>
				<div class="columns two" style="font-weight:bold; color:#10689b;">
					USUARIO
				</div>
				<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
				<div class="columns four">
					<?php echo $paquete["nombre_usuario"]; ?>
				</div>
				<div class="clear_mayor"></div>
				<div class="columns two" style="font-weight:bold; color:#10689b;">
					VALIDO HASTA
				</div>
				<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
				<div class="columns four">
					<?php echo date('d/m/Y',strtotime($paquete["fecha_caducidad"])); ?>
				</div>			
			</div>
			
		</div>

<?php
	}
?>

<div class="clear_mayor"></div>

<div class="columns sixteen" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
	<div class="columns fourteen" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">DESCRPCION DE LA PROMOCION</div>
	<div class="clear"></div>
	<ul class="lista_descripcion">
		<?php
			$datos_agente = explode("\n", $paquete['descr_publicacion']);
			for($i=0; $i<count($datos_agente); $i++)
			{
				if($datos_agente[$i] != "")
				{
		?>
					<li>
						<a><?php echo $datos_agente[$i]; ?></a>
					</li>
		<?php
				}
			}
		?>		
	</ul>
</div>

<div class="clear_mayor"></div>

<div class="columns sixteen" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
	<div class="columns five" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">ARCHIVOS ADJUNTOS</div>
		<div class="clear"></div>
		<ul class="lista_descripcion" id="lista_datos_agente">
	<?php 
		$archivos = publicacion::obtener_archivos_por_publicacion($idpublicacion);
		if(count($archivos) > 0) 
		{
			for($i=0; $i<count($archivos); $i++)
			{
				$archivo = $archivos[$i];
	?>
			<li>
				<a>
					<strong><?php echo $archivo['nombre']; ?> - </strong>
					<?php echo $archivo["descripcion"]; ?>
					<a href="<?php echo $ruta_adjuntos . $archivo['nombre']; ?>" target="_blank"><input type="button" value="Abrir Archivo"></a>
				</a>
	        </li>
	<?php
			}
		}
		else
		{
	?>
			<li><a>No se a&ntilde;adio ning&uacute;n archivo.</a></li>
	<?php
		}
	?>	
		</ul>
</div>

<div class="clear_mayor"></div>

<?php
	if($paquete['datos_agente'] != "")
	{
?>
	<div class="columns sixteen" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
		<div class="columns five" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">DATOS DE AGENTE</div>
		<div class="clear"></div>
		<ul class="lista_descripcion" id="lista_datos_agente">
			<?php
				$datos_agente = explode("\n", $paquete['datos_agente']);
				for($i=0; $i<count($datos_agente); $i++)
				{
					if($datos_agente[$i] != "")
					{
			?>
						<li>
							<a><?php echo $datos_agente[$i]; ?></a>
						</li>
			<?php
					}
				}
			?>		
		</ul>
	</div>
<?php
	}
?>

<div class="clear_mayor"></div>

<div class="columns sixteen" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
	<div class="columns five" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">OPERADORA</div>
	<div class="clear"></div>
	<?php
	$operadoras = publicacion::obtener_operadoras_por_publicacion($idpublicacion);

	if(count($operadoras) > 0)
	{
		for($i = 0; $i < count($operadoras); $i++)
		{
			$operadora = $operadoras[$i];
		?>
			<div class="seven columns">
				&nbsp;<strong><?php echo strtoupper($operadora["nombre_operadora"]); ?></strong>
				<ul class="lista_descripcion">
					<li><a>&nbsp;<?php echo strtoupper($operadora["correos"]); ?></a></li>
					<li><a>&nbsp;<?php echo strtoupper($operadora["telefonos"]); ?></a></li>
					<li><a>&nbsp;<?php echo strtoupper($operadora["observaciones"]); ?></a></li>
				</ul>
			</div>
		<?php
		}
	}
	else
	{
	?>
		<ul class="lista_descripcion"><li><a>ESTE PAQUETE NO TIENE NINGUNA OPERADORA</a></li></ul>
	<?php
	}
	?>
</div>

<div class="clear_mayor"></div>
<?php $usuario = $_SESSION['idusuarios']; ?>

<a href="segmentar_clientes_promocion.php?p=<?php echo $idpublicacion; ?>">
	<input type="button" value="Enviar Por Mail" style="float:right;" />
</a>

<input type="button" value="Imprimir" onclick="Imprimir();" style="float:right;" />

<a href="editar_promocion.php?idpromo=<?php echo $idpublicacion; ?>">
	<input type="button" value="Editar" style="float:right;" />
</a>

<input type="button" value="Nueva Cotizaci&oacute;n" onclick="mostrar_url_popup('crear_cotizacion_paquete.php?idpaquete=<?php echo $idpublicacion; ?>&idusuarios=<?php echo $usuario; ?>&titulo=<?php echo $paquete["titulo"]; ?>' , 450);" style="float:right;">

<script type="text/javascript">
	$(function() {
		$('.vista_pagina').html('VISTA DE UN PAQUETE GENERICO');
	});
</script>

<?php include("iu_vista_promocion_impresion.php"); ?>

<?php include("footer.php"); ?>