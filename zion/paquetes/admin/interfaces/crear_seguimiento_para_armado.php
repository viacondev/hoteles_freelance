<?php
	include '../BD/controladoraBD.php';
	include '../control/c_nuevo_seguimiento.php';
	include '../entidad/usuarios.php';

	$usuario = $_GET['u'];
	$origen  = $_GET['o'];
	$codigo  = $_GET['c'];

	$c_nuevo_seguimiento = new c_nuevo_seguimiento;
?>
<link href="lib/tinybox2/style.css" rel="stylesheet" />
<div class="container" style="height:500px; overflow:auto;" >
	<h3>SEGUIMIENTO INICIAL PARA EL ARMADO DEL PAQUETE</h3>
	<div class="three columns">USUARIO ASIGNADO:</div>
	<div class="four columns">
		<select id="usuario">
			<?php
				$usuarios = $c_nuevo_seguimiento->obtener_usuarios();
				for ($i=0; $i < count($usuarios); $i++) 
				{ 
			?>
					<option value="<?php echo $usuarios[$i]['idusuarios']; ?>" <?php if($usuario == $usuarios[$i]['idusuarios']) echo 'selected'; ?> >
						<?php echo strtoupper($usuarios[$i]['nombre_usuario']); ?>
					</option>
			<?php
				}
			?>
		</select>
	</div>
	<div class="three columns">FECHA LIMITE:</div>
	<div class="four columns">
		<input type="text" placeholder="dd/mm/aaaa" id="fecha_limite" />
	</div>
	<div class="clear"></div>
	<div class="three columns">COMENTARIO:</div>
	<div class="four columns">
		<textarea id="comentario"></textarea>
	</div>
	<div class="clear"></div>
	<div class="three columns">
		<input type="button" value="Crear Seguimiento" onclick="registrar_seguimiento_inicial('<?php echo $origen; ?>', <?php echo $usuario; ?>, <?php echo $codigo; ?>);">
	</div>
</div>