<?php
include("header.php");
include("../control/c_buscar_cotizaciones_paquete.php");
include("../entidad/paquete_cotizado.php");
include("../entidad/destino.php");
include("../entidad/temporada.php");
include("../entidad/paquete.php");
include("../entidad/tipo_paquete.php");
include('../entidad/usuarios_paquetes.php');

$c_buscar_cotizaciones_paquete = new c_buscar_cotizaciones_paquete;
$array_palabras = $c_buscar_cotizaciones_paquete->cargar_palabras_clave();
?>
<script type="text/javascript">
    $(function(){
        var autocompletar = new Array();
        <?php 
            for($p = 0;$p < count($array_palabras); $p++)
            { 
        ?>
                autocompletar.push('<?php echo $array_palabras[$p]; ?>');
        <?php 
            } 
        ?>
         $("#paquete").autocomplete({ 
           source: autocompletar
        });

    });	
</script>
<div class="eight columns">
	<h3>BUSCAR PAQUETES COTIZADOS</h3>
</div>
<div class="seven columns">
	<img src="images/travel.png" style="height:40px; float:left; display:inline;" />	
</div>
<form class="fifteen columns" style="background-color:#f1f1f1; padding:10px;" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">

	<div class="three columns">

		<input type="radio" value="1" name="estado_cotizacion" <?php if($_POST['estado_cotizacion'] == 'C' || !isset($_POST['estado_cotizacion'])) { ?> checked <?php } ?> />COTIZACION

	</div>

	<div class="three columns">

		<input type="radio" value="2" name="estado_cotizacion" <?php if($_POST['estado_cotizacion'] == 'V') { ?> checked <?php } ?> />VENDIDOS
		
	</div>

	<div class="three columns">

		<input type="radio" value="0" name="estado_cotizacion" <?php if($_POST['estado_cotizacion'] == 'A') { ?> checked <?php } ?> />ANULADOS
		
	</div>

	<div class="three columns">

		<input type="radio" value="3" name="estado_cotizacion" <?php if($_POST['estado_cotizacion'] == 'T') { ?> checked <?php } ?> />TODOS
		
	</div>

	<div class="clear_mayor"></div>

	<div class="three columns">
		FECHA DE COTIZACION
	</div>

	<div class="three columns">
		
		<input type="text" placeholder="dd/mm/aaaa" name="fecha_desde" id="fecha_desde" value="<?php echo $_POST['fecha_desde']; ?>" style="width:70%;" />

	</div>

	<div class="three columns">
		
		<input type="text" placeholder="dd/mm/aaaa" name="fecha_hasta" id="fecha_hasta" value="<?php echo $_POST['fecha_hasta']; ?>" style="width:70%;" />

	</div>

	<div class="clear_mayor"></div>

	<div class="three columns">
		CLIENTE
	</div>

	<div class="three columns">

		<input type="text" placeholder="Nombres" name="nombre_cliente" value="<?php echo $_POST['nombre_cliente']; ?>" />

	</div>

	<div class="three columns">

		<input type="text" placeholder="Apellidos" name="apellido_cliente" value="<?php echo $_POST['apellido_cliente']; ?>" />

	</div>

	<div class="clear_mayor"></div>

	<div class="three columns">
		PAQUETE		
	</div>

	<div class="ten columns">

		<input type="text" placeholder="Datos de Paquete" id="paquete" name="paquete" value="<?php echo $_POST['paquete']; ?>" style="width:100%;" />

	</div>

	<div class="clear"></div>

	

	<div class="one columns">
		
		<input type="submit" name="buscar_cotizacion" value="BUSCAR" />

	</div>

</form>

<div class="clear_mayor"></div>

<div class="fifteen columns" style="background-color:#f1f1f1; padding:10px;" >
	
	<h4>Resultados</h4>

	<div class="fifteen columns">
		
		<?php
			if(!isset($_POST['buscar_cotizacion']))
			{
		?>
				NO SE HA RELIZADO NINGUNA B&Uacute;SQUEDA
		<?php
			}
			else
			{
				$c_buscar_cotizacion = new c_buscar_cotizaciones_paquete;
				$cotizaciones = $c_buscar_cotizacion->buscar_paquetes_cotizados_por_palabra_clave($_POST['paquete'], $_POST['nombre_cliente'], $_POST['apellido_cliente'], $_POST['estado_cotizacion'], $_POST['fecha_desde'], $_POST['fecha_hasta']);
				if($cotizaciones !== false)
				{
					$usuarios_paquetes = usuarios_paquetes::enviar_datos_usuario($_SESSION["idusuarios"]);
					$autorizado = 0;
					if(count($usuarios_paquetes) > 0)
					{
						if($usuarios_paquetes[0]["acceso"] == 'E')
						{
							$autorizado = 1;
						}
					}
		?>
					<label style="color:#10689b; font-weight:bold;" >SE ENCONTRARON <?php echo count($cotizaciones); ?> COTIZACIONES</label>
					<center><div class="estilo_tabla">
					<table>
						<tr style="border:1px solid #aaaaaa;">
							<th></th>
							<th>PAQUETE</th>
							<th>CLIENTE</th>
							<th>CREACION</th>
							<th></th>
						</tr>
		<?php
					for ($i=0; $i < count($cotizaciones); $i++) 
					{ 
						$cotizacion = $cotizaciones[$i];
						$color 		= '#000000';
						switch ($cotizacion['estado_cotizacion']) 
						{
							case 'A':
								$color 	= '#FF3300';
								break;

							case 'V':
								$color 	= '#00AA00';
								break;
							
							default:
								# code...
								break;
						}
		?>
						<tr style="border:1px solid #aaaaaa; color:<?php echo $color; ?>;">
							<td><strong><?php echo ($i+1); ?></strong></td>
							<td><?php echo strtoupper($cotizacion['titulo']); ?></td>
							<td><?php echo strtoupper($cotizacion['nombre_cliente'] . ' ' . $cotizacion['apellido_cliente']); ?></td>
							<td><?php echo strtoupper(date('dMY H:i:s', strtotime($cotizacion['fecha_cotizacion']))) . '(' . strtoupper($cotizacion['nombre_usuario']) . ')'; ?></td>
							<td>
								<a href="ver_cotizacion_paquete.php?cotizacion=<?php echo $cotizacion['idpaquete_cotizado']; ?>"><img src="images/icon-view.png" class="icon" title="Visualizar" /></a>
		<?php 
							if($autorizado == 1)
							{
		?>
								<a href="editar_cotizacion_paquete.php?idpaquete=<?php echo $cotizacion['idpaquete_cotizado']; ?>"><img src="images/pencil.png" class="icon" title="Editar" /></a>
		<?php
							}
		?>
							</td>
						</tr>
		<?php
					}
		?>
					</table>
					</div></center>

					<div style="font-size:6pt; padding-left:10px; margin-top:0; padding-top:0;">
						<span style="background-color:#000000;">&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<span style="color:#000000;">COTIZACION</span>&nbsp;&nbsp;&nbsp;
						<span style="background-color:#FF3300;">&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<span style="color:#FF3300;">ANULADO</span>&nbsp;&nbsp;&nbsp;
						<span style="background-color:#00AA00;">&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<span style="color:#00AA00;">VENDIDO</span>&nbsp;&nbsp;&nbsp;
					</div>
		<?php
				}
				else
				{
		?>
					NO SE ENCONTR&Oacute; NINGUNA COTIZACI&Oacute;N
		<?php
				}
			}
		?>

	</div>

</div>

<?php include("footer.php"); ?>

<script type="text/javascript">
	$(function() {

  		crear_calendario_cumpleanios('fecha_desde');
  		crear_calendario_cumpleanios('fecha_hasta');

	});

	function crear_calendario_cumpleanios(id) {
	    anioActual = new Date().getFullYear();
	    anioInicio = anioActual - 90;
	    $("#" + id).datepicker({
	        showOn: "button",
	        buttonImage: "../images/calendar.jpg",
	        buttonImageOnly: true,
	        numberOfMonths: 3,
	        yearRange: anioInicio + ":" + anioActual,
	        constrainInput: false
	    });  
	    $("#" + id).datepicker("option", "dateFormat", 'dd/mm/yy');
	}
	
</script>