<div style="display:none;">
<?php

if($paquete['solo_imagen'] == 'S')
{
?>
    <div style="border:3px solid #000000; background:#ffffff; width:21cm; height:29.7cm;" class="myPrintArea" >
        <table style="width:21cm; font-family:Courier New;" >
            <tr style="border-bottom:1px solid #aaa;">
                <td>
                    <img src="images\logo.png" style="width:350px; height:70px;" />    
                </td>
                <td style="vertical-align:top; text-align:right;">
                    Calle Velasco Esq. Suarez de Figueroa<br/>
                    Telf 591-3-363610<br/>
                    Santa Cruz - Bolivia<br/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <?php 
                        for ($i=0; $i < count($imagenes); $i++) 
                        { 
                            $imagen = $imagenes[$i];
                    ?>
                    <img src="<?php echo $ruta_imagen . $imagen["directorio"]; ?>" style="width:100%;" />
                    <?php
                        }
                    ?>
                </td>
            </tr>
        </table>
    </div>
<?php
}
else
{
?>
    <div style="background:#ffffff; width:21cm; border:1px solid #AAA; padding-bottom:15px;" class="myPrintArea" >
        <table style="width:21cm; font-family:Courier New;" class="sin-borde">
            <tr style="border-bottom:1px solid #aaa;">
                <td>
                    <img src="images\logo.png" style="width:350px; height:70px;" />    
                </td>
                <td style="vertical-align:top; text-align:right;">
                    Calle Velasco Esq. Suarez de Figueroa<br/>
                    Telf 591-3-363610<br/>
                    Santa Cruz - Bolivia<br/>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="font-size: 25pt; color: #ff3300;font-weight: bold; text-align:center; padding: 10px 0px;">
                    <?php echo strtoupper($paquete["titulo"]); ?><br/>
                    <?php 
                        if($paquete["imagen_primera"] != '')
                        {
                            $imagen = $paquete["imagen_primera"];
                        }
                        else
                        {
                            $imagen = 'imagen_generico.JPG';
                        }
                    ?>
                    <img src="<?php echo $ruta_imagen . $imagen; ?>" style="width:170px; height:150px; border:3px solid #d0d0d0; margin:10px 10px;" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding: 20px 0px 0px 20px;">
                    <font style="font-size:12pt; font-weight:bold; color:#ff3300;">SALIDA DESDE&nbsp;:&nbsp;</font>
                    <font style="font-size:12pt; font-weight:bold; color:#000;">
                        <?php 
                            $fecha_salida = strtotime($paquete["fecha_salida"]);
                            if($fecha_salida)
                                $fecha_salida = date('d/m/Y', $fecha_salida);
                            else
                                $fecha_salida = $paquete["fecha_salida"];
                            if($fecha_salida != '')
                                echo $fecha_salida;
                            else
                                echo 'NO INGRESADO';
                        ?>
                    </font>
                    <font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font>
                    <font style="font-size:12pt; font-weight:bold; color:#ff3300;">RETORNO HASTA&nbsp;:&nbsp;</font>
                    <font style="font-size:12pt; font-weight:bold; color:#000;">
                        <?php 
                            $fecha_retorno = strtotime($paquete["fecha_retorno"]);
                            if($fecha_retorno)
                                $fecha_retorno = date('d/m/Y', $fecha_retorno);
                            else
                                $fecha_retorno = $paquete["fecha_retorno"];
                            if($fecha_retorno != '')
                                echo $fecha_retorno;
                            else
                                echo 'NO INGRESADO';
                        ?>
                    </font>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding: 20px 0px 0px 20px;">
                    <font style="font-size:13pt; font-weight:bold; color:#ff3300;">APROVECHE ESTA PROMOCION !!</font>
                    <ul style="list-style: disc inside; margin:10px 0px 0px 20px; line-height:1;">
                        <?php
                            $descripcion = explode("\n", $paquete['descr_publicacion']);
                            for($i=0; $i<count($descripcion); $i++)
                            {
                                if($descripcion[$i] != "")
                                {
                        ?>
                                    <li <?php if($descripcion[$i]{0} == '@') { ?> style="font-weight:bold; list-style:square inside; margin:0;" <?php } else { ?> style="font-size:7pt; margin-left:30px; margin:0;" <?php } ?> >
                                        <font <?php if($descripcion[$i]{0} != '@') {  ?> style="font-size:9pt; margin:0;" <?php } else { $descripcion[$i] = substr($descripcion[$i], 1) ; } ?> ><?php echo $descripcion[$i]; ?></font>
                                    </li>
                        <?php
                                }
                            }
                        ?>      
                    </ul>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding: 20px 0px 0px 20px;">
                    <font style="font-size:12pt; font-weight:bold; color:#ff3300;">TARIFA VALIDA HASTA&nbsp;:&nbsp;</font>
                    <font style="font-size:12pt; font-weight:bold; color:#000;">
                        <?php 
                            $fecha_caducidad = strtotime($paquete["fecha_retorno"]);
                            $fecha_menos_1   = strtotime('-1 day', $fecha_caducidad);
                            echo date('d/m/Y', $fecha_menos_1);
                        ?>
                    </font>
                </td>
            </tr>
        </table>
    </div>
    <?php
}
?>
</div>