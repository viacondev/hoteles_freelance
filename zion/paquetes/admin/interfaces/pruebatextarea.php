<?php
	//include("../BD/controladoraBD.php");
include('header.php');
	include("../entidad/pax.php");
	include("../control/c_mostrar_cotizacion.php");

	$idcotizacion = $_GET['cotizacion'];

	$paxs = c_mostrar_cotizacion::obtener_paxs($idcotizacion);
?>
	
	<table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios">
		<tr style="background-color:#10689b; color:#FFFFFF;">
			<th rowspan="2">NRO</th>
			<th rowspan="2">PASAJERO</th>
			<th colspan="4">PRECIOS EN USD</th>
		</tr>
		<tr style="background-color:#10689b; color:#FFFFFF;">
			<th>HOTEL</th>
			<th>BOLETO</th>
			<th>INSCRIPCION</th>
			<th>SUBTOTAL</th>
		</tr>
		<?php
			$total_costos = 0;
			for ($i=0; $i < count($paxs); $i++) 
			{ 
				$pax 	= $paxs[$i];
				$idpax 	= $pax['idpax'];

				$costo_hoteles 			= c_mostrar_cotizacion::obtener_detalle_hotel_por_pax($idpax);
				$costo_servicios 		= c_mostrar_cotizacion::obtener_detalle_servicios_por_pax($idpax);
				$costo_boletos 			= c_mostrar_cotizacion::obtener_detalle_boletos_por_pax($idpax);
				$costo_inscripciones 	= c_mostrar_cotizacion::obtener_detalle_inscripciones_por_pax($idpax);

			?>
				<tr>
					<td><?php echo ($i+1); ?></td>
					<td><?php echo strtoupper($pax['nombre_cliente'] . ' ' . $pax['apellido_cliente']); ?></td>
					<td>
						<label onclick="$('#hotel_pax_<?php echo $i; ?>').show();"><?php echo $costo_hoteles['precio_total_pax'] + $costo_servicios['precio_total_pax']; ?></label>
						<div id="hotel_pax_<?php echo $i; ?>" style="z-index:999; position:absolute; padding:10px; background-color:#FFF; border:1px solid #AAA; border-radius:10px; display:none;">
							<img src="images/cancel.png" style="width:15px; height:15px; right:2px; top:2px; position:absolute;" onclick="$('#hotel_pax_<?php echo $i; ?>').hide();" />
							<?php
								if(array_key_exists(0, $costo_hoteles))
								{
								?>
									<table>
										<tr style="background-color:#10689b; color:#FFFFFF;">
											<th>HOTEL</th>
											<th>IN</th>
											<th>OUT</th>
											<th>HAB.</th>
										</tr>
									<?php
									//echo "<pre>"; print_r($costo_hoteles); echo "<pre/>";
										for ($j=0; array_key_exists($j, $costo_hoteles); $j++) 
										{ 
											$hotel = $costo_hoteles[$j];
										?>
											<tr>
												<td><?php echo $hotel['nombre_hotel'] . '-' . $hotel['categoria'] . '*' . ' (' . $hotel['destino'] . ')'; ?></td>
												<td><?php echo date('d/m/Y', strtotime($hotel['fecha_in'])); ?></td>
												<td><?php echo date('d/m/Y', strtotime($hotel['fecha_out'])); ?></td>
												<td><?php echo $hotel['habitacion']; ?></td>
											</tr>
										<?php
										}
									?>
									</table>
								<?php
								}
								else
								{
								?>
								 <strong>HOTELES NO INGRESADO</strong><br/>
								<?php
								}
							?>
							<?php
								if(array_key_exists(0, $costo_servicios))
								{
								?>
									<table>
										<tr style="background-color:#10689b; color:#FFFFFF;">
											<th>SERVICIO</th>
											<th>DESCRIPCION</th>
										</tr>
									<?php
									//echo "<pre>"; print_r($costo_hoteles); echo "<pre/>";
										for ($j=0; array_key_exists($j, $costo_servicios); $j++) 
										{ 
											$servicio = $costo_servicios[$j];
										?>
											<tr>
												<td><?php echo $servicio['nombre_servicio']; ?></td>
												<td><?php echo $servicio['descripcion']; ?></td>
											</tr>
										<?php
										}
									?>
									</table>
								<?php
								}
								else
								{
								?>
								 <strong>SERVICIOS NO INGRESADO</strong>
								<?php
								}
							?>	
						</div>
					</td>
					<td>
						<label onclick="$('#boleto_pax_<?php echo $i; ?>').show();"><?php echo $costo_boletos['precio_total_pax']; ?></label>
						<div id="boleto_pax_<?php echo $i; ?>" style="z-index:999; position:absolute; padding:10px; background-color:#FFF; border:1px solid #AAA; border-radius:10px; display:none;">
							<img src="images/cancel.png" style="width:15px; height:15px; right:2px; top:2px; position:absolute;" onclick="$('#boleto_pax_<?php echo $i; ?>').hide();" />
							<?php
								if(array_key_exists(0, $costo_boletos))
								{
								?>
									<table>
										<tr style="background-color:#10689b; color:#FFFFFF;">
											<th>LINEA</th>
											<th>RUTA</th>
											<th>PAX</th>
											<th>T LIMIT</th>
										</tr>
									<?php
									//echo "<pre>"; print_r($costo_hoteles); echo "<pre/>";
										for ($j=0; array_key_exists($j, $costo_boletos); $j++) 
										{ 
											$boleto = $costo_boletos[$j];
										?>
											<tr>
												<td><?php echo $boleto['nombre_linea_aerea'] . ' (' . $boleto['codigo_iata_linea_aerea'] . ')'; ?></td>
												<td><?php echo $boleto['ruta']; ?></td>
												<td><?php echo $boleto['tarifa']; ?></td>
												<td><?php echo date('d/m/Y', strtotime($boleto['time_limit'])); ?></td>
											</tr>
										<?php
										}
									?>
									</table>
								<?php
								}
								else
								{
								?>
								 <strong>NO INGRESADO</strong>
								<?php
								}
							?>	
						</div>
					</td>
					<td>
						<label onclick="$('#inscripcion_pax_<?php echo $i; ?>').show();"><?php echo $costo_inscripciones['precio_total_pax']; ?></label>
						<div id="inscripcion_pax_<?php echo $i; ?>" style="z-index:999; position:absolute; padding:10px; background-color:#FFF; border:1px solid #AAA; border-radius:10px; display:none;">
							<img src="images/cancel.png" style="width:15px; height:15px; right:2px; top:2px; position:absolute;" onclick="$('#inscripcion_pax_<?php echo $i; ?>').hide();" />
							<?php
								if(array_key_exists(0, $costo_inscripciones))
								{
								?>
									<table>
										<tr style="background-color:#10689b; color:#FFFFFF;">
											<th>DESCRIPCION</th>
											<th>T LIMIT</th>
										</tr>
									<?php
									//echo "<pre>"; print_r($costo_hoteles); echo "<pre/>";
										for ($j=0; array_key_exists($j, $costo_inscripciones); $j++) 
										{ 
											$inscripcion = $costo_inscripciones[$j];
										?>
											<tr>
												<td><?php echo $inscripcion['categoria']; ?></td>
												<td><?php echo date('d/m/Y', strtotime($inscripcion['time_limit'])); ?></td>
											</tr>
										<?php
										}
									?>
									</table>
								<?php
								}
								else
								{
								?>
								 <strong>NO INGRESADO</strong>
								<?php
								}
							?>	
						</div>
					</td>
					<td>
						<label>
							<?php 
								$costo_pax 		= $costo_hoteles['precio_total_pax'] + $costo_servicios['precio_total_pax'] + $costo_boletos['precio_total_pax'] + $costo_inscripciones['precio_total_pax']; 
								echo $costo_pax;
								$total_costos 	+= $costo_pax;
							?>
						</label>
					</td>
				</tr>
			<?php
			}
		?>
		<tr>
			<td colspan="5"><label>TOTAL COSTOS</label></td>
			<td><label><?php echo $costo_pax; ?></label></td>
		</tr>
	</table>
<?php include('footer.php'); ?>