<?php
	header('Content-Type: text/html; charset=UTF-8');

	include '../control/c_login.php';
	include '../entidad/usuarios.php';
	include '../BD/controladoraBD.php';

	include '../entidad/temporada.php';
	include '../entidad/tipo_paquete.php';
	include '../entidad/destino.php';

	$temporadas = temporada::obtener_datos_temporada();
	$a_temporadas = array();

	for ($i=0; $i < count($temporadas); $i++) 
	{ 
		$a_temporadas[] = ucwords(strtolower($temporadas[$i]['nombre_temporada']));
	}

	$destinos = destino::obtener_datos_destino();
	$a_destinos = array();

	for ($i=0; $i < count($destinos); $i++) 
	{ 
		$a_destinos[] = ucwords(strtolower($destinos[$i]['nombre_destino']));
	}

	$tipos_pqt = tipo_paquete::obtener_datos_tipo_paquete();
	$a_tipospqt = array();

	for ($i=0; $i < count($tipos_pqt); $i++) 
	{ 
		$a_tipospqt[] = ucwords(strtolower($tipos_pqt[$i]['descripcion']));
	}

	echo 'var temporadas = ["' . implode('","', $a_temporadas) . '"];<br/>';
	echo 'var destinos = ["' . implode('","', $a_destinos) . '"];<br/>';
	echo 'var tipos_pqt = ["' . implode('","', $a_tipospqt) . '"];<br/>';

?>