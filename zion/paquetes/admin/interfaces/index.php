<?php 
	include 'header.php';
	include('../entidad/publicacion.php');
?>

<?php
	$anulacion_paquetes = publicacion::anular_publicaciones_a_vencerse();

	$paquetes_anulados 	= publicacion::buscar_paquetes_vacacionales_a_vencerse();
	//echo "<pre>"; print_r($paquetes_anulados); echo "</pre>";
	?>
		<div class="seven columns" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1; padding:5px;">
			<font style="font-weight:bold; color:#10689b; float:left;">PAQUETES VENCIDOS HOY</font><br/>
			<ul class="lista_descripcion" >
				<?php
				if(count($paquetes_anulados) == 0)
				{
					?>
						<li><a>NO EXISTE NINGUNO</a></li>
					<?php
				}
				else
				{
					for ($i=0; $i < count($paquetes_anulados); $i++) 
					{ 
					?>
						<li>
							<a><?php echo $paquetes_anulados[$i]['titulo']; ?> <strong><?php echo date('d/M/Y', strtotime($paquetes_anulados[$i]['fecha_caducidad'])); ?></strong></a>
						</li>
					<?php
					}
				}
				?>
			</ul>
		</div>
	<?php

	$eventos_anulados 	= publicacion::buscar_paquetes_eventos_a_vencerse();
	//echo "<pre>"; print_r($eventos_anulados); echo "</pre>";
	?>
		<div class="seven columns" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1; padding:5px;">
			<font style="font-weight:bold; color:#10689b; float:left;">PAQUETES DE EVENTOS VENCIDOS HOY</font><br/>
			<ul class="lista_descripcion" >
				<?php
				if(count($eventos_anulados) == 0)
				{
					?>
						<li><a>NO EXISTE NINGUNO</a></li>
					<?php
				}
				else
				{
					for ($i=0; $i < count($eventos_anulados); $i++) 
					{ 
					?>
						<li>
							<a><?php echo $eventos_anulados[$i]['titulo']; ?> <strong><?php echo date('d/M/Y', strtotime($eventos_anulados[$i]['fecha_caducidad'])); ?></strong></a>
						</li>
					<?php
					}
				}
				?>
			</ul>
		</div>
		<div class="clear_mayor"></div>
	<?php

	$paquetes_anular 	= publicacion::buscar_paquetes_vacacionales_a_vencerse_en_siete_dias();
	//echo "<pre>"; print_r($paquetes_anular); echo "</pre>";
	?>
		<div class="seven columns" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1; padding:5px;">
			<font style="font-weight:bold; color:#10689b; float:left;">PAQUETES POR VENCERSE</font><br/>
			<ul class="lista_descripcion" >
				<?php
				if(count($paquetes_anular) == 0)
				{
					?>
						<li><a>NO EXISTE NINGUNO</a></li>
					<?php
				}
				else
				{
					for ($i=0; $i < count($paquetes_anular); $i++) 
					{ 
					?>
						<li>
							<a><?php echo $paquetes_anular[$i]['titulo']; ?> <strong><?php echo date('d/M/Y', strtotime($paquetes_anular[$i]['fecha_caducidad'])); ?></strong></a>
						</li>
					<?php
					}
				}
				?>
			</ul>
		</div>
	<?php

	$eventos_anular 	= publicacion::buscar_paquetes_eventos_a_vencerse_en_siete_dias();
	//echo "<pre>"; print_r($eventos_anular); echo "</pre>";
	?>
		<div class="seven columns" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1; padding:5px;">
			<font style="font-weight:bold; color:#10689b; float:left;">PAQUETES DE EVENTOS POR VENCERSE</font><br/>
			<ul class="lista_descripcion" >
				<?php
				if(count($eventos_anular) == 0)
				{
					?>
						<li><a>NO EXISTE NINGUNO</a></li>
					<?php
				}
				else
				{
					for ($i=0; $i < count($eventos_anular); $i++) 
					{ 
					?>
						<li>
							<a><?php echo $eventos_anular[$i]['titulo']; ?> <strong><?php echo date('d/M/Y', strtotime($eventos_anular[$i]['fecha_caducidad'])); ?></strong></a>
						</li>
					<?php
					}
				}
				?>
			</ul>
		</div>
		<div class="clear_mayor"></div>
		<hr/>

	<?php

	$promos_anuladas 	= publicacion::buscar_promociones_a_vencerse();
	//echo "<pre>"; print_r($eventos_anular); echo "</pre>";
	?>
		<div class="seven columns" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1; padding:5px;">
			<font style="font-weight:bold; color:#10689b; float:left;">PROMOCIONES VENCIDAS HOY</font><br/>
			<ul class="lista_descripcion" >
				<?php
				if(count($promos_anuladas) == 0)
				{
					?>
						<li><a>NO EXISTE NINGUNO</a></li>
					<?php
				}
				else
				{
					for ($i=0; $i < count($promos_anuladas); $i++) 
					{ 
					?>
						<li>
							<a><?php echo $promos_anuladas[$i]['titulo']; ?> <strong><?php echo date('d/M/Y', strtotime($promos_anuladas[$i]['fecha_caducidad'])); ?></strong></a>
						</li>
					<?php
					}
				}
				?>
			</ul>
		</div>
	<?php

	$promos_anular 	= publicacion::buscar_promociones_a_vencerse_en_siete_dias();
	//echo "<pre>"; print_r($paquetes_anular); echo "</pre>";
	?>
		<div class="seven columns" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1; padding:5px;">
			<font style="font-weight:bold; color:#10689b; float:left;">PROMOCIONES POR VENCERSE</font><br/>
			<ul class="lista_descripcion" >
				<?php
				if(count($promos_anular) == 0)
				{
					?>
						<li><a>NO EXISTE NINGUNO</a></li>
					<?php
				}
				else
				{
					for ($i=0; $i < count($promos_anular); $i++) 
					{ 
					?>
						<li>
							<a><?php echo $promos_anular[$i]['titulo']; ?> <strong><?php echo date('d/M/Y', strtotime($promos_anular[$i]['fecha_caducidad'])); ?></strong></a>
						</li>
					<?php
					}
				}
				?>
			</ul>
		</div>

<?php include 'footer.php'; ?>