
<!-- TABLA DE NUEVO DESTINO PARA HOTEL -->

<div id="plantilla_nueva_tabla_tarifa" style="display:none;">
	<div id="tabla_tarifas__idth_">
	<table width="100%;" id="tarifa__idth_" class="encabezado_tarifa_hotel">
		<tr>
			<td style="width:20%;">
				<strong>DESTINO :</strong>
			</td>
			<td style="width:20%;">
				<input type="text" style="margin: 0; width: 100px;" id="tdi_idth_" placeholder="Codigo Ciudad" onchange="avisar_cambio('H', '_idth_');" />
			</td>
			<td style="width:20%;">
				<strong>ORDEN DESTINO:</strong>
			</td>
			<td style="width:20%;">
				<input type="text" style="margin: 0; width: 100px;" id="tdord_idth_" value="_indd_" onchange="avisar_cambio('H', '_idth_');" />
			</td>
			<td>
				<img src="images/cross.png" style="width:15px; height:15px; cursor:pointer; float:right;" title="Eliminar"  onclick="eliminar_tabla_tarifa('_idth_', 'H');" />
				<input type="hidden" value="0" id="idth__idth_" />
				<input type="hidden" value="_idciudad_" id="idciudad__idth_" />
				<span style="display:none;" id="items_eliminados_idth_"></span>
			</td>
		</tr>
	</table>
	<table style="width:100%; border:1px solid #FF0000; font-size:8pt;" id="detalle_tarifa__idth_">
	</table>	
	<a class="mylink is_real0" onclick="agregar_item_hotel('_idth_');">
		<img src="images/add50.png" class="mini" />
		Nuevo Hotel
	</a>
	<a class="mylink is_real1" style="display:none;" onclick="agregar_item_hotel_real('_idth_');">
		<img src="images/add50.png" class="mini" />
		Nuevo Hotel(Reserva)
	</a>
	<a class="mylink" onclick="guardar_tarifa('_idth_');">
		<img src="images/save32.png" class="mini" />
		Guardar Destino
	</a>
	<input type="hidden" value="0" id="c_hoteles__idth_" />
	<hr/>
	</div>
</div>

<!-- TABLA DE NUEVO HOTEL -->

<table id="plantilla_nueva_opcion_hotel" style="display:none;">
	<tr class="item_tarifa_hotel__idth_ is_real_valisreal_" id="item__ind_">
		<td class="thirteen columns" style="padding-top:10px;" >
			<div class="one columns">
				<label style="color:#10689B;">HOTEL:</label>
			</div>
			<div class="three columns">
				<input type="text" placeholder="OBLIGATORIO" value="" onkeyup="buscar_hoteles('_ind_');" id="hotel__ind_" style="margin:0px;" />
				<input type="hidden" id="iidhot__ind_" value="0" onchange="avisar_cambio('H', '_ind_');" />				
			</div>
			<div class="one columns">
				<label style="color:#10689B;">IN:</label>
			</div>
			<div class="two columns">
				<input type="text" id="tin_ind_" style="width:75%; margin:0px;" placeholder="dd/mm/aaaa" onchange="avisar_cambio('H', '_ind_'); obtener_totales_hotel('_ind_');" />
			</div>
			<div class="one columns">
				<label style="color:#10689B;">OUT:</label>
			</div>
			<div class="two columns">
				<input type="text" id="tou_ind_" style="width:75%; margin:0px;" placeholder="dd/mm/aaaa" onchange="avisar_cambio('H', '_ind_'); obtener_totales_hotel('_ind_');" />
			</div>
			<div class="one columns">
				<label style="color:#10689B;">NOCHES:</label>
			</div>
			<div class="one columns">
				<label id="cantnoches_ind_">0</label>
			</div>
			<div class="clear_mayor"></div>
			<div class="two columns is_real1" style="display:none;">
				<label>IDENTIFICADOR</label>
				<input type="text" placeholder="OPCIONAL" value="" id="iide__ind_" style="margin:0px;" onchange="avisar_cambio('H', '_ind_');" />
				<label>TIME LIMIT</label>
				<input type="text" placeholder="dd/mm/aaaa" id="itmlm__ind_" style="width:75%; margin:0px;" onchange="avisar_cambio('H', '_ind_');" />
			</div>
			<div class="one columns">
				<div id="busqueda_hotel__ind_" style="display:none; position:absolute; width:350px; height:200px; z-index:999999; background-color:#FFFFFF; border:1px solid #AAAAAA;">
					<img src="images/cancel.png" style="width:15px; height:15px; margin-left:330px; margin-top:2px; position:absolute;" onclick="$('#busqueda_hotel__ind_').hide();" />
					<div id="panel_busqueda_hotel__ind_" style="width:350px; height:200px; overflow:auto;">
					</div>
					&nbsp;
				</div>
				<label>PRECIOS</label>
				<input type="text" id="isgl_ind_" value="" placeholder="SGL" style="margin:0px;" onblur="obtener_total_hotel('_ind_', 'sgl');" onchange="avisar_cambio('H', '_idth_');"/>
				<input type="text" id="idbl_ind_" value="" placeholder="DBL" style="margin:0px;" onblur="obtener_total_hotel('_ind_', 'dbl');" onchange="avisar_cambio('H', '_idth_');"/>
				<input type="text" id="itpl_ind_" value="" placeholder="TPL" style="margin:0px;" onblur="obtener_total_hotel('_ind_', 'tpl');" onchange="avisar_cambio('H', '_idth_');"/>
				<input type="text" id="icdpl_ind_" value="" placeholder="CPL" style="margin:0px;" onblur="obtener_total_hotel('_ind_', 'cdpl');" onchange="avisar_cambio('H', '_idth_');"/>
			</div>
			<div class="one columns">
				<label>&nbsp;</label>
				<input type="text" id="imnr_ind_" value="" placeholder="CNN" style="margin:0px;" onblur="obtener_total_hotel('_ind_', 'mnr');" onchange="avisar_cambio('H', '_idth_');"/>
				<input type="text" id="iinf_ind_" value="" placeholder="INF" style="margin:0px;" onblur="obtener_total_hotel('_ind_', 'inf');" onchange="avisar_cambio('H', '_idth_');"/>	
				<input type="text" id="icms_ind_" value="" placeholder="FEE" style="margin:0px;" onblur="obtener_totales_hotel('_ind_');" onchange="avisar_cambio('H', '_idth_');" class="pc__idth_" />
				<input type="text" id="iiva_ind_" value="" placeholder="IVA" style="margin:0px;" onblur="obtener_totales_hotel('_ind_');" onchange="avisar_cambio('H', '_idth_');" />
			</div>

			<div class="three columns">
				<label>ALIMENTACION</label>
				<textarea id="ialm_ind_" class="mytextarea" onchange="avisar_cambio('H', '_idth_');" ></textarea>
				<label>OBS</label>
				<textarea id="iiex_ind_" class="mytextarea" onchange="avisar_cambio('H', '_idth_');" ></textarea>
			</div>
			<div class="three columns">
				<label>LINK</label>
				<input type="text" id="ilnk_ind_" value="" onchange="avisar_cambio('H', '_idth_');"/>
				<label>OBS INT</label>
				<textarea id="iobs_ind_" class="mytextarea" onchange="avisar_cambio('H', '_idth_');" ></textarea>	
			</div>
			<div class="one columns">
				<label>SGL:</label>
				<label>DBL:</label>
				<label>TPL:</label>
				<label>CPL:</label>
				<label>CNN:</label>
				<label>INF:</label>
			</div>
			<div class="one columns">
				<label id="tot-sgl_ind_">0</label>
				<label id="tot-dbl_ind_">0</label>
				<label id="tot-tpl_ind_">0</label>
				<label id="tot-cdpl_ind_">0</label>
				<label id="tot-mnr_ind_">0</label>
				<label id="tot-inf_ind_">0</label>
			</div>
			<div class="clear"></div>
			<div class="two columns">
				<strong>FEE:</strong><input type="radio" name="apfee_ind_" value="M" onchange="obtener_totales_hotel('_ind_');" />$<input type="radio" name="apfee_ind_" value="P" onchange="obtener_totales_hotel('_ind_');" checked />%
			</div>
			<div class="two columns">
				<strong>IVA:</strong><input type="radio" name="apiva_ind_" value="M" onchange="obtener_totales_hotel('_ind_');" />$<input type="radio" name="apiva_ind_" value="P" onchange="obtener_totales_hotel('_ind_');" checked />%
			</div>
			<hr/>
		</td>
		<td style="vertical-align:top;">
			<center>
				<br/><img src="images/cancel.png" style="width:10px; height:10px; cursor:pointer;" title="Eliminar" onclick="eliminar_item_hotel('_ind_');" />
				<br/><br/>Act<br/>
				<input type="checkbox" id="est_ind_" checked />
				<input type="hidden" id="hot_is_real_ind_" value="_valisreal_" />
				<input type="hidden" id="iditem_ind_" value="0" />
				<?php
					if(strpos($_SERVER['PHP_SELF'], 'editar_cotizacion_paquete.php') !== false)
					{
					?>
						<br/><br/>
						<input type="button" class="btn_reservar is_real0" value="VENDER" title="Reservar" onclick="ConvertirHotelAReserva('_ind_', '_idth_');" />
					<?php
					}
				?>
			</center>
		</td>
	</tr>
</table>

<!-- TABLA DE NUEVO SERVICIO -->

<table id="plantilla_nueva_otra_tarifa" style="display:none;">
	<tr class="_cls_ is_real_valisreal_" id="otroservicio__ind_">
		<td style="vertical-align:middle; padding-top:10px; padding-bottom:10px;">
			<span class="is_real1" >CODIGO :<input type="text" placeholder="Opcional" value="" id="icodotr_ind_" style="width:70%; margin:0;" /></span>
			CONCPT :<input type="text" placeholder="Obligatorio" value="" id="inser_ind_" style="width:70%; margin:0;" onkeyup="buscar_otros_servicios('_ind_');" />*
			DESCRIP :<input type="text" placeholder="Opcional" value="" id="idesotr_ind_" style="width:70%; margin:0;" />
			<input type="hidden" value="0" id="iidsr_ind_" />
			<input type="hidden" value="_idciudad_" id="iidscd_ind_" />
		</td>
		<td style="vertical-align:middle;">
			<input type="text" id="iociudad_ind_" placeholder="CIUDAD (IATA)" style="margin: 0; width: 70%;" />
			<input type="text" id="iofd_ind_" placeholder="dd/mm/aaaa" value="" style="margin: 0; width: 70%;" />
			<input type="text" id="iofh_ind_" placeholder="dd/mm/aaaa" value="" style="margin: 0; width: 70%;" />
		</td>
		<td style="vertical-align:middle;">
			<div id="busqueda_otro_servicio__ind_" style="display:none; position:absolute; width:350px; height:200px; z-index:999999; background-color:#FFFFFF; border:1px solid #AAAAAA;">
				<img src="images/cancel.png" style="width:15px; height:15px; margin-left:330px; margin-top:2px; position:absolute;" onclick="$('#busqueda_otro_servicio__ind_').hide();" />
				<div id="panel_busqueda_otro_servicio__ind_" style="width:350px; height:200px; overflow:auto;">
				</div>
				&nbsp;
			</div>
			<input type="text" id="inadt_ind_" placeholder="ADT" style="margin: 0; width: 95%;" onblur="obtener_total_servicio('_ind_','adt');" />
			<input type="text" id="inmen_ind_" placeholder="CNN" style="margin: 0; width: 95%;" onblur="obtener_total_servicio('_ind_','men');" />
			<input type="text" id="ininf_ind_" placeholder="INF" style="margin: 0; width: 95%;" onblur="obtener_total_servicio('_ind_','inf');" />
		</td>
		<td style="vertical-align:middle;">
			<input type="text" id="iofee_ind_" placeholder="FEE" style="margin: 0; width: 25%;" onblur="obtener_totales_servicio('_ind_');" />
			<input type="radio" name="sapfee_ind_" value="M" onchange="obtener_totales_servicio('_ind_');"  />$
			<input type="radio" name="sapfee_ind_" value="P" onchange="obtener_totales_servicio('_ind_');"  checked />%
			<input type="text" id="iofac_ind_" placeholder="IVA" value="" style="margin: 0; width: 25%;" onblur="obtener_totales_servicio('_ind_');" />
			<input type="radio" name="sapiva_ind_" value="M" onchange="obtener_totales_servicio('_ind_');" />$
			<input type="radio" name="sapiva_ind_" value="P" onchange="obtener_totales_servicio('_ind_');" checked />%
			<input type="text" id="iotli_ind_" placeholder="dd/mm/aaaa" value="" style="margin: 0; width: 70%;" />
		</td>
		<td style="vertical-align:middle;">
			<select id="imnda_ind_" style="margin: 0; width: 95%;">
				<option value="USD" Selected >USD</option>
				<option value="BOB" >BOB</option>
			</select>
			<textarea id="ioob_ind_" placeholder="Observacion" style="margin:0; width:95%; min-height:10px; height:30px;" ></textarea>
		</td>
		<td style="vertical-align:middle; line-height:1; font-size:8pt;">
			<span id="tot-oadt_ind_">0</span>(ADT)<br/>
			<span id="tot-omen_ind_">0</span>(CNN)<br/>
			<span id="tot-oinf_ind_">0</span>(INF)
		</td>
		<td style="vertical-align:middle;">
		</td>
		<td style="vertical-align:middle;">
			<input type="checkbox" id="iest_ind_" checked /><br/>
			<img src="images/cancel.png" style="width:10px; height:10px; cursor:pointer;" title="Eliminar" onclick="eliminar_otro_servicio('_ind_');" />
			<input type="hidden" id="ser_is_real_ind_" value="_valisreal_" />
			<input type="hidden" id="idotra_tarifa_ind_" value="0" />
			<?php
				if(strpos($_SERVER['PHP_SELF'], 'editar_cotizacion_paquete.php') !== false)
				{
				?>
					<br/>
					<input type="button" class="btn_reservar is_real0" value="VENDER" title="Reservar" onclick="ConvertirServicioAReserva('_idciudad_', '_ind_', '_indexcity_');" />
				<?php
				}
			?>
		</td>
	</tr>
</table>

<!-- TABLA DE NUEVO BOLETO INGRESO MANUAL -->

<table id="plantilla_nuevo_boleto_cotizado" style="display:none;">
	<tr class="_cls_ is_real_valisreal_" id="boletocotizado-_ind_">
		<td style="vertical-align:middle;">
			<input type="text" id="ibord_ind_" value="_ord_" style="margin: 0; width:80%; top;" />	
			<input type="text" id="icodres_ind_" placeholder="Codigo Reserva" value="" style="margin: 0; width:95%;" />	
		</td>
		<td style="vertical-align:middle;">
			<input type="text" placeholder="Linea Aerea" id="ilinaer_ind_" value="" style="margin: 0; width:95%;" onkeyup="buscar_lineas_aereas('_ind_');" />
			<input type="hidden" id="iidlinaer_ind_" value="0" />
			<input type="text" placeholder="Ruta" id="iruta_ind_" value="" style="margin: 0; width:95%;" onkeyup="input_mask(this,'ruta',false);" />	
		</td>
		<td style="vertical-align:middle; padding-top:10px; padding-bottom:10px;">
			<div id="busqueda_linea_aerea__ind_" style="display:none; position:absolute; width:350px; height:200px; z-index:999999; background-color:#FFFFFF; border:1px solid #AAAAAA;">
				<img src="images/cancel.png" style="width:15px; height:15px; margin-left:330px; margin-top:2px; position:absolute;" onclick="$('#busqueda_linea_aerea__ind_').hide();" />
				<div id="panel_busqueda_linea_aerea__ind_" style="width:350px; height:200px; overflow:auto;">
				</div>
				&nbsp;
			</div>
			<input type="text" id="ibadt_ind_" placeholder="ADT" value="" style="margin: 0; width:95%;" onblur="obtener_total_boleto('_ind_','adt');" />	
			<input type="text" id="ibmen_ind_" placeholder="CNN" value="" style="margin: 0; width:95%;" onblur="obtener_total_boleto('_ind_','men');" />	
			<input type="text" id="ibinf_ind_" placeholder="INF" value="" style="margin: 0; width:95%;" onblur="obtener_total_boleto('_ind_','inf');" />	
		</td>
		<td style="vertical-align:middle;">
			<input type="text" id="ibfee_ind_" placeholder="FEE" value="" style="margin: 0; width:25%;" onblur="obtener_totales_boleto('_ind_');" />	
			<input type="radio" name="bapfee_ind_" value="M" onchange="obtener_totales_boleto('_ind_');"  />$
			<input type="radio" name="bapfee_ind_" value="P" onchange="obtener_totales_boleto('_ind_');"  checked />%
			<input type="text" id="ibfac_ind_" placeholder="IVA" value="" style="margin: 0; width:25%;" onblur="obtener_totales_boleto('_ind_');" />	
			<input type="radio" name="bapiva_ind_" value="M" onchange="obtener_totales_boleto('_ind_');" />$
			<input type="radio" name="bapiva_ind_" value="P" onchange="obtener_totales_boleto('_ind_');" checked />%
			<input type="text" id="iotlires_ind_" placeholder="dd/mm/aaaa" value="" style="margin: 0; width: 70%;" />
		</td>
		<td style="vertical-align:middle;">
			<select id="ibmnda_ind_" style="margin: 0; width:95%;">
				<option value="USD" selected >USD</option>
				<option value="BOB" >BOB</option>
			</select>
			<textarea id="ibob_ind_" placeholder="Observacion" style="margin:0; width:95%; min-height:10px; height:30px;" ></textarea>
		</td>
		<td style="vertical-align:middle; line-height:1; font-size:8pt;">
			<span id="tot-adt_ind_">0</span>(ADT)<br/>
			<span id="tot-men_ind_">0</span>(CNN)<br/>
			<span id="tot-inf_ind_">0</span>(INF)
		</td>
		<td style="vertical-align:middle; line-height:1; font-size:8pt;">
		</td>
		<td style="vertical-align:middle;">
			<input type="checkbox" id="iestres_ind_" checked /><br/>
			<img src="images/cancel.png" style="width:10px; height:10px; cursor:pointer;" title="Eliminar" onclick="eliminar_boleto_cotizado('_ind_');" />
			<input type="hidden" id="bol_is_real_ind_" value="_valisreal_" />
			<input type="hidden" id="idboleto_cotizado_ind_" value="<?php echo $idboleto; ?>" />
			<?php
				if(strpos($_SERVER['PHP_SELF'], 'editar_cotizacion_paquete.php') !== false)
				{
				?>
					<br/>
					<input type="button" class="btn_reservar is_real0" value="VENDER" title="Reservar" onclick="ConvertirBoletoAReserva('_indopc_', '_ind_');" />
				<?php
				}
			?>
		</td>
	</tr>
</table>

<!-- TABLA DE NUEVO BOLETO INGRESO POR WEB SERVICE -->

<table id="plantilla_nuevo_boleto_cotizado_automatico" style="display:none;">
	<tr class="_cls_ is_real_valisreal_" id="boletocotizado-_ind_">
		<td style="vertical-align:middle;">
			<input type="text" id="ibord_ind_" value="_ord_" style="margin: 0; width:80%; top;" />	
			<input type="text" id="icodres_ind_" value="_reserva_" style="margin: 0; width:95%;" />	
		</td>
		<td style="vertical-align:middle;">
			<input type="text" placeholder="Linea Aerea" id="ilinaer_ind_" value="_nomlinea_" style="margin: 0; width:95%;" onkeyup="buscar_lineas_aereas('_ind_');" />
			<input type="hidden" id="iidlinaer_ind_" value="_idlinea_" />
			<input type="text" placeholder="Ruta" id="iruta_ind_" value="_ruta_" style="margin: 0; width:95%;" onkeyup="input_mask(this,'ruta',false);" />	
		</td>
		<td style="vertical-align:middle;">
			<div id="busqueda_linea_aerea__ind_" style="display:none; position:absolute; width:350px; height:200px; z-index:999999; background-color:#FFFFFF; border:1px solid #AAAAAA;">
				<img src="images/cancel.png" style="width:15px; height:15px; margin-left:330px; margin-top:2px; position:absolute;" onclick="$('#busqueda_linea_aerea__ind_').hide();" />
				<div id="panel_busqueda_linea_aerea__ind_" style="width:350px; height:200px; overflow:auto;">
				</div>
				&nbsp;
			</div>
			<input type="text" id="ibadt_ind_" placeholder="ADT" value="_adt_" style="margin: 0; width:95%;" onblur="obtener_total_boleto('_ind_','adt');" />	
			<input type="text" id="ibmen_ind_" placeholder="CNN" value="_cnn_" style="margin: 0; width:95%;" onblur="obtener_total_boleto('_ind_','men');" />	
			<input type="text" id="ibinf_ind_" placeholder="INF" value="_inf_" style="margin: 0; width:95%;" onblur="obtener_total_boleto('_ind_','inf');" />	
		</td>
		<td style="vertical-align:middle;">
			<input type="text" id="ibfee_ind_" placeholder="FEE" value="_fee_" style="margin: 0; width:25%;" onblur="obtener_totales_boleto('_ind_');" />	
			<input type="radio" name="bapfee_ind_" value="M" onchange="obtener_totales_boleto('_ind_');"  />$
			<input type="radio" name="bapfee_ind_" value="P" onchange="obtener_totales_boleto('_ind_');"  checked />%
			<input type="text" id="ibfac_ind_" placeholder="IVA" value="" style="margin: 0; width:25%;" onblur="obtener_totales_boleto('_ind_');" />	
			<input type="radio" name="bapiva_ind_" value="M" onchange="obtener_totales_boleto('_ind_');" />$
			<input type="radio" name="bapiva_ind_" value="P" onchange="obtener_totales_boleto('_ind_');" checked />%
			<input type="text" id="iotlires_ind_" value="_tlimit_" style="margin: 0; width: 70%;" />
		</td>
		<td style="vertical-align:middle;">
			<select id="ibmnda_ind_" style="margin: 0; width:95%;">
				<option value="USD" selected >USD</option>
				<option value="BOB" >BOB</option>
			</select>
			<textarea id="ibob_ind_" style="margin:0; width:95%; min-height:10px; height:30px;" >_observ_</textarea>
		</td>
		<td style="vertical-align:middle; line-height:1; font-size:8pt;">
			<span id="tot-adt_ind_">0</span>(ADT)<br/>
			<span id="tot-men_ind_">0</span>(CNN)<br/>
			<span id="tot-inf_ind_">0</span>(INF)
		</td>
		<td style="vertical-align:middle; line-height:1; font-size:8pt;">
		</td>
		<td style="vertical-align:middle;">
			<input type="checkbox" id="iestres_ind_" checked /><br/>
			<img src="images/cancel.png" style="width:10px; height:10px; cursor:pointer;" title="Eliminar" onclick="eliminar_boleto_cotizado('_ind_');" />
			<input type="hidden" id="bol_is_real_ind_" value="_valisreal_" />
			<input type="hidden" id="idboleto_cotizado_ind_" value="0" />
			<?php
				if(strpos($_SERVER['PHP_SELF'], 'editar_cotizacion_paquete.php') !== false)
				{
				?>
					<br/>
					<input type="button" class="btn_reservar is_real0" value="VENDER" title="Reservar" onclick="ConvertirBoletoAReserva('_indopc_', '_ind_');" />
				<?php
				}
			?>
		</td>
	</tr>
</table>

<!-- TABLA DE NUEVO SEGMENTO PARA ITINERARIO MANUAL -->

<table id="plantilla_nuevo_segmento_automatico" style="display:none;">
	<tr class="_cls_" id="segmento-_ind_">
		<td>
			<input type="text" id="iiord_ind_" value="_orden_" style="margin: 0; width: 95%;" />	
		</td>
		<td>
			<input type="text" id="iicod_ind_" value="_codigo_" style="margin: 0; width: 95%;" />	
		</td>
		<td>
			<input type="text" id="iilin_ind_" value="_linea_" style="margin: 0; width: 95%;" class="input_linea_aerea" />	
		</td>
		<td>
			<input type="text" id="iivue_ind_" value="_vuelo_" style="margin: 0; width: 95%;" />	
		</td>
		<td>
			<input type="text" id="iifec_ind_" value="_fecha_" style="margin: 0; width:95%" placeholder="dd/mm/aaaa" onkeyup="input_mask(this,'fecha',false);" />	
		</td>
		<td>
			<input type="text" id="iiasa_ind_" value="_origen_" style="margin: 0; width: 95%;" class="input_aeropuerto" />
		</td>
		<td>
			<input type="text" id="iihsa_ind_" value="_hsale_" style="margin: 0; width: 95%;" placeholder="hh:mm" onkeyup="input_mask(this,'hora',false);" />
		</td>
		<td>
			<input type="text" id="iiall_ind_" value="_destino_" style="margin: 0; width: 95%;" class="input_aeropuerto" />
		</td>
		<td>
			<input type="text" id="iihll_ind_" value="_hllega_" style="margin: 0; width: 95%;" placeholder="hh:mm" onkeyup="input_mask(this,'hora',false);" />
		</td>
		<td>
			<img src="images/cancel.png" style="width:10px; height:10px; cursor:pointer;" title="Eliminar" onclick="eliminar_segmento('_ind_');" />
			<input type="hidden" id="idsegmento_ind_" value="0" />
		</td>
	</tr>
</table>

<!-- TABLA DE NUEVO SEGMENTO PARA ITINERARIO POR WEB SERVICE -->

<table id="plantilla_nuevo_segmento" style="display:none;">
	<tr class="_cls_" id="segmento-_ind_">
		<td>
			<input type="text" id="iiord_ind_" value="_ord_" style="margin: 0; width: 95%;" />	
		</td>
		<td>
			<input type="text" id="iicod_ind_" value="" style="margin: 0; width: 95%;" />	
		</td>
		<td>
			<input type="text" id="iilin_ind_" value="- linea -" style="margin: 0; width: 95%;" class="input_linea_aerea" />	
		</td>
		<td>
			<input type="text" id="iivue_ind_" value="0" style="margin: 0; width: 95%;" />	
		</td>
		<td>
			<input type="text" id="iifec_ind_" value="00/00/0000" style="margin: 0; width:95%" placeholder="dd/mm/aaaa" onkeyup="input_mask(this,'fecha',false);" />	
		</td>
		<td>
			<input type="text" id="iiasa_ind_" value="- origen -" style="margin: 0; width: 95%;" class="input_aeropuerto" />
		</td>
		<td>
			<input type="text" id="iihsa_ind_" value="00:00" style="margin: 0; width: 95%;" placeholder="hh:mm" onkeyup="input_mask(this,'hora',false);" />
		</td>
		<td>
			<input type="text" id="iiall_ind_" value="- destino -" style="margin: 0; width: 95%;" class="input_aeropuerto" />
		</td>
		<td>
			<input type="text" id="iihll_ind_" value="00:00" style="margin: 0; width: 95%;" placeholder="hh:mm" onkeyup="input_mask(this,'hora',false);" />
		</td>
		<td>
			<img src="images/cancel.png" style="width:10px; height:10px; cursor:pointer;" title="Eliminar" onclick="eliminar_segmento('_ind_');" />
			<input type="hidden" id="idsegmento_ind_" value="0" />
		</td>
	</tr>
</table>

<!-- TABLA DE NUEVO SEGMENTO PARA INSCRIPCION -->

<table id="plantilla_nueva_inscripcion" style="display:none;">
	<!--<tr class="_cls_" id="trinscripcion__ind_">
		<td style="vertical-align:middle;">
        	<input id="txt_categoria__ind_" type="text" value="" style="margin: 0; width:95%;"/>
        </td>
        <td style="vertical-align:middle;">
            <select id="txt_moneda__ind_" style="margin: 0; width: 95%;">
                <option value="U" >USD</option>
                <option value="B" >BOB</option>
            </select>
        </td>
        <td style="vertical-align:middle;">
            <input type="text" id="txt_precio__ind_" value="" style="margin: 0; width: 95%;" onblur="obtener_total_inscripcion('_ind_');" />
        </td>
        <td style="vertical-align:middle;">
            <input type="text" id="txt_fee__ind_" value="" style="margin: 0; width: 95%;" onblur="obtener_total_inscripcion('_ind_');" />
        </td>
        <td style="vertical-align:middle;">
            <input type="text" id="txt_factura__ind_" value="" style="margin: 0; width: 95%;" onblur="obtener_total_inscripcion('_ind_');" />
        </td>
        <td style="vertical-align:middle;">
            <input type="text" id="txt_time_limit__ind_" value="" style="margin: 0; width: 75%;" onblur="obtener_total_inscripcion('_ind_');" />
        </td>
        <td style="vertical-align:middle;">
            <textarea id="txt_observacion__ind_" style="margin:0; width:95%; min-height:10px; height:30px;" ></textarea>
        </td>
        <td style="vertical-align:middle; line-height:1; font-size:8pt;">
            <span id="tot-insadt_ind_">0</span>
        </td>
        <td style="vertical-align:middle;">
        	<input type="checkbox" id="chk_estado__ind_" checked /><br/>
            <img src="images/cancel.png" style="width:10px; height:10px; cursor:pointer;" title="Eliminar" onclick="eliminar_inscripcion('_ind_');" />
            <input type="hidden" id="hd_idinscripcion_ind_" value="0" />
        </td>
    </tr>-->
    <tr class="_cls_" id="inscripcion__ind_" >
		<td style="vertical-align:middle; padding-top:10px; padding-bottom:10px;">
			EVENTO :<input type="text" value="" id="ineve_ind_" style="width:70%; margin:0;" onkeyup="buscar_eventos('_ind_');" />*
			CATEG. :<input type="text" value="" id="icatins_ind_" style="width:70%; margin:0;" />
			<input type="hidden" value="0" id="iinsidev_ind_" />
		</td>
		<td style="vertical-align:middle;">
			<div id="busqueda_evento__ind_" style="display:none; position:absolute; width:350px; height:200px; z-index:999999; background-color:#FFFFFF; border:1px solid #AAAAAA;">
				<img src="images/cancel.png" style="width:15px; height:15px; margin-left:330px; margin-top:2px; position:absolute;" onclick="$('#busqueda_evento__ind_').hide();" />
				<div id="panel_busqueda_evento__ind_" style="width:350px; height:200px; overflow:auto;">
				</div>
				&nbsp;
			</div>
			<select id="iinsmnda_ind_" style="margin: 0; width: 95%;">
				<option value="USD" Selected >USD</option>
				<option value="BOB" >BOB</option>
			</select>
			<input type="text" id="iinspre_ind_" placeholder="PRECIO" value="" style="margin: 0; width: 95%;" onblur="obtener_total_inscripcion('_ind_');" />
		</td>
		<td style="vertical-align:middle;">
			<input type="text" id="iinsfee_ind_" placeholder="FEE" value="" style="margin: 0; width: 25%;" onblur="obtener_total_inscripcion('_ind_');" />
			<input type="radio" name="insapfee_ind_" value="M" onchange="obtener_total_inscripcion('_ind_');" />$
			<input type="radio" name="insapfee_ind_" value="P" Checked onchange="obtener_total_inscripcion('_ind_');" />%
			<input type="text" id="iinsfac_ind_" placeholder="IVA" value="" style="margin: 0; width: 25%;" onblur="obtener_total_inscripcion('_ind_');" />
			<input type="radio" name="insapiva_ind_" value="M" onchange="obtener_total_inscripcion('_ind_');" />$
			<input type="radio" name="insapiva_ind_" value="P" Checked onchange="obtener_total_inscripcion('_ind_');" />%
		</td>
		<td style="vertical-align:middle;">
			<textarea id="iinsob_ind_" style="margin:0; width:95%; min-height:10px; height:30px;" ></textarea>
		</td>
		<td style="vertical-align:middle; line-height:1; font-size:8pt; text-align:center;">
			<input type="text" id="iinstli_ind_" placeholder="dd/mm/aaaa" value="" style="margin: 0; width: 70%;" />
			<strong>TOTAL:<span id="tot-ins_ind_">0</span></strong><br/>
		</td>
		<td style="vertical-align:middle;">
		</td>
		<td style="vertical-align:middle;">
			<input type="checkbox" id="iinsest_ind_" checked /><br/>
			<img src="images/cancel.png" style="width:10px; height:10px; cursor:pointer;" title="Eliminar" onclick="eliminar_inscripcion('_ind_');" />
			<input type="hidden" id="idins_evento_ind_" value="0" />
		</td>
	</tr>
</table>

<!-- TABLA DE NUEVA OPCION DE AEREO -->

<div id="plantilla_nueva_opcion_aereo" style="display:none;">
	<div id="opcionaereo__ind_" style="margin-bottom:15px; border:1px solid #888;">
		<table width="100%;" class="encabezado_tarifa_hotel">
			<tr>
				<td>
					<strong>OPCION :</strong>
				</td>
				<td style="width:30%;">
					<input type="text" style="margin: 0; width: 150px;" id="opcbol_ind_" value="" />
				</td>
				<td>
					<strong>SALE :</strong>
				</td>
				<td style="width:30%;">
					<input type="text" style="margin: 0; width: 150px;" id="salbol_ind_" value="" />
				</td>
				<td>
					<input type="checkbox" id="iestaereo_ind_" checked />
					Activo&nbsp;&nbsp;
					<img src="images/cross.png" style="width:15px; height:15px; cursor:pointer;" title="Eliminar" onclick="eliminar_opcion_aereo('_ind_');" />
					<input type="hidden" value="0" id="idtb__ind_" />
					<input type="hidden" value="_idciudad_" id="idbcdd_ind_" />
				</td>
			</tr>
		</table>
		<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" id="detalle_boletos_cotizados_ind_" class="con-borde" >
			<tr style="background-color:#10689b; color:#FFFFFF;">
				<th style="width:10%;">ORD / CODIGO</th>
				<th style="width:22%; line-height:1;">L.A. / RUTA</th>
				<th style="width:10%;">PRECIOS</th>
				<th style="width:18%;">FEE / IVA / TIME LIMIT</th>
				<th style="width:25%;">OBSERVACION</th>
				<th style="width:10%;">TOTALES</th>
				<th></th>
				<th style="width:5%;">ACT</th>
			</tr>
		</table>
		<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" id="detalle_itinerario_ind_" class="con-borde" >
			<tr style="background-color:#10689b; color:#FFFFFF;">
				<th style="width:3%;">ORD</th>
				<th style="width:5%;">CODIGO</th>
				<th style="width:19%;">LINEA AEREA</th>
				<th style="width:5%;">VUELO</th>
				<th style="width:10%;">FECHA</th>
				<th style="width:20%;">ORIGEN</th>
				<th style="width:8%;">SALE</th>
				<th style="width:20%;">DESTINO</th>
				<th style="width:8%;">LLEGA</th>
				<th style="width:2%;"></th>
			</tr>
		</table>

		<span style="display:none;" id="boletos_eliminados_ind_"></span>
		<span style="display:none;" id="segmentos_eliminados_ind_"></span>

		<input type="hidden" id="c_boletos_cotizados_ind_" value="0" />
		<input type="hidden" id="c_segmentos_ind_" value="0" />

		<a class="mylink" onclick="guardar_tarifa_aereo('_ind_');">
			<img src="images/save32.png" class="mini" />
			Guardar Cambios
		</a>

		<?php 
                if ($developer> 0) 
                {
                ?>
<a class="mylink is_real0" onclick="agregar_segmento_de_reserva('_ind_');">
			<img src="images/cloud127.png" class="mini" />
			Obtener de Reserva
		</a>
                <?php 
                }
                ?>
		
		<a class="mylink is_real1" style="display:none;" onclick="agregar_segmento_de_reserva_real('_ind_');">
			<img src="images/cloud127.png" class="mini" />
			Obtener de Reserva(Real)
		</a>
		<a class="mylink is_real0" onclick="agregar_boleto_cotizado('_ind_');">
			<img src="images/add50.png" class="mini" />
			Agregar Tarifa Boleto
		</a>
		<a class="mylink is_real1" style="display:none;" onclick="agregar_boleto_cotizado_real('_ind_');">
			<img src="images/add50.png" class="mini" />
			Agregar Boleto (Real)
		</a>
		<a class="mylink" onclick="agregar_segmento('_ind_');">
			<img src="images/add50.png" class="mini" />
			Agregar Segmento
		</a>
	</div>
	<hr/>
</div>

<!-- PLANTILLA PARA UNA NUEVA CIUDAD DE ORIGEN -->

	<!-- BOTON PARA MOSTRAR LAS TARIFAS POR CIUDAD DE SALIDA -->

	<div id="plantilla_boton_ciudad" style="display:none">
		<input type="button"  id="btn_ciudad_origen__ind_" class="btn_c_o" value="Desde _nombreciudad_" style="float:left; background-color:#AAAAAA;" onclick="RevelarTabCiudad(_ind_);" />
	</div>

	<!-- PLANTILLA DONDE ESTARAN TODAS LAS TARIFAS DE HOTEL Y SERVICIOS -->

	<div id="plantilla_nueva_ciudad_origen" style="display:none;">
		<div class="sixteen columns tab_ciudad" id="tab_ciudad__ind_" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#F1F1F1; display:none;" onmouseover="icon_del_ciudad_show(_ind_);" onmouseout="icon_del_ciudad_hide(_ind_);" >

			<div class="fifteen columns" style="height:20px; text-align:right; margin-top:10px;" >
				<img src="images/cross.png" id="btn_del_ciudad__ind_" style="width:15px; height:15px; cursor:pointer; display:none;" onclick="EliminarCiudadOrigen('_idciudad_', '_typ_', '_ind_');" />
				<strong id="lbl_del_ciudad__ind_" style="display:none;">Eliminar Ciudad</strong>
			</div>

			<div class="clear_mayor"></div>

			<div class="columns two" style="font-weight:bold; color:#10689b; height:30px;">
				SALIDA :
			</div>
			<div class="columns four" style="height:30px;" onmouseover="revelar_icono_edicion('fecha_salida__idciudad_');" onmouseout="ocultar_icono_edicion('fecha_salida__idciudad_');">
				<span id="fecha_salida__idciudad_">
					<span id="datos_fecha_salida__idciudad_">&nbsp;
						_fechasalida_	
					</span>
					<img id="icon_editar_fecha_salida__idciudad_" src="images/pencil.png" onclick="mostrar_edicion('fecha_salida__idciudad_');" class="icon_edit">
				</span>
				<span id="editar_fecha_salida__idciudad_" style="display:none;">
					<input type="text" placeholder="dd/mm/aaaa" name="fecha_salida__idciudad_" id="valor_fecha_salida__idciudad_" value="_fechasalida_"  style="width:150px;" />
					<img src="images/cross.png" onclick="ocultar_edicion('fecha_salida__idciudad_');" class="icon_close">
					<img src="images/disk.png" onclick="guardar_atributo_x_ciudad_origen('pqt_ciudad_origen_paquete', 'fecha_salida', 'idciudad_origen_paquete', '_idciudad_', 'fecha_salida__idciudad_');" class="icon_save">
				</span>
			</div>

			<div class="columns two" style="font-weight:bold; color:#10689b; height:30px;">
				RETORNO :
			</div>
			<div class="columns four" style="height:30px;" onmouseover="revelar_icono_edicion('fecha_retorno__idciudad_');" onmouseout="ocultar_icono_edicion('fecha_retorno__idciudad_');">
				<span id="fecha_retorno__idciudad_">
					<span id="datos_fecha_retorno__idciudad_">&nbsp;
						_fecharetorno_
					</span>
					<img id="icon_editar_fecha_retorno__idciudad_" src="images/pencil.png" onclick="mostrar_edicion('fecha_retorno__idciudad_');" class="icon_edit">
				</span>
				<span id="editar_fecha_retorno__idciudad_" style="display:none;">
					<input type="text" placeholder="dd/mm/aaaa" name="fecha_retorno__idciudad_" id="valor_fecha_retorno__idciudad_" value="_fecharetorno_" style="width:150px;" />
					<img src="images/cross.png" onclick="ocultar_edicion('fecha_retorno__idciudad_');" class="icon_close">
					<img src="images/disk.png" onclick="guardar_atributo_x_ciudad_origen('pqt_ciudad_origen_paquete', 'fecha_retorno', 'idciudad_origen_paquete', '_idciudad_', 'fecha_retorno__idciudad_');" class="icon_save">
				</span>
			</div>

			<div class="clear_mayor"></div>

			<!-- PAQUETE INCLUYE -->
			<div class="columns fifteen" onmouseover="revelar_icono_edicion('paquete_incluye__idciudad_');" onmouseout="ocultar_icono_edicion('paquete_incluye__idciudad_');">
				<font style="font-weight:bold; color:#10689b; float:left;">PAQUETE INCLUYE:</font>
				<img id="icon_editar_paquete_incluye__idciudad_" src="images/pencil.png" onclick="mostrar_edicion('paquete_incluye__idciudad_');" class="icon_edit" style="margin-left:20px; float:left;">
				<br/>
				<span id="paquete_incluye__idciudad_">
					<span id="datos_paquete_incluye__idciudad_">
						<ul class="lista_descripcion" id="lista_paquete_incluye__idciudad_">		
							<li>
								<a>BOLETO AEREO: XXXXXXXXXXXXXXXXXXXXXXXXX.</a>
							</li>
							<li>
								<a>XXX  NOCHES DE ALOJAMIENTO HOTEL A ELECCION.</a>
							</li>
							<li>
								<a>SISTEMA DE ALIMENTACION DE ACUERDO A CADA HOTEL.</a>
							</li>
							<li>
								<a>TARJETA DE ASISTENCIA AL VIAJERO.</a>
							</li>
							<li>
								<a>TRASLADOS AEROPUERTO - HOTEL - AEROPUERTO.</a>
							</li>
						</ul>
					</span>
				</span>
				<span id="editar_paquete_incluye__idciudad_" style="display:none;">
					<textarea name="paquete_incluye__idciudad_" id="valor_paquete_incluye__idciudad_" style="width:500px;"><?php echo 'BOLETO AEREO: XXXXXXXXXXXXXXXXXXXXXXXXX.' . "\n" . 'XXX  NOCHES DE ALOJAMIENTO HOTEL A ELECCION.' . "\n" . 'SISTEMA DE ALIMENTACION DE ACUERDO A CADA HOTEL.' . "\n" . 'TARJETA DE ASISTENCIA AL VIAJERO.' . "\n" . 'TRASLADOS AEROPUERTO - HOTEL - AEROPUERTO.'; ?></textarea>
					<img src="images/cross.png" onclick="ocultar_edicion('paquete_incluye__idciudad_');" class="icon_close">
					<img src="images/disk.png" onclick="guardar_atributo_x_ciudad_origen('pqt_ciudad_origen_paquete', 'paquete_incluye', 'idciudad_origen_paquete', '_idciudad_', 'paquete_incluye__idciudad_');" class="icon_save">
				</span>
			</div>

			<div class="clear_mayor"></div>

			<!-- HOTELES -->
			<div class="columns fifteen">
				<font style="font-weight:bold; color:#10689b; float:left;">TARIFAS DE HOTELES:</font>
				<img src="images/plus32.png" class="right pth" onclick="show_hide_tarifa('pth');" title="HOTELES" />
				<img src="images/minus32.png" class="right hidden pth" onclick="show_hide_tarifa('pth');" title="HOTELES" />
				<div class="clear"></div>
				<div class="ppth hidden" >
					<div id="tarifas_hotel__ind_">
					</div>
					<input type="hidden" value="0" id="c_destinos__ind_" />
					<div>
						<a class="mylinkright" onclick="agregar_tabla_tarifa('_idciudad_', '_ind_');">
							<img src="images/add50.png" class="mini" />
							Nuevo Destino
						</a>
						<a class="mylinkright" onclick="editar_combinacion_hoteles(_idciudad_);">
							<img src="images/viewing.png" class="mini" />
							Combinacion de Hoteles
						</a>
					</div>
				</div>
			</div>

			<div class="clear_mayor"></div>
			<div class="clear_mayor"></div>
			<div class="clear_mayor"></div>
			<div class="clear_mayor"></div>

			<!-- OTROS SERVICIOS -->
			<div class="columns fifteen">
				<font style="font-weight:bold; color:#10689b; float:left;">OTROS SERVICIOS:</font>
				<img src="images/plus32.png" class="right pto" onclick="show_hide_tarifa('pto');" title="OTROS SERVICIOS" />
				<img src="images/minus32.png" class="right hidden pto" onclick="show_hide_tarifa('pto');" title="OTROS SERVICIOS" />
				<div class="clear"></div>
				<div class="ppto hidden" >
					<div id="otros_servicios__ind_">
						<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" id="detalle_otras_tarifas__ind_" class="con-borde" >
							<tr style="background-color:#10689b; color:#FFFFFF;">
								<th style="width:37%;">DETALLE</th>
								<th style="width:9%;">PRECIOS</th>
								<th style="width:18%;">FEE / IVA / TIME LIMIT</th>
								<th style="width:25%;">MONEDA / OBS</th>
								<th style="width:8%;">TOTALES</th>
								<th></th>
								<th>ACT</th>
							</tr>
						</table>
						<a class="mylinkright" onclick="guardar_otros_servicios('_ind_');">
							<img src="images/save32.png" class="mini" />
							Guardar Cambios
						</a>
						<a class="mylinkright" onclick="agregar_otro_servicio('_idciudad_', '_ind_');">
							<img src="images/add50.png" class="mini" />
							Agregar Otro Servicio
						</a>
						<input type="hidden" id="c_otras_tarifas__ind_" value="0" />
						<span id="eliminar_otras_tarifas__ind_" style="display:none;"></span>
					</div>
				</div>
			</div>

			<div class="clear_mayor"></div>
			<div class="clear_mayor"></div>
			<div class="clear_mayor"></div>
			<div class="clear_mayor"></div>

			<!-- BOLETOS AEREOS -->
			<div class="columns fifteen">
				<font style="font-weight:bold; color:#10689b; float:left;">BOLETOS AEREOS:</font>
				<img src="images/plus32.png" class="right ptb" onclick="show_hide_tarifa('ptb');" title="BOLETOS AEREOS" />
				<img src="images/minus32.png" class="right hidden ptb" onclick="show_hide_tarifa('ptb');" title="BOLETOS AEREOS" />
				<div class="clear"></div>
				<div class="pptb hidden" >
					<div id="tarifas_aereo__ind_">
					</div>
					<input type="hidden" id="c_tarifa_aereo__ind_" value="0" />
					<a class="mylinkright" onclick="agregar_tarifa_aereo('_idciudad_', '_ind_');">
						<img src="images/add50.png" class="mini" />
						Agregar Nueva Tarifa
					</a>
				</div>
			</div>

			<div class="clear_mayor"></div>
			<div class="clear_mayor"></div>
			<div class="clear_mayor"></div>
			<div class="clear_mayor"></div>

			<div class="columns fifteen" onmouseover="revelar_icono_edicion('importante__idciudad_');" onmouseout="ocultar_icono_edicion('importante__idciudad_');">
				<font style="font-weight:bold; color:#10689b; float:left;">IMPORTANTE:</font>
				<img id="icon_editar_importante__idciudad_" src="images/pencil.png" onclick="mostrar_edicion('importante__idciudad_');" class="icon_edit" style="margin-left:20px; float:left;">
				<br/>
				<span id="importante__idciudad_">
					<span id="datos_importante__idciudad_">
						<ul class="lista_descripcion" id="lista_importante__idciudad_">
							<li>
								<a>TARIFAS SUJETAS A CAMBIO SIN PREVIO AVISO SEGUN DISPONIBILIDAD, POR LO QUE NO SE GARANTIZA ESPACIOS NI PRECIOS HASTA LA EMISION DEL PAQUETE.</a>
							</li>
						</ul>
					</span>
				</span>
				<span id="editar_importante__idciudad_" style="display:none;">
					<textarea name="importante__idciudad_" id="valor_importante__idciudad_" style="width:500px;"><?php echo 'TARIFAS SUJETAS A CAMBIO SIN PREVIO AVISO SEGUN DISPONIBILIDAD, POR LO QUE NO SE GARANTIZA ESPACIOS NI PRECIOS HASTA LA EMISION DEL PAQUETE.'; ?></textarea>
					<img src="images/cross.png" onclick="ocultar_edicion('importante__idciudad_');" class="icon_close">
					<img src="images/disk.png" onclick="guardar_atributo_x_ciudad_origen('pqt_ciudad_origen_paquete', 'importante', 'idciudad_origen_paquete', '_idciudad_', 'importante__idciudad_');" class="icon_save">
				</span>
			</div>

			<div class="clear_mayor"></div>

			<div class="columns fifteen" onmouseover="revelar_icono_edicion('datos_agente__idciudad_');" onmouseout="ocultar_icono_edicion('datos_agente__idciudad_');">
				<font style="font-weight:bold; color:#10689b; float:left;">DATOS AGENTE:</font>
				<img id="icon_editar_datos_agente__idciudad_" src="images/pencil.png" onclick="mostrar_edicion('datos_agente__idciudad_');" class="icon_edit" style="margin-left:20px; float:left;">
				<br/>
				<span id="datos_agente__idciudad_">
					<span id="datos_datos_agente__idciudad_">
						<ul class="lista_descripcion" id="lista_datos_agente__idciudad_">
							<li>
								<a>DESCRIPCION</a>
							</li>	
						</ul>
					</span>
				</span>
				<span id="editar_datos_agente__idciudad_" style="display:none;">
					<textarea name="datos_agente__idciudad_" id="valor_datos_agente__idciudad_" style="width:500px;">DESCRIPCION</textarea>
					<img src="images/cross.png" onclick="ocultar_edicion('datos_agente__idciudad_');" class="icon_close">
					<img src="images/disk.png" onclick="guardar_atributo_x_ciudad_origen('pqt_ciudad_origen_paquete', 'datos_agente', 'idciudad_origen_paquete', '_idciudad_', 'datos_agente__idciudad_');" class="icon_save">
				</span>
			</div>

		</div>
	</div>
