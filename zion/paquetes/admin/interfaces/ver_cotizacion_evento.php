<?php
	include('header.php');
	include('../entidad/tipo_evento.php');
	include('../entidad/publicacion.php');
	include('../entidad/evento.php');
	include('../entidad/imagen.php');
	include('../entidad/archivo_adjunto.php');
	include('../entidad/operadora.php');
	include('../entidad/destino.php');
	include('../entidad/usuarios_paquetes.php');
	include('../control/c_mostrar_cotizacion_evento.php');
	include('../entidad/evento_cotizado.php');
	include('../entidad/clientes.php');
	include('../entidad/telefono.php');
	include('../entidad/e_mails.php');
	include('../entidad/seguimiento_cotizacion_evento.php');
	include('../entidad/tarifa_hotel.php');
	include('../entidad/tarifa_otros.php');
	include('../entidad/boleto_cotizado.php');
	include('../entidad/segmento_publicado.php');
	include('../entidad/lineas_aereas.php');
	include('../entidad/aeropuerto.php');
	include('../entidad/opcion_hoteles.php');
	include('../control/c_pax_evento.php');
	include("../entidad/pax_evento.php");
	include('../entidad/hotel_cotizado.php');
	include('../entidad/otros_cotizado.php');
	include('../entidad/inscripcion_cotizado.php');

	$idcotizacion 	= $_GET['cotizacion'];
	$tbl 			= 'evento_cotizado_has_pasajeros';
	$atr1			= 'idevento_cotizado';
	$atr2			= 'idclientes';
	$name_hoteles 	= array();
	$costo_hoteles 	= array();
	$idusuario 		= $_SESSION['idusuarios'];

	$c_cotizacion_evento 	= new c_mostrar_cotizacion_evento;
	$ruta_imagenes_ch_g 	= "../../online/js/slider_inicio/promo/data1/images/";

	if(isset($_POST['cotizacion_vendida']))
	{
		$cambio_estado = $c_cotizacion_evento->cerrar_cotizacion($idcotizacion, 'V');
	}
	if(isset($_POST['cotizacion_anulada']))
	{
		$cambio_estado = $c_cotizacion_evento->cerrar_cotizacion($idcotizacion, 'A');
	}

	$cotizacion 			= $c_cotizacion_evento->obtener_cotizacion_evento($idcotizacion);

	if($cotizacion === false)
	{
		echo "-- LA COTIZACION NO EXISTE --";
		exit(0);
	}
	
	$idpublicacion = $cotizacion['idevento'];

	$evento = $c_cotizacion_evento->obtener_evento($idpublicacion);

	if($evento === false)
	{
		echo "-- EL EVENTO NO HA SIDO ENCONTRADO --";
		exit(0);
	}

	$estado = '';
	switch ($cotizacion['estado_cotizacion']) 
	{
		case 'C':
			$estado = 'COTIZACION';
			break;

		case 'A':
			$estado = 'ANULADO';
			break;

		case 'V':
			$estado = 'VENDIDO';
			break;
		
		default:
			$estado = 'ANULADO';
			break;
	}
?>

	<script type="text/javascript">
	$(function() {
  		crear_calendario_cumpleanios('fecha_prox_seguimiento');
		$( "#tabs" ).tabs();
		actualizar_link();
		$('.editar_campo_link').hide();
	});

	function crear_calendario_cumpleanios(id) {
	    anioActual = new Date().getFullYear();
	    $("#" + id).datepicker({
	        showOn: "button",
	        buttonImage: "../images/calendar.jpg",
	        buttonImageOnly: true,
	        numberOfMonths: 3,
	        minDate: 0,
	        yearRange: anioActual + ":2025",
	        constrainInput: false
	    });  
	    $("#" + id).datepicker("option", "dateFormat", 'dd/mm/yy');
	}
	
	function mostrar_buscar_pax()
	{
		TINY.box.show({ url:'buscar_pasajeros.php', fixed: false, openjs:function(){$('#nombre_pax').focus();} });
	}

</script>
<div class="columns sixteen" style="background-color:#f1f1f1; padding-top:10px; padding-bottom:10px; font-size:9pt;">

	<h5><?php echo strtoupper($evento["titulo"] . '(' . $evento["nombre_generico"] . ')'); ?></h5>

	<div class="columns two" style="font-weight:bold; color:#10689b;">
		COTIZACION NRO
	</div>

	<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>

	<div class="columns four">
		<?php echo $idcotizacion; ?>
	</div>

	<div class="columns two" style="font-weight:bold; color:#10689b;">
		ESTADO
	</div>

	<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>

	<div class="columns four">
		<?php echo $estado; ?>
	</div>

	<div class="clear"></div>

	<!--<div class="columns two" style="font-weight:bold; color:#10689b;">
		OBSERVACIONES
	</div>

	<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>

	<div class="columns ten">
		<?php echo strtoupper($cotizacion['observaciones']); ?>
	</div>-->

	<div class="clear"></div>

	<div class="columns two" style="font-weight:bold; color:#10689b;">
		COTIZADO POR
	</div>

	<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>

	<div class="columns ten">
		<?php 
			$datos_usuario = $c_cotizacion_evento->obtener_datos_usuario($cotizacion['idusuarios']);
			echo strtoupper($datos_usuario['nombre_usuario']); 
		?>
	</div>

	<div class="clear_mayor"></div>	

	<div class="columns two" style="font-weight:bold; color:#10689b;">
		CLIENTE
	</div>

	<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>

	<div class="columns twelve">
		<?php
			$cliente = $c_cotizacion_evento->obtener_datos_cliente($cotizacion['idclientes']);
			echo strtoupper($cliente['trato_cliente'] . ' ' . $cliente['nombre_cliente'] . ' ' . $cliente['apellido_cliente']);
		?>
	</div>

	<?php
		if($cotizacion['idsolicitante'] != 0) 
		{
			$solicitante = $c_cotizacion_evento->obtener_datos_cliente($cotizacion['idsolicitante']);
	?>
		<div class="columns two" style="font-weight:bold; color:#10689b;">
			SOLICITADO POR
		</div>
		<div class="columns one" style="font-weight:bold; color:#10689b;">
			:
		</div>
		<div class="columns twelve">
	<?
			if($solicitante !== false)
			{
				echo strtoupper($solicitante['trato_cliente'] . ' ' . $solicitante['nombre_cliente'] . ' ' . $solicitante['apellido_cliente']);
			}
			else
			{
				echo 'EL SOLICITANTE NO HA SIDO ENCONTRADO';
			}
	?>
		</div>
	<?php
		}
	?>

	<div class="clear"></div>

	<div class="columns two" style="font-weight:bold; color:#10689b;">
		TELEFONOS
	</div>

	<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>

	<div class="columns four"  onmouseover="revelar_icono_edicion_multiple('destino');" onmouseout="ocultar_icono_edicion_multiple('destino');" style="line-height:1;">
		<?php 
			$telefonos = $c_cotizacion_evento->obtener_telefonos_cliente($cotizacion['idclientes']);
			if($telefonos !== false)
			{
				for ($i=0; $i < count($telefonos); $i++) 
				{ 
					$telefono = $telefonos[$i];
					echo $telefono['numero_telefono'] . '(' . strtoupper($telefono['tipo_numero_telefono']) . ')<br/>';
				}
			}
			else
			{
				echo "--NO INGRESADO--";
			}
		?>
	</div>

	<div class="columns two" style="font-weight:bold; color:#10689b;">
		CORREOS
	</div>

	<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>

	<div class="columns four"  onmouseover="revelar_icono_edicion_multiple('destino');" onmouseout="ocultar_icono_edicion_multiple('destino');" style="line-height:1;">
		<?php 
			$correos = $c_cotizacion_evento->obtener_mails_cliente($cotizacion['idclientes']);
			if($correos !== false)
			{
				for ($i=0; $i < count($correos); $i++) 
				{ 
					$correo = $correos[$i];
					echo $correo['e_mail'] . '(' . strtoupper($correo['e_mail_tipo']) . ')<br/>';
				}
			}
			else
			{
				echo "--NO INGRESADO--";
			}
		?>
	</div>

	<div class="clear_mayor"></div>

	<div class="columns two" style="font-weight:bold; color:#10689b;">DESDE</div>

	<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>

	<div class="columns four"><?php echo strtoupper($evento["salida"]); ?></div>

	<div class="columns two" style="font-weight:bold; color:#10689b;">HASTA</div>

	<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>

	<div class="columns four"><?php echo strtoupper($evento["retorno"]); ?></div>
	
	<div class="clear"></div>

	<div class="columns two" style="font-weight:bold; color:#10689b;">
		DESTINO(S)
	</div>

	<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>

	<div class="columns four"  onmouseover="revelar_icono_edicion_multiple('destino');" onmouseout="ocultar_icono_edicion_multiple('destino');">
		<div class="columns three" style="margin-left:0px; margin-right:10px;" id="contenedor_destino">
			<?php 
				$destinos = evento::obtener_destinos_por_evento($idpublicacion);
				if(count($destinos) == 0)
					echo "- No hay ninguno -";
				for($i = 0; $i < count($destinos); $i++)
				{
					$nombre_destino 	= $destinos[$i]["nombre_destino"];
					$iddestino 			= $destinos[$i]["iddestino"];
			?>
				<div id="destino_<?php echo $iddestino; ?>">
					<?php echo strtoupper($nombre_destino); ?>
				</div>
			<?php
				}
			?>
		</div>
	</div>

</div>
<div class="clear_mayor"></div>
<div class="clear_mayor"></div>

<div class="columns sixteen">

	<span id="pest_cot" class="mytab tab-crrnt" onclick="cambiar_tab('pest_cot', 'vis_cot', 'pnl_cot', 'mytab', 'tab-cot', 'tab-crrnt');">COTIZACION</span>

	<span id="pest_paq" class="mytab tab-cot" onclick="cambiar_tab('pest_paq', 'vis_ag', 'pnl_cot', 'mytab', 'tab-cot', 'tab-crrnt');">DETALLES DE LA TARIFA</span>
 
	<span id="pest_pax" class="mytab tab-cot" onclick="cambiar_tab('pest_pax', 'vis_pax', 'pnl_cot', 'mytab', 'tab-cot', 'tab-crrnt');">PASAJEROS</span>

</div>

<div id="vis_cot" class="pnl_cot">	
	<div class="columns sixteen" style="background-color:#f1f1f1;padding-top:10px; padding-bottom:10px;">
		<?php
			if($cotizacion['paquete_incluye'] != "")
			{
		?>
			<div class="columns fifteen">

				<strong>PAQUETE INCLUYE</strong>

				<div class="columns fifteen">
					<ul class="lista_descripcion" id="lista_descripcion">
						<?php
							$paquete_incluye = explode("\n", $cotizacion['paquete_incluye']);
							for($i=0; $i<count($paquete_incluye); $i++)
							{
								if($paquete_incluye[$i] != "")
								{
						?>
									<li <?php if($paquete_incluye[$i]{0} == '@') { ?> style="font-weight:bold; list-style:square inside;" <?php } else { ?> style="font-size:7pt; margin-left:30px;" <?php } ?> >
			                            <a <?php if($paquete_incluye[$i]{0} != '@') {  ?> style="font-size:9pt;" <?php } else { $paquete_incluye[$i] = substr($paquete_incluye[$i], 1) ; } ?> id="paquete_incluye_dato_<?php echo $i; ?>" ><?php echo $paquete_incluye[$i]; ?></a>
			                        </li>
						<?php
								}
							}
						?>		
					</ul>
				</div>

			</div>
		<?php
			}
			if($cotizacion['importante'] != "")
			{
		?>
			<div class="columns fifteen">

				<strong>IMPORTANTE</strong>
				
				<div class="columns fifteen">
					<ul class="lista_descripcion" id="lista_importante">
						<?php
							$importante = explode("\n", $cotizacion['importante']);
							for($i=0; $i<count($importante); $i++)
							{
								if($importante[$i] != "")
								{
						?>
									<li>
										<a style="font-size:9pt;" id="importante_dato_<?php echo $i; ?>" ><?php echo $importante[$i]; ?></a>
									</li>
						<?php
								}
							}
						?>		
					</ul>
				</div>

			</div>
		<?php
			}

			$hoteles 					= $c_cotizacion_evento->obtener_combinaciones_hoteles($idcotizacion);
			$precios_boletos 			= $c_cotizacion_evento->obtener_boletos_cotizados($idcotizacion);

			$precios_otros_servicios 	= $c_cotizacion_evento->obtener_otras_tarifas($idcotizacion);

			$total_otros_serv_mas_boletos_adt = $precios_otros_servicios['total_adt'];
			$total_otros_serv_mas_boletos_cnn = $precios_otros_servicios['total_cnn'];
			$total_otros_serv_mas_boletos_inf = $precios_otros_servicios['total_inf'];

			$total_boletos_adt 	= $precios_boletos['total_adt'];
			$total_boletos_cnn 	= $precios_boletos['total_cnn'];
			$total_boletos_inf 	= $precios_boletos['total_inf'];

			$tarifas_hotel = $c_cotizacion_evento->obtener_tarifas_hotel_por_evento($idcotizacion);
			
		?>

		<div class="precios_separado">

			<div class="fifteen columns">
				<strong>TARIFAS DE HOTEL EN DOLARES AMERICANOS</strong>
				<?php
					$columnas_ocultar   = $c_cotizacion_evento->evaluar_vista_hoteles($hoteles);
				?>
					<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" class="con-borde" >
						<tr style="background-color:#10689b; color:#FFFFFF;">
							<th>HOTEL</th>
							<?php if(strpos($columnas_ocultar, 'SGL') !== false) { ?><th>SINGLE</th><?php } ?>
							<?php if(strpos($columnas_ocultar, 'DBL') !== false) { ?><th>DOBLE</th><?php } ?>
							<?php if(strpos($columnas_ocultar, 'TPL') !== false) { ?><th>TRIPLE</th><?php } ?>
							<?php if(strpos($columnas_ocultar, 'CPL') !== false) { ?><th>CDRPLE</th><?php } ?>
							<?php if(strpos($columnas_ocultar, 'CNN') !== false) { ?><th>MENOR</th><?php } ?>
							<?php if(strpos($columnas_ocultar, 'INF') !== false) { ?><th>INFANTE</th><?php } ?>
							<th>ALIMENTACION</th>
							<th>OBS.</th>
						</tr>
				<?php
					for ($i=0; $i < count($hoteles); $i++) 
					{ 
						$tarifa_hotel 	= $hoteles[$i];
				?>
						<tr>
							<td style="line-height:1.2;">
								<?php echo $tarifa_hotel['hotel']; ?>
							</td>
							<?php if(strpos($columnas_ocultar, 'SGL') !== false) { ?>
							<td>
								<?php echo (ceil($tarifa_hotel['precio_single'] + $total_otros_serv_mas_boletos_adt)); ?>
							</td>
							<?php }
							 if(strpos($columnas_ocultar, 'DBL') !== false) {  ?>
							<td>
								<?php echo (ceil($tarifa_hotel['precio_doble'] + $total_otros_serv_mas_boletos_adt)); ?>
							</td>
							<?php }
							 if(strpos($columnas_ocultar, 'TPL') !== false) {  ?>
							<td>
								<?php echo (ceil($tarifa_hotel['precio_triple'] + $total_otros_serv_mas_boletos_adt)); ?>
							</td>
							<?php }
							 if(strpos($columnas_ocultar, 'CPL') !== false) {  ?>
							<td>
								<?php echo (ceil($tarifa_hotel['precio_cuadruple'] + $total_otros_serv_mas_boletos_adt)); ?>
							</td>
							<?php }
							 if(strpos($columnas_ocultar, 'CNN') !== false) {  ?>
							<td>
								<?php echo (ceil($tarifa_hotel['precio_menor'] + $total_otros_serv_mas_boletos_cnn)); ?>
							</td>
							<?php }
							 if(strpos($columnas_ocultar, 'INF') !== false) {  ?>
							<td>
								<?php echo (ceil($tarifa_hotel['precio_infante'] + $total_otros_serv_mas_boletos_inf)); ?>
							</td>
							<?php } ?>
							<td style="line-height:1.2;">
								<?php echo $tarifa_hotel['alimentacion']; ?>
							</td>
							<td style="line-height:1.2;">
								<?php echo $tarifa_hotel['info_extra']; ?>
							</td>
						</tr>
				<?php
					}
				?>
					</table>
			</div>

			<div class="clear_mayor"></div>

			<div class="fifteen columns">
				<strong>BOLETOS AEREOS</strong>
				<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" id="detalle_boletos_cotizados" class="con-borde" >
					<tr style="background-color:#10689b; color:#FFFFFF;">
						<th>LINEA AEREA</th>
						<th>RUTA</th>
						<th>PRECIO ADULTO</th>
						<th>PRECIO MENOR</th>
						<th>PRECIO INFANTE</th>
						<th>T LIMIT</th>
					</tr>
					<?php
						for ($i=0; array_key_exists($i, $precios_boletos); $i++) 
						{ 
							$boleto_cotizado 	= $precios_boletos[$i];
							$idboleto 			= $boleto_cotizado['idboleto_cotizado_evento_cotizado'];
							$estado 			= $boleto_cotizado['estado'];
							if($estado == '1')
							{
					?>
					<tr class="boleto_cotizado" id="boletocotizado_<?php echo $i; ?>" >
						<td>
							<?php echo strtoupper($c_cotizacion_evento->obtener_linea_aerea($boleto_cotizado['linea_aerea'])); ?>
						</td>
						<td>
							<?php echo strtoupper($boleto_cotizado['ruta']); ?>
						</td>
						<td>
							<?php echo $boleto_cotizado['subtotal_adt']; ?>
						</td>
						<td>
							<?php echo $boleto_cotizado['subtotal_cnn']; ?>
						</td>
						<td>
							<?php echo $boleto_cotizado['subtotal_inf']; ?>
						</td>
						<td>
							<?php echo date('d/M/Y', strtotime($boleto_cotizado['time_limit'])); ?>
						</td>
					</tr>
					<?php
							}
						}
					?>
				</table>
			</div>

			<div class="clear_mayor"></div>

			<div class="fifteen columns">
				<strong>ITINERARIO</strong>
				<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" class="con-borde" >
					<tr style="background-color:#10689b; color:#FFFFFF;">
						<th>LINEA AEREA</th>
						<th>VUELO</th>
						<th>FECHA</th>
						<th>ORIGEN</th>
						<th>SALE</th>
						<th>DESTINO</th>
						<th>LLEGA</th>
					</tr>
					<?php
						$itinerario = $c_cotizacion_evento->obtener_segmentos_publicados($idcotizacion);
						if($itinerario === false)
						{
							$itinerario = array();
						}
						
						for ($i=0; $i < count($itinerario); $i++) 
						{ 
							$segmento 	= $itinerario[$i];
							$idsegmento = $segmento['idsegmento_evento'];

							$linea_aerea 		= $c_cotizacion_evento->obtener_linea_aerea($segmento['idlineas_aereas']);
							$aeropuerto_sale 	= $c_cotizacion_evento->obtener_aeropuerto($segmento['origen']);
							$aeropuerto_llega 	= $c_cotizacion_evento->obtener_aeropuerto($segmento['destino']);
					?>
					<tr>
						<td><?php echo $linea_aerea; ?></td>
						<td><?php echo $segmento['nro_vuelo']; ?></td>
						<td><?php echo date('d/m/Y', strtotime($segmento['fecha'])); ?></td>
						<td><?php echo $aeropuerto_sale; ?></td>
						<td><?php echo substr($segmento['hora_sale'], 0, 5); ?></td>
						<td><?php echo $aeropuerto_llega; ?></td>
						<td><?php echo substr($segmento['hora_llega'], 0, 5); ?></td>
					</tr>
					<?php
						}
					?>
				</table>
			</div>
			<?php
				//$opciones = $c_cotizacion_evento->obtener_opciones_de_vuelos($idcotizacion); 
				//echo "<hr/><pre>"; print_r($opciones); echo "</pre>"; 
			?>
		</div>

		<div class="precios_resumido" style="display:none;">

			<div class="fifteen columns">
				<strong>TARIFAS EXPRESADAS EN DOLARES AMERICANOS</strong>
				<?php
					$columnas_ocultar   = $c_cotizacion_evento->evaluar_vista_hoteles($hoteles);
				?>
					<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" class="con-borde" >
						<tr style="background-color:#10689b; color:#FFFFFF;">
							<th>HOTEL</th>
							<?php if(strpos($columnas_ocultar, 'SGL') !== false) { ?><th>SINGLE</th><?php } ?>
							<?php if(strpos($columnas_ocultar, 'DBL') !== false) { ?><th>DOBLE</th><?php } ?>
							<?php if(strpos($columnas_ocultar, 'TPL') !== false) { ?><th>TRIPLE</th><?php } ?>
							<?php if(strpos($columnas_ocultar, 'CPL') !== false) { ?><th>CDRPLE</th><?php } ?>
							<?php if(strpos($columnas_ocultar, 'CNN') !== false) { ?><th>MENOR</th><?php } ?>
							<?php if(strpos($columnas_ocultar, 'INF') !== false) { ?><th>INFANTE</th><?php } ?>
							<th>ALIMENTACION</th>
							<th>OBS.</th>
						</tr>
				<?php
					for ($i=0; $i < count($hoteles); $i++) 
					{ 
						$tarifa_hotel 	= $hoteles[$i];
				?>
						<tr>
							<td style="line-height:1.2;">
								<?php echo $tarifa_hotel['hotel']; ?>
							</td>
							<?php if(strpos($columnas_ocultar, 'SGL') !== false) { ?>
							<td>
								<?php echo (ceil($tarifa_hotel['precio_single'] + $total_otros_serv_mas_boletos_adt + $total_boletos_adt)); ?>
							</td>
							<?php }
							 if(strpos($columnas_ocultar, 'DBL') !== false) {  ?>
							<td>
								<?php echo (ceil($tarifa_hotel['precio_doble'] + $total_otros_serv_mas_boletos_adt + $total_boletos_adt)); ?>
							</td>
							<?php }
							 if(strpos($columnas_ocultar, 'TPL') !== false) {  ?>
							<td>
								<?php echo (ceil($tarifa_hotel['precio_triple'] + $total_otros_serv_mas_boletos_adt + $total_boletos_adt)); ?>
							</td>
							<?php }
							 if(strpos($columnas_ocultar, 'CPL') !== false) {  ?>
							<td>
								<?php echo (ceil($tarifa_hotel['precio_cuadruple'] + $total_otros_serv_mas_boletos_adt + $total_boletos_adt)); ?>
							</td>
							<?php }
							 if(strpos($columnas_ocultar, 'CNN') !== false) {  ?>
							<td>
								<?php echo (ceil($tarifa_hotel['precio_menor'] + $total_otros_serv_mas_boletos_cnn + $total_boletos_cnn)); ?>
							</td>
							<?php }
							 if(strpos($columnas_ocultar, 'INF') !== false) {  ?>
							<td>
								<?php echo (ceil($tarifa_hotel['precio_infante'] + $total_otros_serv_mas_boletos_inf + $total_boletos_inf)); ?>
							</td>
							<?php } ?>
							<td style="line-height:1.2;">
								<?php echo $tarifa_hotel['alimentacion']; ?>
							</td>
							<td style="line-height:1.2;">
								<?php echo $tarifa_hotel['info_extra']; ?>
							</td>
						</tr>
				<?php
					}
				?>
					</table>
			</div>

			<div class="clear_mayor"></div>

			<div class="fifteen columns">
				<strong>ITINERARIO</strong>
				<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" class="con-borde" >
					<tr style="background-color:#10689b; color:#FFFFFF;">
						<th>LINEA AEREA</th>
						<th>VUELO</th>
						<th>FECHA</th>
						<th>ORIGEN</th>
						<th>SALE</th>
						<th>DESTINO</th>
						<th>LLEGA</th>
					</tr>
					<?php
						
						for ($i=0; $i < count($itinerario); $i++) 
						{ 
							$segmento 	= $itinerario[$i];
							$idsegmento = $segmento['idsegmento_evento'];

							$linea_aerea 		= $c_cotizacion_evento->obtener_linea_aerea($segmento['idlineas_aereas']);
							$aeropuerto_sale 	= $c_cotizacion_evento->obtener_aeropuerto($segmento['origen']);
							$aeropuerto_llega 	= $c_cotizacion_evento->obtener_aeropuerto($segmento['destino']);
					?>
					<tr>
						<td><?php echo $linea_aerea; ?></td>
						<td><?php echo $segmento['nro_vuelo']; ?></td>
						<td><?php echo date('d/m/Y', strtotime($segmento['fecha'])); ?></td>
						<td><?php echo $aeropuerto_sale; ?></td>
						<td><?php echo substr($segmento['hora_sale'], 0, 5); ?></td>
						<td><?php echo $aeropuerto_llega; ?></td>
						<td><?php echo substr($segmento['hora_llega'], 0, 5); ?></td>
					</tr>
					<?php
						}
					?>
				</table>
			</div>

		</div>

		<div class="precios_por_opciones_aereo_detallado" style="display:none;">

			<div class="fifteen columns">
				
				<?php
					$columnas_ocultar   = $c_cotizacion_evento->evaluar_vista_hoteles($hoteles);

					$opciones_aereo = $c_cotizacion_evento->obtener_opciones_de_vuelos($idcotizacion);
					//echo "<pre>"; print_r($opciones_aereo); echo "</pre>";
					for ($j=0; $j < count($opciones_aereo); $j++) 
					{
						$tarifa_aereo = $opciones_aereo[$j];
						if($tarifa_aereo['estado'] == 1)
						{
					?>
						<h4><?php echo $tarifa_aereo['descripcion']; ?>, SALE DE <?php echo $tarifa_aereo['salida']; ?></h4>
						<strong style="margin-left:3%;">TARIFAS EXPRESADAS EN DOLARES AMERICANOS: HOTELES</strong>
							<table style="width:97%; border:1px solid #AAAAAA; font-size:8pt; margin-left:3%;" class="con-borde" >
								<tr style="background-color:#10689b; color:#FFFFFF;">
									<th>HOTEL</th>
									<?php if(strpos($columnas_ocultar, 'SGL') !== false) { ?><th>SINGLE</th><?php } ?>
									<?php if(strpos($columnas_ocultar, 'DBL') !== false) { ?><th>DOBLE</th><?php } ?>
									<?php if(strpos($columnas_ocultar, 'TPL') !== false) { ?><th>TRIPLE</th><?php } ?>
									<?php if(strpos($columnas_ocultar, 'CPL') !== false) { ?><th>CDRPLE</th><?php } ?>
									<?php if(strpos($columnas_ocultar, 'CNN') !== false) { ?><th>MENOR</th><?php } ?>
									<?php if(strpos($columnas_ocultar, 'INF') !== false) { ?><th>INFANTE</th><?php } ?>
									<th>ALIMENTACION</th>
									<th>OBS.</th>
								</tr>
						<?php
							for ($i=0; $i < count($hoteles); $i++) 
							{ 
								$tarifa_hotel 	= $hoteles[$i];
						?>
								<tr>
									<td style="line-height:1.2;">
										<?php echo $tarifa_hotel['hotel']; ?>
									</td>
									<?php if(strpos($columnas_ocultar, 'SGL') !== false) { ?>
									<td>
										<?php echo (ceil($tarifa_hotel['precio_single'] + $total_otros_serv_mas_boletos_adt)); ?>
									</td>
									<?php }
									 if(strpos($columnas_ocultar, 'DBL') !== false) {  ?>
									<td>
										<?php echo (ceil($tarifa_hotel['precio_doble'] + $total_otros_serv_mas_boletos_adt)); ?>
									</td>
									<?php }
									 if(strpos($columnas_ocultar, 'TPL') !== false) {  ?>
									<td>
										<?php echo (ceil($tarifa_hotel['precio_triple'] + $total_otros_serv_mas_boletos_adt)); ?>
									</td>
									<?php }
									 if(strpos($columnas_ocultar, 'CPL') !== false) {  ?>
									<td>
										<?php echo (ceil($tarifa_hotel['precio_cuadruple'] + $total_otros_serv_mas_boletos_adt)); ?>
									</td>
									<?php }
									 if(strpos($columnas_ocultar, 'CNN') !== false) {  ?>
									<td>
										<?php echo (ceil($tarifa_hotel['precio_menor'] + $total_otros_serv_mas_boletos_cnn)); ?>
									</td>
									<?php }
									 if(strpos($columnas_ocultar, 'INF') !== false) {  ?>
									<td>
										<?php echo (ceil($tarifa_hotel['precio_infante'] + $total_otros_serv_mas_boletos_inf)); ?>
									</td>
									<?php } ?>
									<td style="line-height:1.2;">
										<?php echo $tarifa_hotel['alimentacion']; ?>
									</td>
									<td style="line-height:1.2;">
										<?php echo $tarifa_hotel['info_extra']; ?>
									</td>
								</tr>
						<?php
							}
						?>
							</table>
							<strong style="margin-left:3%;">PRECIOS DE BOLETOS AEREOS</strong>
							<table style="width:97%; border:1px solid #AAAAAA; font-size:8pt; margin-left:3%;" id="detalle_boletos_cotizados" class="con-borde" >
								<tr style="background-color:#10689b; color:#FFFFFF;">
									<th>LINEA AEREA</th>
									<th>RUTA</th>
									<th>PRECIO ADULTO</th>
									<th>PRECIO MENOR</th>
									<th>PRECIO INFANTE</th>
									<th>T LIMIT</th>
								</tr>
								<?php
									$precios_boletos_opc = $tarifa_aereo['boletos'];
									for ($i=0; array_key_exists($i, $precios_boletos_opc); $i++) 
									{ 
										$boleto_cotizado 	= $precios_boletos_opc[$i];
										$idboleto 			= $boleto_cotizado['idboleto_cotizado_evento_cotizado'];
										$fact 				= (1+($boleto_cotizado['fee']/100)) * (1+($boleto_cotizado['factura']/100));
										$estado 			= $boleto_cotizado['estado'];
										if($estado == '1')
										{
											if($boleto_cotizado['precio_adulto'] > 0)
												$adt = ceil($boleto_cotizado['precio_adulto'] * $fact) . ' ' . $boleto_cotizado['nmoneda'];
											else
												$adt = '-';
											if($boleto_cotizado['precio_menor'] > 0)
												$cnn = ceil($boleto_cotizado['precio_menor'] * $fact) . ' ' . $boleto_cotizado['nmoneda'];
											else
												$cnn = '-';
											if($boleto_cotizado['precio_infante'] > 0)
												$inf = ceil($boleto_cotizado['precio_infante'] * $fact) . ' ' . $boleto_cotizado['nmoneda'];
											else
												$inf = '-';
								?>
								<tr>
									<td>
										<?php echo strtoupper($c_cotizacion_evento->obtener_linea_aerea($boleto_cotizado['linea_aerea'])); ?>
									</td>
									<td>
										<?php echo strtoupper($boleto_cotizado['ruta']); ?>
									</td>
									<td>
										<?php echo $adt; ?>
									</td>
									<td>
										<?php echo $cnn; ?>
									</td>
									<td>
										<?php echo $inf; ?>
									</td>
									<td>
										<?php echo date('d/M/Y', strtotime($boleto_cotizado['time_limit'])); ?>
									</td>
								</tr>
								<?php
										}
									}
								?>
							</table>
							<strong style="margin-left:3%;">ITINERARIO</strong>
							<table style="width:97%; border:1px solid #AAAAAA; font-size:8pt; margin-left:3%;" class="con-borde" >
								<tr style="background-color:#10689b; color:#FFFFFF;">
									<th>LINEA AEREA</th>
									<th>VUELO</th>
									<th>FECHA</th>
									<th>ORIGEN</th>
									<th>SALE</th>
									<th>DESTINO</th>
									<th>LLEGA</th>
								</tr>
								<?php
									$itinerario_opc = $tarifa_aereo['itinerario'];

									if($itinerario_opc === false)
										$itinerario_opc = array();
									
									for ($i=0; $i < count($itinerario_opc); $i++) 
									{ 
										$segmento 	= $itinerario_opc[$i];
										$idsegmento = $segmento['idsegmento_evento'];

										$linea_aerea 		= $c_cotizacion_evento->obtener_linea_aerea($segmento['idlineas_aereas']);
										$aeropuerto_sale 	= $c_cotizacion_evento->obtener_aeropuerto($segmento['origen']);
										$aeropuerto_llega 	= $c_cotizacion_evento->obtener_aeropuerto($segmento['destino']);
								?>
								<tr>
									<td><?php echo $linea_aerea; ?></td>
									<td><?php echo $segmento['nro_vuelo']; ?></td>
									<td><?php echo date('d/m/Y', strtotime($segmento['fecha'])); ?></td>
									<td><?php echo $aeropuerto_sale; ?></td>
									<td><?php echo substr($segmento['hora_sale'], 0, 5); ?></td>
									<td><?php echo $aeropuerto_llega; ?></td>
									<td><?php echo substr($segmento['hora_llega'], 0, 5); ?></td>
								</tr>
								<?php
									}
								?>
							</table>
				<?php
						}
					}
				?>
					
			</div>

		</div>

		<div class="precios_por_opciones_aereo_resumido" style="display:none;">

			<div class="fifteen columns">
				<?php
					for ($j=0; $j < count($opciones_aereo); $j++) 
					{
						$tarifa_aereo = $opciones_aereo[$j];
						if($tarifa_aereo['estado'] == 1)
						{
							$adt =  $tarifa_aereo['adt'];
							$cnn =  $tarifa_aereo['cnn'];
							$inf =  $tarifa_aereo['inf'];
				?>
						<h4><?php echo $tarifa_aereo['descripcion']; ?>, SALE DE <?php echo $tarifa_aereo['salida']; ?></h4>
						<strong style="margin-left:3%;">TARIFAS EXPRESADAS EN DOLARES AMERICANOS: HOTEL MAS BOLETO AEREO</strong>
							<table style="width:97%; border:1px solid #AAAAAA; font-size:8pt; margin-left:3%;" class="con-borde" >
								<tr style="background-color:#10689b; color:#FFFFFF;">
									<th>HOTEL</th>
									<?php if(strpos($columnas_ocultar, 'SGL') !== false) { ?><th>SINGLE</th><?php } ?>
									<?php if(strpos($columnas_ocultar, 'DBL') !== false) { ?><th>DOBLE</th><?php } ?>
									<?php if(strpos($columnas_ocultar, 'TPL') !== false) { ?><th>TRIPLE</th><?php } ?>
									<?php if(strpos($columnas_ocultar, 'CPL') !== false) { ?><th>CDRPLE</th><?php } ?>
									<?php if(strpos($columnas_ocultar, 'CNN') !== false) { ?><th>MENOR</th><?php } ?>
									<?php if(strpos($columnas_ocultar, 'INF') !== false) { ?><th>INFANTE</th><?php } ?>
									<th>ALIMENTACION</th>
									<th>OBS.</th>
								</tr>
						<?php
							for ($i=0; $i < count($hoteles); $i++) 
							{ 
								$tarifa_hotel 	= $hoteles[$i];
						?>
								<tr>
									<td style="line-height:1.2;">
										<?php echo $tarifa_hotel['hotel']; ?>
									</td>
									<?php if(strpos($columnas_ocultar, 'SGL') !== false) { ?>
									<td>
										<?php echo (ceil($tarifa_hotel['precio_single'] + $total_otros_serv_mas_boletos_adt + $adt)); ?>
									</td>
									<?php }
									 if(strpos($columnas_ocultar, 'DBL') !== false) {  ?>
									<td>
										<?php echo (ceil($tarifa_hotel['precio_doble'] + $total_otros_serv_mas_boletos_adt + $adt)); ?>
									</td>
									<?php }
									 if(strpos($columnas_ocultar, 'TPL') !== false) {  ?>
									<td>
										<?php echo (ceil($tarifa_hotel['precio_triple'] + $total_otros_serv_mas_boletos_adt + $adt)); ?>
									</td>
									<?php }
									 if(strpos($columnas_ocultar, 'CPL') !== false) {  ?>
									<td>
										<?php echo (ceil($tarifa_hotel['precio_cuadruple'] + $total_otros_serv_mas_boletos_adt + $adt)); ?>
									</td>
									<?php }
									 if(strpos($columnas_ocultar, 'CNN') !== false) {  ?>
									<td>
										<?php echo (ceil($tarifa_hotel['precio_menor'] + $total_otros_serv_mas_boletos_cnn + $cnn)); ?>
									</td>
									<?php }
									 if(strpos($columnas_ocultar, 'INF') !== false) {  ?>
									<td>
										<?php echo (ceil($tarifa_hotel['precio_infante'] + $total_otros_serv_mas_boletos_inf + $inf)); ?>
									</td>
									<?php } ?>
									<td style="line-height:1.2;">
										<?php echo $tarifa_hotel['alimentacion']; ?>
									</td>
									<td style="line-height:1.2;">
										<?php echo $tarifa_hotel['info_extra']; ?>
									</td>
								</tr>
						<?php
							}
						?>
							</table>
							<strong style="margin-left:3%;">ITINERARIO</strong>
							<table style="width:97%; border:1px solid #AAAAAA; font-size:8pt; margin-left:3%;" class="con-borde" >
								<tr style="background-color:#10689b; color:#FFFFFF;">
									<th>LINEA AEREA</th>
									<th>VUELO</th>
									<th>FECHA</th>
									<th>ORIGEN</th>
									<th>SALE</th>
									<th>DESTINO</th>
									<th>LLEGA</th>
								</tr>
								<?php
									$itinerario_opc = $tarifa_aereo['itinerario'];

									if($itinerario_opc === false)
										$itinerario_opc = array();
									
									for ($i=0; $i < count($itinerario_opc); $i++) 
									{ 
										$segmento 	= $itinerario_opc[$i];
										$idsegmento = $segmento['idsegmento_evento'];

										$linea_aerea 		= $c_cotizacion_evento->obtener_linea_aerea($segmento['idlineas_aereas']);
										$aeropuerto_sale 	= $c_cotizacion_evento->obtener_aeropuerto($segmento['origen']);
										$aeropuerto_llega 	= $c_cotizacion_evento->obtener_aeropuerto($segmento['destino']);
								?>
								<tr>
									<td><?php echo $linea_aerea; ?></td>
									<td><?php echo $segmento['nro_vuelo']; ?></td>
									<td><?php echo date('d/m/Y', strtotime($segmento['fecha'])); ?></td>
									<td><?php echo $aeropuerto_sale; ?></td>
									<td><?php echo substr($segmento['hora_sale'], 0, 5); ?></td>
									<td><?php echo $aeropuerto_llega; ?></td>
									<td><?php echo substr($segmento['hora_llega'], 0, 5); ?></td>
								</tr>
								<?php
									}
								?>
							</table>
				<?php
						}
					}
				?>
					
			</div>

		</div>

		<!--<div class="fifteen columns">
			<strong>ITINERARIO</strong>
			<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" class="con-borde" >
				<tr style="background-color:#10689b; color:#FFFFFF;">
					<th>LINEA AEREA</th>
					<th>VUELO</th>
					<th>FECHA</th>
					<th>ORIGEN</th>
					<th>SALE</th>
					<th>DESTINO</th>
					<th>LLEGA</th>
				</tr>
				<?php
					$itinerario = $c_cotizacion_evento->obtener_segmentos_publicados($idcotizacion);
					if($itinerario === false)
					{
						$itinerario = array();
					}
					
					for ($i=0; $i < count($itinerario); $i++) 
					{ 
						$segmento 	= $itinerario[$i];
						$idsegmento = $segmento['idsegmento_evento'];

						$linea_aerea 		= $c_cotizacion_evento->obtener_linea_aerea($segmento['idlineas_aereas']);
						$aeropuerto_sale 	= $c_cotizacion_evento->obtener_aeropuerto($segmento['origen']);
						$aeropuerto_llega 	= $c_cotizacion_evento->obtener_aeropuerto($segmento['destino']);
				?>
				<tr>
					<td><?php echo $linea_aerea; ?></td>
					<td><?php echo $segmento['nro_vuelo']; ?></td>
					<td><?php echo date('d/m/Y', strtotime($segmento['fecha'])); ?></td>
					<td><?php echo $aeropuerto_sale; ?></td>
					<td><?php echo substr($segmento['hora_sale'], 0, 5); ?></td>
					<td><?php echo $aeropuerto_llega; ?></td>
					<td><?php echo substr($segmento['hora_llega'], 0, 5); ?></td>
				</tr>
				<?php
					}
				?>
			</table>
		</div>-->

		<div class="clear_mayor"></div>

		<div class="fifteen columns">
			<strong>INSCRIPCIONES</strong>
			<a class="mylink" onclick="$('#inscripcion_image').toggle();">Mostrar tabla de categorias</a>
			<?php
		    if($evento['imagen_grande'] != "")
		    {
		    ?>
		    	<center><img id="inscripcion_image" src="<?php echo $ruta_imagenes_ch_g . $evento['imagen_grande']; ?>" style="width:70%; display:none;" /></center>
		    	<br/>
		    <?php
		    }
		    ?>
			<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" class="con-borde" >
				<tr style="background-color:#10689b; color:#FFFFFF;">
					<th>CATEGORIA</th>
					<th>PRECIO EN USD</th>
					<th>VIGENCIA HASTA</th>
				</tr>
				<?php
					$inscripciones = $c_cotizacion_evento->obtener_inscripciones($idcotizacion);

					for ($i=0; $i < count($inscripciones); $i++) 
					{ 
						$opcion_ins = $inscripciones[$i];
						$factor = (1 + $opcion_ins['fee']/100) * (1 + $opcion_ins['factura']/100);
				?>
				<tr>
					<td><?php echo strtoupper($opcion_ins['categoria']); ?></td>
					<td><?php echo ceil($opcion_ins['precio'] * $factor); ?></td>
					<td><?php echo date('d/m/Y', strtotime($opcion_ins['time_limit'])); ?></td>
				</tr>
				<?php
					}
				?>
			</table>
		</div>

		<div class="clear_mayor"></div>

		<div class="fifteen columns" style="font-size:7pt;">
			<input type="radio" name="mostrar" value="1" onchange="validar_vista_precios();" />Boleto + Paquete
			<input type="radio" name="mostrar" value="2" onchange="validar_vista_precios();" checked />Boleto y Paquete
			<input type="radio" name="mostrar" value="3" onchange="validar_vista_precios();" />Por opciones de Itinerario Resumido
			<input type="radio" name="mostrar" value="4" onchange="validar_vista_precios();" />Por opciones de Itinerario Detallado
		</div>

	</div>
</div>

<div id="vis_ag" class="pnl_cot" style="display:none;">
	<div class="columns sixteen" style="background-color:#f1f1f1; padding-top:10px; padding-bottom:10px;">
	    <div class="columns fifteen" id="detalles_tarifa">
	    	
	    	<strong>HOTELES</strong>
	    	<div class="clear"></div>
    		<?php
				for ($i=0; $i < count($tarifas_hotel); $i++) 
				{ 
					$tarifa_hotel 		= $tarifas_hotel[$i];
					$items 				= $tarifa_hotel['opciones'];
					$cant_noch			= $tarifa_hotel['cant_noches'];
					if($cant_noch <= 0)
					{
						$cant_noch = floor( (strtotime($tarifa_hotel['out'] . ' 00:00:00') - strtotime($tarifa_hotel['in'] . ' 00:00:00')) / 86400);
					}
					$columnas_ocultar 	= $c_cotizacion_evento->evaluar_vista_hoteles($items);
			?>
				<div class="three columns" style="font-size:8pt;"><strong>DESTINO : </strong><?php echo $tarifa_hotel['destino']; ?></div>
				<div class="three columns" style="font-size:8pt;"><strong>IN : </strong><?php echo date('d/M/Y', strtotime($tarifa_hotel['in'] . ' 00:00:00')); ?></div>
				<div class="three columns" style="font-size:8pt;"><strong>OUT : </strong><?php echo date('d/M/Y', strtotime($tarifa_hotel['out'] . ' 00:00:00')); ?></div>
				<div class="three columns" style="font-size:8pt;"><strong>NOCHES : </strong><?php echo $cant_noch; ?></div>
				<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" id="detalle_tarifa_<?php echo $i; ?>" class="con-borde" >
					<tr style="background-color:#10689b; color:#FFFFFF;">
						<th>CODIGO</th>
						<th>HOTEL</th>
						<?php if(strpos($columnas_ocultar, 'SGL') !== false) { ?><th>SINGLE</th><?php } ?>
						<?php if(strpos($columnas_ocultar, 'DBL') !== false) { ?><th>DOBLE</th><?php } ?>
						<?php if(strpos($columnas_ocultar, 'TPL') !== false) { ?><th>TRIPLE</th><?php } ?>
						<?php if(strpos($columnas_ocultar, 'CPL') !== false) { ?><th>CDRPLE</th><?php } ?>
						<th>MENOR</th>
						<th>INF</th>
						<th>T LIMIT</th>
						<th>DETALLE PRECIOS X NOCHE</th>
						<th>PAXS</th>
					</tr>
					<?php
						for ($j=0; $j < count($items); $j++) 
						{ 
							$item_tarifa 	= $items[$j];
							$comision 		= (1+ $item_tarifa['comision']/100) * (1+ $item_tarifa['factura']/100);
							if($item_tarifa['link'] == '-' || $item_tarifa['link'] == '' )
								$link = '#';
							else
								$link .= 'http://' . $item_tarifa['link'];
							$estado = $item_tarifa['estado'];
					?>
					<tr class="item_tarifa_hotel_<?php echo $i; ?>" id="item_<?php echo $ind; ?>" style="<?php if($estado == '0') echo 'color:#FF3300;' ?>">
						<td>
							<?php echo $item_tarifa['identificador']; ?>
						</td>
						<td style="line-height:1;">
							<a <?php if($link!='#') { ?>href="<?php echo $link; ?>" target="_blank" <?php } ?> style="text-decoration:none;"><?php echo strtoupper(nl2br($item_tarifa['nombre_hotel'] . ' - ' . $item_tarifa['categoria'] . '*')); ?></a>
						</td>
						<?php if(strpos($columnas_ocultar, 'SGL') !== false) { ?>
						<td>
							<?php echo ($item_tarifa['precio_single'] * $comision * $cant_noch); ?>
						</td>
						<?php }
						 if(strpos($columnas_ocultar, 'DBL') !== false) {  ?>
						<td>
							<?php echo ($item_tarifa['precio_doble'] * $comision * $cant_noch); ?>
						</td>
						<?php }
						 if(strpos($columnas_ocultar, 'TPL') !== false) {  ?>
						<td>
							<?php echo ($item_tarifa['precio_triple'] * $comision * $cant_noch); ?>
						</td>
						<?php }
						 if(strpos($columnas_ocultar, 'CPL') !== false) {  ?>
						<td>
							<?php echo ($item_tarifa['precio_cuadruple'] * $comision * $cant_noch); ?>
						</td>
						<?php } ?>
						<td>
							<?php echo ($item_tarifa['precio_menor'] * $comision * $cant_noch); ?>
						</td>
						<td>
							<?php echo ($item_tarifa['precio_infante'] * $comision * $cant_noch); ?>
						</td>
						<td>
							<?php echo date('d/M/Y', strtotime($item_tarifa['time_limit'])); ?>
						</td>
						<td style="vertical-align:middle; line-height:1;">
							<?php echo 'SGL:' . $item_tarifa['precio_single'] . ' - DBL:' . $item_tarifa['precio_doble'] . 
										' - TPL:' . $item_tarifa['precio_triple'] . ' - CDP:' . $item_tarifa['precio_cuadruple'] . 
										' - CNN:' . $item_tarifa['precio_menor'] . ' - INF:' . $item_tarifa['precio_infante'] . 
										'<br/>FEE:' . $item_tarifa['comision'] . ' - IVA:'.$item_tarifa['factura']; ?>
						</td>
						<td style="vertical-align:middle; line-height:1; font-size:7pt;">
							<?php
								$pasajeros = $c_cotizacion_evento->obtener_paxs_del_hotel($item_tarifa['iditem_hotel_evento_cotizado']);
								for ($k=0; $k < count($pasajeros); $k++) 
								{
									$precio = "";
									switch ($pasajeros[$k]['tipo_precio']) {
									 	case 1 : $precio='SGL'; break;
									 	case 2 : $precio='DBL'; break;
									 	case 3 : $precio='TPL'; break;
									 	case 4 : $precio='CPL'; break;
									 	case 5 : $precio='CNN'; break;
									 	case 6 : $precio='INF'; break;
									 	default: $precio='NN'; break;
									} 
									echo strtoupper(substr($pasajeros[$k]['nombre_cliente'], 0, 7) . '/' . substr($pasajeros[$k]['apellido_cliente'], 0, 7) . '/' . $precio . '<br/>');
								}
							?>
						</td>
					</tr>
					<?php
						}
					?>
				</table><br/>
			<?php
				}
			?>
		    <strong>BOLETOS AEREOS</strong>
		    <table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" id="detalle_boletos_cotizados" class="con-borde" >
				<tr style="background-color:#10689b; color:#FFFFFF;">
					<th>CODIGO</th>
					<th>LINEA AEREA</th>
					<th>RUTA</th>
					<th>NETO ADT</th>
					<th>NETO CNN</th>
					<th>NETO INF</th>
					<th>FEE</th>
					<th>IVA</th>
					<th>T LIMIT</th>
					<th>PAXS</th>
				</tr>
				<?php
					for ($i=0; array_key_exists($i, $precios_boletos); $i++) 
					{ 
						$boleto_cotizado 	= $precios_boletos[$i];
						$idboleto 			= $boleto_cotizado['idboleto_cotizado_evento_cotizado'];
						$estado 			= $boleto_cotizado['estado'];
				?>
				<tr class="boleto_cotizado" id="boletocotizado_<?php echo $i; ?>" style="<?php if($estado == '0') echo 'color:#FF3300;'; ?>">
					<td>
						<?php echo $boleto_cotizado['codigo_reserva']; ?>
					</td>
					<td>
						<?php echo strtoupper($c_cotizacion_evento->obtener_linea_aerea($boleto_cotizado['linea_aerea'])); ?>
					</td>
					<td>
						<?php echo strtoupper($boleto_cotizado['ruta']); ?>
					</td>
					<td>
						<?php echo $boleto_cotizado['precio_adulto']; ?>
					</td>
					<td>
						<?php echo $boleto_cotizado['precio_menor']; ?>
					</td>
					<td>
						<?php echo $boleto_cotizado['precio_infante']; ?>
					</td>
					<td>
						<?php echo $boleto_cotizado['fee']; ?>
					</td>
					<td>
						<?php echo $boleto_cotizado['factura']; ?>
					</td>
					<td>
						<?php echo date('d/M/Y', strtotime($boleto_cotizado['time_limit'])); ?>
					</td>
					<td style="vertical-align:middle; line-height:1; font-size:7pt;">
						<?php
							$pasajeros = $c_cotizacion_evento->obtener_paxs_del_boleto($boleto_cotizado['idboleto_cotizado_evento_cotizado']);
							for ($k=0; $k < count($pasajeros); $k++) 
							{
								$precio = "";
								switch ($pasajeros[$k]['tipo_precio']) {
								 	case 1 : $precio='ADT'; break;
								 	case 2 : $precio='CNN'; break;
								 	case 3 : $precio='INF'; break;
								 	default: $precio='NN'; break;
								} 
								echo strtoupper(substr($pasajeros[$k]['nombre_cliente'], 0, 7) . '/' . substr($pasajeros[$k]['apellido_cliente'], 0, 7) . '/' . $precio . '<br/>');
							}
						?>
					</td>
				</tr>
				<?php
					}
				?>
			</table>
			<br/>
		    <strong>OTROS SERVICIOS</strong>
		    <table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" class="con-borde" >
				<tr style="background-color:#10689b; color:#FFFFFF;">
					<th>CODIGO</th>
					<th>CONCEPTO</th>
					<th>NETO ADT</th>
					<th>NETO CNN</th>
					<th>NETO INF</th>
					<th>FEE</th>
					<th>FACTURA</th>
					<th>T LIMIT</th>
					<th>PAXS</th>
				</tr>
			    <?php
			    	for ($i=0; array_key_exists($i, $precios_otros_servicios); $i++) 
			    	{ 
			    		$tarifa_servicio 	= $precios_otros_servicios[$i];
			    		$estado 			= $tarifa_servicio['estado'];
			    ?>
		    	<tr style="<?php if($estado == '0') echo 'color:#FF3300'; ?>">
		    		<td>
						<?php echo $tarifa_servicio['codigo']; ?>
					</td>
					<td>
						<?php echo strtoupper($tarifa_servicio['nombre_servicio'] . ' :' . $tarifa_servicio['descripcion']); ?>
					</td>
					<td>
						<?php echo $tarifa_servicio['precio_adulto']; ?>
					</td>
					<td>
						<?php echo $tarifa_servicio['precio_menor']; ?>
					</td>
					<td>
						<?php echo $tarifa_servicio['precio_infante']; ?>
					</td>
					<td>
						<?php echo $tarifa_servicio['fee']; ?>
					</td>
					<td>
						<?php echo $tarifa_servicio['factura']; ?>
					</td>
					<td>
						<?php echo date('d/M/Y', strtotime($tarifa_servicio['time_limit'])); ?>
					</td>
					<td style="vertical-align:middle; line-height:1; font-size:7pt;">
						<?php
							$pasajeros = $c_cotizacion_evento->obtener_paxs_del_servicio($tarifa_servicio['idtarifa_otros_evento_cotizado']);
							for ($k=0; $k < count($pasajeros); $k++) 
							{
								$precio = "";
								switch ($pasajeros[$k]['tipo_precio']) {
								 	case 1 : $precio='ADT'; break;
								 	case 2 : $precio='CNN'; break;
								 	case 3 : $precio='INF'; break;
								 	default: $precio='NN'; break;
								} 
								echo strtoupper(substr($pasajeros[$k]['nombre_cliente'], 0, 7) . ' / ' . substr($pasajeros[$k]['apellido_cliente'], 0, 7) . ' / ' . $precio . '<br/>');
							}
						?>
					</td>
				</tr>
			    <?php
			    	}
			    ?>
		    </table>
		    <br/>
		    <strong>INSCRIPCIONES</strong>
			<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" class="con-borde" >
				<tr style="background-color:#10689b; color:#FFFFFF;">
					<th>CATEGORIA</th>
					<th>PRECIO NETO</th>
					<th>FEE</th>
					<th>IVA</th>
					<th>VIGENCIA HASTA</th>
				</tr>
				<?php
					for ($i=0; $i < count($inscripciones); $i++) 
					{ 
						$opcion_ins = $inscripciones[$i];
						$fee 		= $opcion_ins['fee'];
						$factura 	= $opcion_ins['factura'];
						$factor 	= (1 + $fee/100) * (1 + $factura/100);
				?>
				<tr>
					<td><?php echo strtoupper($opcion_ins['categoria']); ?></td>
					<td><?php echo $opcion_ins['precio']; ?></td>
					<td><?php echo $fee; ?></td>
					<td><?php echo $factura; ?></td>
					<td><?php echo date('d/M/Y', strtotime($opcion_ins['time_limit'])); ?></td>
				</tr>
				<?php
					}
				?>
			</table>
			<br/>
	    </div>
	    <?php
			if($evento['datos_agente'] != "")
			{
		?>
			<div class="columns eight">
				<strong>DATOS ADICIONALES</strong>
				<ul class="lista_descripcion" id="lista_datos_agente">
					<?php
						$datos_agente = explode("\n", $evento['datos_agente']);
						for($i=0; $i<count($datos_agente); $i++)
						{
							if($datos_agente[$i] != "")
							{
					?>
								<li>
									<a id="datos_agente_dato_<?php echo $i; ?>" ><?php echo $datos_agente[$i]; ?></a>
								</li>
					<?php
							}
						}
					?>		
				</ul>
			</div>
		<?php
			}
		?>
	</div>
</div>


<div id="vis_pax" class="pnl_cot" style="display:none;">
	<div class="columns sixteen" style="background-color:#f1f1f1; padding-top:10px; padding-bottom:10px;">
    	<input type="hidden" id="hd_idevento_cotizado" value="<? echo $idcotizacion;?>"/>
        <input type="hidden" id="hd_tipo_de_cotizacion" value="2"/><!-- tipo de contizacion es: 1(paquetes), 2 (evento)-->
    	<input type="button" value="Agregar Pasajeros"  onclick="mostrar_buscar_pax();"/>
        
        <div id="tabs">
			<ul>
                <li><a href="#tabs-1">Costos</a></li>
                <li><a href="#tabs-2">Check List</a></li>
   		  	</ul>
            <div id="tabs-1">
				<?
                $rep_pax_evento = c_pax_evento::obtener_pax_evento_idcotizacion($idcotizacion);

                if($rep_pax_evento > -1)
                {
                ?>
                <table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;display:;" class="con-borde user_info_editable">
				<tr style="background-color:#10689b; color:#FFFFFF;border-color:#FFF">
                	<th style="width:16px;">&nbsp;</th>
					<th style="width:30px;">Pasajeros</th>
					<th style="width:50px;">Hotel</th>
					<th style="width:50px;">Boleto</th>
					<th style="width:50px;">Otros</th>
                    <th style="width:50px;">Inscripcion</th>
                    <th style="width:50px;">Totales</th>
				</tr>
                <?
                for($i = 0; $i < count($rep_pax_evento); $i++)
                {
					$total_hotel 		= $c_cotizacion_evento->obtener_total_monto_pax_hotel($rep_pax_evento[$i]['idpax_evento']);//0;
					$total_boleto 		= $c_cotizacion_evento->obtener_total_monto_pax($rep_pax_evento[$i]['idpax_evento']);
					$total_otros 		= $c_cotizacion_evento->obtener_total_monto_pax_otros($rep_pax_evento[$i]['idpax_evento']);//0;
					$total_inscripcion 	= $c_cotizacion_evento->obtener_total_monto_pax_inscripcion($rep_pax_evento[$i]['idpax_evento']);//0;
					$total_pax 			= $total_hotel + $total_boleto + $total_otros + $total_inscripcion;
					$total_cotizacion 	+= $total_pax;
                ?>
                    <tr id="idclientes_<? echo $rep_pax_evento[$i]['idclientes']?>" class="tr_move">
                    	<td style="text-align:center; padding-left:0px; padding-right:0px;">
                            <span class="editar_idclientes_<? echo $rep_pax_evento[$i]['idclientes']?>_link editar_campo_link" >
                               <img src="images/cross.png" title="Eliminar Pasajero" style="cursor:pointer;" onclick="eliminar_pax_cotizacion(<? echo $rep_pax_evento[$i]['idpax_evento'];?>);" />     
                            </span>
                        </td>
                        <td id="nombre_pax_<? echo $rep_pax_evento[$i]['idpax_evento']?>"><? echo $rep_pax_evento[$i]['trato_cliente']." ".$rep_pax_evento[$i]['nombre_cliente']." ".$rep_pax_evento[$i]['apellido_cliente']?></td>
                        <td style="text-align:right;">
                        	<span style=""><? echo number_format(round($total_hotel,2),2);?></span>
                            <span style="float:left;" class="editar_idclientes_<? echo $rep_pax_evento[$i]['idclientes']?>_link editar_campo_link" >
                            	<img src="images/pencil.png" title="Editar Hotel" style="cursor:pointer;" onclick="show_editar_hotel(<? echo $rep_pax_evento[$i]['idpax_evento']?>,2,<? echo $idcotizacion;?>);" />
                            </span>
						</td>
                        <td style="text-align:right;">
                        	<span id="total_<? echo $rep_pax_evento[$i]['idpax_evento'];?>"><? echo number_format(round($total_boleto,2),2);?></span>
                            <span style="float:left;" class="editar_idclientes_<? echo $rep_pax_evento[$i]['idclientes']?>_link editar_campo_link" >
                            	<img src="images/pencil.png" title="Editar Boleto" style="cursor:pointer;" onclick="show_editar_boleto(<? echo $rep_pax_evento[$i]['idpax_evento']?>,2,<? echo $idcotizacion;?>);" />
                            </span>
						</td>
                        <td style="text-align:right;">
                        	<span style=""><? echo number_format(round($total_otros,2),2);?></span>
                            <span style="float:left;" class="editar_idclientes_<? echo $rep_pax_evento[$i]['idclientes']?>_link editar_campo_link" >
                            	<img src="images/pencil.png" title="Editar Otros Servicios" style="cursor:pointer;" onclick="show_editar_otros(<? echo $rep_pax_evento[$i]['idpax_evento']?>,2,<? echo $idcotizacion;?>);" />
                            </span>
						</td>
                        <td style="text-align:right;">
                        	<span style=""><? echo number_format(round($total_inscripcion,2),2);?></span>
                            <span style="float:left;" class="editar_idclientes_<? echo $rep_pax_evento[$i]['idclientes']?>_link editar_campo_link" >
                            	<img src="images/pencil.png" title="Editar Inscripcion" style="cursor:pointer;" onclick="show_editar_inscripcion(<? echo $rep_pax_evento[$i]['idpax_evento']?>,2,<? echo $idcotizacion;?>)" />
                            </span>
						</td>
                        <td style="text-align:right;"><span style=""><? echo $total_pax;?></span></td>
                    </tr>
                <?	
                }
                ?>
                <tr>
                	<td colspan="6" style="text-align:right;"><strong>Totales&nbsp;</strong></td>
					<td style="text-align:right;"><strong><span style=""><? echo $total_cotizacion;?></span></strong></th>
				</tr>
                </table>
                <?
                }
                ?>
            </div>
            <div id="tabs-2">
    			<p>Morbi tincidunt, dui sit amet facilisis feugiat, odio metus gravida ante, ut pharetra massa metus id nunc. Duis scelerisque molestie turpis. Sed fringilla, massa eget luctus malesuada, metus eros molestie lectus, ut tempus eros massa ut dolor. Aenean aliquet fringilla sem. Suspendisse sed ligula in ligula suscipit aliquam. Praesent in eros vestibulum mi adipiscing adipiscing. Morbi facilisis. Curabitur ornare consequat nunc. Aenean vel metus. Ut posuere viverra nulla. Aliquam erat volutpat. Pellentesque convallis. Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.</p>
  			</div>
            <div id="div_loading" style="text-align:center;display:none;"><img src="../images/24.gif"/><br/><span>Cargando...</span></div>
		</div>
    </div> 
</div>

<div class="clear_mayor"></div>

<div class="columns fifteen" onmouseover="revelar_icono_edicion('observaciones');" onmouseout="ocultar_icono_edicion('observaciones');">
	<font style="font-weight:bold; color:#10689b; float:left;">OBSERVACIONES:</font>
	<img id="icon_editar_observaciones" src="images/pencil.png" onclick="mostrar_edicion('observaciones');" class="icon_edit" style="margin-left:20px; float:left;">
	<br/>
	<span id="observaciones">
		<span id="datos_observaciones">
			<ul class="lista_descripcion" id="lista_observaciones">
				<?php
					$paquete_incluye = explode("\n", $cotizacion['observaciones']);
					for($i=0; $i<count($paquete_incluye); $i++)
					{
						if($paquete_incluye[$i] != "")
						{
				?>
							<li>
								<a><?php echo $paquete_incluye[$i]; ?></a>
							</li>
				<?php
						}
					}
				?>		
			</ul>
		</span>
	</span>
	<span id="editar_observaciones" style="display:none;">
		<textarea name="observaciones" id="valor_observaciones" style="width:500px;"><?php echo $cotizacion['observaciones']; ?></textarea>
		<img src="images/cross.png" onclick="ocultar_edicion('observaciones; ?>');" class="icon_close">
		<img src="images/disk.png" onclick="guardar_atributo('evento_cotizado', 'observaciones', 'idevento_cotizado', '<?php echo $idcotizacion; ?>');" class="icon_save">
	</span>
</div>

<div class="sixteen columns">
	<input type="button" value="Imprimir" style="float:right;"  onclick="Imprimir();" />
	<a href="editar_cotizacion_evento.php?idevento=<?php echo $idcotizacion; ?>"><input type="button" value="Editar" style="float:right;" /></a>
	<input type="button" value="Enviar por Mail" style="float:right;" />
	<a href="ver_detalle_precios_cotizacion_por_pax.php?cotizacion=<?php echo $idcotizacion; ?>&type=e" target="_blank"><input type="button" value="Ver Resumen Precios" style="float:right;" /></a>
</div>

<?php include('iu_vista_cotizacion_evento_impresion.php'); ?>
<?php include('footer.php'); ?>

