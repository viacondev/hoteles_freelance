
<html> 
<head> 

  <title>Calendario de pruebas</title> 

  <!-Hoja de estilos del calendario --> 
  <link rel="stylesheet" type="text/css" media="all" href="calendar-green.css" title="win2k-cold-1" /> 

  <!-- librería principal del calendario --> 
 <script type="text/javascript" src="calendar.js"></script> 

 <!-- librería para cargar el lenguaje deseado --> 
  <script type="text/javascript" src="lang/calendar-es.js"></script> 

  <!-- librería que declara la función Calendar.setup, que ayuda a generar un calendario en unas pocas líneas de código --> 
  <script type="text/javascript" src="calendar-setup.js"></script> 

</head> 

<body>

 

<!-- formulario con el campo de texto y el botón para lanzar el calendario--> 
<form action="#" method="get"> 
<input type="text" name="date" id="fecha1" /> 
<input type="image" id="lanzador1" value="..." /> 
<input type="text" name="date" id="fecha2" /> 
<input type="image" id="lanzador2" value="..." /> 
</form> 

<!-- script que define y configura el calendario--> 
<script type="text/javascript"> 
   Calendar.setup({ 
    inputField     :    "fecha1",     // id del campo de texto 
     ifFormat     :     "%d/%m/%Y",     // formato de la fecha que se escriba en el campo de texto 
     button     :    "lanzador1"     // el id del botón que lanzará el calendario 
}); 
</script> 

<script type="text/javascript"> 
   Calendar.setup({ 
    inputField     :    "fecha2",     // id del campo de texto 
     ifFormat     :     "%d/%m/%Y",     // formato de la fecha que se escriba en el campo de texto 
     button     :    "lanzador2"     // el id del botón que lanzará el calendario 
}); 
</script> 

</body> 
</html> 