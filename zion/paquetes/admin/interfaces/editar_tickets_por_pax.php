<link href="lib/tinybox2/style.css" rel="stylesheet">
<?php
	include('../BD/controladoraBD.php');
	include('../entidad/pax.php');
	include('../entidad/ticket.php');
	include('../entidad/boleto_cotizado.php');

	$idboleto_cotizado = $_GET['idbol'];

	$paxs 				= pax::obtener_paxs_segun_boleto_sin_ticket($idboleto_cotizado);
	$boleto_cotizado 	= boleto_cotizado::obtener_boleto_cotizado_x_id($idboleto_cotizado);
	$boleto_cotizado 	= $boleto_cotizado[0];
	$codigo_pnr 		= $boleto_cotizado['codigo_reserva'];
	$tickets 			= ticket::obtener_tickets_por_codigo_reserva($codigo_pnr);

?>
<div class="container" style="height:550px; overflow:auto;">
	<div class="sixteen columns" style="color:#FFFFFF; position:absolute; padding:10px; opacity:0.9; display:none;" id="mensaje_pax">
	</div>

	<div class="plantilla_txt_pax hidden" >
		<div class="clear"></div>
		<span class="tkt_asoc__ntkt_" >
			<strong>*</strong> _tkt_ <strong>-></strong> _pax_
			<img src="images/stop32.png" class="mini" onclick="QuitarAsociacionTktPax('_ntkt_');" style="z-index:#99999;" />
			<input type="hidden" value="_npax_" id="_ntkt_" class="_class_" />
		</span>
	</div>

	<div class="eight columns" style="height:300px; border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1; padding:10px;">
		<div class="columns five" style="font-weight:bold; color:#10689b; font-size:12pt;">PASAJEROS</div>
		<div class="clear"></div>
		<?php
			for ($i=0; $i < count($paxs); $i++) 
			{ 
				$pax 	= $paxs[$i];
			?>
				<span class="pax_<?php echo $pax['idpax']; ?>">
					<input type="checkbox" value="<?php echo $pax['idpax']; ?>" name="pax" />
					<?php echo $pax['nombre_cliente'] . ' ' . $pax['apellido_cliente']; ?>
					<input type="hidden" value="<?php echo $pax['nombre_cliente'] . ' ' . $pax['apellido_cliente']; ?>" class="info_pax_<?php echo $pax['idpax']; ?>" />
				</span>
				<br/>
			<?php
			}
		?>
		<?php //echo "<pre>"; print_r($paxs); echo "</pre>"; ?>
	</div>

	<div class="seven columns" style="height:300px; border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1; padding:10px;">
		<div class="columns five" style="font-weight:bold; color:#10689b; font-size:12pt;">TICKETS EN RESERVA</div>
		<div class="clear"></div>
		<?php
			for ($i=0; $i < count($tickets); $i++) 
			{ 
				$ticket = $tickets[$i];
			?>
				<span class="ticket_<?php echo $ticket['numero_ticket']; ?>">
					<input type="checkbox" value="<?php echo $ticket['numero_ticket']; ?>" name="ticket" id="ticket_<?php echo $ticket['numero_ticket']; ?>" onchange="AsociarTktPax('<?php echo $ticket['numero_ticket']; ?>');" />
					<?php echo $ticket['numero_ticket'] . ' - ' . $ticket['precio_total_ticket'] . ' ' . $ticket['moneda_ticket'] . ' - ' . $ticket['apellido_pasajero'] . '/' . $ticket['nombre_pasajero'] . ' (' . $ticket['codigo_iata_linea_aerea'] . ')'; ?> 
					<input type="hidden" value="<?php echo $ticket['numero_ticket'] . ' - ' . $ticket['precio_total_ticket'] . ' ' . $ticket['moneda_ticket'] . ' - ' . $ticket['apellido_pasajero'] . '/' . $ticket['nombre_pasajero'] . ' (' . $ticket['codigo_iata_linea_aerea'] . ')'; ?>" class="info_tkt_<?php echo $ticket['numero_ticket']; ?>" />
				</span>
				<br/>
			<?php
			}
		?>
		<?php //echo "<pre>"; print_r($ticket); echo "</pre>"; ?>
	</div>

	<div class="clear_mayor"></div>

	<div class="twelve columns panel_tickets_asociados" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1; padding:10px;">
		<div class="columns five" style="font-weight:bold; color:#10689b; font-size:10pt;">TICKETS ASOCIADOS</div>
		<div class="clear_mayor"></div>
	</div>
	<div class="three columns" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1; padding:10px; text-align:center;">
		<input type="button" value="Guardar" style="width:70%;" onclick="GuardarAsociacionPaxTkt(<?php echo $_GET['idbol']; ?>)" />
	</div>
</div>