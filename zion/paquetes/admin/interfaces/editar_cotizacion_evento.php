<?php
include('header.php');
include('../entidad/tipo_evento.php');
include('../entidad/publicacion.php');
include('../entidad/evento.php');
include('../entidad/imagen.php');
include('../entidad/archivo_adjunto.php');
include('../entidad/operadora.php');
include('../entidad/destino.php');
include('../entidad/usuarios_paquetes.php');
include('../control/c_crear_cotizacion_evento.php');
include('../entidad/evento_cotizado.php');
include('../entidad/clientes.php');
include('../entidad/tarifa_hotel.php');
include('../entidad/tarifa_otros.php');
include('../entidad/boleto_cotizado.php');
include('../entidad/segmento_publicado.php');
include('../entidad/lineas_aereas.php');
include('../entidad/aeropuerto.php');
include('../entidad/pqt_otro_servicio.php');
include('../entidad/inscripcion_cotizado.php');
include('../entidad/otros_cotizado.php');
include('../entidad/hotel_cotizado.php');


	$idcotizacion = $_GET['idevento'];
	if(isset($_GET['new']))
	{
		$idgenerico 	= $_GET['new'];
	}

	$c_cotizacion_evento = new c_crear_cotizacion_evento;
	$cotizacion = $c_cotizacion_evento->obtener_cotizacion_evento($idcotizacion);
	if($cotizacion === false)
	{
		echo "-- LA COTIZACION NO EXISTE --";
		exit(0);
	}
	
	$idpublicacion = $cotizacion['idevento'];

	$evento = evento::obtener_evento($idpublicacion);

	include("plantillas_tarifa.php");
?>
<input type="hidden" value="<?php echo $idcotizacion; ?>" id="id_publicacion" />
<input type="hidden" value="evt_cot" id="type_publicacion" />
<div class="columns sixteen" style="background-color:#f1f1f1; padding-top:10px; padding-bottom:10px;">
	<h4><?php echo $evento[0]["titulo"] . '(' . $evento[0]["nombre_generico"] . ')'; ?></h4>
	
	<div class="clear_mayor"></div>

	<div class="columns two" style="font-weight:bold; color:#10689b;">SALIDA</div>
	<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
	<div class="columns four" onmouseover="revelar_icono_edicion('salida');" onmouseout="ocultar_icono_edicion('salida');">
		<span id="salida">
			<span id="datos_salida">&nbsp;<?php if(isset($_GET['new'])) echo $evento[0]["salida"]; else echo $cotizacion["salida"]; ?></span>
			<img id="icon_editar_salida" src="images/pencil.png" onclick="mostrar_edicion('salida');" class="icon_edit">
		</span>
		<span id="editar_salida" style="display:none;">
			<input type="text" name="salida" id="valor_salida" style="width:150px;" value="<?php if(isset($_GET['new'])) echo $evento[0]["salida"]; else echo $cotizacion["salida"]; ?>" />
			<img src="images/cross.png" onclick="ocultar_edicion('salida');" class="icon_close">
			<img src="images/disk.png" onclick="guardar_atributo('evento_cotizado', 'salida', 'idevento_cotizado', '<?php echo $idcotizacion; ?>');" class="icon_save">
		</span>
	</div>
	<div class="columns two" style="font-weight:bold; color:#10689b;">RETORNO</div>
	<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
	<div class="columns four" onmouseover="revelar_icono_edicion('retorno');" onmouseout="ocultar_icono_edicion('retorno');">
		<span id="retorno">
			<span id="datos_retorno">&nbsp;<?php if(isset($_GET['new'])) echo $evento[0]["retorno"]; else echo $cotizacion["retorno"]; ?></span>
			<img id="icon_editar_retorno" src="images/pencil.png" onclick="mostrar_edicion('retorno');" class="icon_edit">
		</span>
		<span id="editar_retorno" style="display:none;">
			<input type="text" name="retorno" id="valor_retorno" style="width:150px;" value="<?php if(isset($_GET['new'])) echo $evento[0]["retorno"]; else echo $cotizacion["retorno"]; ?>" />
			<img src="images/cross.png" onclick="ocultar_edicion('retorno');" class="icon_close">
			<img src="images/disk.png" onclick="guardar_atributo('evento_cotizado', 'retorno', 'idevento_cotizado', '<?php echo $idcotizacion; ?>');" class="icon_save">
		</span>
	</div>
	
	<div class="clear_mayor"></div>

	<div class="columns two" style="font-weight:bold; color:#10689b;">
		DESTINO(S)
	</div>
	<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
	<div class="columns four"  onmouseover="revelar_icono_edicion_multiple('destino');" onmouseout="ocultar_icono_edicion_multiple('destino');">
		<div class="columns three" style="margin-left:0px; margin-right:10px;" id="contenedor_destino">
			<?php 
				$destinos = evento::obtener_destinos_por_evento($idpublicacion);
				if(count($destinos) == 0)
					echo "- No hay ninguno -";
				for($i = 0; $i < count($destinos); $i++)
				{
					$nombre_destino 	= $destinos[$i]["nombre_destino"];
					$iddestino 			= $destinos[$i]["iddestino"];
			?>
				<div id="destino_<?php echo $iddestino; ?>">
					<?php echo strtoupper($nombre_destino); ?>
				</div>
			<?php
				}
			?>
		</div>
	</div>
</div>

<div class="clear_mayor"></div>

<div class="columns sixteen" style="background-color:#f1f1f1; padding-top:10px; padding-bottom:10px;">
	<div class="columns three" style="font-weight:bold; color:#10689b;">
		COTIZACION NRO
	</div>
	<div class="columns one" style="font-weight:bold; color:#10689b;">
		:
	</div>
	<div class="columns ten">
		<?php echo $cotizacion["idevento_cotizado"]; ?>
	</div>
	<div class="columns three" style="font-weight:bold; color:#10689b;">
		CLIENTE
	</div>
	<div class="columns one" style="font-weight:bold; color:#10689b;">
		:
	</div>
	<div class="columns ten">
		<?php
			$cliente = $c_cotizacion_evento->obtener_datos_cliente($cotizacion['idclientes']);
			if($cliente !== false)
			{
				echo strtoupper($cliente['trato_cliente'] . ' ' . $cliente['nombre_cliente'] . ' ' . $cliente['apellido_cliente']);
			}
			else
			{
				echo 'EL CLIENTE NO HA SIDO ENCONTRADO';
			}
		?>
	</div>
	<?php
		if($cotizacion['idsolicitante'] != 0) 
		{
			$solicitante = $c_cotizacion_evento->obtener_datos_cliente($cotizacion['idsolicitante']);
	?>
		<div class="columns three" style="font-weight:bold; color:#10689b;">
			SOLICITADO POR
		</div>
		<div class="columns one" style="font-weight:bold; color:#10689b;">
			:
		</div>
		<div class="columns ten">
	<?
			if($solicitante !== false)
			{
				echo strtoupper($solicitante['trato_cliente'] . ' ' . $solicitante['nombre_cliente'] . ' ' . $solicitante['apellido_cliente']);
			}
			else
			{
				echo 'EL SOLICITANTE NO HA SIDO ENCONTRADO';
			}
	?>
		</div>
	<?php
		}
	?>
	<div class="columns three" style="font-weight:bold; color:#10689b;">
		FECHA DE COTIZACION
	</div>
	<div class="columns one" style="font-weight:bold; color:#10689b;">
		:
	</div>
	<div class="columns ten">
		<?php
			echo strtoupper(date('d/M/Y h:i:s', strtotime($cotizacion['fecha_cotizacion'])));
		?>
	</div>
	<div class="columns three" style="font-weight:bold; color:#10689b;">
		COTIZADO POR
	</div>
	<div class="columns one" style="font-weight:bold; color:#10689b;">
		:
	</div>
	<div class="columns ten">
		<?php
			$usuario = $c_cotizacion_evento->obtener_datos_usuario($cotizacion['idusuarios']);
			if($usuario !== false)
			{
				echo strtoupper($usuario['nombre_usuario']);
			}
			else
			{
				echo 'DESCONOCIDO';
			}
		?>
	</div>
</div>

<?php
	if($evento[0]['solo_imagen'] == 'N')
	{
?>
	<div class="clear_mayor"></div>

	<div class="columns sixteen" style="background-color:#f1f1f1; padding-top:10px; padding-bottom:10px;">
		
		<div class="columns four" style="font-weight:bold; color:#10689b;">
			PAQUETE INCLUYE:
		</div>
		<div class="columns fifteen" onmouseover="revelar_icono_edicion('paquete_incluye');" onmouseout="ocultar_icono_edicion('paquete_incluye');">
			<span id="paquete_incluye">
				<span id="datos_paquete_incluye">
					<ul class="lista_descripcion" id="lista_paquete_incluye">
						<?php
							$paquete_incluye = explode("\n", $cotizacion['paquete_incluye']);
							for($i=0; $i<count($paquete_incluye); $i++)
							{
								if($paquete_incluye[$i] != "")
								{
						?>
									<li>
										<a><?php echo $paquete_incluye[$i]; ?></a>
									</li>
						<?php
								}
							}
						?>		
					</ul>
				</span>
				<img id="icon_editar_paquete_incluye" src="images/pencil.png" onclick="mostrar_edicion('paquete_incluye');" class="icon_edit">
			</span>
			<span id="editar_paquete_incluye" style="display:none;">
				<textarea name="paquete_incluye" id="valor_paquete_incluye" style="width:500px;"><?php echo $cotizacion["paquete_incluye"]; ?></textarea>
				<img src="images/cross.png" onclick="ocultar_edicion('paquete_incluye');" class="icon_close">
				<img src="images/disk.png" onclick="guardar_atributo('evento_cotizado', 'paquete_incluye', 'idevento_cotizado', '<?php echo $idcotizacion; ?>');" class="icon_save">
			</span>
		</div>

		<div class="clear_mayor"></div>

		<div class="columns four" style="font-weight:bold; color:#10689b;">
			TARIFAS DE HOTELES:
		</div>
		<div class="columns fifteen" style="padding-top:10px; overflow: auto; background-color:#f1f1f1; border 1px solid #000;">
			<div class="columns fourteen" id="tarifas_hotel">
				<?php 
					if(isset($_GET['new']))
						$tarifas_por_destino =  $c_cotizacion_evento->obtener_tarifas_hotel_por_evento($idgenerico, 'evento');
					else	
						$tarifas_por_destino =  $c_cotizacion_evento->obtener_tarifas_hotel_por_evento($idcotizacion, 'evento_cotizado');

					if($tarifas_por_destino !== false)
					{
						for ($i=0; $i < count($tarifas_por_destino); $i++) 
						{ 
							$tarifa_hotel 			= $tarifas_por_destino[$i];
							if(isset($_GET['new']))
							{
								$idth 					= 0;
								$idtarifa_hotel_evento 	= $tarifa_hotel['idtarifa_hotel_evento'];
							}
							else
							{
								$idth 					= $tarifa_hotel['idtarifa_hotel_evento_cotizado'];
								$idtarifa_hotel_evento 	= $tarifa_hotel['idtarifa_hotel_evento_cotizado'];
							}
							
							if(isset($_GET['new']))
								$items_tarifa 			= $c_cotizacion_evento->obtener_items_de_tarifa($idtarifa_hotel_evento, 'evento');
							else
								$items_tarifa 			= $c_cotizacion_evento->obtener_items_de_tarifa($idtarifa_hotel_evento, 'evento_cotizado');

							$cant_noches 			= floor( (strtotime($tarifa_hotel['out'] . ' 00:00:00') - strtotime($tarifa_hotel['in'] . ' 00:00:00')) / 86400);

							if($items_tarifa === false)
							{
								$items_tarifa = array();
							}
				?>
					<div id="tabla_tarifas_<?php echo $i; ?>">
						<table width="100%;" id="tarifa_<?php echo $i; ?>" class="encabezado_tarifa_hotel">
							<tr>
								<td>
									<strong>DESTINO :</strong>
								</td>
								<td style="width:20%;">
									<input type="text" style="margin: 0; width: 100px;" id="tdi<?php echo $i; ?>" value="<?php echo strtoupper($tarifa_hotel['destino']); ?>" onchange="avisar_cambio('H', '<?php echo $i; ?>');" />
								</td>
								<td>
									<strong>IN :</strong>
								</td>
								<td style="width:20%;">
									<input type="text" style="margin: 0; width: 100px;" id="tin<?php echo $i; ?>" class="time_limit" value="<?php echo date('d/m/Y', strtotime($tarifa_hotel['in'] . '00:00:00')); ?>" onchange="avisar_cambio('H', '<?php echo $i; ?>'); obtener_totales_hotel_x_destino('<?php echo $i; ?>');" />
								</td>
								<td>
									<strong>OUT :</strong>
								</td>
								<td style="width:20%;">
									<input type="text" style="margin: 0; width: 100px;" id="tou<?php echo $i; ?>" class="time_limit" value="<?php echo date('d/m/Y', strtotime($tarifa_hotel['out'] . '00:00:00')); ?>" onchange="avisar_cambio('H', '<?php echo $i; ?>'); obtener_totales_hotel_x_destino('<?php echo $i; ?>');" />
								</td>
								<td>
									<strong>NOCHES :</strong>
								</td>
								<td id="cant_noches_<?php echo $i; ?>">
									<?php echo $cant_noches; ?>
								</td>
								<td>
									<img src="images/cross.png" style="width:15px; height:15px; cursor:pointer;" title="Eliminar" onclick="eliminar_tabla_tarifa('<?php echo $i; ?>');" />
									<input type="hidden" value="<?php echo $idth; ?>" id="idth_<?php echo $i; ?>" />
									<span style="display:none;" id="items_eliminados<?php echo $i; ?>"></span>
								</td>
							</tr>
						</table>
						<div class="table_hotel">
						<table style="width:100%; font-size:8pt;" id="detalle_tarifa_<?php echo $i; ?>" >
				<?php
							for ($j=0; $j < count($items_tarifa); $j++) 
							{ 
								$item_tarifa = $items_tarifa[$j];

								if(isset($_GET['new']))
									$iditem = 0;
								else	
									$iditem = $item_tarifa['iditem_hotel_evento_cotizado'];

								$ind = $i . '_' . $j;
								$f_com = (1 + ($item_tarifa['comision']/100)) * (1 + ($item_tarifa['factura']/100)) * $cant_noches;
				?>
							<tr class="item_tarifa_hotel_<?php echo $i; ?>" id="item_<?php echo $ind; ?>">
								<td class="thirteen columns" style="border-top:1px solid #ccc; padding-top:10px;" >
									<div class="two columns">
										<input type="text" placeholder="IDENTIF (opc.)" value="<?php echo $item_tarifa['identificador']; ?>" id="iide_<?php echo $ind; ?>" style="margin:0px;" onchange="avisar_cambio('H', '<?php echo $i; ?>');" />
										<label>HOTEL</label>
										<input type="text" placeholder="HOTEL" value="<?php echo strtoupper($item_tarifa['nombre_hotel']); ?>" onkeyup="buscar_hoteles('<?php echo $ind; ?>');" id="hotel_<?php echo $ind; ?>" style="margin:0px;" />
										<input type="hidden" id="iidhot_<?php echo $ind; ?>" value="<?php echo $item_tarifa['idhotel']; ?>" onchange="avisar_cambio('H', '<?php echo $i; ?>');" />
										<label>TIME LIMIT</label>
										<input type="text" placeholder="dd/mm/aaaa" class="time_limit" id="itmlm_<?php echo $ind; ?>" style="width:75%; margin:0px;" onchange="avisar_cambio('H', '<?php echo $i; ?>');" />
									</div>
									<div class="one columns">
										<div id="busqueda_hotel_<?php echo $ind; ?>" style="display:none; position:absolute; width:350px; height:200px; z-index:999999; background-color:#FFFFFF; border:1px solid #AAAAAA;">
											<img src="images/cancel.png" style="width:15px; height:15px; margin-left:330px; margin-top:2px; position:absolute;" onclick="$('#busqueda_hotel_<?php echo $ind; ?>').hide();" />
											<div id="panel_busqueda_hotel_<?php echo $ind; ?>" style="width:350px; height:200px; overflow:auto;">
											</div>
											&nbsp;
										</div>
										<label>PRECIOS</label>
										<input type="text" id="isgl<?php echo $ind; ?>" value="<?php echo $item_tarifa['precio_single']; ?>" placeholder="SGL" style="margin:0px;" onblur="obtener_total_hotel('<?php echo $ind; ?>', 'sgl');" onchange="avisar_cambio('H', '<?php echo $i; ?>');" />
										<input type="text" id="idbl<?php echo $ind; ?>" value="<?php echo $item_tarifa['precio_doble']; ?>" placeholder="DBL" style="margin:0px;" onblur="obtener_total_hotel('<?php echo $ind; ?>', 'dbl');" onchange="avisar_cambio('H', '<?php echo $i; ?>');" />
										<input type="text" id="itpl<?php echo $ind; ?>" value="<?php echo $item_tarifa['precio_triple']; ?>" placeholder="TPL" style="margin:0px;" onblur="obtener_total_hotel('<?php echo $ind; ?>', 'tpl');" onchange="avisar_cambio('H', '<?php echo $i; ?>');" />
										<input type="text" id="icdpl<?php echo $ind; ?>" value="<?php echo $item_tarifa['precio_cuadruple']; ?>" placeholder="CPL" style="margin:0px;" onblur="obtener_total_hotel('<?php echo $ind; ?>', 'cdpl');" onchange="avisar_cambio('H', '<?php echo $i; ?>');" />
									</div>
									<div class="one columns">
										<label>&nbsp;</label>
										<input type="text" id="imnr<?php echo $ind; ?>" value="<?php echo $item_tarifa['precio_menor']; ?>" placeholder="CNN" style="margin:0px;" onblur="obtener_total_hotel('<?php echo $ind; ?>', 'mnr');" onchange="avisar_cambio('H', '<?php echo $i; ?>');" />
										<input type="text" id="iinf<?php echo $ind; ?>" value="<?php echo $item_tarifa['precio_infante']; ?>" placeholder="INF" style="margin:0px;" onblur="obtener_total_hotel('<?php echo $ind; ?>', 'inf');" onchange="avisar_cambio('H', '<?php echo $i; ?>');" />	
										<input type="text" id="icms<?php echo $ind; ?>" value="<?php echo $item_tarifa['comision']; ?>" placeholder="%COM" style="margin:0px;" onblur="obtener_totales_hotel('<?php echo $ind; ?>');" onchange="avisar_cambio('H', '<?php echo $i; ?>');" class="pc_<?php echo $i; ?>" />
										<input type="text" id="iiva<?php echo $ind; ?>" value="<?php echo $item_tarifa['factura']; ?>" placeholder="%IVA" style="margin:0px;" onblur="obtener_totales_hotel('<?php echo $ind; ?>');" onchange="avisar_cambio('H', '<?php echo $i; ?>');" />
									</div>
									<div class="three columns">
										<label>ALIMENTACION</label>
										<textarea id="ialm<?php echo $ind; ?>" class="mytextarea" onchange="avisar_cambio('H', '<?php echo $i; ?>');" ><?php echo strtoupper($item_tarifa['alimentacion']); ?></textarea>
										<label>OBS</label>
										<textarea id="iiex<?php echo $ind; ?>" class="mytextarea" onchange="avisar_cambio('H', '<?php echo $i; ?>');" ><?php echo strtoupper($item_tarifa['info_extra']); ?></textarea>
									</div>
									<div class="three columns">
										<label>LINK</label>
										<input type="text" id="ilnk<?php echo $ind; ?>" value="<?php echo $item_tarifa['link']; ?>" onchange="avisar_cambio('H', '<?php echo $i; ?>');" />
										<label>OBS INT</label>
										<textarea id="iobs<?php echo $ind; ?>" class="mytextarea" onchange="avisar_cambio('H', '<?php echo $i; ?>');" ><?php echo strtoupper($item_tarifa['observacion']); ?></textarea>	
									</div>
									<div class="one columns">
										<label>SGL:</label>
										<label>DBL:</label>
										<label>TPL:</label>
										<label>CPL:</label>
										<label>CNN:</label>
										<label>INF:</label>
									</div>
									<div class="one columns">
										<label id="tot-sgl<?php echo $ind; ?>"><?php echo $item_tarifa['precio_single'] * $f_com; ?></label>
										<label id="tot-dbl<?php echo $ind; ?>"><?php echo $item_tarifa['precio_doble'] * $f_com; ?></label>
										<label id="tot-tpl<?php echo $ind; ?>"><?php echo $item_tarifa['precio_triple'] * $f_com; ?></label>
										<label id="tot-cdpl<?php echo $ind; ?>"><?php echo $item_tarifa['precio_cuadruple'] * $f_com; ?></label>
										<label id="tot-mnr<?php echo $ind; ?>"><?php echo $item_tarifa['precio_menor'] * $f_com; ?></label>
										<label id="tot-inf<?php echo $ind; ?>"><?php echo $item_tarifa['precio_infante'] * $f_com; ?></label>
									</div>
								</td>
								<td style="vertical-align:top;">
									<center>
										<br/><img src="images/cancel.png" style="width:10px; height:10px; cursor:pointer;" title="Eliminar" onclick="eliminar_item_hotel('<?php echo $ind; ?>');" />
										<br/>Act<br/>
										<input type="checkbox" id="est<?php echo $ind; ?>" <?php if($item_tarifa['estado'] == '1') echo 'checked'; ?> />
										<input type="hidden" id="iditem<?php echo $ind; ?>" value="<?php echo $iditem; ?>" />
										<?php
											$pasajeros = $c_cotizacion_evento->obtener_paxs_del_hotel($iditem);
											if(count($pasajeros) > 0)
											{
										?>
												<a class="mylink" onmouseover="mostrar_paxs('<?php echo $ind; ?>');" onmouseout="ocultar_paxs('<?php echo $ind; ?>');" style="line-height:1;" >Pax<?php echo count($pasajeros); ?></a>
												<br/>
												<div id="paxs<?php echo $ind; ?>" style="display:none; position:absolute; line-height:1; font-size:7pt; padding:10px; background-color:#FFFFFF; border:1px solod #AAAAAAl">
										<?php
												for ($k=0; $k < count($pasajeros); $k++) 
												{
													$precio = "";
													switch ($pasajeros[$k]['tipo_precio']) {
													 	case 1 : $precio='SGL'; break;
													 	case 2 : $precio='DBL'; break;
													 	case 3 : $precio='TPL'; break;
													 	case 4 : $precio='CPL'; break;
													 	case 5 : $precio='CNN'; break;
													 	case 6 : $precio='INF'; break;
													 	default: $precio='NN'; break;
													} 
													echo strtoupper(substr($pasajeros[$k]['nombre_cliente'], 0, 7) . '/' . substr($pasajeros[$k]['apellido_cliente'], 0, 7) . '/' . $precio . '<br/>');
												}
										?>
												</div>
										<?php	
											}
										?>
									</center>
								</td>
							</tr>
				<?php
							}
				?>
						</table>
						</div>
						<a class="mylink" onclick="agregar_item_hotel('<?php echo $i; ?>');">Nuevo Hotel</a>
						<a class="mylink" onclick="guardar_tarifa('<?php echo $i; ?>');">Guardar Tabla</a>
						<input type="hidden" value="<?php echo $j; ?>" id="c_hoteles_<?php echo $i; ?>" />
						<hr/>
					</div>
				<?
						}
					}				
				?>
			</div>
			<input type="hidden" value="<?php echo $i; ?>" id="c_destinos" />
			<div class="fourteen columns">
				<a class="mylinkright" onclick="agregar_tabla_tarifa();">Nueva Tabla</a>
				<a class="mylinkright" onclick="editar_hoteles_combinados();">Combinar Hoteles</a>
			</div>
		</div>

		<div class="clear_mayor"></div>

		<div class="columns four" style="font-weight:bold; color:#10689b;">
			TARIFAS OTROS SERVICIOS:
		</div>
		<div class="columns fifteen" style="padding-top:10px; overflow: auto; background-color:#f1f1f1; border 1px solid #000;">
			<div class="columns fourteen" id="otros_servicios">
				<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" id="detalle_otras_tarifas" class="con-borde" >
					<tr style="background-color:#10689b; color:#FFFFFF;">
						<th style="width:25%;">DETALLE</th>
						<th style="width:10%;">MONEDA</th>
						<th style="width:5%;">PRECIOS</th>
						<th style="width:5%;">+ %</th>
						<th style="width:13%;">T LIMIT</th>
						<th style="width:15%;">DATO AGENTE</th>
						<th style="width:8%;">TOTALES</th>
						<th style="width:16%;">PAXS</th>
						<th>ACT</th>
					</tr>
					<?php
						if(isset($_GET['new']))
							$otras_tarifas = $c_cotizacion_evento->obtener_otras_tarifas($idgenerico, 'evento');
						else	
							$otras_tarifas = $c_cotizacion_evento->obtener_otras_tarifas($idcotizacion, 'evento_cotizado');

						if($otras_tarifas === false)
						{
							$otras_tarifas = array();
						}
						for ($i=0; $i < count($otras_tarifas); $i++) 
						{ 
							$tarifa_otros = $otras_tarifas[$i];

							if(isset($_GET['new']))
								$idotra_tarifa = 0;
							else
								$idotra_tarifa = $tarifa_otros['idtarifa_otros_evento_cotizado'];
							$factor   		= (1+($tarifa_otros['fee']/100)) * (1+($tarifa_otros['factura']/100));
					?>
					<tr class="tarifa_otros_servicios" id="otroservicio_<?php echo $i; ?>">
						<td style="vertical-align:middle; padding-top:10px; padding-bottom:10px;">
							<?php
								$nombre_servicio = $c_cotizacion_evento->obtener_nombre_servicio($tarifa_otros['idotro_servicio']);
							?>
							CODIGO:<input type="text" placeholder="OPCIONAL" value="<?php echo $tarifa_otros['codigo']; ?>" id="icodotr<?php echo $i; ?>" style="width:70%; margin:0;" />
							CONCPT:<input type="text" placeholder="OBLIGATORIO" value="<?php echo $nombre_servicio; ?>" id="inser<?php echo $i; ?>" style="width:70%; margin:0;" onkeyup="buscar_otros_servicios('<?php echo $i; ?>');" />*
							DESCRIP:<input type="text" placeholder="OPCIONAL" value="<?php echo $tarifa_otros['descripcion']; ?>" id="idesotr<?php echo $i; ?>" style="width:70%; margin:0;" />
							<input type="hidden" value="<?php echo $tarifa_otros['idotro_servicio']; ?>" id="iidsr<?php echo $i; ?>" />
						</td>
						<td style="vertical-align:middle;">
							<div id="busqueda_otro_servicio_<?php echo $i; ?>" style="display:none; position:absolute; width:350px; height:200px; z-index:999999; background-color:#FFFFFF; border:1px solid #AAAAAA;">
								<img src="images/cancel.png" style="width:15px; height:15px; margin-left:330px; margin-top:2px; position:absolute;" onclick="$('#busqueda_otro_servicio_<?php echo $i; ?>').hide();" />
								<div id="panel_busqueda_otro_servicio_<?php echo $i; ?>" style="width:350px; height:200px; overflow:auto;">
								</div>
								&nbsp;
							</div>
							<select id="imnda<?php echo $i; ?>" style="margin: 0; width: 95%;">
								<option value="USD" <?php if($tarifa_otros['moneda'] == 'U') echo 'Selected'; ?> >USD</option>
								<option value="BOB" <?php if($tarifa_otros['moneda'] == 'B') echo 'Selected'; ?> >BOB</option>
							</select>
						</td>
						<td style="vertical-align:middle;">
							<input type="text" id="inadt<?php echo $i; ?>" placeholder="ADT" value="<?php echo $tarifa_otros['precio_adulto']; ?>" style="margin: 0; width: 95%;" onblur="obtener_total_servicio('<?php echo $i; ?>','adt');" />
							<input type="text" id="inmen<?php echo $i; ?>" placeholder="CNN" value="<?php echo $tarifa_otros['precio_menor']; ?>" style="margin: 0; width: 95%;" onblur="obtener_total_servicio('<?php echo $i; ?>','men');" />
							<input type="text" id="ininf<?php echo $i; ?>" placeholder="INF" value="<?php echo $tarifa_otros['precio_infante']; ?>" style="margin: 0; width: 95%;" onblur="obtener_total_servicio('<?php echo $i; ?>','inf');" />
						</td>
						<td style="vertical-align:middle;">
							<input type="text" id="iofee<?php echo $i; ?>" placeholder="%FEE" value="<?php echo $tarifa_otros['fee']; ?>" style="margin: 0; width: 95%;" onblur="obtener_totales_servicio('<?php echo $i; ?>');" />
							<input type="text" id="iofac<?php echo $i; ?>" placeholder="%IVA" value="<?php echo $tarifa_otros['factura']; ?>" style="margin: 0; width: 95%;" onblur="obtener_totales_servicio('<?php echo $i; ?>');" />
						</td>
						<td style="vertical-align:middle;">
							<input type="text" id="iotli<?php echo $i; ?>" value="<?php echo date('d/m/Y', strtotime($tarifa_otros['time_limit'])); ?>" class="time_limit" style="margin: 0; width: 70%;" />
						</td>
						<td style="vertical-align:middle;">
							<textarea id="ioob<?php echo $i; ?>" style="margin:0; width:95%; min-height:10px; height:30px;" ><?php echo strtoupper($tarifa_otros['observacion']); ?></textarea>
						</td>
						<td style="vertical-align:middle; line-height:1; font-size:8pt;">
							<span id="tot-oadt<?php echo $i; ?>"><?php echo ceil($tarifa_otros['precio_adulto'] * $factor); ?></span>(ADT)<br/>
							<span id="tot-omen<?php echo $i; ?>"><?php echo ceil($tarifa_otros['precio_menor'] * $factor); ?></span>(CNN)<br/>
							<span id="tot-oinf<?php echo $i; ?>"><?php echo ceil($tarifa_otros['precio_infante'] * $factor); ?></span>(INF)
						</td>
						<td style="vertical-align:middle; line-height:1; font-size:7pt;">
							<?php
								$pasajeros = $c_cotizacion_evento->obtener_paxs_del_servicio($idotra_tarifa);
								for ($k=0; $k < count($pasajeros); $k++) 
								{
									$precio = "";
									switch ($pasajeros[$k]['tipo_precio']) {
									 	case 1 : $precio='ADT'; break;
									 	case 2 : $precio='CNN'; break;
									 	case 3 : $precio='INF'; break;
									 	default: $precio='NN'; break;
									} 
									echo strtoupper(substr($pasajeros[$k]['nombre_cliente'], 0, 7) . '/' . substr($pasajeros[$k]['apellido_cliente'], 0, 7) . '/' . $precio . '<br/>');
								}
							?>
						</td>
						<td style="vertical-align:middle;">
							<input type="checkbox" id="iest<?php echo $i; ?>" <?php if($tarifa_otros['estado'] == '1') echo 'checked'; ?> /><br/>
							<img src="images/cancel.png" style="width:10px; height:10px; cursor:pointer;" title="Eliminar" onclick="eliminar_otro_servicio('<?php echo $i; ?>');" />
							<input type="hidden" id="idotra_tarifa<?php echo $i; ?>" value="<?php echo $idotra_tarifa; ?>" />
						</td>
					</tr>
					<?php
						}
					?>
				</table>
				<a class="mylinkright" onclick="guardar_otros_servicios();">Guardar Cambios</a>
				<a class="mylinkright" onclick="agregar_otro_servicio();">Agregar Otro Servicio</a>
				<input type="hidden" id="c_otras_tarifas" value="<?php echo $i; ?>" />
				<span id="eliminar_otras_tarifas" style="display:none;"></span>
			</div>
		</div>


		<div class="clear_mayor"></div>

		<div class="columns four" style="font-weight:bold; color:#10689b;">
			TARIFAS BOLETOS AEREOS:
		</div>
		<div class="columns fifteen" style="padding-top:10px; overflow: auto; background-color:#f1f1f1; border 1px solid #000;">
			<div class="columns fourteen" id="tarifas_aereo">
				<?php
					if(isset($_GET['new']))
						$opciones_aereo = $c_cotizacion_evento->obtener_opciones_boletos($idgenerico, 'evento');
					else
						$opciones_aereo = $c_cotizacion_evento->obtener_opciones_boletos($idcotizacion, 'evento_cotizado');
					
					for ($i=0; $i < count($opciones_aereo); $i++) 
					{ 
						$opcion_aereo 	= $opciones_aereo[$i];

						if(isset($_GET['new']))
						{
							$idopaereo 		= 0;
							$idopcion_bol 	= $opcion_aereo['idopcion_boleto_evento'];
						}
						else
							$idopaereo 		= $opcion_aereo['idopcion_boleto_evento_cotizado'];
				?>
						<div id="opcionaereo_<?php echo $i; ?>" style="margin-bottom:15px; border:1px solid #888;">
							<table width="100%;" class="encabezado_tarifa_hotel">
								<tr>
									<td>
										<strong>OPCION :</strong>
									</td>
									<td style="width:30%;">
										<input type="text" style="margin: 0; width: 150px;" id="opcbol<?php echo $i; ?>" value="<?php echo $opcion_aereo['descripcion']; ?>" />
									</td>
									<td>
										<strong>SALE :</strong>
									</td>
									<td style="width:30%;">
										<input type="text" style="margin: 0; width: 150px;" id="salbol<?php echo $i; ?>" value="<?php echo $opcion_aereo['salida']; ?>" />
									</td>
									<td>
										<input type="checkbox" id="iestaereo<?php echo $i; ?>" <?php if($opcion_aereo['estado'] == '1') echo 'checked'; ?> />
										Activo&nbsp;&nbsp;
										<img src="images/cross.png" style="width:15px; height:15px; cursor:pointer;" title="Eliminar" onclick="eliminar_opcion_aereo('<?php echo $i; ?>');" />
										<input type="hidden" value="<?php echo $idopaereo; ?>" id="idtb_<?php echo $i; ?>" />
									</td>
								</tr>
							</table>
							<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" id="detalle_boletos_cotizados<?php echo $i; ?>" class="con-borde" >
								<tr style="background-color:#10689b; color:#FFFFFF;">
									<!--<th style="width:5%;">ORD</th>
									<th style="width:5%;">CODIGO</th>
									<th style="width:17%; line-height:1;">L.A.<br/>RUTA</th>
									<th style="width:10%;">MNDA</th>
									<th style="width:5%;">$</th>
									<th style="width:5%;">%</th>
									<th style="width:13%;">T LIMIT</th>
									<th style="width:25%;">OBSERVACION</th>
									<th style="width:10%;">TOTALES</th>
									<th></th>
									<th style="width:5%;">ACT</th>-->
									<th style="width:5%;">ORD</th>
									<th style="width:5%;">CODIGO</th>
									<th style="width:12%; line-height:1;">L.A.<br/>RUTA</th>
									<th style="width:10%;">MNDA</th>
									<th style="width:5%;">$</th>
									<th style="width:5%;">%</th>
									<th style="width:13%;">T LIMIT</th>
									<th style="width:15%;">OBSERVACION</th>
									<th style="width:10%;">TOTALES</th>
									<th style="width:15%;">PAXS</th>
									<th style="width:5%;">ACT</th>
								</tr>
								<?php
									if(isset($_GET['new']))
										$boletos = $c_cotizacion_evento->obtener_boletos_de_opcion($idopcion_bol, 'evento');
									else
										$boletos = $c_cotizacion_evento->obtener_boletos_de_opcion($idopaereo, 'evento_cotizado');	

									for ($j=0; $j < count($boletos); $j++) 
									{ 
										$indbol 			= $i . '_' . $j;
										$boleto_cotizado 	= $boletos[$j];

										if(isset($_GET['new']))
											$idboleto = 0;
										else
											$idboleto = $boleto_cotizado['idboleto_cotizado_evento_cotizado'];

										$factor   			= (1+($boleto_cotizado['fee']/100))*(1+($boleto_cotizado['factura']/100));
								?>
										<tr class="boleto_cotizado<?php echo $i; ?>" id="boletocotizado-<?php echo $indbol; ?>">
											<td style="vertical-align:middle;">
												<input type="text" id="ibord<?php echo $indbol; ?>" value="<?php echo $boleto_cotizado['orden']; ?>" style="margin: 0; width:80%; top;" />	
											</td>
											<td style="vertical-align:middle;">
												<input type="text" id="icodres<?php echo $indbol; ?>" value="<?php echo $boleto_cotizado['codigo_reserva']; ?>" style="margin: 0; width:95%;" />	
											</td>
											<td style="vertical-align:middle;">
												<input type="text" placeholder="Linea Aerea" id="ilinaer<?php echo $indbol; ?>" value="<?php echo strtoupper($c_cotizacion_evento->obtener_linea_aerea($boleto_cotizado['linea_aerea'])); ?>" style="margin: 0; width:95%;" onkeyup="buscar_lineas_aereas('<?php echo $indbol; ?>');" />
												<input type="hidden" id="iidlinaer<?php echo $indbol; ?>" value="<?php echo $boleto_cotizado['linea_aerea']; ?>" />
												<input type="text" placeholder="Ruta" id="iruta<?php echo $indbol; ?>" value="<?php echo $boleto_cotizado['ruta']; ?>" style="margin: 0; width:95%;" />	
											</td>
											<td style="vertical-align:middle;">
												<div id="busqueda_linea_aerea_<?php echo $indbol; ?>" style="display:none; position:absolute; width:350px; height:200px; z-index:999999; background-color:#FFFFFF; border:1px solid #AAAAAA;">
													<img src="images/cancel.png" style="width:15px; height:15px; margin-left:330px; margin-top:2px; position:absolute;" onclick="$('#busqueda_linea_aerea_<?php echo $indbol; ?>').hide();" />
													<div id="panel_busqueda_linea_aerea_<?php echo $indbol; ?>" style="width:350px; height:200px; overflow:auto;">
													</div>
													&nbsp;
												</div>
												<select id="ibmnda<?php echo $indbol; ?>" style="margin: 0; width:95%;">
													<option value="USD" <?php if($boleto_cotizado['moneda'] == 'U') echo 'Selected'; ?> >USD</option>
													<option value="BOB" <?php if($boleto_cotizado['moneda'] == 'B') echo 'Selected'; ?> >BOB</option>
												</select>
											</td>
											<td style="vertical-align:middle; padding-top:10px; padding-bottom:10px;">
												<input type="text" id="ibadt<?php echo $indbol; ?>" placeholder="ADT" value="<?php echo $boleto_cotizado['precio_adulto']; ?>" style="margin: 0; width:95%;" onblur="obtener_total_boleto('<?php echo $indbol; ?>','adt');" />	
												<input type="text" id="ibmen<?php echo $indbol; ?>" placeholder="CNN" value="<?php echo $boleto_cotizado['precio_menor']; ?>" style="margin: 0; width:95%;" onblur="obtener_total_boleto('<?php echo $indbol; ?>','men');" />	
												<input type="text" id="ibinf<?php echo $indbol; ?>" placeholder="INF" value="<?php echo $boleto_cotizado['precio_infante']; ?>" style="margin: 0; width:95%;" onblur="obtener_total_boleto('<?php echo $indbol; ?>','inf');" />	
											</td>
											<td style="vertical-align:middle;">
												<input type="text" id="ibfee<?php echo $indbol; ?>" placeholder="%FEE" value="<?php echo $boleto_cotizado['fee']; ?>" style="margin: 0; width:95%;" onblur="obtener_totales_boleto('<?php echo $indbol; ?>');" />	
												<input type="text" id="ibfac<?php echo $indbol; ?>" placeholder="%IVA" value="<?php echo $boleto_cotizado['factura']; ?>" style="margin: 0; width:95%;" onblur="obtener_totales_boleto('<?php echo $indbol; ?>');" />	
											</td>
											<td style="vertical-align:middle;">
												<input type="text" id="iotlires<?php echo $indbol; ?>" value="<?php echo date('d/m/Y', strtotime($boleto_cotizado['time_limit'])); ?>" class="time_limit" style="margin: 0; width: 70%;" />
											</td>
											<td style="vertical-align:middle;">
												<textarea id="ibob<?php echo $indbol; ?>" style="margin:0; width:95%; min-height:10px; height:30px;" ><?php echo $boleto_cotizado['observacion']; ?></textarea>
											</td>
											<td style="vertical-align:middle; line-height:1; font-size:8pt;">
												<span id="tot-adt<?php echo $indbol; ?>"><?php echo ceil($boleto_cotizado['precio_adulto'] * $factor); ?></span>(ADT)<br/>
												<span id="tot-men<?php echo $indbol; ?>"><?php echo ceil($boleto_cotizado['precio_menor'] * $factor); ?></span>(CNN)<br/>
												<span id="tot-inf<?php echo $indbol; ?>"><?php echo ceil($boleto_cotizado['precio_infante'] * $factor); ?></span>(INF)
											</td>
											<td style="vertical-align:middle; line-height:1; font-size:7pt;">
												<?php
													$pasajeros = $c_cotizacion_evento->obtener_paxs_del_boleto($idboleto);
													for ($k=0; $k < count($pasajeros); $k++) 
													{
														$precio = "";
														switch ($pasajeros[$k]['tipo_precio']) {
														 	case 1 : $precio='ADT'; break;
														 	case 2 : $precio='CNN'; break;
														 	case 3 : $precio='INF'; break;
														 	default: $precio='NN'; break;
														} 
														echo strtoupper(substr($pasajeros[$k]['nombre_cliente'], 0, 7) . '/' . substr($pasajeros[$k]['apellido_cliente'], 0, 7) . '/' . $precio . '<br/>');
													}
												?>
											</td>
											<td style="vertical-align:middle;">
												<input type="checkbox" id="iestres<?php echo $indbol; ?>" <?php if($boleto_cotizado['estado'] == '1') echo 'checked'; ?> /><br/>
												<img src="images/cancel.png" style="width:10px; height:10px; cursor:pointer;" title="Eliminar" onclick="eliminar_boleto_cotizado('<?php echo $indbol; ?>');" />
												<input type="hidden" id="idboleto_cotizado<?php echo $indbol; ?>" value="<?php echo $idboleto; ?>" />
											</td>
										</tr>
								<?php
									}
								?>
							</table>
							<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" id="detalle_itinerario<?php echo $i; ?>" class="con-borde" >
								<tr style="background-color:#10689b; color:#FFFFFF;">
									<th style="width:3%;">ORD</th>
									<th style="width:5%;">CODIGO</th>
									<th style="width:19%;">LINEA AEREA</th>
									<th style="width:5%;">VUELO</th>
									<th style="width:10%;">FECHA</th>
									<th style="width:20%;">ORIGEN</th>
									<th style="width:8%;">SALE</th>
									<th style="width:20%;">DESTINO</th>
									<th style="width:8%;">LLEGA</th>
									<th style="width:2%;"></th>
								</tr>
								<?php
									if(isset($_GET['new']))
										$itinerario = $c_cotizacion_evento->obtener_segmentos_de_opcion($idopcion_bol, 'evento');
									else
										$itinerario = $c_cotizacion_evento->obtener_segmentos_de_opcion($idopaereo, 'evento_cotizado');

									for ($k=0; $k < count($itinerario); $k++) 
									{ 
										$indseg 	= $i . '_' . $k;
										$segmento 	= $itinerario[$k];

										if(isset($_GET['new']))
											$idsegmento = 0;
										else
											$idsegmento = $segmento['idsegmento_evento_cotizado'];

										$linea_aerea 		= $c_cotizacion_evento->obtener_linea_aerea($segmento['idlineas_aereas']);
										$aeropuerto_sale 	= $c_cotizacion_evento->obtener_aeropuerto($segmento['origen']);
										$aeropuerto_llega 	= $c_cotizacion_evento->obtener_aeropuerto($segmento['destino']);
								?>
										<tr class="segmento_publicado<?php echo $i; ?>" id="segmento-<?php echo $indseg; ?>" >
											<td>
												<input type="text" id="iiord<?php echo $indseg; ?>" value="<?php echo $segmento['orden']; ?>" style="margin: 0; width: 95%;" />	
											</td>
											<td>
												<input type="text" id="iicod<?php echo $indseg; ?>" value="<?php echo $segmento['codigo']; ?>" style="margin: 0; width: 95%;" />	
											</td>
											<td>
												<input type="text" id="iilin<?php echo $indseg; ?>" value="<?php echo $linea_aerea; ?>" style="margin: 0; width: 95%;" class="input_linea_aerea" />	
											</td>
											<td>
												<input type="text" id="iivue<?php echo $indseg; ?>" value="<?php echo $segmento['nro_vuelo']; ?>" style="margin: 0; width: 95%;" />	
											</td>
											<td>
												<input type="text" id="iifec<?php echo $indseg; ?>" value="<?php echo date('d/m/Y', strtotime($segmento['fecha'])); ?>" style="margin: 0; width:95%" placeholder="dd/mm/aaaa" />	
											</td>
											<td>
												<input type="text" id="iiasa<?php echo $indseg; ?>" value="<?php echo $aeropuerto_sale; ?>" style="margin: 0; width: 95%;" class="input_aeropuerto" />
											</td>
											<td>
												<input type="text" id="iihsa<?php echo $indseg; ?>" value="<?php echo $segmento['hora_sale']; ?>" style="margin: 0; width: 95%;" placeholder="hh:mm" />
											</td>
											<td>
												<input type="text" id="iiall<?php echo $indseg; ?>" value="<?php echo $aeropuerto_llega; ?>" style="margin: 0; width: 95%;" class="input_aeropuerto" />
											</td>
											<td>
												<input type="text" id="iihll<?php echo $indseg; ?>" value="<?php echo $segmento['hora_llega']; ?>" style="margin: 0; width: 95%;" placeholder="hh:mm" />
											</td>
											<td>
												<img src="images/cancel.png" style="width:10px; height:10px; cursor:pointer;" title="Eliminar" onclick="eliminar_segmento('<?php echo $indseg; ?>');" />
												<input type="hidden" id="idsegmento<?php echo $indseg; ?>" value="<?php echo $idsegmento; ?>" />
											</td>
										</tr>
								<?php
									}
								?>
							</table>

							<span style="display:none;" id="boletos_eliminados<?php echo $i; ?>"></span>
							<span style="display:none;" id="segmentos_eliminados<?php echo $i; ?>"></span>

							<input type="hidden" id="c_boletos_cotizados<?php echo $i; ?>" value="<?php echo $j; ?>" />
							<input type="hidden" id="c_segmentos<?php echo $i; ?>" value="<?php echo $k; ?>" />

							<a class="mylinkright" onclick="guardar_tarifa_aereo('<?php echo $i; ?>');">Guardar Cambios</a>
							<a class="mylinkright" onclick="agregar_segmento_de_reserva('<?php echo $i; ?>');">Obtener de Reserva</a>
							<a class="mylinkright" onclick="agregar_boleto_cotizado('<?php echo $i; ?>');">Agregar Tarifa Boleto</a>
							<a class="mylinkright" onclick="agregar_segmento('<?php echo $i; ?>');">Agregar Segmento</a>
						</div>
						<hr/>
				<?php
					}
				?>
			</div>
			<input type="hidden" id="c_tarifa_aereo" value="<?php echo $i; ?>" />
			<a class="mylinkright" onclick="agregar_tarifa_aereo();">Agregar Nueva Tarifa</a>
		</div>

		<!--<div class="columns four" style="font-weight:bold; color:#10689b;">
			TARIFAS BOLETOS AEREOS:
		</div>
		<div class="columns fifteen" style="padding-top:10px; overflow: auto; background-color:#f1f1f1; border 1px solid #000;">
			<div class="columns fourteen" id="boletos_cotizados">
				<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" id="detalle_boletos_cotizados" class="con-borde" >
					<tr style="background-color:#10689b; color:#FFFFFF;">
						<th style="width:5%;">ORD</th>
						<th style="width:5%;">CODIGO</th>
						<th style="width:12%; line-height:1;">L.A.<br/>RUTA</th>
						<th style="width:10%;">MNDA</th>
						<th style="width:5%;">$</th>
						<th style="width:5%;">%</th>
						<th style="width:13%;">T LIMIT</th>
						<th style="width:15%;">OBSERVACION</th>
						<th style="width:10%;">TOTALES</th>
						<th style="width:15%;">PAXS</th>
						<th style="width:5%;">ACT</th>
					</tr>
					<?php
						if(isset($_GET['new']))
						{
							$boletos_cotizados = $c_cotizacion_evento->obtener_boletos_cotizados($idgenerico, 'evento');
						}
						else
						{
							$boletos_cotizados = $c_cotizacion_evento->obtener_boletos_cotizados($idcotizacion, 'evento_cotizado');	
						}
						
						if($boletos_cotizados === false)
						{
							$boletos_cotizados = array();
						}

						for ($i=0; $i < count($boletos_cotizados); $i++) 
						{ 
							$boleto_cotizado = $boletos_cotizados[$i];
							if(isset($_GET['new']))
								$idboleto = 0;
							else
								$idboleto = $boleto_cotizado['idboleto_cotizado_evento_cotizado'];
							$factor   = (1+($boleto_cotizado['fee']/100))*(1+($boleto_cotizado['factura']/100));
					?>
					<tr class="boleto_cotizado" id="boletocotizado_<?php echo $i; ?>">
						<td style="vertical-align:middle;">
							<input type="text" id="ibord<?php echo $i; ?>" value="<?php echo $boleto_cotizado['orden']; ?>" style="margin: 0; width:80%; top;" />	
						</td>
						<td style="vertical-align:middle;">
							<input type="text" id="icodres<?php echo $i; ?>" value="<?php echo $boleto_cotizado['codigo_reserva']; ?>" style="margin: 0; width:95%;" />	
						</td>
						<td style="vertical-align:middle;">
							<input type="text" placeholder="Linea Aerea" id="ilinaer<?php echo $i; ?>" value="<?php echo strtoupper($c_cotizacion_evento->obtener_linea_aerea($boleto_cotizado['linea_aerea'])); ?>" style="margin: 0; width:95%;" onkeyup="buscar_lineas_aereas('<?php echo $i; ?>');" />
							<input type="hidden" id="iidlinaer<?php echo $i; ?>" value="<?php echo $boleto_cotizado['linea_aerea']; ?>" />
							<input type="text" placeholder="Ruta" id="iruta<?php echo $i; ?>" value="<?php echo $boleto_cotizado['ruta']; ?>" style="margin: 0; width:95%;" />	
						</td>
						<td style="vertical-align:middle;">
							<div id="busqueda_linea_aerea_<?php echo $i; ?>" style="display:none; position:absolute; width:350px; height:200px; z-index:999999; background-color:#FFFFFF; border:1px solid #AAAAAA;">
								<img src="images/cancel.png" style="width:15px; height:15px; margin-left:330px; margin-top:2px; position:absolute;" onclick="$('#busqueda_linea_aerea_<?php echo $i; ?>').hide();" />
								<div id="panel_busqueda_linea_aerea_<?php echo $i; ?>" style="width:350px; height:200px; overflow:auto;">
								</div>
								&nbsp;
							</div>
							<select id="ibmnda<?php echo $i; ?>" style="margin: 0; width:95%;">
								<option value="USD" <?php if($boleto_cotizado['moneda'] == 'U') echo 'Selected'; ?> >USD</option>
								<option value="BOB" <?php if($boleto_cotizado['moneda'] == 'B') echo 'Selected'; ?> >BOB</option>
							</select>
						</td>
						<td style="vertical-align:middle; padding-top:10px; padding-bottom:10px;">
							<input type="text" id="ibadt<?php echo $i; ?>" placeholder="ADT" value="<?php echo $boleto_cotizado['precio_adulto']; ?>" style="margin: 0; width:95%;" onblur="obtener_total_boleto('<?php echo $i; ?>','adt');" />	
							<input type="text" id="ibmen<?php echo $i; ?>" placeholder="CNN" value="<?php echo $boleto_cotizado['precio_menor']; ?>" style="margin: 0; width:95%;" onblur="obtener_total_boleto('<?php echo $i; ?>','men');" />	
							<input type="text" id="ibinf<?php echo $i; ?>" placeholder="INF" value="<?php echo $boleto_cotizado['precio_infante']; ?>" style="margin: 0; width:95%;" onblur="obtener_total_boleto('<?php echo $i; ?>','inf');" />	
						</td>
						<td style="vertical-align:middle;">
							<input type="text" id="ibfee<?php echo $i; ?>" placeholder="%FEE" value="<?php echo $boleto_cotizado['fee']; ?>" style="margin: 0; width:95%;" onblur="obtener_totales_boleto('<?php echo $i; ?>');" />	
							<input type="text" id="ibfac<?php echo $i; ?>" placeholder="%IVA" value="<?php echo $boleto_cotizado['factura']; ?>" style="margin: 0; width:95%;" onblur="obtener_totales_boleto('<?php echo $i; ?>');" />	
						</td>
						<td style="vertical-align:middle;">
							<input type="text" id="iotlires<?php echo $i; ?>" value="<?php echo date('d/m/Y', strtotime($boleto_cotizado['time_limit'])); ?>" class="time_limit" style="margin: 0; width: 70%;" />
						</td>
						<td style="vertical-align:middle;">
							<textarea id="ibob<?php echo $i; ?>" style="margin:0; width:95%; min-height:10px; height:30px;" ><?php echo $boleto_cotizado['observacion']; ?></textarea>
						</td>
						<td style="vertical-align:middle; line-height:1; font-size:8pt;">
							<span id="tot-adt<?php echo $i; ?>"><?php echo ceil($boleto_cotizado['precio_adulto'] * $factor); ?></span>(ADT)<br/>
							<span id="tot-men<?php echo $i; ?>"><?php echo ceil($boleto_cotizado['precio_menor'] * $factor); ?></span>(CNN)<br/>
							<span id="tot-inf<?php echo $i; ?>"><?php echo ceil($boleto_cotizado['precio_infante'] * $factor); ?></span>(INF)
						</td>
						<td style="vertical-align:middle; line-height:1; font-size:7pt;">
							<?php
								$pasajeros = $c_cotizacion_evento->obtener_paxs_del_boleto($idboleto);
								for ($k=0; $k < count($pasajeros); $k++) 
								{
									$precio = "";
									switch ($pasajeros[$k]['tipo_precio']) {
									 	case 1 : $precio='ADT'; break;
									 	case 2 : $precio='CNN'; break;
									 	case 3 : $precio='INF'; break;
									 	default: $precio='NN'; break;
									} 
									echo strtoupper(substr($pasajeros[$k]['nombre_cliente'], 0, 7) . '/' . substr($pasajeros[$k]['apellido_cliente'], 0, 7) . '/' . $precio . '<br/>');
								}
							?>
						</td>
						<td style="vertical-align:middle;">
							<input type="checkbox" id="iestres<?php echo $i; ?>" <?php if($boleto_cotizado['estado'] == '1') echo 'checked'; ?> /><br/>
							<img src="images/cancel.png" style="width:10px; height:10px; cursor:pointer;" title="Eliminar" onclick="eliminar_boleto_cotizado('<?php echo $i; ?>');" />
							<input type="hidden" id="idboleto_cotizado<?php echo $i; ?>" value="<?php echo $idboleto; ?>" />
						</td>
					</tr>
					<?php
						}
					?>
				</table>
				<a class="mylinkright" onclick="guardar_boletos_cotizados();">Guardar Cambios</a>
				<a class="mylinkright" onclick="agregar_boleto_cotizado();">Agregar Ruta</a>
				<input type="hidden" id="c_boletos_cotizados" value="<?php echo $i; ?>" />
				<span id="eliminar_boletos_cotizados" style="display:none;"></span>
			</div>
		</div>

		<div class="clear_mayor"></div>

		<div class="columns four" style="font-weight:bold; color:#10689b;">
			ITINERARIO:
		</div>
		<div class="columns fifteen" style="padding-top:10px; overflow: auto; background-color:#f1f1f1; border 1px solid #000;">
			<div class="columns fourteen">
				<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" id="detalle_itinerario" class="con-borde" >
					<tr style="background-color:#10689b; color:#FFFFFF;">
						<th style="width:3%;">ORD</th>
						<th style="width:5%;">CODIGO</th>
						<th style="width:19%;">LINEA AEREA</th>
						<th style="width:5%;">VUELO</th>
						<th style="width:10%;">FECHA</th>
						<th style="width:20%;">ORIGEN</th>
						<th style="width:8%;">SALE</th>
						<th style="width:20%;">DESTINO</th>
						<th style="width:8%;">LLEGA</th>
						<th style="width:2%;"></th>
					</tr>
					<?php
						if(isset($_GET['new']))
							$itinerario = $c_cotizacion_evento->obtener_segmentos_publicados($idgenerico, 'evento');
						else
							$itinerario = $c_cotizacion_evento->obtener_segmentos_publicados($idcotizacion, 'evento_cotizado');

						if($itinerario === false)
						{
							$itinerario = array();
						}
						
						for ($i=0; $i < count($itinerario); $i++) 
						{ 
							$segmento 	= $itinerario[$i];
							$idsegmento = $segmento['idsegmento_evento_cotizado'];

							$linea_aerea 		= $c_cotizacion_evento->obtener_linea_aerea($segmento['idlineas_aereas']);
							$aeropuerto_sale 	= $c_cotizacion_evento->obtener_aeropuerto($segmento['origen']);
							$aeropuerto_llega 	= $c_cotizacion_evento->obtener_aeropuerto($segmento['destino']);
					?>
					<tr class="segmento_publicado" id="segmento_<?php echo $i; ?>" >
						<td>
							<input type="text" id="iiord<?php echo $i; ?>" value="<?php echo $segmento['orden']; ?>" style="margin: 0; width: 95%;" />	
						</td>
						<td>
							<input type="text" id="iicod<?php echo $i; ?>" value="<?php echo $segmento['codigo']; ?>" style="margin: 0; width: 95%;" />	
						</td>
						<td>
							<input type="text" id="iilin<?php echo $i; ?>" value="<?php echo $linea_aerea; ?>" style="margin: 0; width: 95%;" class="input_linea_aerea" />	
						</td>
						<td>
							<input type="text" id="iivue<?php echo $i; ?>" value="<?php echo $segmento['nro_vuelo']; ?>" style="margin: 0; width: 95%;" />	
						</td>
						<td>
							<input type="text" id="iifec<?php echo $i; ?>" value="<?php echo date('d/m/Y', strtotime($segmento['fecha'])); ?>" style="margin: 0; width:95%" placeholder="dd/mm/aaaa" />	
						</td>
						<td>
							<input type="text" id="iiasa<?php echo $i; ?>" value="<?php echo $aeropuerto_sale; ?>" style="margin: 0; width: 95%;" class="input_aeropuerto" />
						</td>
						<td>
							<input type="text" id="iihsa<?php echo $i; ?>" value="<?php echo $segmento['hora_sale']; ?>" style="margin: 0; width: 95%;" placeholder="hh:mm" />
						</td>
						<td>
							<input type="text" id="iiall<?php echo $i; ?>" value="<?php echo $aeropuerto_llega; ?>" style="margin: 0; width: 95%;" class="input_aeropuerto" />
						</td>
						<td>
							<input type="text" id="iihll<?php echo $i; ?>" value="<?php echo $segmento['hora_llega']; ?>" style="margin: 0; width: 95%;" placeholder="hh:mm" />
						</td>
						<td>
							<img src="images/cancel.png" style="width:10px; height:10px; cursor:pointer;" title="Eliminar" onclick="eliminar_segmento('<?php echo $i; ?>');" />
							<input type="hidden" id="idsegmento<?php echo $i; ?>" value="<?php echo $idsegmento; ?>" />
						</td>
					</tr>
					<?php
						}
					?>
				</table>
				<a class="mylinkright" onclick="guardar_itinerario();">Guardar Cambios</a>
				<a class="mylinkright" onclick="agregar_segmento();">Agregar Segmento</a>
				<a class="mylinkright" onclick="agregar_segmento_de_reserva();">Obtener Segmentos de Reserva</a>
				<input type="hidden" id="c_segmentos" value="<?php echo $i; ?>" />
				<span id="eliminar_segmentos" style="display:none;"></span>
			</div>
		</div>

		<div class="clear_mayor"></div>-->

        <!-- INICIO INSCRIPCION -->
        <div class="columns four" style="font-weight:bold; color:#10689b;">
			INSCRIPCION AL EVENTO:
		</div>
        <div class="columns fifteen" style="padding-top:10px; overflow: auto; background-color:#f1f1f1; border 1px solid #000;">
			<div class="columns fourteen" id="inscripcion">
				<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" id="detalle_inscripcion" class="con-borde" >
					<tr style="background-color:#10689b; color:#FFFFFF;">
						<th style="width:20%;">CATEGORIA</th>
						<th style="width:10%;">MONEDA</th>
						<th style="width:11%;">PRECIO</th>
						<th style="width:6%;">%FEE</th>
						<th style="width:6%;">%IVA</th>
						<th style="width:15%;">TIME LIMIT</th>
                        <th style="width:27%;">OBSERVACION</th>						
						<th style="width:5%;">TOTALES</th>
						<th>ACT</th>
					</tr>
					<?php
						$inscripcion = $c_cotizacion_evento->obtener_inscripcion_cotizado($idcotizacion);

						if($inscripcion === false)
						{
							$inscripcion = array();
						}
						
						for ($i=0; $i < count($inscripcion); $i++) 
						{ 
							$factor = (1+($inscripcion[$i]['fee']/100)) * (1+($inscripcion[$i]['factura']/100));
					?>
					<tr class="c_tr_inscripcion" id="trinscripcion_<?php echo $i; ?>">
						<td style="vertical-align:middle;">
                        	<input id="txt_categoria_<?php echo $i; ?>" type="text" value="<? echo $inscripcion[$i]['categoria'];?>" style="margin: 0; width:95%;"/>
						</td>
						<td style="vertical-align:middle;">
							<select id="txt_moneda_<?php echo $i; ?>" style="margin: 0; width: 95%;">
								<option value="U" <?php if($inscripcion[$i]['moneda'] == 'U') echo 'Selected'; ?> >USD</option>
								<option value="B" <?php if($inscripcion[$i]['moneda'] == 'B') echo 'Selected'; ?> >BOB</option>
							</select>
						</td>
						<td style="vertical-align:middle;">
							<input type="text" id="txt_precio_<?php echo $i; ?>" value="<?php echo $inscripcion[$i]['precio']; ?>" style="margin: 0; width: 95%;" onblur="obtener_total_inscripcion('<?php echo $i; ?>');" />
						</td>
						<td style="vertical-align:middle;">
							<input type="text" id="txt_fee_<?php echo $i; ?>" value="<?php echo $inscripcion[$i]['fee']; ?>" style="margin: 0; width: 95%;" onblur="obtener_total_inscripcion('<?php echo $i; ?>');" />
						</td>
						<td style="vertical-align:middle;">
							<input type="text" id="txt_factura_<?php echo $i; ?>" value="<?php echo $inscripcion[$i]['factura']; ?>" style="margin: 0; width: 95%;" onblur="obtener_total_inscripcion('<?php echo $i; ?>');" />
						</td>
						<td style="vertical-align:middle;">
							<input type="text" id="txt_time_limit_<?php echo $i; ?>" value="<?php echo date('d/m/Y' ,strtotime($inscripcion[$i]['time_limit'])); ?>" class="time_limit" style="margin: 0; width: 75%;" onblur="obtener_total_inscripcion('<?php echo $i; ?>');" />
						</td>
                        <td style="vertical-align:middle;">
							<textarea id="txt_observacion_<?php echo $i; ?>" style="margin:0; width:95%; min-height:10px; height:30px;" ><?php echo strtoupper($inscripcion[$i]['observacion']); ?></textarea>
						</td>
						<td style="vertical-align:middle; line-height:1; font-size:8pt;">
							<span id="tot-insadt<?php echo $i; ?>"><?php echo ceil($inscripcion[$i]['precio'] * $factor); ?></span>
						</td>
						<td style="vertical-align:middle;">
							<input type="checkbox" id="chk_estado_<?php echo $i; ?>" <?php if($inscripcion[$i]['estado'] == '1') echo 'checked'; ?> /><br/>
							<img src="images/cancel.png" style="width:10px; height:10px; cursor:pointer;" title="Eliminar" onclick="eliminar_inscripcion('<?php echo $i; ?>');" />
							<input type="hidden" id="hd_idinscripcion<?php echo $i; ?>" value="<?php echo $inscripcion[$i]['idinscripcion_cotizado']; ?>" />
						</td>
					</tr>
					<?php
						}
					?>
				</table>
				<a class="mylinkright" onclick="guardar_inscripcion();">Guardar Cambios</a>
				<a class="mylinkright" onclick="agregar_inscripcion();">Agregar Categoria</a>
				<input type="hidden" id="hd_nro_inscripcion" value="<?php echo $i; ?>" />
				<span id="eliminar_inscripcion" style="display:none;"></span>
			</div>
		</div>
        <!-- FIN INSCRIPCION -->
        <div class="clear_mayor"></div>

		<div class="columns four" style="font-weight:bold; color:#10689b;">
			IMPORTANTE:
		</div>
		<div class="columns fifteen" onmouseover="revelar_icono_edicion('importante');" onmouseout="ocultar_icono_edicion('importante');">
			<span id="importante">
				<span id="datos_importante">
					<ul class="lista_descripcion" id="lista_importante">
						<?php
							$importante = explode("\n", $cotizacion['importante']);
							for($i=0; $i<count($importante); $i++)
							{
								if($importante[$i] != "")
								{
						?>
									<li>
										<a><?php echo $importante[$i]; ?></a>
									</li>
						<?php
								}
							}
						?>		
					</ul>
				</span>
				<img id="icon_editar_importante" src="images/pencil.png" onclick="mostrar_edicion('importante');" class="icon_edit">
			</span>
			<span id="editar_importante" style="display:none;">
				<textarea name="importante" id="valor_importante" style="width:500px;"><?php echo $evento[0]["importante"]; ?></textarea>
				<img src="images/cross.png" onclick="ocultar_edicion('importante');" class="icon_close">
				<img src="images/disk.png" onclick="guardar_atributo('evento_cotizado', 'importante', 'idevento_cotizado', '<?php echo $idcotizacion; ?>');" class="icon_save">
			</span>
		</div>

		<div class="clear_mayor"></div>

	</div>

<?php
	}
	else
	{
?>
	<div class="columns sixteen" style="background-color:#f1f1f1; padding-top:10px; padding-bottom:10px;">
		El contenido del paquete ser&aacute;n las im&aacute;genes de contenido
	</div>
<?php
	} 
?>

<div class="sixteen columns">
	<a href="ver_cotizacion_evento.php?cotizacion=<?php echo $idcotizacion; ?>"><input type="button" value="VER COTIZACION" style="float:right;" /></a>
</div>

<script type="text/javascript">
	$(function() {

	$.datepicker.regional['es'] =
  	{
	  	closeText: 'Cerrar',
	  	prevText: 'Previo',
	  	nextText: 'Próximo',
	  	monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
	  	monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
	  	monthStatus: 'Ver otro mes', yearStatus: 'Ver otro año',
	  	dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
	  	dayNamesShort: ['Dom','Lun','Mar','Mie','Jue','Vie','Sáb'],
	  	dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sa'],
	  	dateFormat: 'dd/mm/yy', firstDay: 0,
	  	initStatus: 'Selecciona la fecha', isRTL: false
	 };
	 	$.datepicker.setDefaults($.datepicker.regional['es']);

  	crear_calendario_cumpleanios('valor_salida');
  	crear_calendario_cumpleanios('valor_retorno');
  	crear_calendario_time_limit();
	});

	function crear_calendario_cumpleanios(id) {
	    anioActual = new Date().getFullYear();
	    anioInicio = anioActual - 90;
	    $("#" + id).datepicker({
	        showOn: "button",
	        buttonImage: "../images/calendar.jpg",
	        buttonImageOnly: true,
	        numberOfMonths: 3,
	        yearRange: anioInicio + ":2025",
	        constrainInput: false
	    });  
	    $("#" + id).datepicker("option", "dateFormat", 'dd/M/yy');
	}

	
</script>

<?php include('footer.php'); ?>