<?php
	include('../BD/controladoraBD.php');
	include('../entidad/ciudad_origen_paquete.php');

	$idcotizacion = $_GET['cot'];
?>
<link href="lib/tinybox2/style.css" rel="stylesheet" />
<div class="container" >
	<div class="sixteen columns" style="color:#FFFFFF; position:absolute; padding:10px; opacity:0.9;" id="mensaje_pax">
	</div>
	<strong>B&Uacute;SQUEDA DE PASAJEROS</strong><hr/>
	<div class="four columns">
		NOMBRE
		<input type="text" id="nombre_pax" onkeyup="ejecutar_pres_intro(13,BuscarPasajeros);"/>	
	</div>
	<div class="four columns">
		APELLIDOS
		<input type="text" id="apellido_pax" onkeyup="ejecutar_pres_intro(13,BuscarPasajeros);"/>
	</div>
	<div class="four columns">
		PASAPORTE
		<input type="text" id="pasaporte_pax" onkeyup="ejecutar_pres_intro(13,BuscarPasajeros);"/>	
	</div>
	<div class="four columns">
		<input type="button" value="Buscar" onclick="BuscarPasajeros();" />	
	</div>

	<input type="hidden" value="<?php echo $cotizacion; ?>" id="cot" />
	<input type="hidden" value="" id="idcli" />
	<input type="hidden" value="" id="ncli" />
	<input type="hidden" value="" id="pcli" />
	<input type="hidden" value="" id="vpcli" />
	<input type="hidden" value="" id="naccli" />
	<input type="hidden" value="" id="scli" />

	<div class="sixteen columns" id="busqueda_pasajeros" style="overflow:auto; height:250px;">
    	<img id="img_load" src="lib/tinybox2/images/preload.gif" style="margin-top:100px; margin-left:50%; display:none;"/>
	</div>
	<div class="clear_mayor"></div>
    <div class="clear_mayor"></div>
    <div class="clear_mayor"></div>
	<strong>PASAJEROS SELECCIONADO</strong><hr/>
    <div class="sixteen columns" id="selec_pasajeros" style="overflow:auto; height:150px; border:1px #000000">
    	<table id="table_pax_sel" style="width:100%;">
		</table>
        <img id="img_load_save" src="lib/tinybox2/images/preload.gif" style="margin-top:50px; margin-left:47%; display:none;"/>
	</div>

	<div class="clear_mayor"></div>

	<div class="eight columns">
		<strong>ELEGIR CIUDAD DE ORIGEN : </strong>
    	<select id="ciudad_origen_pax">
    		<?php
    			$ciudades = ciudad_origen_paquete::obtener_ciudad_origen_paquete_de_cotizacion($idcotizacion);
    			for ($i=0; $i < count($ciudades); $i++) 
    			{ 
    				$ciudad = $ciudades[$i];
    			?>
    				<option value="<?php echo $ciudad['idciudad_origen_paquete']; ?>"><?php echo $ciudad['ciudad']; ?></option>
    			<?php
    			}
    		?>
    	</select>
    </div>

    <div class="eight columns" style="text-align:center;">
    	<input type="hidden" id="hd_nro_pax_sel" value="0"/>
    	<input type="button" value="Guardar Pasajeros" onclick="guardar_pax_seleccionado();"/>
    </div>

	<div class="eight columns">
		<strong id="pax_seleccionado">&nbsp;</strong>
	</div>

</div>