<div class="clear_mayor"></div>
<?php $ruta_imagenes_ch_g = "../../online/js/slider_inicio/promo/data1/images/"; ?>
<div style="display:none;">
    <div style="background:#ffffff; width:21cm; height:29.7cm;" class="myPrintArea" >
        <table style="width:21cm; font-family:Courier New; border:3px solid #000000;">
            <tr style="border-bottom:1px solid #aaa;">
                <td>
                    <img src="images\logo.png" style="width:350px; height:70px;" />    
                </td>
                <td style="vertical-align:top; text-align:right;">
                    CALLE VELASCO ESQ. SUAREZ DE FIGUEROA<br/>
                    TELF 591-3-363610<br/>
                    SANTA CRUZ - BOLIVIA<br/>
                </td>
            </tr>
             <tr>
                <td colspan="2" style="font-size: 25pt; color: #ff3300;font-weight: bold; text-align:center;">
                    <?php 
                    	$ruta = "../../online/js/slider_detalle_paquete/contenido_paquete/data1/images/";
                        $imagenes = imagen::obtener_imagenes_de_publicacion($evento["idpublicacion"]);
                        for ($i=0; $i < count($imagenes); $i++) { 
                            $imagen = $imagenes[$i];
                    ?>
                    <img src="<?php echo $ruta . $imagen["directorio"]; ?>" style="width:100%; margin-bottom:5px;" />
                    <?php
                        }
                    ?>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="font-size: 25pt; color: #ff3300;font-weight: bold; text-align:center; padding: 10px 0px; line-height:1.1;">
                    <font style="font-size:15pt; font-weight:bold; color:#ff3300;"><?php echo strtoupper($evento["titulo"] . '(' . $evento["nombre_generico"] . ')'); ?></font>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:center; padding:0px 0px 0px 0px;">
                <?php
                    $paginas_web = explode("\n", $evento['paginas_web']);
                    for ($i=0; $i < count($paginas_web); $i++)
                    {
                ?>
                        <a href="<?php echo $paginas_web[$i]; ?>" style="font-size:9pt;"><?php echo $paginas_web[$i]; ?></a><br/>
                <?php
                    }
                ?>
                </td>
            </tr>
            <tr>
	            <td style="padding:10px 10px 0px 30px;">
	            	<font style="font-size:10pt; font-weight:bold; color:#ff3300;">CLIENTE : </font>
	            	<?php echo strtoupper($cliente['trato_cliente'] . ' ' . $cliente['nombre_cliente'] . ' ' . $cliente['apellido_cliente']); ?>
	            </td>
	            <td>
	            	<font style="font-size:10pt; font-weight:bold; color:#ff3300;">COTIZACION NRO : </font>
	            	<?php echo $idcotizacion; ?>
	            </td>
            </tr>
            <tr>
            	<td style="padding:10px 10px 0px 30px;">
            		<?php 
            		if($cotizacion["salida"] != '')
            		{
            		?>
            		<font style="font-size:10pt; font-weight:bold; color:#ff3300;">SALIDA :</font><?php echo strtoupper($cotizacion["salida"]); ?>
            		<?php
            		}
            		?>
            	</td>
            	<td style="padding:10px 10px 0px 30px;">
            		<?php
            		if($cotizacion["retorno"] != '')
            		{
            		?>
            		<font style="font-size:10pt; font-weight:bold; color:#ff3300;">RETORNO :</font><?php echo strtoupper($cotizacion["retorno"]); ?>
            		<?php
            		}
            		?>
            	</td>
            </tr>
            <tr>
                <td colspan="2" style="padding:10px 0px 0px 30px;">
                    <font style="font-size:12pt; font-weight:bold; color:#ff3300;">PAQUETE INCLUYE</font>
                       <ul style="list-style: disc inside; margin:10px 0px 0px 20px; line-height:1;">
						<?php
							$paquete_incluye = explode("\n", $cotizacion['paquete_incluye']);
							for($i=0; $i<count($paquete_incluye); $i++)
							{
								if($paquete_incluye[$i] != "")
								{
						?>
									<li <?php if($paquete_incluye[$i]{0} == '@') { ?> style="font-weight:bold; list-style:square inside; margin: 2px 2px 2px 2px;" <?php } else { ?> style="font-size:9pt; margin: 2px 2px 2px 2px; margin-left:30px;" <?php } ?> >
										<?php echo strtoupper(str_replace('@', '', $paquete_incluye[$i])); ?>
			                        </li>
						<?php
								}
							}
						?>		
					</ul>
                </td>
            </tr>
            <tr class="precios_separado">
                <td colspan="2" style="padding:10px 0px 0px 30px;">
                    <?php
                    if($tarifas_hotel !== false)
                    {
                    ?>
                    <font style="font-size:13pt; font-weight:bold; color:#ff3300;">TARIFAS DE HOTELES POR PERSONA EN DOLARES AMERICANOS</font>
                    <?php
                    if($evento[0]["imagen_chica"] != '')
                    {
                    ?>
                    &nbsp;&nbsp;<a href="<?php echo $ruta_imagenes_ch_g . $evento["imagen_chica"]; ?>" target='_blank' style="font-size:9pt;">Ver mapa de Hoteles</a>
                    <?php
                    }
                    ?>
                    <br/>
                    <?php
						$columnas_ocultar   = $c_cotizacion_evento->evaluar_vista_hoteles($hoteles);
					?>
						<table style="width:95%; border:1px solid #AAAAAA;" class="con-borde" >
							<tr style="color:#FF3300; border:1px solid #AAAAAA; font-size:8pt;">
								<th>HOTEL</th>
								<?php if(strpos($columnas_ocultar, 'SGL') !== false) { ?><th>(SINGLE)</th><?php } ?>
								<?php if(strpos($columnas_ocultar, 'DBL') !== false) { ?><th>(DOBLE)</th><?php } ?>
								<?php if(strpos($columnas_ocultar, 'TPL') !== false) { ?><th>(TRIPLE)</th><?php } ?>
								<?php if(strpos($columnas_ocultar, 'CPL') !== false) { ?><th>(CDRPLE)</th><?php } ?>
								<?php if(strpos($columnas_ocultar, 'CNN') !== false) { ?><th>(MENOR)</th><?php } ?>
								<?php if(strpos($columnas_ocultar, 'INF') !== false) { ?><th>(INFANTE)</th><?php } ?>
								<th>(S. ALIM)</th>
                                <th></th>
							</tr>
					<?php
					//echo "<pre>"; print_r($hoteles); echo "</pre>";
						for ($i=0; $i < count($hoteles); $i++) 
						{ 
							$tarifa_hotel 	= $hoteles[$i];
							
					?>
							<tr style="font-size:8pt;">
								<td style="line-height:1; color:#10689b;">
									<?php echo strtoupper($tarifa_hotel['hotel']); ?>
								</td>
								<?php if(strpos($columnas_ocultar, 'SGL') !== false) { ?>
								<td>
									<?php echo (ceil($tarifa_hotel['precio_single'] + $total_otros_serv_mas_boletos_adt)); ?>
								</td>
								<?php }
								 if(strpos($columnas_ocultar, 'DBL') !== false) {  ?>
								<td>
									<?php echo (ceil($tarifa_hotel['precio_doble'] + $total_otros_serv_mas_boletos_adt)); ?>
								</td>
								<?php }
								 if(strpos($columnas_ocultar, 'TPL') !== false) {  ?>
								<td>
									<?php echo (ceil($tarifa_hotel['precio_triple'] + $total_otros_serv_mas_boletos_adt)); ?>
								</td>
								<?php }
								 if(strpos($columnas_ocultar, 'CPL') !== false) {  ?>
								<td>
									<?php echo (ceil($tarifa_hotel['precio_cuadruple'] + $total_otros_serv_mas_boletos_adt)); ?>
								</td>
								<?php }
								 if(strpos($columnas_ocultar, 'CNN') !== false) {  ?>
								<td>
									<?php echo (ceil($tarifa_hotel['precio_menor'] + $total_otros_serv_mas_boletos_cnn)); ?>
								</td>
								<?php }
								 if(strpos($columnas_ocultar, 'INF') !== false) {  ?>
								<td>
									<?php echo (ceil($tarifa_hotel['precio_infante'] + $total_otros_serv_mas_boletos_inf)); ?>
								</td>
								<?php } ?>
                                <td style="line-height:1;">
									<?php echo strtoupper($tarifa_hotel['alimentacion']); ?>
								</td>
								<td style="line-height:1;">
									<?php echo strtoupper($tarifa_hotel['info_extra']); ?>
								</td>
							</tr>
					<?php
						}
					?>
						</table>
                    <?php
                	}
                	?>
                </td>
            </tr>
            <tr class="precios_separado">
            	<td colspan="2" style="padding:10px 0px 0px 30px;">
            		<font style="font-size:13pt; font-weight:bold; color:#ff3300;">BOLETOS AEREOS EN DOLARES AMERICANOS POR PERSONA</font>
            		<table style="width:95%; border:1px solid #AAAAAA;" class="con-borde" >
						<tr style="color:#FF3300; border:1px solid #AAAAAA; font-size:8pt;">
							<th>LINEA AEREA</th>
							<th>RUTA</th>
							<th>ADULTO</th>
							<th>MENOR</th>
							<th>INFANTE</th>
							<th>T LIMIT</th>
						</tr>
						<?php
							for ($i=0; array_key_exists($i, $precios_boletos); $i++) 
							{ 
								$boleto_cotizado 	= $precios_boletos[$i];
								$idboleto 			= $boleto_cotizado['idboleto_cotizado_evento_cotizado'];
								$estado 			= $boleto_cotizado['estado'];
								if($estado == '1')
								{
						?>
						<tr style="font-size:8pt;">
							<td>
								<?php echo strtoupper($c_cotizacion_evento->obtener_linea_aerea($boleto_cotizado['linea_aerea'])); ?>
							</td>
							<td>
								<?php echo strtoupper($boleto_cotizado['ruta']); ?>
							</td>
							<td>
								<?php echo $boleto_cotizado['subtotal_adt']; ?>
							</td>
							<td>
								<?php echo $boleto_cotizado['subtotal_cnn']; ?>
							</td>
							<td>
								<?php echo $boleto_cotizado['subtotal_inf']; ?>
							</td>
							<td>
								<?php echo date('d/M/Y', strtotime($boleto_cotizado['time_limit'])); ?>
							</td>
						</tr>
						<?php
								}
							}
						?>
					</table>
					 <?php
					if($itinerario !== false)
					{
					?>
					<font style="font-size:13pt; font-weight:bold; color:#ff3300;">ITINERARIO</font>
					<table style="width:95%; border:1px solid #AAAAAA;" class="con-borde" >
						<tr style="color:#FF3300; border:1px solid #AAAAAA; font-size:8pt;">
							<th>(LINEA AEREA)</th>
							<th>(VUELO)</th>
							<th>(FECHA)</th>
							<th>(ORIGEN)</th>
							<th>(SALE)</th>
							<th>(DESTINO)</th>
							<th>(LLEGA)</th>
						</tr>
						<?php	
							for ($i=0; $i < count($itinerario); $i++) 
							{ 
								$segmento 	= $itinerario[$i];
								$idsegmento = $segmento['idsegmento_evento'];

								$linea_aerea 		= $c_cotizacion_evento->obtener_linea_aerea($segmento['idlineas_aereas']);
								$aeropuerto_sale 	= $c_cotizacion_evento->obtener_aeropuerto($segmento['origen']);
								$aeropuerto_llega 	= $c_cotizacion_evento->obtener_aeropuerto($segmento['destino']);
						?>
						<tr style="font-size:8pt;">
							<td style="line-height:1;"><?php echo $linea_aerea; ?></td>
							<td><?php echo $segmento['nro_vuelo']; ?></td>
							<td><?php echo date('d/m/Y', strtotime($segmento['fecha'])); ?></td>
							<td style="line-height:1;"><?php echo $aeropuerto_sale; ?></td>
							<td><?php echo substr($segmento['hora_sale'], 0, 5); ?></td>
							<td style="line-height:1;"><?php echo $aeropuerto_llega; ?></td>
							<td><?php echo substr($segmento['hora_llega'], 0, 5); ?></td>
						</tr>
						<?php
							}
						?>
					</table>
					<?php
					}
					?>
            	</td>
            </tr>
            <tr class="precios_resumido" style="display:none;">
                <td colspan="2" style="padding:10px 0px 0px 30px;">
                    <?php
                    if($tarifas_hotel !== false)
                    {
                    ?>
                    <font style="font-size:13pt; font-weight:bold; color:#ff3300;">TARIFAS POR PERSONA EXPRESADA EN DOLARES AMERICANOS</font>
                    <?php
                    if($evento[0]["imagen_chica"] != '')
                    {
                    ?>
                    &nbsp;&nbsp;<a href="<?php echo $ruta_imagenes_ch_g . $evento["imagen_chica"]; ?>" target='_blank' style="font-size:9pt;">Ver mapa de Hoteles</a>
                    <?php
                    }
                    ?>
                    <br/>
                    <?php
						$columnas_ocultar   = $c_cotizacion_evento->evaluar_vista_hoteles($hoteles);
					?>
						<table style="width:95%; border:1px solid #AAAAAA;" class="con-borde" >
							<tr style="color:#FF3300; border:1px solid #AAAAAA; font-size:8pt;">
								<th>HOTEL</th>
								<?php if(strpos($columnas_ocultar, 'SGL') !== false) { ?><th>(SINGLE)</th><?php } ?>
								<?php if(strpos($columnas_ocultar, 'DBL') !== false) { ?><th>(DOBLE)</th><?php } ?>
								<?php if(strpos($columnas_ocultar, 'TPL') !== false) { ?><th>(TRIPLE)</th><?php } ?>
								<?php if(strpos($columnas_ocultar, 'CPL') !== false) { ?><th>(CDRPLE)</th><?php } ?>
								<?php if(strpos($columnas_ocultar, 'CNN') !== false) { ?><th>(MENOR)</th><?php } ?>
								<?php if(strpos($columnas_ocultar, 'INF') !== false) { ?><th>(INFANTE)</th><?php } ?>
								<th>(S. ALIM)</th>
                                <th></th>
							</tr>
					<?php
					//echo "<pre>"; print_r($hoteles); echo "</pre>";
						for ($i=0; $i < count($hoteles); $i++) 
						{ 
							$tarifa_hotel 	= $hoteles[$i];
							
					?>
							<tr style="font-size:8pt;">
								<td style="line-height:1; color:#10689b;">
									<?php echo strtoupper($tarifa_hotel['hotel']); ?>
								</td>
								<?php if(strpos($columnas_ocultar, 'SGL') !== false) { ?>
								<td>
									<?php echo (ceil($tarifa_hotel['precio_single'] + $total_otros_serv_mas_boletos_adt + $total_boletos_adt)); ?>
								</td>
								<?php }
								 if(strpos($columnas_ocultar, 'DBL') !== false) {  ?>
								<td>
									<?php echo (ceil($tarifa_hotel['precio_doble'] + $total_otros_serv_mas_boletos_adt + $total_boletos_adt)); ?>
								</td>
								<?php }
								 if(strpos($columnas_ocultar, 'TPL') !== false) {  ?>
								<td>
									<?php echo (ceil($tarifa_hotel['precio_triple'] + $total_otros_serv_mas_boletos_adt + $total_boletos_adt)); ?>
								</td>
								<?php }
								 if(strpos($columnas_ocultar, 'CPL') !== false) {  ?>
								<td>
									<?php echo (ceil($tarifa_hotel['precio_cuadruple'] + $total_otros_serv_mas_boletos_adt + $total_boletos_adt)); ?>
								</td>
								<?php }
								 if(strpos($columnas_ocultar, 'CNN') !== false) {  ?>
								<td>
									<?php echo (ceil($tarifa_hotel['precio_menor'] + $total_otros_serv_mas_boletos_cnn + $total_boletos_cnn)); ?>
								</td>
								<?php }
								 if(strpos($columnas_ocultar, 'INF') !== false) {  ?>
								<td>
									<?php echo (ceil($tarifa_hotel['precio_infante'] + $total_otros_serv_mas_boletos_inf + $total_boletos_inf)); ?>
								</td>
								<?php } ?>
                                <td style="line-height:1;">
									<?php echo strtoupper($tarifa_hotel['alimentacion']); ?>
								</td>
								<td style="line-height:1;">
									<?php echo strtoupper($tarifa_hotel['info_extra']); ?>
								</td>
							</tr>
					<?php
						}
					?>
						</table>
                    <?php
                	}
                	?>
                	 <?php
					if($itinerario !== false)
					{
					?>
					<font style="font-size:13pt; font-weight:bold; color:#ff3300;">ITINERARIO</font>
					<table style="width:95%; border:1px solid #AAAAAA;" class="con-borde" >
						<tr style="color:#FF3300; border:1px solid #AAAAAA; font-size:8pt;">
							<th>(LINEA AEREA)</th>
							<th>(VUELO)</th>
							<th>(FECHA)</th>
							<th>(ORIGEN)</th>
							<th>(SALE)</th>
							<th>(DESTINO)</th>
							<th>(LLEGA)</th>
						</tr>
						<?php	
							for ($i=0; $i < count($itinerario); $i++) 
							{ 
								$segmento 	= $itinerario[$i];
								$idsegmento = $segmento['idsegmento_evento'];

								$linea_aerea 		= $c_cotizacion_evento->obtener_linea_aerea($segmento['idlineas_aereas']);
								$aeropuerto_sale 	= $c_cotizacion_evento->obtener_aeropuerto($segmento['origen']);
								$aeropuerto_llega 	= $c_cotizacion_evento->obtener_aeropuerto($segmento['destino']);
						?>
						<tr style="font-size:8pt;">
							<td style="line-height:1;"><?php echo $linea_aerea; ?></td>
							<td><?php echo $segmento['nro_vuelo']; ?></td>
							<td><?php echo date('d/m/Y', strtotime($segmento['fecha'])); ?></td>
							<td style="line-height:1;"><?php echo $aeropuerto_sale; ?></td>
							<td><?php echo substr($segmento['hora_sale'], 0, 5); ?></td>
							<td style="line-height:1;"><?php echo $aeropuerto_llega; ?></td>
							<td><?php echo substr($segmento['hora_llega'], 0, 5); ?></td>
						</tr>
						<?php
							}
						?>
					</table>
					<?php
					}
					?>
                </td>
            </tr>
            <tr class="precios_por_opciones_aereo_detallado" style="display:none;">
            	<td colspan="2" style="padding:10px 0px 0px 30px;">
            		<?php
            			for ($j=0; $j < count($opciones_aereo); $j++) 
						{
							$tarifa_aereo = $opciones_aereo[$j];
							if($tarifa_aereo['estado'] == 1)
							{
						?>
							<font style="font-size:13pt; font-weight:bold; color:#ff3300;"><?php echo $tarifa_aereo['descripcion']; ?>, SALE DE <?php echo $tarifa_aereo['salida']; ?></font><br/>
							<font style="font-size:10pt; font-weight:bold; color:#ff3300; margin-left:2%;">TARIFAS EXPRESADAS EN DOLARES AMERICANOS: HOTELES</font>
								<table style="width:97%; border:1px solid #AAAAAA; font-size:8pt; margin-left:2%;" class="con-borde" >
									<tr style="color:#FF3300; border:1px solid #AAAAAA; font-size:8pt;">
										<th>HOTEL</th>
										<?php if(strpos($columnas_ocultar, 'SGL') !== false) { ?><th>SINGLE</th><?php } ?>
										<?php if(strpos($columnas_ocultar, 'DBL') !== false) { ?><th>DOBLE</th><?php } ?>
										<?php if(strpos($columnas_ocultar, 'TPL') !== false) { ?><th>TRIPLE</th><?php } ?>
										<?php if(strpos($columnas_ocultar, 'CPL') !== false) { ?><th>CDRPLE</th><?php } ?>
										<?php if(strpos($columnas_ocultar, 'CNN') !== false) { ?><th>MENOR</th><?php } ?>
										<?php if(strpos($columnas_ocultar, 'INF') !== false) { ?><th>INFANTE</th><?php } ?>
										<th>ALIMENTACION</th>
										<th>OBS.</th>
									</tr>
							<?php
								for ($i=0; $i < count($hoteles); $i++) 
								{ 
									$tarifa_hotel 	= $hoteles[$i];
							?>
									<tr>
										<td style="line-height:1.2;">
											<?php echo $tarifa_hotel['hotel']; ?>
										</td>
										<?php if(strpos($columnas_ocultar, 'SGL') !== false) { ?>
										<td>
											<?php echo (ceil($tarifa_hotel['precio_single'] + $total_otros_serv_mas_boletos_adt)); ?>
										</td>
										<?php }
										 if(strpos($columnas_ocultar, 'DBL') !== false) {  ?>
										<td>
											<?php echo (ceil($tarifa_hotel['precio_doble'] + $total_otros_serv_mas_boletos_adt)); ?>
										</td>
										<?php }
										 if(strpos($columnas_ocultar, 'TPL') !== false) {  ?>
										<td>
											<?php echo (ceil($tarifa_hotel['precio_triple'] + $total_otros_serv_mas_boletos_adt)); ?>
										</td>
										<?php }
										 if(strpos($columnas_ocultar, 'CPL') !== false) {  ?>
										<td>
											<?php echo (ceil($tarifa_hotel['precio_cuadruple'] + $total_otros_serv_mas_boletos_adt)); ?>
										</td>
										<?php }
										 if(strpos($columnas_ocultar, 'CNN') !== false) {  ?>
										<td>
											<?php echo (ceil($tarifa_hotel['precio_menor'] + $total_otros_serv_mas_boletos_cnn)); ?>
										</td>
										<?php }
										 if(strpos($columnas_ocultar, 'INF') !== false) {  ?>
										<td>
											<?php echo (ceil($tarifa_hotel['precio_infante'] + $total_otros_serv_mas_boletos_inf)); ?>
										</td>
										<?php } ?>
										<td style="line-height:1.2;">
											<?php echo $tarifa_hotel['alimentacion']; ?>
										</td>
										<td style="line-height:1.2;">
											<?php echo $tarifa_hotel['info_extra']; ?>
										</td>
									</tr>
							<?php
								}
							?>
								</table>
								<font style="font-size:10pt; font-weight:bold; color:#ff3300; margin-left:2%;">PRECIOS DE BOLETOS AEREOS</font>
								<table style="width:97%; border:1px solid #AAAAAA; font-size:8pt; margin-left:2%;" class="con-borde" >
									<tr style="color:#FF3300; border:1px solid #AAAAAA; font-size:8pt;">
										<th>LINEA AEREA</th>
										<th>RUTA</th>
										<th>PRECIO ADULTO</th>
										<th>PRECIO MENOR</th>
										<th>PRECIO INFANTE</th>
										<th>T LIMIT</th>
									</tr>
									<?php
										$precios_boletos_opc = $tarifa_aereo['boletos'];
										for ($i=0; array_key_exists($i, $precios_boletos_opc); $i++) 
										{ 
											$boleto_cotizado 	= $precios_boletos_opc[$i];
											$idboleto 			= $boleto_cotizado['idboleto_cotizado_evento_cotizado'];
											$fact 				= (1+($boleto_cotizado['fee']/100)) * (1+($boleto_cotizado['factura']/100));
											$estado 			= $boleto_cotizado['estado'];
											if($estado == '1')
											{
												if($boleto_cotizado['precio_adulto'] > 0)
													$adt = ceil($boleto_cotizado['precio_adulto'] * $fact) . ' ' . $boleto_cotizado['nmoneda'];
												else
													$adt = '-';
												if($boleto_cotizado['precio_menor'] > 0)
													$cnn = ceil($boleto_cotizado['precio_menor'] * $fact) . ' ' . $boleto_cotizado['nmoneda'];
												else
													$cnn = '-';
												if($boleto_cotizado['precio_infante'] > 0)
													$inf = ceil($boleto_cotizado['precio_infante'] * $fact) . ' ' . $boleto_cotizado['nmoneda'];
												else
													$inf = '-';
									?>
									<tr>
										<td>
											<?php echo strtoupper($c_cotizacion_evento->obtener_linea_aerea($boleto_cotizado['linea_aerea'])); ?>
										</td>
										<td>
											<?php echo strtoupper($boleto_cotizado['ruta']); ?>
										</td>
										<td>
											<?php echo $adt; ?>
										</td>
										<td>
											<?php echo $cnn; ?>
										</td>
										<td>
											<?php echo $inf; ?>
										</td>
										<td>
											<?php echo date('d/M/Y', strtotime($boleto_cotizado['time_limit'])); ?>
										</td>
									</tr>
									<?php
											}
										}
									?>
								</table>
								<font style="font-size:10pt; font-weight:bold; color:#ff3300; margin-left:2%;">ITINERARIO</font>
								<table style="width:97%; border:1px solid #AAAAAA; font-size:8pt; margin-left:2%;" class="con-borde" >
									<tr style="color:#FF3300; border:1px solid #AAAAAA; font-size:8pt;">
										<th>LINEA AEREA</th>
										<th>VUELO</th>
										<th>FECHA</th>
										<th>ORIGEN</th>
										<th>SALE</th>
										<th>DESTINO</th>
										<th>LLEGA</th>
									</tr>
									<?php
										$itinerario_opc = $tarifa_aereo['itinerario'];

										if($itinerario_opc === false)
											$itinerario_opc = array();
										
										for ($i=0; $i < count($itinerario_opc); $i++) 
										{ 
											$segmento 	= $itinerario_opc[$i];
											$idsegmento = $segmento['idsegmento_evento'];

											$linea_aerea 		= $c_cotizacion_evento->obtener_linea_aerea($segmento['idlineas_aereas']);
											$aeropuerto_sale 	= $c_cotizacion_evento->obtener_aeropuerto($segmento['origen']);
											$aeropuerto_llega 	= $c_cotizacion_evento->obtener_aeropuerto($segmento['destino']);
									?>
									<tr>
										<td><?php echo $linea_aerea; ?></td>
										<td><?php echo $segmento['nro_vuelo']; ?></td>
										<td><?php echo date('d/m/Y', strtotime($segmento['fecha'])); ?></td>
										<td><?php echo $aeropuerto_sale; ?></td>
										<td><?php echo substr($segmento['hora_sale'], 0, 5); ?></td>
										<td><?php echo $aeropuerto_llega; ?></td>
										<td><?php echo substr($segmento['hora_llega'], 0, 5); ?></td>
									</tr>
									<?php
										}
									?>
								</table>
								<br/><br/>
					<?php
							}
						}
					?>
            	</td>
            </tr>
            <tr class="precios_por_opciones_aereo_resumido" style="display:none;">
            	<td colspan="2" style="padding:10px 0px 0px 30px;">
            		<?php
						for ($j=0; $j < count($opciones_aereo); $j++) 
						{
							$tarifa_aereo = $opciones_aereo[$j];
							if($tarifa_aereo['estado'] == 1)
							{
								$adt =  $tarifa_aereo['adt'];
								$cnn =  $tarifa_aereo['cnn'];
								$inf =  $tarifa_aereo['inf'];
						?>
							<font style="font-size:13pt; font-weight:bold; color:#ff3300;">TARIFA <?php echo $tarifa_aereo['descripcion']; ?>, SALE DE <?php echo $tarifa_aereo['salida']; ?></font><br/>
							<font style="font-size:10pt; font-weight:bold; color:#ff3300; margin-left:2%;">TARIFAS EXPRESADAS EN DOLARES AMERICANOS: HOTEL MAS BOLETO AEREO</font>
								<table style="width:97%; border:1px solid #AAAAAA; font-size:8pt; margin-left:2%;" class="con-borde" >
									<tr style="color:#FF3300; border:1px solid #AAAAAA; font-size:8pt;">
										<th>HOTEL</th>
										<?php if(strpos($columnas_ocultar, 'SGL') !== false) { ?><th>SINGLE</th><?php } ?>
										<?php if(strpos($columnas_ocultar, 'DBL') !== false) { ?><th>DOBLE</th><?php } ?>
										<?php if(strpos($columnas_ocultar, 'TPL') !== false) { ?><th>TRIPLE</th><?php } ?>
										<?php if(strpos($columnas_ocultar, 'CPL') !== false) { ?><th>CDRPLE</th><?php } ?>
										<?php if(strpos($columnas_ocultar, 'CNN') !== false) { ?><th>MENOR</th><?php } ?>
										<?php if(strpos($columnas_ocultar, 'INF') !== false) { ?><th>INFANTE</th><?php } ?>
										<th>ALIMENTACION</th>
										<th>OBS.</th>
									</tr>
							<?php
								for ($i=0; $i < count($hoteles); $i++) 
								{ 
									$tarifa_hotel 	= $hoteles[$i];
							?>
									<tr>
										<td style="line-height:1.2;">
											<?php echo $tarifa_hotel['hotel']; ?>
										</td>
										<?php if(strpos($columnas_ocultar, 'SGL') !== false) { ?>
										<td>
											<?php echo (ceil($tarifa_hotel['precio_single'] + $total_otros_serv_mas_boletos_adt + $adt)); ?>
										</td>
										<?php }
										 if(strpos($columnas_ocultar, 'DBL') !== false) {  ?>
										<td>
											<?php echo (ceil($tarifa_hotel['precio_doble'] + $total_otros_serv_mas_boletos_adt + $adt)); ?>
										</td>
										<?php }
										 if(strpos($columnas_ocultar, 'TPL') !== false) {  ?>
										<td>
											<?php echo (ceil($tarifa_hotel['precio_triple'] + $total_otros_serv_mas_boletos_adt + $adt)); ?>
										</td>
										<?php }
										 if(strpos($columnas_ocultar, 'CPL') !== false) {  ?>
										<td>
											<?php echo (ceil($tarifa_hotel['precio_cuadruple'] + $total_otros_serv_mas_boletos_adt + $adt)); ?>
										</td>
										<?php }
										 if(strpos($columnas_ocultar, 'CNN') !== false) {  ?>
										<td>
											<?php echo (ceil($tarifa_hotel['precio_menor'] + $total_otros_serv_mas_boletos_cnn + $cnn)); ?>
										</td>
										<?php }
										 if(strpos($columnas_ocultar, 'INF') !== false) {  ?>
										<td>
											<?php echo (ceil($tarifa_hotel['precio_infante'] + $total_otros_serv_mas_boletos_inf + $inf)); ?>
										</td>
										<?php } ?>
										<td style="line-height:1.2;">
											<?php echo $tarifa_hotel['alimentacion']; ?>
										</td>
										<td style="line-height:1.2;">
											<?php echo $tarifa_hotel['info_extra']; ?>
										</td>
									</tr>
							<?php
								}
							?>
								</table>
								<font style="font-size:10pt; font-weight:bold; color:#ff3300; margin-left:2%;">ITINERARIO</font>
								<table style="width:97%; border:1px solid #AAAAAA; font-size:8pt; margin-left:2%;" class="con-borde" >
									<tr style="color:#FF3300; border:1px solid #AAAAAA; font-size:8pt;">
										<th>LINEA AEREA</th>
										<th>VUELO</th>
										<th>FECHA</th>
										<th>ORIGEN</th>
										<th>SALE</th>
										<th>DESTINO</th>
										<th>LLEGA</th>
									</tr>
									<?php
										$itinerario_opc = $tarifa_aereo['itinerario'];

										if($itinerario_opc === false)
											$itinerario_opc = array();
										
										for ($i=0; $i < count($itinerario_opc); $i++) 
										{ 
											$segmento 	= $itinerario_opc[$i];
											$idsegmento = $segmento['idsegmento_evento'];

											$linea_aerea 		= $c_cotizacion_evento->obtener_linea_aerea($segmento['idlineas_aereas']);
											$aeropuerto_sale 	= $c_cotizacion_evento->obtener_aeropuerto($segmento['origen']);
											$aeropuerto_llega 	= $c_cotizacion_evento->obtener_aeropuerto($segmento['destino']);
									?>
									<tr>
										<td><?php echo $linea_aerea; ?></td>
										<td><?php echo $segmento['nro_vuelo']; ?></td>
										<td><?php echo date('d/m/Y', strtotime($segmento['fecha'])); ?></td>
										<td><?php echo $aeropuerto_sale; ?></td>
										<td><?php echo substr($segmento['hora_sale'], 0, 5); ?></td>
										<td><?php echo $aeropuerto_llega; ?></td>
										<td><?php echo substr($segmento['hora_llega'], 0, 5); ?></td>
									</tr>
									<?php
										}
									?>
								</table>
								<br/><br/>
					<?php
							}
						}
					?>
            	</td>
            </tr>
            <!--<tr>
                <td colspan="2" style="padding:10px 0px 0px 30px;">
	                <?php
					if($itinerario !== false)
					{
					?>
					<font style="font-size:13pt; font-weight:bold; color:#ff3300;">ITINERARIO</font>
					<table style="width:95%; border:1px solid #AAAAAA;" class="con-borde" >
						<tr style="color:#FF3300; border:1px solid #AAAAAA; font-size:8pt;">
							<th>(LINEA AEREA)</th>
							<th>(VUELO)</th>
							<th>(FECHA)</th>
							<th>(ORIGEN)</th>
							<th>(SALE)</th>
							<th>(DESTINO)</th>
							<th>(LLEGA)</th>
						</tr>
						<?php	
							for ($i=0; $i < count($itinerario); $i++) 
							{ 
								$segmento 	= $itinerario[$i];
								$idsegmento = $segmento['idsegmento_evento'];

								$linea_aerea 		= $c_cotizacion_evento->obtener_linea_aerea($segmento['idlineas_aereas']);
								$aeropuerto_sale 	= $c_cotizacion_evento->obtener_aeropuerto($segmento['origen']);
								$aeropuerto_llega 	= $c_cotizacion_evento->obtener_aeropuerto($segmento['destino']);
						?>
						<tr style="font-size:8pt;">
							<td style="line-height:1;"><?php echo $linea_aerea; ?></td>
							<td><?php echo $segmento['nro_vuelo']; ?></td>
							<td><?php echo date('d/m/Y', strtotime($segmento['fecha'])); ?></td>
							<td style="line-height:1;"><?php echo $aeropuerto_sale; ?></td>
							<td><?php echo substr($segmento['hora_sale'], 0, 5); ?></td>
							<td style="line-height:1;"><?php echo $aeropuerto_llega; ?></td>
							<td><?php echo substr($segmento['hora_llega'], 0, 5); ?></td>
						</tr>
						<?php
							}
						?>
					</table>
					<?php
					}
					?>
                </td>
            </tr>-->
            <tr>
                <td colspan="2" style="padding:10px 0px 0px 30px;">
                    <font style="font-size:12pt; font-weight:bold; color:#ff3300;">IMPORTANTE</font>
                    <ul style="list-style: disc inside; margin:10px 0px 0px 20px; line-height:1;">
						<?php
							$importante = explode("\n", $cotizacion['importante']);
							for($i=0; $i<count($importante); $i++)
							{
								if($importante[$i] != "")
								{
						?>
									<li style="font-size:9pt; margin: 2px 2px 2px 2px;">
										<?php echo strtoupper($importante[$i]); ?>
									</li>
						<?php
								}
							}
						?>		
					</ul>
                </td>
            </tr>
            <?php
            if($evento["imagen_grande"] != '')
            {
            ?>
            <tr>
                <td colspan="2" style="padding:10px 0px 0px 30px;">
                    <font style="font-size:13pt; font-weight:bold; color:#ff3300;">INSCRIPCION</font>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding:0px 0px 0px 30px;">
                    <center><img src="<?php echo $ruta_imagenes_ch_g . $evento["imagen_grande"]; ?>" style="width:650px; margin-bottom:5px; margin-top:0px;" /></center>
                </td>
            </tr>
            <?php
            }
            ?>
        </table>
    </div>
</div>