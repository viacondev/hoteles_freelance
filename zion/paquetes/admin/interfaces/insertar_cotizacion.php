<?php 
	include 'header.php';
	include('../entidad/publicacion.php');
	include('../entidad/evento.php');
?>
<form method="post" class="sixteen columns" action="registrar_nueva_cotizacion.php" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1; padding-top:10px; padding-bottom:10px;" >

	<div class="fifteen columns" style="font-weight:bold; color:#10689b; font-size:14pt; margin:10px; text-align:center;">NUEVA COTIZACION</div>

	<div class="columns two" style="font-weight:bold; color:#10689b;">
		TITULO:
	</div>
	<div class="columns thirteen">
		<input type="text" name="titulo" id="titulo" onchange="" style="padding:5px; width:90%;" />
	</div>

	<div class="clear"></div>

	<div class="columns two" style="font-weight:bold; color:#10689b;">
		OBSERVACION:
	</div>
	<div class="columns thirteen">
		<textarea name="observacion" style="width:90%;"></textarea>
	</div>
	
	<hr/>

	<div class="three columns">
		BUSCAR CLIENTE ... 
	</div>
	<div class="two columns">
		<input type="radio" value="natural" name="tipo_cliente" checked onchange="MostrarBusquedaClientes();" />Natural
	</div>
	<div class="three columns">
		<input type="radio" value="empresa_agencia" name="tipo_cliente" onchange="MostrarBusquedaClientes();" />Empresa o Agencia
	</div>

	<div class="clear_mayor"></div>

	<div class="three columns">
		<input type="text" placeholder="Nombre" id="nombre_cliente"/>
	</div>
	<div class="three columns">
		<input type="text" placeholder="Apellidos" id="apellido_cliente">
	</div>
	<div class="three columns">
		<input type="text" placeholder="Pasaporte" id="pasaporte_cliente">
	</div>
	<div class="four columns">
		<input type="button" onclick="BuscarClientes();" value="Buscar Cliente" />
		<input type="button" id="boton_buscar_solicitante" onclick="BuscarSolicitante();" value="Buscar Solicitante" style="display:none;" />
	</div>
	<div class="fifteen columns">
		<table style="width:100%;" style="border:1px solid #aaa;">
			<tr style="border:1px solid #aaa; background-color:#10689b; color:#ffffff; font-weight:bold;">
				<td style="width:70%; text-align:center;">CLIENTE</td>
				<td>PASAPORTE</td>
				<td></td>
			</tr>
		</table>
	</div>
	<div class="fifteen columns" id="filtro_clientes" style="height:150px; overflow:auto;">
		BUSQUEDA DE CLIENTES
	</div>

	<hr/>
	<div class="clear"></div>

	<div class="columns two" style="font-weight:bold; color:#10689b;">
		CLIENTE:
	</div>
	<div class="columns thirteen">
		<strong id="cliente_seleccionado">NO SE HA SELECCIONADO NING&Uacute;N CLIENTE.</strong>
	</div>

	<div id="panel_solicitante" style="display:none;">
		<div class="columns two" style="font-weight:bold; color:#10689b;">
			SOLICITANTE:
		</div>
		<div class="columns thirteen">
			<strong id="solicitante_seleccionado">NO SE HA SELECCIONADO EL SOLICITANTE.</strong>
		</div>
	</div>

	<div class="sixteen columns" style="height:80px;">
		<input type="hidden" id="c_cliente" name="c_cliente" />
		<input type="hidden" id="c_solicitante" name="c_solicitante" />
	</div>

	<div class="sixteen columns">
		<input type="submit" name="registrar_cotizacion" style="float:right; font-size:15pt; padding: 5px 15px; margin: 10px 20px;" value="Registrar" />
	</div>

</form>

<?php
	include("footer.php");
?>