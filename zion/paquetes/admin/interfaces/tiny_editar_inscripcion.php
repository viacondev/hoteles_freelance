<?
	include '../BD/controladoraBD.php';
	include '../entidad/inscripcion_cotizado.php';

	$idpax 			= $_GET['idpax'];
	$tipo 			= $_GET['tipo'];
	$idcotizacion 	= $_GET['idcotizacion'];
	
	if($tipo == 1)
	{
		$class = "paquete";
		$abrev = "IPC";
	}
	else
	{
		$class = "evento";
		$abrev = "IEC";
	}

	$resp = inscripcion_cotizado::obtener_inscripcion_idpax($idpax);
		
	if(count($resp) > 0)
	{
		for($i = 0; $i < count($resp); $i++)
		{
			$precio 	= 0;
			$fee 		= 0;
			$factura	= 0;

			$precio 	= $resp[$i]['precio'];
			$factor 	= (1+$resp[$i]['fee']/100) * (1+$resp[$i]['factura']/100);
			$precio 	= $precio * $factor;
			
			$vec[$i]['idinscripcion_cotizado'] 	= $resp[$i]['idinscripcion_cotizado'];
			$vec[$i]['categoria'] 				= $resp[$i]['categoria'];
			$vec[$i]['precio'] 					= $precio;
			$vec[$i]['moneda'] 					= $resp[$i]['moneda'];
			$vec[$i]['tipo_cambio'] 			= $resp[$i]['tipo_cambio'];
			$vec[$i]['estado'] 					= $resp[$i]['estado'];
		}
	}
	$resp_combo = inscripcion_cotizado::obtener_inscripcion_cotizados($idcotizacion);
?>
    <link href="lib/tinybox2/style.css" rel="stylesheet"/>
    	<div class="sixteen columns" style="color:#FFFFFF; position:absolute; padding:10px; opacity:0.9; display:none;" id="mensaje_pax">
		</div>
		<div class="sixteen columns">
        	<h3>Editar Inscripcion</h3>
            <label id="nombre_pax_tiny"></label>
            <hr/>
            <span>
            	<select id="cb_inscripcion">
                    <option value="0">-- Categoria --</option>
                    <?
                    for($i = 0; $i < count($resp_combo); $i++)
					{
						if($resp_combo[$i]['estado'] == 0)
						{
							echo "<option disabled='disabled' style='color:#ff2d32;' value=".$resp_combo[$i]['idinscripcion_cotizado']." class='cb_delete'>".strtoupper($resp_combo[$i]['categoria'])."</option>";
						}
						else
						{
							echo "<option value=".$resp_combo[$i]['idinscripcion_cotizado']." class='cb_delete'>".strtoupper($resp_combo[$i]['categoria'])."</option>";
						}
					}
					?>
                </select>
            </span>
            <span>
            	<input type="button" value="Agregar" onclick="guardar_inscripcion_pax(<? echo $idpax;?>);"/>
            </span>
            <hr/>
            <div class="fourteen columns" id="table_tiny_inscripcion_<? echo $idpax;?>">
           <table style="width:99%; border:1px solid #AAAAAA; font-size:8pt;display:;" class="con-borde user_info_editable">
            <tr style="background-color:#10689b; color:#FFFFFF;border-color:#FFF; height:20px;">
                <th rowspan="2" style="vertical-align:middle;">Nro</th>
                <th rowspan="2" style="vertical-align:middle; border-left:solid 1px #FFFFFF;">Categoria</th>
                <th colspan="2" style="border-bottom: solid 1px #FFFFFF;border-left: solid 1px #FFFFFF;">Precio</th>
                <th rowspan="2" style="width:5px; border-left: solid 1px #FFFFFF;">&nbsp;</th>
                <th rowspan="2" style="width:5px; border-left: solid 1px #FFFFFF;">&nbsp;</th>
            </tr>
            <tr style="background-color:#10689b; color:#FFFFFF;border-color:#FFF;height:20px;">
                <th style="border-left: solid 1px #FFFFFF;border-right: solid 1px #FFFFFF;">BOB</th>
                <th>USD</th>
            </tr>
            	<?
			$total_b = 0;
			$total_u = 0;
			$totales = 0;
			if(count($vec) > 0)
			{
				$nro = 0;
				foreach($vec as $tabla)
				{
					$ident = $tabla['idinscripcion_cotizado'];
					
					if($tabla['moneda'] == "B")
					{
						$total_b += $tabla['precio'];
						$totales += round($tabla['precio'] / $tabla['tipo_cambio'],2);
					}
					else
					{
						$total_u += $tabla['precio'];
						$totales += round($tabla['precio'],2);
					}
					
			?>
                <tr valign="middle" style="height:15px;<? if($tabla['estado'] == 0) echo "color:#ff2d32;";?>">
                	<td><input type="hidden" id="hd_inscripcion_cotizado_<? echo $tabla['idinscripcion_cotizado'];?>"/><? echo $nro = $nro +1;?></td>
                    <td>
                    	<? echo strtoupper($tabla['categoria'])?>
                    	<div id="archivos<?php echo $ident; ?>" style="display:none; background-color:#f1f1f1; border-radius:5px; border:1px solid #aaaaaa; position:absolute; width:350px; height:200px;">
                		</div>
                    </td>
                    <td style="text-align:right;"><? echo $tabla['moneda'] == "B" ? number_format(round($tabla['precio'],2),2): "0.00";?></td>
                    <td style="text-align:right;"><? echo $tabla['moneda'] == "U" ? number_format(round($tabla['precio'],2),2): "0.00";?></td>
                    <td><img src="images/cross.png" title="Eliminar Inscripcion" style="cursor:pointer;" onclick="eliminar_inscripcion_pax(<? echo $tabla['idinscripcion_cotizado'];?>,<? echo $idpax;?>);"/></td>
                    <td><img src="images/folder.png" title="Archivos Adjuntos" style="cursor:pointer;" onclick="mostrar_dialogo_archivos_x_item('<? echo $ident; ?>', '<?php echo $idpax; ?>', '<?php echo $abrev; ?>');"/></td>
                </tr>
			<?
				}
			}
			else
			{
				?>
				<tr style="height:15px;">
                    <td colspan="6">No Existe Categoria de la Inscripcion Cargados.</td>                    
                </tr>
				<?
					
			}
			?>
            	<tr style="height:15px;">
            		<td colspan="2" style="text-align:right;"><strong>Total</strong></td>
                	<td style="text-align:right;"><? echo number_format(round($total_b,2),2);?></td>
                    <td style="text-align:right;"><? echo number_format(round($total_u,2),2);?></td>
                    <td>&nbsp;</td> 
                    <td>&nbsp;</td> 
            	</tr>
            	</table>
                <div class="clear_mayor"></div><div class="clear_mayor"></div><div class="clear_mayor"></div>
                <div><strong>Total en USD&nbsp;:&nbsp;&nbsp;</strong><span style="width:15px;"><? echo $totales;?></span></div>                
			</div>
            <div class="clear_mayor"></div>
            <div class="nine columns" style="text-align:center;"> 
            	<img id="img_load_inscripcion_pax" src="lib/tinybox2/images/preload.gif" style="display:none;" />
            </div>
        </div>
