<?php
	include 'header.php';
	include('../entidad/tipo_evento.php');
	include('../entidad/temporada.php');
	include('../entidad/publicacion.php');
	include('../entidad/evento.php');
	include('../entidad/imagen.php');
	include('../entidad/archivo_adjunto.php');
	include('../entidad/operadora.php');
	include('../entidad/destino.php');
	include('../entidad/tarifa_hotel.php');
	include('../entidad/tarifa_otros.php');
	include('../entidad/boleto_cotizado.php');
	include('../entidad/segmento_publicado.php');
	include('../entidad/lineas_aereas.php');
	include('../entidad/aeropuerto.php');
	include('../entidad/opcion_hoteles.php');
	include('../entidad/pqt_otro_servicio.php');
	//include('../entidad/pqt_seguimiento.php');
	include('../control/c_mostrar_evento.php');

	$c_mostrar_evento 	= new c_mostrar_evento;

	$idpublicacion 		= $_GET['idevento'];

	$paquete 	= publicacion::obtener_info_paquete($idpublicacion);
	$paquete 	= $paquete[0];

	$imagenes 	= publicacion::obtener_imagenes_por_publicacion($idpublicacion);

?>

<?php
	if($paquete['solo_imagen'] == '1')
	{
	?>
		<div class="columns sixteen" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#F1F1F1; text-align:center;">
			<h4 style="padding:10px; text-align:center;"><?php echo strtoupper($paquete['titulo']); ?></h4>
			<div class="columns two" style="font-weight:bold; color:#10689b;">
				CODIGO
			</div>
			<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
			<div class="columns four">
				<?php echo $paquete["idpublicacion"]; ?>
			</div>
			<div class="clear"></div>
			<img src="<?php echo $ruta_imagen . $imagenes[0]['nombre']; ?>" style="max-width:95%;" />
			<div class="clear_mayor"></div>
		</div>	
<?php
	}
	else
	{
	?>
		<div class="columns sixteen">
			<?php
				if($paquete['imagen_tercera'] != '')
				{
				?>
					<img src="<?php echo $ruta_imagen . $paquete['imagen_tercera']; ?>" style="width:100%; border-radius:10px;" />
					<div class="clear_mayor"></div>
				<?php
				}
			?>
		</div>

		<h4 style="padding:10px; text-align:center;"><?php echo strtoupper($paquete['titulo']); ?></h4>

		<div class="columns eight" style="margin:8px; border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">	
			
			<div class="columns seven" style="font-weight:bold; color:#10689b; text-align:center; font-size:14pt;">
				PAQUETE
			</div>	

			<div class="clear_mayor"></div>		

			<div class="columns two" style="font-weight:bold; color:#10689b;">
				CODIGO
			</div>
			<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
			<div class="columns four">
				<?php echo $paquete["idpublicacion"]; ?>
			</div>
			<div class="clear_mayor"></div>
			<div class="columns two" style="font-weight:bold; color:#10689b;">
				SALIDA
			</div>
			<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
			<div class="columns four">
				<?php 
					$fecha_salida = strtotime($paquete["fecha_salida"]);
					if($fecha_salida)
						$fecha_salida = date('d/m/Y', $fecha_salida);
					else
						$fecha_salida = $paquete["fecha_salida"];
					echo $fecha_salida;
				?>
			</div>
			<div class="columns two" style="font-weight:bold; color:#10689b;">
				RETORNO
			</div>
			<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
			<div class="columns four">
				<?php 
					$fecha_retorno = strtotime($paquete["fecha_retorno"]);
					if($fecha_retorno)
						$fecha_retorno = date('d/m/Y', $fecha_retorno);
					else
						$fecha_retorno = $paquete["fecha_retorno"];
					echo $fecha_retorno;
				?>
			</div>
			<div class="clear_mayor"></div>
			<div class="columns two" style="font-weight:bold; color:#10689b;">
				USUARIO
			</div>
			<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
			<div class="columns four">
				<?php echo $paquete["nombre_usuario"]; ?>
			</div>
			<div class="columns two" style="font-weight:bold; color:#10689b;">
				VALIDO HASTA
			</div>
			<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
			<div class="columns four">
				<?php echo date('d/m/Y',strtotime($paquete["fecha_caducidad"])); ?>
			</div>			
		</div>	

		<?php
			$evento = $c_mostrar_evento->obtener_evento_por_publicacion($idpublicacion);
			if(count($evento) > 0)
			{
			?>
				<div class="columns eight" style="margin:8px; border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">	
			
					<div class="columns seven" style="font-weight:bold; color:#10689b; text-align:center; font-size:14pt;">
						EVENTO
					</div>	

					<div class="clear_mayor"></div>		

					<div class="columns two" style="font-weight:bold; color:#10689b;">
						NOMBRE
					</div>
					<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
					<div class="columns four" style="line-height:1;">
						<?php echo $evento["nombre_evento"]; ?>
					</div>
					<div class="columns two" style="font-weight:bold; color:#10689b;">
						SIGLA
					</div>
					<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
					<div class="columns four">
						<?php echo $evento["sigla_evento"]; ?>
					</div>
					<div class="columns two" style="font-weight:bold; color:#10689b;">
						SEDE
					</div>
					<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
					<div class="columns four">
						<?php echo $evento["sede"]; ?>
					</div>
					<div class="clear_mayor"></div>
					<div class="columns two" style="font-weight:bold; color:#10689b;">
						INICIO
					</div>
					<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
					<div class="columns four">
						<?php echo date('d/m/Y',strtotime($evento["fecha_inicio"])); ?>
					</div>
					<div class="columns two" style="font-weight:bold; color:#10689b;">
						FIN
					</div>
					<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
					<div class="columns four">
						<?php echo date('d/m/Y',strtotime($evento["fecha_fin"])); ?>
					</div>
				</div>
				<?php
					if($evento['paginas_web'] != '')
					{
					?>
						<div class="clear_mayor"></div>

						<div class="sixteen columns" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
							<div class="columns two" style="font-weight:bold; color:#10689b;">
								PAGINAS WEB
							</div>
							<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
							<div class="columns ten">
								<ul class="lista_descripcion" id="lista_paginas_web" style="width:90%;">
									<?php
										$paginas_web = explode("\n", $evento['paginas_web']);
										for($i=0; $i<count($paginas_web); $i++)
										{
											if($paginas_web[$i] != "")
											{
									?>
												<li>
													<a href="<?php echo $paginas_web[$i]; ?>"><?php echo substr($paginas_web[$i], 0, 80) . ' ...'; ?></a>
												</li>
									<?php
											}
										}
									?>		
								</ul>
							</div>
						</div>
					<?php
					}
				?>
			<?php
			}
		?>
					
		

		<div class="clear_mayor"></div>

		<?php
			$ciudades = $c_mostrar_evento->obtener_ciudades_de_salida($idpublicacion);
		?>
		<div class="sixteen columns" id="botones_por_ciudad_origen">
			<?php
				for ($z=0; $z < count($ciudades); $z++) 
				{ 
				?>
					<input type="button" id="btn_ciudad_origen_<?php echo $z; ?>" class="btn_c_o" value="Desde <?php echo $ciudades[$z]['ciudad']; ?>" style="float:right;<?php if($z != 0) echo 'background-color:#AAAAAA;'; ?>" onclick="RevelarTabCiudad(<?php echo $z; ?>);" />
				<?php
				}
			?>&nbsp;
		</div>

		<?php
			for ($z=0; $z < count($ciudades); $z++) 
			{ 
				$ciudad 			= $ciudades[$z];
				$idciudad_origen 	= $ciudad['idciudad_origen_paquete'];
			?>
			<div style="border:1px solid #AAAAAA; border-radius:10px; background-color:#F1F1F1;<?php if($z > 0) echo ' display:none;'; ?>" class="sixteen columns tab_ciudad" id="tab_ciudad_<?php echo $z; ?>" >

				<div class="sixteen columns" style="font-weight:bold; color:#10689b; font-size:14pt; margin:10px; text-align:center;">SALE DESDE <?php echo $c_mostrar_evento->obtener_aeropuerto($ciudad['ciudad']); ?></div>
				
				<div class="clear"></div>

				<!-- FECHA DE SALIDA -->
				<div class="one columns" style="font-weight:bold; color:#10689b; height:30px;">
					&nbsp;
				</div>
				<div class="two columns" style="font-weight:bold; color:#10689b; height:30px;">
					SALIDA :
				</div>
				<div class="columns four" style="height:30px; font-size:9pt; color: #444;" onmouseover="revelar_icono_edicion('fecha_salida_<?php echo $idciudad_origen; ?>');" onmouseout="ocultar_icono_edicion('fecha_salida_<?php echo $idciudad_origen; ?>');">
					<?php 
						$fecha_salida = strtotime($ciudad["fecha_salida"]);
						if($fecha_salida)
							$fecha_salida = date('d/m/Y', $fecha_salida);
						else
							$fecha_salida = $ciudad["fecha_salida"];
						if($fecha_salida != '')
							echo $fecha_salida;
						else
							echo 'NO INGRESADO';
					?>
				</div>

				<!-- FECHA DE RETORNO -->
				<div class="columns two" style="font-weight:bold; color:#10689b; height:30px;">
					RETORNO :
				</div>
				<div class="columns four" style="height:30px; font-size:9pt; color: #444;" onmouseover="revelar_icono_edicion('fecha_retorno_<?php echo $idciudad_origen; ?>');" onmouseout="ocultar_icono_edicion('fecha_retorno_<?php echo $idciudad_origen; ?>');">
					<?php 
						$fecha_retorno = strtotime($ciudad["fecha_retorno"]);
						if($fecha_retorno)
							$fecha_retorno = date('d/m/Y', $fecha_retorno);
						else
							$fecha_retorno = $ciudad["fecha_retorno"];
						if($fecha_retorno != '')
							echo $fecha_retorno;
						else
							echo 'NO INGRESADO';
					?>	
				</div>

				<div class="clear_mayor"></div>

				<!-- PAQUETE INCLUYE -->
				<?php
					if($ciudad['paquete_incluye'] != '')
					{
					?>
						<div class="columns fifteen">
							<div class="columns five" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">PAQUETE INCLUYE</div>
							<div class="clear"></div>
							<ul class="lista_descripcion">
								<?php
									$paquete_incluye = explode("\n", $ciudad['paquete_incluye']);
									for($i=0; $i<count($paquete_incluye); $i++)
									{
										if($paquete_incluye[$i] != "")
										{
								?>
											<li <?php if($paquete_incluye[$i][0] == '@') { $paquete_incluye[$i] = substr($paquete_incluye[$i], 1); ?> style="font-weight:bold; list-style:none; font-size:15pt;" <?php } ?> >
												<a><?php echo $paquete_incluye[$i]; ?></a>
											</li>
								<?php
										}
									}
								?>		
							</ul>
						</div>
					<?php
					}
				?>
				

				<!-- HOTELES -->
				<?php
					$tarifas_por_destino =  $c_mostrar_evento->obtener_destinos_de_hotel($idciudad_origen);
					if(count($tarifas_por_destino) > 0)
					{
					?>
						<div class="columns fifteen todo_precio solo_hotel" style="display:none;">
							<div class="columns fourteen" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">PRECIOS POR PERSONA EN DOLARES AMERICANOS</div>
							<div class="clear"></div>
							<div class="columns thirteen" style="font-weight:bold; color:#10689b; font-size:11pt; margin-left:2%;">HOTELES</div>
							<?php 
								for ($i=0; $i < count($tarifas_por_destino); $i++) 
								{ 
									$tarifa_hotel 		= $tarifas_por_destino[$i];
									$idth 				= $tarifa_hotel['idtarifa_hotel'];
									$items_tarifa 		= $c_mostrar_evento->obtener_hoteles_x_destino($idth);

									$hide_columns 		= $c_mostrar_evento->evaluar_precios_hotel($items_tarifa);
									$cant_col 			= count(explode('/', $hide_columns));
								?>
									<table style="width:50%; margin-left:3%;">
										<tr>
											<td style="width:5%;">
												<strong>DESTINO :</strong>
											</td>
											<td style="width:20%;">
												<?php echo strtoupper($tarifa_hotel['destino']); ?>
											</td>
											<td>
											</td>
										</tr>
									</table>
									<table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios">
										<tr style="background-color:#10689b; color:#FFFFFF;">
											<th>HOTEL</th>
											<?php if(strpos($hide_columns, 'SGL')===false) { ?><th>SINGLE</th><?php } ?>
											<?php if(strpos($hide_columns, 'DBL')===false) { ?><th>DOBLE</th><?php } ?>
											<?php if(strpos($hide_columns, 'TPL')===false) { ?><th>TRIPLE</th><?php } ?>
											<?php if(strpos($hide_columns, 'CPL')===false) { ?><th>CUADRUPLE</th><?php } ?>
											<?php if(strpos($hide_columns, 'CNN')===false) { ?><th>MENOR</th><?php } ?>
											<?php if(strpos($hide_columns, 'INF')===false) { ?><th>INFANTE</th><?php } ?>
											<th>ALIMENTACION</th>
											<th>OBS</th>
										</tr>
								<?php
									for ($j=0; $j < count($items_tarifa); $j++) 
									{ 
										$item_tarifa 	= $items_tarifa[$j];
										$iditem 		= $item_tarifa['iditem_hotel'];

										$cant_noches 	= floor( (strtotime($item_tarifa['fecha_out'] . ' 00:00:00') - strtotime($item_tarifa['fecha_in'] . ' 00:00:00')) / 86400);
										
										$total_sgl 	= $item_tarifa['precio_single'] * $cant_noches;
										$total_dbl 	= $item_tarifa['precio_doble'] * $cant_noches;
										$total_tpl 	= $item_tarifa['precio_triple'] * $cant_noches;
										$total_cpl 	= $item_tarifa['precio_cuadruple'] * $cant_noches;
										$total_cnn 	= $item_tarifa['precio_menor'] * $cant_noches;
										$total_inf 	= $item_tarifa['precio_infante'] * $cant_noches;

										if($item_tarifa['increment_fee'] == 'P')
										{
											$fee 		= 1 + ($item_tarifa['fee']/100);
											$total_sgl 	*= $fee;
											$total_dbl 	*= $fee;
											$total_tpl 	*= $fee;
											$total_cpl 	*= $fee;
											$total_cnn 	*= $fee;
											$total_inf 	*= $fee;
										}
										else
										{
											$fee 		= $item_tarifa['fee'];
											if($item_tarifa['precio_single'] > 0)
												$total_sgl 	+= $fee;
											else
												$total_sgl 	= 0;
											if($item_tarifa['precio_doble'] > 0)
												$total_dbl 	+= $fee;
											else
												$total_dbl 	= 0;
											if($item_tarifa['precio_triple'] > 0)
												$total_tpl 	+= $fee;
											else
												$total_tpl 	= 0;
											if($item_tarifa['precio_cuadruple'] > 0)
												$total_cpl 	+= $fee;
											else
												$total_cpl 	= 0;
											if($item_tarifa['precio_menor'] > 0)
												$total_cnn 	+= $fee;
											else
												$total_cnn 	= 0;
											if($item_tarifa['precio_infante'] > 0)
												$total_inf 	+= $fee;
											else
												$total_inf 	= 0;
										}

										if($item_tarifa['increment_factura'] == 'P')
										{
											$iva 		= 1 + ($item_tarifa['factura']/100);
											$total_sgl 	*= $iva;
											$total_dbl 	*= $iva;
											$total_tpl 	*= $iva;
											$total_cpl 	*= $iva;
											$total_cnn 	*= $iva;
											$total_inf 	*= $iva;
										}
										else
										{
											$iva 		= $item_tarifa['factura'];
											if($item_tarifa['precio_single'] > 0)
												$total_sgl 	+= $iva;
											else
												$total_sgl 	= 0;
											if($item_tarifa['precio_doble'] > 0)
												$total_dbl 	+= $iva;
											else
												$total_dbl 	= 0;
											if($item_tarifa['precio_triple'] > 0)
												$total_tpl 	+= $iva;
											else
												$total_tpl 	= 0;
											if($item_tarifa['precio_cuadruple'] > 0)
												$total_cpl 	+= $iva;
											else
												$total_cpl 	= 0;
											if($item_tarifa['precio_menor'] > 0)
												$total_cnn 	+= $iva;
											else
												$total_cnn 	= 0;
											if($item_tarifa['precio_infante'] > 0)
												$total_inf 	+= $iva;
											else
												$total_inf 	= 0;
										}

										if($item_tarifa['moneda'] == 'B')
										{
											$tc 		= $item_tarifa['tipo_cambio'];
											$total_sgl /= $tc;
											$total_dbl /= $tc;
											$total_tpl /= $tc;
											$total_cpl /= $tc;
											$total_cnn /= $tc;
											$total_inf /= $tc;
										}
									?>
										<tr>
											<td><?php echo $item_tarifa['nombre_hotel'] . ' - ' . $item_tarifa['categoria'] . '*'; ?></td>
											<?php if(strpos($hide_columns, 'SGL')===false) { ?><td style="text-align:right;"><?php if($total_sgl>0) echo ceil($total_sgl); else echo '-'; ?></td><?php } ?>
											<?php if(strpos($hide_columns, 'DBL')===false) { ?><td style="text-align:right;"><?php if($total_dbl>0) echo ceil($total_dbl); else echo '-'; ?></td><?php } ?>
											<?php if(strpos($hide_columns, 'TPL')===false) { ?><td style="text-align:right;"><?php if($total_tpl>0) echo ceil($total_tpl); else echo '-'; ?></td><?php } ?>
											<?php if(strpos($hide_columns, 'CPL')===false) { ?><td style="text-align:right;"><?php if($total_cpl>0) echo ceil($total_cpl); else echo '-'; ?></td><?php } ?>
											<?php if(strpos($hide_columns, 'CNN')===false) { ?><td style="text-align:right;"><?php if($total_cnn>0) echo ceil($total_cnn); else echo '-'; ?></td><?php } ?>
											<?php if(strpos($hide_columns, 'INF')===false) { ?><td style="text-align:right;"><?php if($total_inf>0) echo ceil($total_inf); else echo '-'; ?></td><?php } ?>
											<td><?php echo strtoupper($item_tarifa['alimentacion']); ?></td>
											<td><?php echo strtoupper($item_tarifa['info_extra']); ?></td>
										</tr>
									<?php
									}
							?>
									</table>
							<?
								}				
							?>
						</div>

						<div class="clear_mayor todo_precio solo_hotel" style="display:none;"></div>
					<?php
					}
				?>
				

				<!-- OTROS SERVICIOS -->
				<?php
					$otras_tarifas = $c_mostrar_evento->obtener_otros_servicios_x_ciudad($idciudad_origen);
					$serv_x_dest 	= array();
					$total_serv_adt = 0;
					$total_serv_cnn = 0;
					$total_serv_inf = 0;
					if(count($otras_tarifas) > 0)
					{
					?>
						<div class="columns fifteen todo_precio solo_servicio" style="display:none;">
							<div class="columns thirteen" style="font-weight:bold; color:#10689b; font-size:11pt; margin-left:2%;">OTROS SERVICIOS</div>
							<div class="clear"></div>
							<table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios" >
								<tr style="background-color:#10689b; color:#FFFFFF;">
									<th rowspan="2">DETALLE</th>
									<th colspan="3">PRECIOS</th>
								</tr>
								<tr style="background-color:#10689b; color:#FFFFFF;">
									<th>ADULTO</th>
									<th>MENOR</th>
									<th>INFANTE</th>
								</tr>
								<?php
									for ($i=0; $i < count($otras_tarifas); $i++) 
									{ 
										$tarifa_otros 	= $otras_tarifas[$i];
										$idotra_tarifa 	= $tarifa_otros['idtarifa_otros'];

										$ciudad_serv 	= $tarifa_otros['ciudad'];
										
										$total_adt = $tarifa_otros['precio_adulto'];
										$total_cnn = $tarifa_otros['precio_menor'];
										$total_inf = $tarifa_otros['precio_infante'];

										if($tarifa_otros['increment_fee'] == 'P')
										{
											$fee 		= 1+($tarifa_otros['fee'] / 100);
											$total_adt 	*= $fee;
											$total_cnn 	*= $fee;
											$total_inf 	*= $fee;
										}
										else
										{
											$fee = $tarifa_otros['fee'];
											if($total_adt > 0)
												$total_adt += $fee;
											else
												$total_adt = 0;
											if($total_cnn > 0)
												$total_cnn += $fee;
											else
												$total_cnn = 0;
											if($total_inf > 0)
												$total_inf += $fee;
											else
												$total_inf = 0;
										}

										if($tarifa_otros['increment_factura'] == 'P')
										{
											$iva 		= 1+($tarifa_otros['factura'] / 100);
											$total_adt 	*= $iva;
											$total_cnn 	*= $iva;
											$total_inf 	*= $iva;
										}
										else
										{
											$iva = $tarifa_otros['factura'];
											if($total_adt > 0)
												$total_adt += $iva;
											else
												$total_adt = 0;
											if($total_cnn > 0)
												$total_cnn += $iva;
											else
												$total_cnn = 0;
											if($total_inf > 0)
												$total_inf += $iva;
											else
												$total_inf = 0;
										}

										if($tarifa_otros['moneda'] == 'B')
										{
											$tc 		= $tarifa_otros['tipo_cambio'];
											$total_adt /= $tc;
											$total_cnn /= $tc;
											$total_inf /= $tc;
										}

										$total_serv_adt += ceil($total_adt);
										$total_serv_cnn += ceil($total_cnn);
										$total_serv_inf += ceil($total_inf);

										if(!array_key_exists($ciudad_serv, $serv_x_dest))
										{
											$serv_x_dest[$ciudad_serv] = array();
											$serv_x_dest[$ciudad_serv]['adt'] = 0;
											$serv_x_dest[$ciudad_serv]['cnn'] = 0;
											$serv_x_dest[$ciudad_serv]['inf'] = 0;
										}

										$serv_x_dest[$ciudad_serv]['adt'] += ceil($total_adt); 
										$serv_x_dest[$ciudad_serv]['cnn'] += ceil($total_cnn);
										$serv_x_dest[$ciudad_serv]['inf'] += ceil($total_inf);

										$nombre_servicio = $c_mostrar_evento->obtener_nombre_servicio($tarifa_otros['idotro_servicio']);
								?>
								<tr>
									<td><?php echo strtoupper($nombre_servicio . ' ' . $tarifa_otros['descripcion']); ?></td>
									<td style="text-align:right;">
										<?php if($total_adt>0) echo ceil($total_adt); else echo '-'; ?>
									</td>
									<td style="text-align:right;">
										<?php if($total_cnn>0) echo ceil($total_cnn); else echo '-'; ?>
									</td>
									<td style="text-align:right;">
										<?php if($total_inf>0) echo ceil($total_inf); else echo '-'; ?>
									</td>
								</tr>
								<?php
									}
								?>
							</table>
						</div>

						<div class="clear_mayor todo_precio solo_servicio" style="display:none;"></div>
					<?php
					}
				?>

				<!-- MUESTRA HOTEL MAS SERVICIOS -->
				<?php
					if(count($tarifas_por_destino) > 0)
					{
					?>
						<div class="columns fifteen todo_precio hotel_mas_servicio" style="display:none;">
							<div class="columns fourteen" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">PRECIOS POR PERSONA EN DOLARES AMERICANOS</div>
							<div class="columns thirteen" style="font-weight:bold; color:#10689b; font-size:11pt; margin-left:2%;">PAQUETE TERRESTRE</div>
							<div class="clear"></div>
							<?php 
								for ($i=0; $i < count($tarifas_por_destino); $i++) 
								{ 
									$tarifa_hotel 		= $tarifas_por_destino[$i];
									$idth 				= $tarifa_hotel['idtarifa_hotel'];
									$dest_hotel 		= $tarifa_hotel['destino'];
									$items_tarifa 		= $c_mostrar_evento->obtener_hoteles_x_destino($idth);

									$hide_columns 		= $c_mostrar_evento->evaluar_precios_hotel($items_tarifa);
									$cant_col 			= count(explode('/', $hide_columns));
							?>
									<table style="width:50%; margin-left:3%;">
										<tr>
											<td style="width:5%;">
												<strong>DESTINO :</strong>
											</td>
											<td style="width:20%;">
												<?php echo strtoupper($tarifa_hotel['destino']); ?>
											</td>
											<td>
											</td>
										</tr>
									</table>
									<table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios">
										<tr style="background-color:#10689b; color:#FFFFFF;">
											<th>HOTEL</th>
											<?php if(strpos($hide_columns, 'SGL')===false) { ?><th>SINGLE</th><?php } ?>
											<?php if(strpos($hide_columns, 'DBL')===false) { ?><th>DOBLE</th><?php } ?>
											<?php if(strpos($hide_columns, 'TPL')===false) { ?><th>TRIPLE</th><?php } ?>
											<?php if(strpos($hide_columns, 'CPL')===false) { ?><th>CUADRUPLE</th><?php } ?>
											<?php if(strpos($hide_columns, 'CNN')===false) { ?><th>MENOR</th><?php } ?>
											<?php if(strpos($hide_columns, 'INF')===false) { ?><th>INFANTE</th><?php } ?>
											<th>ALIMENTACION</th>
											<th>OBS</th>
										</tr>
							<?php
									for ($j=0; $j < count($items_tarifa); $j++) 
									{ 
										$item_tarifa 	= $items_tarifa[$j];
										$iditem 		= $item_tarifa['iditem_hotel'];

										$cant_noches 	= floor( (strtotime($item_tarifa['fecha_out'] . ' 00:00:00') - strtotime($item_tarifa['fecha_in'] . ' 00:00:00')) / 86400);
										
										$total_sgl 	= $item_tarifa['precio_single'] * $cant_noches;
										$total_dbl 	= $item_tarifa['precio_doble'] * $cant_noches;
										$total_tpl 	= $item_tarifa['precio_triple'] * $cant_noches;
										$total_cpl 	= $item_tarifa['precio_cuadruple'] * $cant_noches;
										$total_cnn 	= $item_tarifa['precio_menor'] * $cant_noches;
										$total_inf 	= $item_tarifa['precio_infante'] * $cant_noches;

										if($item_tarifa['increment_fee'] == 'P')
										{
											$fee 		= 1 + ($item_tarifa['fee']/100);
											$total_sgl 	*= $fee;
											$total_dbl 	*= $fee;
											$total_tpl 	*= $fee;
											$total_cpl 	*= $fee;
											$total_cnn 	*= $fee;
											$total_inf 	*= $fee;
										}
										else
										{
											$fee 		= $item_tarifa['fee'];
											if($item_tarifa['precio_single'] > 0)
												$total_sgl 	+= $fee;
											else
												$total_sgl 	= 0;
											if($item_tarifa['precio_doble'] > 0)
												$total_dbl 	+= $fee;
											else
												$total_dbl 	= 0;
											if($item_tarifa['precio_triple'] > 0)
												$total_tpl 	+= $fee;
											else
												$total_tpl 	= 0;
											if($item_tarifa['precio_cuadruple'] > 0)
												$total_cpl 	+= $fee;
											else
												$total_cpl 	= 0;
											if($item_tarifa['precio_menor'] > 0)
												$total_cnn 	+= $fee;
											else
												$total_cnn 	= 0;
											if($item_tarifa['precio_infante'] > 0)
												$total_inf 	+= $fee;
											else
												$total_inf 	= 0;
										}

										if($item_tarifa['increment_factura'] == 'P')
										{
											$iva 		= 1 + ($item_tarifa['factura']/100);
											$total_sgl 	*= $iva;
											$total_dbl 	*= $iva;
											$total_tpl 	*= $iva;
											$total_cpl 	*= $iva;
											$total_cnn 	*= $iva;
											$total_inf 	*= $iva;
										}
										else
										{
											$iva 		= $item_tarifa['factura'];
											if($item_tarifa['precio_single'] > 0)
												$total_sgl 	+= $iva;
											else
												$total_sgl 	= 0;
											if($item_tarifa['precio_doble'] > 0)
												$total_dbl 	+= $iva;
											else
												$total_dbl 	= 0;
											if($item_tarifa['precio_triple'] > 0)
												$total_tpl 	+= $iva;
											else
												$total_tpl 	= 0;
											if($item_tarifa['precio_cuadruple'] > 0)
												$total_cpl 	+= $iva;
											else
												$total_cpl 	= 0;
											if($item_tarifa['precio_menor'] > 0)
												$total_cnn 	+= $iva;
											else
												$total_cnn 	= 0;
											if($item_tarifa['precio_infante'] > 0)
												$total_inf 	+= $iva;
											else
												$total_inf 	= 0;
										}

										if($item_tarifa['moneda'] == 'B')
										{
											$tc 		= $item_tarifa['tipo_cambio'];
											$total_sgl /= $tc;
											$total_dbl /= $tc;
											$total_tpl /= $tc;
											$total_cpl /= $tc;
											$total_cnn /= $tc;
											$total_inf /= $tc;
										}

										// SUMAR PRECIOS CON LOS OTROS SERVICIOS
										if($total_sgl > 0)
											$total_sgl += $serv_x_dest[$dest_hotel]['adt'];
										if($total_dbl > 0)
											$total_dbl += $serv_x_dest[$dest_hotel]['adt'];
										if($total_tpl > 0)
											$total_tpl += $serv_x_dest[$dest_hotel]['adt'];
										if($total_cpl > 0)
											$total_cpl += $serv_x_dest[$dest_hotel]['adt'];
										if($total_cnn > 0)
											$total_cnn += $serv_x_dest[$dest_hotel]['cnn'];
										if($total_inf > 0)
											$total_inf += $serv_x_dest[$dest_hotel]['inf'];
							?>
										<tr>
											<td><?php echo $item_tarifa['nombre_hotel'] . ' - ' . $item_tarifa['categoria'] . '*'; ?></td>
											<?php if(strpos($hide_columns, 'SGL')===false) { ?><td style="text-align:right;"><?php if($total_sgl>0) echo ceil($total_sgl); else echo '-'; ?></td><?php } ?>
											<?php if(strpos($hide_columns, 'DBL')===false) { ?><td style="text-align:right;"><?php if($total_dbl>0) echo ceil($total_dbl); else echo '-'; ?></td><?php } ?>
											<?php if(strpos($hide_columns, 'TPL')===false) { ?><td style="text-align:right;"><?php if($total_tpl>0) echo ceil($total_tpl); else echo '-'; ?></td><?php } ?>
											<?php if(strpos($hide_columns, 'CPL')===false) { ?><td style="text-align:right;"><?php if($total_cpl>0) echo ceil($total_cpl); else echo '-'; ?></td><?php } ?>
											<?php if(strpos($hide_columns, 'CNN')===false) { ?><td style="text-align:right;"><?php if($total_cnn>0) echo ceil($total_cnn); else echo '-'; ?></td><?php } ?>
											<?php if(strpos($hide_columns, 'INF')===false) { ?><td style="text-align:right;"><?php if($total_inf>0) echo ceil($total_inf); else echo '-'; ?></td><?php } ?>
											<td><?php echo strtoupper($item_tarifa['alimentacion']); ?></td>
											<td><?php echo strtoupper($item_tarifa['info_extra']); ?></td>
										</tr>
							<?php
									}
							?>
									</table>
							<?
								}				
							?>
						</div>

						<div class="clear_mayor todo_precio hotel_mas_servicio" style="display:none;"></div>
					<?php
					}
				?>

				<!-- HOTELES COMBINADOS MAS SERVICIO -->
				<?php
					$opciones_de_hoteles =  $c_mostrar_evento->obtener_opciones_hotel_combinados($idciudad_origen);
					
					if(array_key_exists(0, $opciones_de_hoteles))
					{
					?>
						<div class="columns fifteen todo_precio hotel_comb_mas_servicio" style="display:none;">
							<div class="columns fourteen" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">PRECIOS POR PERSONA EN DOLARES AMERICANOS</div>
							<div class="clear"></div>
							<?php 
								$cant_col = $opciones_de_hoteles['cant_cols'];
								$mostrar_cols = $opciones_de_hoteles['col_mostrar'];
								?>
									<div class="columns thirteen" style="font-weight:bold; color:#10689b; font-size:11pt; margin-left:2%;">PAQUETE TERRESTRE</div>
									<table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios">
										<tr style="background-color:#10689b; color:#FFFFFF;">
											<th>HOTEL</th>
											<?php if(strpos($mostrar_cols, 'sgl')!==false) { ?><th>SINGLE</th><?php } ?>
											<?php if(strpos($mostrar_cols, 'dbl')!==false) { ?><th>DOBLE</th><?php } ?>
											<?php if(strpos($mostrar_cols, 'tpl')!==false) { ?><th>TRIPLE</th><?php } ?>
											<?php if(strpos($mostrar_cols, 'cpl')!==false) { ?><th>CUADRUPLE</th><?php } ?>
											<?php if(strpos($mostrar_cols, 'cnn')!==false) { ?><th>MENOR</th><?php } ?>
											<?php if(strpos($mostrar_cols, 'inf')!==false) { ?><th>INFANTE</th><?php } ?>
											<th>ALIMENTACION</th>
											<th>OBS</th>
										</tr>
								<?php
								for ($i=0; array_key_exists($i, $opciones_de_hoteles); $i++) 
								{ 
									$opcion_hotel 		= $opciones_de_hoteles[$i];
									if(array_key_exists('precio_sgl', $opcion_hotel))
										$opcion_hotel['precio_sgl'] += $total_serv_adt;
									if(array_key_exists('precio_dbl', $opcion_hotel))
										$opcion_hotel['precio_dbl'] += $total_serv_adt;
									if(array_key_exists('precio_tpl', $opcion_hotel))
										$opcion_hotel['precio_tpl'] += $total_serv_adt;
									if(array_key_exists('precio_cpl', $opcion_hotel))
										$opcion_hotel['precio_cpl'] += $total_serv_adt;
									if(array_key_exists('precio_cnn', $opcion_hotel))
										$opcion_hotel['precio_cnn'] += $total_serv_cnn;
									if(array_key_exists('precio_inf', $opcion_hotel))
										$opcion_hotel['precio_inf'] += $total_serv_inf;
								?>
										<tr>
											<td style="line-height:1.5;">
												<?php
													$hoteles = $opcion_hotel['hoteles'];
													for ($j=0; $j < count($hoteles); $j++) 
													{ 
														$item_tarifa = $hoteles[$j];
													 	echo $item_tarifa['nombre_hotel'] . ' - ' . $item_tarifa['categoria'] . '* (' . $item_tarifa['destino'] . ')<br/>'; 
													}
												?>
											</td>
											<?php if(strpos($mostrar_cols, 'sgl')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_sgl']; ?></td><?php } ?>
											<?php if(strpos($mostrar_cols, 'dbl')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_dbl']; ?></td><?php } ?>
											<?php if(strpos($mostrar_cols, 'tpl')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_tpl']; ?></td><?php } ?>
											<?php if(strpos($mostrar_cols, 'cpl')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_cpl']; ?></td><?php } ?>
											<?php if(strpos($mostrar_cols, 'cnn')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_cnn']; ?></td><?php } ?>
											<?php if(strpos($mostrar_cols, 'inf')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_inf']; ?></td><?php } ?>
											<td style="line-height:1.5;">
												<?php
													$hoteles = $opcion_hotel['hoteles'];
													for ($j=0; $j < count($hoteles); $j++) 
													{ 
														$item_tarifa = $hoteles[$j];
													 	echo strtoupper($item_tarifa['alimentacion']) . '<br/>';
													}
												?>
											</td>
											<td style="line-height:1.5;">
												<?php
													$hoteles = $opcion_hotel['hoteles'];
													for ($j=0; $j < count($hoteles); $j++) 
													{ 
														$item_tarifa = $hoteles[$j];
													 	echo strtoupper($item_tarifa['info_extra']) . '<br/>';
													}
												?>
											</td>
										</tr>									
								<?php
								}				
								?>
									</table>
						</div>

						<div class="clear_mayor todo_precio hotel_comb_mas_servicio"></div>
					<?php
					}
				?>

				<!-- BOLETOS AEREOS -->
				<?php
					$opciones_aereo = $c_mostrar_evento->obtener_opcion_boletos_x_ciudad($idciudad_origen);
					if(count($opciones_aereo) > 0)
					{
					?>
						<div class="columns fifteen todo_precio solo_aereo" style="display:none;">
							<div class="columns thirteen" style="font-weight:bold; color:#10689b; font-size:11pt; margin-left:2%;">BOLETOS AEREOS</div>
							<div class="clear"></div>
							<?php
								for ($i=0; $i < count($opciones_aereo); $i++) 
								{ 
									$opcion_aereo 	= $opciones_aereo[$i];
									$idopaereo 		= $opcion_aereo['idopcion_boleto'];
								?>
									<div class="opc<?php echo $z . '_' . $i; ?> opc_aereo<?php echo $z; ?>" >
										<table style="width:70%; margin-left:3%;">
											<tr>
												<td style="width:5%;">
													<strong>OPCION :</strong>
												</td>
												<td style="width:30%;">
													<?php echo strtoupper($opcion_aereo['descripcion']); ?>
												</td>
												<td style="width:5%;">
													<strong>SALE :</strong>
												</td>
												<td style="width:30%;">
													<?php echo strtoupper($opcion_aereo['salida']); ?>
												</td>
												<td></td>
											</tr>
										</table>
										<table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios" >
											<tr style="background-color:#10689b; color:#FFFFFF;">
												<th rowspan="2">LINEA AEREA</th>
												<th rowspan="2">RUTA</th>
												<th colspan="3">PRECIOS</th>
											</tr>
											<tr style="background-color:#10689b; color:#FFFFFF;">
												<th>ADULTO</th>
												<th>MENOR</th>
												<th>INFANTE</th>
											</tr>
											<?php
												$boletos = $c_mostrar_evento->obtener_boletos_x_opcion_aereo($idopaereo);
												for ($j=0; $j < count($boletos); $j++) 
												{ 
													$boleto_cotizado 	= $boletos[$j];
													$idboleto 			= $boleto_cotizado['idboleto_cotizado'];
													
													$total_adt = $boleto_cotizado['precio_adulto'];
													$total_cnn = $boleto_cotizado['precio_menor'];
													$total_inf = $boleto_cotizado['precio_infante'];

													if($boleto_cotizado['increment_fee'] == 'P')
													{
														$fee 		= 1+($boleto_cotizado['fee'] / 100);
														$total_adt 	*= $fee;
														$total_cnn 	*= $fee;
														$total_inf 	*= $fee;
													}
													else
													{
														$fee = $boleto_cotizado['fee'];
														if($total_adt > 0)
															$total_adt += $fee;
														else
															$total_adt = 0;
														if($total_cnn > 0)
															$total_cnn += $fee;
														else
															$total_cnn = 0;
														if($total_inf > 0)
															$total_inf += $fee;
														else
															$total_inf = 0;
													}

													if($boleto_cotizado['increment_factura'] == 'P')
													{
														$iva 		= 1+($boleto_cotizado['factura'] / 100);
														$total_adt 	*= $iva;
														$total_cnn 	*= $iva;
														$total_inf 	*= $iva;
													}
													else
													{
														$iva = $boleto_cotizado['factura'];
														if($total_adt > 0)
															$total_adt += $iva;
														else
															$total_adt = 0;
														if($total_cnn > 0)
															$total_cnn += $iva;
														else
															$total_cnn = 0;
														if($total_inf > 0)
															$total_inf += $iva;
														else
															$total_inf = 0;
													}

													if($boleto_cotizado['moneda'] == 'B')
													{
														$tc 		= $boleto_cotizado['tipo_cambio'];
														$total_adt /= $tc;
														$total_cnn /= $tc;
														$total_inf /= $tc;
													}

													$total_bol_adt += $total_adt;
													$total_bol_cnn += $total_cnn;
													$total_bol_inf += $total_inf;
											?>
													<tr>
														<td><?php echo strtoupper($c_mostrar_evento->obtener_linea_aerea($boleto_cotizado['idlineas_aereas'])); ?></td>
														<td><?php echo strtoupper($boleto_cotizado['ruta']); ?></td>
														<td style="text-align:right;"><?php if($total_adt>0) echo ceil($total_adt); else echo '-'; ?></td>
														<td style="text-align:right;"><?php if($total_cnn>0) echo ceil($total_cnn); else echo '-'; ?></td>
														<td style="text-align:right;"><?php if($total_inf>0) echo ceil($total_inf); else echo '-'; ?></td>
													</tr>
											<?php
												}
											?>
										</table>
										<br/>
										<div class="columns five" style="font-weight:bold; font-size:10pt; margin-left:3%;">ITINERARIO AEREO DETALLADO</div>
										<table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios" >
											<tr style="background-color:#10689b; color:#FFFFFF;">
												<th>LINEA AEREA</th>
												<th>VUELO</th>
												<th>FECHA</th>
												<th>SALE</th>
												<th>LLEGA</th>
											</tr>
											<?php
												$itinerario = $c_mostrar_evento->obtener_itinerario_x_opcion_aereo($idopaereo);
												for ($k=0; $k < count($itinerario); $k++) 
												{ 
													$segmento 	= $itinerario[$k];
													$idsegmento = $segmento['idsegmento'];

													$linea_aerea 		= $c_mostrar_evento->obtener_linea_aerea($segmento['idlineas_aereas']);
													$aeropuerto_sale 	= $c_mostrar_evento->obtener_aeropuerto($segmento['origen']);
													$aeropuerto_llega 	= $c_mostrar_evento->obtener_aeropuerto($segmento['destino']);
											?>
													<tr>
														<td><?php echo strtoupper($linea_aerea); ?></td>
														<td><?php echo $segmento['nro_vuelo']; ?></td>
														<td><?php echo date('d/m/Y', strtotime($segmento['fecha'])); ?></td>
														<td><?php echo strtoupper($aeropuerto_sale) . ' <font style="font-size:7pt;">A HRS</font> ' . $segmento['hora_sale']; ?></td>
														<td><?php echo strtoupper($aeropuerto_llega) . ' <font style="font-size:7pt;">A HRS</font> ' . $segmento['hora_llega']; ?></td>
													</tr>
											<?php
												}
											?>
										</table>
									</div>
								<?php
								}
							?>
						</div>

						<div class="clear_mayor todo_precio solo_aereo" style="display:none;"></div>
					<?php
					}
				?>

				<!-- MUESTRA HOTEL MAS SERVICIOS MAS AEREO -->
				<?php
					//$opciones_aereo = $c_mostrar_evento->obtener_opcion_boletos_x_ciudad($idciudad_origen);
					$minimo 		= 999999;
					$minimoactual 	= 0;
					if(count($opciones_aereo) > 0)
					{
					?>
						<div class="columns fifteen todo_precio hotel_mas_servicio_mas_aereo">
							<div class="columns fourteen" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">PRECIOS POR PERSONA EN DOLARES AMERICANOS</div>
							<div class="clear"></div>
							<?php
								for ($g=0; $g < count($opciones_aereo); $g++) 
								{ 
									$opcion_aereo 	= $opciones_aereo[$g];
									$idopaereo 		= $opcion_aereo['idopcion_boleto'];
								?>
									<div class="opc<?php echo $z . '_' . $g; ?> opc_aereo<?php echo $z; ?>" >
										<table style="width:70%; margin-left:3%;">
											<tr>
												<td style="width:5%;">
													<strong>OPCION :</strong>
												</td>
												<td style="width:30%;">
													<?php echo strtoupper($opcion_aereo['descripcion']); ?>
												</td>
												<td style="width:5%;">
													<strong>SALE :</strong>
												</td>
												<td style="width:30%;">
													<?php echo strtoupper($opcion_aereo['salida']); ?>
												</td>
												<td></td>
											</tr>
										</table>
										<?php
											$boletos = $c_mostrar_evento->obtener_boletos_x_opcion_aereo($idopaereo);
											$total_bol_adt = 0;
											$total_bol_cnn = 0;
											$total_bol_inf = 0;
											for ($j=0; $j < count($boletos); $j++) 
											{ 
												$boleto_cotizado 	= $boletos[$j];
												$idboleto 			= $boleto_cotizado['idboleto_cotizado'];
												
												$total_adt = $boleto_cotizado['precio_adulto'];
												$total_cnn = $boleto_cotizado['precio_menor'];
												$total_inf = $boleto_cotizado['precio_infante'];

												if($boleto_cotizado['increment_fee'] == 'P')
												{
													$fee 		= 1+($boleto_cotizado['fee'] / 100);
													$total_adt 	*= $fee;
													$total_cnn 	*= $fee;
													$total_inf 	*= $fee;
												}
												else
												{
													$fee = $boleto_cotizado['fee'];
													if($total_adt > 0)
														$total_adt += $fee;
													else
														$total_adt = 0;
													if($total_cnn > 0)
														$total_cnn += $fee;
													else
														$total_cnn = 0;
													if($total_inf > 0)
														$total_inf += $fee;
													else
														$total_inf = 0;
												}

												if($boleto_cotizado['increment_factura'] == 'P')
												{
													$iva 		= 1+($boleto_cotizado['factura'] / 100);
													$total_adt 	*= $iva;
													$total_cnn 	*= $iva;
													$total_inf 	*= $iva;
												}
												else
												{
													$iva = $boleto_cotizado['factura'];
													if($total_adt > 0)
														$total_adt += $iva;
													else
														$total_adt = 0;
													if($total_cnn > 0)
														$total_cnn += $iva;
													else
														$total_cnn = 0;
													if($total_inf > 0)
														$total_inf += $iva;
													else
														$total_inf = 0;
												}

												if($boleto_cotizado['moneda'] == 'B')
												{
													$tc 		= $boleto_cotizado['tipo_cambio'];
													$total_adt /= $tc;
													$total_cnn /= $tc;
													$total_inf /= $tc;
												}

												$total_bol_adt += ceil($total_adt);
												$total_bol_cnn += ceil($total_cnn);
												$total_bol_inf += ceil($total_inf);
											}
											if($total_bol_adt < $minimo)
											{
												$minimo 		= $total_bol_adt;
												$minimoactual 	= $g;
											}
										?>
										<?php
											if(count($tarifas_por_destino) > 0)
											{
											?>
												<div class="clear"></div>
												<?php 
													for ($i=0; $i < count($tarifas_por_destino); $i++) 
													{ 
														$tarifa_hotel 		= $tarifas_por_destino[$i];
														$idth 				= $tarifa_hotel['idtarifa_hotel'];
														$items_tarifa 		= $c_mostrar_evento->obtener_hoteles_x_destino($idth);

														$hide_columns 		= $c_mostrar_evento->evaluar_precios_hotel($items_tarifa);
														$cant_col 			= count(explode('/', $hide_columns));
												?>
														<strong style="margin-left:3%;">HOTELES EN <?php echo strtoupper($c_mostrar_evento->obtener_aeropuerto($tarifa_hotel['destino'])); ?></strong>
														<table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios">
															<tr style="background-color:#10689b; color:#FFFFFF;">
																<th>HOTEL</th>
																<?php if(strpos($hide_columns, 'SGL')===false) { ?><th>SINGLE</th><?php } ?>
																<?php if(strpos($hide_columns, 'DBL')===false) { ?><th>DOBLE</th><?php } ?>
																<?php if(strpos($hide_columns, 'TPL')===false) { ?><th>TRIPLE</th><?php } ?>
																<?php if(strpos($hide_columns, 'CPL')===false) { ?><th>CUADRUPLE</th><?php } ?>
																<?php if(strpos($hide_columns, 'CNN')===false) { ?><th>MENOR</th><?php } ?>
																<?php if(strpos($hide_columns, 'INF')===false) { ?><th>INFANTE</th><?php } ?>
																<th>ALIMENTACION</th>
																<th>OBS</th>
															</tr>
												<?php
														for ($j=0; $j < count($items_tarifa); $j++) 
														{ 
															$item_tarifa 	= $items_tarifa[$j];
															$iditem 		= $item_tarifa['iditem_hotel'];

															$cant_noches 	= floor( (strtotime($item_tarifa['fecha_out'] . ' 00:00:00') - strtotime($item_tarifa['fecha_in'] . ' 00:00:00')) / 86400);
															
															$total_sgl 	= $item_tarifa['precio_single'] * $cant_noches;
															$total_dbl 	= $item_tarifa['precio_doble'] * $cant_noches;
															$total_tpl 	= $item_tarifa['precio_triple'] * $cant_noches;
															$total_cpl 	= $item_tarifa['precio_cuadruple'] * $cant_noches;
															$total_cnn 	= $item_tarifa['precio_menor'] * $cant_noches;
															$total_inf 	= $item_tarifa['precio_infante'] * $cant_noches;

															if($item_tarifa['increment_fee'] == 'P')
															{
																$fee 		= 1 + ($item_tarifa['fee']/100);
																$total_sgl 	*= $fee;
																$total_dbl 	*= $fee;
																$total_tpl 	*= $fee;
																$total_cpl 	*= $fee;
																$total_cnn 	*= $fee;
																$total_inf 	*= $fee;
															}
															else
															{
																$fee 		= $item_tarifa['fee'];
																if($item_tarifa['precio_single'] > 0)
																	$total_sgl 	+= $fee;
																else
																	$total_sgl 	= 0;
																if($item_tarifa['precio_doble'] > 0)
																	$total_dbl 	+= $fee;
																else
																	$total_dbl 	= 0;
																if($item_tarifa['precio_triple'] > 0)
																	$total_tpl 	+= $fee;
																else
																	$total_tpl 	= 0;
																if($item_tarifa['precio_cuadruple'] > 0)
																	$total_cpl 	+= $fee;
																else
																	$total_cpl 	= 0;
																if($item_tarifa['precio_menor'] > 0)
																	$total_cnn 	+= $fee;
																else
																	$total_cnn 	= 0;
																if($item_tarifa['precio_infante'] > 0)
																	$total_inf 	+= $fee;
																else
																	$total_inf 	= 0;
															}

															if($item_tarifa['increment_factura'] == 'P')
															{
																$iva 		= 1 + ($item_tarifa['factura']/100);
																$total_sgl 	*= $iva;
																$total_dbl 	*= $iva;
																$total_tpl 	*= $iva;
																$total_cpl 	*= $iva;
																$total_cnn 	*= $iva;
																$total_inf 	*= $iva;
															}
															else
															{
																$iva 		= $item_tarifa['factura'];
																if($item_tarifa['precio_single'] > 0)
																	$total_sgl 	+= $iva;
																else
																	$total_sgl 	= 0;
																if($item_tarifa['precio_doble'] > 0)
																	$total_dbl 	+= $iva;
																else
																	$total_dbl 	= 0;
																if($item_tarifa['precio_triple'] > 0)
																	$total_tpl 	+= $iva;
																else
																	$total_tpl 	= 0;
																if($item_tarifa['precio_cuadruple'] > 0)
																	$total_cpl 	+= $iva;
																else
																	$total_cpl 	= 0;
																if($item_tarifa['precio_menor'] > 0)
																	$total_cnn 	+= $iva;
																else
																	$total_cnn 	= 0;
																if($item_tarifa['precio_infante'] > 0)
																	$total_inf 	+= $iva;
																else
																	$total_inf 	= 0;
															}

															if($item_tarifa['moneda'] == 'B')
															{
																$tc 		= $item_tarifa['tipo_cambio'];
																$total_sgl /= $tc;
																$total_dbl /= $tc;
																$total_tpl /= $tc;
																$total_cpl /= $tc;
																$total_cnn /= $tc;
																$total_inf /= $tc;
															}

															// SUMAR PRECIOS CON LOS OTROS SERVICIOS
															if($total_sgl > 0)
																$total_sgl += $total_serv_adt + $total_bol_adt;
															if($total_dbl > 0)
																$total_dbl += $total_serv_adt + $total_bol_adt;
															if($total_tpl > 0)
																$total_tpl += $total_serv_adt + $total_bol_adt;
															if($total_cpl > 0)
																$total_cpl += $total_serv_adt + $total_bol_adt;
															if($total_cnn > 0)
																$total_cnn += $total_serv_cnn + $total_bol_cnn;
															if($total_inf > 0)
																$total_inf += $total_serv_inf + $total_bol_inf;
												?>
															<tr>
																<td><?php echo $item_tarifa['nombre_hotel'] . ' - ' . $item_tarifa['categoria'] . '*'; ?></td>
																<?php if(strpos($hide_columns, 'SGL')===false) { ?><td style="text-align:right;"><?php if($total_sgl>0) echo ceil($total_sgl); else echo '-'; ?></td><?php } ?>
																<?php if(strpos($hide_columns, 'DBL')===false) { ?><td style="text-align:right;"><?php if($total_dbl>0) echo ceil($total_dbl); else echo '-'; ?></td><?php } ?>
																<?php if(strpos($hide_columns, 'TPL')===false) { ?><td style="text-align:right;"><?php if($total_tpl>0) echo ceil($total_tpl); else echo '-'; ?></td><?php } ?>
																<?php if(strpos($hide_columns, 'CPL')===false) { ?><td style="text-align:right;"><?php if($total_cpl>0) echo ceil($total_cpl); else echo '-'; ?></td><?php } ?>
																<?php if(strpos($hide_columns, 'CNN')===false) { ?><td style="text-align:right;"><?php if($total_cnn>0) echo ceil($total_cnn); else echo '-'; ?></td><?php } ?>
																<?php if(strpos($hide_columns, 'INF')===false) { ?><td style="text-align:right;"><?php if($total_inf>0) echo ceil($total_inf); else echo '-'; ?></td><?php } ?>
																<td><?php echo strtoupper($item_tarifa['alimentacion']); ?></td>
																<td><?php echo strtoupper($item_tarifa['info_extra']); ?></td>
															</tr>
												<?php
														}
												?>
														</table>
												<?
													}				
												?>
												<div class="clear_mayor"></div>
											<?php
											}
										?>
										<div class="columns five" style="font-weight:bold; font-size:10pt; margin-left:3%;">ITINERARIO AEREO DETALLADO</div>
										<table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios" >
											<tr style="background-color:#10689b; color:#FFFFFF;">
												<th>LINEA AEREA</th>
												<th>VUELO</th>
												<th>FECHA</th>
												<th>SALE</th>
												<th>LLEGA</th>
											</tr>
											<?php
												$itinerario = $c_mostrar_evento->obtener_itinerario_x_opcion_aereo($idopaereo);
												for ($k=0; $k < count($itinerario); $k++) 
												{ 
													$segmento 	= $itinerario[$k];
													$idsegmento = $segmento['idsegmento'];

													$linea_aerea 		= $c_mostrar_evento->obtener_linea_aerea($segmento['idlineas_aereas']);
													$aeropuerto_sale 	= $c_mostrar_evento->obtener_aeropuerto($segmento['origen']);
													$aeropuerto_llega 	= $c_mostrar_evento->obtener_aeropuerto($segmento['destino']);
											?>
													<tr>
														<td><?php echo strtoupper($linea_aerea); ?></td>
														<td><?php echo $segmento['nro_vuelo']; ?></td>
														<td><?php echo date('d/m/Y', strtotime($segmento['fecha'])); ?></td>
														<td><?php echo strtoupper($aeropuerto_sale) . ' <font style="font-size:7pt;">A HRS</font> ' . $segmento['hora_sale']; ?></td>
														<td><?php echo strtoupper($aeropuerto_llega) . ' <font style="font-size:7pt;">A HRS</font> ' . $segmento['hora_llega']; ?></td>
													</tr>
											<?php
												}
											?>
										</table>
									</div>
								<?php
								}
							?>
						</div>

						<div class="clear_mayor todo_precio hotel_mas_servicio_mas_aereo"></div>

						<div id="opc_aereo_show<?php echo $z; ?>" style="display:none;">
							<?php
								for ($g=0; $g < count($opciones_aereo); $g++) 
								{ 
									$opcion_aereo 	= $opciones_aereo[$g];
									$idopaereo 		= $opcion_aereo['idopcion_boleto'];
								?>
									<input type="checkbox" onchange="$('.opc<?php echo $z . '_' . $g; ?>').toggle();" <?php if($minimoactual == $g) echo 'checked'; ?> />
									Mostrar Opcion <?php echo $opcion_aereo['descripcion']; ?>
									<br/>
								<?php
								}
							?>
						</div>
						<script type="text/javascript">
							$(function()
								{
									$('.opc_aereo<?php echo $z; ?>').hide();
									$('.opc<?php echo $z; ?>_<?php echo $minimoactual; ?>').show();
									<?php
										if($z == 0)
										{
									?>
											$('.vista_opciones_aereo').html($('#opc_aereo_show0').html());
									<?php
										}
									?>
								});
						</script>
					<?php
					}
				?>

				<!-- MUESTRA HOTEL COMBINADO MAS SERVICIOS MAS AEREO -->
				<?php
					//$opciones_aereo = $c_mostrar_evento->obtener_opcion_boletos_x_ciudad($idciudad_origen);
					if(count($opciones_aereo) > 0)
					{
					?>
						<div class="columns fifteen todo_precio hotel_comb_mas_servicio_mas_aereo" style="display:none;" >
							<div class="columns fourteen" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">PRECIOS POR PERSONA EN DOLARES AMERICANOS</div>
							<div class="clear"></div>
							<?php
								for ($g=0; $g < count($opciones_aereo); $g++) 
								{ 
									$opcion_aereo 	= $opciones_aereo[$g];
									$idopaereo 		= $opcion_aereo['idopcion_boleto'];
								?>
									<div class="opc<?php echo $z . '_' . $g; ?> opc_aereo<?php echo $z; ?>" >
										<table style="width:70%; margin-left:3%;">
											<tr>
												<td style="width:5%;">
													<strong>OPCION :</strong>
												</td>
												<td style="width:30%;">
													<?php echo strtoupper($opcion_aereo['descripcion']); ?>
												</td>
												<td style="width:5%;">
													<strong>SALE :</strong>
												</td>
												<td style="width:30%;">
													<?php echo strtoupper($opcion_aereo['salida']); ?>
												</td>
												<td></td>
											</tr>
										</table>
										<?php
											$boletos = $c_mostrar_evento->obtener_boletos_x_opcion_aereo($idopaereo);
											$total_bol_adt = 0;
											$total_bol_cnn = 0;
											$total_bol_inf = 0;
											for ($j=0; $j < count($boletos); $j++) 
											{ 
												$boleto_cotizado 	= $boletos[$j];
												$idboleto 			= $boleto_cotizado['idboleto_cotizado'];
												
												$total_adt = $boleto_cotizado['precio_adulto'];
												$total_cnn = $boleto_cotizado['precio_menor'];
												$total_inf = $boleto_cotizado['precio_infante'];

												if($boleto_cotizado['increment_fee'] == 'P')
												{
													$fee 		= 1+($boleto_cotizado['fee'] / 100);
													$total_adt 	*= $fee;
													$total_cnn 	*= $fee;
													$total_inf 	*= $fee;
												}
												else
												{
													$fee = $boleto_cotizado['fee'];
													if($total_adt > 0)
														$total_adt += $fee;
													else
														$total_adt = 0;
													if($total_cnn > 0)
														$total_cnn += $fee;
													else
														$total_cnn = 0;
													if($total_inf > 0)
														$total_inf += $fee;
													else
														$total_inf = 0;
												}

												if($boleto_cotizado['increment_factura'] == 'P')
												{
													$iva 		= 1+($boleto_cotizado['factura'] / 100);
													$total_adt 	*= $iva;
													$total_cnn 	*= $iva;
													$total_inf 	*= $iva;
												}
												else
												{
													$iva = $boleto_cotizado['factura'];
													if($total_adt > 0)
														$total_adt += $iva;
													else
														$total_adt = 0;
													if($total_cnn > 0)
														$total_cnn += $iva;
													else
														$total_cnn = 0;
													if($total_inf > 0)
														$total_inf += $iva;
													else
														$total_inf = 0;
												}

												if($boleto_cotizado['moneda'] == 'B')
												{
													$tc 		= $boleto_cotizado['tipo_cambio'];
													$total_adt /= $tc;
													$total_cnn /= $tc;
													$total_inf /= $tc;
												}

												$total_bol_adt += ceil($total_adt);
												$total_bol_cnn += ceil($total_cnn);
												$total_bol_inf += ceil($total_inf);
											}
										?>
										<?php
											if(array_key_exists(0, $opciones_de_hoteles))
											{
											?>
												<div class="clear"></div>
												<?php 
													$cant_col = $opciones_de_hoteles['cant_cols'];
													$mostrar_cols = $opciones_de_hoteles['col_mostrar'];
													?>
														<table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios">
															<tr style="background-color:#10689b; color:#FFFFFF;">
																<th>HOTEL</th>
																<?php if(strpos($mostrar_cols, 'sgl')!==false) { ?><th>SINGLE</th><?php } ?>
																<?php if(strpos($mostrar_cols, 'dbl')!==false) { ?><th>DOBLE</th><?php } ?>
																<?php if(strpos($mostrar_cols, 'tpl')!==false) { ?><th>TRIPLE</th><?php } ?>
																<?php if(strpos($mostrar_cols, 'cpl')!==false) { ?><th>CUADRUPLE</th><?php } ?>
																<?php if(strpos($mostrar_cols, 'cnn')!==false) { ?><th>MENOR</th><?php } ?>
																<?php if(strpos($mostrar_cols, 'inf')!==false) { ?><th>INFANTE</th><?php } ?>
																<th>ALIMENTACION</th>
																<th>OBS</th>
															</tr>
													<?php
													for ($i=0; array_key_exists($i, $opciones_de_hoteles); $i++) 
													{ 
														$opcion_hotel 		= $opciones_de_hoteles[$i];
														if(array_key_exists('precio_sgl', $opcion_hotel))
															$opcion_hotel['precio_sgl'] += $total_serv_adt + $total_bol_adt;
														if(array_key_exists('precio_dbl', $opcion_hotel))
															$opcion_hotel['precio_dbl'] += $total_serv_adt + $total_bol_adt;
														if(array_key_exists('precio_tpl', $opcion_hotel))
															$opcion_hotel['precio_tpl'] += $total_serv_adt + $total_bol_adt;
														if(array_key_exists('precio_cpl', $opcion_hotel))
															$opcion_hotel['precio_cpl'] += $total_serv_adt + $total_bol_adt;
														if(array_key_exists('precio_cnn', $opcion_hotel))
															$opcion_hotel['precio_cnn'] += $total_serv_cnn + $total_bol_cnn;
														if(array_key_exists('precio_inf', $opcion_hotel))
															$opcion_hotel['precio_inf'] += $total_serv_inf + $total_bol_inf;
													?>
															<tr>
																<td style="line-height:1.5;">
																	<?php
																		$hoteles = $opcion_hotel['hoteles'];
																		for ($j=0; $j < count($hoteles); $j++) 
																		{ 
																			$item_tarifa = $hoteles[$j];
																		 	echo $item_tarifa['nombre_hotel'] . ' - ' . $item_tarifa['categoria'] . '* (' . $item_tarifa['destino'] . ')<br/>'; 
																		}
																	?>
																</td>
																<?php if(strpos($mostrar_cols, 'sgl')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_sgl']; ?></td><?php } ?>
																<?php if(strpos($mostrar_cols, 'dbl')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_dbl']; ?></td><?php } ?>
																<?php if(strpos($mostrar_cols, 'tpl')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_tpl']; ?></td><?php } ?>
																<?php if(strpos($mostrar_cols, 'cpl')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_cpl']; ?></td><?php } ?>
																<?php if(strpos($mostrar_cols, 'cnn')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_cnn']; ?></td><?php } ?>
																<?php if(strpos($mostrar_cols, 'inf')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_inf']; ?></td><?php } ?>
																<td style="line-height:1.5;">
																	<?php
																		$hoteles = $opcion_hotel['hoteles'];
																		for ($j=0; $j < count($hoteles); $j++) 
																		{ 
																			$item_tarifa = $hoteles[$j];
																		 	echo strtoupper($item_tarifa['alimentacion']) . '<br/>';
																		}
																	?>
																</td>
																<td style="line-height:1.5;">
																	<?php
																		$hoteles = $opcion_hotel['hoteles'];
																		for ($j=0; $j < count($hoteles); $j++) 
																		{ 
																			$item_tarifa = $hoteles[$j];
																		 	echo strtoupper($item_tarifa['info_extra']) . '<br/>';
																		}
																	?>
																</td>
															</tr>									
													<?php
													}				
													?>
														</table>
											<?php
											}
										?>
										<br/>
										<div class="columns five" style="font-weight:bold; font-size:10pt; margin-left:3%;">ITINERARIO AEREO DETALLADO</div>
										<table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios" >
											<tr style="background-color:#10689b; color:#FFFFFF;">
												<th>LINEA AEREA</th>
												<th>VUELO</th>
												<th>FECHA</th>
												<th>SALE</th>
												<th>LLEGA</th>
											</tr>
											<?php
												$itinerario = $c_mostrar_evento->obtener_itinerario_x_opcion_aereo($idopaereo);
												for ($k=0; $k < count($itinerario); $k++) 
												{ 
													$segmento 	= $itinerario[$k];
													$idsegmento = $segmento['idsegmento'];

													$linea_aerea 		= $c_mostrar_evento->obtener_linea_aerea($segmento['idlineas_aereas']);
													$aeropuerto_sale 	= $c_mostrar_evento->obtener_aeropuerto($segmento['origen']);
													$aeropuerto_llega 	= $c_mostrar_evento->obtener_aeropuerto($segmento['destino']);
											?>
													<tr>
														<td><?php echo strtoupper($linea_aerea); ?></td>
														<td><?php echo $segmento['nro_vuelo']; ?></td>
														<td><?php echo date('d/m/Y', strtotime($segmento['fecha'])); ?></td>
														<td><?php echo strtoupper($aeropuerto_sale) . ' <font style="font-size:7pt;">A HRS</font> ' . $segmento['hora_sale']; ?></td>
														<td><?php echo strtoupper($aeropuerto_llega) . ' <font style="font-size:7pt;">A HRS</font> ' . $segmento['hora_llega']; ?></td>
													</tr>
											<?php
												}
											?>
										</table>
									</div>
								<?php
								}
							?>
						</div>

						<div class="clear_mayor todo_precio hotel_comb_mas_servicio_mas_aereo" style="display:none;"></div>

					<?php
					}
				?>

				<!-- IMPORTANTE -->
				<?php
					if($ciudad['importante'] != '')
					{
					?>
						<div class="columns fifteen">
							<div class="columns five" style="font-weight:bold; color:#FF3300; font-size:12pt; margin:10px;">IMPORTANTE</div>
							<div class="clear"></div>
							<ul class="lista_descripcion">
								<?php
									$importante = explode("\n", $ciudad['importante']);
									for($i=0; $i<count($importante); $i++)
									{
										if($importante[$i] != "")
										{
								?>
											<li>
												<a><?php echo $importante[$i]; ?></a>
											</li>
								<?php
										}
									}
								?>		
							</ul>
						</div>
					<?php
					}
				?>
				

				<!-- DATOS AGENTE POR CIUDAD -->
				<?php
					if($ciudad['datos_agente'] != '')
					{
					?>
						<div class="columns fifteen">
							<div class="columns five" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">DATOS DE AGENTE</div>
							<div class="clear"></div>
							<ul class="lista_descripcion" id="lista_datos_agente_<?php echo $idciudad_origen; ?>">
								<?php
									$datos_agente = explode("\n", $ciudad['datos_agente']);
									for($i=0; $i<count($datos_agente); $i++)
									{
										if($datos_agente[$i] != "")
										{
								?>
											<li>
												<a><?php echo $datos_agente[$i]; ?></a>
											</li>
								<?php
										}
									}
								?>		
							</ul>
						</div>
					<?php
					}
				?>

			</div>
			<?php
			}
	}
?>

<div class="clear_mayor"></div>

<div class="columns sixteen" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
	<div class="columns five" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">INSCRIPCIONES</div>
	<div class="clear"></div>
	<?php
		if($paquete['imagen_primera'] != '')
		{
		?>
			<img src="<?php echo $ruta_imagen . $paquete['imagen_primera']; ?>" style="width:80%; margin-left:10%;" />
		<?php
		}
		else
		{
		?>
			<ul class="lista_descripcion"><li><a>NO INGRESADO</a></li></ul>
		<?php
		}
	?>
	<div class="clear_mayor"></div>
</div>

<div class="clear_mayor"></div>

<div class="columns sixteen" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
	<div class="columns five" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">MAPA DE HOTELES</div>
	<div class="columns one" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">:</div>
	<div class="columns five" style="font-size:12pt; margin:10px;">
		<?php
			if($paquete['imagen_segunda'] != '')
			{
			?>
				<a href="<?php echo $ruta_imagen . $paquete['imagen_segunda']; ?>" target="_blank" >Ver Mapa</a>
			<?php
			}
			else
			{
			?>
				<a>NO INGRESADO</a>
			<?php
			}
		?>
	</div>
	<div class="clear_mayor"></div>
</div>

<div class="clear_mayor"></div>

<div class="columns sixteen" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
	<div class="columns five" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">ARCHIVOS ADJUNTOS</div>
		<div class="clear"></div>
		<ul class="lista_descripcion" id="lista_datos_agente">
	<?php 
		$archivos = publicacion::obtener_archivos_por_publicacion($idpublicacion);
		if(count($archivos) > 0) 
		{
			for($i=0; $i<count($archivos); $i++)
			{
				$archivo = $archivos[$i];
		?>
			<li>
				<a>
					<strong><?php echo $archivo['nombre']; ?> - </strong>
					<?php echo $archivo["descripcion"]; ?>
					<a href="<?php echo $ruta_adjuntos . $archivo['nombre']; ?>" target="_blank"><input type="button" value="Abrir Archivo"></a>
				</a>
	        </li>
		<?php
			}
		}
		else
		{
	?>
			<li><a>No se a&ntilde;adio ning&uacute;n archivo.</a></li>
	<?php
		}
	?>	
		</ul>
</div>

<div class="clear_mayor"></div>

<?php
	if($paquete['datos_agente'] != "")
	{
	?>
	<div class="columns sixteen" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
		<div class="columns five" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">DATOS DE AGENTE</div>
		<div class="clear"></div>
		<ul class="lista_descripcion" id="lista_datos_agente">
			<?php
				$datos_agente = explode("\n", $paquete['datos_agente']);
				for($i=0; $i<count($datos_agente); $i++)
				{
					if($datos_agente[$i] != "")
					{
			?>
						<li>
							<a><?php echo $datos_agente[$i]; ?></a>
						</li>
			<?php
					}
				}
			?>		
		</ul>
	</div>
	<?php
	}
?>

<div class="clear_mayor"></div>

<div class="columns sixteen" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
	<div class="columns five" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">OPERADORA</div>
	<div class="clear"></div>
	<?php
	$operadoras = publicacion::obtener_operadoras_por_publicacion($idpublicacion);

	if(count($operadoras) > 0)
	{
		for($i = 0; $i < count($operadoras); $i++)
		{
			$operadora = $operadoras[$i];
		?>
			<div class="seven columns">
				&nbsp;<strong><?php echo strtoupper($operadora["nombre_operadora"]); ?></strong>
				<ul class="lista_descripcion">
					<li><a>&nbsp;<?php echo strtoupper($operadora["correos"]); ?></a></li>
					<li><a>&nbsp;<?php echo strtoupper($operadora["telefonos"]); ?></a></li>
					<li><a>&nbsp;<?php echo strtoupper($operadora["observaciones"]); ?></a></li>
				</ul>
			</div>
		<?php
		}
	}
	else
	{
	?>
		<ul class="lista_descripcion"><li><a>ESTE PAQUETE NO TIENE NINGUNA OPERADORA</a></li></ul>
	<?php
	}
	?>
</div>

<div class="clear_mayor"></div>
<?php $usuario = $_SESSION['idusuarios']; ?>

<a href="mostrar_cotizaciones_por_generico.php?p=<?php echo $idpublicacion; ?>" target="_blank">
	<input type="button" value="Ver Planilla" style="float:right;" />
</a>

<a href="segmentar_clientes_evento.php?e=<?php echo $idpublicacion; ?>">
	<input type="button" value="Enviar Por Mail" style="float:right;" />
</a>

<input type="button" value="Imprimir" onclick="Imprimir();" style="float:right;" />

<a href="editar_evento.php?idevento=<?php echo $idpublicacion; ?>">
	<input type="button" value="Editar" style="float:right;" />
</a>

<input type="button" value="Nueva Cotizaci&oacute;n" onclick="mostrar_url_popup('crear_cotizacion_paquete.php?idpaquete=<?php echo $idpublicacion; ?>&idusuarios=<?php echo $usuario; ?>&titulo=<?php echo $paquete["titulo"]; ?>' , 450);" style="float:right;">

<script type="text/javascript">
	$(function() {
		$('.vista_pagina').html('VISTA DE UN EVENTO GENERICO');
	});
</script>

<?php include("iu_vista_evento_impresion.php"); ?>

<div class="clear"></div>


<?php include("footer.php"); ?>