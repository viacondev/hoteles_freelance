<?php
	include('header.php');
	include('../entidad/tipo_paquete.php');
	include('../entidad/publicacion.php');
	include('../entidad/paquete.php');
	include('../entidad/imagen.php');
	include('../entidad/archivo_adjunto.php');
	include('../entidad/operadora.php');
	include('../entidad/destino.php');
	include('../entidad/usuarios_paquetes.php');
	include('../control/c_mostrar_cotizacion_paquete.php');
	include('../entidad/paquete_cotizado.php');
	include('../entidad/clientes.php');
	include('../entidad/telefono.php');
	include('../entidad/e_mails.php');
	include('../entidad/tarifa_hotel.php');
	include('../entidad/tarifa_otros.php');
	include('../entidad/boleto_cotizado.php');
	include('../entidad/segmento_publicado.php');
	//include('../entidad/lineas_aereas.php');
	//include('../entidad/aeropuerto.php');
	include('../entidad/opcion_hoteles.php');
	include('../control/c_pax_paquete.php');
	include("../entidad/pax_paquete.php");
	include('../entidad/hotel_cotizado.php');
	include('../entidad/otros_cotizado.php');
	/*
	* ARCHIVOS PROVENIENTES DE RESERVAS
	*/
	include('../../reservas/BD/sessionCreate_ext.php');
	include('../../reservas/BD/lib.xml.php');
	include('../../reservas/entidades/sabre.php');
	include('../../reservas/controladoras/c_mostrar_reserva.php');
	include('../../reservas/entidades/aeropuerto.php');
	include('../../reservas/entidades/lineas_aereas.php');
    include('../../reservas/entidades/cotizacion.php');
	include('../../reservas/controladoras/c_realizar_reserva.php');
    include('../../reservas/interfaces/condiciones_tarifa_esp.php');

	$idcotizacion = $_GET['cotizacion'];
	$type         = $_GET['type'];

	$c_cotizacion_paquete 	= new c_mostrar_cotizacion_paquete;

	$datos_paquete_cotizado 	= $c_cotizacion_paquete->obtener_cotizacion_paquete($idcotizacion);
	//echo "<pre>"; print_r($datos_paquete_cotizado); echo "</pre>";
	$datos_paquete 			= $c_cotizacion_paquete->obtener_paquete($datos_paquete_cotizado['idpaquete']);
	//echo "<pre>"; print_r($datos_paquete); echo "</pre>";
	$imagenes 			= imagen::obtener_imagenes_de_publicacion($datos_paquete_cotizado['idpaquete']);
	$ruta 				= "../../online/js/slider_detalle_paquete/contenido_paquete/data1/images/";
	$ruta_imagenes_ch_g = "../../online/js/slider_inicio/promo/data1/images/";

	if($type == 'p')
		$rep_pax_paquete 	= c_pax_paquete::obtener_pax_paquete_idcotizacion($idcotizacion);

	$array_servicios_x_pax 	= array();

	if($rep_pax_paquete === false)
		$rep_pax_paquete = array();
?>

<div class="clear_mayor"></div>

<div style="display:none;">
	<div class="myPrintArea">
		<div class="panel_print">
			<table style="width:21cm;">
	            <tr>
	                <td>
	                    <img src="images\logo.png" style="width:350px; height:70px;" />    
	                </td>
	                <td style="font-size:8pt; vertical-align:top; text-align:right; padding-right:10px;">
	                    CALLE VELASCO ESQ. SUAREZ DE FIGUEROA<br/>
	                    TELF 591-3-363610<br/>
	                    SANTA CRUZ - BOLIVIA<br/>
	                </td>
	            </tr>
	        </table>
        </div>
	</div>
</div>
	<!-- INICIO DEL PANEL DE IMPRESION -->
<div class="myPrintArea">
	<div class="panel_print">

		<div class="sixteen columns">
			<!--<img src="<?php echo $ruta . $imagenes[0]['directorio']; ?>" style="width:100%;" />-->
			<center>
				<strong style="font-size:16pt;"><?php echo $datos_paquete['titulo']; ?></strong><br/>
			</center>
		</div>
		<div class="clear_mayor"></div>
		<table style="width:21cm;">
			<tr>
				<td style="width:10%;"></td>
				<td><strong style="color:#10689b;">SALIDA</strong></td>
				<td><strong style="color:#10689b;">:</strong></td>
				<td><strong>&nbsp;<?php echo $datos_paquete_cotizado['salida']; ?></strong></td>
			</tr>
			<tr>
				<td></td>
				<td><strong style="color:#10689b;">RETORNO</strong></td>
				<td><strong style="color:#10689b;">:</strong></td>
				<td><strong>&nbsp;<?php echo $datos_paquete_cotizado['retorno']; ?></strong></td>
			</tr>
		</table>
		<div class="clear_mayor"></div>

<?php
	if(count($rep_pax_paquete) <= 0)
	{
?>
		<div class="sixteen columns">
			NO EXISTEN PASAJEROS EN ESTA COTIZACION
		</div>
<?php
	}
	else
	{
?>
		<div class="sixteen columns" id="tabla_resumen">
			<strong style="font-size:14pt;">RESUMEN DE COSTOS DEL PAQUETE POR PASAJERO</strong>
			<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" class="con-borde" >
				<tr style="background-color:#10689b; color:#FFFFFF;">
					<th rowspan="2">N.</th>
					<th rowspan="2">PASAJERO</th>
					<th colspan="2">BOLETO</th>
					<th colspan="2">PAQUETE</th>
					<th rowspan="2">TOTAL</th>
				</tr>
				<tr style="background-color:#10689b; color:#FFFFFF;">
					<th >COSTO</th>
					<th >TIME LIMIT</th>
					<th >COSTO</th>
					<th >TIME LIMIT</th>
				</tr>
<?php
		$total_cotizacion = 0;

		for ($i=0; $i < count($rep_pax_paquete); $i++) 
		{ 
			$pax = $rep_pax_paquete[$i];
			$pax_pq = $pax['idpax_paquete'];

			$total_hotel 		= ceil($c_cotizacion_paquete->obtener_total_monto_pax_hotel($pax_pq));//0;
			$tlimit_hotel 		= $c_cotizacion_paquete->obtener_min_time_limit_hotel($pax_pq);

			$total_boleto 		= ceil($c_cotizacion_paquete->obtener_total_monto_pax($pax_pq));
			$tlimit_boleto      = $c_cotizacion_paquete->obtener_min_time_limit_boleto($pax_pq);

			$total_otros 		= ceil($c_cotizacion_paquete->obtener_total_monto_pax_otros($pax_pq));//0;
			$total_hotel_otros 	= $total_hotel + $total_otros;

			$total_pax 			= $total_hotel + $total_boleto + $total_otros;
			$total_cotizacion 	+= $total_pax;
?>
				<tr>
					<td style="background-color:#EEEEEE"><?php echo ($i+1); ?></td>
					<td><?php echo strtoupper($pax['trato_cliente'] . ' ' . $pax['nombre_cliente'] . ' ' . $pax['apellido_cliente']); ?></td>
					<td style="background-color:#CEEED4; text-align:right;"><?php echo $total_boleto; ?></td>
					<td style="background-color:#CEEED4; text-align:right;"><?php echo $tlimit_boleto; ?></td>
					<td style="background-color:#FFDFD5; text-align:right;"><?php echo $total_hotel_otros; ?></td>
					<td style="background-color:#FFDFD5; text-align:right;"><?php echo $tlimit_hotel; ?></td>
					<td style="background-color:#FCD5FC; text-align:right;"><?php echo $total_pax; ?></td>
				</tr>
<?
			$s_pax 				= array();
			$s_pax['nombre'] 	= strtoupper($pax['trato_cliente'] . ' ' . $pax['nombre_cliente'] . ' ' . $pax['apellido_cliente']);

			$hoteles 			= $c_cotizacion_paquete->obtener_servicios_de_hotel_por_pax($pax_pq);
			$otros_servicios 	= $c_cotizacion_paquete->obtener_otros_servicios_por_pax($pax_pq);
			$boletos 			= $c_cotizacion_paquete->obtener_boletos_por_pax($pax_pq);

			$s_pax['hoteles'] 			= $hoteles;
			$s_pax['otros_servicios'] 	= $otros_servicios;
			$s_pax['boletos'] 			= $boletos;

			$array_servicios_x_pax[] 	= $s_pax;
		}
?>
				<tr style="font-weight:bold;">
					<td colspan="6">TOTAL MONTO USD</td>
					<td style="text-align:right;"><?php echo $total_cotizacion; ?></td>
				</tr>
			</table>
		</div>

		<div class="clear_mayor"></div>
<?php	
	}
	
	//echo "<pre>"; print_r($array_servicios_x_pax); echo "</pre>";

	$array_reservas 		= array();
	$array_hoteles 			= array();
	$array_otros_servicios 	= array();

	for ($i=0; $i < count($array_servicios_x_pax); $i++) 
	{ 
		/*
		* OBTIENE LAS RESERVAS DE BOLETOS AEREOS
		*/
		$boletos = $array_servicios_x_pax[$i]['boletos'];
		for ($j=0; $j < count($boletos); $j++) 
		{ 
			$array_reservas[] = $boletos[$j]['codigo_reserva'];
		}
		/*
		* HOTELES POR DESTINO Y POR ITEM HOTEL
		*/
		$hoteles = $array_servicios_x_pax[$i]['hoteles'];
		for ($j=0; $j < count($hoteles); $j++) 
		{ 
			$iditem = $hoteles[$j]['iditem_hotel_paquete_cotizado'];
			$destino = $hoteles[$j]['destino'];
			if(!array_key_exists($destino, $array_hoteles))
			{
				$array_hoteles[$destino] = array();
			}
			if(!array_key_exists($iditem, $array_hoteles[$destino]))
			{
				$array_hoteles[$destino][$iditem] 						= array();
				$array_hoteles[$destino][$iditem]['nombre'] 			= $hoteles[$j]['nombre_hotel'] . ' - ' . $hoteles[$j]['categoria'] . '*';
				$array_hoteles[$destino][$iditem]['in'] 				= date('d/M/Y', strtotime($hoteles[$j]['in'] . ' 00:00:00'));
				$array_hoteles[$destino][$iditem]['out'] 				= date('d/M/Y', strtotime($hoteles[$j]['out'] . ' 00:00:00'));
				$array_hoteles[$destino][$iditem]['identificador'] 		= $hoteles[$j]['identificador'];
				$array_hoteles[$destino][$iditem]['alimentacion'] 		= $hoteles[$j]['alimentacion'];
				$array_hoteles[$destino][$iditem]['time_limit'] 		= date('d/M/Y', strtotime($hoteles[$j]['time_limit']));
				$array_hoteles[$destino][$iditem]['info_extra'] 		= $hoteles[$j]['info_extra'];
				$array_hoteles[$destino][$iditem]['moneda'] 			= $hoteles[$j]['moneda'];
				$array_hoteles[$destino][$iditem]['tipo_cambio'] 		= $hoteles[$j]['tipo_cambio'];
				$array_hoteles[$destino][$iditem]['fee'] 				= $hoteles[$j]['comision'];
				$array_hoteles[$destino][$iditem]['iva'] 				= $hoteles[$j]['factura'];
				$array_hoteles[$destino][$iditem]['pax'] 				= array();
			}
			$datos_pax 		= array();
			$acomodacion 	= '';
			$precio 		= 0;
			switch ($hoteles[$j]['tipo_precio']) 
			{
				case 1:
					{
						$acomodacion = 'SINGLE';
						$precio = $hoteles[$j]['precio_single'];
					}
					break;
				case 2:
					{
						$acomodacion = 'DOBLE';
						$precio = $hoteles[$j]['precio_doble'];
					}
					break;
				case 3:
					{
						$acomodacion = 'TRIPLE';
						$precio = $hoteles[$j]['precio_triple'];
					}
					break;
				case 4:
					{
						$acomodacion = 'CUADRUPLE';
						$precio = 	$hoteles[$j]['precio_cuadruple'];
					}
					break;
				case 5:
					{
						$acomodacion = 'MENOR';
						$precio = 	$hoteles[$j]['precio_menor'];		
					}
					break;
				case 6:
					{
						$acomodacion = 'INFANTE';	
						$precio = 	$hoteles[$j]['precio_infante'];
					}
					break;
				default:
					$acomodacion = 'OTRO';
					break;
			}
			$datos_pax['nombre'] 		= $array_servicios_x_pax[$i]['nombre'];
			$datos_pax['acomodacion'] 	= $acomodacion;
			$datos_pax['costo'] 		= $precio;

			$array_hoteles[$destino][$iditem]['pax'][] = $datos_pax;
		}
		/*
		* OTROS SERVICIOS
		*/
		$otros_servicios = $array_servicios_x_pax[$i]['otros_servicios'];
		for ($j=0; $j < count($otros_servicios); $j++) 
		{ 
			$tipo_precio = '';
			switch ($otros_servicios[$j]['tipo_precio']) 
			{
				case 1:
				{
					$tipo_precio = 'ADULTO';
					$costo = $otros_servicios[$j]['precio_adulto'];
				}break;
				case 1:
				{
					$tipo_precio = 'MENOR';
					$costo = $otros_servicios[$j]['precio_menor'];
				}break;
				case 1:
				{
					$tipo_precio = 'INFANTE';
					$costo = $otros_servicios[$j]['precio_infante'];
				}break;
				default:
				{
					$tipo_precio = 'NN';
					$costo = 0;
				}break;
			}

			if($otros_servicios[$j]['moneda'] == 'B')
				$costo = $costo / $otros_servicios[$j]['tipo_cambio'];

			$otros_servicios[$j]['tipo_precio'] 	= $tipo_precio;
			$otros_servicios[$j]['costo'] 			= $costo;
			$otros_servicios[$j]['pax'] 			= $array_servicios_x_pax[$i]['nombre'];
			$otros_servicios[$j]['time_limit'] 		= strtoupper(date('d/M/Y', strtotime($otros_servicios[$j]['time_limit'])));
			$array_otros_servicios[] 				= $otros_servicios[$j];
		}
	}

	$reservas 			= array_map("unserialize", array_unique(array_map("serialize", $array_reservas)));
	$array_reservas 	= array_values($reservas);

?>
	<div class="clear_mayor"></div>
	<div id="descripcion_incluye">
	<div class="sixteen columns">
		<strong style="font-size:14pt;">DESCRIPCION DEL PAQUETE POR PASAJERO</strong><br/>
		<div class="clear_mayor"></div>
<?php
	for ($i=0; $i < count($array_servicios_x_pax); $i++) 
	{ 
?>
		<strong style="color:#10689b; margin-left:15px;"><?php echo $array_servicios_x_pax[$i]['nombre']; ?></strong>
		<ul class="lista_incluye" style="margin-left:30px;">
			<?php
				$boletos = $array_servicios_x_pax[$i]['boletos'];
				if(count($boletos) > 0)
				{
			?>
					<li style="list-style:none;">
		                <Strong style="text-decoration:underline;">BOLETO AEREO</Strong>
		            </li>
			<?php
				}
				for ($j=0; $j < count($boletos); $j++) 
				{ 
			?>
					<li>
		                <a>BOLETO AEREO <strong><?php echo $boletos[$j]['ruta']; ?></strong> CON <strong><?php echo strtoupper($boletos[$j]['nombre_linea_aerea']); ?></strong>.</a>
		            </li>
			<?php
				}
			?>
			<?php
				$hoteles = $array_servicios_x_pax[$i]['hoteles'];
				if(count($hoteles) > 0)
				{
			?>
					<li style="list-style:none;">
		                <Strong style="text-decoration:underline;">HOTEL</Strong>
		            </li>
			<?php
				}
				for ($j=0; $j < count($hoteles); $j++) 
				{ 
					$cant_noch = floor( (strtotime($hoteles[$j]['out'] . ' 00:00:00') - strtotime($hoteles[$j]['in'] . ' 00:00:00')) / 86400);
			?>
					<li>
		                <a><?php echo $cant_noch; ?> NOCHES DE ALOJAMIENTO EN HOTEL <strong><?php echo strtoupper($hoteles[$j]['nombre_hotel']) . ' ' . $hoteles[$j]['categoria'] . '*'; ?></strong> EN <strong><?php echo strtoupper($hoteles[$j]['destino']); ?></strong>.</a>
		            </li>
			<?php
				}
			?>
			<?php
				$otros_servicios = $array_servicios_x_pax[$i]['otros_servicios'];
				if(count($otros_servicios) > 0)
				{
			?>
					<li style="list-style:none;">
		                <Strong style="text-decoration:underline;">OTROS SERVICIOS</Strong>
		            </li>
			<?php
				}
				for ($j=0; $j < count($otros_servicios); $j++) 
				{ 
			?>
					<li>
		                <a><?php echo strtoupper($otros_servicios[$j]['nombre_servicio'] . ' ' . $otros_servicios[$j]['descripcion']); ?>.</a>
		            </li>
			<?php
				}
			?>
		</ul>	
<?php
	}
?>
	</div>
	</div>
	<div class="clear_mayor"></div>
	<!-- INICIO PANEL DETALLE -->
	<div class="clear_mayor"></div>
	<div id="tabla_detalles">
		<div class="sixteen columns">
			<strong style="font-size:14pt;">DETALLE DE ITINERARIOS</strong>
			<?php 
				//echo "<pre>"; print_r($array_reservas); echo "</pre>"; 
				for ($i=0; $i < count($array_reservas); $i++) 
				{ 
					$codigo_reserva = $array_reservas[$i];
					include "../../reservas/interfaces/cargar_reserva.php";

					if(isset($mensaje_error))
					{
						echo $mensaje_error . "<br/>";
						unset($mensaje_error);
						unset($error);
					}
					else
					{
				?>
						<table style="width:100%; font-size:8pt;" >
							<tr>
								<td style="padding:15px;">
									<table style=" border:1px solid #AAAAAA;" class="con-borde">
										<tr style="background-color:#10689b; color:#FFFFFF;">
											<th>PASAJERO</th>
											<th>NRO TKT</th>
										</tr>
										<?php
											for ($j=1; $j <= count($nombres_pasajeros); $j++) 
											{ 
										?>
										<tr>
											<th><?php echo $nombres_pasajeros[$j]; ?></th>
											<th>
												<?php
													for ($k=0; $k < count($array_tickets[$j]); $k++) 
													{
														echo $array_tickets[$j][$k] . '&nbsp;&nbsp;&nbsp;';
													}
												?>
											</th>
										</tr>
										<?php
											}
										?>
									</table>
									<br/>
									<table style="width:90%; border:1px solid #AAAAAA;" class="con-borde">
										<tr style="background-color:#10689b; color:#FFFFFF;">
											<th>N.</th>
											<th>LINEA AEREA</th>
											<th>VUELO</th>
											<th>FECHA</th>
											<th>SALIDA</th>
											<th>RETORNO</th>
											<th>TIEMPO</th>
											<th></th>
										</tr>
										<?php
											$array_seg = array();
											for ($j=0; $j < count($itinerario_completo); $j++) 
											{ 
												$segmento = $itinerario_completo[$j];
												$array_seg[] = $segmento['numero'];
										?>
										<tr>
											<td><?php echo $segmento['numero']; ?></td>
											<td><?php echo $segmento['linea_aerea']; ?></td>
											<td><?php echo $segmento['vuelo']; ?></td>
											<td><?php echo $segmento['fecha_salida']; ?></td>
											<td><?php echo $segmento['origen'] . ' (' . $segmento['hora_salida'] . ')'; ?></td>
											<td><?php echo $segmento['destino'] . ' (' . $segmento['hora_llegada'] . ')'; ?></td>
											<td><?php echo str_replace('.', ':', $segmento['tiempo_vuelo']); ?></td>
											<td><?php echo $segmento['estado_segmento']; ?></td>
										</tr>
										<?php
											}
											$array_seg 		= array_values($array_seg);
											$cant_segmentos = count($array_seg);
										?>
									</table>
									<br/>
									<table style=" border:1px solid #AAAAAA;" class="con-borde">
										<tr style="background-color:#10689b; color:#FFFFFF;">
											<th rowspan="2">PASAJERO</th>
											<th colspan="<?php echo $cant_segmentos; ?>">ASIENTOS POR SEG</th>
											<th rowspan="2">COD VIAJERO FRECUENTE</th>
										</tr>
										<tr style="background-color:#10689b; color:#FFFFFF;">
											<?php 
												for ($j=0; $j < $cant_segmentos; $j++) 
												{ 
											?>
											<th><?php echo $array_seg[$j]; ?></th>
											<?php
												}
											?>
										</tr>
										<?php
											for ($j=1; $j <= count($nombres_pasajeros); $j++) 
											{ 
										?>
										<tr>
											<td><?php echo $nombres_pasajeros[$j]; ?></td>
											<?php 
												for ($k=0; $k < $cant_segmentos; $k++) 
												{ 
											?>
											<td><?php echo $numero_asiento[$j][$array_seg[$k]]; ?></td>
											<?php
												}
												$frecuent_fly = 'NO ASIGNADO';
												if(array_key_exists($j, $viajero_frecuente))
													$frecuent_fly = implode(' - ', $viajero_frecuente[$j]);
											?>
											<td><?php echo $frecuent_fly; ?></td>
										</tr>
										<?php
											}
										?>
									</table>
									<br/>
									<table style=" border:1px solid #AAAAAA;" class="con-borde">
										<tr style="background-color:#10689b; color:#FFFFFF;">
											<th colspan="<?php echo count($array_restricciones); ?>">CONDICIONES</th>
										</tr>
										<?php
											if(count($array_restricciones) > 0)
											{
												foreach ($array_restricciones as $key => $value) 
												{
													$max_equip_la = $maximo_equipaje[$key];
													$descripcion_equip = "";
													if(is_array($max_equip_la))
													{
														$descripcion_equip .= "MAXIMO DE EQUIPAJE POR VUELO:<br/>";
														foreach ($max_equip_la as $n_vuelo => $equipaje) 
														{
															$descripcion_equip .= $equipaje . '(' . $n_vuelo . ') ; ';
														}	
													}
										?>
										<tr>
											<td><?php echo '<center>' . $key . '</center>' . $descripcion_equip . '<br/>' . implode('<br/>', $value); ?></td>
										</tr>
										<?php
												}
											}
										?>
									</table>
								</td>
							</tr>
						</table>
				<?php
						/*echo "<pre>"; print_r($nombres_pasajeros); echo "</pre>";
				        echo "<pre>"; print_r($array_tickets); echo "</pre>";
				        echo "<pre>"; print_r($itinerario_completo); echo "</pre>";
				        echo "<pre>"; print_r($numero_asiento); echo "</pre>";
				        echo "<pre>"; print_r($viajero_frecuente); echo "</pre>";
				        //echo "<pre>"; print_r($array_precios); echo "</pre>";
				        echo "<pre>"; print_r($maximo_equipaje); echo "</pre>";
				        echo "<pre>"; print_r($array_restricciones); echo "</pre>";*/
					}
				?>
				<hr/>
				<?php
				}
			?>
		</div>

		<div class="clear_mayor"></div>

		<div class="sixteen columns">
			<strong style="font-size:14pt;">DETALLE DE HOTELES</strong>
			<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" class="con-borde detalle" >
				<tr style="background-color:#10689b; color:#FFFFFF;">
					<th>HOTEL</th>
					<th>IN</th>
					<th>OUT</th>
					<th>T. LIMIT</th>
					<th>PAX</th>
					<th>HAB</th>
					<th>$ USD</th>
				</tr>
			<?php
				$costo_total_hotel = 0;
				foreach ($array_hoteles as $key => $destino) 
				{
			?>
				<tr>
					<td colspan="7">
						<strong style="color:#10689b;">DESTINO : <?php echo $key; ?></strong>
					</td>
				</tr>
			<?php
					$hoteles = $array_hoteles[$key];
					if(count($hoteles) > 0)
					{
						$k = 0;
						foreach ($hoteles as $ind => $hotel) 
						{
							$paxs = $hotel['pax'];
							$cant_pax = count($paxs);
							$fee = 1 + ($hotel['fee'] / 100);
							$iva = 1 + ($hotel['iva'] / 100);
							$costo_pax = ceil($paxs[0]['costo'] * $fee * $iva);
							$costo_total_hotel += $costo_pax;

							if($k % 2 == 0)
								$color = '#F2F2F2';
							else
								$color = '#D6EAF3';
			?>
				<tr style="background-color:<?php echo $color; ?>;">
					<td rowspan="<?php echo $cant_pax; ?>"><?php echo $hotel['nombre']; ?></td>
					<td rowspan="<?php echo $cant_pax; ?>"><?php echo $hotel['in']; ?></td>
					<td rowspan="<?php echo $cant_pax; ?>"><?php echo $hotel['out']; ?></td>
					<td rowspan="<?php echo $cant_pax; ?>"><?php echo $hotel['time_limit']; ?></td>
					<td><?php echo $paxs[0]['nombre']; ?></td>
					<td><?php echo $paxs[0]['acomodacion']; ?></td>
					<td style="text-align:right;"><?php echo $costo_pax; ?></td>
				</tr>
			<?php
							for ($i=1; $i < count($paxs); $i++)
							{
								$costo_pax = ceil($paxs[$i]['costo'] * $fee * $iva);
								$costo_total_hotel += $costo_pax;
			?>
				<tr style="background-color:<?php echo $color; ?>;">
					<td>
						<?php echo $paxs[$i]['nombre']; ?>
						<!--<img src="ddfd.png" style="float:right;" />-->
					</td>
					<td><?php echo $paxs[$i]['acomodacion']; ?></td>
					<td style="text-align:right;"><?php echo $costo_pax; ?></td>
				</tr>
			<?php
							}
							$k++;
						}
					}
				}
			?>
				<tr style="font-weight:bold;">
					<td colspan="6">TOTAL COSTO HOTEL</td>
					<td style="text-align:right;"><strong><?php echo $costo_total_hotel; ?></strong></td>
				</tr>
			</table>
		</div>

		<div class="clear_mayor"></div>

		<div class="sixteen columns">
			<strong style="font-size:14pt;">DETALLE DE SERVICIOS</strong>
			<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" class="con-borde detalle" >
				<tr style="background-color:#10689b; color:#FFFFFF;">
					<th>SERVICIO</th>
					<th>TIME LIMIT</th>
					<th>PAX</th>
					<th>TARIFA</th>
					<th>PRECIO USD</th>
				</tr>
			<?php
				$costo_total_otros_servicios 	= 0;
				$k 								= 0;
				foreach ($array_otros_servicios as $key => $servicio) 
				{
					if($k % 2 == 0)
						$color = '#F2F2F2';
					else
						$color = '#D6EAF3';

					$fact 		= (1+($servicio['fee']/100)) * (1+($servicio['factura']/100));
					$subtotal 	= ceil($servicio['costo'] * $fact);
			?>
				<tr style="background-color:<?php echo $color; ?>;">
					<td><?php echo strtoupper($servicio['nombre_servicio'] . ' - ' . $servicio['descripcion']); ?></td>
					<td><?php echo $servicio['time_limit']; ?></td>
					<td><?php echo $servicio['pax']; ?></td>
					<td><?php echo $servicio['tipo_precio']; ?></td>
					<td style="text-align:right;"><?php echo $subtotal; ?></td>
				</tr>
			<?php
					$costo_total_otros_servicios += $subtotal;
					$k++;
				}
			?>
				<tr style="font-weight:bold;">
					<td colspan="4">TOTAL COSTO SERVICIOS</td>
					<td style="text-align:right;"><strong><?php echo $costo_total_otros_servicios; ?></strong></td>
				</tr>
			</table>
		</div>

	</div>
	<!-- FIN PANEL DETALLE -->
	</div>
</div>
	<!-- FIN DE LA VISTA DE IMPRESION -->
<p/>
<hr/>

<div class="clear"></div>

<div class="three columns">
	<input type="checkbox" value="" checked onchange="$('#tabla_resumen').toggle();" />Mostrar Resumen
</div>
<div class="three columns">
	<input type="checkbox" value="" checked onchange="$('#descripcion_incluye').toggle();" />Mostrar Descripci&oacute;n
</div>
<div class="three columns">
	<input type="checkbox" value="" checked onchange="$('#tabla_detalles').toggle();" />Mostrar Detalle
</div>
<div class="three columns">
	<input type="button" value="Imprimir" onclick="ImprimirDetallePaquete();" />
</div>

<script type="text/javascript">
	function ImprimirDetallePaquete()
	{
		$('.panel_print').css('border', '1px solid #333'); 
		$('.panel_print').css('padding', '10px'); 
		Imprimir(); 
		setTimeout(function() 
					{ 
						$('.panel_print').css('border', 'none'); 
						$('.panel_print').css('padding', '0px'); 
					}, 500);
	}
</script>

<?php

	function mostrar_segmentos($t,$ultima_posicion_TravelItineraryReadRS,$ultima_posicion_TravelItinerary,$ultima_posicion_ItineraryInfo,$ultima_posicion_ReservationItem,$ultima_posicion_Item)
	{
        global $numero_asiento;
        global $array_ruta;
        global $array_carriers;
        global $itinerario_completo;
        
        $contador_item = 0;
        $contador_itinerarios = 0;
		
        while($contador_item <= $ultima_posicion_Item)
        {
            $array_item = $t['soap-env:Body']['TravelItineraryReadRS'][$ultima_posicion_TravelItineraryReadRS]['TravelItinerary'][$ultima_posicion_TravelItinerary]['ItineraryInfo'][$ultima_posicion_ItineraryInfo]['ReservationItems'][$ultima_posicion_ReservationItem]['Item'][$contador_item];
			
            if(is_array($array_item))
            {
                if(array_key_exists('FlightSegment',$array_item))
                {
                    $index_flightsegment_attr = 0;
					
                    for($k = 0; $k < count($array_item['FlightSegment']); $k++)
                    {
                        if(is_array($array_item['FlightSegment'][$k]))
                        {
                 			$ultima_posicion_FlightSegment = $k;

                            if(isset($array_item['FlightSegment'][$ultima_posicion_FlightSegment]))
                            { 
                                $segmento = array();
                                if(array_key_exists('DepartureDateTime', $array_item['FlightSegment-ATTR']))
                                {
                                    $array_FlightSegmentATTR = $array_item['FlightSegment-ATTR'];
                                }
                                else
                                {
                                    $array_FlightSegmentATTR = $array_item['FlightSegment-ATTR'][$index_flightsegment_attr];   
                                }

                                $clase_servicio = $array_FlightSegmentATTR['ResBookDesigCode'];
								
                                $hora_sale		= $array_FlightSegmentATTR['DepartureDateTime'];//HORA SALE
                                $fecha_sale_f 	= $hora_sale[8].$hora_sale[9]."/".$hora_sale[5].$hora_sale[6]."/".$hora_sale[0].$hora_sale[1].$hora_sale[2].$hora_sale[3];
                                $hora_llega		= $array_FlightSegmentATTR['ArrivalDateTime'];//HORA LLEGA
                                $fecha_llega_f 	= $hora_llega[3].$hora_llega[4].'/'.$hora_llega[0].$hora_llega[1]."/".$hora_sale[0].$hora_sale[1].$hora_sale[2].$hora_sale[3].'';
                                
                                $numero_segmento = (int)$array_FlightSegmentATTR['SegmentNumber'];
                                for($r=1; array_key_exists($r, $numero_asiento); $r++)
                                {
                                        $numero_asiento[$r][$numero_segmento] = "N/A";    
                                }
                                
                                //Agregar al array temporal
                                $segmento["numero"] = $numero_segmento;
                                //Se utilizará para mostrar al final
                                $cod_aerea = $array_item['FlightSegment'][$ultima_posicion_FlightSegment]['MarketingAirline-ATTR']['Code'];
                                $array_carriers[]=$cod_aerea;
                                //Agregar al array temporal
                                $segmento["linea_aerea"] = traer_nombre_linea_aerea($cod_aerea);
                                $segmento["cod_linea_aerea"] = $cod_aerea;
                                //Se utilizará para mostrar al final
                                if(array_key_exists('OperatingAirline-ATTR', $array_item['FlightSegment'][$ultima_posicion_FlightSegment]))
                                {
                                    $cod_operating = $array_item['FlightSegment'][$ultima_posicion_FlightSegment]['OperatingAirline-ATTR']['Code'];
                                    $segmento["linea_aerea_operating"] = $array_item['FlightSegment'][$ultima_posicion_FlightSegment]['OperatingAirline-ATTR']['CompanyShortName'];
                                    $segmento["cod_linea_aerea_operating"] = $cod_operating;
                                }
                                //Agregar al array temporal
                                $segmento["vuelo"] = $array_FlightSegmentATTR['FlightNumber'];
                                //Se utilizará para mostrar al final
                                //Agregar al array temporal
                                $segmento["clase"] = $clase_servicio;
                                //Se utilizará para mostrar al final
                                //Agregar al array temporal
                                $segmento["equipo"] = $array_item['FlightSegment'][$ultima_posicion_FlightSegment]['Equipment-ATTR']['AirEquipType'];;
                                //Se utilizará para mostrar al final
                                //Agregar al array temporal
                                $segmento["fecha_salida"] = substr($fecha_sale_f,0,6).substr($fecha_sale_f,8,2);
                                //Se utilizará para mostrar al final
                                $origen = $array_item['FlightSegment'][$ultima_posicion_FlightSegment]['OriginLocation-ATTR']['LocationCode'];
                                //Agregar al array temporal
                                $segmento["origen"] = strtoupper(traer_nombre_codigo_aeropuerto($origen));
                                //Se utilizará para mostrar al final
                                $hora_s_f = $hora_sale[11].$hora_sale[12].$hora_sale[13].$hora_sale[14].$hora_sale[15];
                                //Agregar al array temporal
                                $segmento["hora_salida"] = $hora_s_f;
                                //Se utilizará para mostrar al final
                                $destino = $array_item['FlightSegment'][$ultima_posicion_FlightSegment]['DestinationLocation-ATTR']['LocationCode'];
                                //Agregar al array temporal
                                $segmento["destino"] = strtoupper(traer_nombre_codigo_aeropuerto($destino));
                                //Se utilizará para mostrar al final
                                if(array_key_exists('Meal-ATTR', $array_item['FlightSegment'][$ultima_posicion_FlightSegment]))
                                {
                                    $segmento["comidas"] = ObtenerComida($array_item['FlightSegment'][$ultima_posicion_FlightSegment]['Meal-ATTR']['Code']);
                                    if(array_key_exists(5, $array_item['FlightSegment'][$ultima_posicion_FlightSegment]['Meal-ATTR']))
                                        $segmento["comidas"] .= ' - ' . ObtenerComida($array_item['FlightSegment'][$ultima_posicion_FlightSegment]['Meal-ATTR'][5]['Code']);
                                }
                                else
                                    $segmento["comidas"] = ' - ';
								
                                $hora_l_f 		= $hora_llega[6].$hora_llega[7].$hora_llega[8].$hora_llega[9].$hora_llega[10];
                                $salida_fec 	= strtotime(substr($fecha_sale_f,6,4)."-".substr($fecha_sale_f,3,2)."-".substr($fecha_sale_f,0,2));
                                $llegada_fec 	= strtotime(substr($fecha_llega_f,6,4)."-".substr($fecha_llega_f,3,2)."-".substr($fecha_llega_f,0,2));
                                $res_fec 		= $llegada_fec - $salida_fec;
                                $dif_fecha 		= round($res_fec/86400);
                             
                                if($dif_fecha > 0)
                                {
                                    //Agregar al array temporal
                                    $segmento["hora_llegada"] = $hora_l_f.'&nbsp;<span class="info" style="font: 7.5pt Courier New;" title="'.substr($fecha_llega_f,0,6).substr($fecha_llega_f,8,2).'">+'.$dif_fecha.'D&iacute;a</span>';
                                    //Se utilizará para mostrar al final                                       
                                }
                                else
                                {
                                    if($dif_fecha < 0)
                                    {
                                        //Agregar al array temporal
                                        $segmento["hora_llegada"] = $hora_l_f.'&nbsp;<span class="info" style="font: 7.5pt Courier New;" title="'.substr($fecha_llega_f,0,6).substr($fecha_llega_f,8,2).'">'.$dif_fech.'D&iacute;a</span>';
                                        //Se utilizará para mostrar al final  
                                    }
                                    else
                                    {
                                        //Agregar al array temporal
                                        $segmento["hora_llegada"] = $hora_l_f.'';
                                        //Se utilizará para mostrar al final  
                                    }               
                                }
                            
                                //Agregar al array temporal
                                $segmento["tiempo_vuelo"] = $array_FlightSegmentATTR['ElapsedTime'];
                                //Se utilizará para mostrar al final
                                //Agregar al array temporal
                                $segmento["estado_segmento"] = $array_FlightSegmentATTR['Status'];
                                //Se utilizará para mostrar al final
                                    if(array_key_exists('SupplierRef-ATTR', $array_item['FlightSegment'][$ultima_posicion_FlightSegment]))
                                    {
                                        $cod_aerea = $array_item['FlightSegment'][$ultima_posicion_FlightSegment]['SupplierRef-ATTR']['ID'] ;
                                        //Agregar al array temporal
                                        $segmento["pnr_segmento"] = $cod_aerea;
                                        //Se utilizará para mostrar al final
                                    }

                                if(count($array_ruta)==0)
                                {
                                    $array_ruta[] = $origen;
                                }
                                $array_ruta[] = $destino;
                                
                                $contador_itinerarios++;

                                $itinerario_completo[] = $segmento;
                            }
                            $index_flightsegment_attr++;
                        }
                    }
                }
                else if(array_key_exists('Seats', $array_item))
                {
                    $ult_pos_seats = count($array_item['Seats']) - 1;
                    if(array_key_exists('Seat-ATTR', $array_item['Seats'][$ult_pos_seats]))
                    {
                        $array_asientos = $array_item['Seats'][$ult_pos_seats]['Seat-ATTR'];
                        if(!array_key_exists(0, $array_asientos))
                        {
                            $array_asientos[0] = $array_asientos;
                        }
                        for($u=0; $u<count($array_asientos); $u++)
                        {
                            $name_number = explode('.', $array_asientos[$u]['NameNumber']);
                            $numero_entero = (int)$name_number[0];
                            $numero_segmento = (int)$array_asientos[$u]['SegmentNumber'];
                            $numero_asiento[$numero_entero][$numero_segmento]=$array_asientos[$u]['Number'];
                        }
                    }
                }
            }
            else
            {
            }
        	$contador_item++;
    	}
		
        global $numero_segmentos;
        $numero_segmentos = $contador_itinerarios;
	}

	function precio_pasajero($PriceQuote,$posicion,$incremento)
	{
        global $historial_precios;

        $array_PriceQuote_validos = array();
        while ($i<count($PriceQuote))
        {
            if(is_array($PriceQuote[$i]))
            {
                $detalle_precio = array();
                if(array_key_exists('PricedItinerary', $PriceQuote[$i]))
                {
                    $ultima_posicion_PricedItinerary            = count($PriceQuote[$i]['PricedItinerary'])-1;
                    $ultima_posicion_AirItineraryPricingInfo    = count($PriceQuote[$i]['PricedItinerary'][$ultima_posicion_PricedItinerary]['AirItineraryPricingInfo'])-1;
                    $ultima_posicion_ItinTotalFare              = count($PriceQuote[$i]['PricedItinerary'][$ultima_posicion_PricedItinerary]['AirItineraryPricingInfo'][$ultima_posicion_AirItineraryPricingInfo]['ItinTotalFare'])-1;
                    $ItinTotalFare                              = $PriceQuote[$i]['PricedItinerary'][$ultima_posicion_PricedItinerary]['AirItineraryPricingInfo'][$ultima_posicion_AirItineraryPricingInfo]['ItinTotalFare'][$ultima_posicion_ItinTotalFare];

                    $PassengerTypeQuantity_ATTR = $PriceQuote[$i]['PricedItinerary'][$ultima_posicion_PricedItinerary]['AirItineraryPricingInfo'][$ultima_posicion_AirItineraryPricingInfo]['PassengerTypeQuantity-ATTR'];
                    $pasajeros                  = $PassengerTypeQuantity_ATTR['Code'];
                    
                    $cantidad_pasajeros         = $PassengerTypeQuantity_ATTR['Quantity'];
                    $precio_pasajero            = $ItinTotalFare['TotalFare-ATTR']['Amount'];
                    $precio_pasajero_moneda     = $ItinTotalFare['TotalFare-ATTR']['CurrencyCode'];
                    
                    $detalle_precio['cantidad']     = $cantidad_pasajeros;
                    $detalle_precio['tipo_pax']     = $pasajeros;
                    $detalle_precio['linea']        = $PriceQuote[$i]['PricedItinerary-ATTR']['ValidatingCarrier'];
                    $detalle_precio['total']        = $precio_pasajero * $cantidad_pasajeros;
                    $detalle_precio['moneda']       = $precio_pasajero_moneda;
                }

                if(array_key_exists('MiscInformation', $PriceQuote[$i]))
                {
                    
                    $ultima_posicion_MiscInformation = count($PriceQuote[$i]["MiscInformation"]) - 1;
                    $ultima_posicion_SignatureLine   = count($PriceQuote[$i]["MiscInformation"][$ultima_posicion_MiscInformation]["SignatureLine"]) - 1;
                    $texto_fecha = $PriceQuote[$i]["MiscInformation"][$ultima_posicion_MiscInformation]["SignatureLine"][$ultima_posicion_SignatureLine]["Text"];
                    
                    $fecha_hora_pq = substr($texto_fecha, -12);

                    if(!array_key_exists($fecha_hora_pq, $historial_precios))
                    {
                        $historial_precios[$fecha_hora_pq] = array();
                    }

                    $signature_line = $PriceQuote[$i]["MiscInformation"][$ultima_posicion_MiscInformation]["SignatureLine-ATTR"];
                    if(array_key_exists('ExpirationDateTime', $signature_line))
                    {
                        if($signature_line['ExpirationDateTime'] == '00:00' && $signature_line['Status'] == 'EXPIRED')
                        {
                            $array_PriceQuote_validos[] = $PriceQuote[$i];
                            $detalle_precio["estado"] = "Activo";
                        }
                        else
                        {
                            $detalle_precio["estado"] = "Inactivo";
                        }
                    }
                    else
                    {
                        $detalle_precio["estado"] = "Inactivo";
                    }

                    $historial_precios[$fecha_hora_pq][] = $detalle_precio;
                }
            }
            $i = $i + 1;
        }

        $PriceQuote = $array_PriceQuote_validos;
        global $array_precios;
    	$total	= 0;
    	$i		= 0;
        $precios= array();
		
    	while ($i<count($PriceQuote))
    	{
        	if(is_array($PriceQuote[$i]))
        	{
            	if(array_key_exists('PricedItinerary', $PriceQuote[$i]))
            	{
					$ultima_posicion_PricedItinerary 			= count($PriceQuote[$i]['PricedItinerary'])-1;
					$ultima_posicion_AirItineraryPricingInfo 	= count($PriceQuote[$i]['PricedItinerary'][$ultima_posicion_PricedItinerary]['AirItineraryPricingInfo'])-1;
					$ultima_posicion_ItinTotalFare 				= count($PriceQuote[$i]['PricedItinerary'][$ultima_posicion_PricedItinerary]['AirItineraryPricingInfo'][$ultima_posicion_AirItineraryPricingInfo]['ItinTotalFare'])-1;
					$ItinTotalFare								= $PriceQuote[$i]['PricedItinerary'][$ultima_posicion_PricedItinerary]['AirItineraryPricingInfo'][$ultima_posicion_AirItineraryPricingInfo]['ItinTotalFare'][$ultima_posicion_ItinTotalFare];
                    
                    if(is_array($ItinTotalFare['BaseFare']))
                    {
                    	if(array_key_exists('Tax-ATTR', $ItinTotalFare['BaseFare']))
    					{
                        	$impuesto = $ItinTotalFare['BaseFare']['Tax-ATTR']['Amount'];
    					}
                    	else
    					{ 
    						if(array_key_exists('Tax-ATTR', $ItinTotalFare['EquivFare']))
    						{
                        		$impuesto = $ItinTotalFare['EquivFare']['Tax-ATTR']['Amount'];
    						}
    					}
                    }
                	
					if(array_key_exists('EquivFare-ATTR', $ItinTotalFare))
                	{
						$precio_neto 	= $ItinTotalFare['EquivFare-ATTR']['Amount'];    
						$moneda 		= $ItinTotalFare['EquivFare-ATTR']['CurrencyCode'];
		            }
        	        else
            	    {
                	    $precio_neto 	= $ItinTotalFare['BaseFare-ATTR']['Amount'];
                   	 	$moneda 		= $ItinTotalFare['BaseFare-ATTR']['CurrencyCode'];  
                	}

                	$PassengerTypeQuantity_ATTR = $PriceQuote[$i]['PricedItinerary'][$ultima_posicion_PricedItinerary]['AirItineraryPricingInfo'][$ultima_posicion_AirItineraryPricingInfo]['PassengerTypeQuantity-ATTR'];
                	$pasajeros  				= $PassengerTypeQuantity_ATTR['Code'];
                    switch ($pasajeros) {
                        case 'ADT':
                            $pax = "ADULTOS";
                            break;
                        case 'CNN':
                            $pax = "MENORES";
                            break;
                        case 'INF':
                            $pax = "INFANTES";
                            break;
                    }
                	$cantidad_pasajeros 		= $PassengerTypeQuantity_ATTR['Quantity'];
	                $precio_pasajero 			= $ItinTotalFare['TotalFare-ATTR']['Amount'];
    	            $precio_pasajero_moneda 	= $ItinTotalFare['TotalFare-ATTR']['CurrencyCode'];
        		    $total_tipo_pax 			= "";
					$total_tipo_pax 			= ((int)($cantidad_pasajeros)) * $precio_pasajero;
					$total 						= $total + $total_tipo_pax;

                    $tarifa = array();
                    $tarifa['cantidad']     = $cantidad_pasajeros;
                    $tarifa['tipo_pax']     = $pasajeros;
                    $tarifa['linea']        = $PriceQuote[$i]['PricedItinerary-ATTR']['ValidatingCarrier'];
                    $tarifa['rph']          = $PriceQuote[$i]['PricedItinerary-ATTR']['RPH'];
                    $tarifa['precio_pax']   = $precio_pasajero;
                    $tarifa['total']        = $total_tipo_pax;
                    $tarifa['moneda']       = $moneda;

                    $precios[]=$tarifa;
            	}
        	}
        	$i = $i + 1;
    	}

        $total_usd = 0;$total_bob = 0;
        for($i=0; $i<count($precios); $i++)
        {
            switch ($precios[$i]['tipo_pax']) {
                case 'ADT':
                    $pax = "ADULTOS";
                    break;
                case 'CNN':
                    $pax = "MENORES";
                    break;
                case 'INF':
                    $pax = "INFANTES";
                    break;
            }
            $cantidad_pax = (int)$precios[$i]['cantidad'];
          
                    if($precios[$i]['moneda'] == 'USD')
                    {
                        $total_usd+=$precios[$i]['total'];
                    }
                    else
                    {
                        $total_bob+=$precios[$i]['total'];
                    }
        }
		$array_precios = $precios;
	}
 
	function traer_nombre_linea_aerea($codigo)
	{
    	$buffer = array();
		
    	if (!array_key_exists($codigo, $buffer))
    	{
			$ciudad_codigo 	= '';
			$lineas_aereas 	= new lineas_aereas;
			$resp			= $lineas_aereas->traer_nombre_linea_aerea($codigo);
			
			if (count($resp) == 0)
			{
				$ciudad_codigo = $codigo;
			} 
			else 
			{
				
				$ciudad			= explode(',',$resp[0]['nombre_linea_aerea']);
				$ciudad_codigo 	= $ciudad[0]."(".strtoupper($resp[0]['codigo_iata_linea_aerea']).")";
			}
        	
			$buffer[$codigo] = $ciudad_codigo;
    	}
    	return $buffer[$codigo];        
	}
           
	function traer_nombre_codigo_aeropuerto($codigo)
	{
    	$buffer = array();
		
		if (!array_key_exists($codigo, $buffer))
		{
			$ciudad_codigo 	= '';
			$aeropuerto		= new aeropuerto;
			$resp			= $aeropuerto->traer_nombre_codigo_aeropuerto($codigo);
			
			if (count($resp) == 0)
			{
				$ciudad_codigo = $codigo;
			} 
			else 
			{
				
				$ciudad			= explode(',',$resp[0]['nombre_aeropuerto']);
				$ciudad_codigo 	= $ciudad[0]."(".$resp[0]['cod_aeropuerto'].")";
			}
			
			$buffer[$codigo] = $ciudad_codigo;
		}
		return $buffer[$codigo];        
	}

    function imprimir_pasajeros_mas_ticket($pasajeros, $nro_tkt)
    {
        global $nombres_pasajeros;
        global $numero_asiento;
        global $array_tickets;
        $array_pasajeros = array();
        $personas = $pasajeros['PersonName'];

        $j = 1;
        if(array_key_exists('CustLoyalty', $pasajeros))
        {
            $auxiliar = $pasajeros['CustLoyalty'];
            if(array_key_exists('Surname', $auxiliar))
            {
                $array_pasajeros[$j] = $j . ". " . $auxiliar['Surname'] . " " . $auxiliar['GivenName'];
                $nombres_pasajeros[$j] = $auxiliar['Surname'] . " " . $auxiliar['GivenName'];
                $j++;
            }
        }
        for($i=0; $i< count($personas); $i++)
        {
            if(is_array($personas[$i]))
            {
                if(array_key_exists('Surname', $personas[$i]))
                {
                    $array_pasajeros[$j] = "<font style='color:#0000ff; font-weight:bold;'>" . $j . ".</font> " . $personas[$i]['Surname'] . " " . $personas[$i]['GivenName'];
                    $nombres_pasajeros[$j] = $personas[$i]['Surname'] . " " . $personas[$i]['GivenName'];
                    $j++;
                }
            }
        }

        if(array_key_exists('AccountingInfo', $nro_tkt))
        {
            for($i=1; $i<$j; $i++)
            {
                $array_tickets[$i] = array();
            }

            $tickets_pax = $nro_tkt['AccountingInfo'];
            for($i=0; $i<count($tickets_pax); $i++)
            {
                if(is_array($tickets_pax[$i]))
                {
                    if(array_key_exists('PersonName-ATTR', $tickets_pax[$i]))
                    {
                        $numeros = explode('.', $tickets_pax[$i]['PersonName-ATTR']['NameNumber']);
                        $numero_asiento[$numeros[0]] = array();
                        if(!BoletoAnulado($tickets_pax[$i]['BaseFare']['Document-ATTR']['Number']))
                        {
                            $array_tickets[$numeros[0]][] = $tickets_pax[$i]['Airline-ATTR']['Code'] . " " . $tickets_pax[$i]['BaseFare']['Document-ATTR']['Number'];
                            if(substr($tickets_pax[$i]['PersonName'], -3) == 'INF')
                            {
                                global $array_infantes;
                                $array_infantes[$numeros[0]] = "true";
                            }
                        }
                    }
                }
            }
        }
        
        if(count($array_tickets) > 0)
        {
            global $es_boleto_electronico;
            $es_boleto_electronico = 1;
        }
    }

	function restricciones_tarifa($PriceQuote)
	{
        global $maximo_equipaje;
    	global $array_restricciones;
		$i = 0;
		
    	while ($i < count($PriceQuote))
    	{
        	if(is_array($PriceQuote[$i]))
        	{
            	if(array_key_exists('PricedItinerary', $PriceQuote[$i]))
            	{
                    if(!array_key_exists($PriceQuote[$i]['PricedItinerary-ATTR']['ValidatingCarrier'], $array_restricciones))
                    {
                        $val_carrier = $PriceQuote[$i]['PricedItinerary-ATTR']['ValidatingCarrier'];
                        
                        $array_restricciones[$val_carrier] = array();
    					$ultima_posicion_PricedItinerary 			= count($PriceQuote[$i]['PricedItinerary'])-1;
    					$ultima_posicion_AirItineraryPricingInfo 	= count($PriceQuote[$i]['PricedItinerary'][$ultima_posicion_PricedItinerary]['AirItineraryPricingInfo'])-1;
    					$PassengerTypeQuantity 						= $PriceQuote[$i]['PricedItinerary'][$ultima_posicion_PricedItinerary]['AirItineraryPricingInfo'][$ultima_posicion_AirItineraryPricingInfo]['PassengerTypeQuantity'];

                        if (array_key_exists('FlightSegment', $PassengerTypeQuantity)) 
                        {
                            $equipaje = "MAX DE EQUIPAJE POR VUELO: ";
                            $baggages = $PassengerTypeQuantity['FlightSegment'];
                            for($x = 0; $x < count($baggages); $x++)
                            {
                                if(is_array($baggages[$x]) && array_key_exists('BaggageAllowance-ATTR', $baggages[$x]))
                                {
                                    $equipaje .= $baggages[$x]['BaggageAllowance-ATTR']['Number'] . "(" . $baggages[$x]['MarketingAirline-ATTR']['FlightNumber'] . "); ";
                                    $maximo_equipaje[$val_carrier][$baggages[$x]['MarketingAirline-ATTR']['FlightNumber']] = $baggages[$x]['BaggageAllowance-ATTR']['Number'];        
                                }
                            }
                        }
    	                if (array_key_exists('Endorsements', $PassengerTypeQuantity)) 
    					{
    						$ultima_posicion_Endorsements 	= count($PassengerTypeQuantity['Endorsements']) - 1;
    						$restrictions 					= explode('/', $PassengerTypeQuantity['Endorsements'][$ultima_posicion_Endorsements]['Text'] );

                        	for($x = 0; $x < count($restrictions); $x++)
                        	{
                                $array_restricciones[$val_carrier][] = condiciones_tarifa_esp::obtener_traduccion($restrictions[$x]);
                        	}
                    	}
    					global $es_boleto_electronico;
                    	if (array_key_exists('ResTicketingRestrictions', $PassengerTypeQuantity) && $es_boleto_electronico == 0) 
    					{
                        	$ultima_posicion_ResTicketingRestrictions = count($PassengerTypeQuantity['ResTicketingRestrictions']) - 1;
    						
                        	for($x = 0; $x <= $ultima_posicion_ResTicketingRestrictions; $x++)
                        	{
    							if(strpos($PassengerTypeQuantity['ResTicketingRestrictions'][$x], "LAST DAY TO PURCHASE") !== false)
    							{
    							    $array_restricciones[$val_carrier][] = "ULTIMO DIA DE COMPRA - " . substr($PassengerTypeQuantity['ResTicketingRestrictions'][$x], -10) . "HRS";
                                }
    							else
    							{
    								if (strpos($PassengerTypeQuantity['ResTicketingRestrictions'][$x], "GUARANTEED FARE APPL IF PURCHASED BEFORE") !== false) 
    								{   
    								}
    								else
    								{
    								}
    							}
                        	}
                    	}
                    }
            	}
        	}
			
        	$i = $i + 1;
    	}
	}

    function Asientos($frequent_fly)
    {
        global $viajero_frecuente;
        if(array_key_exists('MembershipID', $frequent_fly['CustLoyalty-ATTR']))
        {
            $flys[0] = $frequent_fly['CustLoyalty-ATTR'];
            $j=1;
            for($i=0; $i<count($frequent_fly['CustLoyalty-ATTR']); $i++)
            {
                if(array_key_exists($i, $frequent_fly['CustLoyalty-ATTR']))
                {
                    $flys[$j] = $frequent_fly['CustLoyalty-ATTR'][$i];
                    $j++;
                }
            }
        }
        else
        {
            $flys= $frequent_fly['CustLoyalty-ATTR'];
        }
        
        for($i=0; $i<count($flys); $i++)
        {
            if(array_key_exists('MembershipID', $flys[$i]))
            {
                $number = explode('.', $flys[$i]['NameNumber']);
                $index = (int)$number[0];
                $viajero_frecuente[$index][]=$flys[$i]['TravelingCarrierCode'] . " " . $flys[$i]['MembershipID'];
            }
        }
    }

    function BoletoAnulado($nro_tkt)
    {
        global $t_Ticketing;
        foreach ($t_Ticketing as $valor) 
        {
            if(is_array($valor))
            {
                if(array_key_exists('eTicketNumber', $valor))
                {
                    $cadena = $valor['eTicketNumber'];
                    if(strpos($cadena, $nro_tkt)!==false && strpos($cadena, '*VOID*')!==false)
                        return true;
                }
            }
        }    
        return false;
    }

    function ObtenerComida($codigo)
    {
        $comida = "";
        switch($codigo)
        {
            case 'P':   $comida = 'Alcoholic beverages for purchase'; break;
            case 'B':   $comida = 'Breakfast'; break;
            case 'O':   $comida = 'Cold meal'; break;
            case 'C':   $comida = 'Complimentary alcoholic beverages'; break;
            case 'K':   $comida = 'Continental Breakfast'; break;
            case 'D':   $comida = 'Dinner'; break;
            case 'F':   $comida = 'Food for purchase'; break;
            case 'G':   $comida = 'Food - Beverage / for purchase'; break;
            case 'H':   $comida = 'Hot Meal';
            case 'G':   $comida = 'Food and Beverage / for purchase'; break;
            case 'L':   $comida = 'Lunch'; break;
            case 'M':   $comida = 'Meal'; break;
            case 'N':   $comida = 'No meal service'; break;
            case 'R':   $comida = 'Refreshment / complimentary'; break;
            case 'V':   $comida = 'Refreshment / for purchase'; break;
            case 'S':   $comida = 'Snack'; break;
        }
        return $comida;
    }

?>

<?php include 'footer.php'; ?>