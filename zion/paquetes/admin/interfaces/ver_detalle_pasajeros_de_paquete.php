<?php
	include('header.php');
	include('../entidad/aeropuerto.php');
	include('../entidad/boleto_cotizado.php');
	include('../entidad/cotizacion.php');
	include('../entidad/lineas_aereas.php');
	include("../entidad/pax.php");
	include('../entidad/pqt_otro_servicio.php');
	include('../entidad/tarifa_hotel.php');
	include('../entidad/tarifa_otros.php');
	include('../entidad/telefono.php');
	include('../entidad/e_mails.php');
	include('../entidad/clientes.php');
	include('../entidad/inscripcion_evento.php');
	include("../control/c_mostrar_cotizacion.php");
	include('../control/c_mostrar_cotizacion_paquete.php');

	$idcotizacion = $_GET['cotizacion'];

	$c_mostrar_cotizacion_paquete 	= new c_mostrar_cotizacion_paquete;
	$c_mostrar_cotizacion 	= new c_mostrar_cotizacion;

	$info_cotizacion = $c_mostrar_cotizacion_paquete->obtener_info_cotizacion($idcotizacion);
	$info_cotizacion = $info_cotizacion[0];

	$a_paxs = c_mostrar_cotizacion::obtener_paxs($idcotizacion);

	$publicacion 	= $c_mostrar_cotizacion_paquete->obtener_publicacion_de_cotizacion($idcotizacion);
	$publicacion 	= $publicacion[0];
?>

	<div class="view_cotizacion">

		<div class="columns sixteen encabezado">
			<?php
				if($publicacion['imagen_tercera'] != '')
				{
				?>
					<img src="<?php echo $ruta_imagen . $publicacion['imagen_tercera']; ?>" style="width:100%; border-radius:10px;" />
					<div class="clear_mayor"></div>
				<?php
				}
			?>
		</div>

		<h4 style="text-align:center;"><?php echo $info_cotizacion['titulo']; ?></h4>

		<div class="columns sixteen encabezado" style="margin-left:8px; border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1; padding-top:10px; padding-bottom:10px;">

			<div class="columns two" style="font-weight:bold; color:#10689b;">
				COTIZACION
			</div>
			<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
			<div class="columns twelve">
				<span>
					<span><?php echo $info_cotizacion["idcotizacion"]; ?></span>
				</span>
			</div>

			<div class="clear_mayor"></div>

			<div class="columns two" style="font-weight:bold; color:#10689b;">
				CLIENTE
			</div>
			<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
			<div class="columns twelve">
				<span>
					<div onmouseover="$('.contact_cliente').show();" onmouseout="$('.contact_cliente').hide();">
						<?php echo strtoupper($info_cotizacion["nombre_cliente"] . ' ' . $info_cotizacion["apellido_cliente"]); ?>
						<img title="Informacion de Contacto" src="images/contactcard32.png" class="contact_cliente" style="cursor:pointer; width:15px; height:15px; display:none;" onclick="$('#info_contact_cliente').toggle();" />
						<a href="../../crm/interfaces/iu_mostrar_cliente.php?idclientes=<?php echo $info_cotizacion['idclientes']; ?>" target="_blank">
							<img title="Ver en CRM" src="images/user32.png"  class="contact_cliente" style="cursor:pointer; width:15px; height:15px; display:none;" />
						</a>
						<div id="info_contact_cliente" style="position:absolute; border:3px solid #AAA; background-color:#FFF; display:none;">
							<img src="images/cancel.png" style="width:15px; height:15px; margin-left:640px; margin-top:2px; position:absolute;" onclick="$('#info_contact_cliente').hide();" />
							<div class="four columns">
								<strong style="color:#10689b;">TELEFONOS</strong>
								<ul class="lista_descripcion">
									<?php
										$telefonos = $c_mostrar_cotizacion_paquete->obtener_telefonos_de_cliente($info_cotizacion['idclientes']);
										for ($i=0; $i < count($telefonos); $i++) 
										{ 
										?>
											<li>
												<a><?php echo $telefonos[$i]['numero_telefono'] . ' - ' . strtoupper($telefonos[$i]['tipo_numero_telefono']); ?></a>
											</li>
										<?php
										}
									?>
								</ul>
							</div>
							<div class="seven columns">
								<strong style="color:#10689b;">CORREOS</strong>
								<ul class="lista_descripcion">
									<?php
										$correos = $c_mostrar_cotizacion_paquete->obtener_correos_de_cliente($info_cotizacion['idclientes']);
										for ($i=0; $i < count($correos); $i++) 
										{ 
										?>
											<li>
												<a><?php echo $correos[$i]['e_mail'] . ' - ' . strtoupper($correos[$i]['e_mail_tipo']); ?></a>
											</li>
										<?php
										}
									?>
								</ul>
							</div>
						</div>
					</div>
				</span>
			</div>

			<div class="clear_mayor"></div>

			<?php
				$solicitantes = $c_mostrar_cotizacion_paquete->obtener_solicitantes_de_cotizacion($info_cotizacion['idcotizacion']);
				if(count($solicitantes) > 0 )
				{
				?>
					<div class="columns two" style="font-weight:bold; color:#10689b;">
						SOLICITANTE(S)
					</div>
					<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
					<div class="columns four" >
						<div class="columns twelve" style="margin-left:0px; margin-right:10px;">
							<?php 
								for($i = 0; $i < count($solicitantes); $i++)
								{
									$nombre 		= $solicitantes[$i]["nombre_cliente"] . ' ' . $solicitantes[$i]["apellido_cliente"];
									$idclientes 	= $solicitantes[$i]["idclientes"];
							?>
								<div onmouseover="$('.contact_cliente<?php echo $i; ?>').show();" onmouseout="$('.contact_cliente<?php echo $i; ?>').hide();">
									<?php echo strtoupper($nombre); ?>
									<img title="Informacion de Contacto" src="images/contactcard32.png" class="contact_cliente<?php echo $i; ?>" style="cursor:pointer; width:15px; height:15px; display:none;" onclick="$('#info_contact_cliente<?php echo $i; ?>').toggle();" />
									<a href="../../crm/interfaces/iu_mostrar_cliente.php?idclientes=<?php echo $idclientes; ?>" target="_blank">
										<img title="Ver en CRM" src="images/user32.png" class="contact_cliente<?php echo $i; ?>" style="cursor:pointer; width:15px; height:15px; display:none;" />
									</a>
									<div id="info_contact_cliente<?php echo $i; ?>" style="position:absolute; border:3px solid #AAA; background-color:#FFF; display:none;">
										<img src="images/cancel.png" style="width:15px; height:15px; margin-left:640px; margin-top:2px; position:absolute;" onclick="$('#info_contact_cliente<?php echo $i; ?>').hide();" />
										<div class="four columns">
											<strong style="color:#10689b;">TELEFONOS</strong>
											<ul class="lista_descripcion">
												<?php
													$telefonos = $c_mostrar_cotizacion_paquete->obtener_telefonos_de_cliente($idclientes);
													for ($i=0; $i < count($telefonos); $i++) 
													{ 
													?>
														<li>
															<a><?php echo $telefonos[$i]['numero_telefono'] . ' - ' . strtoupper($telefonos[$i]['tipo_numero_telefono']); ?></a>
														</li>
													<?php
													}
												?>
											</ul>
										</div>
										<div class="seven columns">
											<strong style="color:#10689b;">CORREOS</strong>
											<ul class="lista_descripcion">
												<?php
													$correos = $c_mostrar_cotizacion_paquete->obtener_correos_de_cliente($idclientes);
													for ($i=0; $i < count($correos); $i++) 
													{ 
													?>
														<li>
															<a><?php echo $correos[$i]['e_mail'] . ' - ' . strtoupper($correos[$i]['e_mail_tipo']); ?></a>
														</li>
													<?php
													}
												?>
											</ul>
										</div>
									</div>
								</div>
							<?php
								}
							?>
						</div>
					</div>
				<?php
				}
			?>
			
		</div>

		<div class="clear_mayor"></div>

		<div class="columns sixteen" style="margin-left:8px; border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1; padding-top:10px; padding-bottom:10px;">

			<div class="columns two" style="font-weight:bold; color:#10689b;">
				CREACION
			</div>
			<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
			<div class="columns four">
				<span>
					<span><?php echo date('d/m/Y H:i:s', strtotime($info_cotizacion["fecha_creacion"])); ?></span>
				</span>
			</div>

			<div class="columns two" style="font-weight:bold; color:#10689b;">
				USUARIO
			</div>
			<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
			<div class="columns four">
				<span>
					<span><?php echo strtoupper($info_cotizacion["nombre_usuario"]); ?></span>
				</span>
			</div>

		</div>

		<div class="clear_mayor"></div>

		<div style="border:1px solid #AAAAAA; border-radius:10px; background-color:#F1F1F1;" class="sixteen columns costos_resumido">

			<div class="columns ten" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">RESUMEN DE COSTOS POR PASAJERO</div>

			<table style="width:90%; font-size:8pt; margin-left:2%;" class="tabla-precios">
				<tr style="background-color:#10689b; color:#FFFFFF;">
					<th rowspan="2">NRO</th>
					<th rowspan="2">PASAJERO</th>
					<th colspan="5">COSTOS EN $US</th>
				</tr>
				<tr style="background-color:#10689b; color:#FFFFFF;">
					<th class="mlin">BOLETO<br/>AEREO</th>
					<th class="mlin">PAQUETE<br/>TERRESTRE</th>
					<th class="mlin">COSTO<br/>PAQUETE</th>
					<th>INSCRIPCION</th>
					<th>SUBTOTAL</th>
				</tr>
				<?php
					$total_costos 		= 0;

					$total_costos_paq_terrestre = 0;
					$total_costos_aereo 		= 0;
					$total_costos_inscripciones = 0;

					$subtotal_costos 	= 0;

					$subtotal_costos_paq_terrestre 	= 0;
					$subtotal_costos_aereo 			= 0;
					$subtotal_costos_inscripciones 	= 0;

					$servicios_x_pax 	= array();
					$ciudad_actual 		= '';
					
					for ($i=0; $i < count($a_paxs); $i++) 
					{ 
						$pax 	= $a_paxs[$i];
						$idpax 	= $pax['idpax'];

						$costo_hoteles 			= c_mostrar_cotizacion::obtener_detalle_hotel_por_pax($idpax);
						$costo_servicios 		= c_mostrar_cotizacion::obtener_detalle_servicios_por_pax($idpax);
						$costo_boletos 			= c_mostrar_cotizacion::obtener_detalle_boletos_por_pax($idpax);
						$costo_inscripciones 	= c_mostrar_cotizacion::obtener_detalle_inscripciones_por_pax($idpax);

						if($ciudad_actual != $pax['idciudad_origen_paquete'] || $ciudad_actual == '')
						{
						?>
							<tr>
								<td colspan="7" class="c_origen">
									<strong class="twl_blue">SALE DESDE <?php echo strtoupper($c_mostrar_cotizacion_paquete->obtener_aeropuerto($pax['ciudad'])); ?></strong>
								</td>
							</tr>
						<?php
							$ciudad_actual = $pax['idciudad_origen_paquete'];
						}

						$servicios_x_pax[$i] = array('Hotel' => $costo_hoteles, 'Servicios' => $costo_servicios, 'Boletos' => $costo_boletos, 'Inscripciones' => $costo_inscripciones);

					?>
						<tr>
							<td><?php echo ($i+1); ?></td>
							<td><?php echo strtoupper($pax['nombre_cliente'] . ' ' . $pax['apellido_cliente']); ?></td>
							<td class="sright">
								<label onclick="$('#boleto_pax_<?php echo $i; ?>').show();"><?php echo $costo_boletos['precio_total_pax']; ?></label>
								<div id="boleto_pax_<?php echo $i; ?>" style="z-index:999; position:absolute; padding:10px; background-color:#FFF; border:1px solid #AAA; border-radius:10px; display:none;">
									<img src="images/cancel.png" style="width:15px; height:15px; right:2px; top:2px; position:absolute;" onclick="$('#boleto_pax_<?php echo $i; ?>').hide();" />
									<?php
										if(array_key_exists(0, $costo_boletos))
										{
										?>
											<table>
												<tr style="background-color:#10689b; color:#FFFFFF;">
													<th>LINEA</th>
													<th>RUTA</th>
													<th>PAX</th>
													<th>T LIMIT</th>
												</tr>
											<?php
												for ($j=0; array_key_exists($j, $costo_boletos); $j++) 
												{ 
													$boleto = $costo_boletos[$j];
												?>
													<tr>
														<td><?php echo $boleto['nombre_linea_aerea'] . ' (' . $boleto['codigo_iata_linea_aerea'] . ')'; ?></td>
														<td><?php echo $boleto['ruta']; ?></td>
														<td><?php echo $boleto['tarifa']; ?></td>
														<td><?php echo date('d/m/Y', strtotime($boleto['time_limit'])); ?></td>
													</tr>
												<?php
												}
											?>
											</table>
										<?php
										}
										else
										{
										?>
										 <strong>NO INGRESADO</strong>
										<?php
										}
									?>	
								</div>
								<?php
									$total_costos_aereo 	+= $costo_boletos['precio_total_pax'];
									$subtotal_costos_aereo 	+= $costo_boletos['precio_total_pax'];
								?>
							</td>
							<td class="sright">
								<label onclick="$('#hotel_pax_<?php echo $i; ?>').show();"><?php echo $costo_hoteles['precio_total_pax'] + $costo_servicios['precio_total_pax']; ?></label>
								<div id="hotel_pax_<?php echo $i; ?>" style="z-index:999; position:absolute; padding:10px; background-color:#FFF; border:1px solid #AAA; border-radius:10px; display:none;">
									<img src="images/cancel.png" style="width:15px; height:15px; right:2px; top:2px; position:absolute;" onclick="$('#hotel_pax_<?php echo $i; ?>').hide();" />
									<?php
										if(array_key_exists(0, $costo_hoteles))
										{
										?>
											<table>
												<tr style="background-color:#10689b; color:#FFFFFF;">
													<th>HOTEL</th>
													<th>IN</th>
													<th>OUT</th>
													<th>HAB.</th>
												</tr>
											<?php
												for ($j=0; array_key_exists($j, $costo_hoteles); $j++) 
												{ 
													$hotel = $costo_hoteles[$j];
												?>
													<tr>
														<td><?php echo $hotel['nombre_hotel'] . '-' . $hotel['categoria'] . '*' . ' (' . $hotel['destino'] . ')'; ?></td>
														<td><?php echo date('d/m/Y', strtotime($hotel['fecha_in'])); ?></td>
														<td><?php echo date('d/m/Y', strtotime($hotel['fecha_out'])); ?></td>
														<td><?php echo $hotel['habitacion']; ?></td>
													</tr>
												<?php
												}
											?>
											</table>
										<?php
										}
										else
										{
										?>
										 <strong>HOTELES NO INGRESADO</strong><br/>
										<?php
										}
									?>
									<?php
										if(array_key_exists(0, $costo_servicios))
										{
										?>
											<table>
												<tr style="background-color:#10689b; color:#FFFFFF;">
													<th>SERVICIO</th>
													<th>DESCRIPCION</th>
												</tr>
											<?php
												for ($j=0; array_key_exists($j, $costo_servicios); $j++) 
												{ 
													$servicio = $costo_servicios[$j];
												?>
													<tr>
														<td><?php echo $servicio['nombre_servicio']; ?></td>
														<td><?php echo $servicio['descripcion']; ?></td>
													</tr>
												<?php
												}
											?>
											</table>
										<?php
										}
										else
										{
										?>
										 <strong>SERVICIOS NO INGRESADO</strong>
										<?php
										}
									?>	
								</div>
								<?php
									$total_costos_paq_terrestre 	+= $costo_hoteles['precio_total_pax'] + $costo_servicios['precio_total_pax'];
									$subtotal_costos_paq_terrestre 	+= $costo_hoteles['precio_total_pax'] + $costo_servicios['precio_total_pax'];
								?>
							</td>
							<td class="sright bgreen">
								<label><?php echo $costo_boletos['precio_total_pax'] + $costo_hoteles['precio_total_pax'] + $costo_servicios['precio_total_pax']; ?></label>
							</td>
							<td class="sright">
								<label onclick="$('#inscripcion_pax_<?php echo $i; ?>').show();"><?php echo $costo_inscripciones['precio_total_pax']; ?></label>
								<div id="inscripcion_pax_<?php echo $i; ?>" style="z-index:999; position:absolute; padding:10px; background-color:#FFF; border:1px solid #AAA; border-radius:10px; display:none;">
									<img src="images/cancel.png" style="width:15px; height:15px; right:2px; top:2px; position:absolute;" onclick="$('#inscripcion_pax_<?php echo $i; ?>').hide();" />
									<?php
										if(array_key_exists(0, $costo_inscripciones))
										{
										?>
											<table>
												<tr style="background-color:#10689b; color:#FFFFFF;">
													<th>DESCRIPCION</th>
													<th>T LIMIT</th>
												</tr>
											<?php
												for ($j=0; array_key_exists($j, $costo_inscripciones); $j++) 
												{ 
													$inscripcion = $costo_inscripciones[$j];
												?>
													<tr>
														<td><?php echo $inscripcion['categoria']; ?></td>
														<td><?php echo date('d/m/Y', strtotime($inscripcion['time_limit'])); ?></td>
													</tr>
												<?php
												}
											?>
											</table>
										<?php
										}
										else
										{
										?>
										 <strong>NO INGRESADO</strong>
										<?php
										}
									?>	
								</div>
								<?php
									$total_costos_inscripciones 	+= $costo_inscripciones['precio_total_pax'];
									$subtotal_costos_inscripciones 	+= $costo_inscripciones['precio_total_pax'];
								?>
							</td>
							<td class="sright byellow">
								<label>
									<?php 
										$costo_pax 		= $costo_hoteles['precio_total_pax'] + $costo_servicios['precio_total_pax'] + $costo_boletos['precio_total_pax'] + $costo_inscripciones['precio_total_pax']; 
										echo $costo_pax;
										$total_costos 	+= $costo_pax;
										$subtotal_costos+= $costo_pax;
									?>
								</label>
							</td>
						</tr>
					<?php
						if($i + 1 > count($a_paxs) || $a_paxs[$i+1]['idciudad_origen_paquete'] != $ciudad_actual)
						{
						?>
							<tr class="bturkey">
								<td colspan="2">
									<strong>TOTAL DESDE <?php echo strtoupper($c_mostrar_cotizacion_paquete->obtener_aeropuerto($pax['ciudad'])); ?></strong>
								</td>
								<td class="sright"><label><?php echo $subtotal_costos_aereo; ?></label></td>
								<td class="sright"><label><?php echo $subtotal_costos_paq_terrestre; ?></label></td>
								<td class="sright"><label><?php echo $subtotal_costos_paq_terrestre + $subtotal_costos_aereo; ?></label></td>
								<td class="sright"><label><?php echo $subtotal_costos_inscripciones; ?></label></td>
								<td class="sright"><label><?php echo $subtotal_costos; ?></label></td>
							</tr>
						<?php
							$subtotal_costos 				= 0;
							$subtotal_costos_paq_terrestre 	= 0;
							$subtotal_costos_aereo 			= 0;
							$subtotal_costos_inscripciones 	= 0;
						}
					}
				?>
				<tr class="strng">
					<td colspan="2"><label class="twl_blue">TOTAL COSTOS</label></td>
					<td class="sright"><label class="twl_blue"><?php echo $total_costos_aereo; ?></label></td>
					<td class="sright"><label class="twl_blue"><?php echo $total_costos_paq_terrestre; ?></label></td>
					<td class="sright"><label class="twl_blue"><?php echo $total_costos_paq_terrestre + $total_costos_aereo; ?></label></td>
					<td class="sright"><label class="twl_blue"><?php echo $total_costos_inscripciones; ?></label></td>
					<td class="sright"><label class="twl_blue"><?php echo $total_costos; ?></label></td>
				</tr>
			</table>

			<div class="clear_mayor"></div>

		</div>

		<div class="clear_mayor"></div>

		<div style="border:1px solid #AAAAAA; border-radius:10px; background-color:#F1F1F1;" class="sixteen columns descripcion_servicios_por_pax">

			<div class="columns ten" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">DESCRIPCION DE SERVICIOS POR PASAJERO</div>

			<div class="fifteen columns">
				<?php
					$ciudad_actual 		= '';
					for ($i=0; $i < count($a_paxs); $i++) 
					{
						$pax 					= $a_paxs[$i];
						$servicios_boleto 		= $servicios_x_pax[$i]['Boletos'];
						$servicios_hotel 		= $servicios_x_pax[$i]['Hotel'];
						$servicios_otros 		= $servicios_x_pax[$i]['Servicios'];
						$servicios_inscripcion 	= $servicios_x_pax[$i]['Inscripciones'];

						if($ciudad_actual != $pax['idciudad_origen_paquete'] || $ciudad_actual == '')
						{
						?>
							<div class="clear"></div>
							<strong style="margin-left:5px; color:#10689b;">SALE DESDE <?php echo strtoupper($c_mostrar_cotizacion_paquete->obtener_aeropuerto($pax['ciudad'])); ?></strong>
							<div class="clear"></div>
						<?php
							$ciudad_actual = $pax['idciudad_origen_paquete'];
						}

					?>
						<strong style="margin-left:15px;"><?php echo strtoupper($pax['trato_cliente'] . ' ' . $pax['nombre_cliente'] . ' ' . $pax['apellido_cliente']); ?></strong>
						<ul class="lista_incluye" style="margin-left:15px;" >
							<?php
								for ($j=0; array_key_exists($j, $servicios_boleto); $j++) 
								{
									$boleto = $servicios_boleto[$j]; 
								?>
									<li>
										<a><strong>BOLETO AEREO</strong> <?php echo strtoupper($boleto['ruta']); ?> CON <?php echo strtoupper($boleto['nombre_linea_aerea']); ?>.</a>
									</li>
								<?php
								}
							?>
							<?php
								for ($j=0; array_key_exists($j, $servicios_hotel); $j++) 
								{
									$hotel 		= $servicios_hotel[$j]; 
									$cant_noch 	= floor( (strtotime($hotel['fecha_out'] . ' 00:00:00') - strtotime($hotel['fecha_in'] . ' 00:00:00')) / 86400);
								?>
									<li>
										<a><strong><?php echo $cant_noch; ?> NOCHES</strong> EN HOTEL <?php echo strtoupper($hotel['nombre_hotel']) . ' - ' . $hotel['categoria'] . '*'; ?> HABITACION <?php echo strtoupper($hotel['habitacion']); ?> EN <?php echo strtoupper($hotel['destino']); ?>. <strong>IN:</strong><?php echo strtoupper(date('d/M/Y', strtotime($hotel['fecha_in'] . ' 00:00:00'))); ?> <strong>OUT:</strong><?php echo strtoupper(date('d/M/Y', strtotime($hotel['fecha_out'] . ' 00:00:00'))); ?></a>
									</li>
								<?php
								}
							?>
							<?php
								for ($j=0; array_key_exists($j, $servicios_otros); $j++) 
								{
									$servicio 		= $servicios_otros[$j]; 
								?>
									<li>
										<a><strong><?php echo strtoupper($servicio['nombre_servicio']); ?> </strong><?php echo strtoupper($servicio['descripcion']); ?>.</a>
									</li>
								<?php
								}
							?>
							<?php
								for ($j=0; array_key_exists($j, $servicios_inscripcion); $j++) 
								{
									$inscripcion 		= $servicios_inscripcion[$j]; 
								?>
									<li>
										<a><strong>INSCRIPCION</strong> AL EVENTO <?php echo strtoupper($inscripcion['nombre_evento'] . '(' . $inscripcion['sigla_evento'] . ')'); ?> CATEGORIA <?php echo strtoupper($inscripcion['categoria']); ?>.</a>
									</li>
								<?php
								}
							?>
						</ul>
						<div class="clear"></div>
					<?php
					} 
				?>
			</div>

		</div>

		<div class="clear_mayor"></div>

		<?php
				$ciudades = $c_mostrar_cotizacion_paquete->obtener_ciudades_de_salida($idcotizacion);
		?>

		<div style="border:1px solid #AAAAAA; border-radius:10px; background-color:#F1F1F1;" class="sixteen columns vista_aereo" >

			<div class="is_real">
				<div class="fifteen columns" style="font-weight:bold; color:#10689b; font-size:14pt; margin:10px; text-align:center;">BOLETOS AEREOS</div>
				<?php
					for ($z=0; $z < count($ciudades); $z++) 
					{ 
						$ciudad 			= $ciudades[$z];
						$idciudad_origen 	= $ciudad['idciudad_origen_paquete'];
					?>
						<div class="columns fifteen" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">SALE DESDE <?php echo $c_mostrar_cotizacion_paquete->obtener_aeropuerto($ciudad['ciudad']); ?></div>
						<!-- BOLETOS AEREOS TARIFAS REALES -->
						<?php
							$opciones_aereo = $c_mostrar_cotizacion_paquete->obtener_opcion_boletos_x_ciudad($idciudad_origen);
							if(count($opciones_aereo) > 0)
							{
							?>
								<div class="clear_mayor"></div>
								<div class="columns fifteen">
									
									<div class="clear"></div>
									<?php
										for ($i=0; $i < count($opciones_aereo); $i++) 
										{ 
											$opcion_aereo 	= $opciones_aereo[$i];
											$idopaereo 		= $opcion_aereo['idopcion_boleto'];
										?>
											<?php
												$boletos = $c_mostrar_cotizacion_paquete->obtener_boletos_x_opcion_aereo_real_cotizacion($idopaereo, 1);
												for ($j=0; $j < count($boletos); $j++) 
												{ 
													$boleto_cotizado 	= $boletos[$j];
													$idboleto 			= $boleto_cotizado['idboleto_cotizado'];
													?>
													<div class="res_<?php echo $boleto_cotizado['codigo_reserva']; ?>">
														<center>
															<img src="images/cargando.gif" /><br/>
															Obteniendo Reserva
														</center>
													</div>
													<script type="text/javascript">
														$(function() { 
															obtener_reserva_de_sabre_para_mostrar("<?php echo $boleto_cotizado['codigo_reserva']; ?>"); 
														} )
													</script>
													<?php
												}
											?>
										<?php
										}
									?>
								</div>
							<?php
							}
						?>
					<?php
					}
				?>
			</div>

			<div class="is_real hidden">
				<div class="fifteen columns" style="font-weight:bold; color:#10689b; font-size:14pt; margin:10px; text-align:center;">BOLETOS AEREOS</div>
				<?php
					for ($z=0; $z < count($ciudades); $z++) 
					{ 
						$ciudad 			= $ciudades[$z];
						$idciudad_origen 	= $ciudad['idciudad_origen_paquete'];
					?>
						<div class="columns fifteen" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">SALE DESDE <?php echo $c_mostrar_cotizacion_paquete->obtener_aeropuerto($ciudad['ciudad']); ?></div>
						<!-- BOLETOS AEREOS TARIFAS REALES -->
						<?php
							$opciones_aereo = $c_mostrar_cotizacion_paquete->obtener_opcion_boletos_x_ciudad($idciudad_origen);
							if(count($opciones_aereo) > 0)
							{
							?>
								<div class="clear_mayor"></div>
								<div class="columns fifteen">
									
									<div class="clear"></div>
									<?php
										for ($i=0; $i < count($opciones_aereo); $i++) 
										{ 
											$opcion_aereo 	= $opciones_aereo[$i];
											$idopaereo 		= $opcion_aereo['idopcion_boleto'];
										?>
											<?php
												$boletos = $c_mostrar_cotizacion_paquete->obtener_boletos_x_opcion_aereo_real_cotizacion($idopaereo, 0);
												for ($j=0; $j < count($boletos); $j++) 
												{ 
													$boleto_cotizado 	= $boletos[$j];
													$idboleto 			= $boleto_cotizado['idboleto_cotizado'];
													?>
													<div class="res_<?php echo $boleto_cotizado['codigo_reserva']; ?>">
														<center>
															<img src="images/cargando.gif" /><br/>
															Obteniendo Reserva
														</center>
													</div>
													<script type="text/javascript">
														$(function() { 
															obtener_reserva_de_sabre_para_mostrar("<?php echo $boleto_cotizado['codigo_reserva']; ?>"); 
														} )
													</script>
													<?php
												}
											?>
										<?php
										}
									?>
								</div>
							<?php
							}
						?>
					<?php
					}
				?>
			</div>

		</div>

		<div class="clear_mayor"></div>

		<div style="border:1px solid #AAAAAA; border-radius:10px; background-color:#F1F1F1;" class="sixteen columns vista_hoteles" >

			<div class="is_real">
				<div class="columns five" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">PAQUETE TERRESTRE</div>
				<div class="columns fifteen">
					<table style="width:100%; font-size:8pt; margin-left:3%; border:1px solid #AAA;" class="tabla-precios">
						<tr style="background-color:#10689b; color:#FFFFFF;">
							<th>HOTEL</th>
							<th>IN / OUT</th>
							<!--<th>OUT</th>-->
							<th>NOCHES</th>
							<th class="col_sgl hidden">SIMPLE</th>
							<th class="col_dbl hidden">DOBLE</th>
							<th class="col_tpl hidden">TRIPLE</th>
							<th class="col_cpl hidden">CUADRU</th>
							<th class="col_cnn hidden">MENOR</th>
							<th class="col_inf hidden">INFANTE</th>
							<th>ALIMENTACION</th>
							<th>OBS</th>
							<th class="col_paxs">HAB</th>
							<th style="width:250px;" class="col_paxs">PASAJERO</th>
							<th class="col_paxs">ACOM</th>
						</tr>
						<?php
							$habitaciones = 0;
							for ($z=0; $z < count($ciudades); $z++) 
							{ 
								$ciudad 			= $ciudades[$z];
								$idciudad_origen 	= $ciudad['idciudad_origen_paquete'];

								/*
								* OTROS SERVICIOS 
								*/
								$otras_tarifas = $c_mostrar_cotizacion_paquete->obtener_otros_servicios_x_ciudad_real_cotizacion($idciudad_origen, 1);
								$serv_x_dest 	= array();
								$total_serv_adt = 0;
								$total_serv_cnn = 0;
								$total_serv_inf = 0;
								if(count($otras_tarifas) > 0)
								{
									for ($i=0; $i < count($otras_tarifas); $i++) 
									{ 
										$tarifa_otros 	= $otras_tarifas[$i];
										$idotra_tarifa 	= $tarifa_otros['idtarifa_otros'];

										$ciudad_serv 	= $tarifa_otros['ciudad'];
										
										$total_adt = $tarifa_otros['precio_adulto'];
										$total_cnn = $tarifa_otros['precio_menor'];
										$total_inf = $tarifa_otros['precio_infante'];

										if($tarifa_otros['increment_fee'] == 'P')
										{
											$fee 		= 1+($tarifa_otros['fee'] / 100);
											$total_adt 	*= $fee;
											$total_cnn 	*= $fee;
											$total_inf 	*= $fee;
										}
										else
										{
											$fee = $tarifa_otros['fee'];
											if($total_adt > 0)
												$total_adt += $fee;
											else
												$total_adt = 0;
											if($total_cnn > 0)
												$total_cnn += $fee;
											else
												$total_cnn = 0;
											if($total_inf > 0)
												$total_inf += $fee;
											else
												$total_inf = 0;
										}

										if($tarifa_otros['increment_factura'] == 'P')
										{
											$iva 		= 1+($tarifa_otros['factura'] / 100);
											$total_adt 	*= $iva;
											$total_cnn 	*= $iva;
											$total_inf 	*= $iva;
										}
										else
										{
											$iva = $tarifa_otros['factura'];
											if($total_adt > 0)
												$total_adt += $iva;
											else
												$total_adt = 0;
											if($total_cnn > 0)
												$total_cnn += $iva;
											else
												$total_cnn = 0;
											if($total_inf > 0)
												$total_inf += $iva;
											else
												$total_inf = 0;
										}

										if($tarifa_otros['moneda'] == 'B')
										{
											$tc 		= $tarifa_otros['tipo_cambio'];
											$total_adt /= $tc;
											$total_cnn /= $tc;
											$total_inf /= $tc;
										}

										$total_serv_adt += ceil($total_adt);
										$total_serv_cnn += ceil($total_cnn);
										$total_serv_inf += ceil($total_inf);

										if(!array_key_exists($ciudad_serv, $serv_x_dest))
										{
											$serv_x_dest[$ciudad_serv] = array();
											$serv_x_dest[$ciudad_serv]['adt'] = 0;
											$serv_x_dest[$ciudad_serv]['cnn'] = 0;
											$serv_x_dest[$ciudad_serv]['inf'] = 0;
										}

										$serv_x_dest[$ciudad_serv]['adt'] += ceil($total_adt); 
										$serv_x_dest[$ciudad_serv]['cnn'] += ceil($total_cnn);
										$serv_x_dest[$ciudad_serv]['inf'] += ceil($total_inf);

										$nombre_servicio = $c_mostrar_cotizacion_paquete->obtener_nombre_servicio($tarifa_otros['idotro_servicio']);
									}
								}

								/*
								* HOTELES
								*/
								$tarifas_por_destino =  $c_mostrar_cotizacion_paquete->obtener_destinos_de_hotel($idciudad_origen);
								if(count($tarifas_por_destino) > 0)
								{ 
									for ($i=0; $i < count($tarifas_por_destino); $i++) 
									{ 
										$tarifa_hotel 		= $tarifas_por_destino[$i];
										$idth 				= $tarifa_hotel['idtarifa_hotel'];
										$dest_hotel 		= $tarifa_hotel['destino'];
										$items_tarifa 		= $c_mostrar_cotizacion_paquete->obtener_hoteles_x_destino_real_cotizacion($idth, 1);

										$hide_columns 		= $c_mostrar_cotizacion_paquete->evaluar_precios_hotel($items_tarifa);
										$cant_col 			= count(explode('/', $hide_columns));

										$destino 			= strtoupper($c_mostrar_cotizacion_paquete->obtener_aeropuerto($tarifa_hotel['destino']));

										for ($j=0; $j < count($items_tarifa); $j++) 
										{ 
											$item_tarifa 	= $items_tarifa[$j];
											$iditem 		= $item_tarifa['iditem_hotel'];

											/*
											* SE CALCULA LOS PRECIOS
											*/

											$cant_noches 	= floor( (strtotime($item_tarifa['fecha_out'] . ' 00:00:00') - strtotime($item_tarifa['fecha_in'] . ' 00:00:00')) / 86400);
												
											$total_sgl 	= $item_tarifa['precio_single'];
											$total_dbl 	= $item_tarifa['precio_doble'];
											$total_tpl 	= $item_tarifa['precio_triple'];
											$total_cpl 	= $item_tarifa['precio_cuadruple'];
											$total_cnn 	= $item_tarifa['precio_menor'];
											$total_inf 	= $item_tarifa['precio_infante'];

											if($item_tarifa['increment_fee'] == 'P')
											{
												$fee 		= 1 + ($item_tarifa['fee']/100);
												$total_sgl 	*= $fee;
												$total_dbl 	*= $fee;
												$total_tpl 	*= $fee;
												$total_cpl 	*= $fee;
												$total_cnn 	*= $fee;
												$total_inf 	*= $fee;
											}
											else
											{
												$fee 		= $item_tarifa['fee'];
												if($item_tarifa['precio_single'] > 0)
													$total_sgl 	+= $fee;
												else
													$total_sgl 	= 0;
												if($item_tarifa['precio_doble'] > 0)
													$total_dbl 	+= $fee;
												else
													$total_dbl 	= 0;
												if($item_tarifa['precio_triple'] > 0)
													$total_tpl 	+= $fee;
												else
													$total_tpl 	= 0;
												if($item_tarifa['precio_cuadruple'] > 0)
													$total_cpl 	+= $fee;
												else
													$total_cpl 	= 0;
												if($item_tarifa['precio_menor'] > 0)
													$total_cnn 	+= $fee;
												else
													$total_cnn 	= 0;
												if($item_tarifa['precio_infante'] > 0)
													$total_inf 	+= $fee;
												else
													$total_inf 	= 0;
											}

											if($item_tarifa['increment_factura'] == 'P')
											{
												$iva 		= 1 + ($item_tarifa['factura']/100);
												$total_sgl 	*= $iva;
												$total_dbl 	*= $iva;
												$total_tpl 	*= $iva;
												$total_cpl 	*= $iva;
												$total_cnn 	*= $iva;
												$total_inf 	*= $iva;
											}
											else
											{
												$iva 		= $item_tarifa['factura'];
												if($item_tarifa['precio_single'] > 0)
													$total_sgl 	+= $iva;
												else
													$total_sgl 	= 0;
												if($item_tarifa['precio_doble'] > 0)
													$total_dbl 	+= $iva;
												else
													$total_dbl 	= 0;
												if($item_tarifa['precio_triple'] > 0)
													$total_tpl 	+= $iva;
												else
													$total_tpl 	= 0;
												if($item_tarifa['precio_cuadruple'] > 0)
													$total_cpl 	+= $iva;
												else
													$total_cpl 	= 0;
												if($item_tarifa['precio_menor'] > 0)
													$total_cnn 	+= $iva;
												else
													$total_cnn 	= 0;
												if($item_tarifa['precio_infante'] > 0)
													$total_inf 	+= $iva;
												else
													$total_inf 	= 0;
											}

											if($item_tarifa['moneda'] == 'B')
											{
												$tc 		= $item_tarifa['tipo_cambio'];
												$total_sgl /= $tc;
												$total_dbl /= $tc;
												$total_tpl /= $tc;
												$total_cpl /= $tc;
												$total_cnn /= $tc;
												$total_inf /= $tc;
											}

											$total_sgl 	= ceil($total_sgl);
											$total_dbl 	= ceil($total_dbl);
											$total_tpl 	= ceil($total_tpl);
											$total_cpl 	= ceil($total_cpl);
											$total_cnn 	= ceil($total_cnn);
											$total_inf 	= ceil($total_inf);

											/*
											* OBTIENE LOS PASAJEROS
											*/

											$paxs 		= $c_mostrar_cotizacion->obtener_paxs_segun_hotel($item_tarifa['iditem_hotel']);
											
											if(count($paxs) == 0)
												$rowspan = 1;
											else
												$rowspan = count($paxs);
										?>
											<tr>
												<td rowspan="<?php echo $rowspan; ?>" style="line-height:1; padding:3px;">
													<strong><?php echo $item_tarifa['nombre_hotel'] . ' - ' . $item_tarifa['categoria'] . '*'; ?></strong>
													<br/>
													<?php echo $destino; ?>
												</td>
												<td rowspan="<?php echo $rowspan; ?>" style="line-height:1; padding:3px;">
													<strong>IN:</strong><?php echo date('d/m/Y', strtotime($item_tarifa['fecha_in'])); ?>
													<br/>
													<strong>OUT:</strong><?php echo date('d/m/Y', strtotime($item_tarifa['fecha_out'])); ?>
												</td>
												<td rowspan="<?php echo $rowspan; ?>" ><?php echo $cant_noches; ?></td>

												<!--<td rowspan="<?php echo $rowspan; ?>"><?php echo date('d/m/Y', strtotime($item_tarifa['fecha_out'])); ?></td>-->
												<td rowspan="<?php echo $rowspan; ?>" style="text-align:right;" class="col_sgl hidden"><?php echo (($cant_noches * $total_sgl) + $serv_x_dest[$dest_hotel]['adt']) . ' $US'; ?></td>
												<td rowspan="<?php echo $rowspan; ?>" style="text-align:right;" class="col_dbl hidden"><?php echo (($cant_noches * $total_dbl) + $serv_x_dest[$dest_hotel]['adt']) . ' $US'; ?></td>
												<td rowspan="<?php echo $rowspan; ?>" style="text-align:right;" class="col_tpl hidden"><?php echo (($cant_noches * $total_tpl) + $serv_x_dest[$dest_hotel]['adt']) . ' $US'; ?></td>
												<td rowspan="<?php echo $rowspan; ?>" style="text-align:right;" class="col_cpl hidden"><?php echo (($cant_noches * $total_cpl) + $serv_x_dest[$dest_hotel]['adt']) . ' $US'; ?></td>
												<td rowspan="<?php echo $rowspan; ?>" style="text-align:right;" class="col_cnn hidden"><?php echo (($cant_noches * $total_cnn) + $serv_x_dest[$dest_hotel]['cnn']) . ' $US'; ?></td>
												<td rowspan="<?php echo $rowspan; ?>" style="text-align:right;" class="col_inf hidden"><?php echo (($cant_noches * $total_inf) + $serv_x_dest[$dest_hotel]['inf']) . ' $US'; ?></td>
												<td rowspan="<?php echo $rowspan; ?>" ><?php echo strtoupper($item_tarifa['alimentacion']); ?></td>
												<td rowspan="<?php echo $rowspan; ?>"><?php echo strtoupper($item_tarifa['info_extra']); ?></td>
													<?php
														if(count($paxs) > 0)
														{
															$nro_hab 	= '';
															for ($a=0; $a < count($paxs); $a++) 
															{ 
															
																$pax 	= $paxs[$a];
																$id  	= $pax['idpax'] . '_' . $item_tarifa['iditem_hotel'];
																
																switch ($pax['tipo_precio']) 
																{
																	case 1: $hab='SIMPLE'; break;
																	case 2: $hab='DOBLE'; break;
																	case 3: $hab='TRIPLE'; break;
																	case 4: $hab='CUADRUPLE'; break;
																	case 5: $hab='MENOR'; break;
																	case 6: $hab='INFANTE'; break;
																	default:$hab='N/N'; break;
																}

																// VERIFICAMOS LA HABITACION
																if($nro_hab != $pax['habitacion']) 
																{  
																	$nro_hab 	= $pax['habitacion'];
																	$sw 		= true;
																	$habitaciones++;
																}
																else
																	$sw = false;
																
																if($a!=0)
																{
																?>
																	<tr>
																<?php
																}
																?>
																		<td style="<?php if($sw) echo 'border-top:1px solid #AAA; border-bottom:none;";'; else echo 'border-top:none; border-bottom:none;'; ?>" class="col_paxs" ><?php if($sw) echo $habitaciones; ?></td>
																		<td class="col_paxs"><?php echo $pax['nombre_cliente'] . ' ' . $pax['apellido_cliente']; ?></td>
																		<td class="col_paxs"><?php echo $hab; ?></td>
																	</tr>
															<?php
															}
														}
														else
														{
														?>
																<td class="col_paxs" colspan="3">NO INGRESADO</td>
															</tr>
														<?php
														}
													?>
										<?php
										}
									}
								}
							}
						?>
					</table>

					<div class="clear_mayor"></div>
				</div>
			</div>

			<div class="is_real hidden">
				<div class="columns five" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">PAQUETE TERRESTRE</div>
				<div class="columns fifteen">
					<ul class="lista_incluye" style="margin-left:30px;" >
						<li style="list-style:none; font-weight:bold;">INCLUYE:</li>
						<?php
							$serv_x_dest 	= array();
							for ($z=0; $z < count($ciudades); $z++) 
							{ 
								$ciudad 			= $ciudades[$z];
								$idciudad_origen 	= $ciudad['idciudad_origen_paquete'];

								/*
								* OTROS SERVICIOS 
								*/
								$otras_tarifas = $c_mostrar_cotizacion_paquete->obtener_otros_servicios_x_ciudad_real_cotizacion($idciudad_origen, 0);
								$total_serv_adt = 0;
								$total_serv_cnn = 0;
								$total_serv_inf = 0;
								if(count($otras_tarifas) > 0)
								{
									for ($i=0; $i < count($otras_tarifas); $i++) 
									{ 
										$tarifa_otros 	= $otras_tarifas[$i];
										$idotra_tarifa 	= $tarifa_otros['idtarifa_otros'];

										$ciudad_serv 	= $tarifa_otros['ciudad'];
										
										$total_adt = $tarifa_otros['precio_adulto'];
										$total_cnn = $tarifa_otros['precio_menor'];
										$total_inf = $tarifa_otros['precio_infante'];

										if($tarifa_otros['increment_fee'] == 'P')
										{
											$fee 		= 1+($tarifa_otros['fee'] / 100);
											$total_adt 	*= $fee;
											$total_cnn 	*= $fee;
											$total_inf 	*= $fee;
										}
										else
										{
											$fee = $tarifa_otros['fee'];
											if($total_adt > 0)
												$total_adt += $fee;
											else
												$total_adt = 0;
											if($total_cnn > 0)
												$total_cnn += $fee;
											else
												$total_cnn = 0;
											if($total_inf > 0)
												$total_inf += $fee;
											else
												$total_inf = 0;
										}

										if($tarifa_otros['increment_factura'] == 'P')
										{
											$iva 		= 1+($tarifa_otros['factura'] / 100);
											$total_adt 	*= $iva;
											$total_cnn 	*= $iva;
											$total_inf 	*= $iva;
										}
										else
										{
											$iva = $tarifa_otros['factura'];
											if($total_adt > 0)
												$total_adt += $iva;
											else
												$total_adt = 0;
											if($total_cnn > 0)
												$total_cnn += $iva;
											else
												$total_cnn = 0;
											if($total_inf > 0)
												$total_inf += $iva;
											else
												$total_inf = 0;
										}

										if($tarifa_otros['moneda'] == 'B')
										{
											$tc 		= $tarifa_otros['tipo_cambio'];
											$total_adt /= $tc;
											$total_cnn /= $tc;
											$total_inf /= $tc;
										}

										$total_serv_adt += ceil($total_adt);
										$total_serv_cnn += ceil($total_cnn);
										$total_serv_inf += ceil($total_inf);

										if(!array_key_exists($ciudad_serv, $serv_x_dest))
										{
											$serv_x_dest[$ciudad_serv] = array();
											$serv_x_dest[$ciudad_serv]['adt'] = 0;
											$serv_x_dest[$ciudad_serv]['cnn'] = 0;
											$serv_x_dest[$ciudad_serv]['inf'] = 0;
										}

										$serv_x_dest[$ciudad_serv]['adt'] += ceil($total_adt); 
										$serv_x_dest[$ciudad_serv]['cnn'] += ceil($total_cnn);
										$serv_x_dest[$ciudad_serv]['inf'] += ceil($total_inf);

										$nombre_servicio = $c_mostrar_cotizacion_paquete->obtener_nombre_servicio($tarifa_otros['idotro_servicio']);
										?>
											<li><a><?php echo $nombre_servicio . ' ' . $tarifa_otros['descripcion'] . ' (' . $ciudad_serv . ')'; ?></a></li>
										<?php
									}
								}
							}
						?>
					</ul>
				</div>
				<div class="columns fifteen">
					<table style="width:100%; font-size:8pt; margin-left:3%; border:1px solid #AAA;" class="tabla-precios">
						<tr style="background-color:#10689b; color:#FFFFFF;">
							<th>HOTEL</th>
							<th>IN / OUT</th>
							<!--<th>OUT</th>-->
							<th>NOCHES</th>
							<th class="col_sgl hidden">SIMPLE</th>
							<th class="col_dbl hidden">DOBLE</th>
							<th class="col_tpl hidden">TRIPLE</th>
							<th class="col_cpl hidden">CUADRU</th>
							<th class="col_cnn hidden">MENOR</th>
							<th class="col_inf hidden">INFANTE</th>
							<th>ALIMENTACION</th>
							<th>OBS</th>
							<th class="col_paxs">HAB</th>
							<th style="width:250px;" class="col_paxs">PASAJERO</th>
							<th class="col_paxs">ACOM</th>
						</tr>
						<?php
							$habitaciones = 0;
							for ($z=0; $z < count($ciudades); $z++) 
							{ 
								$ciudad 			= $ciudades[$z];
								$idciudad_origen 	= $ciudad['idciudad_origen_paquete'];

								/*
								* HOTELES
								*/
								$tarifas_por_destino =  $c_mostrar_cotizacion_paquete->obtener_destinos_de_hotel($idciudad_origen);
								if(count($tarifas_por_destino) > 0)
								{ 
									for ($i=0; $i < count($tarifas_por_destino); $i++) 
									{ 
										$tarifa_hotel 		= $tarifas_por_destino[$i];
										$idth 				= $tarifa_hotel['idtarifa_hotel'];
										$dest_hotel 		= $tarifa_hotel['destino'];
										$items_tarifa 		= $c_mostrar_cotizacion_paquete->obtener_hoteles_x_destino_real_cotizacion($idth, 0);

										$hide_columns 		= $c_mostrar_cotizacion_paquete->evaluar_precios_hotel($items_tarifa);
										$cant_col 			= count(explode('/', $hide_columns));

										$destino 			= strtoupper($c_mostrar_cotizacion_paquete->obtener_aeropuerto($tarifa_hotel['destino']));

										for ($j=0; $j < count($items_tarifa); $j++) 
										{ 
											$item_tarifa 	= $items_tarifa[$j];
											$iditem 		= $item_tarifa['iditem_hotel'];

											/*
											* SE CALCULA LOS PRECIOS
											*/

											$cant_noches 	= floor( (strtotime($item_tarifa['fecha_out'] . ' 00:00:00') - strtotime($item_tarifa['fecha_in'] . ' 00:00:00')) / 86400);
												
											$total_sgl 	= $item_tarifa['precio_single'];
											$total_dbl 	= $item_tarifa['precio_doble'];
											$total_tpl 	= $item_tarifa['precio_triple'];
											$total_cpl 	= $item_tarifa['precio_cuadruple'];
											$total_cnn 	= $item_tarifa['precio_menor'];
											$total_inf 	= $item_tarifa['precio_infante'];

											if($item_tarifa['increment_fee'] == 'P')
											{
												$fee 		= 1 + ($item_tarifa['fee']/100);
												$total_sgl 	*= $fee;
												$total_dbl 	*= $fee;
												$total_tpl 	*= $fee;
												$total_cpl 	*= $fee;
												$total_cnn 	*= $fee;
												$total_inf 	*= $fee;
											}
											else
											{
												$fee 		= $item_tarifa['fee'];
												if($item_tarifa['precio_single'] > 0)
													$total_sgl 	+= $fee;
												else
													$total_sgl 	= 0;
												if($item_tarifa['precio_doble'] > 0)
													$total_dbl 	+= $fee;
												else
													$total_dbl 	= 0;
												if($item_tarifa['precio_triple'] > 0)
													$total_tpl 	+= $fee;
												else
													$total_tpl 	= 0;
												if($item_tarifa['precio_cuadruple'] > 0)
													$total_cpl 	+= $fee;
												else
													$total_cpl 	= 0;
												if($item_tarifa['precio_menor'] > 0)
													$total_cnn 	+= $fee;
												else
													$total_cnn 	= 0;
												if($item_tarifa['precio_infante'] > 0)
													$total_inf 	+= $fee;
												else
													$total_inf 	= 0;
											}

											if($item_tarifa['increment_factura'] == 'P')
											{
												$iva 		= 1 + ($item_tarifa['factura']/100);
												$total_sgl 	*= $iva;
												$total_dbl 	*= $iva;
												$total_tpl 	*= $iva;
												$total_cpl 	*= $iva;
												$total_cnn 	*= $iva;
												$total_inf 	*= $iva;
											}
											else
											{
												$iva 		= $item_tarifa['factura'];
												if($item_tarifa['precio_single'] > 0)
													$total_sgl 	+= $iva;
												else
													$total_sgl 	= 0;
												if($item_tarifa['precio_doble'] > 0)
													$total_dbl 	+= $iva;
												else
													$total_dbl 	= 0;
												if($item_tarifa['precio_triple'] > 0)
													$total_tpl 	+= $iva;
												else
													$total_tpl 	= 0;
												if($item_tarifa['precio_cuadruple'] > 0)
													$total_cpl 	+= $iva;
												else
													$total_cpl 	= 0;
												if($item_tarifa['precio_menor'] > 0)
													$total_cnn 	+= $iva;
												else
													$total_cnn 	= 0;
												if($item_tarifa['precio_infante'] > 0)
													$total_inf 	+= $iva;
												else
													$total_inf 	= 0;
											}

											if($item_tarifa['moneda'] == 'B')
											{
												$tc 		= $item_tarifa['tipo_cambio'];
												$total_sgl /= $tc;
												$total_dbl /= $tc;
												$total_tpl /= $tc;
												$total_cpl /= $tc;
												$total_cnn /= $tc;
												$total_inf /= $tc;
											}

											$total_sgl 	= ceil($total_sgl);
											$total_dbl 	= ceil($total_dbl);
											$total_tpl 	= ceil($total_tpl);
											$total_cpl 	= ceil($total_cpl);
											$total_cnn 	= ceil($total_cnn);
											$total_inf 	= ceil($total_inf);

											/*
											* OBTIENE LOS PASAJEROS
											*/

											$paxs 		= $c_mostrar_cotizacion->obtener_paxs_segun_hotel($item_tarifa['iditem_hotel']);
											
											if(count($paxs) == 0)
												$rowspan = 1;
											else
												$rowspan = count($paxs);
										?>
											<tr>
												<td rowspan="<?php echo $rowspan; ?>" style="line-height:1; padding:3px;">
													<strong><?php echo $item_tarifa['nombre_hotel'] . ' - ' . $item_tarifa['categoria'] . '*'; ?></strong>
													<br/>
													<?php echo $destino; ?>
												</td>
												<td rowspan="<?php echo $rowspan; ?>" style="line-height:1; padding:3px;">
													<strong>IN:</strong><?php echo date('d/m/Y', strtotime($item_tarifa['fecha_in'])); ?>
													<br/>
													<strong>OUT:</strong><?php echo date('d/m/Y', strtotime($item_tarifa['fecha_out'])); ?>
												</td>
												<td rowspan="<?php echo $rowspan; ?>" ><?php echo $cant_noches; ?></td>

												<!--<td rowspan="<?php echo $rowspan; ?>"><?php echo date('d/m/Y', strtotime($item_tarifa['fecha_out'])); ?></td>-->
												<td rowspan="<?php echo $rowspan; ?>" style="text-align:right;" class="col_sgl hidden"><?php echo (($cant_noches * $total_sgl) + $serv_x_dest[$dest_hotel]['adt']) . ' $US'; ?></td>
												<td rowspan="<?php echo $rowspan; ?>" style="text-align:right;" class="col_dbl hidden"><?php echo (($cant_noches * $total_dbl) + $serv_x_dest[$dest_hotel]['adt']) . ' $US'; ?></td>
												<td rowspan="<?php echo $rowspan; ?>" style="text-align:right;" class="col_tpl hidden"><?php echo (($cant_noches * $total_tpl) + $serv_x_dest[$dest_hotel]['adt']) . ' $US'; ?></td>
												<td rowspan="<?php echo $rowspan; ?>" style="text-align:right;" class="col_cpl hidden"><?php echo (($cant_noches * $total_cpl) + $serv_x_dest[$dest_hotel]['adt']) . ' $US'; ?></td>
												<td rowspan="<?php echo $rowspan; ?>" style="text-align:right;" class="col_cnn hidden"><?php echo (($cant_noches * $total_cnn) + $serv_x_dest[$dest_hotel]['cnn']) . ' $US'; ?></td>
												<td rowspan="<?php echo $rowspan; ?>" style="text-align:right;" class="col_inf hidden"><?php echo (($cant_noches * $total_inf) + $serv_x_dest[$dest_hotel]['inf']) . ' $US'; ?></td>
												<td rowspan="<?php echo $rowspan; ?>" ><?php echo strtoupper($item_tarifa['alimentacion']); ?></td>
												<td rowspan="<?php echo $rowspan; ?>"><?php echo strtoupper($item_tarifa['info_extra']); ?></td>
													<?php
														if(count($paxs) > 0)
														{
															$nro_hab 	= '';
															for ($a=0; $a < count($paxs); $a++) 
															{ 
															
																$pax 	= $paxs[$a];
																$id  	= $pax['idpax'] . '_' . $item_tarifa['iditem_hotel'];
																
																switch ($pax['tipo_precio']) 
																{
																	case 1: $hab='SIMPLE'; break;
																	case 2: $hab='DOBLE'; break;
																	case 3: $hab='TRIPLE'; break;
																	case 4: $hab='CUADRUPLE'; break;
																	case 5: $hab='MENOR'; break;
																	case 6: $hab='INFANTE'; break;
																	default:$hab='N/N'; break;
																}

																// VERIFICAMOS LA HABITACION
																if($nro_hab != $pax['habitacion']) 
																{  
																	$nro_hab 	= $pax['habitacion'];
																	$sw 		= true;
																	$habitaciones++;
																}
																else
																	$sw = false;
																
																if($a!=0)
																{
																?>
																	<tr>
																<?php
																}
																?>
																		<td style="<?php if($sw) echo 'border-top:1px solid #AAA; border-bottom:none;";'; else echo 'border-top:none; border-bottom:none;'; ?>" class="col_paxs" ><?php if($sw) echo $habitaciones; ?></td>
																		<td class="col_paxs"><?php echo $pax['nombre_cliente'] . ' ' . $pax['apellido_cliente']; ?></td>
																		<td class="col_paxs"><?php echo $hab; ?></td>
																	</tr>
															<?php
															}
														}
														else
														{
														?>
																<td class="col_paxs" colspan="3">NO INGRESADO</td>
															</tr>
														<?php
														}
													?>
										<?php
										}
									}
								}
							}
						?>
					</table>

					<div class="clear_mayor"></div>
				</div>
			</div>

			<div class="check_vista_ciudades" style="display:none;">
				<div style="padding-left:10px; line-height:1;">
					<?php
						for ($z=0; $z < count($ciudades); $z++) 
						{ 
							$ciudad 			= $ciudades[$z];
						?>
							<input type="checkbox" checked onchange="$('.detalle_hotel_aereo_<?php echo $z; ?>').toggle();" /><?php echo $ciudad['ciudad']; ?><br/>
						<?php
						}
					?>
				</div>
			</div>

			<script type="text/javascript">
				$(function() { $('.vista_detalle_pax').append($('.check_vista_ciudades').html()); });
			</script>
		</div>

		<div class="clear"></div>

		<!-- INSCRIPCIONES -->
		<div class="clear_mayor"></div>
		
		<div style="border:1px solid #AAAAAA; border-radius:10px; background-color:#F1F1F1;" class="sixteen columns inscripciones_a_evento" >
			<div class="columns five" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">INSCRIPCIONES</div>
			<div class="clear"></div>
			<?php
				$inscripciones = $c_mostrar_cotizacion_paquete->obtener_inscripciones_de_evento($idcotizacion);
				
				if(count($inscripciones) > 0)
				{
				?>
					<table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios inscripciones_pax">
						<tr style="background-color:#10689b; color:#FFFFFF;">
							<th>CATEGORIA</th>
							<th>EVENTO</th>
							<th>PASAJERO</th>
							<th>COSTO $US</th>
						</tr>
						<?php 
							for ($i=0; $i < count($inscripciones); $i++) 
							{ 
								$inscripcion 	= $inscripciones[$i];
								$idinscripcion 	= $inscripcion['idinscripcion_evento'];

								$total = $inscripcion['precio'];

								if($inscripcion['increment_fee'] == 'P')
								{
									$fee 	= 1+($inscripcion['fee'] / 100);
									$total 	*= $fee;
								}
								else
								{
									$fee = $inscripcion['fee'];
									if($total > 0)
										$total += $fee;
									else
										$total = 0;
								}

								if($inscripcion['increment_factura'] == 'P')
								{
									$iva 	= 1+($inscripcion['factura'] / 100);
									$total 	*= $iva;
								}
								else
								{
									$iva = $inscripcion['factura'];
									if($total > 0)
										$total += $iva;
									else
										$total = 0;
								}

								if($inscripcion['moneda'] == 'B')
								{
									$tc 	= $inscripcion['tipo_cambio'];
									$total 	/= $tc;
								}

								$paxs 			= $c_mostrar_cotizacion->obtener_paxs_segun_inscripcion($idinscripcion);

								if(count($paxs) == 0)
									$rowspan = 1;
								else
									$rowspan = count($paxs);
							?>
								<tr>
									<td rowspan="<?php echo $rowspan; ?>"><?php echo strtoupper($inscripcion['categoria']); ?></td>
									<td rowspan="<?php echo $rowspan; ?>"><?php echo strtoupper($inscripcion['nombre_evento']); ?></td>
									<?php
										if(count($paxs) > 0)
										{
											for ($j=0; $j < count($paxs); $j++) 
											{ 
												if($j!=0)
												{
												?>
													<tr>
												<?php
												}
												?>
														<td class="col_paxs_insc"><?php echo $paxs[$j]['nombre_cliente'] . ' ' . $paxs[$j]['apellido_cliente']; ?></td>
														<td class="col_paxs_insc"><?php echo $total; ?></td>
													</tr>
											<?php
											}
										}
										else
										{
										?>
												<td class="col_paxs_insc" colspan="2">NO INGRESADO</td>
											</tr>
										<?php
										}
									?>
							<?php
							}
						?>
					</table>
				<?php
				}
			?>
			<div class="clear_mayor"></div>
			<?php
				if($publicacion['imagen_primera'] != '')
				{
				?>
					<div class="fifteen columns inscripcion_imagen" style="margin-left:3%; display:none;">
						<img src="<?php echo $ruta_imagen . $publicacion['imagen_primera']; ?>" style="width:90%; border-radius:10px;" />
						<div class="clear_mayor"></div>
					</div>
				<?php
				}
			?>
		</div>

		<div class="clear_mayor"></div>

	</div>

	<div class="clear"></div>

	<?php include 'vista_detalle_pasajeros_impresion.php'; ?>

	<div class="clear"></div>

	<div class="sixteen columns">
		<input type="button" value="Imprimir" style="float:right;"  onclick="Imprimir();" />
		
		<a href="editar_cotizacion_paquete.php?idpaquete=<?php echo $idcotizacion; ?>" style="float:right;">
			<input type="button" value="Editar Cotizacion" />	
		</a>

		<a href="ver_cotizacion_paquete.php?cotizacion=<?php echo $idcotizacion; ?>" style="float:right;">
			<input type="button" value="Ver Cotizacion" />	
		</a>

		<a href="mostrar_planilla_cotizacion.php?c=<?php echo $idcotizacion; ?>" target="_blank">
			<input type="button" value="Ver Planilla" style="float:right;" />
		</a>
	</div>

	
	<?php include 'footer.php'; ?>