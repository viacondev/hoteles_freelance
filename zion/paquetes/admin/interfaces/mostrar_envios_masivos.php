<?php

include '../BD/controladoraBD.php';
include '../entidad/pqt_envio_masivo.php';

	$idpublicacion 	= $_GET['p'];

	$envios 		= pqt_envio_masivo::obtener_envios_por_publicacion($idpublicacion);

	if($envios === false)
		$envios = array();
?>
<link href="lib/tinybox2/style.css" rel="stylesheet" />
<div class="container" style="height:500px; overflow:auto;" >
	<?php
		for ($i=0; $i < count($envios); $i++) 
		{ 
		 	$envio = $envios[$i];
	?>
			<div class="fifteen columns" style="color:#FFFFFF; background-color:#10689b; padding:10px;">
				<div class="seven columns">
					<strong style="color:#FFFFFF;">FECHA DE ENVIO: </strong><?php echo strtoupper(date('d/M/Y H:i:s', strtotime($envio['fecha_envio']))); ?>
				</div>
				<div class="six columns">
					<strong style="color:#FFFFFF;">USUARIO: </strong><?php echo strtoupper($envio['nombre_usuario']); ?>
				</div>	
			</div>
			
			<div class="clear_mayor"></div>
			<div class="eight columns">
				<strong>CONTENIDO</strong>	
			</div>
			<div class="eight columns">
				<a onclick="$('#ctn<?php echo $envio['idenvio_masivo']; ?>').toggle();" style="cursor:pointer;">Mostrar Contenido</a>
			</div>
			<div id="ctn<?php echo $envio['idenvio_masivo']; ?>" style="display:none;">
				<div class="fifteen columns">
					<?php echo $envio['contenido']; ?>
				</div>	
			</div>
			<div class="clear_mayor"></div>
			<div class="three columns">
				<strong>OBSERVACIONES:</strong>
			</div>
			<div class="eleven columns">
				<?php echo strtoupper(nl2br($envio['observacion'])); ?>
			</div>
			<div class="clear_mayor"></div>
			<div class="seven columns">
				<strong>CLI C/MAIL: </strong><?php echo $envio['cantidad_clientes_con_mail']; ?>
			</div>
			<div class="seven columns">
				<strong>CLI S/MAIL: </strong><?php echo $envio['cantidad_clientes_sin_mail']; ?>
			</div>
			<div class="clear_mayor"></div>
			<div class="seven columns">
				<strong>ENVIOS CORRECTOS: </strong><?php echo $envio['cantidad_envios_correctos']; ?>
			</div>
			<div class="seven columns">
				<strong>ENVIOS INCORRECTOS: </strong><?php echo $envio['cantidad_envios_incorrectos']; ?>
			</div>
			<hr/>
	<?
		} 
	?>
</div>