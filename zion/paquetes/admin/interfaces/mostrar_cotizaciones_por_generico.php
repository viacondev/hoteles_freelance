<?php

	include 'header.php';
	include('../control/c_mostrar_cotizacion.php');
	include('../entidad/publicacion.php');
	include('../entidad/cotizacion.php');
	include('../entidad/clientes.php');
	include('../entidad/pax.php');
	include('../entidad/archivo_adjunto.php');

	include('../entidad/tarifa_hotel.php');
	include('../entidad/tarifa_otros.php');
	include('../entidad/boleto_cotizado.php');
	include('../entidad/inscripcion_evento.php');

	$idpublicacion 	= $_GET['p'];

	$cotizaciones 	= c_mostrar_cotizacion::obtener_cotizaciones_por_publicacion($idpublicacion);

	$info_paquete 	= publicacion::obtener_info_paquete($idpublicacion);

	// OBTENER TODOS LOS PASAJEROS DE TODAS LAS COTIZACIONES DEL PAQUETE

	$ids_cotizaciones = array();
	for ($i=0; $i < count($cotizaciones); $i++) 
	{ 
		$cotizacion 	= $cotizaciones[$i];
		$idcotizacion 	= $cotizacion['idcotizacion'];
		$idclientes 	= $cotizacion['idclientes'];

		$ids_cotizaciones[] = $idcotizacion;

		// INCLUIR NOMBRES DE PASAJEROS

		$paxs 						= c_mostrar_cotizacion::obtener_paxs($idcotizacion);
		$cotizaciones[$i]['paxs'] 	= $paxs;

		// INCLUIR NOMBRE DEL CLIENTE

		$cotizaciones[$i]['nombre_cliente'] = c_mostrar_cotizacion::obtener_nombre_del_cliente($idclientes);

	}

	// OBTENER TELEFONOS Y CORREOS DEL CLIENTE

	$info_contacto = array();
	for ($i=0; $i < count($cotizaciones); $i++) 
	{ 
		$paxs = $cotizaciones[$i]['paxs'];
		for ($j=0; $j < count($paxs); $j++) 
		{ 
			$pax = $paxs[$j];
			$info_contacto[$pax['idclientes']] 				= array();
			$info_contacto[$pax['idclientes']]['telefono'] 	= c_mostrar_cotizacion::obtener_telefonos_x_pax($pax['idclientes']);
			$info_contacto[$pax['idclientes']]['correo'] 	= c_mostrar_cotizacion::obtener_correos_x_pax($pax['idclientes']);
		}
	}

	/*
	* SERVICIOS DE HOTEL
	*/

		// OBTENER TODOS LOS HOTELES Y AGRUPARLOS POR EL DESTINO

		$destinos_hotel 				= c_mostrar_cotizacion::obtener_destinos_hotel_por_rango_cotizaciones(implode(',', $ids_cotizaciones));
		
		$a_servicios_hotel 				= array();
		$t_servicios_hotel 				= array();

		$total_columnas_hoteles_in 		= 0;
		$subtotales_col_dhotel 			= array(); // PARA DESTINOS DE HOTEL
		$subtotales_col_hoteles 		= array(); // PARA NOMBRES DE HOTEL
		$subtotales_col_in 				= array(); // PARA IN DE HOTEL

		$arbol_hoteles = array();

		$cont_hotel = 0;
		$cont_f_in 	= 0;

		for ($i=0; $i < count($destinos_hotel); $i++) 
		{ 
			$destino 	= $destinos_hotel[$i];
			$ndestino 	= $destino['destino']; // NOMBRE DEL DESTINO

			$key_dest 	= $ndestino;

			$a_servicios_hotel[$key_dest] 	= array();

			$total_hoteles_x_destino 		= 0;

			// OBTENER TARIFAS SEGUN EL DESTINO Y ORDEN

			$tarifas = c_mostrar_cotizacion::obtener_items_hotel_x_destino($ndestino, implode(',', $ids_cotizaciones));

			// ORDENAR POR NOMBRE DE HOTEL

			$a_items_hotel = array();
			$t_items_hotel = array();

			// AGRUPAMOS POR HOTEL

			for ($j=0; $j < count($tarifas); $j++) 
			{ 
				$tarifa 	= $tarifas[$j];
				$idhotel 	= $tarifa['idhotel'];  // IDENTIFICADOR DEL HOTEL

				if(!array_key_exists($idhotel, $a_items_hotel))
				{
					$a_items_hotel[$idhotel] 			= array();
					$a_items_hotel[$idhotel]['tarifas'] = array();
					$a_items_hotel[$idhotel]['nombre']  = strtoupper($tarifa['nombre_hotel']) . ' ' .  $tarifa['categoria'] . '*';
				}

				$a_items_hotel[$idhotel]['tarifas'][] = $tarifa;
			}

			// AGRUPAR POR FECHA DE IN DE HOTEL
			
			foreach ($a_items_hotel as $hkey => $hotel) 
			{
				$tarifas = $hotel['tarifas'];
				$namhotel  = $hotel['nombre'];

				$a_items_x_in = array();
				$t_items_x_in = array();

				for ($j=0; $j < count($tarifas); $j++) 
				{ 
					$tarifa 			= $tarifas[$j];
					$tarifa['class'] 	= 'sh dh' . $i . ' hh' . $cont_hotel . ' eh' . $cont_f_in;
					$fechas = date('d/M', strtotime($tarifa['fecha_in'])) . ' ' . date('d/M', strtotime($tarifa['fecha_out']));

					if(!array_key_exists($fechas, $a_items_x_in))
					{
						$a_items_x_in[$fechas] 	= array();
						$subtotales_col_in[] 	= array(0 =>$fechas, 2 =>'sh dh' . $i . ' hh' . $cont_hotel . ' eh' . $cont_f_in);
						$t_items_x_in[] 		= array('name' => $fechas, 'class' => 'eh' . $cont_f_in, 'parent' => 'hh' . $cont_hotel, 'cols' =>1);

						$cont_f_in++;
					}

					$a_items_x_in[$fechas][] 	= $tarifa;
				}

				$a_items_hotel[$hkey] 		= $a_items_x_in;
				$subtotales_col_hoteles[] 	= array(0 =>$namhotel, 1 =>count($a_items_x_in), 2 =>'sh dh' . $i . ' hh' . $cont_hotel);
				$t_items_hotel[] 			= array('name' => $namhotel, 'class' => 'hh' . $cont_hotel, 'parent' => 'dh' . $i, 'childs' => $t_items_x_in, 'cols' =>count($a_items_x_in));

				$total_hoteles_x_destino 	+= count($a_items_x_in);
				$total_columnas_hoteles_in 	+= count($a_items_x_in);

				$cont_hotel++;
			}

			$a_servicios_hotel[$key_dest] 	= $a_items_hotel;
			$subtotales_col_dhotel[] 		= array(0 =>$key_dest, 1 =>$total_hoteles_x_destino, 2 =>'sh dh' . $i);
			$t_servicios_hotel[] 			= array('name' => $key_dest, 'class' => 'dh' . $i, 'parent' => 'sh', 'childs' => $t_items_hotel, 'cols' =>$total_hoteles_x_destino);
		}
	
	/*
	* OTROS SERVICIOS
	*/

		// OBTENER TODOS LOS SERVICIOS Y AGRUPARLOS POR EL TIPO DE SERVICIO

		$otros_servicios 				= c_mostrar_cotizacion::obtener_servicios_por_rango_cotizaciones(implode(',', $ids_cotizaciones));
		$a_servicios_varios 			= array();
		$total_columnas_otros_servicios = 0;
		$subtotales_col_servicios_ind 	= array();
		$subtotales_col_otros_servicios = array();
		$subtotales_col_destinos 		= array();

		for ($i=0; $i < count($otros_servicios); $i++) 
		{ 
			$otro_servicio 		= $otros_servicios[$i];
			$idotro_servicio 	= $otro_servicio['idotro_servicio']; 

			$a_servicios_varios[$idotro_servicio] 			= array();
			$a_servicios_varios[$idotro_servicio]['nombre'] = $otro_servicio['nombre_servicio'];

			$total_destinos_x_servicio 	= 0;
			
			// OBTENER TARIFAS SEGUN EL TIPO DE SERVICIO

			$tarifas = c_mostrar_cotizacion::obtener_tarifas_otros_servicios_por_servicio($idotro_servicio, implode(',', $ids_cotizaciones));

			// ORDENAR POR DESTINO

			$a_tarifa_otros = array();
			for ($j=0; $j < count($tarifas); $j++) 
			{ 
				$tarifa 	= $tarifas[$j];
				$destino 	= $tarifa['ciudad'];

				if(!array_key_exists($destino, $a_tarifa_otros))
				{
					$a_tarifa_otros[$destino] 				= array();
					$a_tarifa_otros[$destino]['tarifas'] 	= array();
				}

				$a_tarifa_otros[$destino]['tarifas'][] = $tarifa;
			}
			
			// ORDENAR Y AGRUPAR POR FECHA DE INICIO Y FIN DEL SERVICIO
			
			foreach ($a_tarifa_otros as $skey => $ciudad) 
			{
				$subtotal_cols = 0;
				$tarifas = $ciudad['tarifas'];
				$a_tarifa_otros_x_fecha = array();
				for ($j=0; $j < count($tarifas); $j++) 
				{ 
					$tarifa = $tarifas[$j];
					$tarifa['class'] = 'so' . $i;
					$fechas = date('d/M', strtotime($tarifa['fecha_desde'])) . ' ' . date('d/M', strtotime($tarifa['fecha_hasta']));

					if(!array_key_exists($fechas, $a_tarifa_otros_x_fecha))
					{
						$a_tarifa_otros_x_fecha[$fechas] = array();
						$subtotales_col_servicios_ind[] 	= array(0 =>$fechas, 2 =>'so' . $i);
					}

					$a_tarifa_otros_x_fecha[$fechas][] 	= $tarifa;
				}

				$a_tarifa_otros[$skey] 				= $a_tarifa_otros_x_fecha;
				$total_columnas_otros_servicios 	+= count($a_tarifa_otros_x_fecha);
				$total_destinos_x_servicio 			+= count($a_tarifa_otros_x_fecha);
				$subtotales_col_otros_servicios[] 	= array(0 =>$skey , 1=>count($a_tarifa_otros_x_fecha), 2 =>'so' . $i);
			}

			$a_servicios_varios[$idotro_servicio]['tarifas'] = $a_tarifa_otros;

			$subtotales_col_destinos[] = array(0 => $otro_servicio['nombre_servicio'],1 => $total_destinos_x_servicio, 2 =>'so' . $i);

		}

	/*
	* BOLETOS AEREOS
	*/

		// OBTENER TODOS LOS BOLETOS Y AGRUPARLOS POR LA LINEA AEREA

		$lineas_aereas 					= c_mostrar_cotizacion::obtener_lineas_aereas_por_rango_cotizaciones(implode(',', $ids_cotizaciones));
		$a_boletos_aereos 				= array();
		$subtotales_col_lineas_aereas 	= array();
		$subtotales_col_rutas 			= array();
		$total_columnas_boletos_aereos 	= 0;

		for ($i=0; $i < count($lineas_aereas); $i++) 
		{ 
			$linea_aerea 		= $lineas_aereas[$i];
			$idlineas_aereas 	= $linea_aerea['idlineas_aereas'];
			$codigo_iata 		= $linea_aerea['codigo_iata_linea_aerea'];
			$nombre_linea 		= $codigo_iata;

			$a_boletos_aereos[$codigo_iata] = array();

			$total_rutas_por_linea = 0;

			// OBTENER TARIFAS POR LA LINEA AEREA

			$tarifas = c_mostrar_cotizacion::obtener_boletos_por_linea_aerea($idlineas_aereas, implode(',', $ids_cotizaciones));

			// ORDENAR POR RUTA

			$a_boletos_cotizados = array();

			for ($j=0; $j < count($tarifas); $j++) 
			{ 
				$tarifa 				= $tarifas[$j];
				$ruta 					= $tarifa['ruta'];
				$tarifa['nombre_linea'] = $nombre_linea;
				$tarifa['class'] 		= 'sb rb' . $j;

				if(!array_key_exists($ruta, $a_boletos_cotizados))
				{
					$a_boletos_cotizados[$ruta] = array();
					$subtotales_col_rutas[] 	= array(0 =>$ruta, 2 =>'sb rb' . $j);
				}

				$a_boletos_cotizados[$ruta][] = $tarifa;
			}
			
			$a_boletos_aereos[$codigo_iata] = $a_boletos_cotizados;
			$subtotales_col_lineas_aereas[] = array(0 =>$nombre_linea, 1=>count($a_boletos_cotizados) , 2 =>'sb');
			$total_columnas_boletos_aereos 	+= count($a_boletos_cotizados);
		}

	/*
	* INSCRIPCIONES
	*/

		// OBTENER TODAS LAS INSCRIPCIONES Y AGRUPARLAS POR EVENTO

		$eventos 						= c_mostrar_cotizacion::obtener_eventos_por_rango_cotizaciones(implode(',', $ids_cotizaciones));
		$a_inscripciones 				= array();
		$subtotales_col_eventos 		= array();
		$total_columnas_inscripciones 	= 0;

		for ($i=0; $i < count($eventos); $i++) 
		{ 
			$evento 			= $eventos[$i];
			$idevento 			= $evento['idevento'];
			$sigla_evento 		= $evento['sigla_evento'];
			$nombre_evento 		= $evento['nombre_evento'];

			$a_inscripciones[$idevento] = array();

			$total_insc_x_evento = 0;

			// OBTENER TARIFAS POR LA LINEA AEREA

			$tarifas = c_mostrar_cotizacion::obtener_inscripciones_por_evento($idevento, implode(',', $ids_cotizaciones));

			// ORDENAR POR RUTA

			$a_boletos_cotizados = array();

			for ($j=0; $j < count($tarifas); $j++) 
			{ 
				$tarifas[$j]['nombre_evento'] 	= $nombre_evento;
				$tarifas[$j]['class'] 			= 'si ei' . $i;
			}
			
			$a_inscripciones[$idevento] = $tarifas;
			$subtotales_col_eventos[] 	= array(0 =>$sigla_evento, 2 =>'si');
		}
		$total_columnas_inscripciones = $i;

?>
</div>
<div class="clear"></div>

	<a href="visualizar_paquete.php?idpaquete=<?php echo $idpublicacion; ?>" target="_blank" style="text-decoration:none;">
		<h3><?php echo $info_paquete[0]['titulo']; ?></h3>
	</a>

	<!-- INICIA LAS TABLAS -->
	<div style="float:left;">

		<!-- MUESTRA DETALLE DE PRECIOS  -->

			<!-- NOMBRES DE CLIENTES Y PASAJEROS -->	
			<div class="vista_precios" style="width:360px; float:left;">
				<table class="tabla-resumen">
					<!-- TITULOS GENERALES -->
						<tr>
							<th style="height:58px !important; width:15px !important;">C</th>
							<th style="height:58px !important; width:150px !important;">CLIENTE</th>
							<th style="height:58px !important; width:190px !important;">PASAJEROS</th>
						</tr>

					<!-- DETALLAMOS NOBRES DE PASAJEROS -->

					<?php
						for ($i=0; $i < count($cotizaciones); $i++) 
						{ 
							if($i%2 == 0)
								$color = 'white';
							else
								$color = 'gray';

							$cotizacion = $cotizaciones[$i];
							$idcot 		= $cotizacion['idcotizacion'];
							$paxs 		= $cotizacion['paxs'];
							$cant_pax	= count($paxs);

							if($cant_pax > 0)
							{
								for ($j=0; $j < count($paxs); $j++) 
								{ 
									$pax = $paxs[$j];
								?>
									<tr class="<?php echo $color; ?>">
										<?php
											if($j == 0)
											{
											?>
												<td rowspan="<?php echo $cant_pax; ?>" ><a href="ver_cotizacion_paquete.php?cotizacion=<?php echo $idcot; ?>" target="_blank"><?php echo $idcot; ?></a></td>
												<td rowspan="<?php echo $cant_pax; ?>" title="<?php echo $cotizacion['nombre_cliente']; ?>" >
													<a href="mostrar_planilla_cotizacion.php?c=<?php echo $idcot; ?>" target="_blank">
														<?php echo substr($cotizacion['nombre_cliente'], 0, 17); ?>
													</a>
												</td>
											<?php
											}
										?>
												<td title="<?php echo strtoupper($pax['nombre_cliente'] . ' ' . $pax['apellido_cliente']); ?>"><?php echo substr(strtoupper($pax['nombre_cliente'] . ' ' . $pax['apellido_cliente']), 0, 20); ?></td>
									</tr>
								<?php
								}
							}
							else
							{
							?>
								<tr class="<?php echo $color; ?>">
									<td><a href="ver_cotizacion_paquete.php?cotizacion=<?php echo $idcot; ?>" target="_blank"><?php echo $idcot; ?></a></td>
									<td title="<?php echo $cotizacion['nombre_cliente']; ?>" ><?php echo substr($cotizacion['nombre_cliente'], 0, 15); ?></td>
									<td>- NO INGRESADO -</td>
								</tr>
							<?php
							}
						}
					?>
					<tr style="background-color:#F1F1F1;"><td colspan="3" style="border:1px solid #000;">TOTALES</td></tr>

					<!-- TITULOS GENERALES -->
						<tr>
							<th style="height:58px !important; width:15px !important;">C</th>
							<th style="height:58px !important; width:150px !important;">CLIENTE</th>
							<th style="height:58px !important; width:190px !important;">PASAJEROS</th>
						</tr>
					
				</table>
			</div>

			<!-- PRECIOS DE CADA SERVICIO -->
			<div class="vista_precios" style="width:785px; overflow-x:scroll; overflow-y:hidden; float:left; border-left:1px solid #AAA; border-right:1px solid #AAA;">
				<table class="tabla-resumen">

					<!-- TITULOS GENERALES -->
						<tr>
							<!-- BOLETOS AEREOS -->
							<?php
								if($total_columnas_boletos_aereos > 0)
								{
									$total_cols_aereo = $total_columnas_boletos_aereos + 1;
								?>
								<th class="subtitle sb cell_sb" colspan="<?php echo $total_columnas_boletos_aereos; ?>">AEREO</th>
								<th class="subtitle sb" rowspan="4">SUB<br/>TOTAL</th>
								<?php
								} 
							?>

							<!-- PARA HOTELES -->
							<?php
								if($total_columnas_hoteles_in > 0)
								{
									$total_cols_hotel = $total_columnas_hoteles_in + 1;
								?>
									<th class="subtitle sh cell_sh" id="" colspan="<?php echo $total_columnas_hoteles_in; ?>">HOTELES</th>
									<th class="subtitle sh" rowspan="4">SUB<br/>TOTAL</th>
								<?php
								} 
							?>
							
							<!-- MOSTRAMOS LOS SERVICIOS VARIOS -->
							<?php
								for ($i=0; $i < count($subtotales_col_destinos); $i++) 
								{ 
									$aux = $subtotales_col_destinos[$i];
								?>
									<th class="subtitle <?php echo $aux[2]; ?> cell_<?php echo substr($aux[2], -3); ?>" colspan="<?php echo $aux[1]; ?>"><?php echo substr($aux[0], 0, 7); ?></th>
									<th class="subtitle <?php echo $aux[2]; ?>" rowspan="4">SUB<br/>TOTAL</th>
								<?php
								}
							?>

							<!-- INSCRIPCIONES -->
							<?php
								if($total_columnas_inscripciones > 0)
								{
								?>
									<th class="subtitle si cell_si" colspan="<?php echo $total_columnas_inscripciones; ?>">INSCRIP</th>
								<?php
								} 
							?>

							<th class="subtitle total_familia" rowspan="4">TOTAL</th>
						</tr>
					
					<!-- TITULOS PARA LOS SERVICIOS -->

						<tr>
							<!-- MOSTRAMOS LAS LINEAS AEREAS -->
							<?php
								for ($i=0; $i < count($subtotales_col_lineas_aereas); $i++) 
								{ 
									$aux = $subtotales_col_lineas_aereas[$i];
								?>
									<th style="height:27px !important;" class="subtitle <?php echo $aux[2]; ?> cell_<?php echo substr($aux[2], -3); ?>" rowspan="2" colspan="<?php echo $aux[1]; ?>"><?php echo strtoupper($aux[0]); ?></th>
								<?php
								}
							?>

							<!-- MOSTRAMOS LOS DESTINOS POR HOTELES -->
							<?php
								for ($i=0; $i < count($subtotales_col_dhotel); $i++) 
								{ 
									$aux = $subtotales_col_dhotel[$i];
								?>
									<th class="subtitle <?php echo $aux[2]; ?> cell_<?php echo substr($aux[2], -2); ?>" id="" colspan="<?php echo $aux[1]; ?>"><?php echo $aux[0]; ?></th>
								<?php
								}
							?>

							<!-- MOSTRAMOS LOS DESTINOS POR OTROS SERVICIOS -->
							<?php
								for ($i=0; $i < count($subtotales_col_otros_servicios); $i++) 
								{ 
									$aux = $subtotales_col_otros_servicios[$i];
								?>
									<th style="height:27px !important;" class="subtitle <?php echo $aux[2]; ?> cell_<?php echo substr($aux[2], -3); ?>" rowspan="2" colspan="<?php echo $aux[1]; ?>"><?php echo $aux[0]; ?></th>
								<?php
								}
							?>

							<!-- MOSTRAMOS LOS EVENTOS -->
							<?php
								for ($i=0; $i < count($subtotales_col_eventos); $i++) 
								{ 
									$aux = $subtotales_col_eventos[$i];
								?>
									<th style="height:27px !important;" class="subtitle <?php echo $aux[2]; ?> cell_<?php echo substr($aux[2], -3); ?>" rowspan="3" title="<?php echo $aux[0]; ?>" ><?php echo strtoupper(substr($aux[0], 0, 7)); ?></th>
								<?php
								}
							?>

						</tr>

						<tr>
							<!-- MOSTRAMOS LOS HOTELES -->
							<?php
							for ($i=0; $i < count($subtotales_col_hoteles); $i++) 
							{ 
								$aux = $subtotales_col_hoteles[$i];
							?>
								<th style="min-width:70px !important;" class="subtitle2 <?php echo $aux[2]; ?> cell_<?php echo substr($aux[2], -2); ?>" id="" title="<?php echo $aux[0]; ?>" colspan="<?php echo $aux[1]; ?>"><?php echo substr($aux[0], 0, 7); ?></th>
							<?php
							}
							?>
						</tr>

						<tr>
							<!-- MOSTRAMOS LAS RUTAS DE BOLETOS AEREOS -->
							<?php
								for ($i=0; $i < count($subtotales_col_rutas); $i++) 
								{ 
									$aux = $subtotales_col_rutas[$i];
								?>
									<th class="subtitle2 <?php echo $aux[2]; ?> cell_<?php echo substr($aux[2], -3); ?>" title="<?php echo strtoupper($aux[0]); ?>" ><?php echo strtoupper(substr($aux[0], 0, 6) . '..'); ?><br/><br/></th>
								<?php
								}
							?>

							<!-- MOSTRAMOS LAS FECHAS DE ENTRADAS DE HOTELES -->
							<?php
								for ($i=0; $i < count($subtotales_col_in); $i++) 
								{ 
									$aux = $subtotales_col_in[$i];
								?>
									<th style="min-width:50px !important;" class="subtitle2 <?php echo $aux[2]; ?> cell_<?php echo substr($aux[2], -2); ?>" id="" ><?php echo strtoupper(str_replace(' ', ' A<br/>',$aux[0])); ?></th>
								<?php
								}
							?>

							<!-- MOSTRAMOS LAS FECHAS POR OTROS SERVICIOS -->
							<?php
								for ($i=0; $i < count($subtotales_col_servicios_ind); $i++) 
								{ 
									$aux = $subtotales_col_servicios_ind[$i];
								?>
									<th style="min-width:50px !important;" class="subtitle2 <?php echo $aux[2]; ?> cell_<?php echo substr($aux[2], -3); ?>"><?php echo strtoupper(str_replace(' ', ' A<br/>',$aux[0])); ?></th>
								<?php
								}
							?>
						</tr>

					<!-- DETALLAMOS NOMBRES DE PASAJEROS Y COSTOS DE SERVICIOS -->

						<?php
							$total_servicio_hoteles = 0;
							$total_servicio_otros 	= array();
							$total_servicio_boleto  = 0;
							$total_costo_paquete 	= 0;

							for ($i=0; $i < count($cotizaciones); $i++) 
							{ 
								$total_familia 			= 0;
								$total_familia_hotel 	= 0;
								$total_familia_servicio = array();
								$total_familia_aereo 	= 0;

								if($i%2 == 0)
								{
									$color = 'white';
									$colsub = '#feffbf';
								}
								else
								{
									$color = 'gray';
									$colsub = '#ffcfcc';
								}

								$cotizacion = $cotizaciones[$i];
								$idcot 		= $cotizacion['idcotizacion'];
								$paxs 		= $cotizacion['paxs'];
								$cant_pax	= count($paxs);

								if($cant_pax > 0)
								{
									for ($j=0; $j < count($paxs); $j++) 
									{ 
										$pax = $paxs[$j];
									?>
										<tr class="<?php echo $color; ?>">
											<!-- COLOCAR CADA UNO DE LOS BOLETOS AEREOS -->
												<?php
													$cod_aereos = c_mostrar_cotizacion::obtener_id_servicios_aereo($pax['idpax']);

													foreach ($a_boletos_aereos as $cod_aepto => $blinea_aerea) 
													{
														foreach ($blinea_aerea as $ruta => $tarifas) 
														{
															$precios 		= '';
															$class 			= '';

															if(!array_key_exists('total_servicio', $a_boletos_aereos[$cod_aepto][$ruta]))
															{
																$a_boletos_aereos[$cod_aepto][$ruta]['total_servicio'] = 0;
															}

															for ($k=0; array_key_exists($k, $tarifas); $k++) 
															{ 
																$tarifa 	= $tarifas[$k];
																$class 		= $tarifa['class'];
																$idtarifa 	= $tarifa['idboleto_cotizado'];

																if(array_key_exists($idtarifa, $cod_aereos))
																{
																	$acomodacion = $cod_aereos[$idtarifa]['t'];
																	$costo = '';
																	$acom  = '';
																	switch ($acomodacion) 
																	{
																		case 1: $costo=$tarifa['precio_adulto']; $acom='ADT'; break;
																		case 2: $costo=$tarifa['precio_menor']; $acom='CNN'; break;
																		case 3: $costo=$tarifa['precio_infante']; $acom='INF'; break;
																		default: $costo = 0; $acom='N/N'; break;
																	}
																	
																	// TRANSFORMAR A DOLARES SI ESTA EN BOLIVIANOS
																	if($tarifa['moneda'] == 'B')
																	{
																		$costo /= $tarifa['tipo_cambio'];
																	}

																	// SUMAR FEE
																	if($tarifa['increment_fee'] == 'P')
																	{
																		$fee 	= 1 + ($tarifa['fee']/100);
																		$costo 	*= $fee;
																	}
																	else
																	{
																		$fee 	= $tarifa['fee'];
																		$costo 	+= $fee;
																	}

																	// SUMAR IVA
																	if($tarifa['increment_factura'] == 'P')
																	{
																		$iva 	= 1 + ($tarifa['factura']/100);
																		$costo 	*= $iva;
																	}
																	else
																	{
																		$iva 	= $tarifa['factura'];
																		$costo 	+= $iva;
																	}

																	// REDONDEAMOS AL ENTERO INMEDIATO SUPERIOR
																	$costo 					= ceil($costo);
																	$total_familia 			+= $costo;
																	$total_familia_aereo 	+= $costo;
																	$total_servicio_boleto  += $costo;

																	$a_boletos_aereos[$cod_aepto][$ruta]['total_servicio'] += $costo;

																	$precios 	.= $costo . '<br/>';
																}
															}
															if($precios == '')
																$precios .= '<br/>';
															?>
																<td style="text-align:right;" class="<?php echo $class; ?>" ><?php echo $precios; ?></td>
															<?php
														}
													}
												?>
												<?php
													if(count($a_boletos_aereos)>0)
													{
														if($j == count($paxs) - 1)
														{
														?>
															<td style="text-align:right; border-top:none; background-color:<?php echo $colsub; ?>;" class="sb">
																<strong><?php echo $total_familia_aereo; ?></strong>
															</td>
														<?php
														}
														else
														{
														?>
															<td style="border-top:none; border-bottom:none; background-color:<?php echo $colsub; ?>;" class="sb"></td>
														<?php
														}	
													}
												?>

											<!-- COLOCAR CADA UNO DE LOS SERVICIOS (HOTELES) -->
												<?php
													$cod_hoteles = c_mostrar_cotizacion::obtener_id_servicios_hoteles($pax['idpax']);

													foreach ($a_servicios_hotel as $hkey => $hdestino) 
													{
														foreach ($hdestino as $idhotel => $hotel) 
														{
															foreach ($hotel as $fecha => $tarifas) 
															{
																$precios 		= '';
																$class 			= '';

																if(!array_key_exists('total_servicio', $a_servicios_hotel[$hkey][$idhotel][$fecha]))
																{
																	$a_servicios_hotel[$hkey][$idhotel][$fecha]['total_servicio'] = 0;
																}

																for ($k=0; array_key_exists($k, $tarifas); $k++) 
																{ 
																	$tarifa 	= $tarifas[$k];
																	$class 		= $tarifa['class'];
																	$idtarifa 	= $tarifa['iditem_hotel'];

																	if(array_key_exists($idtarifa, $cod_hoteles))
																	{
																		$acomodacion = $cod_hoteles[$idtarifa]['t'];
																		$costo = '';
																		$acom  = '';
																		switch ($acomodacion) 
																		{
																			case 1: $costo=$tarifa['precio_single']; $acom='SGL'; break;
																			case 2: $costo=$tarifa['precio_doble']; $acom='DBL'; break;
																			case 3: $costo=$tarifa['precio_triple']; $acom='TPL'; break;
																			case 4: $costo=$tarifa['precio_cuadruple']; $acom='CPL'; break;
																			case 5: $costo=$tarifa['precio_menor']; $acom='CNN'; break;
																			case 6: $costo=$tarifa['precio_infante']; $acom='INF'; break;
																			default: $costo = 0; $acom='N/N'; break;
																		}

																		// MULTIPLICAR POR LA CANTIDAD DE NOCHES
																		$cant_noches 	= floor((strtotime($tarifa['fecha_out'] . ' 00:00:00') - strtotime($tarifa['fecha_in'] . ' 00:00:00')) / 86400);
																		$costo    		*= $cant_noches;

																		// TRANSFORMAR A DOLARES SI ESTA EN BOLIVIANOS
																		if($tarifa['moneda'] == 'B')
																		{
																			$costo /= $tarifa['tipo_cambio'];
																		}

																		// SUMAR FEE
																		if($tarifa['increment_fee'] == 'P')
																		{
																			$fee 	= 1 + ($tarifa['fee']/100);
																			$costo 	*= $fee;
																		}
																		else
																		{
																			$fee 	= $tarifa['fee'];
																			$costo 	+= $fee;
																		}

																		// SUMAR IVA
																		if($tarifa['increment_factura'] == 'P')
																		{
																			$iva 	= 1 + ($tarifa['factura']/100);
																			$costo 	*= $iva;
																		}
																		else
																		{
																			$iva 	= $tarifa['factura'];
																			$costo 	+= $iva;
																		}

																		// REDONDEAMOS AL ENTERO INMEDIATO SUPERIOR
																		$costo 					= ceil($costo);
																		$total_familia 			+= $costo;
																		$total_familia_hotel 	+= $costo;
																		$total_servicio_hoteles += $costo;

																		$a_servicios_hotel[$hkey][$idhotel][$fecha]['total_servicio'] += $costo;

																		$precios 	.= $costo . '<br/>';
																	}
																}
																if($precios == '')
																	$precios .= '<br/>';
																?>
																	<td style="text-align:right;" class="<?php echo $class; ?>" ><?php echo $precios; ?></td>
																<?php
															}
														}
													}
												?>
												<?php
													if(count($a_servicios_hotel)>0)
													{
														if($j == count($paxs) - 1)
														{
														?>
															<td style="text-align:right; border-top:none; background-color:<?php echo $colsub; ?>;" class="sh">
																<strong><?php echo $total_familia_hotel; ?></strong>
															</td>
														<?php
														}
														else
														{
														?>
															<td style="border-top:none; border-bottom:none; background-color:<?php echo $colsub; ?>;" class="sh"></td>
														<?php
														}
													}
												?>

											<!-- COLOCAR CADA UNO DE LOS SERVICIOS VARIOS -->
												<?php
													$cod_servicios = c_mostrar_cotizacion::obtener_id_otros_servicios($pax['idpax']);
													
													foreach ($a_servicios_varios as $skey => $tipo_servicio) 
													{
														$destinos 				= $tipo_servicio['tarifas'];
														if(!array_key_exists($skey, $total_familia_servicio))
														{
															$total_familia_servicio[$skey] = 0;
														}
														if(!array_key_exists($skey, $total_servicio_otros))
														{
															$total_servicio_otros[$skey] = array();
															$total_servicio_otros[$skey]['total'] = 0;
															$total_servicio_otros[$skey]['subtotales'] = array();
														}
														
														foreach ($destinos as $dkey => $destino) 
														{
															foreach ($destino as $tkey => $tarifas) 
															{
																$precios 		= '';

																if(!array_key_exists('total_servicio', $total_servicio_otros[$skey]['subtotales'][$dkey][$tkey]))
																{
																	$total_servicio_otros[$skey]['subtotales'][$dkey][$tkey]['total_servicio'] = 0;
																}

																for ($k=0; $k < count($tarifas); $k++) 
																{ 
																	$tarifa 	= $tarifas[$k];
																	$class 		= $tarifa['class'];
																	$idtarifa 	= $tarifa['idtarifa_otros'];

																	if(array_key_exists($idtarifa, $cod_servicios))
																	{
																		$acomodacion = $cod_servicios[$idtarifa]['t'];
																		$costo = '';
																		$acom  = '';
																		switch ($acomodacion) 
																		{
																			case 1: $costo=$tarifa['precio_adulto']; $acom='ADT'; break;
																			case 2: $costo=$tarifa['precio_menor']; $acom='CNN'; break;
																			case 3: $costo=$tarifa['precio_infante']; $acom='INF'; break;
																			default: $costo = 0; $acom='N/N'; break;
																		}

																		// TRANSFORMAR A DOLARES SI ESTA EN BOLIVIANOS
																		if($tarifa['moneda'] == 'B')
																		{
																			$costo /= $tarifa['tipo_cambio'];
																		}

																		// SUMAR FEE
																		if($tarifa['increment_fee'] == 'P')
																		{
																			$fee 	= 1 + ($tarifa['fee']/100);
																			$costo 	*= $fee;
																		}
																		else
																		{
																			$fee 	= $tarifa['fee'];
																			$costo 	+= $fee;
																		}

																		// SUMAR IVA
																		if($tarifa['increment_factura'] == 'P')
																		{
																			$iva 	= 1 + ($tarifa['factura']/100);
																			$costo 	*= $iva;
																		}
																		else
																		{
																			$iva 	= $tarifa['factura'];
																			$costo 	+= $iva;
																		}

																		// REDONDEAMOS AL ENTERO INMEDIATO SUPERIOR
																		$costo 					= ceil($costo);
																		$total_familia 			+= $costo;
																		$total_familia_servicio[$skey] += $costo;

																		$total_servicio_otros[$skey]['subtotales'][$dkey][$tkey]['total_servicio'] += $costo;
																		$total_servicio_otros[$skey]['total'] += $costo;

																		$precios 	.= $costo . '<br/>';
																	}
																}
																if($precios == '')
																	$precios .= '<br/>';
																?>
																	<td style="text-align:right;" class="<?php echo $class; ?>"><?php echo $precios; ?></td>
																<?php
															}
														}
														
														?>
														<?php
															if($j == count($paxs) - 1)
															{
															?>
																<td style="text-align:right; border-top:none; background-color:<?php echo $colsub; ?>;" class="<?php echo substr($class, 0, 3); ?>" >
																	<strong><?php echo $total_familia_servicio[$skey]; ?></strong>
																</td>
															<?php
															}
															else
															{
															?>
																<td style="border-top:none; border-bottom:none; background-color:<?php echo $colsub; ?>;" class="<?php echo substr($class, 0, 3); ?>" ></td>
															<?php
															}
														?>
														<?php
													}
												?>

											<!-- COLOCAR CADA UNA DE LAS INSCRIPCIONES -->
												<?php
													$cod_inscrip = c_mostrar_cotizacion::obtener_id_servicios_inscripcion($pax['idpax']);// AQUI QUEDE

													foreach ($a_inscripciones as $idevento => $tarifas) 
													{
														$precios 		= '';
														$class 			= '';

														if(!array_key_exists('total_servicio', $a_inscripciones[$idevento]))
														{
															$a_inscripciones[$idevento]['total_servicio'] = 0;
														}

														for ($k=0; array_key_exists($k, $tarifas); $k++) 
														{ 
															$tarifa 	= $tarifas[$k];
															$class 		= $tarifa['class'];
															$idtarifa 	= $tarifa['idinscripcion_evento'];

															if(array_key_exists($idtarifa, $cod_inscrip))
															{
																$costo = $tarifa['precio'];
																
																// TRANSFORMAR A DOLARES SI ESTA EN BOLIVIANOS
																if($tarifa['moneda'] == 'B')
																{
																	$costo /= $tarifa['tipo_cambio'];
																}

																// SUMAR FEE
																if($tarifa['increment_fee'] == 'P')
																{
																	$fee 	= 1 + ($tarifa['fee']/100);
																	$costo 	*= $fee;
																}
																else
																{
																	$fee 	= $tarifa['fee'];
																	$costo 	+= $fee;
																}

																// SUMAR IVA
																if($tarifa['increment_factura'] == 'P')
																{
																	$iva 	= 1 + ($tarifa['factura']/100);
																	$costo 	*= $iva;
																}
																else
																{
																	$iva 	= $tarifa['factura'];
																	$costo 	+= $iva;
																}

																// REDONDEAMOS AL ENTERO INMEDIATO SUPERIOR
																$costo 					= ceil($costo);
																$total_familia 			+= $costo;
																$a_inscripciones[$idevento]['total_servicio'] += $costo;

																$precios 	.= $costo . '<br/>';
															}
														}
														if($precios == '')
															$precios .= '<br/>';
														?>
															<td style="text-align:right;" class="<?php echo $class; ?>" ><?php echo $precios; ?></td>
														<?php
													}
												?>

											<!-- MOSTRAR EL TOTAL DE LA FAMILIA -->
												<?php
													if($j == count($paxs) - 1)
													{
													?>
														<td style="text-align:right; border-top:none; background-color:#cadfd8;" class="total_familia">
															<strong><?php echo $total_familia; ?></strong>
														</td>
													<?php
													}
													else
													{
													?>
														<td style="border-top:none; border-bottom:none; background-color:#cadfd8;" class="total_familia"></td>
													<?php
													}
												?>
										</tr>
									<?php
									}
								}
								else
								{
								?>
									<tr class="<?php echo $color; ?>">
										<td class="no-pax" colspan="<?php echo $total_cols_hotel + $total_columnas_otros_servicios + $total_cols_aereo + 1 + count($subtotales_col_destinos) + $total_columnas_inscripciones; ?>"><br/></td>
										<!--<td style="text-align:right;" class="total_familia"><strong>0</strong></td>-->
									</tr>
								<?php
								}
							}
						?>

						<!-- MOSTRAMOS LOS TOTALES POR SERVICIO -->
						<tr style="background-color:#F1F1F1;">
							<!-- AEREO -->
							<?php
								foreach ($a_boletos_aereos as $cod_aepto => $blinea_aerea) 
								{
									foreach ($blinea_aerea as $ruta => $tarifas) 
									{
									?>
										<td style="text-align:right; border:1px solid #000;" class="sb"><?php echo $a_boletos_aereos[$cod_aepto][$ruta]['total_servicio']; ?></td>
									<?php
									}
								}
							?>
							<td style="text-align:right; border:1px solid #000;" class="sb"><?php echo $total_servicio_boleto; $total_costo_paquete += $total_servicio_boleto; ?></td>
							
							<!-- HOTEL -->
							<?php
								foreach ($a_servicios_hotel as $hkey => $hdestino) 
								{
									foreach ($hdestino as $idhotel => $hotel) 
									{
										foreach ($hotel as $fecha => $tarifas) 
										{
										?>
											<td style="text-align:right; border:1px solid #000;" class="sh"><?php echo $a_servicios_hotel[$hkey][$idhotel][$fecha]['total_servicio']; ?></td>
										<?php
										}
									}
								}
							?>
							<td style="text-align:right; border:1px solid #000;" class="sh"><?php echo $total_servicio_hoteles; $total_costo_paquete += $total_servicio_hoteles; ?></td>
							
							<!-- OTROS SERVICIOS -->
							<?php 
								$l = 0;
								foreach ($a_servicios_varios as $skey => $tipo_servicio) 
								{ 
									$cls = 'so' . $l;
									$destinos 				= $tipo_servicio['tarifas'];
									foreach ($destinos as $dkey => $destino) 
									{
										foreach ($destino as $tkey => $tarifas) 
										{
										?>
											<td style="text-align:right; border:1px solid #000;" class="<?php echo $cls; ?>"><?php echo $total_servicio_otros[$skey]['subtotales'][$dkey][$tkey]['total_servicio']; ?></td>
										<?php
										}
									}
									?>
										<td style="text-align:right; border:1px solid #000;" class="<?php echo $cls; ?>"><?php echo $total_servicio_otros[$skey]['total']; $total_costo_paquete += $total_servicio_otros[$skey]['total']; ?></td>
									<?php
									$l++;
								}
							?>

							<!-- INSCRIPCIONES -->
							<?php
								foreach ($a_inscripciones as $idevento => $tarifas) 
								{
								?>
									<td style="text-align:right; border:1px solid #000;" class="si"><?php echo $a_inscripciones[$idevento]['total_servicio']; $total_costo_paquete += $a_inscripciones[$idevento]['total_servicio'];  ?></td>
								<?php
								}
							?>
							<td style="text-align:right; border:1px solid #000;" class="total_familia"><strong><?php echo $total_costo_paquete; ?></strong></td>
						</tr>
					
					<!-- TITULOS PARA LOS SERVICIOS (POSTERIOR) -->
						
						<tr>
							<!-- MOSTRAMOS LAS RUTAS DE BOLETOS AEREOS -->
							<?php
								for ($i=0; $i < count($subtotales_col_rutas); $i++) 
								{ 
									$aux = $subtotales_col_rutas[$i];
								?>
									<th class="subtitle2 <?php echo $aux[2]; ?> cell_<?php echo substr($aux[2], -3); ?>" title="<?php echo strtoupper($aux[0]); ?>" ><?php echo strtoupper(substr($aux[0], 0, 6) . '..'); ?><br/><br/></th>
								<?php
								}
							?>

							<!-- SUBTOTAL PARA AEREO -->
							<?php
								if($total_columnas_boletos_aereos > 0)
								{
								?>
									<th class="subtitle sb" rowspan="4">SUB<br/>TOTAL</th>
								<?php
								}
							?>

							<!-- MOSTRAMOS LAS FECHAS DE ENTRADAS DE HOTELES -->
							<?php
								for ($i=0; $i < count($subtotales_col_in); $i++) 
								{ 
									$aux = $subtotales_col_in[$i];
								?>
									<th style="min-width:50px !important;" class="subtitle2 <?php echo $aux[2]; ?> cell_<?php echo substr($aux[2], -2); ?>" id="" ><?php echo strtoupper(str_replace(' ', ' A<br/>',$aux[0])); ?></th>
								<?php
								}
							?>

							<!-- SUBTOTAL PARA HOTELES -->
							<?php
								if($total_columnas_hoteles_in > 0)
								{
								?>
									<th class="subtitle sh" rowspan="4">SUB<br/>TOTAL</th>
								<?php
								}
							?>
							
							<!-- MOSTRAMOS LAS FECHAS POR OTROS SERVICIOS -->
							<?php
								$j = 0;
								$aux3 = substr($subtotales_col_servicios_ind[0][2], 0, 3);

								for ($i=0; $i < count($subtotales_col_servicios_ind); $i++) 
								{ 
									$aux 	= $subtotales_col_servicios_ind[$i];

								?>
									<th style="min-width:50px !important;" class="subtitle2 <?php echo $aux[2]; ?> cell_<?php echo substr($aux[2], -3); ?>"><?php echo strtoupper(str_replace(' ', ' A<br/>',$aux[0])); ?></th>
									<?php
										if($i < count($subtotales_col_servicios_ind))
										{
											$aux2 	= substr($subtotales_col_servicios_ind[$i+1][2], 0, 3);
											if($aux2 != $aux3)
											{
											?>
												<th class="subtitle <?php echo $aux3; ?>" rowspan="4">SUB<br/>TOTAL</th>
											<?php
												$j++;
												$aux3 = $aux2;
											}
										}
									?>
								<?php
								}
							?>

							<!-- MOSTRAMOS LOS EVENTOS -->
							<?php
							for ($i=0; $i < count($subtotales_col_eventos); $i++) 
							{ 
								$aux = $subtotales_col_eventos[$i];
							?>
								<th style="height:27px !important;" class="subtitle <?php echo $aux[2]; ?> cell_<?php echo substr($aux[2], -3); ?>" rowspan="3" title="<?php echo strtoupper($aux[0]); ?>" ><?php echo strtoupper(substr($aux[0], 0, 7)); ?></th>
							<?php
							}
							?>
							
							<th class="subtitle total_familia" rowspan="4">TOTAL</th>

						</tr>

						<tr>
							<!-- MOSTRAMOS LAS LINEAS AEREAS -->
							<?php
							for ($i=0; $i < count($subtotales_col_lineas_aereas); $i++) 
							{ 
								$aux = $subtotales_col_lineas_aereas[$i];
							?>
								<th style="height:27px !important;" class="subtitle <?php echo $aux[2]; ?> cell_<?php echo substr($aux[2], -3); ?>" rowspan="2" colspan="<?php echo $aux[1]; ?>"><?php echo strtoupper($aux[0]); ?></th>
							<?php
							}
							?>

							<!-- MOSTRAMOS LOS DESTINOS POR HOTELES -->
							<?php
								for ($i=0; $i < count($subtotales_col_dhotel); $i++) 
								{ 
									$aux = $subtotales_col_dhotel[$i];
								?>
									<th class="subtitle <?php echo $aux[2]; ?> cell_<?php echo substr($aux[2], -2); ?>" id="" colspan="<?php echo $aux[1]; ?>"><?php echo $aux[0]; ?></th>
								<?php
								}
							?>

							<!-- MOSTRAMOS LOS DESTINOS POR OTROS SERVICIOS -->
							<?php
							for ($i=0; $i < count($subtotales_col_otros_servicios); $i++) 
							{ 
								$aux = $subtotales_col_otros_servicios[$i];
							?>
								<th style="height:27px !important;" class="subtitle <?php echo $aux[2]; ?> cell_<?php echo substr($aux[2], -3); ?>" rowspan="2" colspan="<?php echo $aux[1]; ?>"><?php echo $aux[0]; ?></th>
							<?php
							}
							?>
						</tr>

						<tr>
							<!-- MOSTRAMOS LOS HOTELES -->
							<?php
							for ($i=0; $i < count($subtotales_col_hoteles); $i++) 
							{ 
								$aux = $subtotales_col_hoteles[$i];
							?>
								<th style="min-width:70px !important;" id="cell_<?php echo substr($aux[2], -2); ?>" class="subtitle2 <?php echo $aux[2]; ?>" colspan="<?php echo $aux[1]; ?> cell_<?php echo substr($aux[2], -2); ?>" title="<?php echo $aux[0]; ?>" ><?php echo substr($aux[0], 0, 7); ?></th>
							<?php
							}
							?>
						</tr>

					<!-- TITULOS GENERALES (POSTERIOR) -->
						<tr>
							<!-- AEREO -->
							<?php
								if($total_columnas_boletos_aereos > 0)
								{
								?>
									<th class="subtitle sb cell_sb" colspan="<?php echo $total_columnas_boletos_aereos; ?>">AEREO</th>
								<?php
								}
							?>

							<!-- HOTEL -->
							<?php
								if($total_columnas_hoteles_in > 0)
								{
								?>
									<th class="subtitle sh cell_sh" id="" colspan="<?php echo $total_columnas_hoteles_in; ?>">HOTELES</th>
								<?php
								}
							?>
							
							<!-- MOSTRAMOS LOS SERVICIOS VARIOS -->
							<?php
								for ($i=0; $i < count($subtotales_col_destinos); $i++) 
								{ 
									$aux = $subtotales_col_destinos[$i];
								?>
									<th class="subtitle <?php echo $aux[2]; ?> cell_<?php echo substr($aux[2], -3); ?>" colspan="<?php echo $aux[1]; ?>" title="<?php echo $aux[0]; ?>" ><?php echo substr($aux[0], 0, 7); ?></th>
								<?php
								}
							?>

							<!-- INSCRIPCIONES -->
							<?php
								if($total_columnas_inscripciones > 0)
								{
								?>
									<th class="subtitle si cell_si" colspan="<?php echo $total_columnas_inscripciones; ?>">INSCRIP</th>
								<?php
								} 
							?>
						</tr>

				</table>
			</div>

		<div class="clear_mayor"></div>

		<!-- MUESTRA CODIGOS DE RESERVA -->

			<div class="vista_reserva" style="width:1140px; display:none;">

				<!-- TABLA DE AEREO (CODIGOS DE RESERVA) -->
				<?php
					if($total_columnas_boletos_aereos > 0)
					{
					?>
						<table class="tabla-resumen sb" style="width:100%; margin-bottom:5px;">
							<tr>
								<th colspan="12" class="nombre_servicio">PASAJES AEREOS</th>
							</tr>
							<tr>
								<th class="subtitle">N</th>
								<th class="subtitle">PASAJERO</th>
								<th class="subtitle">PAX</th>
								<th class="subtitle">COSTO<br/>$US</th>
								<th class="subtitle">LINEA<br/>AEREA</th>
								<th class="subtitle">RUTA</th>
								<th class="subtitle">CODIGO</th>
								<th class="subtitle col_time_limit">TIME<br/>LIMIT</th>
								<th class="subtitle col_adjunto">ADJUNTOS</th>
								<th class="subtitle col_pass">PASS</th>
								<th class="subtitle col_fnac">FECHA<br/>NAC</th>
							</tr>
							<?php
								$color 		= 'gray';
								$cont_pax 	= 1;
								foreach ($a_boletos_aereos as $cod_aepto => $blinea_aerea) 
								{
									foreach ($blinea_aerea as $ruta => $tarifas) 
									{
										$precios 		= '';
										$class 			= '';

										for ($k=0; array_key_exists($k, $tarifas); $k++) 
										{ 
											$tarifa 	= $tarifas[$k];
											$class 		= $tarifa['class'];
											$idtarifa 	= $tarifa['idboleto_cotizado'];

											$pasajeros 	= c_mostrar_cotizacion::obtener_pasajeros_por_servicio_aereo($idtarifa);
											$adjuntos 	= c_mostrar_cotizacion::mostrar_adjuntos_por_boleto($idtarifa);

											if(count($pasajeros) > 0)
											{
												if($color == 'gray')
													$color = 'white';
												else
													$color = 'gray';	
											}

											for ($x=0; $x < count($pasajeros); $x++) 
											{ 
												$pax 	= $pasajeros[$x];
												$acom 	= '';
												$costo 	= '';
												switch ($pax['tipo_precio']) 
												{
													case 1: $costo=$tarifa['precio_adulto']; $acom='ADT'; break;
													case 2: $costo=$tarifa['precio_menor']; $acom='CNN'; break;
													case 3: $costo=$tarifa['precio_infante']; $acom='INF'; break;
													default: $costo=0; $acom='N/N'; break;
												}

												// TRANSFORMAR A DOLARES SI ESTA EN BOLIVIANOS
												if($tarifa['moneda'] == 'B')
												{
													$costo /= $tarifa['tipo_cambio'];
												}

												// SUMAR FEE
												if($tarifa['increment_fee'] == 'P')
												{
													$fee 	= 1 + ($tarifa['fee']/100);
													$costo 	*= $fee;
												}
												else
												{
													$fee 	= $tarifa['fee'];
													$costo 	+= $fee;
												}

												// SUMAR IVA
												if($tarifa['increment_factura'] == 'P')
												{
													$iva 	= 1 + ($tarifa['factura']/100);
													$costo 	*= $iva;
												}
												else
												{
													$iva 	= $tarifa['factura'];
													$costo 	+= $iva;
												}

												// REDONDEAMOS AL ENTERO INMEDIATO SUPERIOR
												$costo 		= ceil($costo);

												?>
													<tr class="<?php echo $color; ?>" >
														<td><?php echo $cont_pax; ?></td>
														<td><a style="text-decoration:none;" href="ver_cotizacion_paquete.php?cotizacion=<?php echo $pax['idcotizacion']; ?>" target="_blank"><?php echo ucwords(strtolower($pax['nombre_cliente'] . ' ' . $pax['apellido_cliente'])); ?></a></td>
														<td><?php echo $acom; ?></td>
														<td><?php echo $costo; ?></td>
														<?php
															if($x == 0)
															{
																$fecha_limit 	= strtotime($tarifa['time_limit']);
																$fecha_actual 	= strtotime("now");
															?>
																<td rowspan="<?php echo count($pasajeros); ?>">
																	<?php echo strtoupper(substr($tarifa['nombre_linea'], 0, 10)); ?>
																</td>
																<td rowspan="<?php echo count($pasajeros); ?>">
																	<?php echo strtoupper($tarifa['ruta']); ?>
																</td>
																<td rowspan="<?php echo count($pasajeros); ?>">
																	<?php echo $tarifa['codigo_reserva']; ?>
																</td>
																<td class="col_time_limit" rowspan="<?php echo count($pasajeros); ?>" style="background-color:#FFCCCC; <?php if($fecha_actual >= $fecha_limit) { ?>color:#FF3300;<?php } ?>">
																	<?php echo date('d/M', strtotime($tarifa['time_limit'])); ?>
																</td>
																<td style="line-height:1;" class="col_adjunto" rowspan="<?php echo count($pasajeros); ?>">
																	<?php
																	for ($nadj=0; $nadj < count($adjuntos); $nadj++) 
																	{ 
																		$adjunto = $adjuntos[$nadj];
																	?>
																		<a href="<?php echo $ruta_adjuntos . $adjunto['nombre']; ?>"><?php echo $adjunto['descripcion']; ?></a><br/>
																	<?php
																	}
																	?>
																</td>
															<?php
															}
														?>
														<td class="col_pass"><?php echo $pax['numero_pasaporte_cliente']; ?></td>
														<td class="col_fnac"><?php echo date('d/m/Y', strtotime($pax['fecha_nacimiento_cliente'])); ?></td>
													</tr>
												<?php
												$cont_pax++;
											}
										}
									}
								}
							?>
						</table>
					<?php
					}
				?>

				<!-- TABLA DE AEREO (CODIGOS DE RESERVA) -->
				<?php
					$a_pax_aereo 	= array();
					$max_cant_rutas = 0;

					for ($i=0; $i < count($cotizaciones); $i++) 
					{ 
						$cotizacion = $cotizaciones[$i];
						$idcot 		= $cotizacion['idcotizacion'];
						$paxs 		= $cotizacion['paxs'];
						$cant_pax	= count($paxs);

						// CARGAMOS AL ARRAY A PARTIR DE LA COTIZACION
						$a_pax_aereo[$idcot] = array();

						if($cant_pax > 0)
						{
							for ($j=0; $j < count($paxs); $j++) 
							{ 
								$pax = $paxs[$j];

								// CARGAMOS EL NOMBRE DEL PAX
								$a_pax_aereo[$idcot][$j] 			= array();
								$a_pax_aereo[$idcot][$j]['nombre'] 	= $pax['nombre_cliente'] . ' ' . $pax['apellido_cliente'];
								$a_pax_aereo[$idcot][$j]['origen']  = $pax['ciudad'];
								$a_pax_aereo[$idcot][$j]['idorg']  	= $pax['idciudad_origen_paquete'];
								$a_pax_aereo[$idcot][$j]['rutas'] 	= array();

								$cod_aereos = c_mostrar_cotizacion::obtener_id_servicios_aereo($pax['idpax']);

								foreach ($a_boletos_aereos as $cod_aepto => $blinea_aerea) 
								{
									foreach ($blinea_aerea as $ruta => $tarifas) 
									{

										for ($k=0; array_key_exists($k, $tarifas); $k++) 
										{ 
											$tarifa 	= $tarifas[$k];
											$class 		= $tarifa['class'];
											$idtarifa 	= $tarifa['idboleto_cotizado'];

											if(array_key_exists($idtarifa, $cod_aereos))
											{
												$acomodacion = $cod_aereos[$idtarifa]['t'];
												$costo = '';
												$acom  = '';
												switch ($acomodacion) 
												{
													case 1: $costo=$tarifa['precio_adulto']; $acom='ADT'; break;
													case 2: $costo=$tarifa['precio_menor']; $acom='CNN'; break;
													case 3: $costo=$tarifa['precio_infante']; $acom='INF'; break;
													default: $costo = 0; $acom='N/N'; break;
												}
												
												// TRANSFORMAR A DOLARES SI ESTA EN BOLIVIANOS
												$currency = '$Us.';
												if($tarifa['moneda'] == 'B')
												{
													$currency = 'Bs.';
												}

												// SUMAR FEE
												if($tarifa['increment_fee'] == 'P')
												{
													$fee 	= 1 + ($tarifa['fee']/100);
													$costo 	*= $fee;
												}
												else
												{
													$fee 	= $tarifa['fee'];
													$costo 	+= $fee;
												}

												// SUMAR IVA
												if($tarifa['increment_factura'] == 'P')
												{
													$iva 	= 1 + ($tarifa['factura']/100);
													$costo 	*= $iva;
												}
												else
												{
													$iva 	= $tarifa['factura'];
													$costo 	+= $iva;
												}

												// REDONDEAMOS AL ENTERO INMEDIATO SUPERIOR
												$costo 					= ceil($costo);
												$total_familia 			+= $costo;
												$total_familia_aereo 	+= $costo;
												$total_servicio_boleto  += $costo;

												$a_boletos_aereos[$cod_aepto][$ruta]['total_servicio'] += $costo;

												// CARGAMOS AL ARRAY LA RUTA, ACOMODACION, LINEA AEREA, CODIGO, TIMELIMIT
												$a_ruta 			= array();
												$a_ruta['cia'] 		= $cod_aepto;
												$a_ruta['costo'] 	= $currency . $costo;
												$a_ruta['ruta'] 	= $ruta;
												$a_ruta['codigo'] 	= $tarifa['codigo_reserva'];
												if($cod_aereos[$idtarifa]['b'] != '')
													$a_ruta['tlimit'] = '<a href="../../contable/interface/iu_imprimir_boleto.php?numero_ticket=' . $cod_aereos[$idtarifa]['b'] . '" target="_blank" >' . $cod_aereos[$idtarifa]['b'] . '</a>';
												else if(strtotime($tarifa['time_limit']) <= strtotime('now'))
													$a_ruta['tlimit'] 	= '<font style="color:#DD0000;">' . strtoupper(date('d/M', strtotime($tarifa['time_limit']))) . '</font>';
												else
													$a_ruta['tlimit'] 	= '<font style="color:#007700;">' . strtoupper(date('d/M', strtotime($tarifa['time_limit']))) . '</font>';
												
												// CARGAMOS AL ARRAY GLOBAL
												$a_pax_aereo[$idcot][$j]['rutas'][] = $a_ruta;
											}
										}
									}
								}

								// VERIFICAMOS LA MAYOR CANTIDAD DE RUTAS DE UN PASAJERO
								if(count($a_pax_aereo[$idcot][$j]['rutas']) > $max_cant_rutas)
									$max_cant_rutas = count($a_pax_aereo[$idcot][$j]['rutas']);
							}
						}
					}
					
				?>
				<table class="tabla-resumen sbb" style="width:100%; margin-bottom:5px;">
					<tr>
						<th class="subtitle">N</th>
						<th class="subtitle">PAX</th>
						<?php
							for($i=0; $i < $max_cant_rutas; $i++) 
							{ 
							?>
								<th class="subtitle">CIA</th>
								<th class="subtitle">COSTO</th>
								<th class="subtitle">RUTA</th>
								<th class="subtitle">CODIGO</th>
								<th class="subtitle">T.LIM<br/>TKT</th>
							<?php
							}
						?>
					</tr>
					<?php
						$color 		= 'gray';
						$cont_pax   = 1;
						foreach ($a_pax_aereo as $idcot => $cot) 
						{
							if(count($cot) > 0)
							{
								if($color == 'gray')
									$color = 'white';
								else
									$color = 'gray';

								$idciudad = '';
								for ($i=0; $i < count($cot); $i++) 
								{ 
									$name_pax = $cot[$i]['nombre'];
									$rutas 	  = $cot[$i]['rutas'];

									if($cot[$i]['idorg'] != $idciudad)
									{
										?>
											<tr class="<?php echo $color; ?>">
												<td colspan="<?php echo ($max_cant_rutas*5) + 2; ?>" style="font-weight:bold; text-align:center;" >
													SALE DESDE <?php echo $cot[$i]['origen']; ?>
												</td>
											</tr>
										<?php
										$idciudad = $cot[$i]['idorg'];
									}

									?>
										<tr class="<?php echo $color; ?>">
											<td><?php echo $cont_pax; ?></td>
											<td><?php echo $name_pax; ?></td>
									<?php
									for ($j=0; $j < count($rutas); $j++) 
									{ 
										?>
											<td style="border-left:2px solid #000 !important;"><?php echo $rutas[$j]['cia']; ?></td>
											<td><?php echo $rutas[$j]['costo']; ?></td>
											<td title="<?php echo $rutas[$j]['ruta']; ?>"><?php echo substr($rutas[$j]['ruta'], 0, 11) . '..'; ?></td>
											<td><?php echo $rutas[$j]['codigo']; ?></td>
											<td><?php echo $rutas[$j]['tlimit']; ?></td>
										<?php
									} 
									while ($j<$max_cant_rutas) 
									{
										?>
											<!-- COMPLETAMOS LAS CELDAS -->
											<td style="border-left:2px solid #000 !important;"></td><td></td><td></td><td></td><td></td>
										<?php
										$j++;
									}	
									?>
										</tr>
									<?php
									$cont_pax++;
								}
							}
						}
					?>
				</table>

				<div class="clear_mayor"></div>

				<!-- TABLA DE HOTELES (CODIGOS DE RESERVA) -->
				<?php
					if($total_columnas_hoteles_in > 0)
					{
						$habitaciones = 0;
					?>
						<table class="tabla-resumen sh" style="width:100%; margin-bottom:5px; border:1px solid #AAA;">
							<tr>
								<th colspan="14" class="nombre_servicio">HOTEL</th>
							</tr>
							<tr>
								<th class="subtitle">N</th>
								<th class="subtitle">HAB</th>
								<th class="subtitle">PASAJERO</th>
								<th class="subtitle">ACOM</th>
								<th class="subtitle">COSTO<br/>$US</th>
								<th class="subtitle">HOTEL</th>
								<th class="subtitle">IN</th>
								<th class="subtitle">OUT</th>
								<th class="subtitle">CODIGO</th>
								<th class="subtitle col_time_limit">TIME<br/>LIMIT</th>
								<th class="subtitle col_comentario">OBS</th>
								<th class="subtitle col_adjunto">ADJUNTOS</th>
								<th class="subtitle col_pass">PASS</th>
								<th class="subtitle col_fnac">FECHA<br/>NAC</th>
							</tr>
							<?php
								$color 		= 'gray';
								$cont_pax 	= 1;
								foreach ($a_servicios_hotel as $hkey => $hdestino) 
								{
									foreach ($hdestino as $idhotel => $hotel) 
									{
										foreach ($hotel as $fecha => $tarifas) 
										{
											$precios 		= '';
											$class 			= '';

											for ($k=0; array_key_exists($k, $tarifas); $k++) 
											{ 
												$tarifa 	= $tarifas[$k];
												$class 		= $tarifa['class'];
												$idtarifa 	= $tarifa['iditem_hotel'];

												$pasajeros 	= c_mostrar_cotizacion::obtener_pasajeros_por_servicio_hotel($idtarifa);
												$adjuntos 	= c_mostrar_cotizacion::mostrar_adjuntos_por_hotel($idtarifa);

												if(count($pasajeros) > 0)
												{
													if($color == 'gray')
														$color = 'white';
													else
														$color = 'gray';	
												}

												$nro_hab 	= '';

												for ($x=0; $x < count($pasajeros); $x++) 
												{ 
													$pax 	= $pasajeros[$x];
													$acom 	= '';
													$costo 	= '';
													$acom  	= '';
													switch ($pax['tipo_precio']) 
													{
														case 1: $costo=$tarifa['precio_single']; $acom='SGL'; break;
														case 2: $costo=$tarifa['precio_doble']; $acom='DBL'; break;
														case 3: $costo=$tarifa['precio_triple']; $acom='TPL'; break;
														case 4: $costo=$tarifa['precio_cuadruple']; $acom='CPL'; break;
														case 5: $costo=$tarifa['precio_menor']; $acom='CNN'; break;
														case 6: $costo=$tarifa['precio_infante']; $acom='INF'; break;
														default: $costo = 0; $acom='N/N'; break;
													}

													// MULTIPLICAR POR LA CANTIDAD DE NOCHES
													$cant_noches 	= floor((strtotime($tarifa['fecha_out'] . ' 00:00:00') - strtotime($tarifa['fecha_in'] . ' 00:00:00')) / 86400);
													$costo    		*= $cant_noches;

													// TRANSFORMAR A DOLARES SI ESTA EN BOLIVIANOS
													if($tarifa['moneda'] == 'B')
													{
														$costo /= $tarifa['tipo_cambio'];
													}

													// SUMAR FEE
													if($tarifa['increment_fee'] == 'P')
													{
														$fee 	= 1 + ($tarifa['fee']/100);
														$costo 	*= $fee;
													}
													else
													{
														$fee 	= $tarifa['fee'];
														$costo 	+= $fee;
													}

													// SUMAR IVA
													if($tarifa['increment_factura'] == 'P')
													{
														$iva 	= 1 + ($tarifa['factura']/100);
														$costo 	*= $iva;
													}
													else
													{
														$iva 	= $tarifa['factura'];
														$costo 	+= $iva;
													}

													// REDONDEAMOS AL ENTERO INMEDIATO SUPERIOR
													$costo 		= ceil($costo);
													
													// VERIFICAMOS LA HABITACION
													if($nro_hab != $pax['habitacion']) 
													{  
														$nro_hab 	= $pax['habitacion'];
														$sw 		= true;
														$habitaciones++;
													}
													else
														$sw = false;

													?>
														<tr class="<?php echo $color; ?>" >
															<td <?php if($sw) echo 'style="border-top:2px solid #000;";'; ?> ><?php echo $cont_pax; ?></td>
															<td style="<?php if($sw) echo 'border-top:2px solid #000; border-bottom:none;";'; else echo 'border-top:none; border-bottom:none;'; ?>" ><?php if($sw) echo $habitaciones; ?></td>
															<td <?php if($sw) echo 'style="border-top:2px solid #000;";'; ?> ><a style="text-decoration:none;" href="ver_cotizacion_paquete.php?cotizacion=<?php echo $pax['idcotizacion']; ?>" target="_blank"><?php echo ucwords(strtolower($pax['nombre_cliente'] . ' ' . $pax['apellido_cliente'])); ?></a></td>
															<td <?php if($sw) echo 'style="border-top:2px solid #000;";'; ?> ><?php echo $acom; ?></td>
															<td <?php if($sw) echo 'style="border-top:2px solid #000;";'; ?> ><?php echo $costo; ?></td>
															<?php
																if($x == 0)
																{
																	$fecha_limit 	= strtotime($tarifa['time_limit']);
																	$fecha_actual 	= strtotime("now");
																?>
																	<td rowspan="<?php echo count($pasajeros); ?>">
																		<?php echo substr($tarifa['nombre_hotel'], 0, 10) . ' ' . $tarifa['categoria'] . '*'; ?>
																	</td>
																	<td rowspan="<?php echo count($pasajeros); ?>">
																		<?php echo date('d/M', strtotime($tarifa['fecha_in'])); ?>
																	</td>
																	<td rowspan="<?php echo count($pasajeros); ?>">
																		<?php echo date('d/M', strtotime($tarifa['fecha_out'])); ?>
																	</td>
																	<td rowspan="<?php echo count($pasajeros); ?>">
																		<?php echo $tarifa['identificador']; ?>
																	</td>
																	<td class="col_time_limit" rowspan="<?php echo count($pasajeros); ?>" style="background-color:#FFCCCC; <?php if($fecha_actual >= $fecha_limit) { ?>color:#FF3300;<?php } ?>">
																		<?php echo date('d/M', strtotime($tarifa['time_limit'])); ?>
																	</td>
																	<td class="col_comentario" rowspan="<?php echo count($pasajeros); ?>" >
																		<?php echo strtoupper($tarifa['observacion']); ?>
																	</td>
																	<td style="line-height:1;" class="col_adjunto" rowspan="<?php echo count($pasajeros); ?>">
																		<?php
																		for ($nadj=0; $nadj < count($adjuntos); $nadj++) 
																		{ 
																			$adjunto = $adjuntos[$nadj];
																		?>
																			<a href="<?php echo $ruta_adjuntos . $adjunto['nombre']; ?>"><?php echo $adjunto['descripcion']; ?></a><br/>
																		<?php
																		}
																		?>
																	</td>
																<?php
																}
															?>
															<td class="col_pass"><?php echo $pax['numero_pasaporte_cliente']; ?></td>
															<td class="col_fnac"><?php echo date('d/m/Y', strtotime($pax['fecha_nacimiento_cliente'])); ?></td>
														</tr>
													<?php
													$cont_pax++;
												}
											}
										}
									}
								}
							?>
						</table>
						<span style="float:right; font-size:7pt;" class="sh">
							<div style="width:30px; float:left;"><hr style="border:1px solid #000;" /></div>
							<div style="width:120px; float:left; margin-left:10px;">Separador de Habitaciones</div>
						</span>
					<?php
					}
				?>
				
				<div class="clear_mayor"></div>
				<div class="clear_mayor"></div>

				<!-- MUESTRA TABLA DEL RESTO DE LOS SERVICIOS -->
				<?php
					$i = 0;
					
					foreach ($a_servicios_varios as $skey => $tipo_servicio)
					{ 
						$nombre 	= $tipo_servicio["nombre"];
						$destinos 	= $tipo_servicio['tarifas'];
						$aux 		= $subtotales_col_destinos[$i];
						$color 		= 'gray';
					?>
						<table class="tabla-resumen <?php echo $aux[2]; ?>" style="width:100%; margin-bottom:20px;">
							<tr>
								<th colspan="12" class="nombre_servicio"><?php echo $nombre; ?></th>
							</tr>
							<tr>
								<th class="subtitle">N</th>
								<th class="subtitle">PASAJERO</th>
								<th class="subtitle">PAX</th>
								<th class="subtitle">COSTO<br/>$US</th>
								<th class="subtitle">DESCR</th>
								<th class="subtitle">DESDE</th>
								<th class="subtitle">HASTA</th>
								<th class="subtitle">CODIGO</th>
								<th class="subtitle col_time_limit">TIME<br/>LIMIT</th>
								<th class="subtitle col_adjunto">ADJUNTOS</th>
								<th class="subtitle col_pass">PASS</th>
								<th class="subtitle col_fnac">FECHA<br/>NAC</th>
							</tr>
							<?php
								$cont_pax = 1;
								foreach ($destinos as $dkey => $destino) 
								{
									foreach ($destino as $tkey => $tarifas) 
									{
										for ($k=0; $k < count($tarifas); $k++) 
										{ 
											$tarifa 	= $tarifas[$k];
											$idtarifa 	= $tarifa['idtarifa_otros'];

											if($color == 'gray')
												$color = 'white';
											else
												$color = 'gray';

											$pasajeros = c_mostrar_cotizacion::obtener_pasajeros_por_servicio($idtarifa);
											$adjuntos 	= c_mostrar_cotizacion::mostrar_adjuntos_por_servicio($idtarifa);

											for ($x=0; $x < count($pasajeros); $x++) 
											{ 
												$pax 	= $pasajeros[$x];
												$acom 	= '';
												switch ($pax['tipo_precio']) 
												{
													case 1: $costo=$tarifa['precio_adulto']; $acom='ADT'; break;
													case 2: $costo=$tarifa['precio_menor']; $acom='CNN'; break;
													case 3: $costo=$tarifa['precio_infante']; $acom='INF'; break;
													default: $costo=0; $acom='N/N'; break;
												}

												// TRANSFORMAR A DOLARES SI ESTA EN BOLIVIANOS
												if($tarifa['moneda'] == 'B')
												{
													$costo /= $tarifa['tipo_cambio'];
												}

												// SUMAR FEE
												if($tarifa['increment_fee'] == 'P')
												{
													$fee 	= 1 + ($tarifa['fee']/100);
													$costo 	*= $fee;
												}
												else
												{
													$fee 	= $tarifa['fee'];
													$costo 	+= $fee;
												}

												// SUMAR IVA
												if($tarifa['increment_factura'] == 'P')
												{
													$iva 	= 1 + ($tarifa['factura']/100);
													$costo 	*= $iva;
												}
												else
												{
													$iva 	= $tarifa['factura'];
													$costo 	+= $iva;
												}

												// REDONDEAMOS AL ENTERO INMEDIATO SUPERIOR
												$costo 		= ceil($costo);
											?>
												<tr class="<?php echo $color; ?>">
													<td><?php echo $cont_pax; ?></td>
													<td><a style="text-decoration:none;" href="ver_cotizacion_paquete.php?cotizacion=<?php echo $pax['idcotizacion']; ?>" target="_blank"><?php echo ucwords(strtolower($pax['nombre_cliente'] . ' ' . $pax['apellido_cliente'])); ?></a></td>
													<td><?php echo $acom; ?></td>
													<td><?php echo $costo; ?></td>
													<?php
														if($x == 0)
														{
															$fecha_limit 	= strtotime($tarifa['time_limit']);
															$fecha_actual 	= strtotime("now");
														?>
															<td rowspan="<?php echo count($pasajeros); ?>">
																<?php echo substr($tarifa['descripcion'], 0, 15); ?>
															</td>
															<td rowspan="<?php echo count($pasajeros); ?>">
																<?php echo date('d/M', strtotime($tarifa['fecha_desde'])); ?>
															</td>
															<td rowspan="<?php echo count($pasajeros); ?>">
																<?php echo date('d/M', strtotime($tarifa['fecha_hasta'])); ?>
															</td>
															<td rowspan="<?php echo count($pasajeros); ?>">
																<?php echo $tarifa['codigo']; ?>
															</td>
															<td class="col_time_limit" rowspan="<?php echo count($pasajeros); ?>" style="background-color:#FFCCCC; <?php if($fecha_actual >= $fecha_limit) { ?>color:#FF3300;<?php } ?>">
																<?php echo date('d/M', strtotime($tarifa['time_limit'])); ?>
															</td>
															<td style="line-height:1;" class="col_adjunto" rowspan="<?php echo count($pasajeros); ?>">
																<?php
																for ($nadj=0; $nadj < count($adjuntos); $nadj++) 
																{ 
																	$adjunto = $adjuntos[$nadj];
																?>
																	<a href="<?php echo $ruta_adjuntos . $adjunto['nombre']; ?>"><?php echo $adjunto['descripcion']; ?></a><br/>
																<?php
																}
																?>
															</td>
														<?php
														}
													?>
													<td class="col_pass"><?php echo $pax['numero_pasaporte_cliente']; ?></td>
													<td class="col_fnac"><?php echo date('d/m/Y', strtotime($pax['fecha_nacimiento_cliente'])); ?></td>
												</tr>
											<?php
												$cont_pax++;
											}
										}
									}
								}
							?>
						</table>
					<?php
						$i++;
					}
				?>

				<!-- TABLA DE INSCRIPCIONES (CODIGOS DE RESERVA) -->
				<?php
					if($total_columnas_inscripciones > 0)
					{
					?>
						<table class="tabla-resumen si" style="width:100%; margin-bottom:5px;">
							<tr>
								<th colspan="13" class="nombre_servicio">INSCRIPCIONES</th>
							</tr>
							<tr>
								<th>N</th>
								<th class="subtitle">PASAJERO</th>
								<th class="subtitle col_costo">COSTO<br/>$US</th>
								<th class="subtitle">CATEGORIA</th>
								<th class="subtitle col_time_limit">TIME<br/>LIMIT</th>
								<th class="subtitle col_adjunto">ADJUNTOS</th>
								<th class="subtitle col_telefono">TELEFONO</th>
								<th class="subtitle col_email">EMAIL</th>
								<th class="subtitle col_pass">PASS</th>
								<th class="subtitle col_fnac">FECHA<br/>NAC</th>
							</tr>
							<?php
								$color 		= 'gray';
								$cont_pax 	= 1;
								foreach ($a_inscripciones as $idevento => $tarifas) 
								{
									?>
										<tr>
											<td colspan="13" style="color:#10689b; font-weight:bold; text-align:center;">
												<?php echo strtoupper($tarifas[0]['nombre_evento']); ?>
											</td>
										</tr>
									<?php
									for ($k=0; array_key_exists($k, $tarifas); $k++) 
									{ 
										$tarifa 	= $tarifas[$k];
										$idtarifa 	= $tarifa['idinscripcion_evento'];

										$pasajeros 	= c_mostrar_cotizacion::obtener_pasajeros_por_servicio_inscripcion($idtarifa);
										$adjuntos 	= c_mostrar_cotizacion::mostrar_adjuntos_por_inscripcion($idtarifa);

										if(count($pasajeros) > 0)
										{
											if($color == 'gray')
												$color = 'white';
											else
												$color = 'gray';	
										}

										for ($x=0; $x < count($pasajeros); $x++) 
										{ 
											$pax 	= $pasajeros[$x];
											$costo 	= $tarifa['precio'];
											
											// TRANSFORMAR A DOLARES SI ESTA EN BOLIVIANOS
											if($tarifa['moneda'] == 'B')
											{
												$costo /= $tarifa['tipo_cambio'];
											}

											// SUMAR FEE
											if($tarifa['increment_fee'] == 'P')
											{
												$fee 	= 1 + ($tarifa['fee']/100);
												$costo 	*= $fee;
											}
											else
											{
												$fee 	= $tarifa['fee'];
												$costo 	+= $fee;
											}

											// SUMAR IVA
											if($tarifa['increment_factura'] == 'P')
											{
												$iva 	= 1 + ($tarifa['factura']/100);
												$costo 	*= $iva;
											}
											else
											{
												$iva 	= $tarifa['factura'];
												$costo 	+= $iva;
											}

											// REDONDEAMOS AL ENTERO INMEDIATO SUPERIOR
											$costo 		= ceil($costo);

											?>
												<tr class="<?php echo $color; ?>" >
													<td><?php echo $cont_pax; ?></td>
													<td><a style="text-decoration:none;" href="ver_cotizacion_paquete.php?cotizacion=<?php echo $pax['idcotizacion']; ?>" target="_blank"><?php echo ucwords(strtolower($pax['nombre_cliente'] . ' ' . $pax['apellido_cliente'])); ?></a></td>
													<td><?php echo $costo; ?></td>
													<?php
														if($x == 0)
														{
															$fecha_limit 	= strtotime($tarifa['time_limit']);
															$fecha_actual 	= strtotime("now");
														?>
															<td rowspan="<?php echo count($pasajeros); ?>">
																<?php echo strtoupper($tarifa['categoria']); ?>
															</td>
															<td class="col_time_limit" rowspan="<?php echo count($pasajeros); ?>" style="background-color:#FFCCCC; <?php if($fecha_actual >= $fecha_limit) { ?>color:#FF3300;<?php } ?>">
																<?php echo date('d/M', strtotime($tarifa['time_limit'])); ?>
															</td>
															<td style="line-height:1;" class="col_adjunto" rowspan="<?php echo count($pasajeros); ?>">
																<?php
																for ($nadj=0; $nadj < count($adjuntos); $nadj++) 
																{ 
																	$adjunto = $adjuntos[$nadj];
																?>
																	<a href="<?php echo $ruta_adjuntos . $adjunto['nombre']; ?>"><?php echo $adjunto['descripcion']; ?></a><br/>
																<?php
																}
																?>
															</td>
														<?php
														}
													?>
													<td class="col_telefono"><?php echo $info_contacto[$pax['idclientes']]['telefono']; ?></td>
													<td class="col_email"><?php echo $info_contacto[$pax['idclientes']]['correo']; ?></td>
													<td class="col_pass"><?php echo $pax['numero_pasaporte_cliente']; ?></td>
													<td class="col_fnac"><?php echo date('d/m/Y', strtotime($pax['fecha_nacimiento_cliente'])); ?></td>
												</tr>
											<?php
											$cont_pax++;
										}
									}
								}
							?>
						</table>
					<?php
					}
				?>

			</div>
	</div>

	<!-- PANEL PARA VISTAS DE LAS COTIZACIONES -->
	<div style="width:200px; float:left;">

		<!-- VER PRECIOS O RESERVAS -->
		<div style="margin:10px; padding:5px; background-color:#F1F1F1; border: 1px solid #AAA; border-radius:5px;">
			<input type="checkbox" checked onchange="$('.vista_precios').toggle();" />Vista de Precios<br/>
			<input type="checkbox" onchange="$('.vista_reserva').toggle();" />Vista de Reservas x Precios<br/>
		</div>

		<!-- VER POR TIPOS DE SERVICIO -->
		<div style="margin:10px; padding:5px; background-color:#F1F1F1; border: 1px solid #AAA; border-radius:5px;">
			<?php
				if($total_columnas_boletos_aereos > 0)
				{
				?>
					<input type="checkbox" checked id="chksb" onchange="mostrar_ocultar_servicio('sb');" value="<?php echo $total_columnas_boletos_aereos; ?>" />Aéreo<br/>
					<input style="margin-left:20px;" type="checkbox" checked id="chksbb" onchange="mostrar_ocultar_servicio('sbb');" value="<?php echo $total_columnas_boletos_aereos; ?>" />Aéreo Resumen<br/>
				<?php
				}
			?>

			<?php
				if($total_columnas_hoteles_in > 0)
				{
				?>
					<input type="checkbox" checked id="chksh" onchange="mostrar_ocultar_servicio('sh');" value="<?php echo $total_columnas_hoteles_in; ?>" />Hotel<br/>
				<?php
				}
			?>

			<?php
				for ($i=0; $i < count($subtotales_col_destinos); $i++) 
				{ 
					$aux = $subtotales_col_destinos[$i];
				?>
					<input type="checkbox" checked id="chk<?php echo $aux[2]; ?>" onchange="mostrar_ocultar_servicio('so<?php echo $i; ?>');" value="<?php echo $aux[1]; ?>" /><?php echo ucwords(strtolower($aux[0])); ?><br/>
				<?php
				}
			?>

			<?php
				if($total_columnas_inscripciones > 0)
				{
				?>
					<input type="checkbox" checked id="chksi" onchange="mostrar_ocultar_servicio('si');" value="<?php echo $total_columnas_inscripciones; ?>" />Inscripción<br/>
				<?php
				}
			?>
		</div>

		<!-- MOSTRAR TIME LIMIT -->
		<div style="margin:10px; padding:5px; background-color:#F1F1F1; border: 1px solid #AAA; border-radius:5px;">
			<input type="checkbox" checked onchange="$('.total_familia').toggle();" />Ver Total Familia<br/>
			<input type="checkbox" checked onchange="$('.col_time_limit').toggle();" />Ver Time Limit<br/>
			<input type="checkbox" checked onchange="$('.col_telefono').toggle();" />Ver Telefonos<br/>
			<input type="checkbox" checked onchange="$('.col_email').toggle();" />Ver Emails<br/>
			<input type="checkbox" checked onchange="$('.col_pass').toggle();" />Ver Pasaporte<br/>
			<input type="checkbox" checked onchange="$('.col_fnac').toggle();" />Ver Fecha Nac<br/>
			<input type="checkbox" checked onchange="$('.col_adjunto').toggle();" />Ver Adjuntos<br/>
			<input type="checkbox" checked onchange="$('.col_comentario').toggle();" />Ver Comentario<br/>
		</div>

	</div>

<div class="clear"></div>

<?php include('mostrar_cotizaciones_por_generico_impresion.php'); ?>

<div class="container">

<script type="text/javascript">
	


</script>

	<input type="button" value="Imprimir" onclick="Imprimir();" style="float:right;" />

	<div class="clear"></div>


<?php include("footer.php"); ?>