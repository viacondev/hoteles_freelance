<?php

include '../BD/controladoraBD.php';
include '../entidad/cotizacion.php';

if(isset($_POST['crear_cotizacion']))
{
	$idpublicacion 		= $_POST['c_paquete'];
	$idclientes 		= $_POST['c_cliente'];
	$titulo 			= $_POST['titulo'];
	$observaciones 		= $_POST['observaciones'];
	$idusuarios 		= $_POST['c_user'];
	$idsolicitante		= $_POST['c_solicitante'];

	$registro_cotizacion 	= cotizacion::registrar_nueva_cotizacion($idclientes, $idusuarios, $titulo, $observaciones); 
	if($registro_cotizacion !== false)
	{
		$idcotizacion 			= mysql_insert_id();

		if($idsolicitante != '')
		{
			$registro_solicitante 	= cotizacion::registrar_solicitante_de_cotizacion($idcotizacion, $idsolicitante);
		}

		$registro_cotizacion_publicacion 	= cotizacion::registrar_relacion_publicacion_cotizacion($idpublicacion, $idcotizacion);
		if($registro_cotizacion_publicacion !== false)
		{
			header('location:editar_cotizacion_paquete.php?idpaquete=' . $idcotizacion . '&vista=0');	
		}
		else
		{
			echo 'NO SE PUDO CREAR LA COTIZACION';
			exit();
		}
	}
	
}

?>
<link href="lib/tinybox2/style.css" rel="stylesheet">
<div class="container" >
<div class="sixteen columns" style="color:#FFFFFF; position:absolute; padding:10px; opacity:0.9; display:none;" id="mensaje_pax">
	</div>
	<div class="three columns">
		BUSCAR CLIENTE ... 
	</div>
	<div class="two columns">
		<input type="radio" value="natural" name="tipo_cliente" checked onchange="MostrarBusquedaClientes();" />Natural
	</div>
	<div class="three columns">
		<input type="radio" value="empresa_agencia" name="tipo_cliente" onchange="MostrarBusquedaClientes();" />Empresa o Agencia
	</div>

	<div class="clear_mayor"></div>

	<div class="three columns">
		<input type="text" placeholder="Nombre" id="nombre_cliente"/>
	</div>
	<div class="three columns">
		<input type="text" placeholder="Apellidos" id="apellido_cliente">
	</div>
	<div class="three columns">
		<input type="text" placeholder="Pasaporte" id="pasaporte_cliente">
	</div>
	<div class="four columns">
		<input type="button" onclick="BuscarClientes();" value="Buscar Cliente" />
		<input type="button" id="boton_buscar_solicitante" onclick="BuscarSolicitante();" value="Buscar Solicitante" style="display:none;" />
	</div>
	<div class="fifteen columns">
		<table style="width:100%;" style="border:1px solid #aaa;">
			<tr style="border:1px solid #aaa; background-color:#10689b; color:#ffffff; font-weight:bold;">
				<td style="width:70%; text-align:center;">CLIENTE</td>
				<td>PASAPORTE</td>
				<td></td>
			</tr>
		</table>
	</div>
	<div class="fifteen columns" id="filtro_clientes" style="height:150px; overflow:auto;">
		BUSQUEDA DE CLIENTES
	</div>

	<hr/>
	<div class="clear"></div>

	<div class="sixteen columns">
		<strong style="color:#10689b;">CLIENTE : </strong><strong id="cliente_seleccionado">NO SE HA SELECCIONADO NING&Uacute;N CLIENTE.</strong>
	</div>
	<div class="sixteen columns" id="panel_solicitante" style="display:none;">
		<strong style="color:#10689b;">SOLICITANTE : </strong><strong id="solicitante_seleccionado">NO SE HA SELECCIONADO EL SOLICITANTE.</strong>
	</div>
	<div class="sixteen columns" style="height:80px;">
		<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
			<input type="hidden" id="c_cliente" name="c_cliente" />
			<input type="hidden" id="c_solicitante" name="c_solicitante" />
			<input type="hidden" id="c_paquete" name="c_paquete" value="<?php echo $_GET['idpaquete']; ?>" />
			<input type="hidden" id="c_user" name="c_user" value="<?php echo $_GET['idusuarios']; ?>" />
			<div class="eigth columns">
				<input type="text" placeholder="Titulo de la Cotizacion" name="titulo" id="titulo" value="<?php echo strtoupper($_GET['titulo']); ?>" style="display:none; width:300px;"></textarea>	
			</div>
			<div class="eigth columns">
				<textarea placeholder="Observaciones" name="observaciones" id="observaciones" style="display:none; width:300px;"></textarea>	
			</div>
			<div class="eigth columns">
				<input type="submit" value="Crear Cotizaci&oacute;n" name="crear_cotizacion" id="mysubmit" style="display:none;" />	
			</div>
		</form>
		<!--<a href="editar_cotizacion_paquete.php?idpaquete=<?php echo $_GET['idpaquete']; ?>"><input type="button" value="Crear Cotizaci&oacute;n" style="float:right;" /></a>		-->
	</div>

</div>
