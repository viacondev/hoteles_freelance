<?php 
	include 'header.php';
?>

<form method="post" class="miform" action="registrar_nueva_promocion.php" >
	<div class="six columns">
		<h3>NUEVA PROMOCION</h3>
	</div>
	<div class="eight columns">
		<img src="images/travel.png" style="height:40px; float:left; display:inline;" />	
	</div>
	<div class="clear"></div>
	<div class="columns six" style="background-color:#e1e1e1; padding-top:10px; padding-bottom:10px; border:1px solid #AAA; border-radius:10px;">
		<h4>Datos Generales</h4>
		<div class="columns two" style="font-weight:bold; color:#10689b;">
			TITULO:
		</div>
		<div class="columns three">
			<input type="text" name="titulo"/>
		</div>
		<div class="clear"></div>
		<div class="columns two" style="font-weight:bold; color:#10689b;">
			DESDE USD:
		</div>
		<div class="columns three">
			<input type="text" name="precio_desde" />
		</div>
		<div class="clear"></div>
		<div class="columns two" style="font-weight:bold; color:#10689b;">
			FECHA VENC:
		</div>
		<div class="columns three">
			<input type="text" name="vencimiento" id="vencimiento" style="width:80%;" />
		</div>
		<div class="clear"></div>
		<div class="columns two" style="font-weight:bold; color:#10689b;">
			SALIDA DESDE:
		</div>
		<div class="columns three">
			<input type="text" name="salida" id="salida" style="width:80%;"/>
		</div>
		<div class="clear"></div>
		<div class="columns two" style="font-weight:bold; color:#10689b;">
			RETORNO HASTA:
		</div>
		<div class="columns three">
			<input type="text" name="retorno" id="retorno" style="width:80%;"/>
		</div>
		
	</div>

	<div class="columns nine" style="background-color:#e1e1e1; padding-top:10px; padding-bottom:10px; border:1px solid #AAA; border-radius:10px;">
		<div class="columns eight" style="font-weight:bold; color:#10689b;">
			CONTENIDO DEL PAQUETE:
		</div>
		<div class="seven columns">
			<input type="radio" name="solo_imagen" value="0" checked/>Descriptivo	
		</div>
		<div class="seven columns">
			<input type="radio" name="solo_imagen" value="1" />Como imagen
		</div>
	</div>

	<div class="columns nine" style="background-color:#fff;">
		<input type="submit" value="Crear Paquete" name="registrar_paquete" style="float:right; font-size:15pt; padding: 5px 15px; margin: 10px 20px;"/>
	</div>
</form>

<script type="text/javascript">
	$(function() {

	$.datepicker.regional['es'] =
  	{
	  	closeText: 'Cerrar',
	  	prevText: 'Previo',
	  	nextText: 'Próximo',
	  	monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
	  	monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
	  	monthStatus: 'Ver otro mes', yearStatus: 'Ver otro año',
	  	dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
	  	dayNamesShort: ['Dom','Lun','Mar','Mie','Jue','Vie','Sáb'],
	  	dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sa'],
	  	dateFormat: 'dd/mm/yy', firstDay: 0,
	  	initStatus: 'Selecciona la fecha', isRTL: false
	 };
	 	$.datepicker.setDefaults($.datepicker.regional['es']);

  		anioActual = new Date().getFullYear();
	    anioInicio = anioActual - 90;

	    $("#salida").datepicker({
	        showOn: "button",
	        buttonImage: "../images/calendar.jpg",
	        buttonImageOnly: true,
	        numberOfMonths: 3,
	        yearRange: anioInicio + ":2025",
	        constrainInput: false,
	        onClose: function(date) { $("#retorno").datepicker("option", "minDate", date); }
	    });  
	    $("#salida").datepicker("option", "dateFormat", 'dd/mm/yy');

	    $("#retorno").datepicker({
	        showOn: "button",
	        buttonImage: "../images/calendar.jpg",
	        buttonImageOnly: true,
	        numberOfMonths: 3,
	        yearRange: anioInicio + ":2025",
	        constrainInput: false
	    });  
	    $("#retorno").datepicker("option", "dateFormat", 'dd/mm/yy');

	    $("#vencimiento").datepicker({
	        showOn: "button",
	        buttonImage: "../images/calendar.jpg",
	        buttonImageOnly: true,
	        numberOfMonths: 3,
	        yearRange: anioInicio + ":2025",
	        constrainInput: false
	    });  
	    $("#vencimiento").datepicker("option", "dateFormat", 'dd/mm/yy');

  		var milisegundos = 900000;
		timer = setTimeout("tiempo()", milisegundos);
	});

	var milisegundos = 900000;

	function tiempo() {
	$.ajax({
            type: "POST",
            url: "../control/actualizar_sesion.php",
            data: "sesion=1",
            success: function(datos) {
                alert("Sesion actualizada");
            }
        }); 
	timer = setTimeout("tiempo()", milisegundos);
	}

</script>
<?php
	include("footer.php");
?>