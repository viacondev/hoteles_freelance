$(function()
{
    $('.icon_edit').attr('title', 'Editar');
    $('.icon_close').attr('title', 'Terminar Edicion');
    $('.icon_save').attr('title', 'Guardar');
    $('.icon_delete').attr('title', 'Quitar de la Lista');
});

function input_mask(d, type, nums)
{
    if(type == 'ruta')
    {
        var pat = new Array(3,3,3,3,3,3,3,3,3,3);
        var sep = '/';    
    }
    else if(type == 'fecha')
    {
        var pat = new Array(2,2,4);
        var sep = '/';   
    }
    else if(type == 'hora')
    {
        var pat = new Array(2,2);
        var sep = ':';
    }
    
    if(d.valant != d.value)
    {
        val     = d.value
        largo   = val.length
        val     = val.split(sep)
        val2    = ''
        for(r=0;r<val.length;r++)
        {
            val2 += val[r]  
        }
        if(nums)
        {
            for(z=0;z<val2.length;z++)
            {
                if(isNaN(val2.charAt(z)))
                {
                    letra = new RegExp(val2.charAt(z),"g")
                    val2 = val2.replace(letra,"")
                }
            }
        }
        val     = ''
        val3    = new Array()
        for(s=0; s<pat.length; s++)
        {
            val3[s] = val2.substring(0,pat[s])
            val2    = val2.substr(pat[s])
        }
        for(q=0;q<val3.length; q++)
        {
            if(q ==0)
            {
                val = val3[q]
            }
            else
            {
                if(val3[q] != "")
                {
                    val += sep + val3[q]
                }
            }
        }
        d.value     = val
        d.valant    = val
    }
}

function mostrar_url_popup(url, height) {
    TINY.box.show({ url: url, fixed: false, height: height });
}
function mostrar_html_popup(id) {
	TINY.box.show({ html: '<link href="lib/tinybox2/style.css" rel="stylesheet" /><div class="container" style="height:500px; overflow:auto;">' + $('#' + id).html() + '</div>' , fixed:false, height: 550});
}

/*
* FUNCION PARA RESTAR FECHAS
*/
    function diferencia_fechas_en_dias(fecha2, fecha1)
    {
        var a_fecha2    = fecha2.split('/');
        var a_fecha1    = fecha1.split('/');

        var time2   = new Date(parseInt(a_fecha2[2], 10), parseInt(a_fecha2[1], 10) - 1, parseInt(a_fecha2[0], 10));
        var time1   = new Date(parseInt(a_fecha1[2], 10), parseInt(a_fecha1[1], 10) - 1, parseInt(a_fecha1[0], 10));

        var diferencia = time2.getTime() - time1.getTime();
        var diferencia_dias = Math.floor(diferencia/(1000 * 60 * 60 * 24));

        return diferencia_dias;
    }

/*
* FUNCION PARA MOSTRAR CALENDARIO
*/
    function crear_calendario_cumpleanios_mm(id) 
    {
        anioActual = new Date().getFullYear();
        anioInicio = anioActual - 90;
        $("#" + id).datepicker({
            showOn: "button",
            buttonImage: "../images/calendar.jpg",
            buttonImageOnly: true,
            numberOfMonths: 3,
            yearRange: anioInicio + ":2025",
            constrainInput: false
        });  
        $("#" + id).datepicker("option", "dateFormat", 'dd/mm/yy');
    }
    function crear_calendario_time_limit()
    {
        $(".time_limit").datepicker({
                showOn: "button",
                buttonImage: "../images/calendar.jpg",
                buttonImageOnly: true,
                numberOfMonths: 3,
                yearRange: anioInicio + ":2025",
                constrainInput: false
            }); 
        $(".time_limit").datepicker("option", "dateFormat", 'dd/mm/yy');
    }
    function crear_calendario_time_limit_por_id(id)
    {
        $("#" + id).datepicker({
                showOn: "button",
                buttonImage: "../images/calendar.jpg",
                buttonImageOnly: true,
                numberOfMonths: 3,
                yearRange: anioInicio + ":2025",
                constrainInput: false
            }); 
        $("#" + id).datepicker("option", "dateFormat", 'dd/mm/yy');
    }

/***
* REGISTRO DE ADMINISTRACION
*/
    function eliminar_registro_adm(t, i, v)
    {
        if(confirm('Esta seguro de eliminar este registro ?'))
        {
            $.ajax({
                type: "POST",
                url: "../modelo_negocio/mdn_eliminar_registro_administracion.php",
                data: "t=" + t + "&i=" + i + "&v=" + v,
                success: function(datos) {
                    if(datos == 'CORRECTO')
                    {
                        mostrar_mensaje_correcto('Se elimino correctamente');
                        $('#' + t + v).remove();
                    }
                    else
                        mostrar_mensaje_error(datos);
                }
            });
        }
    }

/***
* FUNCION PARA BUSCAR CLIENTES
*/
    function BuscarClientes()
    {
        var nombre = $('#nombre_cliente').val();
        var apellido = $('#apellido_cliente').val();
        var pasaporte = $('#pasaporte_cliente').val();
        var tipo_cliente = 2;
        if($("input[name='tipo_cliente']:checked").val() == 'natural')
        {
            tipo_cliente = 1;
        }

        var longitud = nombre.length + apellido.length + pasaporte.length;
        if(longitud >= 1)
        {
            $.ajax({
                type: "POST",
                url: "../modelo_negocio/mdn_buscar_cliente.php",
                data: "nombre=" + nombre + "&apellido=" + apellido + "&pasaporte=" + pasaporte + "&tipo_cliente=" + tipo_cliente,
                success: function(datos) {
                        $('#filtro_clientes').html(datos);
                }
            });
        }
    }
    function BuscarSolicitante()
    {
        var cliente = $('#c_cliente').val();

        if(cliente != "")
        {
            $.ajax({
                type: "POST",
                url: "../modelo_negocio/mdn_buscar_cliente_relacion.php",
                data: "cliente=" + cliente,
                success: function(datos) {
                        $('#filtro_clientes').html(datos);
                }
            });
        }
        else
        {
            $('#mensaje_pax').html('Seleccione primero un cliente empresa.');
            $('#mensaje_pax').css('background-color', '#FF3300');
            $('#mensaje_pax').show();
            setTimeout(function() { $('#mensaje_pax').hide();}, 5000); 
        }
    }
    function MostrarBusquedaClientes()
    {
        $('#solicitante_seleccionado').html('NO SE HA SELECCIONADO EL SOLICITANTE.');
        $('#c_solicitante').val('');
        $('#cliente_seleccionado').html('NO SE HA SELECCIONADO NING&Uacute;N CLIENTE.');
        $('#observaciones').hide();
        $('#mysubmit').hide();

        if($("input[name='tipo_cliente']:checked").val() == 'natural')
        {
            $('#apellido_cliente').show();
            $('#pasaporte_cliente').show();
            $('#panel_solicitante').hide();
            $('#boton_buscar_solicitante').hide();
        }
        else
        {
            $('#apellido_cliente').hide();
            $('#pasaporte_cliente').hide();
            $('#panel_solicitante').show();
            $('#boton_buscar_solicitante').show();
        }
    }
    function SeleccionarCliente(nam, num)
    {
        $('#cliente_seleccionado').html(nam);
        $('#c_cliente').val(num);
        $('#mysubmit').show();
        $('#observaciones').show();
        $('#titulo').show();
    }
    function SeleccionarClienteSolicitante(nam, num)
    {
        $('#solicitante_seleccionado').html(nam);
        $('#c_solicitante').val(num);
    }

/*
* FUNCION PARA MOSTRAR U OCULTAR TARIFAS
*/
    function show_hide_tarifa(clase)
    {
        $('.' + clase).toggle();
        $('.p' + clase).slideToggle();
    }

/***
* FUNCIONES GENERALES PARA INTERFAZ
*/
    function Imprimir()
    {
        $(".no_myPrintArea").hide();
        $(".myPrintArea").printThis();
    }
    function mostrar_mensaje_correcto(mensaje)
    {
        $('#mensaje_correcto').html(mensaje);
        $('#mensaje_correcto').show();
        setTimeout(function() {
                    $('#mensaje_correcto').hide();}, 4000);

    }
    function mostrar_mensaje_error(mensaje)
    {
        $('#mensaje_error').html(mensaje);
        $('#mensaje_error').show();
        setTimeout(function() {
                    $('#mensaje_error').hide();}, 8000);
    }
    function mostrar_mensaje_correcto_tinybox(mensaje)
    {
        $('#mensaje_pax').html(mensaje);
        $('#mensaje_pax').css('background-color', '#00AA00');
        $('#mensaje_pax').show();
        setTimeout(function() { $('#mensaje_pax').hide();}, 4000);
    }
    function mostrar_mensaje_error_tinybox(mensaje)
    {
    	$('#mensaje_pax').html(mensaje);
        $('#mensaje_pax').css('background-color', '#FF3300');
        $('#mensaje_pax').show();
        setTimeout(function() { $('#mensaje_pax').hide();}, 4000);
    }
    function icon_del_ciudad_show(index)
    {
        $('#btn_del_ciudad_' + index).show();
        $('#lbl_del_ciudad_' + index).show();
    }
    function icon_del_ciudad_hide(index)
    {
        $('#btn_del_ciudad_' + index).hide();
        $('#lbl_del_ciudad_' + index).hide();
    }
    function revelar_icono_edicion(atributo)
    {
    	$('#icon_editar_' + atributo).show();
    }
    function ocultar_icono_edicion(atributo)
    {
    	$('#icon_editar_' + atributo).hide();
    }
    function revelar_icono_edicion_multiple(atributo)
    {
        $('#icon_editar_' + atributo).show();
    }
    function ocultar_icono_edicion_multiple(atributo)
    {
        $('#icon_editar_' + atributo).hide();
    }
    function mostrar_edicion(atributo)
    {
    	$('#' + atributo).hide();
    	$('#editar_' + atributo).show();
    }
    function ocultar_edicion(atributo)
    {
    	$('#editar_' + atributo).hide();
    	$('#' + atributo).show();
    }
    function mostrar_edicion_multiple(atributo)
    {
        $('#editar_' + atributo).show();
        $('.delete_' + atributo).show();
    }
    function ocultar_edicion_multiple(atributo)
    {
        $('#editar_' + atributo).hide();
        $('.delete_' + atributo).hide();
    }
    function validar_vista_tarifas_paquete()
    {
        $('.todo_precio').hide();
        switch($('input:radio[name=vista_precios]:checked').val())
        {
            case '1':
            {
                $('.solo_hotel').show();
                $('.solo_servicio').show();
                $('.solo_aereo').show();
            }
            break;
            case '2':
            {
                $('.hotel_mas_servicio').show();
                $('.solo_aereo').show();   
            }
            break;
            case '3':
            {
                $('.hotel_mas_servicio_mas_aereo').show(); 
            }
            break;
            case '4':
            {
                $('.hotel_comb_mas_servicio').show();
                $('.solo_aereo').show(); 
            }
            break;
            case '5':
            {
                $('.hotel_comb_mas_servicio_mas_aereo').show();    
            }
            break;
        }
    }
    function validar_vista_cotizacion()
    {
        if($('input:radio[name=vista_cotizacion_edicion]:checked').val() == 1)
        {
            $('.is_real1').hide();
            $('.is_real0').show();
            $('.tab_ciudad').css('background-color', '#F1F1F1');
        }
        else
        {
            $('.is_real0').hide();
            $('.is_real1').show();
            $('.tab_ciudad').css('background-color', '#DFFFC8');
        }
        $('.boton_cotizacion').toggle();
    }

/**
* MOSTRAR LOS ENVIOS REALIZADOS
*/
    function Mostrar_Envios_Realizados(p)
    {
        mostrar_url_popup('mostrar_envios_masivos.php?p=' + p, 550);
    }

/*
* TABS POR CUDAD DE ORIGEN
*/
    function RevelarTabCiudad(index)
    {
        $('.tab_ciudad').hide();
        $('#tab_ciudad_' + index).show();
        $('#tab_ciudad_print_' + index).show();
        $('.btn_c_o').css('background-color', '#AAAAAA');
        $('#btn_ciudad_origen_' + index).css('background-color', '#FF3300');

        $('.vista_opciones_aereo').html($('#opc_aereo_show' + index).html());
    }

/*
* TABS PARA VISTA REAL O COTIZACION
*/
    function RevelarTabVistaCotizacion(val)
    {
        $('.btn_vista').css('background-color', '#AAAAAA');
        if(val == 0)
        {
            $('.tab_cotizacion').hide();
            $('#botones_por_ciudad_origen').show();
            $('#vista_del_paquete').show();
            $('.vista_paquete').show();
            $('.vista_de_precios').show();
            $('.ver_cotizacion').css('background-color', '#FF3300');  
        }
        else
        {
            $('#botones_por_ciudad_origen').hide();
            $('.vista_paquete').hide();
            $('.vista_de_precios').hide();
            if(val == 1)
            {
                $('.tab_cotizacion').hide();
                $('#vista_cotizacion_pasajeros').show();
                $('.ver_pasajeros').css('background-color', '#FF3300'); 
            }
            else if(val == 2)
            {
                $('.tab_cotizacion').hide();
                $('#vista_requisitos_de_viaje').show();
                $('.ver_req_viaje').css('background-color', '#FF3300'); 
            }
            else if(val == 3)
            {
                $('.tab_cotizacion').hide();
                $('#vista_checklist_requisitos').show();
                $('.ver_req_x_pax').css('background-color', '#FF3300'); 
            }
        }
    }

/***
* FUNCIONES PARA ALMACENAR EN LA BD LA EDICION DE DATOS (GENÉRICO)
*/
    function guardar_atributo(tabla, atributo, idtabla, idobjeto)
    {
    	var valor = $('#valor_' + atributo).val();
        if(atributo == 'paginas_web')
            valor = encodeURIComponent(valor);
        if(atributo == 'fecha_salida' || atributo == 'fecha_retorno' || atributo == 'fecha_caducidad' || atributo == 'fecha_inicio' || atributo == 'fecha_fin')
        {
            var a_fecha = valor.split('/');
            if(a_fecha.length == 3)
                valor = a_fecha[2] + '-' + a_fecha[1] + '-' + a_fecha[0];
        }
    	$.ajax({
            type: "POST",
            url: "../control/registrar_atributo_en_tabla.php",
            data: "tabla=" + tabla + "&atributo=" + atributo + "&valor=" + valor + "&idtabla=" + idtabla + "&idobjeto=" + idobjeto,
            success: function(datos) {
                if(datos == "CORRECTO")
                {
                    ocultar_edicion(atributo);
                    if(atributo != 'descr_publicacion' && atributo != 'caracteristicas' && atributo != 'observacion' && atributo != 'observaciones' && atributo != 'datos_agente' && atributo != 'paquete_incluye' && atributo != 'importante' && atributo != 'paginas_web')
                    {
                        valor = $('#valor_' + atributo).val();
                        $('#datos_' + atributo).html(valor);
                    }
                	else
                    {
                        var items = $('#valor_' + atributo).val().split('\n');
                        var mi_html = "<ul class='lista_descripcion' id='lista_" + atributo + "'>";
                        for (var i = 0; i < items.length; i++) 
                        {
                            mi_html += "<li><a>" + items[i] + "</a></li>";
                        }
                        mi_html     += "</ul>";
                        $('#datos_' + atributo).html(mi_html);
                    }
                    mostrar_mensaje_correcto("Se guardo correctamente");
                }
                else
                {
                	mostrar_mensaje_error("Error al guardar");
                }
            }
        });
    }
    function guardar_atributo_x_ciudad_origen(tabla, atributo, idtabla, idobjeto, campo_ciudad)
    {
        var valor = $('#valor_' + campo_ciudad).val();
        
        if(atributo == 'fecha_salida' || atributo == 'fecha_retorno')
        {
            var a_fecha = valor.split('/');
            if(a_fecha.length == 3)
                valor = a_fecha[2] + '-' + a_fecha[1] + '-' + a_fecha[0];
        }

        $.ajax({
            type: "POST",
            url: "../control/registrar_atributo_en_tabla.php",
            data: "tabla=" + tabla + "&atributo=" + atributo + "&valor=" + valor + "&idtabla=" + idtabla + "&idobjeto=" + idobjeto,
            success: function(datos) {
                if(datos == "CORRECTO")
                {
                    ocultar_edicion(campo_ciudad);
                    
                    if(atributo == 'paquete_incluye' || atributo == 'importante' || atributo == 'datos_agente')
                    {
                        var items = $('#valor_' + campo_ciudad).val().split('\n');
                        var mi_html = "<ul class='lista_descripcion' id='lista_" + campo_ciudad + "'>";
                        for (var i = 0; i < items.length; i++) 
                        {
                            mi_html += "<li><a>" + items[i] + "</a></li>";
                        }
                        mi_html     += "</ul>";
                        $('#datos_' + campo_ciudad).html(mi_html);
                    }
                    else
                    {
                        valor = $('#valor_' + campo_ciudad).val();
                        $('#datos_' + campo_ciudad).html(valor);
                    }
                    
                    mostrar_mensaje_correcto("Se guardo correctamente");
                }
                else
                {
                    mostrar_mensaje_error("Error al guardar");
                }
            }
        });
    }
    function anular_publicacion(publicacion)
    {
        if(confirm("La publicación será anulada. Continuar?"))
        {
            $.ajax({
                type: "POST",
                url: "../control/registrar_atributo_en_tabla.php",
                data: "tabla=pqt_publicacion&atributo=estado&valor=0&idtabla=idpublicacion&idobjeto=" + publicacion,
                success: function(datos) {
                    if(datos == "CORRECTO")
                    {
                        $('#publicacion_' + publicacion).remove();
                        mostrar_mensaje_correcto('Se anuló correctamente.');
                    }
                    else
                        alert(datos);
                }
            });   
        }
    }
    function guardar_asociacion(tabla, id, atributo1, valor1, atributo2)
    {
        var valor2 = $('#valor_' + id).val();
        
        $.ajax({
            type: "POST",
            url: "../control/registrar_tabla_asociada.php",
            data: "tabla=" + tabla + "&atributo1=" + atributo1 + "&valor1=" + valor1 + "&atributo2=" + atributo2 + "&valor2=" + valor2,
            success: function(datos) {
                if(datos == "CORRECTO")
                {
                    ocultar_edicion_multiple(id);

                    var texto_seleccionado = $('#valor_' + id + ' option:selected').text();

                    var html_nuevo  = "<div id='" + id + "_" + valor2 + "'>";
                    html_nuevo          += texto_seleccionado;
                    html_nuevo          += "<img src='images/delete.png' class='delete_" + id + " icon_delete' onclick='quitar_asociacion(\"" + tabla + "\", \"" + id + "\", \"" + atributo1 + "\", \"" + valor1 + "\", \"" + atributo2 + "\", \"" + valor2 + "\");' >";
                    html_nuevo      += "</div>";

                    if( $('#contenedor_' + id).children().length == 0)
                        $('#contenedor_' + id).html("");

                    $('#contenedor_' + id).append(html_nuevo);
                    mostrar_mensaje_correcto('Agregado Correctamente');
                }
                else
                    alert(datos);
            }
        });
    }
    function quitar_asociacion(tabla, id, atributo1, valor1, atributo2, valor2)
    {
        $.ajax({
            type: "POST",
            url: "../control/eliminar_asociacion.php",
            data: "tabla=" + tabla + "&atributo1=" + atributo1 + "&valor1=" + valor1 + "&atributo2=" + atributo2 + "&valor2=" + valor2,
            success: function(datos) {
                if(datos == "CORRECTO")
                {
                    ocultar_edicion_multiple(id);
                    $('#' + id + '_' + valor2 ).remove();

                    if( $('#contenedor_' + id).children().length == 0)
                        $('#contenedor_' + id).html("- No hay ninguno -");

                    mostrar_mensaje_correcto('Eliminado Correctamente');
                }
                else
                    mostrar_mensaje_error(datos);
            }
        });
    }

/***
* GUARDADO DE IMAGENES DE PUBLICACIONES
*/
    function seleccionar_y_guardar_imagen(id, publicacion)
    {
        $("#" + id + "_loading").show();
        var archivos    = document.getElementById("archivo_" + id);
        var archivo     = archivos.files;

        var data = new FormData();

        for(i=0; i<archivo.length; i++)
        {
            data.append('archivo'+i,archivo[i]);
        }

        data.append('id',id);
        data.append('publicacion',publicacion);

        $.ajax({
            url:            '../control/guardar_imagen.php',
            type:           'POST',
            contentType:    false,
            data:           data,
            processData:    false,
            cache:          false
        }).done(function(msg){
            $("#" + id + "_loading").hide();
            $("#" + id + "_cargados").html(msg);
        });
    }
    function seleccionar_y_guardar_imagen_multiple(publicacion)
    {
        $("#imagen_contenido_loading").show();
        var archivos    = document.getElementById("archivo_imagen_contenido");
        var archivo     = archivos.files;

        var data = new FormData();

        for(i=0; i<archivo.length; i++)
        {
            data.append('archivo'+i,archivo[i]);
        }

        data.append('publicacion',publicacion);

        $.ajax({
            url:            '../control/guardar_imagen_multiple.php',
            type:           'POST',
            contentType:    false,
            data:           data,
            processData:    false,
            cache:          false
        }).done(function(msg){
            $("#imagen_contenido_loading").hide();
            $("#imagen_contenido_cargados").append(msg);
        });   
    }
    function seleccionar_y_guardar_archivo_multiple(publicacion)
    {
        var descripcion = $('#descripcion_archivo').val();
        if(descripcion!="")
        {
            $("#adjunto_loading").show();
            var archivos    = document.getElementById("archivo_adjunto");
            var archivo     = archivos.files;

            var data = new FormData();

            for(i=0; i<archivo.length; i++)
            {
                data.append('archivo'+i,archivo[i]);
            }

            var publicar = '0';
            if($("#archivo_publicar").is(':checked'))
                publicar = '1';

            data.append('publicacion',publicacion);
            data.append('descripcion',descripcion);
            data.append('publicar',publicar);

            $.ajax({
                url:            '../control/guardar_archivo_multiple.php',
                type:           'POST',
                contentType:    false,
                data:           data,
                processData:    false,
                cache:          false
            }).done(function(msg){
                $("#adjunto_loading").hide();
                $("#archivos_cargados").append(msg);
            });    
        }
        else
        {
            alert("Debe escribir alguna descripcion del archivo");  
            $('#archivo_adjunto').val('');
        }
    }
    function seleccionar_imagen_tipo_paquete()    // QUEDA PENDIENTE ¡¡¡¡¡¡¡¡¡¡¡¡¡  REVISAR   !!!!!!!!!!!!!!!!   
    {
        var archivos    = document.getElementById("archivo_imagen");
        var archivo     = archivos.files;

        var data = new FormData();

        for(i=0; i<archivo.length; i++)
        {
            data.append('archivo'+i,archivo[i]);
        }

        $.ajax({
            url:            '../control/mostrar_imagen_subida_tp.php',
            type:           'POST',
            contentType:    false,
            data:           data,
            processData:    false,
            cache:          false
        }).done(function(msg){
        $("#imagen_contenido").html(msg);
        });   
    }

/***
* ELIMINACION DE IMAGENES DE PUBLICACIÓN
*/
    function eliminar_imagen_multiple(imagen)
    {
        if(confirm("La imagen sera eliminada. Continura?"))
        {
            var data = new FormData();
            data.append('imagen',imagen);

            $.ajax({
                url:            '../control/eliminar_imagen.php',
                type:           'POST',
                contentType:    false,
                data:           data,
                processData:    false,
                cache:          false
            }).done(function(msg){
                 if(msg != "CORRECTO")
                    alert(msg);
                else
                {
                    $('#imagen_contenido_' + imagen).remove();
                }
            });    
        }
           
    }
    function eliminar_archivo_multiple(archivo, publicacion)
    {
        if(confirm("El archivo se eliminara. Continuar?"))
        {
            var data = new FormData();
            data.append('archivo',archivo);
            data.append('publicacion',publicacion);

            $.ajax({
                url:            '../control/eliminar_archivo.php',
                type:           'POST',
                contentType:    false,
                data:           data,
                processData:    false,
                cache:          false
            }).done(function(msg){
                 if(msg != "CORRECTO")
                    alert("ERROR");
                else
                {
                    $('#archivo_adjunto_' + archivo).remove();
                }
            });    
        }
    }

/*
* SE GUARDA SI UNA PUBLICACION SE MOSTRARÁ EN PRIMER PLANO
*/
    function GuardarResaltado(publicacion)
    {
        if($("#resaltar").is(':checked'))
            $('#valor_resaltar').val(1);
        else
            $('#valor_resaltar').val(0);
        guardar_atributo('pqt_publicacion', 'resaltar', 'idpublicacion', publicacion);
        mostrar_mensaje_correcto("Cambios realizados");
    }
    function GuardarPublicado(publicacion)
    {
        if($("#publicado").is(':checked'))
            $('#valor_publicado').val(1);
        else
            $('#valor_publicado').val(0);
        guardar_atributo('pqt_publicacion', 'publicado', 'idpublicacion', publicacion);
        mostrar_mensaje_correcto("Cambios realizados");
    }

/*
* GESTION DE PASAJEROS EN COTIZACIONES
*/
    function BuscarPasajeros()
    {
        var nombre      = $('#nombre_pax').val();
        var apellido    = $('#apellido_pax').val();
        var pasaporte   = $('#pasaporte_pax').val();
    	$('.resultado_pax').remove();
    	$('#img_load').show();
    	
        $.ajax({
            type: "POST",
            url: "../modelo_negocio/mdn_buscar_pasajeros.php",
            data: "nombre=" + nombre + "&apellido=" + apellido + "&pasaporte=" + pasaporte,
            success: function(datos) {
                $('#busqueda_pasajeros').append(datos);
    			$('#img_load').hide();
            }
        });
    }
    function SeleccionarPasajero(icl, ncl, pcl, vcl, nacl, scl)
    {
    	if(verificar_si_esta_seleccionado(icl))
    	{
    		alert("Error: El Pasajeros ya esta seleccionado.")
    	}
    	else
    	{
    		var tr_sel = $('#tr_pax_'+icl).html();
    		tr_sel = tr_sel.replace(/td_accion_seleccionar_/g, 'td_accion_eliminar_');
    		var html_tr = "<tr id='tr_pax_sel_"+icl+"' style='border:1px solid #aaa;'>"+tr_sel+"</tr>";
    		$('#table_pax_sel').prepend(html_tr);
    		var link_eliminar = "<input type='hidden' class='hd_pax_seleccionado' value='"+icl+"'/><a style='font-weight:bold; cursor:pointer;' onclick='eliminar_pax_seleccionado("+icl+");'>Eliminar</a>"; 
    		$('#td_accion_eliminar_'+icl).html(link_eliminar);
    		$('#hd_nro_pax_sel').val(parseFloat($('#hd_nro_pax_sel').val())+1);
    		$('#tr_pax_sel_' + icl).effect("highlight", {}, 1000);
    		$('#table_pax_sel').show();
    	}
    	
    }

/*
* GESTION DE CIUDADES DE ORIGEN
*/
    function AniadirCiudadDeOrigen(idpublicacion, type)
    {
        //mostrar_url_popup('registrar_nueva_ciudad_origen.php', 500);
        TINY.box.show({ url: 'registrar_nueva_ciudad_origen.php?pub=' + idpublicacion + '&typ=' + type, fixed: false, height: 200, openjs: function(){ crear_calendario_time_limit_por_id('fecha_salida_ciudad'); crear_calendario_time_limit_por_id('fecha_retorno_ciudad'); } });
    }
    function CrearCiudadPaquete()
    {
        var ciudad          = $('#ciudad').val();

        var fecha_salida    = $('#fecha_salida_ciudad').val();
        var a_fecha_salida  = fecha_salida.split('/');
        if(a_fecha_salida.length == 3)
            fecha_salida = a_fecha_salida[2] + '-' + a_fecha_salida[1] + '-' + a_fecha_salida[0];

        var fecha_retorno    = $('#fecha_retorno_ciudad').val();
        var a_fecha_retorno  = fecha_retorno.split('/');
        if(a_fecha_retorno.length == 3)
            fecha_retorno = a_fecha_retorno[2] + '-' + a_fecha_retorno[1] + '-' + a_fecha_retorno[0];

        var post    = $('#publicacion').val();
        var type    = $('#type').val();
        $.ajax(
        {
            type: "POST",
            url: "../modelo_negocio/mdn_registrar_ciudad_origen_paquete.php",
            data: "c=" + ciudad + "&s=" + fecha_salida + "&r=" + fecha_retorno + "&p=" + post + "&t=" + type,
            success: function(datos) { 
                if(datos.indexOf('CORRECTO') != -1)
                {
                    var idciudad = datos.split(':')[1];

                    fecha_salida = $('#fecha_salida_ciudad').val();
                    fecha_retorno    = $('#fecha_retorno_ciudad').val();

                    AgregarCiudadDeOrigen(idciudad, ciudad, fecha_salida, fecha_retorno, type);

                    parent.TINY.box.hide();
                }
            }
        });
    }
    function AgregarCiudadDeOrigen(ciudad, nciudad, salida, retorno, typ)
    {
        var ultimo_index = $('#c_ciudades_origen').val();

        var html = $('#plantilla_nueva_ciudad_origen').html().replace(/_ind_/g , ultimo_index);
        html = html.replace(/_idciudad_/g , ciudad);
        html = html.replace(/_fechasalida_/g , salida);
        html = html.replace(/_fecharetorno_/g , retorno);
        html = html.replace(/_typ_/g , typ);
        $('#tabs_por_ciudades').append(html);

        crear_calendario_time_limit_por_id('valor_fecha_salida_' + ciudad);
        crear_calendario_time_limit_por_id('valor_fecha_retorno_' + ciudad);

        var htmlb = $('#plantilla_boton_ciudad').html().replace(/_ind_/g , ultimo_index);
        htmlb = htmlb.replace(/_nombreciudad_/g , nciudad);
        $('#botones_por_ciudad_origen').append(htmlb);

        $('#c_ciudades_origen').val(parseInt(ultimo_index, 10) + 1);
    }
    function EliminarCiudadOrigen(ciudad, type, index)
    {
        if(confirm('Se Eliminara Permanentemente la Ciudad de Origen. Desea Continuar ?'))
        {
            var post = $('#id_publicacion').val();
            $.ajax(
            {
                type: "POST",
                url: "../modelo_negocio/mdn_eliminar_ciudad_origen.php",
                data: "c=" + ciudad + "&p=" + post + "&t=" + type,
                success: function(datos) { 
                    if(datos == 'CORRECTO')
                    {
                        mostrar_mensaje_correcto('Eliminado Correctamente');
                        $('#tab_ciudad_' + index).remove();
                        $('#btn_ciudad_origen_' + index).remove();
                    }
                    else
                    {
                        mostrar_mensaje_error('Debe Eliminar Tarifas Guardadas');
                    }
                }
            });
        }
    }

/*
* GESTION DE TARIFAS DE HOTEL
*/

    function avisar_cambio(x, i)
    {
        if(x=='H')
            $('#detalle_tarifa_' + i).css('border', '1px solid #FF0000');
    }

/*
* RECORRIDO DE ELEMENTOS GUARDAR TARIFA (HOTELES)
*/
    function guardar_tarifa(index)
    {
        var idtarifa    = $('#idth_' + index).val();
        var destino     = $('#tdi' + index).val();
        var orden       = $('#tdord' + index).val();
        var idciudad    = $('#idciudad_' + index).val();

        var data = {
            indext      : index,
            tarifa      : idtarifa,
            destino     : destino,
            orden       : orden,
            idciudad    : idciudad
        };


        var items = new Array();

        $('.item_tarifa_hotel_' + index).each(
            function(ind, val)
            { 
                var display = $(val).css('display');
                if(display != 'none')
                {
                    var id      = $(val).attr('id'); 
                    
                    var valor   = id.split('_'); 
                    var initem  = valor[1] + '_' + valor[2] + '_' + valor[3]; 
                    var iditem  = $('#iditem' + initem).val();
                    var identif = $('#iide_' + initem).val();
                    var hotel   = $('#iidhot_' + initem).val();
                    var a_in    = $('#tin' + initem).val().split('/');
                    var v_in    = a_in[2] + '-' + a_in[1] + '-' + a_in[0];
                    var a_out   = $('#tou' + initem).val().split('/');
                    var v_out   = a_out[2] + '-' + a_out[1] + '-' + a_out[0];
                    var sing    = $('#isgl' + initem).val();
                    var dobl    = $('#idbl' + initem).val();
                    var trip    = $('#itpl' + initem).val();
                    var cuad    = $('#icdpl' + initem).val();
                    var meno    = $('#imnr' + initem).val();
                    var infa    = $('#iinf' + initem).val();
                    var obse    = $('#iobs' + initem).val();
                    var link    = $('#ilnk' + initem).val();
                    var alim    = $('#ialm' + initem).val();
                    var coms    = $('#icms' + initem).val();
                    var inc_fee = $('input:radio[name=apfee' + initem + ']:checked').val();
                    var fact    = $('#iiva' + initem).val();
                    var inc_iva = $('input:radio[name=apiva' + initem + ']:checked').val();
                    if($('#itmlm_' + initem).val() != '')
                    {
                        var a_tl    = $('#itmlm_' + initem).val().split('/');
                        var tlim    = a_tl[2] + '-' + a_tl[1] + '-' + a_tl[0];    
                    }
                    else
                        var tlim    = '';
                    var esta    = 0;
                    if($('#est' + initem).is(':checked'))
                        esta    = 1;
                    var is_real = $('#hot_is_real' + initem).val();
                    var inex    = $('#iiex' + initem).val();

                    var item = {
                        index       : initem,
                        id          : iditem,
                        identif     : identif,
                        hotel       : hotel,
                        fecha_in    : v_in,
                        fecha_out   : v_out,
                        single      : sing,
                        doble       : dobl,
                        triple      : trip,
                        cuadruple   : cuad,
                        menor       : meno,
                        infante     : infa,
                        observacion : obse,
                        link        : link,
                        alimentacion: alim,
                        fee         : coms,
                        inc_fee     : inc_fee,
                        factura     : fact,
                        inc_iva     : inc_iva,
                        tlimit      : tlim,
                        info_extra  : inex,
                        is_real     : is_real,
                        estado      : esta
                    };
                    items.push(item);
                }  
            }
        );

        var delet = $('#items_eliminados' + index).html();

        $.ajax({
            type: "POST",
            url: "../modelo_negocio/mdn_registrar_tarifas.php",
            data: "data=" + JSON.stringify(data) + "&tarifas=" + JSON.stringify(items) + "&idel=" + delet,
            success: function(datos) { 
                var resultados = datos.split('?');
                var a_tarifa = resultados[0].split('/');
                $('#idth_' + a_tarifa[0]).val(a_tarifa[1]);

                var a_items = resultados[1].split(';');
                for (var i = 0; i < a_items.length; i++) 
                {
                    var a_item = a_items[i].split('/');
                    $('#iditem' + a_item[0]).val(a_item[1]);
                }
                mostrar_mensaje_correcto('La tabla ya ha sido guardada');

                $('#detalle_tarifa_' + index).css('border', '1px solid #008800');
            }
        });
    }

    function eliminar_item_hotel(index)
    {
        if(confirm("Esta seguro de eliminar esta opcion ?"))
        {
            var item = $('#iditem' + index).val();
            if(item != 0)
            {
                var array_index = index.split('_');
                $('#items_eliminados' + array_index[0] + '_' + array_index[1]).append(item + ',');
            }   
            mostrar_mensaje_correcto("La opción ya ha sido quitada completa y correctamente, para guardar los cambios en la tabla, haga click en 'Guardar Tabla'.");
            $('#item_' + index).remove(); 
            var a_index = index.split('_');
            $('#detalle_tarifa_' + a_index[0]).css('border', '1px solid #FF0000');
        }
    }

    function eliminar_tabla_tarifa(index)
    {
        if(confirm("Esta seguro de eliminar TODO el destino, mas sus tarifas ?,\n esta operación es irreversible."))
        {
            var item    = $('#idth_' + index).val();

            if(item != 0)
            {
                $.ajax({
                    type: "POST",
                    url: "../modelo_negocio/mdn_del_tarifa.php",
                    data: "tfa=" + item,
                    success: function(datos) {
                        mostrar_mensaje_correcto("La tabla ha sido eliminada completa y correctamente.");
                    }
                });
            }
            else
            {
                mostrar_mensaje_correcto("La tabla ha sido eliminada completa y correctamente.");
            }
            $('#tabla_tarifas_' + index).remove(); 
        }
    }

    function agregar_tabla_tarifa(ciudad, index)
    {
        var ultimo_index = $('#c_destinos_' + index).val();

        var html    = $('#plantilla_nueva_tabla_tarifa').html().replace(/_idth_/g , index + '_' + ultimo_index);
        html        = html.replace(/_idciudad_/g , ciudad);
        html        = html.replace(/_indd_/g , parseInt(ultimo_index, 10) + 2);
        $('#tarifas_hotel_' + index).append(html);
        $('#c_destinos_' + index).val(parseInt(ultimo_index, 10) + 1);
    }

    function agregar_item_hotel(index)
    {
        var ultimo_index    = $('#c_hoteles_' + index).val();
        var html            = $('#plantilla_nueva_opcion_hotel').html().replace(/_idth_/g,index);
        html                = html.replace(/_ind_/g , index + '_' + ultimo_index);
        html                = html.replace(/_valisreal_/g, 0);

        $('#detalle_tarifa_' + index).append(html);
        $('#c_hoteles_' + index).val(parseInt(ultimo_index, 10) + 1);
        crear_calendario_time_limit_por_id('itmlm_' + index + '_' + ultimo_index);
        crear_calendario_time_limit_por_id('tin' + index + '_' + ultimo_index);

        if($('#tin' + index + '_' + (ultimo_index-1)).length)
            $('#tin' + index + '_' + ultimo_index).val($('#tin' + index + '_' + (ultimo_index-1)).val());
        
        crear_calendario_time_limit_por_id('tou' + index + '_' + ultimo_index);
        
        if($('#tou' + index + '_' + (ultimo_index-1)).length)
            $('#tou' + index + '_' + ultimo_index).val($('#tou' + index + '_' + (ultimo_index-1)).val());
        
        $('#detalle_tarifa_' + index).css('border', '1px solid #FF0000');
    }

    function agregar_item_hotel_real(index)
    {
        var ultimo_index    = $('#c_hoteles_' + index).val();
        var html            = $('#plantilla_nueva_opcion_hotel').html().replace(/_idth_/g,index);
        html                = html.replace(/_ind_/g , index + '_' + ultimo_index);
        html                = html.replace(/_valisreal_/g, 1);

        $('#detalle_tarifa_' + index).append(html);
        $('#c_hoteles_' + index).val(parseInt(ultimo_index, 10) + 1);
        crear_calendario_time_limit_por_id('itmlm_' + index + '_' + ultimo_index);
        crear_calendario_time_limit_por_id('tin' + index + '_' + ultimo_index);
        crear_calendario_time_limit_por_id('tou' + index + '_' + ultimo_index);
        $('#detalle_tarifa_' + index).css('border', '1px solid #FF0000');
    }

    function editar_combinacion_hoteles(idciudad)
    {
        mostrar_url_popup('editar_combinaciones_hoteles.php?c=' + idciudad, 550);
    }

    function ConvertirHotelAReserva(indexhotel, indextarifa)
    {
        if(confirm('Se creará una copia de esta opción. Continuar ?'))
        {
            var ultimo_index    = $('#c_hoteles_' + indextarifa).val();
            var html            = $('#plantilla_nueva_opcion_hotel').html().replace(/_idth_/g,indextarifa);
            html                = html.replace(/_ind_/g , indextarifa + '_' + ultimo_index);
            html                = html.replace(/_valisreal_/g, 1);

            $('#detalle_tarifa_' + indextarifa).append(html);
            $('#c_hoteles_' + indextarifa).val(parseInt(ultimo_index, 10) + 1);
            crear_calendario_time_limit_por_id('itmlm_' + indextarifa + '_' + ultimo_index);
            crear_calendario_time_limit_por_id('tin' + indextarifa + '_' + ultimo_index);
            crear_calendario_time_limit_por_id('tou' + indextarifa + '_' + ultimo_index);
            
            // COPIAMOS LOS VALORES DEL ANTERIOR HOTEL
            var newindex = indextarifa + '_' + ultimo_index;
            $('#item_' + newindex).hide();
            $('#hotel_' + newindex).val($('#hotel_' + indexhotel).val());
            $('#iidhot_' + newindex).val($('#iidhot_' + indexhotel).val());
            $('#tin' + newindex).val($('#tin' + indexhotel).val());
            $('#tou' + newindex).val($('#tou' + indexhotel).val());
            $('#cantnoches' + newindex).html($('#cantnoches' + indexhotel).html());
            $('#iide_' + newindex).val($('#iide_' + indexhotel).val());
            $('#itmlm_' + newindex).val($('#itmlm_' + indexhotel).val());
            $('#isgl' + newindex).val($('#isgl' + indexhotel).val());
            $('#idbl' + newindex).val($('#idbl' + indexhotel).val());
            $('#itpl' + newindex).val($('#itpl' + indexhotel).val());
            $('#icdpl' + newindex).val($('#icdpl' + indexhotel).val());
            $('#imnr' + newindex).val($('#imnr' + indexhotel).val());
            $('#iinf' + newindex).val($('#iinf' + indexhotel).val());
            $('#icms' + newindex).val($('#icms' + indexhotel).val());
            $('#iiva' + newindex).val($('#iiva' + indexhotel).val());
            $('#ialm' + newindex).val($('#ialm' + indexhotel).val());
            $('#iiex' + newindex).val($('#iiex' + indexhotel).val());
            $('#ilnk' + newindex).val($('#ilnk' + indexhotel).val());
            $('#iobs' + newindex).val($('#iobs' + indexhotel).val());
            $('#tot-sgl' + newindex).val($('#tot-sgl' + indexhotel).val());
            $('#tot-dbl' + newindex).val($('#tot-dbl' + indexhotel).val());
            $('#tot-tpl' + newindex).val($('#tot-tpl' + indexhotel).val());
            $('#tot-cdpl' + newindex).val($('#tot-cdpl' + indexhotel).val());
            $('#tot-mnr' + newindex).val($('#tot-mnr' + indexhotel).val());
            $('#tot-inf' + newindex).val($('#tot-inf' + indexhotel).val());
            if($('input:radio[name=apfee' + indexhotel + ']:checked').val() == 'M')
                $('input:radio[name=apfee' + newindex + '][value=M]').prop('checked', 'checked');
            else
                $('input:radio[name=apfee' + newindex + '][value=P]').prop('checked', 'checked');
            if($('input:radio[name=apiva' + indexhotel + ']:checked').val() == 'M')
                $('input:radio[name=apiva' + newindex + '][value=M]').prop('checked', 'checked');
            else
                $('input:radio[name=apiva' + newindex + '][value=P]').prop('checked', 'checked');
            // TERMINA LA COPIA DE VALORES
            
            $('#detalle_tarifa_' + indextarifa).css('border', '1px solid #FF0000');    

            mostrar_mensaje_correcto('Se creó la reserva, verificar en la sección de Reservas.');
        }
    }

/*
* GESTION DE OTROS SERVICIOS COTIZADOS
*/
    function buscar_otros_servicios(index)
    {
        var lgt = $('#inser' + index).val().length;
        if(lgt>0 && (lgt%3)==0)
        {
            var n = $('#inser' + index).val();
            $('#busqueda_otro_servicio_' + index).show();
            $.ajax({
                type    : "POST",
                url     : "../modelo_negocio/mdn_buscar_otro_servicio.php",
                data    : "i=" + index + "&n=" + n,
                success : function(datos)
                {
                    $('#panel_busqueda_otro_servicio_' + index).html(datos);
                }
            });
        }
    }

    function seleccionar_servicio(nm, id, index)
    {
        $('#inser' + index).val(nm);
        $('#iidsr' + index).val(id);
        $('#busqueda_otro_servicio_' + index).hide();
    }

    function agregar_otro_servicio(ciudad, index)
    {
        var ultimo_index    = $('#c_otras_tarifas_' + index).val();
        var html            = $('#plantilla_nueva_otra_tarifa').html().replace(/_ind_/g, index + '_' + ultimo_index);
        html                = html.replace(/_cls_/g, 'tarifa_otros_servicios_' + index);
        html                = html.replace(/_idciudad_/g, ciudad);
        html                = html.replace(/_indexcity_/g, index);
        html                = html.replace(/_valisreal_/g, 0);

        $('#detalle_otras_tarifas_' + index).append(html);
        crear_calendario_time_limit_por_id('iotli' + index + '_' + ultimo_index);
        crear_calendario_time_limit_por_id('iofd' + index + '_' + ultimo_index);
        crear_calendario_time_limit_por_id('iofh' + index + '_' + ultimo_index);
        $('#c_otras_tarifas_' + index).val(parseInt(ultimo_index, 10) + 1);
        $('#detalle_otras_tarifas_' + index).css('border', '1px solid #FF0000');
    }

    function agregar_otro_servicio_real(ciudad, index)
    {
        var ultimo_index    = $('#c_otras_tarifas_' + index).val();
        var html            = $('#plantilla_nueva_otra_tarifa').html().replace(/_ind_/g, index + '_' + ultimo_index);
        html                = html.replace(/_cls_/g, 'tarifa_otros_servicios_' + index);
        html                = html.replace(/_idciudad_/g, ciudad);
        html                = html.replace(/_valisreal_/g, 1);
        
        $('#detalle_otras_tarifas_' + index).append(html);
        crear_calendario_time_limit_por_id('iotli' + index + '_' + ultimo_index);
        crear_calendario_time_limit_por_id('iofd' + index + '_' + ultimo_index);
        crear_calendario_time_limit_por_id('iofh' + index + '_' + ultimo_index);
        $('#c_otras_tarifas_' + index).val(parseInt(ultimo_index, 10) + 1);
        $('#detalle_otras_tarifas_' + index).css('border', '1px solid #FF0000');
    }

    function eliminar_otro_servicio(index)
    {
        if(confirm("Se quitará toda la fila. Continuar ?"))
        {
            var id = $('#idotra_tarifa' + index).val();
            var index_ciudad = index.split('_')[0];
            if(id != 0)
            {
                $('#eliminar_otras_tarifas_' + index_ciudad).append(id + ',');
            }
            mostrar_mensaje_correcto("El servicio ha sido quitada completa y correctamente, para guardar los cambios en la tabla, haga click en 'Guardar Cambios'.");
            $('#otroservicio_' + index).remove();
            $('#detalle_otras_tarifas_' + index_ciudad).css('border', '1px solid #FF0000');    
        }
    }

    function guardar_otros_servicios(index_ciudad)
    {
        var servicios = new Array();
        $('.tarifa_otros_servicios_' + index_ciudad).each(
            function(ind, val)
            { 
                var display = $(val).css('display');
                if(display != 'none')
                {
                    var id      = $(val).attr('id'); 
                    var valor   = id.split('_'); 
                    var index   = valor[1] + '_' + valor[2];
                    var ciudad  = $('#iidscd' + index).val();
                    var codigo  = $('#icodotr' + index).val();
                    var idsrv   = $('#idotra_tarifa' + index).val();
                    var desc    = $('#idesotr' + index).val();
                    var city    = $('#iociudad' + index).val();
                    var a_td    = $('#iofd' + index).val().split('/');
                    var v_td    = a_td[2] + '-' + a_td[1] + '-' + a_td[0];
                    var a_th    = $('#iofh' + index).val().split('/');
                    var v_th    = a_th[2] + '-' + a_th[1] + '-' + a_th[0]; 
                    var serv    = $('#iidsr' + index).val();
                    var mnda    = $('#imnda' + index).val();
                    var nadt    = $('#inadt' + index).val();
                    var nmen    = $('#inmen' + index).val();
                    var ninf    = $('#ininf' + index).val();
                    var ofee    = $('#iofee' + index).val();
                    var inc_fee = $('input:radio[name=sapfee' + index + ']:checked').val();
                    var ofac    = $('#iofac' + index).val();
                    var inc_iva = $('input:radio[name=sapiva' + index + ']:checked').val();
                    var a_tl    = $('#iotli' + index).val().split('/');
                    var v_tl    = a_tl[2] + '-' + a_tl[1] + '-' + a_tl[0];
                    var oob     = $('#ioob' + index).val();
                    var is_real = $('#ser_is_real' + index).val();
                    var est     = 0;
                    if($('#iest' + index).is(':checked'))
                        est     = 1;

                    var serv = {
                        index       : index,
                        id          : idsrv,
                        ciudad      : ciudad,
                        codigo      : codigo,
                        servicio    : serv,
                        descripcion : desc,
                        city        : city,
                        fecha_desde : v_td,
                        fecha_hasta : v_th,
                        moneda      : mnda,
                        adulto      : nadt,
                        menor       : nmen,
                        infante     : ninf,
                        fee         : ofee,
                        inc_fee     : inc_fee,
                        factura     : ofac,
                        inc_iva     : inc_iva,
                        tlimit      : v_tl,
                        observacion : oob,
                        is_real     : is_real,
                        estado      : est
                    };
                    servicios.push(serv);
                }
            }
        );

        var delet = $('#eliminar_otras_tarifas_' + index_ciudad).html();

        $.ajax({
            type: "POST",
            url: "../modelo_negocio/mdn_registrar_otros_servicios.php",
            data: "tarifas=" + JSON.stringify(servicios) + "&idel=" + delet,
            success: function(datos) {
                var a_serv = datos.split(';');

                for (var i = 0; i < a_serv.length; i++) 
                {
                    var a_srv = a_serv[i].split('/');
                    $('#idotra_tarifa' + a_srv[0]).val(a_srv[1]);
                }
                mostrar_mensaje_correcto('La tabla ya ha sido guardada');
                $('#detalle_otras_tarifas').css('border', '1px solid #008800');
            }
        });
    }

    function ConvertirServicioAReserva(ciudad, indexserv ,index)
    {
        if(confirm('Se creará una copia de este servicio. Continuar ?'))
        {
            var ultimo_index    = $('#c_otras_tarifas_' + index).val();
            var html            = $('#plantilla_nueva_otra_tarifa').html().replace(/_ind_/g, index + '_' + ultimo_index);
            html                = html.replace(/_cls_/g, 'tarifa_otros_servicios_' + index);
            html                = html.replace(/_idciudad_/g, ciudad);
            html                = html.replace(/_valisreal_/g, 1);
            $('#detalle_otras_tarifas_' + index).append(html);
            crear_calendario_time_limit_por_id('iotli' + index + '_' + ultimo_index);
            $('#c_otras_tarifas_' + index).val(parseInt(ultimo_index, 10) + 1);

            // COPIAMOS LOS VALORES DEL ANTERIOR SERVICIO
            var newindex = index + '_' + ultimo_index;
            $('#otroservicio_' + newindex).hide();
            $('#icodotr' + newindex).val($('#icodotr' + indexserv).val());
            $('#inser' + newindex).val($('#inser' + indexserv).val());
            $('#idesotr' + newindex).val($('#idesotr' + indexserv).val());
            $('#iidsr' + newindex).val($('#iidsr' + indexserv).val());
            $('#iociudad' + newindex).val($('#iociudad' + indexserv).val());
            $('#iidscd' + newindex).val($('#iidscd' + indexserv).val());
            $('#inadt' + newindex).val($('#inadt' + indexserv).val());
            $('#inmen' + newindex).val($('#inmen' + indexserv).val());
            $('#ininf' + newindex).val($('#ininf' + indexserv).val());
            $('#iofee' + newindex).val($('#iofee' + indexserv).val());
            $('#iofac' + newindex).val($('#iofac' + indexserv).val());
            if($('input:radio[name=sapfee' + indexserv + ']:checked').val() == 'M')
                $('input:radio[name=sapfee' + newindex + '][value=M]').prop('checked', 'checked');
            else
                $('input:radio[name=sapfee' + newindex + '][value=P]').prop('checked', 'checked');
            if($('input:radio[name=sapiva' + indexserv + ']:checked').val() == 'M')
                $('input:radio[name=sapiva' + newindex + '][value=M]').prop('checked', 'checked');
            else
                $('input:radio[name=sapiva' + newindex + '][value=P]').prop('checked', 'checked');
            $('#iotli' + newindex).val($('#iotli' + indexserv).val());
            $('#iofd' + newindex).val($('#iofd' + indexserv).val());
            $('#iofh' + newindex).val($('#iofh' + indexserv).val());
            $('#imnda' + newindex).val($('#imnda' + indexserv).val());
            $('#tot-oadt' + newindex).html($('#tot-oadt' + indexserv).html());
            $('#tot-omen' + newindex).html($('#tot-omen' + indexserv).html());
            $('#tot-oinf' + newindex).html($('#tot-oinf' + indexserv).html());
            $('#icodotr' + newindex).val($('#icodotr' + indexserv).val());
            // 

            $('#detalle_otras_tarifas_' + index).css('border', '1px solid #FF0000');

            mostrar_mensaje_correcto('Se creó la reserva, verificar en la sección de Reservas.');
        }
    }

/*
* GESTION DE INSCRIPCIONES
*/
    function agregar_inscripcion()
    {
        var ultimo_index    = $('#c_inscripciones').val();
        var html            = $('#plantilla_nueva_inscripcion').html().replace(/_ind_/g, ultimo_index);
        html                = html.replace(/_cls_/g, 'tarifa_inscripciones');
        $('#detalle_inscripciones').append(html);
        crear_calendario_time_limit_por_id('iinstli' + ultimo_index);
        $('#c_inscripciones').val(parseInt(ultimo_index, 10) + 1);
        $('#detalle_inscripciones').css('border', '1px solid #FF0000');
    }
    
    function eliminar_inscripcion(index)
    {
        if(confirm("Se quitará toda la fila. Continuar ?"))
        {
            var id = $('#idins_evento' + index).val();
            if(id != 0)
            {
                $('#eliminar_inscripciones').append(id + ',');
            }
            mostrar_mensaje_correcto("El servicio ha sido quitada completa y correctamente, para guardar los cambios en la tabla, haga click en 'Guardar Cambios'.");
            $('#inscripcion_' + index).remove();
            $('#detalle_inscripciones').css('border', '1px solid #FF0000');    
        }
    }

    function buscar_eventos(index)
    {
        var lgt = $('#ineve' + index).val().length;
        if(lgt>0 && (lgt%3)==0)
        {
            var n = $('#ineve' + index).val();
            $('#busqueda_evento_' + index).show();
            $.ajax({
                type    : "POST",
                url     : "../modelo_negocio/mdn_buscar_evento.php",
                data    : "i=" + index + "&n=" + n,
                success : function(datos)
                {
                    $('#panel_busqueda_evento_' + index).html(datos);
                }
            });
        }
    }

    function seleccionar_evento(nm, id, index)
    {
        $('#ineve' + index).val(nm);
        $('#iinsidev' + index).val(id);
        $('#busqueda_evento_' + index).hide();
    }

    function guardar_inscripciones()
    {
        var cotizacion      = $('#publicacion').val();
    
        var inscripciones   = new Array();
        $('.tarifa_inscripciones').each(
            function(ind, val)
            { 
                var id      = $(val).attr('id'); 
                var valor   = id.split('_'); 
                var index   = valor[1];
                var idinscripcion   = $('#idins_evento' + index).val();
                var idevento        = $('#iinsidev' + index).val(); 
                var categoria       = $('#icatins' + index).val();
                var precio          = $('#iinspre' + index).val();
                var moneda          = $('#iinsmnda' + index).val();
                var fee             = $('#iinsfee' + index).val();
                var inc_fee         = $('input:radio[name=insapfee' + index + ']:checked').val();
                var factura         = $('#iinsfac' + index).val();
                var inc_iva         = $('input:radio[name=insapiva' + index + ']:checked').val();
                var observacion     = $('#iinsob' + index).val();
                var a_tlim          = $('#iinstli' + index).val().split('/');
                var time_limit      = a_tlim[2] + '-' + a_tlim[1] + '-' + a_tlim[0];
                var estado          = 0;
                if($('#iinsest' + index).is(':checked'))
                    estado  = 1;

                var inscripcion = {
                    index           : index,
                    idinscripcion   : idinscripcion,
                    idevento        : idevento,
                    categoria       : categoria,
                    precio          : precio,
                    moneda          : moneda,
                    fee             : fee,
                    inc_fee         : inc_fee,
                    factura         : factura,
                    inc_iva         : inc_iva,
                    observacion     : observacion,
                    time_limit      : time_limit,
                    estado          : estado
                };
                inscripciones.push(inscripcion);
            }
        );
    
        var delet = $('#eliminar_inscripciones').html();
    
        $.ajax({
            type: "POST",
            url: "../modelo_negocio/mdn_registrar_inscripcion.php",
            data: "c=" + cotizacion + "&i=" + JSON.stringify(inscripciones) + "&idel=" + delet,
            success: function(datos) {
                var a_serv = datos.split(';');
    
                for (var i = 0; i < a_serv.length; i++) 
                {
                    var a_srv = a_serv[i].split('/');
                    $('#idins_evento' + a_srv[0]).val(a_srv[1]);
                }
                mostrar_mensaje_correcto('Las inscripciones fueron guardadas.');
                $('#detalle_inscripciones').css('border', '1px solid #008800');
            }
        });
    }
    
    function guardar_inscripcion_pax(idpax)
    {       
        var idinscripcion_cotizado  = $('#cb_inscripcion').val();
        var tipo_precio             = "1";
        if(parseFloat(idinscripcion_cotizado) > 0)
        {
            if(validar_agregar_inscripcion(idinscripcion_cotizado))
            {
                alert('Error: La categoria ya esta seleccionado.'); 
            }
            else
            {
                $('#table_tiny_inscripcion_'+idpax).html('');
                $('#img_load_inscripcion_pax').show();
                var consulta = "guardar";
                var tipo = $('#hd_tipo_de_cotizacion').val();
                $.ajax(
                {
                    type: "POST",
                    url: "../modelo_negocio/mdn_obtener_inscripcion_pax.php",
                    data: "idinscripcion_cotizado=" + idinscripcion_cotizado + "&idpax=" + idpax + "&tipo_precio=" + tipo_precio + "&consulta=" + consulta + "&tipo=" + tipo,
                    success: function(datos) 
                    {
                        $('#table_tiny_inscripcion_'+idpax).append(datos);
                        $('#img_load_inscripcion_pax').hide();
                    }
                });
            }
        }
        else
        {
            alert('ERROR: Seleccione la categoria de la inscripcion.');
        }       
    }
    
    function validar_agregar_inscripcion(idinscripcion_cotizado)
    {
        if ($('#hd_inscripcion_cotizado_'+idinscripcion_cotizado).length > 0) 
        {
            return true;
        }
        return false;
    }
    
    function eliminar_inscripcion_pax(idinscripcion_cotizado,idpax)
    {
        $('#table_tiny_inscripcion_'+idpax).html('');
        var consulta = "eliminar";
        var tipo = $('#hd_tipo_de_cotizacion').val();
        $.ajax(
        {
            type: "POST",
            url: "../modelo_negocio/mdn_obtener_inscripcion_pax.php",
            data: "idinscripcion_cotizado=" + idinscripcion_cotizado + "&idpax=" + idpax + "&consulta=" + consulta + "&tipo=" + tipo,
            success: function(datos) 
            {
                $('#table_tiny_inscripcion_'+idpax).append(datos);
                $('#img_load_inscripcion_pax').hide();
            }
        });
    }

/*
* GESTION DE TARIFAS DE BOLETOS AEREOS + ITINERARIO
*/
    function agregar_tarifa_aereo(ciudad, index)
    {
        var ultimo_index = $('#c_tarifa_aereo_' + index).val();
        var html = $('#plantilla_nueva_opcion_aereo').html().replace(/_ind_/g, index + '_' + ultimo_index);
        html = html.replace(/_idciudad_/g, ciudad);
        $('#tarifas_aereo_' + index).append(html);
        $('#c_tarifa_aereo_' + index).val(parseInt(ultimo_index, 10) + 1);
        $('#opcionaereo_' + index + '_' + ultimo_index).css('border', '1px solid #FF0000');
    }

    function guardar_tarifa_aereo(ind)
    {
        var idtar_bol   = $('#idtb_' + ind).val();
        var idciudad    = $('#idbcdd' + ind).val();
        var salida      = $('#salbol' + ind).val();
        var descripcion = $('#opcbol' + ind).val();
        var estado      = 0;
        if($('#iestaereo' + ind).is(':checked'))
            estado = 1;

        var tarifabol   = {
            idtarifa    : idtar_bol,
            idciudad    : idciudad,
            salida      : salida,
            descripcion : descripcion,
            estado      : estado
        };

        $.ajax({
            type: "POST",
            url: "../modelo_negocio/mdn_registrar_opcion_aereo.php",
            data: "tarifa=" + JSON.stringify(tarifabol) ,
            success: function(datos) {
    
                if(!isNaN(parseInt(datos, 10)) && isFinite(datos))
                {
                    var idtarifa    = parseInt(datos, 10);
                    $('#idtb_' + ind).val(idtarifa);

                    guardar_boletos_cotizados(ind);
                    guardar_itinerario(ind);

                    mostrar_mensaje_correcto('Las rutas ya han sido guardadas');
                    $('#opcionaereo_' + ind).css('border', '1px solid #008800');    
                }
                else
                {
                    mostrar_mensaje_error('No se pudo guardar la tarifa');
                }
            }
        });
    }

    function eliminar_opcion_aereo(i)
    {
        if(confirm('Se eliminará los precios mas el itinerario. Continuar ?'))
        {
            var idtarifa    = $('#idtb_' + i).val();

            if(idtarifa != 0)
            {
                $.ajax({
                    type: "POST",
                    url: "../modelo_negocio/mdn_eliminar_opcion_aereo.php",
                    data: "i=" + idtarifa,
                    success: function(datos) {
                        if(datos == 'CORRECTO')
                        {
                            mostrar_mensaje_correcto('Eliminado Correctamente');
                            $('#opcionaereo_' + i).remove();
                        }
                        else
                        {
                            mostrar_mensaje_error('No se pudo eliminar');
                        }
                    }
                });           
            }
            else
            {
                mostrar_mensaje_correcto('Quitado Correctamente');
                $('#opcionaereo_' + i).remove();
            }
        }
    }

/*
* GESTION DE BOLETOS COTIZADOS
*/
    function agregar_boleto_cotizado(i)
    {
        var ultimo_index    = $('#c_boletos_cotizados' + i).val();
        var html            = $('#plantilla_nuevo_boleto_cotizado').html().replace(/_ind_/g, i + '_' + ultimo_index);
        html                = html.replace(/_indopc_/g, i);
        html                = html.replace(/_ord_/g, parseInt(ultimo_index, 10) + 1 );
        html                = html.replace(/_cls_/g, 'boleto_cotizado' + i);
        html                = html.replace(/_valisreal_/g, 0);

        $('#detalle_boletos_cotizados' + i).append(html);
        crear_calendario_time_limit_por_id('iotlires' + i + '_' + ultimo_index);
        $('#c_boletos_cotizados' + i).val(parseInt(ultimo_index, 10) + 1);
        $('#opcionaereo_' + i).css('border', '1px solid #FF0000');
    }

    function agregar_boleto_cotizado_real(i)
    {
        var ultimo_index    = $('#c_boletos_cotizados' + i).val();
        var html            = $('#plantilla_nuevo_boleto_cotizado').html().replace(/_ind_/g, i + '_' + ultimo_index);
        html                = html.replace(/_ord_/g, parseInt(ultimo_index, 10) + 1 );
        html                = html.replace(/_cls_/g, 'boleto_cotizado' + i);
        html                = html.replace(/_valisreal_/g, 1);

        $('#detalle_boletos_cotizados' + i).append(html);
        crear_calendario_time_limit_por_id('iotlires' + i + '_' + ultimo_index);
        $('#c_boletos_cotizados' + i).val(parseInt(ultimo_index, 10) + 1);
        $('#opcionaereo_' + i).css('border', '1px solid #FF0000');
    }

    function buscar_lineas_aereas(index)
    {
        var lgt = $('#ilinaer' + index).val().length;
        if(lgt>0 && (lgt%2)==0)
        {
            var n = $('#ilinaer' + index).val();
            $('#busqueda_linea_aerea_' + index).show();
            $.ajax({
                type    : "POST",
                url     : "../modelo_negocio/mdn_buscar_linea_aerea.php",
                data    : "i=" + index + "&n=" + n,
                success : function(datos)
                {
                    $('#panel_busqueda_linea_aerea_' + index).html(datos);
                }
            });
        }
    }

    function seleccionar_linea_aerea(nm, id, index)
    {
        $('#ilinaer' + index).val(nm);
        $('#iidlinaer' + index).val(id);
        $('#busqueda_linea_aerea_' + index).hide();
    }

    function eliminar_boleto_cotizado(index)
    {
        if(confirm("Se quitará toda la fila. Continuar ?"))
        {
            var id = $('#idboleto_cotizado' + index).val();
            var index_aereo= index.split('_');
            if(id != 0)
            {
                $('#boletos_eliminados' + index_aereo[0] + '_' + index_aereo[1]).append(id + ',');
            }
            mostrar_mensaje_correcto("La ruta ha sido quitada completa y correctamente, para guardar los cambios en la tabla, haga click en 'Guardar Cambios'.");
            $('#boletocotizado-' + index).remove();
            $('#opcionaereo_' + index_aereo[0] + '_' + index_aereo[1]).css('border', '1px solid #FF0000');    
        }
    }

    function guardar_boletos_cotizados(x)
    {
        var op_aereo    = $('#idtb_' + x).val();

        var rutas = new Array();
        $('.boleto_cotizado' + x).each(
            function(ind, val)
            { 
                var id      = $(val).attr('id'); 
                var valor   = id.split('-'); 
                var index   = valor[1];
                var idbol   = $('#idboleto_cotizado' + index).val(); 
                var ordn    = $('#ibord' + index).val();
                var codg    = $('#icodres' + index).val();
                var lina    = $('#iidlinaer' + index).val();
                var ruta    = $('#iruta' + index).val();
                var mnda    = $('#ibmnda' + index).val();
                var badt    = $('#ibadt' + index).val();
                var bmen    = $('#ibmen' + index).val();
                var binf    = $('#ibinf' + index).val();
                var bfee    = $('#ibfee' + index).val();
                var inc_fee = $('input:radio[name=bapfee' + index + ']:checked').val();;
                var bfac    = $('#ibfac' + index).val();
                var inc_iva = $('input:radio[name=bapiva' + index + ']:checked').val();;
                var a_tl    = $('#iotlires' + index).val().split('/');
                var tlim    = a_tl[2] + '-' + a_tl[1] + '-' + a_tl[0];
                var bobs    = $('#ibob' + index).val();
                var is_real = $('#bol_is_real' + index).val();
                var estd    = 0;
                if($('#iestres' + index).is(':checked'))
                    estd    = 1;

                var blto = {
                    index       : index,
                    id          : idbol,
                    op_aereo    : op_aereo,
                    orden       : ordn,
                    codigo      : codg,
                    linea       : lina,
                    ruta        : ruta,
                    moneda      : mnda,
                    adulto      : badt,
                    menor       : bmen,
                    infante     : binf,
                    fee         : bfee,
                    inc_fee     : inc_fee,
                    factura     : bfac,
                    inc_iva     : inc_iva,
                    t_limit     : tlim,
                    observacion : bobs,
                    is_real     : is_real,
                    estado      : estd
                };
                rutas.push(blto);
            }
        );

        var delet = $('#boletos_eliminados' + x).html();

        $.ajax({
            type: "POST",
            url: "../modelo_negocio/mdn_registrar_boletos_cotizados.php",
            data: "bol=" + JSON.stringify(rutas) + "&idel=" + delet,
            success: function(datos) {
                //mostrar_mensaje_correcto(datos);
                var a_rtas = datos.split(';');

                for (var i = 0; i < a_rtas.length; i++) 
                {
                    var a_rta = a_rtas[i].split('/');
                    $('#idboleto_cotizado' + a_rta[0]).val(a_rta[1]);
                }
                mostrar_mensaje_correcto('Las rutas ya han sido guardadas');
                //$('#detalle_boletos_cotizados').css('border', '1px solid #008800');
            }
        });
    }

    function ConvertirBoletoAReserva(i, indexbol)
    {
        if(confirm('Se creará una copia de este servicio. Continuar ?'))
        {
            var ultimo_index    = $('#c_boletos_cotizados' + i).val();
            var html            = $('#plantilla_nuevo_boleto_cotizado').html().replace(/_ind_/g, i + '_' + ultimo_index);
            html                = html.replace(/_ord_/g, parseInt(ultimo_index, 10) + 1 );
            html                = html.replace(/_cls_/g, 'boleto_cotizado' + i);
            html                = html.replace(/_valisreal_/g, 1);
            $('#detalle_boletos_cotizados' + i).append(html);
            crear_calendario_time_limit_por_id('iotlires' + i + '_' + ultimo_index);
            $('#c_boletos_cotizados' + i).val(parseInt(ultimo_index, 10) + 1);

            // COPIAMOS VALORES DEL ANTERIOR BOLETO
            var newindex = i + '_' + ultimo_index;
            $('#boletocotizado-' + newindex).hide();
            $('#ibord' + newindex).val($('#ibord' + indexbol).val());
            $('#icodres' + newindex).val($('#icodres' + indexbol).val());
            $('#ilinaer' + newindex).val($('#ilinaer' + indexbol).val());
            $('#iidlinaer' + newindex).val($('#iidlinaer' + indexbol).val());
            $('#iruta' + newindex).val($('#iruta' + indexbol).val());
            $('#ibadt' + newindex).val($('#ibadt' + indexbol).val());
            $('#ibmen' + newindex).val($('#ibmen' + indexbol).val());
            $('#ibinf' + newindex).val($('#ibinf' + indexbol).val());
            $('#ibfee' + newindex).val($('#ibfee' + indexbol).val());
            $('#ibfac' + newindex).val($('#ibfac' + indexbol).val());

            if($('input:radio[name=bapfee' + indexbol + ']:checked').val() == 'M')
                $('input:radio[name=bapfee' + newindex + '][value=M]').prop('checked', 'checked');
            else
                $('input:radio[name=bapfee' + newindex + '][value=P]').prop('checked', 'checked');
            if($('input:radio[name=bapiva' + indexbol + ']:checked').val() == 'M')
                $('input:radio[name=bapiva' + newindex + '][value=M]').prop('checked', 'checked');
            else
                $('input:radio[name=bapiva' + newindex + '][value=P]').prop('checked', 'checked');

            $('#iotlires' + newindex).val($('#iotlires' + indexbol).val());
            $('#ibmnda' + newindex).val($('#ibmnda' + indexbol).val());
            $('#ibob' + newindex).val($('#ibob' + indexbol).val());

            $('#tot-adt' + newindex).html($('#tot-adt' + indexbol).html());
            $('#tot-men' + newindex).html($('#tot-men' + indexbol).html());
            $('#tot-inf' + newindex).html($('#tot-inf' + indexbol).html());

            //

            $('#opcionaereo_' + i).css('border', '1px solid #FF0000');
            mostrar_mensaje_correcto('Se creó la reserva, verificar en la sección de Reservas.');
        }
    }

/*
* TABLA DE ITINERARIO
*/

    function agregar_segmento(i)
    {
        var ultimo_index    = $('#c_segmentos' + i).val();
        var html            = $('#plantilla_nuevo_segmento').html().replace(/_ind_/g, i + '_' + ultimo_index);
        html                = html.replace(/_cls_/g, 'segmento_publicado' + i);
        html                = html.replace(/_ord_/g, parseInt(ultimo_index, 10) + 1);

        $('#detalle_itinerario' + i).append(html);
        $('#c_segmentos' + i).val(parseInt(ultimo_index, 10) + 1);
        $('#opcionaereo_' + i).css('border', '1px solid #FF0000');
    }

    function eliminar_segmento(index)
    {
        if(confirm("Se quitará toda la fila. Continuar ?"))
        {
            var id = $('#idsegmento' + index).val();
            var index_ciudad = index.split('_');
            if(id != 0)
            {
                $('#segmentos_eliminados' + index_ciudad[0] + '_' + index_ciudad[1]).append(id + ',');
            }
            mostrar_mensaje_correcto("El segmento de borró completa y correctamente, para guardar los cambios del itinerario, haga click en 'Guardar Cambios'.");
            $('#segmento-' + index).remove();
            $('#opcionaereo_' + index_ciudad[0] + '_' + index_ciudad[1]).css('border', '1px solid #FF0000');    
        }
    }

    function agregar_segmento_de_reserva(ind)
    {
        mostrar_url_popup('input_codigo_reserva.php?i=' + ind, 200);
    }

    function agregar_segmento_de_reserva_real(ind)
    {
        mostrar_url_popup('input_codigo_reserva.php?i=' + ind + '&r=r', 200);
    }

    function obtener_itinerario_de_sabre()
    {
        var codigo = $('#codigo_sabre').val();
        $('.boton_agregar_segmento').hide();
        var html = "<img src='images/cargando.gif' style='width:50px; height:50px; display:block; margin:auto;' />";
        $('#itinerario_web').html(html);
        $.ajax({
            type: "POST",
            url: "../../reservas/controladoras/mdn_mostrar_itinerario.php",
            data: "pnr=" + codigo,
            success: function(datos) {
                $('#itinerario_web').html(datos);
                $('.boton_agregar_segmento').show();
            }
        });
    }

    function agregar_segmentos_sabre(ind, is_real)
    {
        if($('#reemplazar').is(':checked'))
        {
            $('.segmento_publicado' + ind).each(
                function(ind, val)
                { 
                    var id      = $(val).attr('id'); 
                    var valor   = id.split('-'); 
                    var index   = valor[1];
                    var idseg   = $('#idsegmento' + index).val();
                    if(idseg != 0)
                    {
                        $('#eliminar_segmentos' + ind).append(idseg + ',');
                    }
                }
            );
            $('.segmento_publicado' + ind).remove();
            $('#c_segmentos' + ind).val(0);
        }

        if($('#itinerario').is(':checked'))
        {
            var ultimo_index = $('#c_segmentos' + ind).val();

            var i=0;
            var html_seg = $('#plantilla_nuevo_segmento_automatico').html();
            while($('#x_linea_aerea' + i).length)
            {
                var codigo  = $('#x_codigo' + i).val();
                var linea   = $('#x_linea_aerea' + i).val();
                var vuelo   = $('#x_nro_vuelo' + i).val();
                var fecha   = $('#x_fecha_salida' + i).val();
                var origen  = $('#x_origen' + i).val();
                var hsale   = $('#x_hora_salida' + i).val();
                var destino = $('#x_destino' + i).val();
                var hllega  = $('#x_hora_llegada' + i).val();

                var nuevo_seg = html_seg.replace(/_cls_/g, 'segmento_publicado' + ind);
                nuevo_seg = nuevo_seg.replace(/_ind_/g, ind + '_' + ultimo_index);
                nuevo_seg = nuevo_seg.replace(/_codigo_/g, codigo);
                nuevo_seg = nuevo_seg.replace(/_orden_/g, parseInt(ultimo_index, 10) + 1);
                nuevo_seg = nuevo_seg.replace(/_linea_/g, linea);
                nuevo_seg = nuevo_seg.replace(/_vuelo_/g, vuelo);
                nuevo_seg = nuevo_seg.replace(/_fecha_/g, fecha);
                nuevo_seg = nuevo_seg.replace(/_origen_/g, origen);
                nuevo_seg = nuevo_seg.replace(/_hsale_/g, hsale);
                nuevo_seg = nuevo_seg.replace(/_destino_/g, destino);
                nuevo_seg = nuevo_seg.replace(/_hllega_/g, hllega);
                $('#detalle_itinerario' + ind).append(nuevo_seg);
                ultimo_index ++;
                i++;
            }
            $('#c_segmentos' + ind).val(ultimo_index);
            $('#opcionaereo_' + ind).css('border', '1px solid #FF0000');
        }

        if($('#boleto').is(':checked'))
        {
            var i=0;
            var html_bol = $('#plantilla_nuevo_boleto_cotizado_automatico').html();
            var ultimo_index = $('#c_boletos_cotizados' + ind).val();

            while($('#reserva_' + i).length)
            {
                var reserva     = $('#reserva_' + i).val();
                var linea       = $('#linea_' + i).val();
                var idlinea     = $('#idlinea_' + i).val();
                var ruta        = $('#ruta_' + i).val();
                var moneda      = $('#moneda_' + i).val();
                var adt         = $('#adt_' + i).val();
                var cnn         = $('#cnn_' + i).val();
                var inf         = $('#inf_' + i).val();
                var fee         = $('#fee_' + i).val();
                var tlimit      = $('#tlimit_' + i).val();
                var observ      = $('#observ_' + i).val().replace(/<br>/g, '\n');
        
                var html = html_bol.replace(/_ind_/g, ind + '_' + ultimo_index);
                html = html.replace(/_indopc_/g, ind);
                html = html.replace(/_cls_/g, 'boleto_cotizado' + ind);
                html = html.replace(/_valisreal_/g, is_real);

                html = html.replace(/_ord_/g, parseInt(ultimo_index, 10) + 1);
                html = html.replace(/_reserva_/g, reserva);
                html = html.replace(/_nomlinea_/g, linea);
                html = html.replace(/_idlinea_/g, idlinea);
                html = html.replace(/_ruta_/g, ruta);
                html = html.replace(/_adt_/g, adt);
                html = html.replace(/_cnn_/g, cnn);
                html = html.replace(/_inf_/g, inf);
                html = html.replace(/_fee_/g, fee);
                html = html.replace(/_observ_/g, observ);

                $('#detalle_boletos_cotizados' + ind).append(html);

                $('#ibmnda' + ind + '_' + ultimo_index).val(moneda);

                crear_calendario_time_limit_por_id('iotlires' + ind + '_' + ultimo_index);

                $('#iotlires' + ind + '_' + ultimo_index).val(tlimit);

                ultimo_index++;
                i++;
            }
            
            $('#c_boletos_cotizados' + ind).val(ultimo_index);
            $('#opcionaereo_' + ind).css('border', '1px solid #FF0000');
        }
    }

    function guardar_itinerario(x)
    {
        var op_aereo    = $('#idtb_' + x).val();

        var itinerario  = new Array();
        $('.segmento_publicado' + x).each(
            function(ind, val)
            { 
                var id          = $(val).attr('id'); 
                var valor       = id.split('-'); 
                var index       = valor[1];
                var idseg       = $('#idsegmento' + index).val(); 
                var ordn        = $('#iiord' + index).val();
                var codigo      = $('#iicod' + index).val();
                var linea       = $('#iilin' + index).val();
                var vuelo       = $('#iivue' + index).val();
                var fecha       = $('#iifec' + index).val();
                var aero_sale   = $('#iiasa' + index).val();
                var h_sale      = $('#iihsa' + index).val();
                var aero_llega  = $('#iiall' + index).val();
                var h_llega     = $('#iihll' + index).val();

                var sgmt = {
                    index       : index,
                    id          : idseg,
                    op_aereo    : op_aereo,
                    orden       : ordn,
                    codigo      : codigo,
                    linea       : linea,
                    vuelo       : vuelo,
                    fecha       : fecha,
                    aero_sale   : aero_sale,
                    h_sale      : h_sale,
                    aero_llega  : aero_llega,
                    h_llega     : h_llega
                };
                itinerario.push(sgmt);
            }
        );

        var delet = $('#segmentos_eliminados' + x).html();

        $.ajax({
            type: "POST",
            url: "../modelo_negocio/mdn_registrar_segmentos_publicados.php",
            data: "itn=" + JSON.stringify(itinerario) + "&idel=" + delet,
            success: function(datos) {
                var a_segm = datos.split(';');

                for (var i = 0; i < a_segm.length; i++) 
                {
                    var a_sgm = a_segm[i].split('/');
                    $('#idsegmento' + a_sgm[0]).val(a_sgm[1]);
                }
                mostrar_mensaje_correcto('Itinerario guardado correctamente');
            }
        });
    }

/*
* REGISTRO DE OPCIONES DE HOTEL
*/

    function buscar_hoteles(index)
    {
        var lgt = $('#hotel_' + index).val().length;
        if(lgt>0 && (lgt%3)==0)
        {
            var n = $('#hotel_' + index).val();
            $('#busqueda_hotel_' + index).show();
            $.ajax({
                type    : "POST",
                url     : "../modelo_negocio/mdn_buscar_hotel.php",
                data    : "i=" + index + "&n=" + n,
                success : function(datos)
                {
                    $('#panel_busqueda_hotel_' + index).html(datos);
                }
            });
        }
    }

    function seleccionar_hotel(nm, id, index)
    {
        $('#hotel_' + index).val(nm);
        $('#iidhot_' + index).val(id);
        $('#busqueda_hotel_' + index).hide();
    }

    function agrupar_hoteles_por_destino()
    {
    	var ultind 	= $('#c_opciones').val();
    	var html 	= "";
    	var cant 	= 0;
     	$('.mis_destinos').each(
            function(ind, val)
            { 
                var id          = $(val).val(); 
                if($('input[name=opcion_' + id + ']:checked').length)
                {
                	var item 	= $('input[name=opcion_' + id + ']:checked').val();	
                	var destino = $('#dest_' + id).html();
                	var hotel 	= $('#hot_' + item).html();

                	var mihtml  = hotel + ' (' + destino + ')';
                	var myinp 	= $('#cd_item').html().replace(/_v_/g, item);
                	myinp 		= myinp.replace(/_i_/g, ultind);
                	mihtml 		+= myinp + '<br/>';

                	html 		+= mihtml;

                    cant++;
                }
            }
        );

     	if(cant > 0)
     	{
    		var html_opc = $('#opcion_hotel').html().replace(/_i_/g, ultind);
    		html_opc     = html_opc.replace(/_x_/g, html);
    		html_opc     = html_opc.replace(/_cls_/g, 'opciones_hotel');

    		$('#opciones').append(html_opc);

    	    $('#c_opciones').val(parseInt(ultind, 10) + 1);

    	    mostrar_mensaje_correcto_tinybox('Opción agregada correctamente.');
    	    $('#opciones').css('border', '2px solid #FF0000');
        }
    }

    function quitar_combinacion(i)
    {
    	if(confirm('Se eliminará esta opción de hoteles. Continuar?'))
    	{
    		var id = $('input[name=' + i + ']').val();
    		if(id != 0)
    		{
    			$('#combi_eliminar').append(id + ',');
    		}
    		$('#opc_' + i).remove();
    		mostrar_mensaje_correcto_tinybox('La opción ya fue quitada correctamente<br/>pero debe guardar los cambios.')
    		$('#opciones').css('border', '2px solid #FF0000');
    	}
    }

    function guardar_tabla_hoteles()
    {
    	var i 		= $('#city').val();
    	var h 		= new Array();
    	var d 		= $('#combi_eliminar').html();

    	$('.opciones_hotel').each(
            function(ind, val)
            { 
                var index 	= $(val).attr('name'); 
                var items   = new Array();
                $('.myop_' + index).each(
                	function(i, v)
                	{
                		var idit = $(v).val();
                		items.push(idit);
                	}
                );
                var obs = $('#obs_' + index).val();
                var idopc = $('input[name=' + index + ']').val();
                var opcion_hotel = {
                	id 			: idopc,
                	index 		: index,
                	observacion : obs,
                	hotls 		: items
                };
                h.push(opcion_hotel);
            }
        );

        $.ajax({
        	type 	: "POST",
        	url 	: "../modelo_negocio/mdn_registrar_combinaciones_hotel.php",
        	data 	: "id=" + i + "&it=" + JSON.stringify(h) + "&dl=" + d,
        	success : function(datos)
        	{
        		var resultados = datos.split(';');
        		for (var i = 0; i < resultados.length-1; i++) 
        		{
        			var index = resultados[i].split(',');
        			$('input[name=' + index[0] + ']').val(index[1]);
        		}
        		$('#opciones').css('border', '2px solid #00CC00');
        		mostrar_mensaje_correcto_tinybox('Cambios guardados correctamente');
        	}
        });
    }

/*
* MUESTRA TOTALES SEGUN EDICION DE LAS TABLAS
*/

    function obtener_total_hotel(index, typ)
    {
        var monto   = parseFloat($('#i' + typ + index).val());
        var fee = parseFloat($('#icms' + index).val());
        if(isNaN(fee))
            fee = 0;
        var iva = parseFloat($('#iiva' + index).val());
        if(isNaN(iva))
            iva = 0;

        var noch    = diferencia_fechas_en_dias($('#tou' + index).val() ,$('#tin' + index).val());
        $('#cantnoches' + index).html(noch);
        monto = monto * noch;

        if($('input:radio[name=apfee' + index + ']:checked').val() == 'P')
            monto   = monto * (1 + (fee / 100));
        else
            monto   = monto + fee;

        if($('input:radio[name=apiva' + index + ']:checked').val() == 'P')
            monto   = monto * (1 + (iva / 100));
        else
            monto   = monto + iva;

        if(isNaN(monto))
            monto = 0;
        $('#tot-' + typ + index).html(Math.ceil(monto));
    }
    function obtener_totales_hotel(index)
    {  
        obtener_total_hotel(index, 'sgl');
        obtener_total_hotel(index, 'dbl');
        obtener_total_hotel(index, 'tpl');
        obtener_total_hotel(index, 'cdpl');
        obtener_total_hotel(index, 'mnr');
        obtener_total_hotel(index, 'inf');
    }

    function obtener_totales_hotel_x_destino(index)
    {
        $('.pc_' + index).each(
            function(ind, val)
            { 
                var id = $(val).attr('id');
                var indx  = id.split('_')[1];
                var nind  = index + '_' + indx;
                obtener_totales_hotel(nind);
            }
        );
    }

    function obtener_total_boleto(index, typ)
    {
        var monto   = parseFloat($('#ib' + typ + index).val());
        var fee = parseFloat($('#ibfee' + index).val());
        if(isNaN(fee))
            fee = 0;
        var iva = parseFloat($('#ibfac' + index).val());
        if(isNaN(iva))
            iva = 0;

        if($('input:radio[name=bapfee' + index + ']:checked').val() == 'P')
            monto   = monto * (1 + (fee / 100));
        else
            monto   = monto + fee;

        if($('input:radio[name=bapiva' + index + ']:checked').val() == 'P')
            monto   = monto * (1 + (iva / 100));
        else
            monto   = monto + iva;

        if(isNaN(monto))
            monto = 0;
        $('#tot-' + typ + index).html(Math.ceil(monto));
    }

    function obtener_totales_boleto(index)
    {  
        obtener_total_boleto(index,'adt');
        obtener_total_boleto(index,'men');
        obtener_total_boleto(index,'inf');
    }

    function obtener_total_servicio(index, typ)
    {
        var monto   = parseFloat($('#in' + typ + index).val());
        var fee = parseFloat($('#iofee' + index).val());
        if(isNaN(fee))
            fee = 0;
        var iva = parseFloat($('#iofac' + index).val());
        if(isNaN(iva))
            iva = 0;

        if($('input:radio[name=sapfee' + index + ']:checked').val() == 'P')
            monto   = monto * (1 + (fee / 100));
        else
            monto   = monto + fee;

        if($('input:radio[name=sapiva' + index + ']:checked').val() == 'P')
            monto   = monto * (1 + (iva / 100));
        else
            monto   = monto + iva;
        
        if(isNaN(monto))
            monto = 0;
        $('#tot-o' + typ + index).html(Math.ceil(monto));
    }

    function obtener_totales_servicio(index)
    { 
        obtener_total_servicio(index,'adt');
        obtener_total_servicio(index,'men');
        obtener_total_servicio(index,'inf');
    }

    function obtener_total_inscripcion(index)
    {
        var monto   = parseFloat($('#iinspre' + index).val());
        var fee = parseFloat($('#iinsfee' + index).val());
        if(isNaN(fee))
            fee = 0;
        var iva = parseFloat($('#iinsfac' + index).val());
        if(isNaN(iva))
            iva = 0;

        if($('input:radio[name=insapfee' + index + ']:checked').val() == 'P')
            monto   = monto * (1 + (fee / 100));
        else
            monto   = monto + fee;

        if($('input:radio[name=insapiva' + index + ']:checked').val() == 'P')
            monto   = monto * (1 + (iva / 100));
        else
            monto   = monto + iva;

        if(isNaN(monto))
            monto = 0;
        $('#tot-ins' + index).html(Math.ceil(monto));
    }

    function segmentar_clientes_por_criterios()
    {
        document.forms['form_segmentar'].submit(); 
    }

    function mostrar_html_popup_por_contenido(html) 
    {
        TINY.box.show({ html: '<link href="lib/tinybox2/style.css" rel="stylesheet" /><div class="container" style="height:500px; overflow:auto;">' + html + '</div>' , fixed:false, height: 550});
    }

    function mostrar_busqueda_ind_cliente()
    {
        var html = '<input type="text" placeholder="Nombre" id="n_cliente" />';
        html += '<input type="text" placeholder="Apellido"  id="a_cliente" />';
        html += '<br/><a class="mylinkleft" onclick="buscar_cliente_individual();">Buscar</a>';
        html += '<form id="resultado_clientes_ind" style="height:400px; overflow:auto;" method="post" action="segmentar_clientes_evento.php">';
        html += '</form>';
        mostrar_html_popup_por_contenido(html);
    }

    function buscar_cliente_individual()
    {
        var n = $('#n_cliente').val();
        var a = $('#a_cliente').val();

        $.ajax({
            type    : "POST",
            url     : "../modelo_negocio/mdn_segmentar_cliente.php",
            data    : "c=d"+ "&n=" + n + "&a=" + a,
            success : function(datos)
            {
                $('#resultado_clientes_ind').html(datos);
            }
        });   
    }

/*
* BUSQUEDA DE ESPECIALIDADES SEGUN LA PROFESION SELECCIONADA
*/

    function Mostrar_Especialidades()
    {
        var p = $('#profesion').val();
        $.ajax({
            type    : "POST",
            url     : "../modelo_negocio/mdn_especialidades.php",
            data    : "p=" + p,
            success : function(datos)
            {
                $('#especialidad').html(datos);
            }
        });      
    }

	//function de rudy marca
    	function ejecutar_pres_intro(codigo,metodo)//codigo 13 el tecla enter
    	{
    		if(window.event.keyCode == codigo)
    		{
    			metodo();
    		}
    	}

    	function eliminar_pax_seleccionado(idcliente)
    	{
    		$('#hd_nro_pax_sel').val(parseFloat($('#hd_nro_pax_sel').val())-1);

    		$('#tr_pax_sel_'+idcliente).fadeOut('fast', function()
    		{
            	$(this).remove();
        	});
    	
    	}
    	
    	function verificar_si_esta_seleccionado(idcliente)
    	{	
    		if ($('#tr_pax_sel_'+idcliente).length > 0) 
    		{
    			return true;
    		}
    		return false;
    		
    	}
    	
    	function guardar_pax_seleccionado()
    	{
    		if(parseFloat($('#hd_nro_pax_sel').val()) > 0)
    		{
    			vec = "";
    			$('.hd_pax_seleccionado').each(function() 
    			{
    				vec += $(this).val()+"<->";
    			});
    			
    			if($('#hd_tipo_de_cotizacion').val() == 1)// 1 paquete; 2 evento
    			{
    				registrar_pax_paquete($('#hd_idpaquete_cotizado').val(),vec);
    			}
    			else
    			{
    				registrar_pax_evento($('#hd_idevento_cotizado').val(),vec);
    			}
    			
    		}
    		else
    		{
    			alert('ERROR: No tiene ningun pasajeros seleccionado.');
    		}
    			
    	}



/*
* ESTE ES UN NUEVO FORMATO PARA PASAJEROS DE UNA COTIZACION
*/
        function SeleccionarPax(pax, tpax)
        {
            if($('#chk_pax_' + tpax + '_' + pax).is(':checked')) 
            {
                $('.pax_' + pax).attr('disabled', 'disabled');
                $('#chk_pax_' + tpax + '_' + pax).removeAttr('disabled', 'false');   
            }
            else
                $('.pax_' + pax).removeAttr('disabled', 'false');   
        }
        function AgregarPaxAlServicioBusqueda(s, t, d)
        {
            var c = $('#idcotizacion').val();
            TINY.box.show({ url: 'buscar_paxs_para_servicio.php?c=' + c + '&s=' + s + '&t=' + t + '&d=' + d , fixed: false, height: 550, openjs:function(){ window.paxss = new Array(); } });
        }
        function AgregarPaxsServicio()
        {
            var p       = $('#tipo_precio').val();

            var paxs    = new Array();
            var dpaxs   = new Array();

            $('.pax_seleccionado').each(
                function(ind, val)
                { 
                    if($(val).is(':checked'))
                    {
                        var idpax = $(val).val();
                        var npax  = $('#pax' + idpax).html();

                        var pax = {
                            idpax   : idpax,
                            nombre  : npax,
                            precio  : p
                        };
                        dpaxs.push(npax + '(' + $("#tipo_precio option:selected").text().split(' - ')[0] + ')');
                        paxs.push(pax);  
                        $(val).attr('disabled', 'disabled');
                        $(val).removeAttr('checked', 'false');
                    }                    
                }
            );
            $('.pax_seleccionado_cnn').each(
                function(ind, val)
                { 
                    if($(val).is(':checked'))
                    {
                        var idpax = $(val).val();
                        var npax  = $('#pax' + idpax).html();

                        var pax = {
                            idpax   : idpax,
                            nombre  : npax,
                            precio  : 5
                        };
                        dpaxs.push(npax + '(CNN)');
                        paxs.push(pax);  
                        $(val).attr('disabled', 'disabled');
                        $(val).removeAttr('checked', 'false');
                    }                    
                }
            );
            $('.pax_seleccionado_inf').each(
                function(ind, val)
                { 
                    if($(val).is(':checked'))
                    {
                        var idpax = $(val).val();
                        var npax  = $('#pax' + idpax).html();

                        var pax = {
                            idpax   : idpax,
                            nombre  : npax,
                            precio  : 6
                        };
                        dpaxs.push(npax + '(INF)');
                        paxs.push(pax); 
                        $(val).attr('disabled', 'disabled');
                        $(val).removeAttr('checked', 'false');
                    }                    
                }
            );

            window.paxss.push(paxs);
            var longitud = paxss.length;
            
            var html = $('.plantilla_pax_hab').html();
            html = html.replace(/_contenido_/g , dpaxs.join('<br/>'));
            html = html.replace(/_ind_/g , longitud-1);

            $('.pasajeros_x_hab').append(html);
        }
        function QuitarPaxServicioAsignado(ind)
        {
            var paxs = window.paxss[ind];

            for (var i = 0; i < paxs.length; i++) {
                $('.pax_' + paxs[i].idpax).removeAttr('disabled', 'false');
            };

            $('.hab' + ind).remove();

            delete window.paxss[ind];
        }
        function AgregarPaxServicio()
        {
            var s       = $('#idserviciopax').val();
            var t       = $('#typeserviciopax').val();
            var p       = $('#tipo_precio').val();
            
            $.ajax(
            {
                type: "POST",
                url: "../modelo_negocio/mdn_registrar_pax_en_servicio.php",
                data: "s=" + s + "&t=" + t + "&p=" + p + "&x=" + JSON.stringify(window.paxss),
                success: function(datos) 
                {
                    parent.TINY.box.hide();
                    mostrar_mensaje_correcto('Se registro correctamente los pasajeros.');
                    $('#pax' + t + s).append(datos);
                    ObtenerCostosPax();
                }
            });
        }
        function QuitarPaxServicio(p, s, t)
        {
            if(confirm('El pasajero sera excluido de este servicio. Continuar ?'))
            {
                $.ajax(
                    {
                        type: "POST",
                        url: "../modelo_negocio/mdn_quitar_pax_de_servicio.php",
                        data: "s=" + s + "&t=" + t + "&p=" + p,
                        success: function(datos) 
                        {
                            if(datos == 'CORRECTO')
                            {
                                $('#pax' + t + '' + p + '_' + s).remove();
                                mostrar_mensaje_correcto('El pasajero fue quitado correctamente');
                            }
                            else
                            {
                                mostrar_mensaje_error('No se pudo quitar el pasajero. Por favor, verifique.');
                            }
                            ObtenerCostosPax();
                            ObtenerCheckListPax();
                        }
                    });    
            }
        }
        function AbrirEdicionPaxTicket(idbol)
        {
            mostrar_url_popup('editar_tickets_por_pax.php?idbol=' + idbol , 550);
        }
        function AsociarTktPax(tkt)
        {
            if($('input[name=pax]:checked').length > 0 && $('#ticket_' + tkt).is(':checked'))
            {
                var idpax       = $($('input[name=pax]:checked')[0]).val();
                var info_pax    = $('.info_pax_' + idpax).val();

                var info_tkt    = $('.info_tkt_' + tkt).val();

                var html        = $('.plantilla_txt_pax').html().replace(/_tkt_/g , info_tkt);
                html            = html.replace(/_ntkt_/g , tkt);
                html            = html.replace(/_pax_/g , info_pax);
                html            = html.replace(/_npax_/g , idpax);
                html            = html.replace(/_class_/g , 'tkt_asoc_pax');

                $('.ticket_' + tkt).hide();
                $('.pax_' + idpax).hide();

                $('input[name=pax]').removeAttr('checked');
                $('input[name=ticket]').removeAttr('checked');

                $('.panel_tickets_asociados').append(html);
            }
        }
        function QuitarAsociacionTktPax(tkt)
        {
            var idpax = $('#' + tkt).val();
            
            $('.ticket_' + tkt).show();
            $('.pax_' + idpax).show();  

            $('.tkt_asoc_' + tkt).remove();
        }
        function GuardarAsociacionPaxTkt(idbol)
        {
            var paxs   = new Array();

            $('.tkt_asoc_pax').each(
                function(ind, val)
                { 
                    var pax = $(val).val();
                    var tkt = $(val).attr('id');

                    var pax_tkt = {
                        pax     : pax,
                        tkt     : tkt
                    };

                    paxs.push(pax_tkt);    
                }
            );

            $.ajax({
            type    : "POST",
            url     : "../modelo_negocio/mdn_registrar_pax_tkt.php",
            data    : "idbol=" + idbol + "&pax=" + JSON.stringify(paxs),
            success :   function(datos)
                        {

                            if(datos == 'CORRECTO')
                            {
                                mostrar_mensaje_correcto_tinybox('Pasajeros Registrados Correctamente');    
                            }
                            else
                            {
                                mostrar_mensaje_error_tinybox('No se pudo Registrar el Ticket');
                            }
                        }
            });
        }

        /*
        * AÑADIR EL PASAJERO EN UNA COTIZACION
        */
            function mostrar_buscar_pax()
            {
                var cot = $('#idcotizacion').val();
                TINY.box.show({ url:'buscar_pasajeros.php?cot=' + cot, fixed: false, openjs:function(){$('#nombre_pax').focus();} });
            }

            function guardar_pax_seleccionado()
            {
                if(parseFloat($('#hd_nro_pax_sel').val()) > 0)
                {
                    vec = "";
                    $('.hd_pax_seleccionado').each(function() 
                    {
                        vec += $(this).val()+"<->";
                    });
                    
                    registrar_pax_cotizacion($('#idcotizacion').val(),vec);
                    
                }
                else
                {
                    alert('ERROR: No tiene ningun pasajeros seleccionado.');
                }
                    
            }

            function registrar_pax_cotizacion(idcotizacion, vec)
            {
                $('#table_pax_sel').hide();
                $('#img_load_save').show();

                var idciudad = $('#ciudad_origen_pax').val();

                $.ajax(
                {
                    type: "POST",
                    url: "../modelo_negocio/mdn_registrar_pax.php",
                    data: "cotizacion=" + idcotizacion + "&vec=" + vec + "&ciudad=" + idciudad,
                    success: function(datos) 
                    {
                        if(parseFloat(datos) > 0)
                        {
                            mostrar_mensaje_correcto('Se registro correctamente los pasajeros.');
                            //obtener_pax_paquete(idpaquete_cotizado);
                            $('#lista_de_pasajeros').hide();
                            $('#div_loading').show();
                        }
                        else
                        {
                            mostrar_mensaje_error('ERROR: No pudo registrar los pasajeros a la cotizacion.');
                        }
                        parent.TINY.box.hide();
                        ObtenerCostosPax();
                    }
                });
            }

            function QuitarPax(idpax)
            {
                if(confirm('Este pasajero sera eliminado de la cotizacion. Continuar ?'))
                {
                    $.ajax(
                    {
                        type: "POST",
                        url: "../modelo_negocio/mdn_quitar_pax_de_cotizacion.php",
                        data: "pax=" + idpax,
                        success: function(datos) 
                        {
                            if(datos == 'CORRECTO')
                            {
                                mostrar_mensaje_correcto('Se quito correctamente el pasajero.');
                                ObtenerCostosPax();
                                ObtenerCheckListPax();
                            }
                            else
                            {
                                mostrar_mensaje_error('ERROR: Verifique si no cuenta con algun servicio.');
                            }
                        }
                    });
                }
            }

            function actualizar_tabla_costo_cotizado()
            {
                
                var consulta = "select";
                var tipo = parseFloat($('#hd_tipo_de_cotizacion').val());
                $('#div_loading').show();
                $('#tabs-1').html('');
                
                var idpaquete_cotizado = $('#hd_idpaquete_cotizado').val();
                
                $.ajax(
                {
                    type: "POST",
                    url: "../modelo_negocio/mdn_obtener_costo_paquete.php",
                    data: "idpaquete_cotizado=" + idpaquete_cotizado +"&consulta="+ consulta,
                    success: function(datos) 
                    {
                        $('#tabs-1').html(datos);
                        actualizar_link()
                        $('#div_loading').hide();
                    }
                });
                
            }

        /*
        * OBTIENE TODOS LOS COSTOS DE LOS PASAJEROS DE UNA COTIZACION
        */

            function ObtenerCostosPax()
            {
                var cotizacion = $('#idcotizacion').val();
                $.ajax(
                {
                    type: "POST",
                    url: "../modelo_negocio/mdn_obtener_lista_paxs_mas_costo_cotizacion.php",
                    data: "cotizacion=" + cotizacion,
                    success: function(datos) 
                    {
                        $('#lista_de_pasajeros').html(datos);
                        $('#div_loading').hide();
                        $('#lista_de_pasajeros').show();
                    }
                });
            }

        /*
        * OBTIENE CHECKLIST DE LOS PASAJEROS
        */

            function ObtenerCheckListPax()
            {
                var cotizacion  = $('#idcotizacion').val();
                var paises      = $('#paises_en_cotizacion').val();

                $('#div_loading_checklist').show();
                $('#lista_de_pasajeros_checklist').hide();
                $.ajax(
                {
                    type: "POST",
                    url: "../modelo_negocio/mdn_obtener_lista_paxs_checklist.php",
                    data: "cotizacion=" + cotizacion + "&paises=" + paises,
                    success: function(datos) 
                    {
                        $('#lista_de_pasajeros_checklist').html(datos);
                        $('#div_loading_checklist').hide();
                        $('#lista_de_pasajeros_checklist').show();
                    }
                });   
            }

    /***/
	
	function registrar_pax_paquete(idpaquete_cotizado, vec)
	{
		$('#table_pax_sel').hide();
		$('#img_load_save').show();
		$.ajax(
		{
        	type: "POST",
        	url: "../modelo_negocio/mdn_registrar_pax_paquete.php",
			data: "idpaquete_cotizado=" + idpaquete_cotizado + "&vec=" + vec,
        	success: function(datos) 
			{
				if(parseFloat(datos) > 0)
				{
					mostrar_mensaje_correcto('Se registro correctamente los pasajeros.');
					//obtener_pax_paquete(idpaquete_cotizado);
					$('#div_loading').show();
				}
				else
				{
					mostrar_mensaje_error('ERROR: No pudo registrar los pasajeros a la cotizacion.');
				}
				parent.TINY.box.hide();
				actalizar_tabla_costo_cotizado();
        	}
    	});
	}
	
	function registrar_pax_evento(idevento_cotizado, vec)
	{
		$('#table_pax_sel').hide();
		$('#img_load_save').show();
		$.ajax(
		{
        	type: "POST",
        	url: "../modelo_negocio/mdn_registrar_pax_evento.php",
			data: "idevento_cotizado=" + idevento_cotizado + "&vec=" + vec,
        	success: function(datos) 
			{
				if(parseFloat(datos) > 0)
				{
					mostrar_mensaje_correcto('Se registro correctamente los pasajeros.');
					//obtener_pax_evento(idevento_cotizado);
					$('#div_loading').show();
				}
				else
				{
					mostrar_mensaje_error('ERROR: No pudo registrar los pasajeros a la cotizacion.');
				}
				parent.TINY.box.hide();
				actalizar_tabla_costo_cotizado();
        	}
    	});
	}
	
	function actualizar_link()
	{
		datos_editables = $('.user_info_editable').find('tr');
		$.each(datos_editables, function(key, tr) {
			tr.onmouseover = function(e) { show_editar_link(tr.id); }
			tr.onmouseout = function(e) { hide_editar_link(tr.id); }
		});
	}

	function show_editar_link(id) 
	{
		$('.editar_' + id + '_link').show();
	}

	function hide_editar_link(id)
	{
		$('.editar_' + id + '_link').hide();
	}
	
	// MOSTRAR TINYBOX, PARA SELEECIONAR ITEMS DEL PAX
	
    	function show_editar_hotel(idpax,tipo,idcotizacion)
    	{
    		TINY.box.show({ url:'tiny_editar_hotel.php?idpax='+idpax+'&tipo='+tipo+'&idcotizacion='+idcotizacion, fixed: false,width:700,height:350,openjs:function(){$('#nombre_pax_tiny').html($('#nombre_pax_'+idpax).html());},closejs:function(){actalizar_tabla_costo_cotizado();}});
    	}
    	
    	function show_editar_boleto(idpax,tipo,idcotizacion)
    	{
    		TINY.box.show({ url:'tiny_editar_boleto.php?idpax='+idpax+'&tipo='+tipo+'&idcotizacion='+idcotizacion, fixed: false,width:700,height:350,openjs:function(){$('#nombre_pax_tiny').html($('#nombre_pax_'+idpax).html());},closejs:function(){actalizar_tabla_costo_cotizado();}});
    	}
    	
    	function show_editar_otros(idpax,tipo,idcotizacion)
    	{
    		TINY.box.show({ url:'tiny_editar_otros.php?idpax='+idpax+'&tipo='+tipo+'&idcotizacion='+idcotizacion, fixed: false,width:700,height:350,openjs:function(){$('#nombre_pax_tiny').html($('#nombre_pax_'+idpax).html());},closejs:function(){actalizar_tabla_costo_cotizado();}});
    	}
    	
    	function show_editar_inscripcion(idpax,tipo,idcotizacion)
    	{
    		TINY.box.show({ url:'tiny_editar_inscripcion.php?idpax='+idpax+'&tipo='+tipo+'&idcotizacion='+idcotizacion, fixed: false,width:700,height:350,openjs:function(){$('#nombre_pax_tiny').html($('#nombre_pax_'+idpax).html());},closejs:function(){actalizar_tabla_costo_cotizado();}});
    	}
	
	// EJECUTAR COMBOBOX DINAMICO
	
    	function obtener_hotel_cotizado(idcotizacion, tipo)
    	{
    		$('.cb_delete').remove();
    		$.ajax(
    		{
    			type: "POST",
    			url: "../modelo_negocio/mdn_generar_combo_hotel.php",
    			data: "idcotizacion=" + idcotizacion + "&tipo=" + tipo,
    			success: function(datos) 
    			{
    				$('#cb_hotel_nombre').append(datos);
    				//$('#nombre_pax_tiny').append(datos);
    			}
    		});
    	}
    	
    	function obtener_boleto_cotizado(idcotizacion, tipo)
    	{
    		$('.cb_delete').remove();	
    		$.ajax(
    		{
    			type: "POST",
    			url: "../modelo_negocio/mdn_generar_combo_boleto.php",
    			data: "idcotizacion=" + idcotizacion + "&tipo=" + tipo,
    			success: function(datos) 
    			{
    				$('#cb_boleto_ruta').append(datos);
    			}
    		});
    	}
	
	//GUARDAR BOLETO PAX
	
    	function guardar_boleto_pax(idpax)
    	{		
    		var tipo_precio 		= $('#cb_boleto').val();
    		var idboleto_cotizado 	= $('#cb_boleto_ruta').val();
    		
    		if(parseFloat(tipo_precio) > 0 && parseFloat(idboleto_cotizado) > 0)
    		{
    			if(validar_agregar_boleto(idboleto_cotizado))
    			{
    				alert('Error: La ruta ya esta seleccionado.');	
    			}
    			else
    			{
    				$('#table_tiny_boleto_'+idpax).html('');
    				$('#img_load_boleto_pax').show();
    				var consulta = "guardar";
    				var tipo = $('#hd_tipo_de_cotizacion').val();
    				$.ajax(
    				{
    					type: "POST",
    					url: "../modelo_negocio/mdn_obtener_boleto_pax.php",
    					data: "idboleto_cotizado=" + idboleto_cotizado + "&idpax=" + idpax + "&tipo_precio=" + tipo_precio + "&consulta=" + consulta + "&tipo=" + tipo,
    					success: function(datos) 
    					{
    						$('#table_tiny_boleto_'+idpax).append(datos);
    						$('#img_load_boleto_pax').hide();
    						
    					}
    				});
    			}
    		}
    		else
    		{
    			alert('ERROR: Selecciones el tipo de pax y la ruta');
    		}		
    	}
    	
    	function validar_agregar_boleto(idboleto_cotizado)
    	{
    		if ($('#hd_boleto_cotizado_'+idboleto_cotizado).length > 0) 
    		{
    			return true;
    		}
    		return false;
    	}
    	
    	function actalizar_tabla_costo_cotizado()
    	{
    		
    		var consulta = "select";
    		var tipo = parseFloat($('#hd_tipo_de_cotizacion').val());
    		$('#div_loading').show();
    		$('#tabs-1').html('');
    		
    		if(tipo == 1)//cotizacion_paquete
    		{
    			var idpaquete_cotizado = $('#hd_idpaquete_cotizado').val();
    			
    			$.ajax(
    			{
    				type: "POST",
    				url: "../modelo_negocio/mdn_obtener_costo_paquete.php",
    				data: "idpaquete_cotizado=" + idpaquete_cotizado +"&consulta="+ consulta,
    				success: function(datos) 
    				{
    					$('#tabs-1').html(datos);
    					actualizar_link()
    					$('#div_loading').hide();
    				}
    			});
    		}
    		else//cotizacion_evento
    		{
    			var idevento_cotizado = $('#hd_idevento_cotizado').val();
    			
    			$.ajax(
    			{
    				type: "POST",
    				url: "../modelo_negocio/mdn_obtener_costo_evento.php",
    				data: "idevento_cotizado=" + idevento_cotizado +"&consulta="+ consulta,
    				success: function(datos) 
    				{
    					$('#tabs-1').html(datos);
    					actualizar_link()
    					$('#div_loading').hide();
    				}
    			});
    		}
    		
    	}
    	
    	function eliminar_ruta_pax(idboleto_cotizado,idpax)
    	{
    		$('#table_tiny_boleto_'+idpax).html('');
    		var consulta = "eliminar";
    		var tipo = $('#hd_tipo_de_cotizacion').val();
    		$.ajax(
    		{
    			type: "POST",
    			url: "../modelo_negocio/mdn_obtener_boleto_pax.php",
    			data: "idboleto_cotizado=" + idboleto_cotizado + "&idpax=" + idpax + "&consulta=" + consulta + "&tipo=" + tipo,
    			success: function(datos) 
    			{
    				$('#table_tiny_boleto_'+idpax).append(datos);
    				$('#img_load_boleto_pax').hide();
    				
    			}
    		});
    	}
    	
    	function eliminar_pax_cotizacion(idpax)
    	{
    		var tipo = parseFloat($('#hd_tipo_de_cotizacion').val());
    		
    		if(tipo == 1)
    		{
    			var idpaquete_cotizado 	= $('#hd_idpaquete_cotizado').val();
    			var consulta 			= "delete";
    			$('#div_loading').show();
    			$('#tabs-1').html('');
    			$.ajax(
    			{
    				type: "POST",
    				url: "../modelo_negocio/mdn_obtener_costo_paquete.php",
    				data: "idpaquete_cotizado=" + idpaquete_cotizado +"&consulta="+ consulta +"&idpax="+ idpax,
    				success: function(datos) 
    				{
    					$('#tabs-1').html(datos);
    					actualizar_link()
    					$('#div_loading').hide();
    				}
    			});
    		}
    		else
    		{
    			var idevento_cotizado 	= $('#hd_idevento_cotizado').val();
    			var consulta 			= "delete";
    			$('#div_loading').show();
    			$('#tabs-1').html('');
    			$.ajax(
    			{
    				type: "POST",
    				url: "../modelo_negocio/mdn_obtener_costo_evento.php",
    				data: "idevento_cotizado=" + idevento_cotizado +"&consulta="+ consulta +"&idpax="+ idpax,
    				success: function(datos) 
    				{
    					$('#tabs-1').html(datos);
    					actualizar_link()
    					$('#div_loading').hide();
    				}
    			});
    		}
    	}
	
	//GUARDAR HOTEL PAX
	
    	function guardar_hotel_pax(idpax)
    	{		
    		var tipo_precio 		= $('#cb_hotel').val();
    		var idhotel_cotizado 	= $('#cb_hotel_nombre').val();
    		
    		if(parseFloat(tipo_precio) > 0 && parseFloat(idhotel_cotizado) > 0)
    		{
    			if(validar_agregar_hotel(idhotel_cotizado))
    			{
    				alert('Error: La ruta ya esta seleccionado.');	
    			}
    			else
    			{
    				$('#table_tiny_hotel_'+idpax).html('');
    				$('#img_load_hotel_pax').show();
    				var consulta = "guardar";
    				var tipo = $('#hd_tipo_de_cotizacion').val();
    				$.ajax(
    				{
    					type: "POST",
    					url: "../modelo_negocio/mdn_obtener_hotel_pax.php",
    					data: "idhotel_cotizado=" + idhotel_cotizado + "&idpax=" + idpax + "&tipo_precio=" + tipo_precio + "&consulta=" + consulta + "&tipo=" + tipo,
    					success: function(datos) 
    					{
    						$('#table_tiny_hotel_'+idpax).append(datos);
    						$('#img_load_hotel_pax').hide();
    					}
    				});
    			}
    		}
    		else
    		{
    			alert('ERROR: Selecciones el tipo de acomodacion y el hotel');
    		}		
    	}
    	
    	function validar_agregar_hotel(idhotel_cotizado)
    	{
    		if ($('#hd_hotel_cotizado_'+idhotel_cotizado).length > 0) 
    		{
    			return true;
    		}
    		return false;
    	}
    	
    	function eliminar_hotel_pax(idhotel_cotizado,idpax)
    	{
    		$('#table_tiny_hotel_'+idpax).html('');
    		var consulta = "eliminar";
    		var tipo = $('#hd_tipo_de_cotizacion').val();
    		$.ajax(
    		{
    			type: "POST",
    			url: "../modelo_negocio/mdn_obtener_hotel_pax.php",
    			data: "idhotel_cotizado=" + idhotel_cotizado + "&idpax=" + idpax + "&consulta=" + consulta + "&tipo=" + tipo,
    			success: function(datos) 
    			{
    				$('#table_tiny_hotel_'+idpax).append(datos);
    				$('#img_load_hotel_pax').hide();
    			}
    		});
    	}
	
	//COMBO DINAMICO PARA OTROS SERVICIOS
	
    	function obtener_otros_cotizado(idcotizacion, tipo)
    	{
    		$('.cb_delete').remove();	
    		$.ajax(
    		{
    			type: "POST",
    			url: "../modelo_negocio/mdn_generar_combo_otros.php",
    			data: "idcotizacion=" + idcotizacion + "&tipo=" + tipo,
    			success: function(datos) 
    			{
    				$('#cb_otros_nombre').append(datos);
    			}
    		});
    	}
    	
    	function guardar_otros_pax(idpax)
    	{		
    		var tipo_precio 		= $('#cb_otros').val();
    		var idotros_cotizado 	= $('#cb_otros_nombre').val();
    		
    		if(parseFloat(tipo_precio) > 0 && parseFloat(idotros_cotizado) > 0)
    		{
    			if(validar_agregar_otros(idotros_cotizado))
    			{
    				alert('Error: El Servicio ya esta seleccionado.');	
    			}
    			else
    			{
    				$('#table_tiny_otros_'+idpax).html('');
    				$('#img_load_otros_pax').show();
    				var consulta = "guardar";
    				var tipo = $('#hd_tipo_de_cotizacion').val();
    				$.ajax(
    				{
    					type: "POST",
    					url: "../modelo_negocio/mdn_obtener_otros_pax.php",
    					data: "idotros_cotizado=" + idotros_cotizado + "&idpax=" + idpax + "&tipo_precio=" + tipo_precio + "&consulta=" + consulta + "&tipo=" + tipo,
    					success: function(datos) 
    					{
    						$('#table_tiny_otros_'+idpax).append(datos);
    						$('#img_load_otros_pax').hide();
    					}
    				});
    			}
    		}
    		else
    		{
    			alert('ERROR: Selecciones el tipo de pax y el servicio.');
    		}		
    	}
    	
    	function validar_agregar_otros(idotros_cotizado)
    	{
    		if ($('#hd_otros_cotizado_'+idotros_cotizado).length > 0) 
    		{
    			return true;
    		}
    		return false;
    	}
    	
    	function eliminar_otros_pax(idotros_cotizado,idpax)
    	{
    		$('#table_tiny_otros_'+idpax).html('');
    		var consulta = "eliminar";
    		var tipo = $('#hd_tipo_de_cotizacion').val();
    		$.ajax(
    		{
    			type: "POST",
    			url: "../modelo_negocio/mdn_obtener_otros_pax.php",
    			data: "idotros_cotizado=" + idotros_cotizado + "&idpax=" + idpax + "&consulta=" + consulta + "&tipo=" + tipo,
    			success: function(datos) 
    			{
    				$('#table_tiny_otros_'+idpax).append(datos);
    				$('#img_load_otros_pax').hide();
    			}
    		});
    	}
	
	

    /*
    * OBTENER ARCHIVOS POR ITEM
    * LOS ITEM PUEDEN SER HOTELES, OTROS SERVICIOS O INSCRIPCIONES
    */

        function mostrar_dialogo_archivos_x_item(index, pax, typ)
        {
            $.ajax(
            {
                type: "POST",
                url: "../modelo_negocio/mdn_obtener_adjuntos.php",
                data: "i=" + index + "&p=" + pax + "&t=" + typ,
                success: function(datos) 
                {
                    $('#archivos' + index).html(datos);
                    $('#archivos' + index).show();
                }
            });   
        }
        function guardar_archivo_para_pax()
        {
            if($('#descripcion_archivo').val() != '')
            {
                var desc    = $('#descripcion_archivo').val();
                var item    = $('#item').val();
                var pax     = $('#pax').val();
                var type    = $('#type').val();
                var vist    = $('input:radio[name=vista]:checked').val();

                var archivos    = document.getElementById("archivo_adjunto");
                var archivo     = archivos.files;

                var data = new FormData();

                for(i=0; i<archivo.length; i++)
                {
                    data.append('archivo'+i,archivo[i]);
                }

                data.append('desc', desc);
                data.append('item', item);
                data.append('pax', pax);
                data.append('type', type);
                data.append('vist', vist);

                $.ajax({
                    url:            '../modelo_negocio/mdn_guardar_archivo_adjunto_x_pax.php',
                    type:           'POST',
                    contentType:    false,
                    data:           data,
                    processData:    false,
                    cache:          false
                    }).done(function(msg)
                            {
                                $('#adjuntos').append(msg);
                            });

            }
            else
            {
                mostrar_mensaje_error_tinybox('Introduzca obligatoriamente una descripcion.');
                $('#archivo_adjunto').val('');
            }
        }
        function eliminar_adjunto_pax(a, i)
        {
            if(confirm('Se borrará el archivo. Desea Continuar ?'))
            {
                $.ajax(
                {
                    type: "POST",
                    url: "../modelo_negocio/mdn_eliminar_archivo_adjunto_x_pax.php",
                    data: "a=" + a + "&i=" + i,
                    success: function(datos) 
                    {
                        if(datos == 'CORRECTO')
                        {
                            mostrar_mensaje_correcto_tinybox('Eliminado Correctamente');
                            $('#adj' + i).remove();
                        }
                        else
                        {
                            mostrar_mensaje_error_tinybox('No se Pudo Eliminar el Archivo');
                        }
                    }
                });       
            }
        }
        // NUEVO FORMATO
        function seleccionar_y_guardar_archivo_por_servicio()
        {
            var descripcion = $('#descripcion').val();
            if(descripcion!="")
            {
                $('.imagen_load').show();

                var archivos    = document.getElementById("archivo_adjunto");
                var archivo     = archivos.files;

                var data = new FormData();

                for(i=0; i<archivo.length; i++)
                {
                    data.append('archivo'+i,archivo[i]);
                }

                var servicio    = $('#id_servicio').val();
                data.append('servicio', servicio);

                var type        = $('#tipo_servicio').val();
                data.append('type', type);

                data.append('descripcion', descripcion);

                var is_visible  = $('#is_visible').val();
                data.append('is_visible', is_visible);

                $.ajax({
                    url:            '../control/guardar_archivo_por_servicio.php',
                    type:           'POST',
                    contentType:    false,
                    data:           data,
                    processData:    false,
                    cache:          false
                }).done(function(msg)
                {
                    $('.imagen_load').hide();                
                    $('#aadj' + type + servicio).append(msg);
                    parent.TINY.box.hide();
                    mostrar_mensaje_correcto('Archivo Registrado Correctamente');
                });    
            }
            else
            {
                alert("Debe escribir alguna descripcion del archivo");  
                $('#archivo_adjunto').val('');
            }
        }
        function eliminar_adjunto_x_servicio(servicio, type, adjunto)
        {
            if(confirm('Se eliminará el archivo seleccionado. Continuar ?'))
            {
                $.ajax(
                {
                    type: "POST",
                    url: "../control/eliminar_archivo_por_servicio.php",
                    data: "s=" + servicio + "&t=" + type + "&a=" + adjunto,
                    success: function(datos) 
                    {
                        if(datos == 'CORRECTO')
                        {
                            mostrar_mensaje_correcto('Eliminado Correctamente');
                            $('#adj' + type + adjunto).remove();
                        }
                        else
                        {
                            mostrar_mensaje_error('No se Pudo Eliminar el Archivo');
                        }
                    }
                }); 
            }
        }

/*
* REGISTRAR CIUDAD DE ORIGEN
*/

/*
* PASAR DE GENERICO UNA COPIA
*/
    function BuscarCotizacionParaCopia(x)
    {
        html = '<link href="lib/tinybox2/style.css" rel="stylesheet" />';
        html += '<label>NRO COTIZACION:</label><input type="text" value="' + x + '" id="idcotizacioncopiar" />';
        html += '<input type="button" value="Buscar" onclick="ObtenerDeCotizacion();" />';
        TINY.box.show({ html: html , fixed:false, height: 250});
    }
    function BuscarGenericoParaCopia(x)
    {
        html = '<link href="lib/tinybox2/style.css" rel="stylesheet" />';
        html += '<label>NRO PAQUETE:</label><input type="text" value="' + x + '" id="idgenericocopiar" />';
        html += '<input type="button" value="Buscar" onclick="ObtenerDeGenerico();" />';
        TINY.box.show({ html: html , fixed:false, height: 250});
    }
    function ObtenerDeCotizacion()
    {
        var cot = $('#idcotizacioncopiar').val();
        mostrar_url_popup('obtener_tarifas_cotizacion.php?cot=' + cot, 600); 
    }
    function ObtenerDeGenerico()
    {
        var gen = $('#idgenericocopiar').val();
        mostrar_url_popup('obtener_tarifas_generico.php?pqt=' + gen, 600);  
    }
    function PasarCiudadGenericoToCotizacion(index)
    {
        if(confirm('Se agregará la ciudad. Pero debe guardar los siguientes datos si desea conservarlos:' + "\n" + '- Paquete Incluye' + "\n" + '- Tarifas de Hoteles' + "\n" + '- Otros Servicios' + "\n" + '- Boletos Aereos' + "\n" + '- Importante' + "\n" + '- Datos Agente'))
        {
            var nciudad     = $('#nombre_ciudad_' + index).val();
            var salida      = $('#fecha_salida_ciudad_' + index).val();
            var retorno     = $('#fecha_retorno_ciudad_' + index).val();
            var post        = $('#publicacion').val();
            var type        = $('#type').val();

            $.ajax(
            {
                type: "POST",
                url: "../modelo_negocio/mdn_registrar_ciudad_origen_paquete.php",
                data: "c=" + nciudad + "&s=" + salida + "&r=" + retorno + "&p=" + post + "&t=" + type,
                success: function(datos) { 
                    if(datos.indexOf('CORRECTO') != -1)
                    {
                        var idciudad = datos.split(':')[1];

                        var ultimo_index    = $('#c_ciudades_origen').val();
            
                        var html = $('#tarifas_generico_' + index).html().replace(/_indz_/g , ultimo_index);
                        html = html.replace(/_type_/g , 'C');
                        html = html.replace(/_idciudadorigen_/g , idciudad);
                        $('#tabs_por_ciudades').append(html);

                        crear_calendario_time_limit_por_id('valor_fecha_salida_' + idciudad);
                        crear_calendario_time_limit_por_id('valor_fecha_retorno_' + idciudad);
                        crear_calendario_time_limit();
                        var htmlb = $('#plantilla_boton_ciudad').html().replace(/_ind_/g , ultimo_index);
                        htmlb = htmlb.replace(/_nombreciudad_/g , nciudad);
                        $('#botones_por_ciudad_origen').append(htmlb);

                        $('#c_ciudades_origen').val(parseInt(ultimo_index, 10) + 1);

                        mostrar_mensaje_correcto_tinybox('Se agrego la ciudad ' + nciudad + '.\n Pero debe guardar las tarifas y demas datos.');
                    }
                    else
                    {
                        mostrar_mensaje_correcto_tinybox('No se pudo agregar la ciudad ' + nciudad + '.');   
                    }
                }
            });      
        }
          
    }


    function revelar_celda(clase)
    {
        if($('#chk' + clase).is(':checked'))
        {
            $('.c' + clase).removeAttr('disabled', 'false');
            $('.' + clase).show();
        }
        else
        {
            var clase_ocultar = clase;
            var parent  = $('#' + clase).val();
            var cols = $('#cols' + clase).val();
            var current = clase;
            while(parent != 0)
            { 
                var cols_c  = $('#cols' + parent).val(); // VALOR ACTUA;
                cols_c      -= cols;
                $('#cols' + parent).val(cols_c);

                if(cols_c <= 0)
                    clase_ocultar = parent;
                else
                {
                    $('.cell_' + parent).attr('colspan', cols_c);
                }

                current     = parent;
                parent      = $('#' + current).val();
            }
            $('.' + clase_ocultar).hide();
            $('.c' + clase).attr('disabled', 'disabled');
            var colspanactual = $('.no-pax').attr('colspan');
            $('.no-pax').attr('colspan', colspanactual - cols_c);
        }
    }
    function mostrar_ocultar_servicio(clase)
    {
        if($('#chk' + clase).is(':checked'))
        {
            var cols_c = $('#chk' + clase).val();
            var colspanactual = $('.no-pax').attr('colspan');
            /*$('.no-pax').attr('colspan', colspanactual + cols_c);*/
            $('.' + clase).show();
        }
        else
        {
            var cols_c = $('#chk' + clase).val();
            var colspanactual = $('.no-pax').attr('colspan');
            /*$('.no-pax').attr('colspan', colspanactual - cols_c - 1);*/
            $('.' + clase).hide();
        }
    }

/*
* MOSTRAR RESERVA AEREA MEDIANTE AJAX
*/
    function obtener_reserva_de_sabre_para_mostrar(codigo)
    {
        $.ajax(
            {
                type: "POST",
                url: "../../reservas/interfaces/obtener_reserva_aerea.php",
                data: "pnr=" + codigo,
                success: function(datos) 
                {
                    a_html = datos.split("<!-- SEPARADOR -->");
                    $('.res_' + codigo).html(a_html[0]);
                    $('.resprint_' + codigo).html(a_html[1]);
                }
            }); 
    }

/*
* CERRAR UNA COTIZACION
*/
    function cambiar_estado_cotizacion(cot, est)
    {
        var confirmacion = '';
        if(est == 0)
            confirmacion = 'Si la cotización es anulada, significa que no tuvo éxito de venta, Continuar ?';
        if(est == 1)
            confirmacion = 'Usted afirma que esta cotización aún se encuentra en proceso, Continuar ?';
        if(est == 2)
            confirmacion = 'Usted afirma que esta cotización ya está vendida, Continuar ?';

        if(confirm(confirmacion))
        {
            $.ajax({
                type: "POST",
                url: "../modelo_negocio/mdn_cierre_cotizacion.php",
                data: "cot=" + cot + "&est=" + est,
                success: function(datos) { 
                    if(datos == "CORRECTO")
                    {
                        if(est == 1)
                        {
                            mostrar_mensaje_correcto('La cotización está aún en proceso');
                            $('.vista_pagina').html('VISTA DE COTIZACION DE PAQUETE<br/>(EN PROCESO)'); 
                            $('.boton_cotizacion_en_proceso').hide();
                        }
                        else
                        {
                            if(est == 0)
                            {
                                mostrar_mensaje_correcto('La cotización está anulada');
                                $('.vista_pagina').html('VISTA DE COTIZACION DE PAQUETE<br/><font style="color:#FF3300;">(ANULADO)</font>');
                            }
                            if(est == 2)
                            {
                                mostrar_mensaje_correcto('La cotización está oficialmente vendida');
                                $('.vista_pagina').html('VISTA DE COTIZACION DE PAQUETE<br/><font style="color:#009629;">(CERRADA CON EXITO)</font>');
                            }

                            $('.boton_cerrar_cotizacion').hide();
                        }
                    }
                    else
                    {
                        mostrar_mensaje_error("Error al guardar");
                    }
                }
            });      
        }
    }