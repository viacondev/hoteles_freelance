<div class="clear"></div>
<div style="display:none;">
	<div style="width:100%;" class="myPrintArea">

		<div class="vista_precios" style="float:left;">
			<h3>Tabla de Costos: <?php echo $info_paquete[0]['titulo']; ?></h3>
			<table class="tabla-resumen" style="font-family:Courier New; width:100%; ">
				<!-- TITULOS GENERALES -->
					<tr class="tabla-resumen-print">
						<th rowspan="4" style="height:58px !important; width:15px !important;">C</th>
						<th rowspan="4" style="height:58px !important; width:150px !important;">CLIENTE</th>
						<th rowspan="4" style="height:58px !important; width:190px !important;">PASAJEROS</th>
						
						<!-- AEREO -->
						<?php
							if($total_columnas_boletos_aereos > 0)
							{
							?>
								<th class="subtitle sb cell_sb" colspan="<?php echo $total_columnas_boletos_aereos; ?>">AEREO</th>
								<th class="subtitle sb" rowspan="4">SUB<br/>TOTAL</th>
							<?php
							}
						?>

						<!-- HOTELES -->
						<?php
							if($total_columnas_hoteles_in > 0)
							{
							?>
								<th class="subtitle sh cell_sh" colspan="<?php echo $total_columnas_hoteles_in; ?>">HOTELES</th>
								<th class="subtitle sh" rowspan="4">SUB<br/>TOTAL</th>
							<?php
							}
						?>
						
						<!-- OTROS SERVICIOS -->
						<?php
							for ($i=0; $i < count($subtotales_col_destinos); $i++) 
							{ 
								$aux = $subtotales_col_destinos[$i];
							?>
								<th class="subtitle <?php echo $aux[2]; ?> cell_<?php echo substr($aux[2], -3); ?>" colspan="<?php echo $aux[1]; ?>"><?php echo substr($aux[0], 0, 7); ?></th>
								<th class="subtitle <?php echo $aux[2]; ?>" rowspan="4">SUB<br/>TOTAL</th>
							<?php
							}
						?>

						<!-- INSCRIPCIONES -->
						<?php
							if($total_columnas_inscripciones > 0)
							{
							?>
								<th class="subtitle si cell_si" colspan="<?php echo $total_columnas_inscripciones; ?>">INSCRIP</th>
							<?php
							} 
						?>
						
						<th class="subtitle total_familia" rowspan="4">TOTAL</th>
					</tr>

				<!-- CLASIFICACION DE SERVICIOS -->
					<tr class="tabla-resumen-print">
						<!-- MOSTRAMOS LAS LINEAS AEREAS -->
						<?php
							for ($i=0; $i < count($subtotales_col_lineas_aereas); $i++) 
							{ 
								$aux = $subtotales_col_lineas_aereas[$i];
							?>
								<th style="height:27px !important;" class="subtitle <?php echo $aux[2]; ?> cell_<?php echo substr($aux[2], -3); ?>" rowspan="2" colspan="<?php echo $aux[1]; ?>"><?php echo strtoupper(substr($aux[0], 0, 7)); ?></th>
							<?php
							}
						?>

						<!-- MOSTRAMOS LOS DESTINOS POR HOTELES -->
						<?php
							for ($i=0; $i < count($subtotales_col_dhotel); $i++) 
							{ 
								$aux = $subtotales_col_dhotel[$i];
							?>
								<th class="subtitle <?php echo $aux[2]; ?> cell_<?php echo substr($aux[2], -2); ?>" id="" colspan="<?php echo $aux[1]; ?>"><?php echo $aux[0]; ?></th>
							<?php
							}
						?>

						<!-- MOSTRAMOS LOS DESTINOS POR OTROS SERVICIOS -->
						<?php
							for ($i=0; $i < count($subtotales_col_otros_servicios); $i++) 
							{ 
								$aux = $subtotales_col_otros_servicios[$i];
							?>
								<th style="height:27px !important;" class="subtitle <?php echo $aux[2]; ?> cell_<?php echo substr($aux[2], -3); ?>" rowspan="2" colspan="<?php echo $aux[1]; ?>"><?php echo $aux[0]; ?></th>
							<?php
							}
						?>

						<!-- MOSTRAMOS LOS EVENTOS -->
						<?php
							for ($i=0; $i < count($subtotales_col_eventos); $i++) 
							{ 
								$aux = $subtotales_col_eventos[$i];
							?>
								<th style="height:27px !important;" class="subtitle <?php echo $aux[2]; ?> cell_<?php echo substr($aux[2], -3); ?>" rowspan="3" ><?php echo strtoupper(substr($aux[0], 0, 7)); ?></th>
							<?php
							}
						?>
					</tr>

					<tr class="tabla-resumen-print">
						<!-- MOSTRAMOS LOS HOTELES -->
						<?php
							for ($i=0; $i < count($subtotales_col_hoteles); $i++) 
							{ 
								$aux = $subtotales_col_hoteles[$i];
							?>
								<th style="min-width:70px !important;" class="subtitle2 <?php echo $aux[2]; ?> cell_<?php echo substr($aux[2], -2); ?>" id="" colspan="<?php echo $aux[1]; ?>"><?php echo $aux[0]; ?></th>
							<?php
							}
						?>
					</tr>

					<tr class="tabla-resumen-print">
						<!-- MOSTRAMOS LAS RUTAS DE BOLETOS AEREOS -->
						<?php
							for ($i=0; $i < count($subtotales_col_rutas); $i++) 
							{ 
								$aux = $subtotales_col_rutas[$i];
							?>
								<th class="subtitle2 <?php echo $aux[2]; ?> cell_<?php echo substr($aux[2], -3); ?>" ><?php echo strtoupper(substr($aux[0], 0, 6) . '..'); ?></th>
							<?php
							}
						?>

						<!-- MOSTRAMOS LAS FECHAS DE ENTRADAS DE HOTELES -->
						<?php
							for ($i=0; $i < count($subtotales_col_in); $i++) 
							{ 
								$aux = $subtotales_col_in[$i];
							?>
								<th style="min-width:50px !important;" class="subtitle2 <?php echo $aux[2]; ?> cell_<?php echo substr($aux[2], -2); ?>" id="" ><?php echo strtoupper(str_replace(' ', ' A<br/>',$aux[0])); ?></th>
							<?php
							}
						?>

						<!-- MOSTRAMOS LAS FECHAS POR OTROS SERVICIOS -->
						<?php
							for ($i=0; $i < count($subtotales_col_servicios_ind); $i++) 
							{ 
								$aux = $subtotales_col_servicios_ind[$i];
							?>
								<th style="min-width:50px !important;" class="subtitle2 <?php echo $aux[2]; ?> cell_<?php echo substr($aux[2], -3); ?>"><?php echo strtoupper(str_replace(' ', ' A<br/>',$aux[0])); ?></th>
							<?php
							}
						?>
					</tr>

				<!-- DETALLAMOS NOBRES DE PASAJEROS Y COSTOS DE SERVICIOS -->

				<?php
					for ($i=0; $i < count($cotizaciones); $i++) 
					{ 
						$total_familia 			= 0;
						$total_familia_hotel 	= 0;
						$total_familia_servicio = array();

						if($i%2 == 0)
							$color = 'white';
						else
							$color = 'gray';

						$cotizacion = $cotizaciones[$i];
						$idcot 		= $cotizacion['idcotizacion'];
						$paxs 		= $cotizacion['paxs'];
						$cant_pax	= count($paxs);

						if($cant_pax > 0)
						{
							for ($j=0; $j < count($paxs); $j++) 
							{ 
								$pax = $paxs[$j];
							?>
								<tr class="<?php echo $color; ?>">
									<?php
										if($j == 0)
										{
										?>
											<td style="padding:1px !important;" rowspan="<?php echo $cant_pax; ?>" ><a href="ver_cotizacion_paquete.php?cotizacion=<?php echo $idcot; ?>" target="_blank"><?php echo $idcot; ?></a></td>
											<td style="padding:1px !important;" rowspan="<?php echo $cant_pax; ?>" ><?php echo substr($cotizacion['nombre_cliente'], 0, 20); ?></td>
										<?php
										}
									?>
											<td style="padding:1px !important;"><?php echo substr(strtoupper($pax['nombre_cliente'] . ' ' . $pax['apellido_cliente']), 0, 25); ?></td>
										<!-- COLOCAR CADA UNO DE LOS BOLETOS AEREOS -->
											<?php
												$cod_aereos = c_mostrar_cotizacion::obtener_id_servicios_aereo($pax['idpax']);

												foreach ($a_boletos_aereos as $cod_aepto => $blinea_aerea) 
												{
													foreach ($blinea_aerea as $ruta => $tarifas) 
													{
														$precios 		= '';
														$class 			= '';

														for ($k=0; array_key_exists($k, $tarifas); $k++) 
														{ 
															$tarifa 	= $tarifas[$k];
															$class 		= $tarifa['class'];
															$idtarifa 	= $tarifa['idboleto_cotizado'];

															if(array_key_exists($idtarifa, $cod_aereos))
															{
																$acomodacion = $cod_aereos[$idtarifa]['t'];
																$costo = '';
																$acom  = '';
																switch ($acomodacion) 
																{
																	case 1: $costo=$tarifa['precio_adulto']; $acom='ADT'; break;
																	case 2: $costo=$tarifa['precio_menor']; $acom='CNN'; break;
																	case 3: $costo=$tarifa['precio_infante']; $acom='INF'; break;
																	default: $costo = 0; $acom='N/N'; break;
																}
																
																// TRANSFORMAR A DOLARES SI ESTA EN BOLIVIANOS
																if($tarifa['moneda'] == 'B')
																{
																	$costo /= $tarifa['tipo_cambio'];
																}

																// SUMAR FEE
																if($tarifa['increment_fee'] == 'P')
																{
																	$fee 	= 1 + ($tarifa['fee']/100);
																	$costo 	*= $fee;
																}
																else
																{
																	$fee 	= $tarifa['fee'];
																	$costo 	+= $fee;
																}

																// SUMAR IVA
																if($tarifa['increment_factura'] == 'P')
																{
																	$iva 	= 1 + ($tarifa['factura']/100);
																	$costo 	*= $iva;
																}
																else
																{
																	$iva 	= $tarifa['factura'];
																	$costo 	+= $iva;
																}

																// REDONDEAMOS AL ENTERO INMEDIATO SUPERIOR
																$costo 					= ceil($costo);
																$total_familia 			+= $costo;
																$total_familia_aereo 	+= $costo;

																$precios 	.= $costo . '<br/>';
															}
														}
														if($precios == '')
															$precios .= '<br/>';
														?>
															<td style="text-align:right; padding:1px !important;" class="<?php echo $class; ?>" ><?php echo $precios; ?></td>
														<?php
													}
												}
											?>
											<?php
												if(count($a_boletos_aereos)>0)
												{
													if($j == count($paxs) - 1)
													{
													?>
														<td style="text-align:right; border-top:none; padding:1px !important;" class="sb">
															<strong><?php echo $total_familia_aereo; ?></strong>
														</td>
													<?php
													}
													else
													{
													?>
														<td style="border-top:none; border-bottom:none; padding:1px !important;" class="sb"></td>
													<?php
													}	
												}
											?>

										<!-- COLOCAR CADA UNO DE LOS SERVICIOS (HOTELES) -->
											<?php
												$cod_hoteles = c_mostrar_cotizacion::obtener_id_servicios_hoteles($pax['idpax']);

												foreach ($a_servicios_hotel as $hkey => $hdestino) 
												{

													foreach ($hdestino as $idhotel => $hotel) 
													{
														
														foreach ($hotel as $fecha => $tarifas) 
														{
															$precios 		= '';
															$class 			= '';

															for ($k=0; $k < count($tarifas); $k++) 
															{ 
																$tarifa 	= $tarifas[$k];
																$class 		= $tarifa['class'];
																$idtarifa 	= $tarifa['iditem_hotel'];

																if(array_key_exists($idtarifa, $cod_hoteles))
																{
																	$acomodacion = $cod_hoteles[$idtarifa]['t'];
																	$costo = '';
																	$acom  = '';
																	switch ($acomodacion) 
																	{
																		case 1: $costo=$tarifa['precio_single']; $acom='SGL'; break;
																		case 2: $costo=$tarifa['precio_doble']; $acom='DBL'; break;
																		case 3: $costo=$tarifa['precio_triple']; $acom='TPL'; break;
																		case 4: $costo=$tarifa['precio_cuadruple']; $acom='CPL'; break;
																		case 5: $costo=$tarifa['precio_menor']; $acom='CNN'; break;
																		case 6: $costo=$tarifa['precio_infante']; $acom='INF'; break;
																		default: $costo = 0; $acom='N/N'; break;
																	}

																	// MULTIPLICAR POR LA CANTIDAD DE NOCHES
																	$cant_noches 	= floor((strtotime($tarifa['fecha_out'] . ' 00:00:00') - strtotime($tarifa['fecha_in'] . ' 00:00:00')) / 86400);
																	$costo    		*= $cant_noches;

																	// TRANSFORMAR A DOLARES SI ESTA EN BOLIVIANOS
																	if($tarifa['moneda'] == 'B')
																	{
																		$costo /= $tarifa['tipo_cambio'];
																	}

																	// SUMAR FEE
																	if($tarifa['increment_fee'] == 'P')
																	{
																		$fee 	= 1 + ($tarifa['fee']/100);
																		$costo 	*= $fee;
																	}
																	else
																	{
																		$fee 	= $tarifa['fee'];
																		$costo 	+= $fee;
																	}

																	// SUMAR IVA
																	if($tarifa['increment_factura'] == 'P')
																	{
																		$iva 	= 1 + ($tarifa['factura']/100);
																		$costo 	*= $iva;
																	}
																	else
																	{
																		$iva 	= $tarifa['factura'];
																		$costo 	+= $iva;
																	}

																	// REDONDEAMOS AL ENTERO INMEDIATO SUPERIOR
																	$costo 					= ceil($costo);
																	$total_familia 			+= $costo;
																	$total_familia_hotel 	+= $costo;

																	$precios 	.= $costo . '<br/>';
																}
															}
															if($precios == '')
																$precios .= '<br/>';
															?>
																<td style="text-align:right; padding:1px !important;" class="sh <?php echo $class; ?>" ><?php echo $precios; ?></td>
															<?php
														}
													}
												}
											?>
											<?php
												if(count($a_servicios_hotel)>0)
												{
													if($j == count($paxs) - 1)
													{
													?>
														<td style="text-align:right; border-top:none; padding:1px !important;" class="sh">
															<strong><?php echo $total_familia_hotel; ?></strong>
														</td>
													<?php
													}
													else
													{
													?>
														<td style="border-top:none; border-bottom:none; padding:1px !important;" class="sh"></td>
													<?php
													}	
												}
											?>

										<!-- COLOCAR CADA UNO DE LOS SERVICIOS VARIOS -->
											<?php
												$cod_servicios = c_mostrar_cotizacion::obtener_id_otros_servicios($pax['idpax']);
												
												foreach ($a_servicios_varios as $skey => $tipo_servicio) 
												{
													$destinos = $tipo_servicio['tarifas'];
													if(!array_key_exists($skey, $total_familia_servicio))
															$total_familia_servicio[$skey] = 0;

													foreach ($destinos as $dkey => $destino) 
													{
														
														foreach ($destino as $tkey => $tarifas) 
														{
															$precios 		= '';

															for ($k=0; $k < count($tarifas); $k++) 
															{ 
																$tarifa 	= $tarifas[$k];
																$class 		= $tarifa['class'];
																$idtarifa 	= $tarifa['idtarifa_otros'];

																if(array_key_exists($idtarifa, $cod_servicios))
																{
																	$acomodacion = $cod_servicios[$idtarifa]['t'];
																	$costo = '';
																	$acom  = '';
																	switch ($acomodacion) 
																	{
																		case 1: $costo=$tarifa['precio_adulto']; $acom='ADT'; break;
																		case 2: $costo=$tarifa['precio_menor']; $acom='CNN'; break;
																		case 3: $costo=$tarifa['precio_infante']; $acom='INF'; break;
																		default: $costo = 0; $acom='N/N'; break;
																	}

																	// TRANSFORMAR A DOLARES SI ESTA EN BOLIVIANOS
																	if($tarifa['moneda'] == 'B')
																	{
																		$costo /= $tarifa['tipo_cambio'];
																	}

																	// SUMAR FEE
																	if($tarifa['increment_fee'] == 'P')
																	{
																		$fee 	= 1 + ($tarifa['fee']/100);
																		$costo 	*= $fee;
																	}
																	else
																	{
																		$fee 	= $tarifa['fee'];
																		$costo 	+= $fee;
																	}

																	// SUMAR IVA
																	if($tarifa['increment_factura'] == 'P')
																	{
																		$iva 	= 1 + ($tarifa['factura']/100);
																		$costo 	*= $iva;
																	}
																	else
																	{
																		$iva 	= $tarifa['factura'];
																		$costo 	+= $iva;
																	}

																	// REDONDEAMOS AL ENTERO INMEDIATO SUPERIOR
																	$costo 					= ceil($costo);
																	$total_familia 			+= $costo;
																	$total_familia_servicio[$skey] += $costo;

																	$precios 	.= $costo . '<br/>';
																}
															}
															if($precios == '')
																$precios .= '<br/>';
															?>
																<td style="text-align:right; padding:1px !important;" class="<?php echo $class; ?>"><?php echo $precios; ?></td>
															<?php
														}
													}
													?>
													<?php
															if($j == count($paxs) - 1)
															{
															?>
																<td style="text-align:right; border-top:none; padding:1px !important;" class="<?php echo substr($class, 0, 3); ?>" >
																	<strong><?php echo $total_familia_servicio[$skey]; ?></strong>
																</td>
															<?php
															}
															else
															{
															?>
																<td style="border-top:none; border-bottom:none; padding:1px !important;" class="<?php echo substr($class, 0, 3); ?>" ></td>
															<?php
															}
														?>
														<?php
												}
											?>	

										<!-- COLOCAR CADA UNA DE LAS INSCRIPCIONES -->
											<?php
												$cod_inscrip = c_mostrar_cotizacion::obtener_id_servicios_inscripcion($pax['idpax']);// AQUI QUEDE

												foreach ($a_inscripciones as $idevento => $tarifas) 
												{
													$precios 		= '';
													$class 			= '';

													for ($k=0; array_key_exists($k, $tarifas); $k++) 
													{ 
														$tarifa 	= $tarifas[$k];
														$class 		= $tarifa['class'];
														$idtarifa 	= $tarifa['idinscripcion_evento'];

														if(array_key_exists($idtarifa, $cod_inscrip))
														{
															$costo = $tarifa['precio'];
															
															// TRANSFORMAR A DOLARES SI ESTA EN BOLIVIANOS
															if($tarifa['moneda'] == 'B')
															{
																$costo /= $tarifa['tipo_cambio'];
															}

															// SUMAR FEE
															if($tarifa['increment_fee'] == 'P')
															{
																$fee 	= 1 + ($tarifa['fee']/100);
																$costo 	*= $fee;
															}
															else
															{
																$fee 	= $tarifa['fee'];
																$costo 	+= $fee;
															}

															// SUMAR IVA
															if($tarifa['increment_factura'] == 'P')
															{
																$iva 	= 1 + ($tarifa['factura']/100);
																$costo 	*= $iva;
															}
															else
															{
																$iva 	= $tarifa['factura'];
																$costo 	+= $iva;
															}

															// REDONDEAMOS AL ENTERO INMEDIATO SUPERIOR
															$costo 					= ceil($costo);
															$total_familia 			+= $costo;

															$precios 	.= $costo . '<br/>';
														}
													}
													if($precios == '')
														$precios .= '<br/>';
													?>
														<td style="text-align:right; padding:1px !important;" class="<?php echo $class; ?>" ><?php echo $precios; ?></td>
													<?php
												}
											?>

										<?php
											if($j == count($paxs) - 1)
											{
											?>
												<td style="text-align:right; border-top:none; padding:1px !important;" class="total_familia">
													<strong><?php echo $total_familia; ?></strong>
												</td>
											<?php
											}
											else
											{
											?>
												<td style="border-top:none; border-bottom:none; padding:1px !important;" class="total_familia"></td>
											<?php
											}
										?>
								</tr>
							<?php
							}
						}
						else
						{
						?>
							<tr class="<?php echo $color; ?>">
								<td style="padding:1px !important;"><a href="ver_cotizacion_paquete.php?cotizacion=<?php echo $idcot; ?>" target="_blank"><?php echo $idcot; ?></a></td>
								<td style="padding:1px !important;"><?php echo substr($cotizacion['nombre_cliente'], 0, 20); ?></td>
								<td style="padding:1px !important;">- NO INGRESADO -</td>
								<td style="padding:1px !important;" class="no-pax" colspan="<?php echo $total_cols_hotel + $total_columnas_otros_servicios + 1 + count($subtotales_col_destinos) + $total_cols_aereo + $total_columnas_inscripciones; ?>"><br/></td>
							</tr>
						<?php
						}
					}
				?>
				
				<tr style="background-color:#F1F1F1;">
					<td colspan="3" style="border:1px solid #000;">TOTALES</td>

					<!-- AEREO -->
					<?php
						foreach ($a_boletos_aereos as $cod_aepto => $blinea_aerea) 
						{
							foreach ($blinea_aerea as $ruta => $tarifas) 
							{
							?>
								<td style="text-align:right; border:1px solid #000; padding:1px !important;" class="sb"><?php echo $a_boletos_aereos[$cod_aepto][$ruta]['total_servicio']; ?></td>
							<?php
							}
						}
					?>
					<td style="text-align:right; border:1px solid #000; padding:1px !important;" class="sb"><?php echo $total_servicio_boleto; ?></td>

					<!-- HOTEL -->
					<?php
						foreach ($a_servicios_hotel as $hkey => $hdestino) 
						{
							foreach ($hdestino as $idhotel => $hotel) 
							{
								foreach ($hotel as $fecha => $tarifas) 
								{
								?>
									<td style="text-align:right; border:1px solid #000; padding:1px !important;" class="sh"><?php echo $a_servicios_hotel[$hkey][$idhotel][$fecha]['total_servicio']; ?></td>
								<?php
								}
							}
						}
					?>
					<td style="text-align:right; border:1px solid #000; padding:1px !important;" class="sh"><?php echo $total_servicio_hoteles; ?></td>

					<!-- OTROS SERVICIOS -->
					<?php 
						$l = 0;
						foreach ($a_servicios_varios as $skey => $tipo_servicio) 
						{ 
							$cls = 'so' . $l;
							$destinos 				= $tipo_servicio['tarifas'];
							foreach ($destinos as $dkey => $destino) 
							{
								foreach ($destino as $tkey => $tarifas) 
								{
								?>
									<td style="text-align:right; border:1px solid #000; padding:1px !important;" class="<?php echo $cls; ?>" ><?php echo $total_servicio_otros[$skey]['subtotales'][$dkey][$tkey]['total_servicio']; ?></td>
								<?php
								}
							}
							?>
								<td style="text-align:right; border:1px solid #000; padding:1px !important;" class="<?php echo $cls; ?>"><?php echo $total_servicio_otros[$skey]['total']; ?></td>
							<?php
							$l++;
						}
					?>
					
					<!-- INSCRIPCIONES -->
					<?php
						foreach ($a_inscripciones as $idevento => $tarifas) 
						{
						?>
							<td style="text-align:right; border:1px solid #000; padding:1px !important;" class="si"><?php echo $a_inscripciones[$idevento]['total_servicio']; ?></td>
						<?php
						}
					?>
					<td style="text-align:right; border:1px solid #000; padding:1px !important;" class="total_familia"><strong><?php echo $total_costo_paquete; ?></strong></td>
				</tr>

			</table>
		</div>

		<div class="vista_reserva" style="width:100%; display:none;">

			<h3>Detalle de Servicios por Pasajero: <?php echo $info_paquete[0]['titulo']; ?></h3>

			<!-- TABLA DE AEREO (CODIGOS DE RESERVA) -->
			<?php
				if($total_columnas_boletos_aereos > 0)
				{
				?>
					<table class="tabla-resumen sb" style="width:100%; margin-bottom:10px; font-family:Courier New;">
						<tr class="tabla-resumen-print">
							<th colspan="12" class="nombre_servicio">PASAJES AEREOS</th>
						</tr>
						<tr class="tabla-resumen-print">
							<th class="subtitle">N</th>
							<th class="subtitle">PASAJERO</th>
							<th class="subtitle">ACOM</th>
							<th class="subtitle">COSTO<br/>$US</th>
							<th class="subtitle">LINEA AEREA</th>
							<th class="subtitle">RUTA</th>
							<th class="subtitle">CODIGO</th>
							<th class="subtitle col_time_limit">TIME LIMIT</th>
							<th class="subtitle col_adjunto">ADJUNTOS</th>
							<th class="subtitle col_pass">PASS</th>
							<th class="subtitle col_fnac">FECHA NAC</th>
						</tr>
						<?php
							$color 		= 'gray';
							$cont_pax 	= 1;
							foreach ($a_boletos_aereos as $cod_aepto => $blinea_aerea) 
							{
								foreach ($blinea_aerea as $ruta => $tarifas) 
								{
									$precios 		= '';
									$class 			= '';

									for ($k=0; array_key_exists($k, $tarifas); $k++) 
									{ 
										$tarifa 	= $tarifas[$k];
										$class 		= $tarifa['class'];
										$idtarifa 	= $tarifa['idboleto_cotizado'];

										$pasajeros 	= c_mostrar_cotizacion::obtener_pasajeros_por_servicio_aereo($idtarifa);
										$adjuntos 	= c_mostrar_cotizacion::mostrar_adjuntos_por_boleto($idtarifa);

										if(count($pasajeros) > 0)
										{
											if($color == 'gray')
												$color = 'white';
											else
												$color = 'gray';	
										}

										for ($x=0; $x < count($pasajeros); $x++) 
										{ 
											$pax 	= $pasajeros[$x];
											$acom 	= '';
											$costo 	= '';
											switch ($pax['tipo_precio']) 
											{
												case 1: $costo=$tarifa['precio_adulto']; $acom='ADT'; break;
												case 2: $costo=$tarifa['precio_menor']; $acom='CNN'; break;
												case 3: $costo=$tarifa['precio_infante']; $acom='INF'; break;
												default: $costo=0; $acom='N/N'; break;
											}

											// TRANSFORMAR A DOLARES SI ESTA EN BOLIVIANOS
											if($tarifa['moneda'] == 'B')
											{
												$costo /= $tarifa['tipo_cambio'];
											}

											// SUMAR FEE
											if($tarifa['increment_fee'] == 'P')
											{
												$fee 	= 1 + ($tarifa['fee']/100);
												$costo 	*= $fee;
											}
											else
											{
												$fee 	= $tarifa['fee'];
												$costo 	+= $fee;
											}

											// SUMAR IVA
											if($tarifa['increment_factura'] == 'P')
											{
												$iva 	= 1 + ($tarifa['factura']/100);
												$costo 	*= $iva;
											}
											else
											{
												$iva 	= $tarifa['factura'];
												$costo 	+= $iva;
											}

											// REDONDEAMOS AL ENTERO INMEDIATO SUPERIOR
											$costo 		= ceil($costo);

											?>
												<tr class="<?php echo $color; ?>" >
													<td style="padding:1px !important;" ><?php echo $cont_pax; ?></td>
													<td style="padding:1px !important;" ><a style="text-decoration:none;" href="ver_cotizacion_paquete.php?cotizacion=<?php echo $pax['idcotizacion']; ?>" target="_blank"><?php echo ucwords(strtolower($pax['nombre_cliente'] . ' ' . $pax['apellido_cliente'])); ?></a></td>
													<td style="padding:1px !important;" ><?php echo $acom; ?></td>
													<td style="padding:1px !important;" ><?php echo $costo; ?></td>
													<?php
														if($x == 0)
														{
															$fecha_limit 	= strtotime($tarifa['time_limit']);
															$fecha_actual 	= strtotime("now");
														?>
															<td style="padding:1px !important;" rowspan="<?php echo count($pasajeros); ?>">
																<?php echo strtoupper(substr($tarifa['nombre_linea'], 0, 10)); ?>
															</td>
															<td style="padding:1px !important;" rowspan="<?php echo count($pasajeros); ?>">
																<?php echo strtoupper($tarifa['ruta']); ?>
															</td>
															<td style="padding:1px !important;" rowspan="<?php echo count($pasajeros); ?>">
																<?php echo $tarifa['codigo_reserva']; ?>
															</td>
															<td class="col_time_limit" rowspan="<?php echo count($pasajeros); ?>" style="padding:1px !important; background-color:#FFCCCC; <?php if($fecha_actual >= $fecha_limit) { ?>color:#FF3300;<?php } ?>">
																<?php echo date('d/M', strtotime($tarifa['time_limit'])); ?>
															</td>
															<td style="line-height:1; padding:1px !important;" class="col_adjunto" rowspan="<?php echo count($pasajeros); ?>">
																<?php
																for ($nadj=0; $nadj < count($adjuntos); $nadj++) 
																{ 
																	$adjunto = $adjuntos[$nadj];
																?>
																	<a href="<?php echo $ruta_adjuntos . $adjunto['nombre']; ?>"><?php echo $adjunto['descripcion']; ?></a><br/>
																<?php
																}
																?>
															</td>
														<?php
														}
													?>
													<td style="padding:1px !important;" class="col_pass"><?php echo $pax['numero_pasaporte_cliente']; ?></td>
													<td style="padding:1px !important;" class="col_fnac"><?php echo date('d/m/Y', strtotime($pax['fecha_nacimiento_cliente'])); ?></td>
												</tr>
											<?php
											$cont_pax++;
										}
									}
								}
							}
						?>
					</table>
				<?php
				}
			?>

			<table class="tabla-resumen sbb" style="width:100%; margin-bottom:5px; font-family:Courier New;">
				<tr class="tabla-resumen-print">
					<th class="subtitle">N</th>
					<th class="subtitle">PAX</th>
					<?php
						for($i=0; $i < $max_cant_rutas; $i++) 
						{ 
						?>
							<th class="subtitle">CIA</th>
							<th class="subtitle">COSTO</th>
							<th class="subtitle">RUTA</th>
							<th class="subtitle">CODIGO</th>
							<th class="subtitle">T.LIM<br/>TKT</th>
						<?php
						}
					?>
				</tr>
				<?php
					$color 		= 'gray';
					$cont_pax   = 1;
					foreach ($a_pax_aereo as $idcot => $cot) 
					{
						if(count($cot) > 0)
						{
							if($color == 'gray')
								$color = 'white';
							else
								$color = 'gray';

							$idciudad = '';
							for ($i=0; $i < count($cot); $i++) 
							{ 
								$name_pax = $cot[$i]['nombre'];
								$rutas 	  = $cot[$i]['rutas'];

								if($cot[$i]['idorg'] != $idciudad)
								{
									?>
										<tr class="<?php echo $color; ?>">
											<td colspan="<?php echo ($max_cant_rutas*5) + 2; ?>" style="font-weight:bold; text-align:center; padding:2px !important;" >
												SALE DESDE <?php echo $cot[$i]['origen']; ?>
											</td>
										</tr>
									<?php
									$idciudad = $cot[$i]['idorg'];
								}

								?>
									<tr class="<?php echo $color; ?>">
										<td style="padding:1px !important;" ><?php echo $cont_pax; ?></td>
										<td style="padding:1px !important;" ><?php echo $name_pax; ?></td>
								<?php
								for ($j=0; $j < count($rutas); $j++) 
								{ 
									?>
										<td style="border-left:2px solid #000 !important; padding:1px !important;"><?php echo $rutas[$j]['cia']; ?></td>
										<td style="padding:1px !important;" ><?php echo $rutas[$j]['costo']; ?></td>
										<td title="<?php echo $rutas[$j]['ruta']; ?>" style="padding:1px !important;" ><?php echo substr($rutas[$j]['ruta'], 0, 11) . '..'; ?></td>
										<td style="padding:1px !important;" ><?php echo $rutas[$j]['codigo']; ?></td>
										<td style="padding:1px !important;" ><?php echo $rutas[$j]['tlimit']; ?></td>
									<?php
								} 
								while ($j<$max_cant_rutas) 
								{
									?>
										<!-- COMPLETAMOS LAS CELDAS -->
										<td style="border-left:2px solid #000 !important;"></td><td></td><td></td><td></td><td></td>
									<?php
									$j++;
								}	
								?>
									</tr>
								<?php
								$cont_pax++;
							}
						}
					}
				?>
			</table>

			<!-- TABLA DE HOTELES (CODIGOS DE RESERVA) -->
			<?php
				if($total_columnas_hoteles_in > 0)
				{
					$habitaciones = 0;
				?>
					<table class="tabla-resumen sh" style="width:100%; margin-bottom:10px; font-family:Courier New;">
						<tr class="tabla-resumen-print">
							<th colspan="13" class="nombre_servicio">HOTEL</th>
						</tr>
						<tr class="tabla-resumen-print">
							<th class="subtitle">N</th>
							<th class="subtitle">HAB</th>
							<th class="subtitle">PASAJERO</th>
							<th class="subtitle">ACOM</th>
							<th class="subtitle">COSTO<br/>$US</th>
							<th class="subtitle">HOTEL</th>
							<th class="subtitle">IN</th>
							<th class="subtitle">OUT</th>
							<th class="subtitle">CODIGO</th>
							<th class="subtitle col_time_limit">TIME<br/>LIMIT</th>
							<th class="subtitle col_adjunto">ADJUNTOS</th>
							<th class="subtitle col_pass">PASS</th>
							<th class="subtitle col_fnac">FECHA<br/>NAC</th>
						</tr>
						<?php
							$color 		= 'gray';
							$cont_pax 	= 1;
							foreach ($a_servicios_hotel as $hkey => $hdestino) 
							{
								foreach ($hdestino as $idhotel => $hotel) 
								{
									foreach ($hotel as $fecha => $tarifas) 
									{
										$precios 		= '';
										$class 			= '';

										for ($k=0; $k < count($tarifas); $k++) 
										{ 
											$tarifa 	= $tarifas[$k];
											$class 		= $tarifa['class'];
											$idtarifa 	= $tarifa['iditem_hotel'];

											$pasajeros 	= c_mostrar_cotizacion::obtener_pasajeros_por_servicio_hotel($idtarifa);
											$adjuntos 	= c_mostrar_cotizacion::mostrar_adjuntos_por_hotel($idtarifa);

											if(count($pasajeros) > 0)
											{
												if($color == 'gray')
													$color = 'white';
												else
													$color = 'gray';	
											}

											$nro_hab 	= '';

											for ($x=0; $x < count($pasajeros); $x++) 
											{ 
												$pax 	= $pasajeros[$x];
												$acom 	= '';
												$costo 	= '';
												$acom  	= '';
												switch ($pax['tipo_precio']) 
												{
													case 1: $costo=$tarifa['precio_single']; $acom='SGL'; break;
													case 2: $costo=$tarifa['precio_doble']; $acom='DBL'; break;
													case 3: $costo=$tarifa['precio_triple']; $acom='TPL'; break;
													case 4: $costo=$tarifa['precio_cuadruple']; $acom='CPL'; break;
													case 5: $costo=$tarifa['precio_menor']; $acom='CNN'; break;
													case 6: $costo=$tarifa['precio_infante']; $acom='INF'; break;
													default: $costo = 0; $acom='N/N'; break;
												}

												// MULTIPLICAR POR LA CANTIDAD DE NOCHES
												$cant_noches 	= floor((strtotime($tarifa['fecha_out'] . ' 00:00:00') - strtotime($tarifa['fecha_in'] . ' 00:00:00')) / 86400);
												$costo    		*= $cant_noches;

												// TRANSFORMAR A DOLARES SI ESTA EN BOLIVIANOS
												if($tarifa['moneda'] == 'B')
												{
													$costo /= $tarifa['tipo_cambio'];
												}

												// SUMAR FEE
												if($tarifa['increment_fee'] == 'P')
												{
													$fee 	= 1 + ($tarifa['fee']/100);
													$costo 	*= $fee;
												}
												else
												{
													$fee 	= $tarifa['fee'];
													$costo 	+= $fee;
												}

												// SUMAR IVA
												if($tarifa['increment_factura'] == 'P')
												{
													$iva 	= 1 + ($tarifa['factura']/100);
													$costo 	*= $iva;
												}
												else
												{
													$iva 	= $tarifa['factura'];
													$costo 	+= $iva;
												}

												// REDONDEAMOS AL ENTERO INMEDIATO SUPERIOR
												$costo 		= ceil($costo);
												
												// VERIFICAMOS LA HABITACION
												if($nro_hab != $pax['habitacion']) 
												{  
													$nro_hab = $pax['habitacion'];
													$sw = true;
													$habitaciones++;
												}
												else
													$sw = false;

												?>
													<tr class="<?php echo $color; ?>" >
														<td style="<?php if($sw) echo 'border-top:2px solid #000;'; ?>padding:1px !important;" ><?php echo $cont_pax; ?></td>
														<td style="padding:1px !important;<?php if($sw) echo 'border-top:2px solid #000; border-bottom:none;";'; else echo 'border-top:none; border-bottom:none;'; ?>" ><?php if($sw) echo $habitaciones; ?></td>
														<td style="<?php if($sw) echo 'border-top:2px solid #000;'; ?>padding:1px !important;" ><a style="text-decoration:none;" href="ver_cotizacion_paquete.php?cotizacion=<?php echo $pax['idcotizacion']; ?>" target="_blank"><?php echo ucwords(strtolower($pax['nombre_cliente'] . ' ' . $pax['apellido_cliente'])); ?></a></td>
														<td style="<?php if($sw) echo 'border-top:2px solid #000;'; ?>padding:1px !important;" ><?php echo $acom; ?></td>
														<td style="<?php if($sw) echo 'border-top:2px solid #000;'; ?>padding:1px !important;" ><?php echo $costo; ?></td>
														<?php
															if($x == 0)
															{
																$fecha_limit 	= strtotime($tarifa['time_limit']);
																$fecha_actual 	= strtotime("now");
															?>
																<td rowspan="<?php echo count($pasajeros); ?>" style="padding:1px !important;">
																	<?php echo substr($tarifa['nombre_hotel'], 0, 10) . ' ' . $tarifa['categoria'] . '*'; ?>
																</td>
																<td rowspan="<?php echo count($pasajeros); ?>" style="padding:1px !important;">
																	<?php echo date('d/M', strtotime($tarifa['fecha_in'])); ?>
																</td>
																<td rowspan="<?php echo count($pasajeros); ?>" style="padding:1px !important;">
																	<?php echo date('d/M', strtotime($tarifa['fecha_out'])); ?>
																</td>
																<td rowspan="<?php echo count($pasajeros); ?>" style="padding:1px !important;">
																	<?php echo $tarifa['identificador']; ?>
																</td>
																<td class="col_time_limit" rowspan="<?php echo count($pasajeros); ?>" style="padding:1px !important; background-color:#FFCCCC; <?php if($fecha_actual >= $fecha_limit) { ?>color:#FF3300;<?php } ?>">
																	<?php echo date('d/M', strtotime($tarifa['time_limit'])); ?>
																</td>
																<td style="line-height:1; padding:1px !important;" class="col_adjunto" rowspan="<?php echo count($pasajeros); ?>">
																	<?php
																	for ($nadj=0; $nadj < count($adjuntos); $nadj++) 
																	{ 
																		$adjunto = $adjuntos[$nadj];
																	?>
																		<a href="<?php echo $ruta_adjuntos . $adjunto['nombre']; ?>"><?php echo $adjunto['descripcion']; ?></a><br/>
																	<?php
																	}
																	?>
																</td>
															<?php
															}
														?>
														<td class="col_pass" style="padding:1px !important;"><?php echo $pax['numero_pasaporte_cliente']; ?></td>
														<td class="col_fnac" style="padding:1px !important;"><?php echo date('d/m/Y', strtotime($pax['fecha_nacimiento_cliente'])); ?></td>
													</tr>
												<?php
												$cont_pax++;
											}
										}
									}
								}
							}
						?>
					</table>

					<span style="float:right; font-size:7pt;" class="sh">
						<div style="width:30px; float:left;"><hr style="border:1px solid #000;" /></div>
						<div style="width:120px; float:left; margin-left:10px;">Separador de Habitaciones</div>
					</span>
				<?php
				}
			?>

			<!-- MUESTRA TABLA DEL RESTO DE LOS SERVICIOS -->
			<?php
				$i = 0;
				
				foreach ($a_servicios_varios as $skey => $tipo_servicio)
				{ 
					$nombre 	= $tipo_servicio["nombre"];
					$destinos 	= $tipo_servicio['tarifas'];
					$aux 		= $subtotales_col_destinos[$i];
					$color 		= 'gray';
				?>
					<table class="tabla-resumen <?php echo $aux[2]; ?>" style="width:100%; margin-bottom:20px; font-family:Courier New;">
						<tr class="tabla-resumen-print">
							<th colspan="12" class="nombre_servicio"><?php echo $nombre; ?></th>
						</tr>
						<tr class="tabla-resumen-print">
							<th class="subtitle">N</th>
							<th class="subtitle">PASAJERO</th>
							<th class="subtitle">ACOM</th>
							<th class="subtitle">COSTO<br/>$US</th>
							<th class="subtitle">DESCR</th>
							<th class="subtitle">DESDE</th>
							<th class="subtitle">HASTA</th>
							<th class="subtitle">CODIGO</th>
							<th class="subtitle col_time_limit">TIME<br/>LIMIT</th>
							<th class="subtitle col_adjunto">ADJUNTOS</th>
							<th class="subtitle col_pass">PASS</th>
							<th class="subtitle col_fnac">FECHA<br/>NAC</th>
						</tr>
						<?php
							$cont_pax = 1;
							foreach ($destinos as $dkey => $destino) 
							{
								foreach ($destino as $tkey => $tarifas) 
								{
									for ($k=0; $k < count($tarifas); $k++) 
									{ 
										$tarifa 	= $tarifas[$k];
										$idtarifa 	= $tarifa['idtarifa_otros'];

										if($color == 'gray')
											$color = 'white';
										else
											$color = 'gray';

										$pasajeros = c_mostrar_cotizacion::obtener_pasajeros_por_servicio($idtarifa);
										$adjuntos 	= c_mostrar_cotizacion::mostrar_adjuntos_por_servicio($idtarifa);

										for ($x=0; $x < count($pasajeros); $x++) 
										{ 
											$pax 	= $pasajeros[$x];
											$acom 	= '';
											switch ($pax['tipo_precio']) 
											{
												case 1: $costo=$tarifa['precio_adulto']; $acom='ADT'; break;
												case 2: $costo=$tarifa['precio_menor']; $acom='CNN'; break;
												case 3: $costo=$tarifa['precio_infante']; $acom='INF'; break;
												default: $costo=0; $acom='N/N'; break;
											}

											// TRANSFORMAR A DOLARES SI ESTA EN BOLIVIANOS
											if($tarifa['moneda'] == 'B')
											{
												$costo /= $tarifa['tipo_cambio'];
											}

											// SUMAR FEE
											if($tarifa['increment_fee'] == 'P')
											{
												$fee 	= 1 + ($tarifa['fee']/100);
												$costo 	*= $fee;
											}
											else
											{
												$fee 	= $tarifa['fee'];
												$costo 	+= $fee;
											}

											// SUMAR IVA
											if($tarifa['increment_factura'] == 'P')
											{
												$iva 	= 1 + ($tarifa['factura']/100);
												$costo 	*= $iva;
											}
											else
											{
												$iva 	= $tarifa['factura'];
												$costo 	+= $iva;
											}

											// REDONDEAMOS AL ENTERO INMEDIATO SUPERIOR
											$costo 		= ceil($costo);
										?>
											<tr class="<?php echo $color; ?>">
												<td style="padding:1px !important"><?php echo $cont_pax; ?></td>
												<td style="padding:1px !important"><a style="text-decoration:none;" href="ver_cotizacion_paquete.php?cotizacion=<?php echo $pax['idcotizacion']; ?>" target="_blank"><?php echo ucwords(strtolower($pax['nombre_cliente'] . ' ' . $pax['apellido_cliente'])); ?></a></td>
												<td style="padding:1px !important"><?php echo $acom; ?></td>
												<td style="padding:1px !important"><?php echo $costo; ?></td>
												<?php
													if($x == 0)
													{
														$fecha_limit 	= strtotime($tarifa['time_limit']);
														$fecha_actual 	= strtotime("now");
													?>
														<td style="padding:1px !important" rowspan="<?php echo count($pasajeros); ?>">
															<?php echo substr($tarifa['descripcion'], 0, 15); ?>
														</td>
														<td style="padding:1px !important" rowspan="<?php echo count($pasajeros); ?>">
															<?php echo date('d/M', strtotime($tarifa['fecha_desde'])); ?>
														</td>
														<td style="padding:1px !important" rowspan="<?php echo count($pasajeros); ?>">
															<?php echo date('d/M', strtotime($tarifa['fecha_hasta'])); ?>
														</td>
														<td style="padding:1px !important" rowspan="<?php echo count($pasajeros); ?>">
															<?php echo $tarifa['codigo']; ?>
														</td>
														<td class="col_time_limit" rowspan="<?php echo count($pasajeros); ?>" style="padding:1px !important; background-color:#FFCCCC; <?php if($fecha_actual >= $fecha_limit) { ?>color:#FF3300;<?php } ?>">
															<?php echo date('d/M', strtotime($tarifa['time_limit'])); ?>
														</td>
														<td style="line-height:1; padding:1px !important;" class="col_adjunto" rowspan="<?php echo count($pasajeros); ?>">
															<?php
															for ($nadj=0; $nadj < count($adjuntos); $nadj++) 
															{ 
																$adjunto = $adjuntos[$nadj];
															?>
																<a href="<?php echo $ruta_adjuntos . $adjunto['nombre']; ?>"><?php echo $adjunto['descripcion']; ?></a><br/>
															<?php
															}
															?>
														</td>
													<?php
													}
												?>
												<td class="col_pass" style="padding:1px !important"><?php echo $pax['numero_pasaporte_cliente']; ?></td>
												<td class="col_fnac" style="padding:1px !important"><?php echo date('d/m/Y', strtotime($pax['fecha_nacimiento_cliente'])); ?></td>
											</tr>
										<?php
											$cont_pax++;
										}
									}
								}
							}
						?>
					</table>
				<?php
					$i++;
				}
			?>

			<!-- TABLA DE INSCRIPCIONES (CODIGOS DE RESERVA) -->
			<?php
				if($total_columnas_inscripciones > 0)
				{
				?>
					<table class="tabla-resumen si" style="width:100%; margin-bottom:10px; font-family:Courier New;">
						<tr class="tabla-resumen-print">
							<th colspan="13" class="nombre_servicio">INSCRIPCIONES</th>
						</tr>
						<tr class="tabla-resumen-print">
							<th>N</th>
							<th class="subtitle">PASAJERO</th>
							<th class="subtitle col_costo">COSTO<br/>$US</th>
							<th class="subtitle">CATEGORIA</th>
							<th class="subtitle col_time_limit">TIME<br/>LIMIT</th>
							<th class="subtitle col_adjunto">ADJUNTOS</th>
							<th class="subtitle col_telefono">TELEFONO</th>
							<th class="subtitle col_email">EMAIL</th>
							<th class="subtitle col_pass">PASS</th>
							<th class="subtitle col_fnac">FECHA<br/>NAC</th>
						</tr>
						<?php
							$color 		= 'gray';
							$cont_pax	= 1;
							foreach ($a_inscripciones as $idevento => $tarifas) 
							{
								?>
									<tr>
										<td colspan="13" class="col_evento" style="font-weight:bold; text-align:center; padding:2px !important;" >
											<?php echo strtoupper($tarifas[0]['nombre_evento']); ?>
										</td>
									</tr>
								<?php
								for ($k=0; array_key_exists($k, $tarifas); $k++) 
								{ 
									$tarifa 	= $tarifas[$k];
									$idtarifa 	= $tarifa['idinscripcion_evento'];

									$pasajeros 	= c_mostrar_cotizacion::obtener_pasajeros_por_servicio_inscripcion($idtarifa);
									$adjuntos 	= c_mostrar_cotizacion::mostrar_adjuntos_por_inscripcion($idtarifa);

									if(count($pasajeros) > 0)
									{
										if($color == 'gray')
											$color = 'white';
										else
											$color = 'gray';	
									}

									for ($x=0; $x < count($pasajeros); $x++) 
									{ 
										$pax 	= $pasajeros[$x];
										$costo 	= $tarifa['precio'];
										
										// TRANSFORMAR A DOLARES SI ESTA EN BOLIVIANOS
										if($tarifa['moneda'] == 'B')
										{
											$costo /= $tarifa['tipo_cambio'];
										}

										// SUMAR FEE
										if($tarifa['increment_fee'] == 'P')
										{
											$fee 	= 1 + ($tarifa['fee']/100);
											$costo 	*= $fee;
										}
										else
										{
											$fee 	= $tarifa['fee'];
											$costo 	+= $fee;
										}

										// SUMAR IVA
										if($tarifa['increment_factura'] == 'P')
										{
											$iva 	= 1 + ($tarifa['factura']/100);
											$costo 	*= $iva;
										}
										else
										{
											$iva 	= $tarifa['factura'];
											$costo 	+= $iva;
										}

										// REDONDEAMOS AL ENTERO INMEDIATO SUPERIOR
										$costo 		= ceil($costo);

										?>
											<tr class="<?php echo $color; ?>" >
												<td style="padding:1px !important;" ><?php echo $cont_pax; ?></td>
												<td style="padding:1px !important;" ><a style="text-decoration:none;" href="ver_cotizacion_paquete.php?cotizacion=<?php echo $pax['idcotizacion']; ?>" target="_blank"><?php echo ucwords(strtolower($pax['nombre_cliente'] . ' ' . $pax['apellido_cliente'])); ?></a></td>
												<td style="padding:1px !important;" ><?php echo $costo; ?></td>
												<?php
													if($x == 0)
													{
														$fecha_limit 	= strtotime($tarifa['time_limit']);
														$fecha_actual 	= strtotime("now");
													?>
														<td style="padding:1px !important;" rowspan="<?php echo count($pasajeros); ?>">
															<?php echo strtoupper($tarifa['categoria']); ?>
														</td>
														<td class="col_time_limit" rowspan="<?php echo count($pasajeros); ?>" style="padding:1px !important; background-color:#FFCCCC; <?php if($fecha_actual >= $fecha_limit) { ?>color:#FF3300;<?php } ?>">
															<?php echo date('d/M', strtotime($tarifa['time_limit'])); ?>
														</td>
														<td style="line-height:1; padding:1px !important;" class="col_adjunto" rowspan="<?php echo count($pasajeros); ?>">
															<?php
															for ($nadj=0; $nadj < count($adjuntos); $nadj++) 
															{ 
																$adjunto = $adjuntos[$nadj];
															?>
																<a href="<?php echo $ruta_adjuntos . $adjunto['nombre']; ?>"><?php echo $adjunto['descripcion']; ?></a><br/>
															<?php
															}
															?>
														</td>
													<?php
													}
												?>
												<td style="padding:1px !important;" class="col_telefono"><?php echo $info_contacto[$pax['idclientes']]['telefono']; ?></td>
												<td style="padding:1px !important;" class="col_email"><?php echo $info_contacto[$pax['idclientes']]['correo']; ?></td>
												<td style="padding:1px !important;" class="col_pass"><?php echo $pax['numero_pasaporte_cliente']; ?></td>
												<td style="padding:1px !important;" class="col_fnac"><?php echo date('d/m/Y', strtotime($pax['fecha_nacimiento_cliente'])); ?></td>
											</tr>
										<?php
										$cont_pax++;
									}
								}
							}
						?>
					</table>
				<?php
				}
			?>
		</div>

	</div>
</div>