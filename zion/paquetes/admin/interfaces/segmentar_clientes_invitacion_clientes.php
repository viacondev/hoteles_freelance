<?php
	set_time_limit(0);
	include 'header.php';
	ini_set('display_errors', 1);
	include('../entidad/clientes.php');
	include('../entidad/imagen.php');
	include('../entidad/evento.php');
	include('../control/c_segmentar_clientes.php');

	$c_segmentar_clientes = new c_segmentar_clientes;

	if(isset($_GET['e']))
	{
		$_SESSION['segmentados'] 	= array();
		$_SESSION['criterios'] 		= array();
	}

	if(!isset($_SESSION['segmentados']))
	{
		$_SESSION['segmentados'] 	= array();
		$_SESSION['criterios'] 		= array();	
	}

	/*
	* REALIZAMOS LA BUSQUEDA EN CASO DE NECESITARSE
	*/
	if(isset($_POST['rubro']))
	{
		$clientes_seleccionados 	= $_SESSION['segmentados'];
		$clientes_segmentados = $c_segmentar_clientes->segmentar_clientes($_POST['rubro'], $_POST['profesion'], $_POST['especialidad']);
		if(count($clientes_seleccionados) > 0)
		{
			if(count($clientes_segmentados) > 0 && $clientes_segmentados !== false)
				$nueva_segmentacion 	= array_merge($clientes_seleccionados, $clientes_segmentados);
			else
				$nueva_segmentacion 	= $clientes_seleccionados;
		}
		else
		{
			$nueva_segmentacion 	= $clientes_segmentados;
		}
		
		$resultado_actual 	= array_map("unserialize", array_unique(array_map("serialize", $nueva_segmentacion)));

		$_SESSION['segmentados'] 	= $resultado_actual;

		$criterios_segmentacion 	= $_SESSION['criterios'];
		if(isset($_POST['rubro']) && $_POST['rubro'] != 0)
		{
			$rubro = $c_segmentar_clientes->obtener_rubro($_POST['rubro']);
			$criterios_segmentacion[] = 'SEGMENTADO POR RUBRO (' . $rubro['nombre_rubro'] . ')';
		}
		if(isset($_POST['especialidad']) && $_POST['especialidad'] != 0)
		{
			$especialidad = $c_segmentar_clientes->obtener_especialidad($_POST['especialidad']);
			$criterios_segmentacion[] = 'SEGMENTADO POR ESPECIALIDAD (' . $especialidad['nombre_especialidad'] . ')';
		}
		if(isset($_POST['profesion']) && $_POST['profesion'] != 0)
		{
			$profesion = $c_segmentar_clientes->obtener_profesion($_POST['profesion']);
			$criterios_segmentacion[] = 'SEGMENTADO POR PROFESION (' . $profesion['nombre_profesion'] . ')';
		}

		$_SESSION['criterios'] = $criterios_segmentacion;
	}
	/*
	* SE VERIFICA SI SE NECESITA VACIAR LOS CLIENTES Y CRITERIOS
	*/
	if(isset($_POST['new_seg']))
	{
		$_SESSION['segmentados'] 	= array();
		$_SESSION['criterios'] 		= array();	
	}

	/*
	* VERIFICAMOS SI SE NECESITA QUITAR ALGUN CLIENTE
	*/
	if(isset($_POST['quitar_clientes']))
	{
		$clientes_seleccionados 	= $_SESSION['segmentados'];
		$cant_cli = $_POST['cant_select'];
		for ($i=0; $i < $cant_cli; $i++) 
		{ 
			if(isset($_POST['chk_cliente_' . $i]))
			{
				unset($clientes_seleccionados[$i]);
			}
		}
		$clientes_seleccionados = array_values($clientes_seleccionados);
		$_SESSION['segmentados'] = $clientes_seleccionados;
	}
	/*
	* VERIFICAMOS SI AGREGA ALGUN/OS CLIENTE/S
	*/
	if(isset($_POST['add_cliente']))
	{
		$cant_cli = $_POST['cant_clientes'];
		for ($i=0; $i < $cant_cli; $i++) 
		{ 
			if(isset($_POST['chk_cliente_' . $i]))
			{
				$client = $c_segmentar_clientes->buscar_cliente($_POST['chk_cliente_' . $i]);
				if($client !== false)
					$clientes_seleccionados[] 	= $client;			
			}
		}
	}

?>
	<center><h2>INVITACION AL SISTEMA CLIENTES</h2></center>
<?php
	/*
	* VERIFICAMOS SI DEBE ENVIAR UN MAIL CON EL LINK DEL EVENTO
	*/
	$ruta 			= "http://viacontours.com/online/js/slider_detalle_paquete/contenido_paquete/data1/images/";
	$imagenes 		= imagen::obtener_imagenes_de_publicacion($_SESSION['evento']);

	$html = '<table width="100%" height="100%" style="width:100%; height:100%;">
			<tr>
				<td style="background-color:#F2F2F2;">
					<center>
						<table style="width:700px; border:1px solid #345e8f; background-color:#FFFFFF;">
							<tr>
								<td colspan="2" style="background-color:#345e8f; color:#FFFFFF;">
									<center><h3 style="font-family:Lucida Sans, Sans serif; color:#ffffff; margin:0;">NUEVA PLATAFORMA WEB DE CONGRESOS Y PAQUETES</h3></center>
								</td>
							</tr>
							<tr>
								<td colspan="2" style="font-family:Lucida Sans, Sans serif; font-size:12pt; color:#345e8f; padding:10px;">
									/nombre_cli/..
								</td>
							</tr>
							<tr>
								<td colspan="2" style="font-family:Lucida Sans, Sans serif; padding-left:10px; padding-right:10px; font-size:9pt;">
									NUESTRO DEPARTAMENTO DE CONGRESOS HA CREADO UNA PLATAFORMA DONDE PUBLICAREMOS LOS MAS IMPORTANTES EVENTOS MEDICOS, UD. RECIBIRA INFORMACION SOBRE LOS MISMOS POR ESTE MEDIO, DE IGUAL MANERA SERAN PUBLICADOS EN NUESTRA PAGINA  WEB <a style="color:#345e8f;" href="http://viacontours.com/clientes">www.viacontours.com/clientes</a>
									<p/>
									EN ESTA PAGINA UD. PODRA ACCEDER A TODOS NUESTROS SERVICIOS ONLINE COMO SER: RESERVAS DE BOLETOS AEREOS, PAQUETES TURISTICOS, ESTADOS DE CUENTAS, CONGRESOS, Y MUCHO MAS.
									<p/>
									PARA PODER INGRESAR ES NECESARIO TENER UNA FIRMA EN NUESTRO SISTEMA,  SI UD. YA CUENTA CON UNA INGRESE DE FORMA HABITUAL <a style="color:#345e8f;" href="http://viacontours.com/clientes">AQUI</a>, DE LO COTRARIO HAGA SU SOLICITUD <a style="color:#345e8f;" href="http://viacontours.com/clientes/enviar_correo_solicitud_cuenta_clientes.php?c=/idcli/">AQUI</a>.<p/>
									ESPERAMOS QUE ESTA HERRAMIENTA SEA DE GRAN  UTILIDAD PARA UD.<p/>
									ATENTAMENTE<br/>
									&nbsp;&nbsp;&nbsp;&nbsp;- DPTO DE CONGRESOS 
									<hr/>
								</td>
							</tr>
							<tr>
								<td style="width:400px;">
									<img src="http://viacontours.com/clientes/images/logo-mini.png" style="width:300px;" />
								</td>
								<td style="width:300px; font-family:Lucida Sans, Sans serif; font-size:7pt;">
									CALLE VELASCO NRO.232 ESQ. SUAREZ DE FIGUEROA<br/>
									TELFEFONO 3-363610<br/>
									PAGINA WEB <a href="http://viacontours.com">www.viacontours.com</a>
								</td>
							</tr>
						</table>
					</center>
				</td>
			</tr>
		</table>';

	if(isset($_POST['enviar']))
	{
		$i=0;
		$contador_clientes_total 		= 0;
		$contador_clientes_con_mail 	= 0;
		$contador_clientes_sin_mail 	= 0;
		$contador_envios_correctos 		= 0;
		$contador_envios_incorrectos 	= 0;

		$titulo 		= 'NUEVA PLATAFORMA PARA CONGRESOS Y PAQUETES';
		$headers  		= 'MIME-Version: 1.0' . "\r\n";
		$headers 		.= 'Content-type: text/html; charset=utf-8' . "\r\n";    
		$headers 		.= "From: VIACONTOURS <congresos@viacontours.com>" . "\r\n";

		while (isset($_POST['cliente_enviar_' . $i])) 
		{
			$datos_cliente 		= $c_segmentar_clientes->buscar_cliente($_POST['cliente_enviar_' . $i]);
			$nombre_completo 	= $datos_cliente['trato_cliente'] . ' ' . $datos_cliente['nombre_cliente'] . ' ' . $datos_cliente['apellido_cliente'];
			$codigo_cliente 	= $datos_cliente['codigo_cliente'];
			/*
			*/
			$idcliente = $datos_cliente['idclientes'];

			$correos_cliente = $c_segmentar_clientes->obtener_correos_cliente($_POST['cliente_enviar_' . $i]);

			echo '<font style="margin-left:5px; font-weight:bold;">ENVIANDO A ' . $nombre_completo . '</font><br/>';

			if($correos_cliente !== false)
			{		
				$contenido = str_replace('/nombre_cli/', $nombre_completo, $html);
				$contenido = str_replace('/poner_codigo/', $codigo_cliente, $contenido);
				$contenido = str_replace('/idcli/', $idcliente, $contenido);

				for ($j=0; $j < count($correos_cliente); $j++) 
				{ 
					$para 		= $correos_cliente[$j]['e_mail'];
					$enviado 	= mail($para,$titulo,$contenido, $headers);
					if($enviado === true)
					{
						echo '<font style="margin-left:20px; color:#007700;">ENVIADO CORRECTAMENTE A ' . $para . '</font><br/>';
						$contador_envios_correctos++;
					}
					else
					{
						echo '<font style="margin-left:20px; color:#FF0000;">NO SE PUDO ENVIAR A ' . $para . '</font><br/>';
						$contador_envios_incorrectos++;
					}
				}
				$contador_clientes_con_mail++;
			}
			else
			{
				echo '<font style="margin-left:20px; color:#F000F0;">ESTE CLIENTE NO TIENE CORREOS</font><br/>';
				$contador_clientes_sin_mail++;
			}
			$i++;
			$contador_clientes_total++ ;
		}

		echo "<hr/>EL ULTIMO MENSAJE HA SIDO REENVIADO A congresos@viacontours.com<hr/>";
		echo "SE ENVIO CORREOS A " . $contador_clientes_total . " CLIENTES DE LOS CUALES :<br/>";
		echo '&nbsp;&nbsp;* ' . $contador_clientes_con_mail . " TIENEN AL MENOS UN CORREO REGISTRADO.<br/>";
		echo '&nbsp;&nbsp;* ' . $contador_clientes_sin_mail . " NO TIENEN CORREO.<br/>";
		echo "EN EL ENVIO DE MENSAJES POR MAIL ..<br/>";
		echo '&nbsp;&nbsp;* ' . $contador_envios_correctos . " SE ENVIARON CORRECTAMENTE.<br/>";
		echo '&nbsp;&nbsp;* ' . $contador_envios_incorrectos . " NO LOGRARON ENVIAR.";

		$contenido_agente =  $contenido . '<center>
									<table>
										<tr>
											<td style="background-color:#FFFFFF; border:1px solid #d1d1d1; padding:5px;" >
												<table>
													<tr>
														<td style="padding:10px; background-color:#e1e1e1;">
															<table style="font-family:Arial, Helvetica, sans-serif; width:700px; font-size:10pt; color:#333333;">
																<tr>
																	<td>
																		SE ENVIO CORREOS A ' . $contador_clientes_total . ' CLIENTES DE LOS CUALES :
																	</td>
																</tr>
																<tr>
																	<td style="padding-left:20px;">
																		* ' . $contador_clientes_con_mail . ' TIENEN AL MENOS UN CORREO REGISTRADO.<br/>
																		* ' . $contador_clientes_sin_mail . ' NO TIENEN CORREO.
																	</td>
																</tr>
																<tr>
																	<td>
																		EN EL ENVIO DE MENSAJES POR MAIL ..
																	</td>
																</tr>
																<tr>
																	<td style="padding-left:20px;">
																		* ' . $contador_envios_correctos . ' SE ENVIARON CORRECTAMENTE.<br/>
																		* ' . $contador_envios_incorrectos . ' NO LOGRARON ENVIAR.
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>			
											</td>
										</tr>
									</table>
								</center>';
		$enviado 	= mail('congresos@viacontours.com',$titulo,$contenido_agente, $headers);
		include('footer.php');
		exit();
	}

?>

<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" id="form_segmentar" name="form_segmentar">
	<div class="four columns">
		<label>Rubro</label>
		<select id="rubro" name="rubro">
			<option value="0">Ninguno</option>
			<?php
				$rubros = $c_segmentar_clientes->obtener_rubros();
				for ($i=0; $i < count($rubros); $i++) 
				{ 
			?>
					<option value="<?php echo $rubros[$i]['idrubro']; ?>"><?php echo strtoupper($rubros[$i]['nombre_rubro']); ?></option>
			<?php
				}
			?>
		</select>
	</div>
	<div class="four columns">
		<label>Profesion</label>
		<select id="profesion" name="profesion" onchange="Mostrar_Especialidades();">
			<option value="0">Ninguno</option>
			<?php
				$profesiones = $c_segmentar_clientes->obtener_profesiones();
				for ($i=0; $i < count($profesiones); $i++) 
				{ 
			?>
					<option value="<?php echo $profesiones[$i]['idprofesiones']; ?>"><?php echo strtoupper($profesiones[$i]['nombre_profesion']); ?></option>
			<?php
				}
			?>
		</select>
	</div>
	<div class="four columns">
		<label>Especialidad</label>
		<select id="especialidad" name="especialidad" id="combo_especialidad">
			<option value="0">Ninguno</option>
		</select>
	</div>
</form>
<div class="four columns">
	<a class="mylinkleft" onclick="segmentar_clientes_por_criterios();">Segmentar y Añadir</a>
	<br/>
	<a class="mylinkleft" onclick="mostrar_busqueda_ind_cliente();">Agregar Cliente Individual</a>
</div>

<form class="sixteen columns" id="clientes" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
	<table>
		<?php
			$criterios_segmentacion = $_SESSION['criterios'];
			for ($i=0; $i < count($criterios_segmentacion); $i++) 
			{ 
		?>
			<tr>
				<td><?php echo $criterios_segmentacion[$i]; ?></td>
			</tr>
		<?
			}
		?>
	</table>
	<center><div class="estilo_tabla">
	<table id="lista_clientes">
		<tr>
			<th></th>
			<th>CLIENTES EN LISTA</th>
			<th>SELEC</th>
		</tr>
	<?php
		$clientes_seleccionados = $_SESSION['segmentados'];
		
		for ($i=0; $i < count($clientes_seleccionados); $i++) 
		{ 
	?>
		<tr>
			<td><?php echo ($i+1); ?></td>
			<td><?php echo strtoupper($clientes_seleccionados[$i]['trato_cliente'] . ' ' . $clientes_seleccionados[$i]['nombre_cliente'] . ' ' . $clientes_seleccionados[$i]['apellido_cliente']); ?></td>
			<td>
				<input type="checkbox" value="<?php echo $clientes_seleccionados[$i]['idclientes']; ?>" name="chk_cliente_<?php echo $i; ?>" />
				<input type="hidden" value="<?php echo $clientes_seleccionados[$i]['idclientes']; ?>" name="cliente_enviar_<?php echo $i; ?>" />
			</td>
		</tr>
	<?php
		}
	?>
	</table>
	<hr/>
	<input type="checkbox" id="todos_clientes" name="todos_clientes" onchange="listado();" />Enviar a Todos los Clientes
	<hr/>
	<input type="submit" value="Quitar Seleccionados" name="quitar_clientes" />
	<input type="submit" value="Reiniciar Segmentación" name="new_seg" />
	<input type="submit" value="Enviar Mail a Listado" name="enviar" />
	<input type="hidden" value="<?php echo count($clientes_seleccionados); ?>" name="cant_select">
	</div></center>
</form>

<script type="text/javascript">
	function listado()
	{
		if($('#todos_clientes').is(':checked'))
		{
			$('#lista_clientes').hide();	
		}
		else
		{
			$('#lista_clientes').show();
		}
	}
</script>

<?php include('footer.php'); ?>

