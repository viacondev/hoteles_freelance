<?php
	ini_set('memory_limit', '-1');
	set_time_limit(0);
	include 'header.php';
	ini_set('display_errors', 1);
	include('../entidad/tipo_paquete.php');
	include('../entidad/temporada.php');
	include('../entidad/publicacion.php');
	include('../entidad/paquete.php');
	include('../entidad/destino.php');
	include('../control/c_buscar_paquete.php');
	
	$c_buscar_paquete = new c_buscar_paquete;	

	if(isset($_GET['i']))
	{
		$_SESSION['paquetes_segmentados'] 	= array();
		$_SESSION['palabras_clave'] = $c_buscar_paquete->cargar_palabras_clave();
	}

	$array_palabras = $_SESSION['palabras_clave'];

	/*
	* SE VERIFICA SI SE NECESITA VACIAR LOS CLIENTES Y CRITERIOS
	*/
	if(isset($_POST['new_seg']))
	{
		$_SESSION['paquetes_segmentados'] 	= array();
	}

	/*
	* VERIFICAMOS SI SE NECESITA QUITAR ALGUN CLIENTE
	*/
	if(isset($_POST['quitar_paquetes']))
	{
		$paquetes_seleccionados 	= $_SESSION['paquetes_segmentados'];
		$cant_paq = count($paquetes_seleccionados);
		for ($i=0; $i < $cant_paq; $i++) 
		{ 
			if(isset($_POST['chk_paquete_' . $i]))
			{
				unset($paquetes_seleccionados[$i]);
			}
		}
		$paquetes_seleccionados = array_values($paquetes_seleccionados);
		$_SESSION['paquetes_segmentados'] = $paquetes_seleccionados;
	}

	/*
	* VERIFICAMOS SI SE NECESITA AGREGAR ALGUN PAQUETE
	*/
	if(isset($_POST['agregar_paquetes']))
	{
		$paquetes_seleccionados 	= $_SESSION['paquetes_segmentados'];
		$paquetes_busqueda 			= $_SESSION['paquetes_temporal'];
		$cant_paq = count($paquetes_busqueda);
		for ($i=0; $i < $cant_paq; $i++) 
		{ 
			if(isset($_POST['chk_paquete_add_' . $i]))
			{
				$paquetes_seleccionados[] = $paquetes_busqueda[$i];
			}
		}
		$paquetes_seleccionados 			= array_values($paquetes_seleccionados);
		$_SESSION['paquetes_segmentados'] 	= $paquetes_seleccionados;
		$_SESSION['paquetes_temporal'] 		= array();
	}

?>
<div class="three columns" style="color:#FF3300;">
	<label>1. Elegir Paquetes</label>
</div>
<div class="one columns">>></div>
<div class="three columns" style="color:#BBBBBB;">
	<label>2. Ver Diseño</label>
</div>
<div class="one columns">>></div>
<div class="three columns" style="color:#BBBBBB;">
	<label>3. Segmentar Clientes</label>
</div>
<div class="one columns">>></div>
<div class="three columns" style="color:#BBBBBB;">
	<label>4. Enviar</label>
</div>

<hr/>

<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" id="form_segmentar" name="form_segmentar">
	<h4>BUSCAR PAQUETE</h4>
	<div class="fifteen columns">
    	<input type="text" name="busqueda" id="busqueda" value="<?php echo $_POST['busqueda']; ?>" style="width:90%; padding:5px;" placeholder="Introduzca Paquete, Temporada o Destino" />
    </div>
    <div class="four columns">
    	<input type="submit" value="Buscar" />
    </div>
</form>
<hr/>

<?php
if(isset($_POST['busqueda']))
{
	$mis_paquetes = $c_buscar_paquete->buscar_paquetes_por_palabra_clave($_POST['busqueda'], 1);
	$_SESSION['paquetes_temporal'] = $mis_paquetes;
?>
	<form class="sixteen columns" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
		<center><div class="estilo_tabla">
		<table>
			<tr>
				<th>TITULO</th>
				<th>PRECIO</th>
				<th>NOCHES</th>
				<th></th>
			</tr>
		<?php
			for ($i=0; $i < count($mis_paquetes); $i++) 
			{ 
				$mi_paquete = $mis_paquetes[$i];
		?>
				<tr>
					<td><?php echo strtoupper($mi_paquete['titulo']); ?></td>
					<td><?php echo 'DESDE ' . $mi_paquete['precio_desde'] . ' USD'; ?></td>
					<td><?php echo $mi_paquete['fecha_salida']; ?></td>
					<td>
						<input type="checkbox" value="<?php echo $mi_paquete['idpublicacion']; ?>" name="chk_paquete_add_<?php echo $i; ?>" />
					</td>
				</tr>
		<?php
			}
		?>
		</table>
		</div>
		<input type="submit" value="Añadir Seleccionados" name="agregar_paquetes" />
		</center>
	</form>
<?php
}
?>

<form class="sixteen columns" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
	<h4>Paquetes Seleccionados</h4>
	<center><div class="estilo_tabla">
	<table>
		<tr>
			<th></th>
			<th>TITULO</th>
			<th>PRECIO</th>
			<th>NOCHES</th>
			<th></th>
		</tr>
	<?php
		$paquetes_seleccionados = $_SESSION['paquetes_segmentados'];
		
		for ($i=0; $i < count($paquetes_seleccionados); $i++) 
		{ 
			$paquete_seleccionado = $paquetes_seleccionados[$i];
	?>
		<tr>
			<td><?php echo ($i+1); ?></td>
			<td><?php echo strtoupper($paquete_seleccionado['titulo']); ?></td>
			<td><?php echo 'DESDE ' . $paquete_seleccionado['precio_desde'] . ' USD'; ?></td>
			<td><?php echo $paquete_seleccionado['fecha_salida']; ?></td>
			<td>
				<input type="checkbox" value="<?php echo $paquete_seleccionado['idpublicacion']; ?>" name="chk_paquete_<?php echo $i; ?>" />
			</td>
		</tr>
	<?php
		}
	?>
	</table>
	<input type="submit" value="Quitar Seleccionados" name="quitar_paquetes" />
	<input type="submit" value="Limpiar Listado" name="new_seg" />
	</div></center><!--<?php echo "<pre>"; print_r($_SESSION['paquetes_segmentados']); echo "</pre>"; ?>-->
</form>

<form class="sixteen columns" method="post" action="ver_diseno_boletin.php">
	<center><input type="submit" value="Ir a Vista Previa" name="enviar" /></center>
</form>

<script type="text/javascript">
	$(function(){
        var autocompletar = new Array();
        <?php 
            for($p = 0;$p < count($array_palabras); $p++)
            { 
        ?>
                autocompletar.push('<?php echo strtoupper($array_palabras[$p]); ?>');
        <?php 
            } 
        ?>
         $("#busqueda").autocomplete({ 
           source: autocompletar,
           minLength: 3
        });

    });	
</script>

<?php include('footer.php'); ?>
