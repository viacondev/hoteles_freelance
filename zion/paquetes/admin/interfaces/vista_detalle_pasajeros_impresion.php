<div class="view_cotizacion hidden">
	<center>- * -</center>
	<br/>
	<center>
		<div style="background:#ffffff; width:21cm; border:1px solid #AAA; padding-bottom:15px;" class="myPrintArea" >
			<table style="width:100%;">
				<tr>
					<td style="border:1px solid #AAA;">
						<table style="width:21cm; font-family:Courier New;" class="sin-borde">
				            <tr>
				                <td>
				                    <img src="images\logo-min.png" />    
				                </td>
				                <td style="vertical-align:top; text-align:right;font-family:Courier New; line-height:1;">
				                    Calle Velasco Esq. Suarez de Figueroa<br/>
				                    Telf 591-3-363610<br/>
				                    Santa Cruz - Bolivia<br/>
				                </td>
				            </tr>
				        </table>
				    </td>
				</tr>
				<tr>
					<td style="border:1px solid #AAA;">
				        <table style="width:21cm; font-family:Courier New;" class="sin-borde encabezado">
				            <tr>
				                <td colspan="2" style="font-size: 20pt; color: #ff3300;font-weight: bold; text-align:center; padding: 0px 0px; font-family:Courier New;">
				                    <?php 
				                        if($publicacion['imagen_tercera'] != '' && $publicacion['tipo_publicacion'] == 'E')
				                        { 
				                    ?>
				                            <img src="<?php echo $ruta_imagen . $publicacion['imagen_tercera']; ?>" style="width:100%; margin-bottom:10px;" />
				                            <?php echo strtoupper($info_cotizacion["titulo"]); ?><br/>
				                    <?php
				                        }
				                        else
				                        {
				                        ?>
				                            <?php echo strtoupper($info_cotizacion["titulo"]); ?><br/>
				                        <?php
				                        }
				                    ?>
				                </td>
				            </tr>
				            <tr>
				                <td colspan="2" style="text-align:center;" >
				                    <strong style="color:#FF3300;font-family:Courier New;">CLIENTE : </strong>
				                    <strong style="font-family:Courier New;"><?php echo strtoupper($info_cotizacion["nombre_cliente"] . ' ' . $info_cotizacion["apellido_cliente"]); ?></strong>
				                </td>
				            </tr>
				            <?php
								if(count($solicitantes) > 0 )
								{
								?>
									<tr>
						                <td colspan="2" style="text-align:center;" >
						                    <strong style="color:#FF3300;font-family:Courier New;">SOLICITANTE : </strong>
						                    <?php 
												for($i = 0; $i < count($solicitantes); $i++)
												{
													$nombre 		= $solicitantes[$i]["nombre_cliente"] . ' ' . $solicitantes[$i]["apellido_cliente"];
													$idclientes 	= $solicitantes[$i]["idclientes"];
												?>
													<strong style="font-family:Courier New;"><?php echo strtoupper($nombre); ?></strong>
												<?php
												}
											?>
						                    
						                </td>
						            </tr>
								<?php
								}
							?>
				        </table>

				        <!-- RESUMEN DE COSTOS -->
				        <table style="width:21cm; font-family:Courier New;" class="costos_resumido" >
				            <tr>
				                <td colspan="2" style="padding-top:10px;">
				                    <font style="font-family:Courier New; font-size:14pt; font-weight:bold; color:#ff3300; margin-left:2%;">RESUMEN DE COSTOS POR PASAJERO</font>
				                </td>
				            </tr>
				            <tr>
				            	<td colspan="2">
				            		<table style="width:90%; font-size:8pt; margin-left:2%;" class="tabla-precios">
										<tr style="background-color:#10689b; color:#FFFFFF;">
											<th style="font-family:Courier New;" rowspan="2">NRO</th>
											<th style="font-family:Courier New;" rowspan="2">PASAJERO</th>
											<th style="font-family:Courier New;" colspan="5">COSTOS EN $US</th>
										</tr>
										<tr style="background-color:#10689b; color:#FFFFFF;">
											<th style="font-family:Courier New; line-height:1;">BOLETO<br/>AEREO</th>
											<th style="font-family:Courier New; line-height:1;">PAQUETE<br/>TERRESTRE</th>
											<th style="font-family:Courier New; line-height:1;">SUBTOTAL<br/>PAQUETE</th>
											<th style="font-family:Courier New; line-height:1;">INSCRIPCION</th>
											<th style="font-family:Courier New; line-height:1;">TOTAL<br/>PAX</th>
										</tr>
										<?php
											$total_costos 		= 0;

											$total_costos_paq_terrestre = 0;
											$total_costos_aereo 		= 0;
											$total_costos_inscripciones = 0;

											$subtotal_costos 	= 0;

											$subtotal_costos_paq_terrestre 	= 0;
											$subtotal_costos_aereo 			= 0;
											$subtotal_costos_inscripciones 	= 0;

											$servicios_x_pax 	= array();
											$ciudad_actual 		= '';

											for ($i=0; $i < count($a_paxs); $i++) 
											{ 
												$pax 	= $a_paxs[$i];
												$idpax 	= $pax['idpax'];

												$costo_hoteles 			= c_mostrar_cotizacion::obtener_detalle_hotel_por_pax($idpax);
												$costo_servicios 		= c_mostrar_cotizacion::obtener_detalle_servicios_por_pax($idpax);
												$costo_boletos 			= c_mostrar_cotizacion::obtener_detalle_boletos_por_pax($idpax);
												$costo_inscripciones 	= c_mostrar_cotizacion::obtener_detalle_inscripciones_por_pax($idpax);

												if($ciudad_actual != $pax['idciudad_origen_paquete'] || $ciudad_actual == '')
												{
												?>
													<tr>
														<td colspan="7" style="text-align:center; background-color: #f3dbdb;">
															<strong style="font-size: 10pt; color: #10689b; font-family:Courier New;">SALE DESDE <?php echo strtoupper($c_mostrar_cotizacion_paquete->obtener_aeropuerto($pax['ciudad'])); ?></strong>
														</td>
													</tr>
												<?php
													$ciudad_actual = $pax['idciudad_origen_paquete'];
												}

												$servicios_x_pax[$i] = array('Hotel' => $costo_hoteles, 'Servicios' => $costo_servicios, 'Boletos' => $costo_boletos, 'Inscripciones' => $costo_inscripciones);

												$total_costos_aereo 	+= $costo_boletos['precio_total_pax'];
												$subtotal_costos_aereo 	+= $costo_boletos['precio_total_pax'];

												$total_costos_paq_terrestre 	+= $costo_hoteles['precio_total_pax'] + $costo_servicios['precio_total_pax'];
												$subtotal_costos_paq_terrestre 	+= $costo_hoteles['precio_total_pax'] + $costo_servicios['precio_total_pax'];

												$total_costos_inscripciones 	+= $costo_inscripciones['precio_total_pax'];
												$subtotal_costos_inscripciones 	+= $costo_inscripciones['precio_total_pax'];

											?>
												<tr>
													<td style="font-family:Courier New;"><?php echo ($i+1); ?></td>
													<td style="font-family:Courier New;"><?php echo strtoupper($pax['nombre_cliente'] . ' ' . $pax['apellido_cliente']); ?></td>
													<td style="text-align:right; font-family:Courier New;">
														<?php echo $costo_boletos['precio_total_pax']; ?>
													</td>
													<td style="text-align:right; font-family:Courier New;">
														<?php echo $costo_hoteles['precio_total_pax'] + $costo_servicios['precio_total_pax']; ?>
													</td>
													<td style="text-align:right; font-family:Courier New; background-color: #b7f3b7;">
														<?php echo $costo_boletos['precio_total_pax'] + $costo_hoteles['precio_total_pax'] + $costo_servicios['precio_total_pax']; ?>
													</td>
													<td style="text-align:right; font-family:Courier New;">
														<?php echo $costo_inscripciones['precio_total_pax']; ?>
													</td>
													<td style="text-align:right; font-family:Courier New; background-color: #f3f3b7;">
														<?php 
															$costo_pax 		= $costo_hoteles['precio_total_pax'] + $costo_servicios['precio_total_pax'] + $costo_boletos['precio_total_pax'] + $costo_inscripciones['precio_total_pax']; 
															echo $costo_pax;
															$total_costos 	+= $costo_pax;
															$subtotal_costos+= $costo_pax;
														?>
													</td>
												</tr>
											<?php
												if($i + 1 > count($a_paxs) || $a_paxs[$i+1]['idciudad_origen_paquete'] != $ciudad_actual)
												{
												?>
													<tr style="background-color: #cadfd8;">
														<td colspan="2" style="font-family:Courier New; font-weight:bold;">
															TOTAL DESDE <?php echo strtoupper($c_mostrar_cotizacion_paquete->obtener_aeropuerto($pax['ciudad'])); ?>
														</td>
														<td style="font-family:Courier New; text-align:right; font-weight:bold;"><?php echo $subtotal_costos_aereo; ?></td>
														<td style="font-family:Courier New; text-align:right; font-weight:bold;"><?php echo $subtotal_costos_paq_terrestre; ?></td>
														<td style="font-family:Courier New; text-align:right; font-weight:bold;"><?php echo $subtotal_costos_paq_terrestre + $subtotal_costos_aereo; ?></td>
														<td style="font-family:Courier New; text-align:right; font-weight:bold;"><?php echo $subtotal_costos_inscripciones; ?></td>
														<td style="font-family:Courier New; text-align:right; font-weight:bold;"><?php echo $subtotal_costos; ?></td>
													</tr>
												<?php
													$subtotal_costos 				= 0;
													$subtotal_costos_paq_terrestre 	= 0;
													$subtotal_costos_aereo 			= 0;
													$subtotal_costos_inscripciones 	= 0;
												}
											}
										?>
										<tr style="border: 2px solid #555;">
											<td style=" color: #10689b; font-family:Courier New;" colspan="2">TOTAL COSTOS</td>
											<td style="text-align:right; color: #10689b; font-family:Courier New;"><?php echo $total_costos_aereo; ?></td>
											<td style="text-align:right; color: #10689b; font-family:Courier New;"><?php echo $total_costos_paq_terrestre; ?></td>
											<td style="text-align:right; color: #10689b; font-family:Courier New;"><?php echo $total_costos_paq_terrestre + $total_costos_aereo; ?></td>
											<td style="text-align:right; color: #10689b; font-family:Courier New;"><?php echo $total_costos_inscripciones; ?></td>
											<td style="text-align:right; color: #10689b; font-family:Courier New;"><?php echo $total_costos; ?></td>
										</tr>
									</table>
				            	</td>
				            </tr>
				        </table>

						<p/>

						<!-- DESCRIPCION DE SERVICIOS POR PASAJERO -->
						<table style="width:21cm; font-family:Courier New;" class="descripcion_servicios_por_pax" >
				            <tr>
				                <td colspan="2" style="padding-top:10px;">
				                    <font style="font-size:14pt; font-weight:bold; color:#ff3300; margin-left:2%; font-family:Courier New;">DESCRIPCION DE SERVICIOS POR PASAJERO</font>
				                </td>
				            </tr>
				            <tr>
				            	<td colspan="2" style="padding-left:2%">
				            		<?php
				            			$ciudad_actual 		= '';
										for ($i=0; $i < count($a_paxs); $i++) 
										{
											$pax = $a_paxs[$i];
											$servicios_boleto 		= $servicios_x_pax[$i]['Boletos'];
											$servicios_hotel 		= $servicios_x_pax[$i]['Hotel'];
											$servicios_otros 		= $servicios_x_pax[$i]['Servicios'];
											$servicios_inscripcion 	= $servicios_x_pax[$i]['Inscripciones'];

											if($ciudad_actual != $pax['idciudad_origen_paquete'] || $ciudad_actual == '')
											{
											?>
												<br/>
												<strong style="color:#10689b; margin:0px 0px 0px 10px; font-family:Courier New;">SALE DESDE <?php echo strtoupper($c_mostrar_cotizacion_paquete->obtener_aeropuerto($pax['ciudad'])); ?></strong>
												<br/>
											<?php
												$ciudad_actual = $pax['idciudad_origen_paquete'];
											}

										?>
											<strong style="font-family:Courier New; margin:0px 0px 0px 20px; font-size:10pt;"><?php echo strtoupper($pax['trato_cliente'] . ' ' . $pax['nombre_cliente'] . ' ' . $pax['apellido_cliente']); ?></strong>
											<ul class="lista_incluye" style="margin:0px 0px 0px 30px;" >
												<?php
													for ($j=0; array_key_exists($j, $servicios_boleto); $j++) 
													{
														$boleto = $servicios_boleto[$j]; 
													?>
														<li style="font-family:Courier New; color:#333;">
															<a style="font-family:Courier New; color:#333;"><strong style="font-family:Courier New;">BOLETO AEREO</strong> <?php echo strtoupper($boleto['ruta']); ?> CON <?php echo strtoupper($boleto['nombre_linea_aerea']); ?>.</a>
														</li>
													<?php
													}
												?>
												<?php
													for ($j=0; array_key_exists($j, $servicios_hotel); $j++) 
													{
														$hotel 		= $servicios_hotel[$j]; 
														$cant_noch 	= floor( (strtotime($hotel['fecha_out'] . ' 00:00:00') - strtotime($hotel['fecha_in'] . ' 00:00:00')) / 86400);
													?>
														<li style="font-family:Courier New; color:#333;">
															<a style="font-family:Courier New; color:#333;"><strong style="font-family:Courier New;"><?php echo $cant_noch; ?> NOCHES</strong> EN HOTEL <?php echo strtoupper($hotel['nombre_hotel']) . ' - ' . $hotel['categoria'] . '*'; ?> HABITACION <?php echo strtoupper($hotel['habitacion']); ?> EN <?php echo strtoupper($hotel['destino']); ?>. <strong style="font-family:Courier New;">IN:</strong><?php echo strtoupper(date('d/M/Y', strtotime($hotel['fecha_in'] . ' 00:00:00'))); ?> <strong style="font-family:Courier New;">OUT:</strong><?php echo strtoupper(date('d/M/Y', strtotime($hotel['fecha_out'] . ' 00:00:00'))); ?></a>
														</li>
													<?php
													}
												?>
												<?php
													for ($j=0; array_key_exists($j, $servicios_otros); $j++) 
													{
														$servicio 		= $servicios_otros[$j]; 
													?>
														<li style="font-family:Courier New; color:#333;">
															<a style="font-family:Courier New; color:#333;"><strong style="font-family:Courier New;"><?php echo strtoupper($servicio['nombre_servicio']); ?> </strong><?php echo strtoupper($servicio['descripcion']); ?>.</a>
														</li>
													<?php
													}
												?>
												<?php
													for ($j=0; array_key_exists($j, $servicios_inscripcion); $j++) 
													{
														$inscripcion 		= $servicios_inscripcion[$j]; 
													?>
														<li style="font-family:Courier New; color:#333;">
															<a style="font-family:Courier New; color:#333;"><strong style="font-family:Courier New;">INSCRIPCION</strong> AL EVENTO <?php echo strtoupper($inscripcion['nombre_evento'] . '(' . $inscripcion['sigla_evento'] . ')'); ?> CATEGORIA <?php echo strtoupper($inscripcion['categoria']); ?>.</a>
														</li>
													<?php
													}
												?>
											</ul>
											<br/>
										<?php
										} 
									?>
				            	</td>
				            </tr>
				        </table>

						<p/>

						<!-- BOLETO AEREO -->
						<table style="width:21cm !important; font-family:Courier New;" class="vista_aereo" >
				            <tr>
				                <td colspan="2" style="padding-top:10px;">
				                    <font style="font-size:14pt; font-weight:bold; color:#ff3300; margin-left:2%; font-family:Courier New;">BOLETO AEREO</font>
				                </td>
				            </tr>
							<?php
								for ($z=0; $z < count($ciudades); $z++) 
								{ 
									$ciudad 			= $ciudades[$z];
									$idciudad_origen 	= $ciudad['idciudad_origen_paquete'];
								?>
						            <tr class="is_real">
						            	<!-- BOLETOS AEREOS TARIFAS REALES -->
						            	<td colspan="2" style="padding-left:2%">
						            		<?php
												$opciones_aereo = $c_mostrar_cotizacion_paquete->obtener_opcion_boletos_x_ciudad($idciudad_origen);
												if(count($opciones_aereo) > 0)
												{
												?>
													<strong style="color:#10689b; margin-left:10px; font-family:Courier New;">SALE DESDE <?php echo $c_mostrar_cotizacion_paquete->obtener_aeropuerto($ciudad['ciudad']); ?></strong>
													<br/>
													<?php
														for ($i=0; $i < count($opciones_aereo); $i++) 
														{ 
															$opcion_aereo 	= $opciones_aereo[$i];
															$idopaereo 		= $opcion_aereo['idopcion_boleto'];
														?>
															<br/>
															<?php
																$boletos = $c_mostrar_cotizacion_paquete->obtener_boletos_x_opcion_aereo_real_cotizacion($idopaereo, 1);
																for ($j=0; $j < count($boletos); $j++) 
																{ 
																	$boleto_cotizado 	= $boletos[$j];
																	$idboleto 			= $boleto_cotizado['idboleto_cotizado'];
																?>
																	<div class="resprint_<?php echo $boleto_cotizado['codigo_reserva']; ?>" style="width:100%;">
																	</div>
																<?php
																}
															?>
														<?php
														}
													?>
													<br/>
												<?php
												}
											?>
						            	</td>
						            </tr>
						            <tr class="is_real hidden">
						            	<!-- BOLETOS AEREOS TARIFAS REALES -->
						            	<td colspan="2" style="padding-left:2%">
						            		<?php
												$opciones_aereo = $c_mostrar_cotizacion_paquete->obtener_opcion_boletos_x_ciudad($idciudad_origen);
												if(count($opciones_aereo) > 0)
												{
												?>
													<strong style="color:#10689b; margin-left:10px; font-family:Courier New;">SALE DESDE <?php echo $c_mostrar_cotizacion_paquete->obtener_aeropuerto($ciudad['ciudad']); ?></strong>
													<br/>
													<?php
														for ($i=0; $i < count($opciones_aereo); $i++) 
														{ 
															$opcion_aereo 	= $opciones_aereo[$i];
															$idopaereo 		= $opcion_aereo['idopcion_boleto'];
														?>
															<br/>
															<?php
																$boletos = $c_mostrar_cotizacion_paquete->obtener_boletos_x_opcion_aereo_real_cotizacion($idopaereo, 0);
																for ($j=0; $j < count($boletos); $j++) 
																{ 
																	$boleto_cotizado 	= $boletos[$j];
																	$idboleto 			= $boleto_cotizado['idboleto_cotizado'];
																?>
																	<div class="resprint_<?php echo $boleto_cotizado['codigo_reserva']; ?>" style="width:100%;">
																	</div>
																<?php
																}
															?>
														<?php
														}
													?>
													<br/>
												<?php
												}
											?>
						            	</td>
						            </tr>
								<?php
								}
							?>
						</table>

						<p/>

						<!-- HOTEL Y OTROS SERVICIOS -->
						<table style="width:21cm !important; font-family:Courier New;" class="vista_hoteles" >
				            <tr>
				                <td colspan="2" style="padding-top:10px;">
				                    <font style="font-size:14pt; font-weight:bold; color:#ff3300; margin-left:2%; font-family:Courier New;">PAQUETE TERRESTRE</font>
				                </td>
				                <tr class="is_real">
					            	<td colspan="2" style="padding-left:2%">
						                <table style="width:95%; font-size:8pt; border:1px solid #AAA;" class="tabla-precios">
											<tr style="background-color:#10689b; color:#FFFFFF;">
												<th style="font-family:Courier New;">HOTEL</th>
												<th style="font-family:Courier New;">IN / OUT</th>
												<!--<th>OUT</th>-->
												<th style="font-family:Courier New; line-height:1;">NOCHES</th>
												<th class="col_sgl hidden" style="font-family:Courier New; line-height:1;">SIMPLE</th>
												<th class="col_dbl hidden" style="font-family:Courier New; line-height:1;">DOBLE</th>
												<th class="col_tpl hidden" style="font-family:Courier New; line-height:1;">TRIPLE</th>
												<th class="col_cpl hidden" style="font-family:Courier New; line-height:1;">CUADRU</th>
												<th class="col_cnn hidden" style="font-family:Courier New; line-height:1;">MENOR</th>
												<th class="col_inf hidden" style="font-family:Courier New; line-height:1;">INFANTE</th>
												<th style="font-family:Courier New; line-height:1;">ALIMENTACION</th>
												<th style="font-family:Courier New; line-height:1;">OBS</th>
												<th style="font-family:Courier New; line-height:1;" class="col_paxs">HAB</th>
												<th style="width:250px; font-family:Courier New; line-height:1;" class="col_paxs" >PASAJERO</th>
												<th class="col_paxs" style="font-family:Courier New; line-height:1;">ACOM</th>
											</tr>
											<?php
												$habitaciones = 0;
												for ($z=0; $z < count($ciudades); $z++) 
												{ 
													$ciudad 			= $ciudades[$z];
													$idciudad_origen 	= $ciudad['idciudad_origen_paquete'];

													/*
													* OTROS SERVICIOS 
													*/
													$otras_tarifas = $c_mostrar_cotizacion_paquete->obtener_otros_servicios_x_ciudad_real_cotizacion($idciudad_origen, 1);
													$serv_x_dest 	= array();
													$total_serv_adt = 0;
													$total_serv_cnn = 0;
													$total_serv_inf = 0;
													if(count($otras_tarifas) > 0)
													{
														for ($i=0; $i < count($otras_tarifas); $i++) 
														{ 
															$tarifa_otros 	= $otras_tarifas[$i];
															$idotra_tarifa 	= $tarifa_otros['idtarifa_otros'];

															$ciudad_serv 	= $tarifa_otros['ciudad'];
															
															$total_adt = $tarifa_otros['precio_adulto'];
															$total_cnn = $tarifa_otros['precio_menor'];
															$total_inf = $tarifa_otros['precio_infante'];

															if($tarifa_otros['increment_fee'] == 'P')
															{
																$fee 		= 1+($tarifa_otros['fee'] / 100);
																$total_adt 	*= $fee;
																$total_cnn 	*= $fee;
																$total_inf 	*= $fee;
															}
															else
															{
																$fee = $tarifa_otros['fee'];
																if($total_adt > 0)
																	$total_adt += $fee;
																else
																	$total_adt = 0;
																if($total_cnn > 0)
																	$total_cnn += $fee;
																else
																	$total_cnn = 0;
																if($total_inf > 0)
																	$total_inf += $fee;
																else
																	$total_inf = 0;
															}

															if($tarifa_otros['increment_factura'] == 'P')
															{
																$iva 		= 1+($tarifa_otros['factura'] / 100);
																$total_adt 	*= $iva;
																$total_cnn 	*= $iva;
																$total_inf 	*= $iva;
															}
															else
															{
																$iva = $tarifa_otros['factura'];
																if($total_adt > 0)
																	$total_adt += $iva;
																else
																	$total_adt = 0;
																if($total_cnn > 0)
																	$total_cnn += $iva;
																else
																	$total_cnn = 0;
																if($total_inf > 0)
																	$total_inf += $iva;
																else
																	$total_inf = 0;
															}

															if($tarifa_otros['moneda'] == 'B')
															{
																$tc 		= $tarifa_otros['tipo_cambio'];
																$total_adt /= $tc;
																$total_cnn /= $tc;
																$total_inf /= $tc;
															}

															$total_serv_adt += ceil($total_adt);
															$total_serv_cnn += ceil($total_cnn);
															$total_serv_inf += ceil($total_inf);

															if(!array_key_exists($ciudad_serv, $serv_x_dest))
															{
																$serv_x_dest[$ciudad_serv] = array();
																$serv_x_dest[$ciudad_serv]['adt'] = 0;
																$serv_x_dest[$ciudad_serv]['cnn'] = 0;
																$serv_x_dest[$ciudad_serv]['inf'] = 0;
															}

															$serv_x_dest[$ciudad_serv]['adt'] += ceil($total_adt); 
															$serv_x_dest[$ciudad_serv]['cnn'] += ceil($total_cnn);
															$serv_x_dest[$ciudad_serv]['inf'] += ceil($total_inf);

															$nombre_servicio = $c_mostrar_cotizacion_paquete->obtener_nombre_servicio($tarifa_otros['idotro_servicio']);
														}
													}

													/*
													* HOTELES
													*/
													$tarifas_por_destino =  $c_mostrar_cotizacion_paquete->obtener_destinos_de_hotel($idciudad_origen);
													if(count($tarifas_por_destino) > 0)
													{ 
														for ($i=0; $i < count($tarifas_por_destino); $i++) 
														{ 
															$tarifa_hotel 		= $tarifas_por_destino[$i];
															$idth 				= $tarifa_hotel['idtarifa_hotel'];
															$dest_hotel 		= $tarifa_hotel['destino'];
															$items_tarifa 		= $c_mostrar_cotizacion_paquete->obtener_hoteles_x_destino_real_cotizacion($idth, 1);

															$hide_columns 		= $c_mostrar_cotizacion_paquete->evaluar_precios_hotel($items_tarifa);
															$cant_col 			= count(explode('/', $hide_columns));

															$destino 			= strtoupper($c_mostrar_cotizacion_paquete->obtener_aeropuerto($tarifa_hotel['destino']));

															for ($j=0; $j < count($items_tarifa); $j++) 
															{ 
																$item_tarifa 	= $items_tarifa[$j];
																$iditem 		= $item_tarifa['iditem_hotel'];

																/*
																* SE CALCULA LOS PRECIOS
																*/

																$cant_noches 	= floor( (strtotime($item_tarifa['fecha_out'] . ' 00:00:00') - strtotime($item_tarifa['fecha_in'] . ' 00:00:00')) / 86400);
																	
																$total_sgl 	= $item_tarifa['precio_single'];
																$total_dbl 	= $item_tarifa['precio_doble'];
																$total_tpl 	= $item_tarifa['precio_triple'];
																$total_cpl 	= $item_tarifa['precio_cuadruple'];
																$total_cnn 	= $item_tarifa['precio_menor'];
																$total_inf 	= $item_tarifa['precio_infante'];

																if($item_tarifa['increment_fee'] == 'P')
																{
																	$fee 		= 1 + ($item_tarifa['fee']/100);
																	$total_sgl 	*= $fee;
																	$total_dbl 	*= $fee;
																	$total_tpl 	*= $fee;
																	$total_cpl 	*= $fee;
																	$total_cnn 	*= $fee;
																	$total_inf 	*= $fee;
																}
																else
																{
																	$fee 		= $item_tarifa['fee'];
																	if($item_tarifa['precio_single'] > 0)
																		$total_sgl 	+= $fee;
																	else
																		$total_sgl 	= 0;
																	if($item_tarifa['precio_doble'] > 0)
																		$total_dbl 	+= $fee;
																	else
																		$total_dbl 	= 0;
																	if($item_tarifa['precio_triple'] > 0)
																		$total_tpl 	+= $fee;
																	else
																		$total_tpl 	= 0;
																	if($item_tarifa['precio_cuadruple'] > 0)
																		$total_cpl 	+= $fee;
																	else
																		$total_cpl 	= 0;
																	if($item_tarifa['precio_menor'] > 0)
																		$total_cnn 	+= $fee;
																	else
																		$total_cnn 	= 0;
																	if($item_tarifa['precio_infante'] > 0)
																		$total_inf 	+= $fee;
																	else
																		$total_inf 	= 0;
																}

																if($item_tarifa['increment_factura'] == 'P')
																{
																	$iva 		= 1 + ($item_tarifa['factura']/100);
																	$total_sgl 	*= $iva;
																	$total_dbl 	*= $iva;
																	$total_tpl 	*= $iva;
																	$total_cpl 	*= $iva;
																	$total_cnn 	*= $iva;
																	$total_inf 	*= $iva;
																}
																else
																{
																	$iva 		= $item_tarifa['factura'];
																	if($item_tarifa['precio_single'] > 0)
																		$total_sgl 	+= $iva;
																	else
																		$total_sgl 	= 0;
																	if($item_tarifa['precio_doble'] > 0)
																		$total_dbl 	+= $iva;
																	else
																		$total_dbl 	= 0;
																	if($item_tarifa['precio_triple'] > 0)
																		$total_tpl 	+= $iva;
																	else
																		$total_tpl 	= 0;
																	if($item_tarifa['precio_cuadruple'] > 0)
																		$total_cpl 	+= $iva;
																	else
																		$total_cpl 	= 0;
																	if($item_tarifa['precio_menor'] > 0)
																		$total_cnn 	+= $iva;
																	else
																		$total_cnn 	= 0;
																	if($item_tarifa['precio_infante'] > 0)
																		$total_inf 	+= $iva;
																	else
																		$total_inf 	= 0;
																}

																if($item_tarifa['moneda'] == 'B')
																{
																	$tc 		= $item_tarifa['tipo_cambio'];
																	$total_sgl /= $tc;
																	$total_dbl /= $tc;
																	$total_tpl /= $tc;
																	$total_cpl /= $tc;
																	$total_cnn /= $tc;
																	$total_inf /= $tc;
																}

																$total_sgl 	= ceil($total_sgl);
																$total_dbl 	= ceil($total_dbl);
																$total_tpl 	= ceil($total_tpl);
																$total_cpl 	= ceil($total_cpl);
																$total_cnn 	= ceil($total_cnn);
																$total_inf 	= ceil($total_inf);

																/*
																* OBTIENE LOS PASAJEROS
																*/

																$paxs 		= $c_mostrar_cotizacion->obtener_paxs_segun_hotel($item_tarifa['iditem_hotel']);
																
																if(count($paxs) == 0)
																	$rowspan = 1;
																else
																	$rowspan = count($paxs);
															?>
																<tr>
																	<td rowspan="<?php echo $rowspan; ?>" style="line-height:1; padding:3px; font-family:Courier New;">
																		<strong style="font-family:Courier New;"><?php echo $item_tarifa['nombre_hotel'] . ' - ' . $item_tarifa['categoria'] . '*'; ?></strong>
																		<br/>
																		<?php echo $destino; ?>
																	</td>
																	<td rowspan="<?php echo $rowspan; ?>" style="line-height:1; padding:3px; font-family:Courier New;">
																		<strong style="font-family:Courier New;">IN:</strong><?php echo date('d/m/Y', strtotime($item_tarifa['fecha_in'])); ?>
																		<br/>
																		<strong style="font-family:Courier New;">OUT:</strong><?php echo date('d/m/Y', strtotime($item_tarifa['fecha_out'])); ?>
																	</td>
																	<td rowspan="<?php echo $rowspan; ?>" style="font-family:Courier New;" ><?php echo $cant_noches; ?></td>

																	<!--<td rowspan="<?php echo $rowspan; ?>"><?php echo date('d/m/Y', strtotime($item_tarifa['fecha_out'])); ?></td>-->
																	<td rowspan="<?php echo $rowspan; ?>" style="text-align:right; font-family:Courier New; line-height:1;" class="col_sgl hidden"><?php echo (($cant_noches * $total_sgl) + $serv_x_dest[$dest_hotel]['adt']) . ' $US'; ?></td>
																	<td rowspan="<?php echo $rowspan; ?>" style="text-align:right; font-family:Courier New; line-height:1;" class="col_dbl hidden"><?php echo (($cant_noches * $total_dbl) + $serv_x_dest[$dest_hotel]['adt']) . ' $US'; ?></td>
																	<td rowspan="<?php echo $rowspan; ?>" style="text-align:right; font-family:Courier New; line-height:1;" class="col_tpl hidden"><?php echo (($cant_noches * $total_tpl) + $serv_x_dest[$dest_hotel]['adt']) . ' $US'; ?></td>
																	<td rowspan="<?php echo $rowspan; ?>" style="text-align:right; font-family:Courier New; line-height:1;" class="col_cpl hidden"><?php echo (($cant_noches * $total_cpl) + $serv_x_dest[$dest_hotel]['adt']) . ' $US'; ?></td>
																	<td rowspan="<?php echo $rowspan; ?>" style="text-align:right; font-family:Courier New; line-height:1;" class="col_cnn hidden"><?php echo (($cant_noches * $total_cnn) + $serv_x_dest[$dest_hotel]['cnn']) . ' $US'; ?></td>
																	<td rowspan="<?php echo $rowspan; ?>" style="text-align:right; font-family:Courier New; line-height:1;" class="col_inf hidden"><?php echo (($cant_noches * $total_inf) + $serv_x_dest[$dest_hotel]['inf']) . ' $US'; ?></td>
																	<td rowspan="<?php echo $rowspan; ?>" style="text-align:right; font-family:Courier New; line-height:1;" ><?php echo strtoupper($item_tarifa['alimentacion']); ?></td>
																	<td rowspan="<?php echo $rowspan; ?>" style="text-align:right; font-family:Courier New; line-height:1;" ><?php echo strtoupper($item_tarifa['info_extra']); ?></td>
																		<?php
																			$nro_hab 	= '';
																			if(count($paxs) > 0)
																			{
																				for ($a=0; $a < count($paxs); $a++) 
																				{ 
																				
																					$pax 	= $paxs[$a];
																					$id  	= $pax['idpax'] . '_' . $item_tarifa['iditem_hotel'];
																					$nhab 	= $pax['habitacion'];
																					switch ($pax['tipo_precio']) 
																					{
																						case 1: $hab='SIMPLE'; break;
																						case 2: $hab='DOBLE'; break;
																						case 3: $hab='TRIPLE'; break;
																						case 4: $hab='CUADRUPLE'; break;
																						case 5: $hab='MENOR'; break;
																						case 6: $hab='INFANTE'; break;
																						default:$hab='N/N'; break;
																					}

																					// VERIFICAMOS LA HABITACION
																					if($nro_hab != $pax['habitacion']) 
																					{  
																						$nro_hab 	= $pax['habitacion'];
																						$sw 		= true;
																						$habitaciones++;
																					}
																					else
																						$sw = false;
																					
																					if($a!=0)
																					{
																					?>
																						<tr>
																					<?php
																					}
																					?>
																							<td style="font-family:Courier New;<?php if($sw) echo 'border-top:1px solid #AAA; border-bottom:none;";'; else echo 'border-top:none; border-bottom:none;'; ?>" class="col_paxs" ><?php if($sw) echo $habitaciones; ?></td>
																							<td style="font-family:Courier New;" class="col_paxs"><?php echo $pax['nombre_cliente'] . ' ' . $pax['apellido_cliente']; ?></td>
																							<td style="font-family:Courier New;" class="col_paxs"><?php echo $hab; ?></td>
																						</tr>
																				<?php
																				}
																			}
																			else
																			{
																			?>
																					<td style="font-family:Courier New;" class="col_paxs" colspan="3">NO INGRESADO</td>
																				</tr>
																			<?php
																			}
																		?>
															<?php
															}
														}
													}
												}
											?>
										</table>
									</td>
								</tr>
								<tr class="is_real hidden">
									<td colspan="2" style="padding-left:2%">
										<br/>
										<ul class="lista_incluye" style="margin:0px 0px 0px 20px;" >
											<li style="font-family:Courier New; color:#10689b; list-style:none; font-weight:bold;">
												INCLUYE :
											</li>
											<?php
												$serv_x_dest 	= array();
												for ($z=0; $z < count($ciudades); $z++) 
												{ 
													$ciudad 			= $ciudades[$z];
													$idciudad_origen 	= $ciudad['idciudad_origen_paquete'];

													/*
													* OTROS SERVICIOS 
													*/
													$otras_tarifas = $c_mostrar_cotizacion_paquete->obtener_otros_servicios_x_ciudad_real_cotizacion($idciudad_origen, 0);
													$total_serv_adt = 0;
													$total_serv_cnn = 0;
													$total_serv_inf = 0;
													if(count($otras_tarifas) > 0)
													{
														for ($i=0; $i < count($otras_tarifas); $i++) 
														{ 
															$tarifa_otros 	= $otras_tarifas[$i];
															$idotra_tarifa 	= $tarifa_otros['idtarifa_otros'];

															$ciudad_serv 	= $tarifa_otros['ciudad'];
															
															$total_adt = $tarifa_otros['precio_adulto'];
															$total_cnn = $tarifa_otros['precio_menor'];
															$total_inf = $tarifa_otros['precio_infante'];

															if($tarifa_otros['increment_fee'] == 'P')
															{
																$fee 		= 1+($tarifa_otros['fee'] / 100);
																$total_adt 	*= $fee;
																$total_cnn 	*= $fee;
																$total_inf 	*= $fee;
															}
															else
															{
																$fee = $tarifa_otros['fee'];
																if($total_adt > 0)
																	$total_adt += $fee;
																else
																	$total_adt = 0;
																if($total_cnn > 0)
																	$total_cnn += $fee;
																else
																	$total_cnn = 0;
																if($total_inf > 0)
																	$total_inf += $fee;
																else
																	$total_inf = 0;
															}

															if($tarifa_otros['increment_factura'] == 'P')
															{
																$iva 		= 1+($tarifa_otros['factura'] / 100);
																$total_adt 	*= $iva;
																$total_cnn 	*= $iva;
																$total_inf 	*= $iva;
															}
															else
															{
																$iva = $tarifa_otros['factura'];
																if($total_adt > 0)
																	$total_adt += $iva;
																else
																	$total_adt = 0;
																if($total_cnn > 0)
																	$total_cnn += $iva;
																else
																	$total_cnn = 0;
																if($total_inf > 0)
																	$total_inf += $iva;
																else
																	$total_inf = 0;
															}

															if($tarifa_otros['moneda'] == 'B')
															{
																$tc 		= $tarifa_otros['tipo_cambio'];
																$total_adt /= $tc;
																$total_cnn /= $tc;
																$total_inf /= $tc;
															}

															$total_serv_adt += ceil($total_adt);
															$total_serv_cnn += ceil($total_cnn);
															$total_serv_inf += ceil($total_inf);

															if(!array_key_exists($ciudad_serv, $serv_x_dest))
															{
																$serv_x_dest[$ciudad_serv] = array();
																$serv_x_dest[$ciudad_serv]['adt'] = 0;
																$serv_x_dest[$ciudad_serv]['cnn'] = 0;
																$serv_x_dest[$ciudad_serv]['inf'] = 0;
															}

															$serv_x_dest[$ciudad_serv]['adt'] += ceil($total_adt); 
															$serv_x_dest[$ciudad_serv]['cnn'] += ceil($total_cnn);
															$serv_x_dest[$ciudad_serv]['inf'] += ceil($total_inf);

															$nombre_servicio = $c_mostrar_cotizacion_paquete->obtener_nombre_servicio($tarifa_otros['idotro_servicio']);
															?>
																<li style="font-family:Courier New; color:#333;">
																	<a style="font-family:Courier New; color:#333;"><?php echo $nombre_servicio . ' ' . $tarifa_otros['descripcion'] . ' (' . $ciudad_serv . ')'; ?></a>
																</li>
															<?php
														}
													}
												}
											?>
										</ul>
										<br/>
										<table style="width:95%; font-size:8pt; border:1px solid #AAA;" class="tabla-precios">
											<tr style="background-color:#10689b; color:#FFFFFF;">
												<th style="font-family:Courier New;">HOTEL</th>
												<th style="font-family:Courier New;">IN / OUT</th>
												<!--<th>OUT</th>-->
												<th style="font-family:Courier New; line-height:1;">NOCHES</th>
												<th class="col_sgl hidden" style="font-family:Courier New; line-height:1;">SIMPLE</th>
												<th class="col_dbl hidden" style="font-family:Courier New; line-height:1;">DOBLE</th>
												<th class="col_tpl hidden" style="font-family:Courier New; line-height:1;">TRIPLE</th>
												<th class="col_cpl hidden" style="font-family:Courier New; line-height:1;">CUADRU</th>
												<th class="col_cnn hidden" style="font-family:Courier New; line-height:1;">MENOR</th>
												<th class="col_inf hidden" style="font-family:Courier New; line-height:1;">INFANTE</th>
												<th style="font-family:Courier New; line-height:1;">ALIMENTACION</th>
												<th style="font-family:Courier New; line-height:1;">OBS</th>
												<th style="font-family:Courier New; line-height:1;" class="col_paxs">HAB</th>
												<th style="width:250px; font-family:Courier New; line-height:1;" class="col_paxs" >PASAJERO</th>
												<th class="col_paxs" style="font-family:Courier New; line-height:1;">ACOM</th>
											</tr>
											<?php
												$habitaciones = 0;
												for ($z=0; $z < count($ciudades); $z++) 
												{ 
													$ciudad 			= $ciudades[$z];
													$idciudad_origen 	= $ciudad['idciudad_origen_paquete'];

													/*
													* HOTELES
													*/
													$tarifas_por_destino =  $c_mostrar_cotizacion_paquete->obtener_destinos_de_hotel($idciudad_origen);
													if(count($tarifas_por_destino) > 0)
													{ 
														for ($i=0; $i < count($tarifas_por_destino); $i++) 
														{ 
															$tarifa_hotel 		= $tarifas_por_destino[$i];
															$idth 				= $tarifa_hotel['idtarifa_hotel'];
															$dest_hotel 		= $tarifa_hotel['destino'];
															$items_tarifa 		= $c_mostrar_cotizacion_paquete->obtener_hoteles_x_destino_real_cotizacion($idth, 0);

															$hide_columns 		= $c_mostrar_cotizacion_paquete->evaluar_precios_hotel($items_tarifa);
															$cant_col 			= count(explode('/', $hide_columns));

															$destino 			= strtoupper($c_mostrar_cotizacion_paquete->obtener_aeropuerto($tarifa_hotel['destino']));

															for ($j=0; $j < count($items_tarifa); $j++) 
															{ 
																$item_tarifa 	= $items_tarifa[$j];
																$iditem 		= $item_tarifa['iditem_hotel'];

																/*
																* SE CALCULA LOS PRECIOS
																*/

																$cant_noches 	= floor( (strtotime($item_tarifa['fecha_out'] . ' 00:00:00') - strtotime($item_tarifa['fecha_in'] . ' 00:00:00')) / 86400);
																	
																$total_sgl 	= $item_tarifa['precio_single'];
																$total_dbl 	= $item_tarifa['precio_doble'];
																$total_tpl 	= $item_tarifa['precio_triple'];
																$total_cpl 	= $item_tarifa['precio_cuadruple'];
																$total_cnn 	= $item_tarifa['precio_menor'];
																$total_inf 	= $item_tarifa['precio_infante'];

																if($item_tarifa['increment_fee'] == 'P')
																{
																	$fee 		= 1 + ($item_tarifa['fee']/100);
																	$total_sgl 	*= $fee;
																	$total_dbl 	*= $fee;
																	$total_tpl 	*= $fee;
																	$total_cpl 	*= $fee;
																	$total_cnn 	*= $fee;
																	$total_inf 	*= $fee;
																}
																else
																{
																	$fee 		= $item_tarifa['fee'];
																	if($item_tarifa['precio_single'] > 0)
																		$total_sgl 	+= $fee;
																	else
																		$total_sgl 	= 0;
																	if($item_tarifa['precio_doble'] > 0)
																		$total_dbl 	+= $fee;
																	else
																		$total_dbl 	= 0;
																	if($item_tarifa['precio_triple'] > 0)
																		$total_tpl 	+= $fee;
																	else
																		$total_tpl 	= 0;
																	if($item_tarifa['precio_cuadruple'] > 0)
																		$total_cpl 	+= $fee;
																	else
																		$total_cpl 	= 0;
																	if($item_tarifa['precio_menor'] > 0)
																		$total_cnn 	+= $fee;
																	else
																		$total_cnn 	= 0;
																	if($item_tarifa['precio_infante'] > 0)
																		$total_inf 	+= $fee;
																	else
																		$total_inf 	= 0;
																}

																if($item_tarifa['increment_factura'] == 'P')
																{
																	$iva 		= 1 + ($item_tarifa['factura']/100);
																	$total_sgl 	*= $iva;
																	$total_dbl 	*= $iva;
																	$total_tpl 	*= $iva;
																	$total_cpl 	*= $iva;
																	$total_cnn 	*= $iva;
																	$total_inf 	*= $iva;
																}
																else
																{
																	$iva 		= $item_tarifa['factura'];
																	if($item_tarifa['precio_single'] > 0)
																		$total_sgl 	+= $iva;
																	else
																		$total_sgl 	= 0;
																	if($item_tarifa['precio_doble'] > 0)
																		$total_dbl 	+= $iva;
																	else
																		$total_dbl 	= 0;
																	if($item_tarifa['precio_triple'] > 0)
																		$total_tpl 	+= $iva;
																	else
																		$total_tpl 	= 0;
																	if($item_tarifa['precio_cuadruple'] > 0)
																		$total_cpl 	+= $iva;
																	else
																		$total_cpl 	= 0;
																	if($item_tarifa['precio_menor'] > 0)
																		$total_cnn 	+= $iva;
																	else
																		$total_cnn 	= 0;
																	if($item_tarifa['precio_infante'] > 0)
																		$total_inf 	+= $iva;
																	else
																		$total_inf 	= 0;
																}

																if($item_tarifa['moneda'] == 'B')
																{
																	$tc 		= $item_tarifa['tipo_cambio'];
																	$total_sgl /= $tc;
																	$total_dbl /= $tc;
																	$total_tpl /= $tc;
																	$total_cpl /= $tc;
																	$total_cnn /= $tc;
																	$total_inf /= $tc;
																}

																$total_sgl 	= ceil($total_sgl);
																$total_dbl 	= ceil($total_dbl);
																$total_tpl 	= ceil($total_tpl);
																$total_cpl 	= ceil($total_cpl);
																$total_cnn 	= ceil($total_cnn);
																$total_inf 	= ceil($total_inf);

																/*
																* OBTIENE LOS PASAJEROS
																*/

																$paxs 		= $c_mostrar_cotizacion->obtener_paxs_segun_hotel($item_tarifa['iditem_hotel']);
																
																if(count($paxs) == 0)
																	$rowspan = 1;
																else
																	$rowspan = count($paxs);
															?>
																<tr>
																	<td rowspan="<?php echo $rowspan; ?>" style="line-height:1; padding:3px; font-family:Courier New;">
																		<strong style="font-family:Courier New;"><?php echo $item_tarifa['nombre_hotel'] . ' - ' . $item_tarifa['categoria'] . '*'; ?></strong>
																		<br/>
																		<?php echo $destino; ?>
																	</td>
																	<td rowspan="<?php echo $rowspan; ?>" style="line-height:1; padding:3px; font-family:Courier New;">
																		<strong style="font-family:Courier New;">IN:</strong><?php echo date('d/m/Y', strtotime($item_tarifa['fecha_in'])); ?>
																		<br/>
																		<strong style="font-family:Courier New;">OUT:</strong><?php echo date('d/m/Y', strtotime($item_tarifa['fecha_out'])); ?>
																	</td>
																	<td rowspan="<?php echo $rowspan; ?>" style="font-family:Courier New;" ><?php echo $cant_noches; ?></td>

																	<!--<td rowspan="<?php echo $rowspan; ?>"><?php echo date('d/m/Y', strtotime($item_tarifa['fecha_out'])); ?></td>-->
																	<td rowspan="<?php echo $rowspan; ?>" style="text-align:right; font-family:Courier New; line-height:1;" class="col_sgl hidden"><?php echo (($cant_noches * $total_sgl) + $serv_x_dest[$dest_hotel]['adt']) . ' $US'; ?></td>
																	<td rowspan="<?php echo $rowspan; ?>" style="text-align:right; font-family:Courier New; line-height:1;" class="col_dbl hidden"><?php echo (($cant_noches * $total_dbl) + $serv_x_dest[$dest_hotel]['adt']) . ' $US'; ?></td>
																	<td rowspan="<?php echo $rowspan; ?>" style="text-align:right; font-family:Courier New; line-height:1;" class="col_tpl hidden"><?php echo (($cant_noches * $total_tpl) + $serv_x_dest[$dest_hotel]['adt']) . ' $US'; ?></td>
																	<td rowspan="<?php echo $rowspan; ?>" style="text-align:right; font-family:Courier New; line-height:1;" class="col_cpl hidden"><?php echo (($cant_noches * $total_cpl) + $serv_x_dest[$dest_hotel]['adt']) . ' $US'; ?></td>
																	<td rowspan="<?php echo $rowspan; ?>" style="text-align:right; font-family:Courier New; line-height:1;" class="col_cnn hidden"><?php echo (($cant_noches * $total_cnn) + $serv_x_dest[$dest_hotel]['cnn']) . ' $US'; ?></td>
																	<td rowspan="<?php echo $rowspan; ?>" style="text-align:right; font-family:Courier New; line-height:1;" class="col_inf hidden"><?php echo (($cant_noches * $total_inf) + $serv_x_dest[$dest_hotel]['inf']) . ' $US'; ?></td>
																	<td rowspan="<?php echo $rowspan; ?>" style="text-align:right; font-family:Courier New; line-height:1;" ><?php echo strtoupper($item_tarifa['alimentacion']); ?></td>
																	<td rowspan="<?php echo $rowspan; ?>" style="text-align:right; font-family:Courier New; line-height:1;" ><?php echo strtoupper($item_tarifa['info_extra']); ?></td>
																		<?php
																			$nro_hab 	= '';
																			if(count($paxs) > 0)
																			{
																				for ($a=0; $a < count($paxs); $a++) 
																				{ 
																				
																					$pax 	= $paxs[$a];
																					$id  	= $pax['idpax'] . '_' . $item_tarifa['iditem_hotel'];
																					$nhab 	= $pax['habitacion'];
																					switch ($pax['tipo_precio']) 
																					{
																						case 1: $hab='SIMPLE'; break;
																						case 2: $hab='DOBLE'; break;
																						case 3: $hab='TRIPLE'; break;
																						case 4: $hab='CUADRUPLE'; break;
																						case 5: $hab='MENOR'; break;
																						case 6: $hab='INFANTE'; break;
																						default:$hab='N/N'; break;
																					}

																					// VERIFICAMOS LA HABITACION
																					if($nro_hab != $pax['habitacion']) 
																					{  
																						$nro_hab 	= $pax['habitacion'];
																						$sw 		= true;
																						$habitaciones++;
																					}
																					else
																						$sw = false;
																					
																					if($a!=0)
																					{
																					?>
																						<tr>
																					<?php
																					}
																					?>
																							<td style="font-family:Courier New;<?php if($sw) echo 'border-top:1px solid #AAA; border-bottom:none;";'; else echo 'border-top:none; border-bottom:none;'; ?>" class="col_paxs" ><?php if($sw) echo $habitaciones; ?></td>
																							<td style="font-family:Courier New;" class="col_paxs"><?php echo $pax['nombre_cliente'] . ' ' . $pax['apellido_cliente']; ?></td>
																							<td style="font-family:Courier New;" class="col_paxs"><?php echo $hab; ?></td>
																						</tr>
																				<?php
																				}
																			}
																			else
																			{
																			?>
																					<td style="font-family:Courier New;" class="col_paxs" colspan="3">NO INGRESADO</td>
																				</tr>
																			<?php
																			}
																		?>
															<?php
															}
														}
													}
												}
											?>
										</table>
									</td>
								</tr>
				            </tr>
				        </table>
						

						<p/>

						<!-- INSCRIPCIONES -->
						
							<table style="width:21cm; font-family:Courier New;" class="inscripciones_a_evento" >
					            <tr>
					                <td colspan="2" style="padding-top:10px;">
					                    <font style="font-size:14pt; font-weight:bold; color:#ff3300; margin-left:2%; font-family:Courier New;">INSCRIPCION</font>
					                </td>
					            </tr>
					            <?php
									if(count($inscripciones) > 0)
									{
									?>
							            <tr class="inscripciones_pax">
							                <td colspan="2">
							                	<table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios">
													<tr style="background-color:#10689b; color:#FFFFFF;">
														<th style="font-family:Courier New;">CATEGORIA</th>
														<th style="font-family:Courier New;">EVENTO</th>
														<th style="font-family:Courier New;">PASAJERO</th>
														<th style="font-family:Courier New;">COSTO $US</th>
													</tr>
													<?php 
														for ($i=0; $i < count($inscripciones); $i++) 
														{ 
															$inscripcion 	= $inscripciones[$i];
															$idinscripcion 	= $inscripcion['idinscripcion_evento'];

															$paxs 			= $c_mostrar_cotizacion->obtener_paxs_segun_inscripcion($idinscripcion);//echo "<pre>"; print_r($paxs); echo "</pre>";

															if(count($paxs) == 0)
																$rowspan = 1;
															else
																$rowspan = count($paxs);
														?>
															<tr>
																<td rowspan="<?php echo $rowspan; ?>" style="font-family:Courier New; vertical-align: text-top;"><?php echo strtoupper($inscripcion['categoria']); ?></td>
																<td rowspan="<?php echo $rowspan; ?>" style="font-family:Courier New; vertical-align: text-top;"><?php echo strtoupper($inscripcion['nombre_evento']); ?></td>
																<?php
																	if(count($paxs) > 0)
																	{
																		for ($j=0; $j < count($paxs); $j++) 
																		{ 
																			if($j!=0)
																			{
																			?>
																				<tr>
																			<?php
																			}
																			?>
																					<td style="font-family:Courier New;" class="col_paxs_insc"><?php echo $paxs[$j]['nombre_cliente'] . ' ' . $paxs[$j]['apellido_cliente']; ?></td>
																					<td style="font-family:Courier New;" class="col_paxs_insc"><?php echo $total; ?></td>
																				</tr>
																		<?php
																		}
																	}
																	else
																	{
																	?>
																			<td style="font-family:Courier New;" class="col_paxs_insc" colspan="2">NO INGRESADO</td>
																		</tr>
																	<?php
																	}
																?>
														<?php
														}
													?>
												</table>
							                </td>
							            </tr>
					            	<?php
									}
								?>
					            <tr class="inscripcion_imagen" style="display:none;">
					            	<td colspan="2">
					            		<?php
											if($publicacion['imagen_primera'] != '')
											{
											?>
												<img src="<?php echo $ruta_imagen . $publicacion['imagen_primera']; ?>" style="width:90%; border-radius:10px; margin-left:3%;" />
											<?php
											}
										?>
					            	</td>
					            </tr>
					        </table>
					        <br/>
							
						<br/>
					</td>
				</tr>
			</table>
		</div>
	</center>
	<br/>
	<center>- * -</center>
</div>
