<?php
ini_set('memory_limit', '-1');
set_time_limit(0);
session_start();
include '../BD/controladoraBD.php';
include('../entidad/clientes.php');
include('../control/c_segmentar_clientes.php');

?>

<div style="float:left; width:200px; margin-left: 10px; margin-right: 10px;">
	&nbsp;
</div>
<div style="float:left; width:124px; margin-left: 10px; margin-right: 10px; color:#BBBBBB; display:inline; font-family:HelveticaNeue, Helvetica Neue, Helvetica, Arial, sans-serif;">
	<label style="font-weight:bold; font-size:13px;">1. Elegir Paquetes</label>
</div>
<div style="float:left; width:28px; margin-left: 10px; margin-right: 10px; display:inline; font-family:HelveticaNeue, Helvetica Neue, Helvetica, Arial, sans-serif;">>></div>
<div style="float:left; width:124px; margin-left: 10px; margin-right: 10px; color:#BBBBBB; display:inline; font-family:HelveticaNeue, Helvetica Neue, Helvetica, Arial, sans-serif;">
	<label style="font-weight:bold; font-size:13px;">2. Ver Dise&ntilde;o</label>
</div>
<div style="float:left; width:28px; margin-left: 10px; margin-right: 10px; display:inline; font-family:HelveticaNeue, Helvetica Neue, Helvetica, Arial, sans-serif;">>></div>
<div style="float:left; width:124px; margin-left: 10px; margin-right: 10px; color:#BBBBBB; display:inline; font-family:HelveticaNeue, Helvetica Neue, Helvetica, Arial, sans-serif;">
	<label style="font-weight:bold; font-size:13px;">3. Segmentar Clientes</label>
</div>
<div style="float:left; width:28px; margin-left: 10px; margin-right: 10px; display:inline; font-family:HelveticaNeue, Helvetica Neue, Helvetica, Arial, sans-serif;">>></div>
<div style="float:left; width:124px; margin-left: 10px; margin-right: 10px; color:#FF3300; display:inline; font-family:HelveticaNeue, Helvetica Neue, Helvetica, Arial, sans-serif;">
	<label style="font-weight:bold; font-size:13px;">4. Enviar</label>
</div>
<div style="float:left; width:200px; margin-left: 10px; margin-right: 10px;">
	&nbsp;
</div>
<div style="clear:both;"></div>
<hr/>
<center>
	ENVIO DE CORREOS LISTO PARA COMENZAR....<br/>
	<?php

	$c_segmentar_clientes = new c_segmentar_clientes;

	if(isset($_POST['enviar']))
	{
		if(isset($_POST['todos_clientes']))
		{
			$array_clientes = $c_segmentar_clientes->obtener_todos_clientes();
		}
		else
		{
			$array_clientes = $_SESSION['segmentados'];	
		}

		for($i=0; $i < count($array_clientes); $i++) 
		{ 
			$correos_cliente = $c_segmentar_clientes->obtener_correos_cliente($array_clientes[$i]['idclientes']);
			if($correos_cliente === false)
				$correos_cliente = array();

			$array_clientes[$i]['mails'] = $correos_cliente;
		}
	}
	$_SESSION['segmentados'] = $array_clientes;
	?>
	LOS CORREOS SE ENVIARAN DE 1000 EN 1000...<p/>
	<form method="post" action="enviar_mail_masivo_paquete.php" name="form_segmentar">
		<input type="text" value="" style="width:300px;" name="titulo" placeholder="Titulo del Envio"><br/>
		<textarea name="comentario" style="width:300px; height:100px;" placeholder="Comentario"></textarea><br/>
		<input type="submit" value="Comenzar Envio !" name="enviar" />
	</form>
</center>