<?php
session_start();
require('../control/c_login.php');
require('../entidad/clientes.php');
require('../entidad/usuarios.php');
require('../BD/controladoraBD.php');

$err = '';
if (isset($_POST['login'])) {
    $usuario = $_POST['usuario'];
    $password = $_POST['password'];
    $url = $_POST['return'];
    if (!c_login::iniciar_session($usuario, $password, $url)) {
        $err = 'Usuario o password inv&aacute;lido. Por favor vuelva a intentar.';
    }
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>BarryBolivia</title>

    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

    <!-- Skeleton -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <link rel="stylesheet" href="lib/Skeleton/stylesheets/base.css">
    <link rel="stylesheet" href="lib/Skeleton/stylesheets/skeleton.css">
    <link rel="stylesheet" href="lib/Skeleton/stylesheets/layout.css">

    <!-- Personal files -->
    <link href="styles.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div id="wrapper">
        <div id="login">
            <form method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>">
                <div>Usuario: <input type="text" class="input" name="usuario" /></div>
                <div>Password: <input type="password" class="input" name="password" /></div>
                <input name="return" type="hidden" value="<?php echo $_GET['return'] ?>">
                <input type="submit" class="btn btnuser" name="login" value="Entrar" />
            </form>
            <div id="login_err">
                <? echo $err ?>
            </div>
        </div>
    </div>
</body>
</html>