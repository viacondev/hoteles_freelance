<?php
	include('header.php');
	include('../entidad/tipo_paquete.php');
	include('../entidad/publicacion.php');
	include('../entidad/paquete.php');
	include('../entidad/imagen.php');
	include('../entidad/archivo_adjunto.php');
	include('../entidad/operadora.php');
	include('../entidad/destino.php');
	include('../entidad/usuarios_paquetes.php');
	include('../control/c_mostrar_cotizacion.php');
	include('../control/c_mostrar_cotizacion_paquete.php');
	include('../entidad/paquete_cotizado.php');
	include('../entidad/clientes.php');
	include('../entidad/telefono.php');
	include('../entidad/e_mails.php');
	//include('../entidad/seguimiento_cotizacion_paquete.php');
	include('../entidad/tarifa_hotel.php');
	include('../entidad/tarifa_otros.php');
	include('../entidad/boleto_cotizado.php');
	include('../entidad/segmento_publicado.php');
	include('../entidad/lineas_aereas.php');
	include('../entidad/aeropuerto.php');
	include('../entidad/opcion_hoteles.php');
	include('../control/c_pax_paquete.php');
	include("../entidad/pax_paquete.php");
	include('../entidad/hotel_cotizado.php');
	include('../entidad/otros_cotizado.php');
	include('../entidad/pqt_otro_servicio.php');
	include('../entidad/cotizacion.php');
	include('../entidad/inscripcion_evento.php');
	include('../entidad/pax.php');
	include('../entidad/cotizacion_has_pais.php');

	$c_mostrar_cotizacion_paquete 	= new c_mostrar_cotizacion_paquete;
	$c_mostrar_cotizacion 			= new c_mostrar_cotizacion;
	
	$idcotizacion 	= $_GET['cotizacion'];

	$cotizacion 	= $c_mostrar_cotizacion_paquete->obtener_info_cotizacion($idcotizacion);
	$cotizacion 	= $cotizacion[0];
	$idcotizacion 	= $cotizacion['idcotizacion'];

	$publicacion 	= $c_mostrar_cotizacion_paquete->obtener_publicacion_de_cotizacion($idcotizacion);
	$publicacion 	= $publicacion[0];
	$idpublicacion 	= $publicacion['idpublicacion'];
	$inscripcion_imagen = $publicacion['imagen_primera'];

	/*
	* EN ESTE ARRAY SE CARGARAN LOS PAISES QUE ESTAN EN EL PAQUETE DE ACUERDO A LOS DESTINOS QUE APAREZCAN
	*/
	$array_paises = array();

?>

<!-- DATOS DE ENCABEZADO -->

	<input type="hidden" id="idcotizacion" value="<?php echo $idcotizacion; ?>" />

	<div class="columns sixteen">
		<?php
			if($publicacion['imagen_tercera'] != '')
			{
			?>
				<img src="<?php echo $ruta_imagen . $publicacion['imagen_tercera']; ?>" style="width:100%; border-radius:10px;" />
				<div class="clear_mayor"></div>
			<?php
			}
		?>
	</div>

	<h4 style="text-align:center;"><?php echo $cotizacion['titulo']; ?></h4>

	<div class="columns sixteen" style="margin-left:8px; border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1; padding-top:10px; padding-bottom:10px;">

		<div class="columns two" style="font-weight:bold; color:#10689b;">
			COTIZACION
		</div>
		<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
		<div class="columns twelve">
			<span>
				<span><?php echo $cotizacion["idcotizacion"]; ?></span>
			</span>
		</div>

		<div class="clear_mayor"></div>

		<div class="columns two" style="font-weight:bold; color:#10689b;">
			CLIENTE
		</div>
		<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
		<div class="columns twelve">
			<span>
				<div onmouseover="$('.contact_cliente').show();" onmouseout="$('.contact_cliente').hide();">
					<?php echo strtoupper($cotizacion["nombre_cliente"] . ' ' . $cotizacion["apellido_cliente"]); ?>
					<img title="Informacion de Contacto" src="images/contactcard32.png" class="contact_cliente" style="cursor:pointer; width:15px; height:15px; display:none;" onclick="$('#info_contact_cliente').toggle();" />
					<a href="../../crm/interfaces/iu_mostrar_cliente.php?idclientes=<?php echo $cotizacion['idclientes']; ?>" target="_blank">
						<img title="Ver en CRM" src="images/user32.png"  class="contact_cliente" style="cursor:pointer; width:15px; height:15px; display:none;" />
					</a>
					<div id="info_contact_cliente" style="position:absolute; border:3px solid #AAA; background-color:#FFF; display:none;">
						<img src="images/cancel.png" style="width:15px; height:15px; margin-left:640px; margin-top:2px; position:absolute;" onclick="$('#info_contact_cliente').hide();" />
						<div class="four columns">
							<strong style="color:#10689b;">TELEFONOS</strong>
							<ul class="lista_descripcion">
								<?php
									$telefonos = $c_mostrar_cotizacion_paquete->obtener_telefonos_de_cliente($cotizacion['idclientes']);
									for ($i=0; $i < count($telefonos); $i++) 
									{ 
									?>
										<li>
											<a><?php echo $telefonos[$i]['numero_telefono'] . ' - ' . strtoupper($telefonos[$i]['tipo_numero_telefono']); ?></a>
										</li>
									<?php
									}
								?>
							</ul>
						</div>
						<div class="seven columns">
							<strong style="color:#10689b;">CORREOS</strong>
							<ul class="lista_descripcion">
								<?php
									$correos = $c_mostrar_cotizacion_paquete->obtener_correos_de_cliente($cotizacion['idclientes']);
									for ($i=0; $i < count($correos); $i++) 
									{ 
									?>
										<li>
											<a><?php echo $correos[$i]['e_mail'] . ' - ' . strtoupper($correos[$i]['e_mail_tipo']); ?></a>
										</li>
									<?php
									}
								?>
							</ul>
						</div>
					</div>
				</div>
			</span>
		</div>

		<div class="clear_mayor"></div>

		<div class="columns two" style="font-weight:bold; color:#10689b;">
			SOLICITANTE(S)
		</div>
		<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
		<div class="columns four" >
			<div class="columns twelve" style="margin-left:0px; margin-right:10px;">
				<?php 
					$solicitantes = $c_mostrar_cotizacion_paquete->obtener_solicitantes_de_cotizacion($idcotizacion);
					if(count($solicitantes) <=0 )
						echo "- No hay ninguno -";
					for($i = 0; $i < count($solicitantes); $i++)
					{
						$nombre 		= $solicitantes[$i]["nombre_cliente"] . ' ' . $solicitantes[$i]["apellido_cliente"];
						$idclientes 	= $solicitantes[$i]["idclientes"];
				?>
					<div onmouseover="$('.contact_cliente<?php echo $i; ?>').show();" onmouseout="$('.contact_cliente<?php echo $i; ?>').hide();">
						<?php echo strtoupper($nombre); ?>
						<img title="Informacion de Contacto" src="images/contactcard32.png" class="contact_cliente<?php echo $i; ?>" style="cursor:pointer; width:15px; height:15px; display:none;" onclick="$('#info_contact_cliente<?php echo $i; ?>').toggle();" />
						<a href="../../crm/interfaces/iu_mostrar_cliente.php?idclientes=<?php echo $idclientes; ?>" target="_blank">
							<img title="Ver en CRM" src="images/user32.png" class="contact_cliente<?php echo $i; ?>" style="cursor:pointer; width:15px; height:15px; display:none;" />
						</a>
						<div id="info_contact_cliente<?php echo $i; ?>" style="position:absolute; border:3px solid #AAA; background-color:#FFF; display:none;">
							<img src="images/cancel.png" style="width:15px; height:15px; margin-left:640px; margin-top:2px; position:absolute;" onclick="$('#info_contact_cliente<?php echo $i; ?>').hide();" />
							<div class="four columns">
								<strong style="color:#10689b;">TELEFONOS</strong>
								<ul class="lista_descripcion">
									<?php
										$telefonos = $c_mostrar_cotizacion_paquete->obtener_telefonos_de_cliente($idclientes);
										for ($i=0; $i < count($telefonos); $i++) 
										{ 
										?>
											<li>
												<a><?php echo $telefonos[$i]['numero_telefono'] . ' - ' . strtoupper($telefonos[$i]['tipo_numero_telefono']); ?></a>
											</li>
										<?php
										}
									?>
								</ul>
							</div>
							<div class="seven columns">
								<strong style="color:#10689b;">CORREOS</strong>
								<ul class="lista_descripcion">
									<?php
										$correos = $c_mostrar_cotizacion_paquete->obtener_correos_de_cliente($idclientes);
										for ($i=0; $i < count($correos); $i++) 
										{ 
										?>
											<li>
												<a><?php echo $correos[$i]['e_mail'] . ' - ' . strtoupper($correos[$i]['e_mail_tipo']); ?></a>
											</li>
										<?php
										}
									?>
								</ul>
							</div>
						</div>
					</div>
				<?php
					}
				?>
			</div>
		</div>

	</div>

	<div class="clear_mayor"></div>

	<div class="columns sixteen" style="margin-left:8px; border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1; padding-top:10px; padding-bottom:10px;">

		<div class="columns two" style="font-weight:bold; color:#10689b;">
			CREACION
		</div>
		<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
		<div class="columns four">
			<span>
				<span><?php echo date('d/m/Y H:i:s', strtotime($cotizacion["fecha_creacion"])); ?></span>
			</span>
		</div>

		<div class="columns two" style="font-weight:bold; color:#10689b;">
			USUARIO
		</div>
		<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
		<div class="columns four">
			<span>
				<span><?php echo strtoupper($cotizacion["nombre_usuario"]); ?></span>
			</span>
		</div>

	</div>

<!---->

<div class="clear_mayor"></div>

<!-- BOTONES PARA VISTAS -->

	<div class="eight columns">
		<input type="button" onclick="RevelarTabVistaCotizacion(0);" class="btn_vista ver_cotizacion" value="Cotizacion" style="float:left; background-color:#FF3300;" />
		<input type="button" onclick="RevelarTabVistaCotizacion(1);" class="btn_vista ver_pasajeros" value="Pasajeros" style="float:left; background-color:#AAAAAA;" />
		<input type="button" onclick="RevelarTabVistaCotizacion(2);" class="btn_vista ver_req_viaje" value="ReqDeViaje" style="float:left; background-color:#AAAAAA;" />
		<input type="button" onclick="RevelarTabVistaCotizacion(3);" class="btn_vista ver_req_x_pax" value="CheckList" style="float:left; background-color:#AAAAAA;" />
	</div>

	<?php
		$ciudades = $c_mostrar_cotizacion_paquete->obtener_ciudades_de_salida($idcotizacion);
	?>
	<div class="eight columns" id="botones_por_ciudad_origen">
	<?php
		for ($z=0; $z < count($ciudades); $z++) 
		{ 
		?>
			<input type="button" id="btn_ciudad_origen_<?php echo $z; ?>" class="btn_c_o" value="Desde <?php echo $ciudades[$z]['ciudad']; ?>" style="float:right;<?php if($z != 0) echo 'background-color:#AAAAAA;'; ?>" onclick="RevelarTabCiudad(<?php echo $z; ?>);" />
		<?php
		}
	?>&nbsp;
	</div>

<!---->

<!-- VISTA DE COSTOS Y SERVICIOS -->

	<div id="vista_del_paquete" class="tab_cotizacion">
		<?php
			for ($z=0; $z < count($ciudades); $z++) 
			{ 
				$ciudad 			= $ciudades[$z];
				$idciudad_origen 	= $ciudad['idciudad_origen_paquete'];
			?>

				<div style="border:1px solid #AAAAAA; border-radius:10px; background-color:#F1F1F1;<?php if($z > 0) echo ' display:none;'; ?>" class="sixteen columns tab_ciudad" id="tab_ciudad_<?php echo $z; ?>" >

					<div class="sixteen columns" style="font-weight:bold; color:#10689b; font-size:14pt; margin:10px; text-align:center;">SALE DESDE <?php echo $c_mostrar_cotizacion_paquete->obtener_aeropuerto($ciudad['ciudad']); ?></div>
					
					<div class="clear"></div>

					<!-- FECHA DE SALIDA -->
					<div class="one columns" style="font-weight:bold; color:#10689b; height:30px;">
						&nbsp;
					</div>
					<div class="two columns" style="font-weight:bold; color:#10689b; height:30px;">
						SALIDA :
					</div>
					<div class="columns four" style="height:30px; font-size:9pt; color: #444;" onmouseover="revelar_icono_edicion('fecha_salida_<?php echo $idciudad_origen; ?>');" onmouseout="ocultar_icono_edicion('fecha_salida_<?php echo $idciudad_origen; ?>');">
						<?php 
							$fecha_salida = strtotime($ciudad["fecha_salida"]);
							if($fecha_salida)
								$fecha_salida = date('d/m/Y', $fecha_salida);
							else
								$fecha_salida = $ciudad["fecha_salida"];
							if($fecha_salida != '')
								echo $fecha_salida;
							else
								echo 'NO INGRESADO';
						?>
					</div>

					<!-- FECHA DE RETORNO -->
					<div class="columns two" style="font-weight:bold; color:#10689b; height:30px;">
						RETORNO :
					</div>
					<div class="columns four" style="height:30px; font-size:9pt; color: #444;" onmouseover="revelar_icono_edicion('fecha_retorno_<?php echo $idciudad_origen; ?>');" onmouseout="ocultar_icono_edicion('fecha_retorno_<?php echo $idciudad_origen; ?>');">
						<?php 
							$fecha_retorno = strtotime($ciudad["fecha_retorno"]);
							if($fecha_retorno)
								$fecha_retorno = date('d/m/Y', $fecha_retorno);
							else
								$fecha_retorno = $ciudad["fecha_retorno"];
							if($fecha_retorno != '')
								echo $fecha_retorno;
							else
								echo 'NO INGRESADO';
						?>	
					</div>

					<div class="clear_mayor"></div>

					<!-- PAQUETE INCLUYE -->
					<?php
						if($ciudad['paquete_incluye'] != '')
						{
						?>
							<div class="columns fifteen">
								<div class="columns five" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">PAQUETE INCLUYE</div>
								<div class="clear"></div>
								<ul class="lista_descripcion">
									<?php
										$paquete_incluye = explode("\n", $ciudad['paquete_incluye']);
										for($i=0; $i<count($paquete_incluye); $i++)
										{
											if($paquete_incluye[$i] != "")
											{
									?>
												<li <?php if($paquete_incluye[$i][0] == '@') { $paquete_incluye[$i] = substr($paquete_incluye[$i], 1); ?> style="font-weight:bold; list-style:none; font-size:15pt;" <?php } ?> >
													<a><?php echo $paquete_incluye[$i]; ?></a>
												</li>
									<?php
											}
										}
									?>		
								</ul>
							</div>
						<?php
						}
					?>

					<div class="is_real0">

						<!-- HOTELES -->
						<?php
							$tarifas_por_destino =  $c_mostrar_cotizacion_paquete->obtener_destinos_de_hotel($idciudad_origen);
							if(count($tarifas_por_destino) > 0)
							{
							?>
								<div class="columns fifteen todo_precio solo_hotel" style="display:none;">
									<div class="columns fourteen" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">PRECIOS POR PERSONA EN DOLARES AMERICANOS</div>
									<div class="clear"></div>
									<div class="columns thirteen" style="font-weight:bold; color:#10689b; font-size:11pt; margin-left:2%;">HOTELES</div>
									<?php 
										for ($i=0; $i < count($tarifas_por_destino); $i++) 
										{ 
											$tarifa_hotel 		= $tarifas_por_destino[$i];
											$idth 				= $tarifa_hotel['idtarifa_hotel'];
											$items_tarifa 		= $c_mostrar_cotizacion_paquete->obtener_hoteles_x_destino_real_cotizacion($idth, 0);

											$hide_columns 		= $c_mostrar_cotizacion_paquete->evaluar_precios_hotel($items_tarifa);
											$cant_col 			= count(explode('/', $hide_columns));

											/*
											* AGREGA PAIS AL ARRAY SI HUBIESE
											*/
											$pais 							= $c_mostrar_cotizacion_paquete->obtener_pais(strtoupper($tarifa_hotel['destino']));
											$array_paises[$pais['idpais']] 	= $pais['nombre_pais'];
											/***/

									?>
											<table style="width:50%; margin-left:3%;">
												<tr>
													<td style="width:5%;">
														<strong>DESTINO :</strong>
													</td>
													<td style="width:20%;">
														<?php echo strtoupper($tarifa_hotel['destino']); ?>
													</td>
													<td>
													</td>
												</tr>
											</table>
											<table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios">
												<tr style="background-color:#10689b; color:#FFFFFF;">
													<th>HOTEL</th>
													<?php if(strpos($hide_columns, 'SGL')===false) { ?><th>SINGLE</th><?php } ?>
													<?php if(strpos($hide_columns, 'DBL')===false) { ?><th>DOBLE</th><?php } ?>
													<?php if(strpos($hide_columns, 'TPL')===false) { ?><th>TRIPLE</th><?php } ?>
													<?php if(strpos($hide_columns, 'CPL')===false) { ?><th>CUADRUPLE</th><?php } ?>
													<?php if(strpos($hide_columns, 'CNN')===false) { ?><th>MENOR</th><?php } ?>
													<?php if(strpos($hide_columns, 'INF')===false) { ?><th>INFANTE</th><?php } ?>
													<th>ALIMENTACION</th>
													<th>OBS</th>
												</tr>
									<?php
											for ($j=0; $j < count($items_tarifa); $j++) 
											{ 
												$item_tarifa 	= $items_tarifa[$j];
												$iditem 		= $item_tarifa['iditem_hotel'];

												$cant_noches 	= floor( (strtotime($item_tarifa['fecha_out'] . ' 00:00:00') - strtotime($item_tarifa['fecha_in'] . ' 00:00:00')) / 86400);
												
												$total_sgl 	= $item_tarifa['precio_single'] * $cant_noches;
												$total_dbl 	= $item_tarifa['precio_doble'] * $cant_noches;
												$total_tpl 	= $item_tarifa['precio_triple'] * $cant_noches;
												$total_cpl 	= $item_tarifa['precio_cuadruple'] * $cant_noches;
												$total_cnn 	= $item_tarifa['precio_menor'] * $cant_noches;
												$total_inf 	= $item_tarifa['precio_infante'] * $cant_noches;

												if($item_tarifa['increment_fee'] == 'P')
												{
													$fee 		= 1 + ($item_tarifa['fee']/100);
													$total_sgl 	*= $fee;
													$total_dbl 	*= $fee;
													$total_tpl 	*= $fee;
													$total_cpl 	*= $fee;
													$total_cnn 	*= $fee;
													$total_inf 	*= $fee;
												}
												else
												{
													$fee 		= $item_tarifa['fee'];
													if($item_tarifa['precio_single'] > 0)
														$total_sgl 	+= $fee;
													else
														$total_sgl 	= 0;
													if($item_tarifa['precio_doble'] > 0)
														$total_dbl 	+= $fee;
													else
														$total_dbl 	= 0;
													if($item_tarifa['precio_triple'] > 0)
														$total_tpl 	+= $fee;
													else
														$total_tpl 	= 0;
													if($item_tarifa['precio_cuadruple'] > 0)
														$total_cpl 	+= $fee;
													else
														$total_cpl 	= 0;
													if($item_tarifa['precio_menor'] > 0)
														$total_cnn 	+= $fee;
													else
														$total_cnn 	= 0;
													if($item_tarifa['precio_infante'] > 0)
														$total_inf 	+= $fee;
													else
														$total_inf 	= 0;
												}

												if($item_tarifa['increment_factura'] == 'P')
												{
													$iva 		= 1 + ($item_tarifa['factura']/100);
													$total_sgl 	*= $iva;
													$total_dbl 	*= $iva;
													$total_tpl 	*= $iva;
													$total_cpl 	*= $iva;
													$total_cnn 	*= $iva;
													$total_inf 	*= $iva;
												}
												else
												{
													$iva 		= $item_tarifa['factura'];
													if($item_tarifa['precio_single'] > 0)
														$total_sgl 	+= $iva;
													else
														$total_sgl 	= 0;
													if($item_tarifa['precio_doble'] > 0)
														$total_dbl 	+= $iva;
													else
														$total_dbl 	= 0;
													if($item_tarifa['precio_triple'] > 0)
														$total_tpl 	+= $iva;
													else
														$total_tpl 	= 0;
													if($item_tarifa['precio_cuadruple'] > 0)
														$total_cpl 	+= $iva;
													else
														$total_cpl 	= 0;
													if($item_tarifa['precio_menor'] > 0)
														$total_cnn 	+= $iva;
													else
														$total_cnn 	= 0;
													if($item_tarifa['precio_infante'] > 0)
														$total_inf 	+= $iva;
													else
														$total_inf 	= 0;
												}

												if($item_tarifa['moneda'] == 'B')
												{
													$tc 		= $item_tarifa['tipo_cambio'];
													$total_sgl /= $tc;
													$total_dbl /= $tc;
													$total_tpl /= $tc;
													$total_cpl /= $tc;
													$total_cnn /= $tc;
													$total_inf /= $tc;
												}
									?>
												<tr>
													<td style="line-height:1;">
														<?php echo $item_tarifa['nombre_hotel'] . ' - ' . $item_tarifa['categoria'] . '*'; ?><br/>
														<strong>IN:</strong><?php echo date('d/m/Y', strtotime($item_tarifa['fecha_in'])); ?><br/>
														<strong>OUT:</strong><?php echo date('d/m/Y', strtotime($item_tarifa['fecha_out'])); ?><br/>
													</td>
													<?php if(strpos($hide_columns, 'SGL')===false) { ?><td style="text-align:right;"><?php if($total_sgl>0) echo ceil($total_sgl); else echo '-'; ?></td><?php } ?>
													<?php if(strpos($hide_columns, 'DBL')===false) { ?><td style="text-align:right;"><?php if($total_dbl>0) echo ceil($total_dbl); else echo '-'; ?></td><?php } ?>
													<?php if(strpos($hide_columns, 'TPL')===false) { ?><td style="text-align:right;"><?php if($total_tpl>0) echo ceil($total_tpl); else echo '-'; ?></td><?php } ?>
													<?php if(strpos($hide_columns, 'CPL')===false) { ?><td style="text-align:right;"><?php if($total_cpl>0) echo ceil($total_cpl); else echo '-'; ?></td><?php } ?>
													<?php if(strpos($hide_columns, 'CNN')===false) { ?><td style="text-align:right;"><?php if($total_cnn>0) echo ceil($total_cnn); else echo '-'; ?></td><?php } ?>
													<?php if(strpos($hide_columns, 'INF')===false) { ?><td style="text-align:right;"><?php if($total_inf>0) echo ceil($total_inf); else echo '-'; ?></td><?php } ?>
													<td><?php echo strtoupper($item_tarifa['alimentacion']); ?></td>
													<td><?php echo strtoupper($item_tarifa['info_extra']); ?></td>
												</tr>
									<?php
											}
									?>
											</table>
									<?
										}				
									?>
								</div>

								<div class="clear_mayor todo_precio solo_hotel" style="display:none;"></div>
							<?php
							}
						?>

						<!-- OTROS SERVICIOS -->
						<?php
							$otras_tarifas = $c_mostrar_cotizacion_paquete->obtener_otros_servicios_x_ciudad_real_cotizacion($idciudad_origen, 0);
							$serv_x_dest 	= array();
							$total_serv_adt = 0;
							$total_serv_cnn = 0;
							$total_serv_inf = 0;
							if(count($otras_tarifas) > 0)
							{
							?>
								<div class="columns fifteen todo_precio solo_servicio" style="display:none;">
									<div class="columns thirteen" style="font-weight:bold; color:#10689b; font-size:11pt; margin-left:2%;">OTROS SERVICIOS</div>
									<div class="clear"></div>
									<table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios" >
										<tr style="background-color:#10689b; color:#FFFFFF;">
											<th rowspan="2">DETALLE</th>
											<th colspan="3">PRECIOS</th>
										</tr>
										<tr style="background-color:#10689b; color:#FFFFFF;">
											<th>ADULTO</th>
											<th>MENOR</th>
											<th>INFANTE</th>
										</tr>
										<?php
											for ($i=0; $i < count($otras_tarifas); $i++) 
											{ 
												$tarifa_otros 	= $otras_tarifas[$i];
												$idotra_tarifa 	= $tarifa_otros['idtarifa_otros'];

												$ciudad_serv 	= $tarifa_otros['ciudad'];

												/*
												* AGREGA PAIS AL ARRAY SI HUBIESE
												*/
												$pais 							= $c_mostrar_cotizacion_paquete->obtener_pais(strtoupper($ciudad_serv));
												$array_paises[$pais['idpais']] 	= $pais['nombre_pais'];
												/***/
												
												$total_adt = $tarifa_otros['precio_adulto'];
												$total_cnn = $tarifa_otros['precio_menor'];
												$total_inf = $tarifa_otros['precio_infante'];

												if($tarifa_otros['increment_fee'] == 'P')
												{
													$fee 		= 1+($tarifa_otros['fee'] / 100);
													$total_adt 	*= $fee;
													$total_cnn 	*= $fee;
													$total_inf 	*= $fee;
												}
												else
												{
													$fee = $tarifa_otros['fee'];
													if($total_adt > 0)
														$total_adt += $fee;
													else
														$total_adt = 0;
													if($total_cnn > 0)
														$total_cnn += $fee;
													else
														$total_cnn = 0;
													if($total_inf > 0)
														$total_inf += $fee;
													else
														$total_inf = 0;
												}

												if($tarifa_otros['increment_factura'] == 'P')
												{
													$iva 		= 1+($tarifa_otros['factura'] / 100);
													$total_adt 	*= $iva;
													$total_cnn 	*= $iva;
													$total_inf 	*= $iva;
												}
												else
												{
													$iva = $tarifa_otros['factura'];
													if($total_adt > 0)
														$total_adt += $iva;
													else
														$total_adt = 0;
													if($total_cnn > 0)
														$total_cnn += $iva;
													else
														$total_cnn = 0;
													if($total_inf > 0)
														$total_inf += $iva;
													else
														$total_inf = 0;
												}

												if($tarifa_otros['moneda'] == 'B')
												{
													$tc 		= $tarifa_otros['tipo_cambio'];
													$total_adt /= $tc;
													$total_cnn /= $tc;
													$total_inf /= $tc;
												}

												$total_serv_adt += ceil($total_adt);
												$total_serv_cnn += ceil($total_cnn);
												$total_serv_inf += ceil($total_inf);

												if(!array_key_exists($ciudad_serv, $serv_x_dest))
												{
													$serv_x_dest[$ciudad_serv] = array();
													$serv_x_dest[$ciudad_serv]['adt'] = 0;
													$serv_x_dest[$ciudad_serv]['cnn'] = 0;
													$serv_x_dest[$ciudad_serv]['inf'] = 0;
												}

												$serv_x_dest[$ciudad_serv]['adt'] += ceil($total_adt); 
												$serv_x_dest[$ciudad_serv]['cnn'] += ceil($total_cnn);
												$serv_x_dest[$ciudad_serv]['inf'] += ceil($total_inf);

												$nombre_servicio = $c_mostrar_cotizacion_paquete->obtener_nombre_servicio($tarifa_otros['idotro_servicio']);
										?>
										<tr>
											<td><?php echo strtoupper($nombre_servicio . ' ' . $tarifa_otros['descripcion']); ?></td>
											<td style="text-align:right;">
												<?php if($total_adt>0) echo ceil($total_adt); else echo '-'; ?>
											</td>
											<td style="text-align:right;">
												<?php if($total_cnn>0) echo ceil($total_cnn); else echo '-'; ?>
											</td>
											<td style="text-align:right;">
												<?php if($total_inf>0) echo ceil($total_inf); else echo '-'; ?>
											</td>
										</tr>
										<?php
											}
										?>
									</table>
								</div>

								<div class="clear_mayor todo_precio solo_servicio" style="display:none;"></div>
							<?php
							}
						?>

						<!-- MUESTRA HOTEL MAS SERVICIOS -->
						<?php
							if(count($tarifas_por_destino) > 0)
							{
							?>
								<div class="columns fifteen todo_precio hotel_mas_servicio" style="display:none;">
									<div class="columns fourteen" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">PRECIOS POR PERSONA EN DOLARES AMERICANOS</div>
									<div class="columns thirteen" style="font-weight:bold; color:#10689b; font-size:11pt; margin-left:2%;">PAQUETE TERRESTRE</div>
									<div class="clear"></div>
									<?php 
										for ($i=0; $i < count($tarifas_por_destino); $i++) 
										{ 
											$tarifa_hotel 		= $tarifas_por_destino[$i];
											$idth 				= $tarifa_hotel['idtarifa_hotel'];
											$dest_hotel 		= $tarifa_hotel['destino'];
											$items_tarifa 		= $c_mostrar_cotizacion_paquete->obtener_hoteles_x_destino_real_cotizacion($idth, 0);

											$hide_columns 		= $c_mostrar_cotizacion_paquete->evaluar_precios_hotel($items_tarifa);
											$cant_col 			= count(explode('/', $hide_columns));
										?>
											<table style="width:50%; margin-left:3%;">
												<tr>
													<td style="width:5%;">
														<strong>DESTINO :</strong>
													</td>
													<td style="width:20%;">
														<?php echo strtoupper($tarifa_hotel['destino']); ?>
													</td>
													<td>
													</td>
												</tr>
											</table>
											<table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios">
												<tr style="background-color:#10689b; color:#FFFFFF;">
													<th>HOTEL</th>
													<?php if(strpos($hide_columns, 'SGL')===false) { ?><th>SINGLE</th><?php } ?>
													<?php if(strpos($hide_columns, 'DBL')===false) { ?><th>DOBLE</th><?php } ?>
													<?php if(strpos($hide_columns, 'TPL')===false) { ?><th>TRIPLE</th><?php } ?>
													<?php if(strpos($hide_columns, 'CPL')===false) { ?><th>CUADRUPLE</th><?php } ?>
													<?php if(strpos($hide_columns, 'CNN')===false) { ?><th>MENOR</th><?php } ?>
													<?php if(strpos($hide_columns, 'INF')===false) { ?><th>INFANTE</th><?php } ?>
													<th>ALIMENTACION</th>
													<th>OBS</th>
												</tr>
										<?php
											for ($j=0; $j < count($items_tarifa); $j++) 
											{ 
												$item_tarifa 	= $items_tarifa[$j];
												$iditem 		= $item_tarifa['iditem_hotel'];

												$cant_noches 	= floor( (strtotime($item_tarifa['fecha_out'] . ' 00:00:00') - strtotime($item_tarifa['fecha_in'] . ' 00:00:00')) / 86400);
												
												$total_sgl 	= $item_tarifa['precio_single'] * $cant_noches;
												$total_dbl 	= $item_tarifa['precio_doble'] * $cant_noches;
												$total_tpl 	= $item_tarifa['precio_triple'] * $cant_noches;
												$total_cpl 	= $item_tarifa['precio_cuadruple'] * $cant_noches;
												$total_cnn 	= $item_tarifa['precio_menor'] * $cant_noches;
												$total_inf 	= $item_tarifa['precio_infante'] * $cant_noches;

												if($item_tarifa['increment_fee'] == 'P')
												{
													$fee 		= 1 + ($item_tarifa['fee']/100);
													$total_sgl 	*= $fee;
													$total_dbl 	*= $fee;
													$total_tpl 	*= $fee;
													$total_cpl 	*= $fee;
													$total_cnn 	*= $fee;
													$total_inf 	*= $fee;
												}
												else
												{
													$fee 		= $item_tarifa['fee'];
													if($item_tarifa['precio_single'] > 0)
														$total_sgl 	+= $fee;
													else
														$total_sgl 	= 0;
													if($item_tarifa['precio_doble'] > 0)
														$total_dbl 	+= $fee;
													else
														$total_dbl 	= 0;
													if($item_tarifa['precio_triple'] > 0)
														$total_tpl 	+= $fee;
													else
														$total_tpl 	= 0;
													if($item_tarifa['precio_cuadruple'] > 0)
														$total_cpl 	+= $fee;
													else
														$total_cpl 	= 0;
													if($item_tarifa['precio_menor'] > 0)
														$total_cnn 	+= $fee;
													else
														$total_cnn 	= 0;
													if($item_tarifa['precio_infante'] > 0)
														$total_inf 	+= $fee;
													else
														$total_inf 	= 0;
												}

												if($item_tarifa['increment_factura'] == 'P')
												{
													$iva 		= 1 + ($item_tarifa['factura']/100);
													$total_sgl 	*= $iva;
													$total_dbl 	*= $iva;
													$total_tpl 	*= $iva;
													$total_cpl 	*= $iva;
													$total_cnn 	*= $iva;
													$total_inf 	*= $iva;
												}
												else
												{
													$iva 		= $item_tarifa['factura'];
													if($item_tarifa['precio_single'] > 0)
														$total_sgl 	+= $iva;
													else
														$total_sgl 	= 0;
													if($item_tarifa['precio_doble'] > 0)
														$total_dbl 	+= $iva;
													else
														$total_dbl 	= 0;
													if($item_tarifa['precio_triple'] > 0)
														$total_tpl 	+= $iva;
													else
														$total_tpl 	= 0;
													if($item_tarifa['precio_cuadruple'] > 0)
														$total_cpl 	+= $iva;
													else
														$total_cpl 	= 0;
													if($item_tarifa['precio_menor'] > 0)
														$total_cnn 	+= $iva;
													else
														$total_cnn 	= 0;
													if($item_tarifa['precio_infante'] > 0)
														$total_inf 	+= $iva;
													else
														$total_inf 	= 0;
												}

												if($item_tarifa['moneda'] == 'B')
												{
													$tc 		= $item_tarifa['tipo_cambio'];
													$total_sgl /= $tc;
													$total_dbl /= $tc;
													$total_tpl /= $tc;
													$total_cpl /= $tc;
													$total_cnn /= $tc;
													$total_inf /= $tc;
												}

												// SUMAR PRECIOS CON LOS OTROS SERVICIOS
												if($total_sgl > 0)
													$total_sgl += $serv_x_dest[$dest_hotel]['adt'];
												if($total_dbl > 0)
													$total_dbl += $serv_x_dest[$dest_hotel]['adt'];
												if($total_tpl > 0)
													$total_tpl += $serv_x_dest[$dest_hotel]['adt'];
												if($total_cpl > 0)
													$total_cpl += $serv_x_dest[$dest_hotel]['adt'];
												if($total_cnn > 0)
													$total_cnn += $serv_x_dest[$dest_hotel]['cnn'];
												if($total_inf > 0)
													$total_inf += $serv_x_dest[$dest_hotel]['inf'];
											?>
												<tr>
													<td><?php echo $item_tarifa['nombre_hotel'] . ' - ' . $item_tarifa['categoria'] . '*'; ?></td>
													<?php if(strpos($hide_columns, 'SGL')===false) { ?><td style="text-align:right;"><?php if($total_sgl>0) echo ceil($total_sgl); else echo '-'; ?></td><?php } ?>
													<?php if(strpos($hide_columns, 'DBL')===false) { ?><td style="text-align:right;"><?php if($total_dbl>0) echo ceil($total_dbl); else echo '-'; ?></td><?php } ?>
													<?php if(strpos($hide_columns, 'TPL')===false) { ?><td style="text-align:right;"><?php if($total_tpl>0) echo ceil($total_tpl); else echo '-'; ?></td><?php } ?>
													<?php if(strpos($hide_columns, 'CPL')===false) { ?><td style="text-align:right;"><?php if($total_cpl>0) echo ceil($total_cpl); else echo '-'; ?></td><?php } ?>
													<?php if(strpos($hide_columns, 'CNN')===false) { ?><td style="text-align:right;"><?php if($total_cnn>0) echo ceil($total_cnn); else echo '-'; ?></td><?php } ?>
													<?php if(strpos($hide_columns, 'INF')===false) { ?><td style="text-align:right;"><?php if($total_inf>0) echo ceil($total_inf); else echo '-'; ?></td><?php } ?>
													<td><?php echo strtoupper($item_tarifa['alimentacion']); ?></td>
													<td><?php echo strtoupper($item_tarifa['info_extra']); ?></td>
												</tr>
											<?php
											}
										?>
											</table>
									<?
										}				
									?>
								</div>

								<div class="clear_mayor todo_precio hotel_mas_servicio" style="display:none;"></div>
							<?php
							}
						?>

						<!-- HOTELES COMBINADOS MAS SERVICIO -->
						<?php
							$opciones_de_hoteles =  $c_mostrar_cotizacion_paquete->obtener_opciones_hotel_combinados($idciudad_origen);
							
							if(array_key_exists(0, $opciones_de_hoteles))
							{
							?>
								<div class="columns fifteen todo_precio hotel_comb_mas_servicio" style="display:none;">
									<div class="columns fourteen" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">PRECIOS POR PERSONA EN DOLARES AMERICANOS</div>
									<div class="clear"></div>
									<?php 
										$cant_col = $opciones_de_hoteles['cant_cols'];
										$mostrar_cols = $opciones_de_hoteles['col_mostrar'];
										?>
											<div class="columns thirteen" style="font-weight:bold; color:#10689b; font-size:11pt; margin-left:2%;">PAQUETE TERRESTRE</div>
											<table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios">
												<tr style="background-color:#10689b; color:#FFFFFF;">
													<th>HOTEL</th>
													<?php if(strpos($mostrar_cols, 'sgl')!==false) { ?><th>SINGLE</th><?php } ?>
													<?php if(strpos($mostrar_cols, 'dbl')!==false) { ?><th>DOBLE</th><?php } ?>
													<?php if(strpos($mostrar_cols, 'tpl')!==false) { ?><th>TRIPLE</th><?php } ?>
													<?php if(strpos($mostrar_cols, 'cpl')!==false) { ?><th>CUADRUPLE</th><?php } ?>
													<?php if(strpos($mostrar_cols, 'cnn')!==false) { ?><th>MENOR</th><?php } ?>
													<?php if(strpos($mostrar_cols, 'inf')!==false) { ?><th>INFANTE</th><?php } ?>
													<th>ALIMENTACION</th>
													<th>OBS</th>
												</tr>
										<?php
										for ($i=0; array_key_exists($i, $opciones_de_hoteles); $i++) 
										{ 
											$opcion_hotel 		= $opciones_de_hoteles[$i];
											if(array_key_exists('precio_sgl', $opcion_hotel))
												$opcion_hotel['precio_sgl'] += $total_serv_adt;
											if(array_key_exists('precio_dbl', $opcion_hotel))
												$opcion_hotel['precio_dbl'] += $total_serv_adt;
											if(array_key_exists('precio_tpl', $opcion_hotel))
												$opcion_hotel['precio_tpl'] += $total_serv_adt;
											if(array_key_exists('precio_cpl', $opcion_hotel))
												$opcion_hotel['precio_cpl'] += $total_serv_adt;
											if(array_key_exists('precio_cnn', $opcion_hotel))
												$opcion_hotel['precio_cnn'] += $total_serv_cnn;
											if(array_key_exists('precio_inf', $opcion_hotel))
												$opcion_hotel['precio_inf'] += $total_serv_inf;
										?>
												<tr>
													<td style="line-height:1.5;">
														<?php
															$hoteles = $opcion_hotel['hoteles'];
															for ($j=0; $j < count($hoteles); $j++) 
															{ 
																$item_tarifa = $hoteles[$j];
															 	echo $item_tarifa['nombre_hotel'] . ' - ' . $item_tarifa['categoria'] . '* (' . $item_tarifa['destino'] . ')<br/>'; 
															}
														?>
													</td>
													<?php if(strpos($mostrar_cols, 'sgl')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_sgl']; ?></td><?php } ?>
													<?php if(strpos($mostrar_cols, 'dbl')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_dbl']; ?></td><?php } ?>
													<?php if(strpos($mostrar_cols, 'tpl')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_tpl']; ?></td><?php } ?>
													<?php if(strpos($mostrar_cols, 'cpl')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_cpl']; ?></td><?php } ?>
													<?php if(strpos($mostrar_cols, 'cnn')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_cnn']; ?></td><?php } ?>
													<?php if(strpos($mostrar_cols, 'inf')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_inf']; ?></td><?php } ?>
													<td style="line-height:1.5;">
														<?php
															$hoteles = $opcion_hotel['hoteles'];
															for ($j=0; $j < count($hoteles); $j++) 
															{ 
																$item_tarifa = $hoteles[$j];
															 	echo strtoupper($item_tarifa['alimentacion']) . '<br/>';
															}
														?>
													</td>
													<td style="line-height:1.5;">
														<?php
															$hoteles = $opcion_hotel['hoteles'];
															for ($j=0; $j < count($hoteles); $j++) 
															{ 
																$item_tarifa = $hoteles[$j];
															 	echo strtoupper($item_tarifa['info_extra']) . '<br/>';
															}
														?>
													</td>
												</tr>									
										<?php
										}				
										?>
											</table>
								</div>

								<div class="clear_mayor todo_precio hotel_comb_mas_servicio"></div>
							<?php
							}
						?>

						<!-- BOLETOS AEREOS -->
						<?php
							$opciones_aereo = $c_mostrar_cotizacion_paquete->obtener_opcion_boletos_x_ciudad($idciudad_origen);
							if(count($opciones_aereo) > 0)
							{
							?>
								<div class="columns fifteen todo_precio solo_aereo" style="display:none;">
									<div class="columns thirteen" style="font-weight:bold; color:#10689b; font-size:11pt; margin-left:2%;">BOLETOS AEREOS</div>
									<div class="clear"></div>
									<?php
										for ($i=0; $i < count($opciones_aereo); $i++) 
										{ 
											$opcion_aereo 	= $opciones_aereo[$i];
											$idopaereo 		= $opcion_aereo['idopcion_boleto'];
										?>
											<div class="opc<?php echo $z . '_' . $i; ?> opc_aereo<?php echo $z; ?>" >
												<table style="width:70%; margin-left:3%;">
													<tr>
														<td style="width:5%;">
															<strong>OPCION :</strong>
														</td>
														<td style="width:30%;">
															<?php echo strtoupper($opcion_aereo['descripcion']); ?>
														</td>
														<td style="width:5%;">
															<strong>SALE :</strong>
														</td>
														<td style="width:30%;">
															<?php echo strtoupper($opcion_aereo['salida']); ?>
														</td>
														<td></td>
													</tr>
												</table>
												<table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios" >
													<tr style="background-color:#10689b; color:#FFFFFF;">
														<th rowspan="2">LINEA AEREA</th>
														<th rowspan="2">RUTA</th>
														<th colspan="3">PRECIOS</th>
													</tr>
													<tr style="background-color:#10689b; color:#FFFFFF;">
														<th>ADULTO</th>
														<th>MENOR</th>
														<th>INFANTE</th>
													</tr>
													<?php
														$boletos = $c_mostrar_cotizacion_paquete->obtener_boletos_x_opcion_aereo_real_cotizacion($idopaereo, 0);
														for ($j=0; $j < count($boletos); $j++) 
														{ 
															$boleto_cotizado 	= $boletos[$j];
															$idboleto 			= $boleto_cotizado['idboleto_cotizado'];
															
															$total_adt = $boleto_cotizado['precio_adulto'];
															$total_cnn = $boleto_cotizado['precio_menor'];
															$total_inf = $boleto_cotizado['precio_infante'];

															/*
															* AGREGA PAIS AL ARRAY SI HUBIESE
															*/
															$a_ruta = explode('/', $boleto_cotizado['ruta']);
															foreach ($a_ruta as $key => $value) 
															{
																$pais 							= $c_mostrar_cotizacion_paquete->obtener_pais(strtoupper($value));
																$array_paises[$pais['idpais']] 	= $pais['nombre_pais'];
															}
															/***/

															if($boleto_cotizado['increment_fee'] == 'P')
															{
																$fee 		= 1+($boleto_cotizado['fee'] / 100);
																$total_adt 	*= $fee;
																$total_cnn 	*= $fee;
																$total_inf 	*= $fee;
															}
															else
															{
																$fee = $boleto_cotizado['fee'];
																if($total_adt > 0)
																	$total_adt += $fee;
																else
																	$total_adt = 0;
																if($total_cnn > 0)
																	$total_cnn += $fee;
																else
																	$total_cnn = 0;
																if($total_inf > 0)
																	$total_inf += $fee;
																else
																	$total_inf = 0;
															}

															if($boleto_cotizado['increment_factura'] == 'P')
															{
																$iva 		= 1+($boleto_cotizado['factura'] / 100);
																$total_adt 	*= $iva;
																$total_cnn 	*= $iva;
																$total_inf 	*= $iva;
															}
															else
															{
																$iva = $boleto_cotizado['factura'];
																if($total_adt > 0)
																	$total_adt += $iva;
																else
																	$total_adt = 0;
																if($total_cnn > 0)
																	$total_cnn += $iva;
																else
																	$total_cnn = 0;
																if($total_inf > 0)
																	$total_inf += $iva;
																else
																	$total_inf = 0;
															}

															if($boleto_cotizado['moneda'] == 'B')
															{
																$tc 		= $boleto_cotizado['tipo_cambio'];
																$total_adt /= $tc;
																$total_cnn /= $tc;
																$total_inf /= $tc;
															}

															$total_bol_adt += $total_adt;
															$total_bol_cnn += $total_cnn;
															$total_bol_inf += $total_inf;
													?>
															<tr>
																<td><?php echo strtoupper($c_mostrar_cotizacion_paquete->obtener_linea_aerea($boleto_cotizado['idlineas_aereas'])); ?></td>
																<td><?php echo strtoupper($boleto_cotizado['ruta']); ?></td>
																<td style="text-align:right;"><?php if($total_adt>0) echo ceil($total_adt); else echo '-'; ?></td>
																<td style="text-align:right;"><?php if($total_cnn>0) echo ceil($total_cnn); else echo '-'; ?></td>
																<td style="text-align:right;"><?php if($total_inf>0) echo ceil($total_inf); else echo '-'; ?></td>
															</tr>
													<?php
														}
													?>
												</table>
												<br/>
												<div class="columns thirteen" style="font-weight:bold; font-size:10pt; margin-left:3%;">ITINERARIO AEREO DETALLADO</div>
												<table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios" >
													<tr style="background-color:#10689b; color:#FFFFFF;">
														<th>LINEA AEREA</th>
														<th>VUELO</th>
														<th>FECHA</th>
														<th>SALE</th>
														<th>LLEGA</th>
													</tr>
													<?php
														$itinerario = $c_mostrar_cotizacion_paquete->obtener_itinerario_x_opcion_aereo($idopaereo);
														for ($k=0; $k < count($itinerario); $k++) 
														{ 
															$segmento 	= $itinerario[$k];
															$idsegmento = $segmento['idsegmento'];

															$linea_aerea 		= $c_mostrar_cotizacion_paquete->obtener_linea_aerea($segmento['idlineas_aereas']);
															$aeropuerto_sale 	= $c_mostrar_cotizacion_paquete->obtener_aeropuerto($segmento['origen']);
															$aeropuerto_llega 	= $c_mostrar_cotizacion_paquete->obtener_aeropuerto($segmento['destino']);
													?>
															<tr>
																<td><?php echo strtoupper($linea_aerea); ?></td>
																<td><?php echo $segmento['nro_vuelo']; ?></td>
																<td><?php echo date('d/m/Y', strtotime($segmento['fecha'])); ?></td>
																<td><?php echo strtoupper($aeropuerto_sale) . ' <font style="font-size:7pt;">A HRS</font> ' . $segmento['hora_sale']; ?></td>
																<td><?php echo strtoupper($aeropuerto_llega) . ' <font style="font-size:7pt;">A HRS</font> ' . $segmento['hora_llega']; ?></td>
															</tr>
													<?php
														}
													?>
												</table>
											</div>
										<?php
										}
									?>
								</div>

								<div class="clear_mayor todo_precio solo_aereo" style="display:none;"></div>
							<?php
							}
						?>

						<!-- MUESTRA HOTEL MAS SERVICIOS MAS AEREO -->
						<?php
							//$opciones_aereo = $c_mostrar_cotizacion_paquete->obtener_opcion_boletos_x_ciudad($idciudad_origen);
							$minimo 		= 999999;
							$minimoactual 	= 0;
							if(count($opciones_aereo) > 0)
							{
							?>
								<div class="columns fifteen todo_precio hotel_mas_servicio_mas_aereo">
									<div class="columns fourteen" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">PRECIOS POR PERSONA EN DOLARES AMERICANOS</div>
									<div class="clear"></div>
									<?php
										for ($g=0; $g < count($opciones_aereo); $g++) 
										{ 
											$opcion_aereo 	= $opciones_aereo[$g];
											$idopaereo 		= $opcion_aereo['idopcion_boleto'];
										?>
											<div class="opc<?php echo $z . '_' . $g; ?> opc_aereo<?php echo $z; ?>" >
												<table style="width:70%; margin-left:3%;">
													<tr>
														<td style="width:5%;">
															<strong>OPCION :</strong>
														</td>
														<td style="width:30%;">
															<?php echo strtoupper($opcion_aereo['descripcion']); ?>
														</td>
														<td style="width:5%;">
															<strong>SALE :</strong>
														</td>
														<td style="width:30%;">
															<?php echo strtoupper($opcion_aereo['salida']); ?>
														</td>
														<td></td>
													</tr>
												</table>
												<?php
													$boletos = $c_mostrar_cotizacion_paquete->obtener_boletos_x_opcion_aereo_real_cotizacion($idopaereo, 0);
													$total_bol_adt = 0;
													$total_bol_cnn = 0;
													$total_bol_inf = 0;
													for ($j=0; $j < count($boletos); $j++) 
													{ 
														$boleto_cotizado 	= $boletos[$j];
														$idboleto 			= $boleto_cotizado['idboleto_cotizado'];
														
														$total_adt = $boleto_cotizado['precio_adulto'];
														$total_cnn = $boleto_cotizado['precio_menor'];
														$total_inf = $boleto_cotizado['precio_infante'];

														if($boleto_cotizado['increment_fee'] == 'P')
														{
															$fee 		= 1+($boleto_cotizado['fee'] / 100);
															$total_adt 	*= $fee;
															$total_cnn 	*= $fee;
															$total_inf 	*= $fee;
														}
														else
														{
															$fee = $boleto_cotizado['fee'];
															if($total_adt > 0)
																$total_adt += $fee;
															else
																$total_adt = 0;
															if($total_cnn > 0)
																$total_cnn += $fee;
															else
																$total_cnn = 0;
															if($total_inf > 0)
																$total_inf += $fee;
															else
																$total_inf = 0;
														}

														if($boleto_cotizado['increment_factura'] == 'P')
														{
															$iva 		= 1+($boleto_cotizado['factura'] / 100);
															$total_adt 	*= $iva;
															$total_cnn 	*= $iva;
															$total_inf 	*= $iva;
														}
														else
														{
															$iva = $boleto_cotizado['factura'];
															if($total_adt > 0)
																$total_adt += $iva;
															else
																$total_adt = 0;
															if($total_cnn > 0)
																$total_cnn += $iva;
															else
																$total_cnn = 0;
															if($total_inf > 0)
																$total_inf += $iva;
															else
																$total_inf = 0;
														}

														if($boleto_cotizado['moneda'] == 'B')
														{
															$tc 		= $boleto_cotizado['tipo_cambio'];
															$total_adt /= $tc;
															$total_cnn /= $tc;
															$total_inf /= $tc;
														}

														$total_bol_adt += ceil($total_adt);
														$total_bol_cnn += ceil($total_cnn);
														$total_bol_inf += ceil($total_inf);
													}
													if($total_bol_adt < $minimo)
													{
														$minimo 		= $total_bol_adt;
														$minimoactual 	= $g;
													}
												?>
												<?php
													if(count($tarifas_por_destino) > 0)
													{
													?>
														<div class="clear"></div>
														<?php 
															for ($i=0; $i < count($tarifas_por_destino); $i++) 
															{ 
																$tarifa_hotel 		= $tarifas_por_destino[$i];
																$idth 				= $tarifa_hotel['idtarifa_hotel'];
																$items_tarifa 		= $c_mostrar_cotizacion_paquete->obtener_hoteles_x_destino_real_cotizacion($idth, 0);

																$hide_columns 		= $c_mostrar_cotizacion_paquete->evaluar_precios_hotel($items_tarifa);
																$cant_col 			= count(explode('/', $hide_columns));
															?>
																<strong style="margin-left:3%;">HOTELES EN <?php echo strtoupper($c_mostrar_cotizacion_paquete->obtener_aeropuerto($tarifa_hotel['destino'])); ?></strong>
																<table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios">
																	<tr style="background-color:#10689b; color:#FFFFFF;">
																		<th>HOTEL</th>
																		<?php if(strpos($hide_columns, 'SGL')===false) { ?><th>SINGLE</th><?php } ?>
																		<?php if(strpos($hide_columns, 'DBL')===false) { ?><th>DOBLE</th><?php } ?>
																		<?php if(strpos($hide_columns, 'TPL')===false) { ?><th>TRIPLE</th><?php } ?>
																		<?php if(strpos($hide_columns, 'CPL')===false) { ?><th>CUADRUPLE</th><?php } ?>
																		<?php if(strpos($hide_columns, 'CNN')===false) { ?><th>MENOR</th><?php } ?>
																		<?php if(strpos($hide_columns, 'INF')===false) { ?><th>INFANTE</th><?php } ?>
																		<th>ALIMENTACION</th>
																		<th>OBS</th>
																	</tr>
															<?php
																for ($j=0; $j < count($items_tarifa); $j++) 
																{ 
																	$item_tarifa 	= $items_tarifa[$j];
																	$iditem 		= $item_tarifa['iditem_hotel'];

																	$cant_noches 	= floor( (strtotime($item_tarifa['fecha_out'] . ' 00:00:00') - strtotime($item_tarifa['fecha_in'] . ' 00:00:00')) / 86400);
																	
																	$total_sgl 	= $item_tarifa['precio_single'] * $cant_noches;
																	$total_dbl 	= $item_tarifa['precio_doble'] * $cant_noches;
																	$total_tpl 	= $item_tarifa['precio_triple'] * $cant_noches;
																	$total_cpl 	= $item_tarifa['precio_cuadruple'] * $cant_noches;
																	$total_cnn 	= $item_tarifa['precio_menor'] * $cant_noches;
																	$total_inf 	= $item_tarifa['precio_infante'] * $cant_noches;

																	if($item_tarifa['increment_fee'] == 'P')
																	{
																		$fee 		= 1 + ($item_tarifa['fee']/100);
																		$total_sgl 	*= $fee;
																		$total_dbl 	*= $fee;
																		$total_tpl 	*= $fee;
																		$total_cpl 	*= $fee;
																		$total_cnn 	*= $fee;
																		$total_inf 	*= $fee;
																	}
																	else
																	{
																		$fee 		= $item_tarifa['fee'];
																		if($item_tarifa['precio_single'] > 0)
																			$total_sgl 	+= $fee;
																		else
																			$total_sgl 	= 0;
																		if($item_tarifa['precio_doble'] > 0)
																			$total_dbl 	+= $fee;
																		else
																			$total_dbl 	= 0;
																		if($item_tarifa['precio_triple'] > 0)
																			$total_tpl 	+= $fee;
																		else
																			$total_tpl 	= 0;
																		if($item_tarifa['precio_cuadruple'] > 0)
																			$total_cpl 	+= $fee;
																		else
																			$total_cpl 	= 0;
																		if($item_tarifa['precio_menor'] > 0)
																			$total_cnn 	+= $fee;
																		else
																			$total_cnn 	= 0;
																		if($item_tarifa['precio_infante'] > 0)
																			$total_inf 	+= $fee;
																		else
																			$total_inf 	= 0;
																	}

																	if($item_tarifa['increment_factura'] == 'P')
																	{
																		$iva 		= 1 + ($item_tarifa['factura']/100);
																		$total_sgl 	*= $iva;
																		$total_dbl 	*= $iva;
																		$total_tpl 	*= $iva;
																		$total_cpl 	*= $iva;
																		$total_cnn 	*= $iva;
																		$total_inf 	*= $iva;
																	}
																	else
																	{
																		$iva 		= $item_tarifa['factura'];
																		if($item_tarifa['precio_single'] > 0)
																			$total_sgl 	+= $iva;
																		else
																			$total_sgl 	= 0;
																		if($item_tarifa['precio_doble'] > 0)
																			$total_dbl 	+= $iva;
																		else
																			$total_dbl 	= 0;
																		if($item_tarifa['precio_triple'] > 0)
																			$total_tpl 	+= $iva;
																		else
																			$total_tpl 	= 0;
																		if($item_tarifa['precio_cuadruple'] > 0)
																			$total_cpl 	+= $iva;
																		else
																			$total_cpl 	= 0;
																		if($item_tarifa['precio_menor'] > 0)
																			$total_cnn 	+= $iva;
																		else
																			$total_cnn 	= 0;
																		if($item_tarifa['precio_infante'] > 0)
																			$total_inf 	+= $iva;
																		else
																			$total_inf 	= 0;
																	}

																	if($item_tarifa['moneda'] == 'B')
																	{
																		$tc 		= $item_tarifa['tipo_cambio'];
																		$total_sgl /= $tc;
																		$total_dbl /= $tc;
																		$total_tpl /= $tc;
																		$total_cpl /= $tc;
																		$total_cnn /= $tc;
																		$total_inf /= $tc;
																	}

																	// SUMAR PRECIOS CON LOS OTROS SERVICIOS
																	if($total_sgl > 0)
																		$total_sgl += $total_serv_adt + $total_bol_adt;
																	if($total_dbl > 0)
																		$total_dbl += $total_serv_adt + $total_bol_adt;
																	if($total_tpl > 0)
																		$total_tpl += $total_serv_adt + $total_bol_adt;
																	if($total_cpl > 0)
																		$total_cpl += $total_serv_adt + $total_bol_adt;
																	if($total_cnn > 0)
																		$total_cnn += $total_serv_cnn + $total_bol_cnn;
																	if($total_inf > 0)
																		$total_inf += $total_serv_inf + $total_bol_inf;
																?>
																	<tr>
																		<td><?php echo $item_tarifa['nombre_hotel'] . ' - ' . $item_tarifa['categoria'] . '*'; ?></td>
																		<?php if(strpos($hide_columns, 'SGL')===false) { ?><td style="text-align:right;"><?php if($total_sgl>0) echo ceil($total_sgl); else echo '-'; ?></td><?php } ?>
																		<?php if(strpos($hide_columns, 'DBL')===false) { ?><td style="text-align:right;"><?php if($total_dbl>0) echo ceil($total_dbl); else echo '-'; ?></td><?php } ?>
																		<?php if(strpos($hide_columns, 'TPL')===false) { ?><td style="text-align:right;"><?php if($total_tpl>0) echo ceil($total_tpl); else echo '-'; ?></td><?php } ?>
																		<?php if(strpos($hide_columns, 'CPL')===false) { ?><td style="text-align:right;"><?php if($total_cpl>0) echo ceil($total_cpl); else echo '-'; ?></td><?php } ?>
																		<?php if(strpos($hide_columns, 'CNN')===false) { ?><td style="text-align:right;"><?php if($total_cnn>0) echo ceil($total_cnn); else echo '-'; ?></td><?php } ?>
																		<?php if(strpos($hide_columns, 'INF')===false) { ?><td style="text-align:right;"><?php if($total_inf>0) echo ceil($total_inf); else echo '-'; ?></td><?php } ?>
																		<td><?php echo strtoupper($item_tarifa['alimentacion']); ?></td>
																		<td><?php echo strtoupper($item_tarifa['info_extra']); ?></td>
																	</tr>
																<?php
																}
															?>
																</table>
															<?
															}				
														?>
														<div class="clear_mayor"></div>
													<?php
													}
												?>
												<div class="columns thirteen" style="font-weight:bold; font-size:10pt; margin-left:3%;">ITINERARIO AEREO DETALLADO</div>
												<table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios" >
													<tr style="background-color:#10689b; color:#FFFFFF;">
														<th>LINEA AEREA</th>
														<th>VUELO</th>
														<th>FECHA</th>
														<th>SALE</th>
														<th>LLEGA</th>
													</tr>
													<?php
														$itinerario = $c_mostrar_cotizacion_paquete->obtener_itinerario_x_opcion_aereo($idopaereo);
														for ($k=0; $k < count($itinerario); $k++) 
														{ 
															$segmento 	= $itinerario[$k];
															$idsegmento = $segmento['idsegmento'];

															$linea_aerea 		= $c_mostrar_cotizacion_paquete->obtener_linea_aerea($segmento['idlineas_aereas']);
															$aeropuerto_sale 	= $c_mostrar_cotizacion_paquete->obtener_aeropuerto($segmento['origen']);
															$aeropuerto_llega 	= $c_mostrar_cotizacion_paquete->obtener_aeropuerto($segmento['destino']);
													?>
															<tr>
																<td><?php echo strtoupper($linea_aerea); ?></td>
																<td><?php echo $segmento['nro_vuelo']; ?></td>
																<td><?php echo date('d/m/Y', strtotime($segmento['fecha'])); ?></td>
																<td><?php echo strtoupper($aeropuerto_sale) . ' <font style="font-size:7pt;">A HRS</font> ' . $segmento['hora_sale']; ?></td>
																<td><?php echo strtoupper($aeropuerto_llega) . ' <font style="font-size:7pt;">A HRS</font> ' . $segmento['hora_llega']; ?></td>
															</tr>
													<?php
														}
													?>
												</table>
											</div>
										<?php
										}
									?>
								</div>

								<div class="clear_mayor todo_precio hotel_mas_servicio_mas_aereo"></div>

								<div id="opc_aereo_show<?php echo $z; ?>" style="display:none;" >
									<?php
										for ($g=0; $g < count($opciones_aereo); $g++) 
										{ 
											$opcion_aereo 	= $opciones_aereo[$g];
											$idopaereo 		= $opcion_aereo['idopcion_boleto'];
										?>
											<input type="checkbox" onchange="$('.opc<?php echo $z . '_' . $g; ?>').toggle();" <?php if($minimoactual == $g) echo 'checked'; ?> />
											Mostrar Opcion <?php echo $opcion_aereo['descripcion']; ?>
											<br/>
										<?php
										}
									?>
								</div>
								<script type="text/javascript">
									$(function()
										{
											$('.opc_aereo<?php echo $z; ?>').hide();
											$('.opc<?php echo $z; ?>_<?php echo $minimoactual; ?>').show();
											<?php
												if($z == 0)
												{
											?>
													$('.vista_opciones_aereo').html($('#opc_aereo_show0').html());
											<?php
												}
											?>
										});
								</script>
							<?php
							}
						?>

						<!-- MUESTRA HOTEL COMBINADO MAS SERVICIOS MAS AEREO -->
						<?php
							//$opciones_aereo = $c_mostrar_cotizacion_paquete->obtener_opcion_boletos_x_ciudad($idciudad_origen);
							if(count($opciones_aereo) > 0)
							{
							?>
								<div class="columns fifteen todo_precio hotel_comb_mas_servicio_mas_aereo" style="display:none;" >
									<div class="columns fourteen" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">PRECIOS POR PERSONA EN DOLARES AMERICANOS</div>
									<div class="clear"></div>
									<?php
										for ($g=0; $g < count($opciones_aereo); $g++) 
										{ 
											$opcion_aereo 	= $opciones_aereo[$g];
											$idopaereo 		= $opcion_aereo['idopcion_boleto'];
										?>
											<div class="opc<?php echo $z . '_' . $g; ?> opc_aereo<?php echo $z; ?>" >
												<table style="width:70%; margin-left:3%;">
													<tr>
														<td style="width:5%;">
															<strong>OPCION :</strong>
														</td>
														<td style="width:30%;">
															<?php echo strtoupper($opcion_aereo['descripcion']); ?>
														</td>
														<td style="width:5%;">
															<strong>SALE :</strong>
														</td>
														<td style="width:30%;">
															<?php echo strtoupper($opcion_aereo['salida']); ?>
														</td>
														<td></td>
													</tr>
												</table>
												<?php
													$boletos = $c_mostrar_cotizacion_paquete->obtener_boletos_x_opcion_aereo($idopaereo);
													$total_bol_adt = 0;
													$total_bol_cnn = 0;
													$total_bol_inf = 0;
													for ($j=0; $j < count($boletos); $j++) 
													{ 
														$boleto_cotizado 	= $boletos[$j];
														$idboleto 			= $boleto_cotizado['idboleto_cotizado'];
														
														$total_adt = $boleto_cotizado['precio_adulto'];
														$total_cnn = $boleto_cotizado['precio_menor'];
														$total_inf = $boleto_cotizado['precio_infante'];

														if($boleto_cotizado['increment_fee'] == 'P')
														{
															$fee 		= 1+($boleto_cotizado['fee'] / 100);
															$total_adt 	*= $fee;
															$total_cnn 	*= $fee;
															$total_inf 	*= $fee;
														}
														else
														{
															$fee = $boleto_cotizado['fee'];
															if($total_adt > 0)
																$total_adt += $fee;
															else
																$total_adt = 0;
															if($total_cnn > 0)
																$total_cnn += $fee;
															else
																$total_cnn = 0;
															if($total_inf > 0)
																$total_inf += $fee;
															else
																$total_inf = 0;
														}

														if($boleto_cotizado['increment_factura'] == 'P')
														{
															$iva 		= 1+($boleto_cotizado['factura'] / 100);
															$total_adt 	*= $iva;
															$total_cnn 	*= $iva;
															$total_inf 	*= $iva;
														}
														else
														{
															$iva = $boleto_cotizado['factura'];
															if($total_adt > 0)
																$total_adt += $iva;
															else
																$total_adt = 0;
															if($total_cnn > 0)
																$total_cnn += $iva;
															else
																$total_cnn = 0;
															if($total_inf > 0)
																$total_inf += $iva;
															else
																$total_inf = 0;
														}

														if($boleto_cotizado['moneda'] == 'B')
														{
															$tc 		= $boleto_cotizado['tipo_cambio'];
															$total_adt /= $tc;
															$total_cnn /= $tc;
															$total_inf /= $tc;
														}

														$total_bol_adt += ceil($total_adt);
														$total_bol_cnn += ceil($total_cnn);
														$total_bol_inf += ceil($total_inf);
													}
												?>
												<?php
													if(array_key_exists(0, $opciones_de_hoteles))
													{
													?>
														<div class="clear"></div>
														<?php 
															$cant_col = $opciones_de_hoteles['cant_cols'];
															$mostrar_cols = $opciones_de_hoteles['col_mostrar'];
															?>
																<table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios">
																	<tr style="background-color:#10689b; color:#FFFFFF;">
																		<th>HOTEL</th>
																		<?php if(strpos($mostrar_cols, 'sgl')!==false) { ?><th>SINGLE</th><?php } ?>
																		<?php if(strpos($mostrar_cols, 'dbl')!==false) { ?><th>DOBLE</th><?php } ?>
																		<?php if(strpos($mostrar_cols, 'tpl')!==false) { ?><th>TRIPLE</th><?php } ?>
																		<?php if(strpos($mostrar_cols, 'cpl')!==false) { ?><th>CUADRUPLE</th><?php } ?>
																		<?php if(strpos($mostrar_cols, 'cnn')!==false) { ?><th>MENOR</th><?php } ?>
																		<?php if(strpos($mostrar_cols, 'inf')!==false) { ?><th>INFANTE</th><?php } ?>
																		<th>ALIMENTACION</th>
																		<th>OBS</th>
																	</tr>
															<?php
															for ($i=0; array_key_exists($i, $opciones_de_hoteles); $i++) 
															{ 
																$opcion_hotel 		= $opciones_de_hoteles[$i];
																if(array_key_exists('precio_sgl', $opcion_hotel))
																	$opcion_hotel['precio_sgl'] += $total_serv_adt + $total_bol_adt;
																if(array_key_exists('precio_dbl', $opcion_hotel))
																	$opcion_hotel['precio_dbl'] += $total_serv_adt + $total_bol_adt;
																if(array_key_exists('precio_tpl', $opcion_hotel))
																	$opcion_hotel['precio_tpl'] += $total_serv_adt + $total_bol_adt;
																if(array_key_exists('precio_cpl', $opcion_hotel))
																	$opcion_hotel['precio_cpl'] += $total_serv_adt + $total_bol_adt;
																if(array_key_exists('precio_cnn', $opcion_hotel))
																	$opcion_hotel['precio_cnn'] += $total_serv_cnn + $total_bol_cnn;
																if(array_key_exists('precio_inf', $opcion_hotel))
																	$opcion_hotel['precio_inf'] += $total_serv_inf + $total_bol_inf;
															?>
																	<tr>
																		<td style="line-height:1.5;">
																			<?php
																				$hoteles = $opcion_hotel['hoteles'];
																				for ($j=0; $j < count($hoteles); $j++) 
																				{ 
																					$item_tarifa = $hoteles[$j];
																				 	echo $item_tarifa['nombre_hotel'] . ' - ' . $item_tarifa['categoria'] . '* (' . $item_tarifa['destino'] . ')<br/>'; 
																				}
																			?>
																		</td>
																		<?php if(strpos($mostrar_cols, 'sgl')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_sgl']; ?></td><?php } ?>
																		<?php if(strpos($mostrar_cols, 'dbl')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_dbl']; ?></td><?php } ?>
																		<?php if(strpos($mostrar_cols, 'tpl')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_tpl']; ?></td><?php } ?>
																		<?php if(strpos($mostrar_cols, 'cpl')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_cpl']; ?></td><?php } ?>
																		<?php if(strpos($mostrar_cols, 'cnn')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_cnn']; ?></td><?php } ?>
																		<?php if(strpos($mostrar_cols, 'inf')!==false) { ?><td style="text-align:right;"><?php echo $opcion_hotel['precio_inf']; ?></td><?php } ?>
																		<td style="line-height:1.5;">
																			<?php
																				$hoteles = $opcion_hotel['hoteles'];
																				for ($j=0; $j < count($hoteles); $j++) 
																				{ 
																					$item_tarifa = $hoteles[$j];
																				 	echo strtoupper($item_tarifa['alimentacion']) . '<br/>';
																				}
																			?>
																		</td>
																		<td style="line-height:1.5;">
																			<?php
																				$hoteles = $opcion_hotel['hoteles'];
																				for ($j=0; $j < count($hoteles); $j++) 
																				{ 
																					$item_tarifa = $hoteles[$j];
																				 	echo strtoupper($item_tarifa['info_extra']) . '<br/>';
																				}
																			?>
																		</td>
																	</tr>									
															<?php
															}				
															?>
																</table>
													<?php
													}
												?>
												<br/>
												<div class="columns thirteen" style="font-weight:bold; font-size:10pt; margin-left:3%;">ITINERARIO AEREO DETALLADO</div>
												<table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios" >
													<tr style="background-color:#10689b; color:#FFFFFF;">
														<th>LINEA AEREA</th>
														<th>VUELO</th>
														<th>FECHA</th>
														<th>SALE</th>
														<th>LLEGA</th>
													</tr>
													<?php
														$itinerario = $c_mostrar_cotizacion_paquete->obtener_itinerario_x_opcion_aereo($idopaereo);
														for ($k=0; $k < count($itinerario); $k++) 
														{ 
															$segmento 	= $itinerario[$k];
															$idsegmento = $segmento['idsegmento'];

															$linea_aerea 		= $c_mostrar_cotizacion_paquete->obtener_linea_aerea($segmento['idlineas_aereas']);
															$aeropuerto_sale 	= $c_mostrar_cotizacion_paquete->obtener_aeropuerto($segmento['origen']);
															$aeropuerto_llega 	= $c_mostrar_cotizacion_paquete->obtener_aeropuerto($segmento['destino']);
														?>
															<tr>
																<td><?php echo strtoupper($linea_aerea); ?></td>
																<td><?php echo $segmento['nro_vuelo']; ?></td>
																<td><?php echo date('d/m/Y', strtotime($segmento['fecha'])); ?></td>
																<td><?php echo strtoupper($aeropuerto_sale) . ' <font style="font-size:7pt;">A HRS</font> ' . $segmento['hora_sale']; ?></td>
																<td><?php echo strtoupper($aeropuerto_llega) . ' <font style="font-size:7pt;">A HRS</font> ' . $segmento['hora_llega']; ?></td>
															</tr>
														<?php
														}
													?>
												</table>
											</div>
										<?php
										}
									?>
								</div>

								<div class="clear_mayor todo_precio hotel_comb_mas_servicio_mas_aereo" style="display:none;"></div>

							<?php
							}
						?>

					</div>

					<div class="is_real1" style="display:none;">
						
						<!-- HOTELES TARIFAS REALES -->
						<?php
							if(count($tarifas_por_destino) > 0)
							{
							?>
								<div class="columns fifteen">
									<div class="columns five" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">PRECIOS DE HOTELES</div>
									<div class="clear"></div>
									<?php 
										for ($i=0; $i < count($tarifas_por_destino); $i++) 
										{ 
											$tarifa_hotel 		= $tarifas_por_destino[$i];
											$idth 				= $tarifa_hotel['idtarifa_hotel'];
											$items_tarifa 		= $c_mostrar_cotizacion_paquete->obtener_hoteles_x_destino_real_cotizacion($idth, 1);

											$hide_columns 		= $c_mostrar_cotizacion_paquete->evaluar_precios_hotel($items_tarifa);
											$cant_col 			= count(explode('/', $hide_columns));
									?>
											<table style="width:50%; margin-left:3%;">
												<tr>
													<td style="width:5%;">
														<strong>DESTINO :</strong>
													</td>
													<td style="width:20%;">
														<?php echo strtoupper($tarifa_hotel['destino']); ?>
													</td>
													<td>
													</td>
												</tr>
											</table>
											<table style="width:100%; font-size:8pt; margin-left:3%;" class="tabla-precios">
												<tr style="background-color:#10689b; color:#FFFFFF;">
													<th rowspan="2">HOTEL</th>
													<?php if($cant_col > 0) { ?><th colspan="<?php echo (7-$cant_col); ?>">PRECIOS</th><?php } ?>
													<th rowspan="2">ALIMENTACION</th>
													<th rowspan="2">OBS</th>
													<th rowspan="2" colspan="2" style="width:100px !important; max-width:100px !important;">PAXS</th>
													<th rowspan="2" colspan="2" style="width:100px !important; max-width:100px !important;">ADJUNTOS</th>
												</tr>
												<tr style="background-color:#10689b; color:#FFFFFF;">
													<?php if(strpos($hide_columns, 'SGL')===false) { ?><th>SINGLE</th><?php } ?>
													<?php if(strpos($hide_columns, 'DBL')===false) { ?><th>DOBLE</th><?php } ?>
													<?php if(strpos($hide_columns, 'TPL')===false) { ?><th>TRIPLE</th><?php } ?>
													<?php if(strpos($hide_columns, 'CPL')===false) { ?><th>CUADRUPLE</th><?php } ?>
													<?php if(strpos($hide_columns, 'CNN')===false) { ?><th>MENOR</th><?php } ?>
													<?php if(strpos($hide_columns, 'INF')===false) { ?><th>INFANTE</th><?php } ?>
												</tr>
									<?php
											for ($j=0; $j < count($items_tarifa); $j++) 
											{ 
												$item_tarifa 	= $items_tarifa[$j];
												$iditem 		= $item_tarifa['iditem_hotel'];

												$cant_noches 	= floor( (strtotime($item_tarifa['fecha_out'] . ' 00:00:00') - strtotime($item_tarifa['fecha_in'] . ' 00:00:00')) / 86400);
												
												$total_sgl 	= $item_tarifa['precio_single'] * $cant_noches;
												$total_dbl 	= $item_tarifa['precio_doble'] * $cant_noches;
												$total_tpl 	= $item_tarifa['precio_triple'] * $cant_noches;
												$total_cpl 	= $item_tarifa['precio_cuadruple'] * $cant_noches;
												$total_cnn 	= $item_tarifa['precio_menor'] * $cant_noches;
												$total_inf 	= $item_tarifa['precio_infante'] * $cant_noches;

												if($item_tarifa['increment_fee'] == 'P')
												{
													$fee 		= 1 + ($item_tarifa['fee']/100);
													$total_sgl 	*= $fee;
													$total_dbl 	*= $fee;
													$total_tpl 	*= $fee;
													$total_cpl 	*= $fee;
													$total_cnn 	*= $fee;
													$total_inf 	*= $fee;
												}
												else
												{
													$fee 		= $item_tarifa['fee'];
													if($item_tarifa['precio_single'] > 0)
														$total_sgl 	+= $fee;
													else
														$total_sgl 	= 0;
													if($item_tarifa['precio_doble'] > 0)
														$total_dbl 	+= $fee;
													else
														$total_dbl 	= 0;
													if($item_tarifa['precio_triple'] > 0)
														$total_tpl 	+= $fee;
													else
														$total_tpl 	= 0;
													if($item_tarifa['precio_cuadruple'] > 0)
														$total_cpl 	+= $fee;
													else
														$total_cpl 	= 0;
													if($item_tarifa['precio_menor'] > 0)
														$total_cnn 	+= $fee;
													else
														$total_cnn 	= 0;
													if($item_tarifa['precio_infante'] > 0)
														$total_inf 	+= $fee;
													else
														$total_inf 	= 0;
												}

												if($item_tarifa['increment_factura'] == 'P')
												{
													$iva 		= 1 + ($item_tarifa['factura']/100);
													$total_sgl 	*= $iva;
													$total_dbl 	*= $iva;
													$total_tpl 	*= $iva;
													$total_cpl 	*= $iva;
													$total_cnn 	*= $iva;
													$total_inf 	*= $iva;
												}
												else
												{
													$iva 		= $item_tarifa['factura'];
													if($item_tarifa['precio_single'] > 0)
														$total_sgl 	+= $iva;
													else
														$total_sgl 	= 0;
													if($item_tarifa['precio_doble'] > 0)
														$total_dbl 	+= $iva;
													else
														$total_dbl 	= 0;
													if($item_tarifa['precio_triple'] > 0)
														$total_tpl 	+= $iva;
													else
														$total_tpl 	= 0;
													if($item_tarifa['precio_cuadruple'] > 0)
														$total_cpl 	+= $iva;
													else
														$total_cpl 	= 0;
													if($item_tarifa['precio_menor'] > 0)
														$total_cnn 	+= $iva;
													else
														$total_cnn 	= 0;
													if($item_tarifa['precio_infante'] > 0)
														$total_inf 	+= $iva;
													else
														$total_inf 	= 0;
												}

												if($item_tarifa['moneda'] == 'B')
												{
													$tc 		= $item_tarifa['tipo_cambio'];
													$total_sgl /= $tc;
													$total_dbl /= $tc;
													$total_tpl /= $tc;
													$total_cpl /= $tc;
													$total_cnn /= $tc;
													$total_inf /= $tc;
												}
									?>
												<tr>
													<td style="line-height:1;">
														<?php echo strtoupper($item_tarifa['nombre_hotel'] . ' - ' . $item_tarifa['categoria'] . '* (' . $item_tarifa['identificador'] . ')'); ?><br/>
														<strong>IN:</strong><?php echo date('d/m/Y', strtotime($item_tarifa['fecha_in'])); ?><br/>
														<strong>OUT:</strong><?php echo date('d/m/Y', strtotime($item_tarifa['fecha_out'])); ?><br/>
													</td>
													<?php if(strpos($hide_columns, 'SGL')===false) { ?><td style="text-align:right;"><?php if($total_sgl>0) echo ceil($total_sgl); else echo '-'; ?></td><?php } ?>
													<?php if(strpos($hide_columns, 'DBL')===false) { ?><td style="text-align:right;"><?php if($total_dbl>0) echo ceil($total_dbl); else echo '-'; ?></td><?php } ?>
													<?php if(strpos($hide_columns, 'TPL')===false) { ?><td style="text-align:right;"><?php if($total_tpl>0) echo ceil($total_tpl); else echo '-'; ?></td><?php } ?>
													<?php if(strpos($hide_columns, 'CPL')===false) { ?><td style="text-align:right;"><?php if($total_cpl>0) echo ceil($total_cpl); else echo '-'; ?></td><?php } ?>
													<?php if(strpos($hide_columns, 'CNN')===false) { ?><td style="text-align:right;"><?php if($total_cnn>0) echo ceil($total_cnn); else echo '-'; ?></td><?php } ?>
													<?php if(strpos($hide_columns, 'INF')===false) { ?><td style="text-align:right;"><?php if($total_inf>0) echo ceil($total_inf); else echo '-'; ?></td><?php } ?>
													<td><?php echo strtoupper($item_tarifa['alimentacion']); ?></td>
													<td><?php echo strtoupper($item_tarifa['info_extra']); ?></td>
													<td id="paxh<?php echo $item_tarifa['iditem_hotel']; ?>" >
														<?php
															$paxs = $c_mostrar_cotizacion->obtener_paxs_segun_hotel($item_tarifa['iditem_hotel']);
															//echo "<pre>"; print_r($paxs); echo "</pre>";
															$aux1 = '';
															$aux2 = 0;
															$color = '#00FF00';
															for ($a=0; $a < count($paxs); $a++) 
															{ 
																$pax = $paxs[$a];
																$id  = $pax['idpax'] . '_' . $item_tarifa['iditem_hotel'];
																switch ($pax['tipo_precio']) 
																{
																	case 1: $hab='SGL'; break;
																	case 2: $hab='DBL'; break;
																	case 3: $hab='TPL'; break;
																	case 4: $hab='CPL'; break;
																	case 5: $hab='CNN'; break;
																	case 6: $hab='INF'; break;
																	default:$hab='N/N'; break;
																}

																if($pax['habitacion'] != $aux1)
																{
																	$aux1 = $pax['habitacion'];
																	$aux2++;
																}
																if($aux2 % 2 == 0)
																	$color = '#009629';
																else
																	$color = '#0000FF';

																?>
																	<div style="color:<?php echo $color; ?>;" id="paxh<?php echo $id; ?>" onmouseover="$('#delh<?php echo $id; ?>').show();" onmouseout="$('#delh<?php echo $id; ?>').hide();" >
																		<?php echo $pax['nombre_cliente'] . $pax['apellido_cliente'] . '(' . $hab . ')'; ?>
																		<img id="delh<?php echo $id; ?>" src="images/userminus32.png" style="width:15px; height:15px; cursor:pointer; display:none;" title="Quitar Pasajero" onclick="QuitarPaxServicio(<?php echo $pax['idpax']; ?>, <?php echo $item_tarifa['iditem_hotel']; ?>, 'h');" />			
																	</div>
																<?php
															}
														?>
													</td>
													<td style="width:17px;">
														<img id="addh<?php echo $item_tarifa['iditem_hotel']; ?>" src="images/userplus32.png" style="width:15px; height:15px; cursor:pointer;" title="Añadir Pasajero" onclick="AgregarPaxAlServicioBusqueda(<?php echo $item_tarifa['iditem_hotel']; ?>, 'h', '<?php echo $item_tarifa['nombre_hotel']; ?>');" />
													</td>
													<td id="aadjh<?php echo $item_tarifa['iditem_hotel']; ?>">
														<?php
															$adjuntos = $c_mostrar_cotizacion->mostrar_adjuntos_por_hotel($item_tarifa['iditem_hotel']);
															for ($x=0; $x < count($adjuntos); $x++) 
															{ 
																$adjunto 	= $adjuntos[$x];
																$id 		= $adjunto['idarchivo_adjunto'];
																if($adjunto['is_visible'] == 0)
																	$color = '#FF3300';
																else
																	$color = '#999999';
															?>
																<div id="adjh<?php echo $id; ?>" onmouseover="$('#deladjh<?php echo $id; ?>').show();" onmouseout="$('#deladjh<?php echo $id; ?>').hide();" >
																	<a style="color:<?php echo $color; ?>;" href="<?php echo $ruta_adjuntos . $adjunto['nombre']; ?>" target="_blank"><?php echo substr($adjunto['descripcion'], 0, 15); ?></a>
																	<img id="deladjh<?php echo $id; ?>" src="images/userminus32.png" style="width:15px; height:15px; cursor:pointer; display:none;" title="Eliminar Archivo" onclick="eliminar_adjunto_x_servicio(<?php echo $item_tarifa['iditem_hotel']; ?>, 'h', '<?php echo $id; ?>');" />
																</div>
															<?php
															}
														?>
													</td>
													<td style="width:17px;">
														<img id="addah<?php echo $item_tarifa['iditem_hotel']; ?>" src="images/upload32.png" style="width:15px; height:15px; cursor:pointer;" title="Añadir Archivo" onclick="mostrar_url_popup('cargar_archivo_adjunto_por_servicio.php?s=<?php echo $item_tarifa['iditem_hotel']; ?>&t=h', 200);" />
													</td>
												</tr>
									<?php
											}
									?>
											</table>
									<?
										}				
									?>
								</div>

								<div class="clear_mayor"></div>
							<?php
							}
						?>

						<!-- OTROS SERVICIOS TARIFAS REALES -->
						<?php
							$otras_tarifas = $c_mostrar_cotizacion_paquete->obtener_otros_servicios_x_ciudad_real_cotizacion($idciudad_origen, 1);
							if(count($otras_tarifas) > 0)
							{
							?>
								<div class="columns fifteen">
									<div class="columns five" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">PRECIOS DE OTROS SERVICIOS</div>
									<div class="clear"></div>
									<table style="width:100%; font-size:8pt; margin-left:3%;" class="tabla-precios" >
										<tr style="background-color:#10689b; color:#FFFFFF;">
											<th rowspan="2">DETALLE</th>
											<th colspan="3">PRECIOS</th>
											<th rowspan="2" colspan="2" style="width:100px !important; max-width:100px !important;">PAXS</th>
											<th rowspan="2" colspan="2" style="width:100px !important; max-width:100px !important;">ADJUNTOS</th>
										</tr>
										<tr style="background-color:#10689b; color:#FFFFFF;">
											<th>ADULTO</th>
											<th>MENOR</th>
											<th>INFANTE</th>
										</tr>
										<?php
											for ($i=0; $i < count($otras_tarifas); $i++) 
											{ 
												$tarifa_otros 	= $otras_tarifas[$i];
												$idotra_tarifa 	= $tarifa_otros['idtarifa_otros'];
												
												$total_adt = $tarifa_otros['precio_adulto'];
												$total_cnn = $tarifa_otros['precio_menor'];
												$total_inf = $tarifa_otros['precio_infante'];

												/*
												* AGREGA PAIS AL ARRAY SI HUBIESE
												*/
												$pais 							= $c_mostrar_cotizacion_paquete->obtener_pais(strtoupper($tarifa_otros['ciudad']));
												$array_paises[$pais['idpais']] 	= $pais['nombre_pais'];
												/***/

												if($tarifa_otros['increment_fee'] == 'P')
												{
													$fee 		= 1+($tarifa_otros['fee'] / 100);
													$total_adt 	*= $fee;
													$total_cnn 	*= $fee;
													$total_inf 	*= $fee;
												}
												else
												{
													$fee = $tarifa_otros['fee'];
													if($total_adt > 0)
														$total_adt += $fee;
													else
														$total_adt = 0;
													if($total_cnn > 0)
														$total_cnn += $fee;
													else
														$total_cnn = 0;
													if($total_inf > 0)
														$total_inf += $fee;
													else
														$total_inf = 0;
												}

												if($tarifa_otros['increment_factura'] == 'P')
												{
													$iva 		= 1+($tarifa_otros['factura'] / 100);
													$total_adt 	*= $iva;
													$total_cnn 	*= $iva;
													$total_inf 	*= $iva;
												}
												else
												{
													$iva = $tarifa_otros['factura'];
													if($total_adt > 0)
														$total_adt += $iva;
													else
														$total_adt = 0;
													if($total_cnn > 0)
														$total_cnn += $iva;
													else
														$total_cnn = 0;
													if($total_inf > 0)
														$total_inf += $iva;
													else
														$total_inf = 0;
												}

												if($tarifa_otros['moneda'] == 'B')
												{
													$tc 		= $tarifa_otros['tipo_cambio'];
													$total_adt /= $tc;
													$total_cnn /= $tc;
													$total_inf /= $tc;
												}

												$nombre_servicio = $c_mostrar_cotizacion_paquete->obtener_nombre_servicio($tarifa_otros['idotro_servicio']);
										?>
										<tr>
											<td><?php echo strtoupper($nombre_servicio . ' ' . $tarifa_otros['descripcion'] . ' (' . $tarifa_otros['codigo'] . ')'); ?></td>
											<td style="text-align:right;">
												<?php if($total_adt>0) echo ceil($total_adt); else echo '-'; ?>
											</td>
											<td style="text-align:right;">
												<?php if($total_cnn>0) echo ceil($total_cnn); else echo '-'; ?>
											</td>
											<td style="text-align:right;">
												<?php if($total_inf>0) echo ceil($total_inf); else echo '-'; ?>
											</td>
											<td id="paxs<?php echo $tarifa_otros['idtarifa_otros']; ?>">
												<?php
													$paxs = $c_mostrar_cotizacion->obtener_paxs_segun_servicio($tarifa_otros['idtarifa_otros']);
													for ($a=0; $a < count($paxs); $a++)
													{ 
														$pax = $paxs[$a];
														$id  = $pax['idpax'] . '_' . $tarifa_otros['idtarifa_otros'];

														switch ($pax['tipo_precio']) 
														{
															case 1: $tar='ADT'; break;
															case 2: $tar='CNN'; break;
															case 3: $tar='INF'; break;
															default:$tar='N/N'; break;
														}

													?>
														<div id="paxs<?php echo $id; ?>" onmouseover="$('#dels<?php echo $id; ?>').show();" onmouseout="$('#dels<?php echo $id; ?>').hide();">
															<?php echo $pax['nombre_cliente'] . $pax['apellido_cliente'] . ' (' . $tar . ')'; ?>
															<img id="dels<?php echo $id; ?>" src="images/userminus32.png" style="width:15px; height:15px; cursor:pointer; display:none;" title="Quitar Pasajero" onclick="QuitarPaxServicio(<?php echo $pax['idpax']; ?>, <?php echo $tarifa_otros['idtarifa_otros']; ?>, 's');" />
														</div>
													<?php
													}
												?>
											</td>
											<td style="width:17px;">
												<img id="adds<?php echo $tarifa_otros['idtarifa_otros']; ?>" src="images/userplus32.png" style="width:15px; height:15px; cursor:pointer;" title="Añadir Pasajero" onclick="AgregarPaxAlServicioBusqueda(<?php echo $tarifa_otros['idtarifa_otros']; ?>, 's', '<?php echo $nombre_servicio; ?>');" />
											</td>
											<td id="aadjs<?php echo $tarifa_otros['idtarifa_otros']; ?>">
												<?php
													$adjuntos = $c_mostrar_cotizacion->mostrar_adjuntos_por_servicio($tarifa_otros['idtarifa_otros']);
													for ($x=0; $x < count($adjuntos); $x++) 
													{ 
														$adjunto 	= $adjuntos[$x];
														$id 		= $adjunto['idarchivo_adjunto'];
														if($adjunto['is_visible'] == 0)
															$color = '#FF3300';
														else
															$color = '#999999';
													?>
														<div id="adjs<?php echo $id; ?>" onmouseover="$('#deladjs<?php echo $id; ?>').show();" onmouseout="$('#deladjs<?php echo $id; ?>').hide();" >
															<a style="color:<?php echo $color; ?>;" href="<?php echo $ruta_adjuntos . $adjunto['nombre']; ?>" target="_blank"><?php echo substr($adjunto['descripcion'], 0, 15); ?></a>
															<img id="deladjs<?php echo $id; ?>" src="images/userminus32.png" style="width:15px; height:15px; cursor:pointer; display:none;" title="Eliminar Archivo" onclick="eliminar_adjunto_x_servicio(<?php echo $tarifa_otros['idtarifa_otros']; ?>, 's', '<?php echo $id; ?>');" />
														</div>
													<?php
													}
												?>
											</td>
											<td style="width:17px;">
												<img id="addas<?php echo $tarifa_otros['idtarifa_otros']; ?>" src="images/upload32.png" style="width:15px; height:15px; cursor:pointer;" title="Añadir Archivo" onclick="mostrar_url_popup('cargar_archivo_adjunto_por_servicio.php?s=<?php echo $tarifa_otros['idtarifa_otros']; ?>&t=s', 200);" />
											</td>
										</tr>
										<?php
											}
										?>
									</table>
								</div>

								<div class="clear_mayor"></div>
							<?php
							}
						?>

						<!-- BOLETOS AEREOS TARIFAS REALES -->
						<?php
							if(count($opciones_aereo) > 0)
							{
							?>
								<div class="columns fifteen">
									<div class="columns five" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">BOLETOS AEREOS</div>
									<div class="clear"></div>
									<?php
										for ($i=0; $i < count($opciones_aereo); $i++) 
										{ 
											$opcion_aereo 	= $opciones_aereo[$i];
											$idopaereo 		= $opcion_aereo['idopcion_boleto'];
										?>
											<table style="width:70%; margin-left:3%;">
												<tr>
													<td style="width:5%;">
														<strong>OPCION :</strong>
													</td>
													<td style="width:30%;">
														<?php echo strtoupper($opcion_aereo['descripcion']); ?>
													</td>
													<td style="width:5%;">
														<strong>SALE :</strong>
													</td>
													<td style="width:30%;">
														<?php echo strtoupper($opcion_aereo['salida']); ?>
													</td>
													<td></td>
												</tr>
											</table>
											<table style="width:100%; font-size:8pt; margin-left:3%;" class="tabla-precios" >
												<tr style="background-color:#10689b; color:#FFFFFF;">
													<th rowspan="2">LINEA AEREA</th>
													<th rowspan="2">RUTA</th>
													<th colspan="3">PRECIOS</th>
													<th rowspan="2" colspan="2" style="width:100px !important; max-width:100px !important;">PAXS</th>
													<th rowspan="2" colspan="2" style="width:100px !important; max-width:100px !important;">ADJUNTOS</th>
												</tr>
												<tr style="background-color:#10689b; color:#FFFFFF;">
													<th>ADULTO</th>
													<th>MENOR</th>
													<th>INFANTE</th>
												</tr>
												<?php
													$boletos = $c_mostrar_cotizacion_paquete->obtener_boletos_x_opcion_aereo_real_cotizacion($idopaereo, 1);
													for ($j=0; $j < count($boletos); $j++) 
													{ 
														$boleto_cotizado 	= $boletos[$j];
														$idboleto 			= $boleto_cotizado['idboleto_cotizado'];

														/*
														* AGREGA PAIS AL ARRAY SI HUBIESE
														*/
														$a_ruta = explode('/', $boleto_cotizado['ruta']);
														foreach ($a_ruta as $key => $value) 
														{
															$pais 							= $c_mostrar_cotizacion_paquete->obtener_pais(strtoupper($value));
															$array_paises[$pais['idpais']] 	= $pais['nombre_pais'];
														}
														/***/
														
														$total_adt = $boleto_cotizado['precio_adulto'];
														$total_cnn = $boleto_cotizado['precio_menor'];
														$total_inf = $boleto_cotizado['precio_infante'];

														if($boleto_cotizado['increment_fee'] == 'P')
														{
															$fee 		= 1+($boleto_cotizado['fee'] / 100);
															$total_adt 	*= $fee;
															$total_cnn 	*= $fee;
															$total_inf 	*= $fee;
														}
														else
														{
															$fee = $boleto_cotizado['fee'];
															if($total_adt > 0)
																$total_adt += $fee;
															else
																$total_adt = 0;
															if($total_cnn > 0)
																$total_cnn += $fee;
															else
																$total_cnn = 0;
															if($total_inf > 0)
																$total_inf += $fee;
															else
																$total_inf = 0;
														}

														if($boleto_cotizado['increment_factura'] == 'P')
														{
															$iva 		= 1+($boleto_cotizado['factura'] / 100);
															$total_adt 	*= $iva;
															$total_cnn 	*= $iva;
															$total_inf 	*= $iva;
														}
														else
														{
															$iva = $boleto_cotizado['factura'];
															if($total_adt > 0)
																$total_adt += $iva;
															else
																$total_adt = 0;
															if($total_cnn > 0)
																$total_cnn += $iva;
															else
																$total_cnn = 0;
															if($total_inf > 0)
																$total_inf += $iva;
															else
																$total_inf = 0;
														}

														if($boleto_cotizado['moneda'] == 'B')
														{
															$tc 		= $boleto_cotizado['tipo_cambio'];
															$total_adt /= $tc;
															$total_cnn /= $tc;
															$total_inf /= $tc;
														}

														$total_bol_adt += $total_adt;
														$total_bol_cnn += $total_cnn;
														$total_bol_inf += $total_inf;
												?>
														<tr>
															<td><?php echo strtoupper($c_mostrar_cotizacion_paquete->obtener_linea_aerea($boleto_cotizado['idlineas_aereas']) . ' (' . $boleto_cotizado['codigo_reserva'] . ')'); ?></td>
															<td><?php echo strtoupper($boleto_cotizado['ruta']); ?></td>
															<td style="text-align:right;"><?php if($total_adt>0) echo ceil($total_adt); else echo '-'; ?></td>
															<td style="text-align:right;"><?php if($total_cnn>0) echo ceil($total_cnn); else echo '-'; ?></td>
															<td style="text-align:right;"><?php if($total_inf>0) echo ceil($total_inf); else echo '-'; ?></td>
															<td id="paxb<?php echo $boleto_cotizado['idboleto_cotizado']; ?>" >
																<?php
																	$paxs = $c_mostrar_cotizacion->obtener_paxs_segun_boleto($boleto_cotizado['idboleto_cotizado']);
																	for ($a=0; $a < count($paxs); $a++) 
																	{ 
																		$pax = $paxs[$a];
																		$id  = $pax['idpax'] . '_' . $boleto_cotizado['idboleto_cotizado'];
																		switch ($pax['tipo_precio']) 
																		{
																			case 1: $tar='ADT'; break;
																			case 2: $tar='CNN'; break;
																			case 3: $tar='INF'; break;
																			default:$tar='N/N'; break;
																		}
																		?>
																			<div id="paxb<?php echo $id; ?>" onmouseover="$('#delb<?php echo $id; ?>').show();" onmouseout="$('#delb<?php echo $id; ?>').hide();">
																				<?php echo $pax['nombre_cliente'] . $pax['apellido_cliente'] . '(' . $tar . ')'; ?>
																				<img id="delb<?php echo $id; ?>" src="images/userminus32.png" style="width:15px; height:15px; cursor:pointer; display:none;" title="Quitar Pasajero" onclick="QuitarPaxServicio(<?php echo $pax['idpax']; ?>, <?php echo $boleto_cotizado['idboleto_cotizado']; ?>, 'b');" />			
																			</div>
																		<?php
																	}
																?>
															</td>
															<td style="width:17px;">
																<img id="addb<?php echo $boleto_cotizado['idboleto_cotizado']; ?>" src="images/userplus32.png" style="width:15px; height:15px; cursor:pointer;" title="Añadir Pasajero" onclick="AgregarPaxAlServicioBusqueda(<?php echo $boleto_cotizado['idboleto_cotizado']; ?>, 'b', '<?php echo $boleto_cotizado['ruta']; ?>');" />
																<img id="addtkt<?php echo $boleto_cotizado['idboleto_cotizado']; ?>" src="images/bag32.png" style="width:15px; height:15px; cursor:pointer;" title="Añadir Ticket" onclick="AbrirEdicionPaxTicket(<?php echo $boleto_cotizado['idboleto_cotizado']; ?>);" />
															</td>
															<td id="aadjb<?php echo $boleto_cotizado['idboleto_cotizado']; ?>">
																<?php
																	$adjuntos = $c_mostrar_cotizacion->mostrar_adjuntos_por_boleto($boleto_cotizado['idboleto_cotizado']);
																	for ($x=0; $x < count($adjuntos); $x++) 
																	{ 
																		$adjunto 	= $adjuntos[$x];
																		$id 		= $adjunto['idarchivo_adjunto'];
																		if($adjunto['is_visible'] == 0)
																			$color = '#FF3300';
																		else
																			$color = '#999999';
																	?>
																		<div id="adjb<?php echo $id; ?>" onmouseover="$('#deladjb<?php echo $id; ?>').show();" onmouseout="$('#deladjb<?php echo $id; ?>').hide();" >
																			<a style="color:<?php echo $color; ?>;" href="<?php echo $ruta_adjuntos . $adjunto['nombre']; ?>" target="_blank"><?php echo substr($adjunto['descripcion'], 0, 15); ?></a>
																			<img id="deladjb<?php echo $id; ?>" src="images/userminus32.png" style="width:15px; height:15px; cursor:pointer; display:none;" title="Eliminar Archivo" onclick="eliminar_adjunto_x_servicio(<?php echo $boleto_cotizado['idboleto_cotizado']; ?>, 'b', '<?php echo $id; ?>');" />
																		</div>
																	<?php
																	}
																?>
															</td>
															<td style="width:17px;">
																<img id="addab<?php echo $boleto_cotizado['idboleto_cotizado']; ?>" src="images/upload32.png" style="width:15px; height:15px; cursor:pointer;" title="Añadir Archivo" onclick="mostrar_url_popup('cargar_archivo_adjunto_por_servicio.php?s=<?php echo $boleto_cotizado['idboleto_cotizado']; ?>&t=b', 200);" />
															</td>
														</tr>
												<?php
													}
												?>
											</table>
										<?php
										}
									?>
								</div>

								<div class="clear_mayor"></div>
							<?php
							}
						?>

					</div>

					<!-- IMPORTANTE -->
					<?php
						if($ciudad['importante'] != '')
						{
						?>
							<div class="columns fifteen">
								<div class="columns five" style="font-weight:bold; color:#FF3300; font-size:12pt; margin:10px;">IMPORTANTE</div>
								<div class="clear"></div>
								<ul class="lista_descripcion">
									<?php
										$importante = explode("\n", $ciudad['importante']);
										for($i=0; $i<count($importante); $i++)
										{
											if($importante[$i] != "")
											{
									?>
												<li>
													<a><?php echo $importante[$i]; ?></a>
												</li>
									<?php
											}
										}
									?>		
								</ul>
							</div>
						<?php
						}
					?>
					

					<!-- DATOS AGENTE POR CIUDAD -->
					<?php
						if($ciudad['datos_agente'] != '')
						{
						?>
							<div class="columns fifteen">
								<div class="columns five" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">DATOS DE AGENTE</div>
								<div class="clear"></div>
								<ul class="lista_descripcion" id="lista_datos_agente_<?php echo $idciudad_origen; ?>">
									<?php
										$datos_agente = explode("\n", $ciudad['datos_agente']);
										for($i=0; $i<count($datos_agente); $i++)
										{
											if($datos_agente[$i] != "")
											{
									?>
												<li>
													<a><?php echo $datos_agente[$i]; ?></a>
												</li>
									<?php
											}
										}
									?>		
								</ul>
							</div>
						<?php
						}
					?>

				</div>
			
			<?php
			}
		?>

		<div class="clear_mayor"></div>

		<!-- INSCRIPCIONES -->
		<?php
			$inscripciones = $c_mostrar_cotizacion_paquete->obtener_inscripciones_de_evento($idcotizacion);
			//echo "<pre>"; print_r($inscripciones); echo "</pre>";
			if(count($inscripciones) > 0)
			{
			?>
				<div class="sixteen columns is_real1" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#F1F1F1; display:none;">
					<div class="columns five" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">PRECIOS DE INSCRIPCIONES</div>
					<div class="clear"></div>
					<table style="width:95%; font-size:8pt; margin-left:3%;" class="tabla-precios" >
						<tr style="background-color:#10689b; color:#FFFFFF;">
							<th>EVENTO</th>
							<th>CATEGORIA</th>
							<th>PRECIO</th>
							<th colspan="2" style="width:100px !important; max-width:100px !important;">PAXS</th>
							<th colspan="2" style="width:100px !important; max-width:100px !important;">ADJUNTOS</th>
						</tr>
						<?php
							for ($i=0; $i < count($inscripciones); $i++) 
							{ 
								$inscripcion 	= $inscripciones[$i];
								$idinscripcion 	= $inscripcion['idinscripcion_evento'];
								
								$total = $inscripcion['precio'];

								if($inscripcion['increment_fee'] == 'P')
								{
									$fee 	= 1+($inscripcion['fee'] / 100);
									$total 	*= $fee;
								}
								else
								{
									$fee = $inscripcion['fee'];
									if($total > 0)
										$total += $fee;
									else
										$total = 0;
								}

								if($inscripcion['increment_factura'] == 'P')
								{
									$iva 	= 1+($inscripcion['factura'] / 100);
									$total 	*= $iva;
								}
								else
								{
									$iva = $inscripcion['factura'];
									if($total > 0)
										$total += $iva;
									else
										$total = 0;
								}

								if($inscripcion['moneda'] == 'B')
								{
									$tc 	= $inscripcion['tipo_cambio'];
									$total 	/= $tc;
								}
						?>
						<tr>
							<td><?php echo strtoupper($inscripcion['nombre_evento'] . ' (' . $inscripcion['sigla_evento'] . ')'); ?></td>
							<td><?php echo strtoupper($inscripcion['categoria']); ?></td>
							<td style="text-align:right;">
								<?php echo ceil($total); ?>
							</td>
							<td id="paxi<?php echo $inscripcion['idinscripcion_evento']; ?>">
								<?php
									$paxs = $c_mostrar_cotizacion->obtener_paxs_segun_inscripcion($inscripcion['idinscripcion_evento']);
									for ($a=0; $a < count($paxs); $a++)
									{ 
										$pax = $paxs[$a];
										$id  = $pax['idpax'] . '_' . $inscripcion['idinscripcion_evento'];
									?>
										<div id="paxi<?php echo $id; ?>" onmouseover="$('#deli<?php echo $id; ?>').show();" onmouseout="$('#deli<?php echo $id; ?>').hide();">
											<?php echo $pax['nombre_cliente'] . $pax['apellido_cliente']; ?>
											<img id="deli<?php echo $id; ?>" src="images/userminus32.png" style="width:15px; height:15px; cursor:pointer; display:none;" title="Quitar Pasajero" onclick="QuitarPaxServicio(<?php echo $pax['idpax']; ?>, <?php echo $inscripcion['idinscripcion_evento']; ?>, 'i');" />
										</div>
									<?php
									}
								?>
							</td>
							<td style="width:17px;">
								<img id="addi<?php echo $inscripcion['idinscripcion_evento']; ?>" src="images/userplus32.png" style="width:15px; height:15px; cursor:pointer;" title="Añadir Pasajero" onclick="AgregarPaxAlServicioBusqueda(<?php echo $inscripcion['idinscripcion_evento']; ?>, 'i', '<?php echo strtoupper($inscripcion['categoria']); ?>');" />
							</td>
							<td id="aadji<?php echo $inscripcion['idinscripcion_evento']; ?>">
								<?php
									$adjuntos = $c_mostrar_cotizacion->mostrar_adjuntos_por_boleto($inscripcion['idinscripcion_evento']);
									for ($x=0; $x < count($adjuntos); $x++) 
									{ 
										$adjunto 	= $adjuntos[$x];
										$id 		= $adjunto['idarchivo_adjunto'];
										if($adjunto['is_visible'] == 0)
											$color = '#FF3300';
										else
											$color = '#999999';
									?>
										<div id="adji<?php echo $id; ?>" onmouseover="$('#deladji<?php echo $id; ?>').show();" onmouseout="$('#deladji<?php echo $id; ?>').hide();" >
											<a style="color:<?php echo $color; ?>;" href="<?php echo $ruta_adjuntos . $adjunto['nombre']; ?>" target="_blank"><?php echo substr($adjunto['descripcion'], 0, 15); ?></a>
											<img id="deladji<?php echo $id; ?>" src="images/userminus32.png" style="width:15px; height:15px; cursor:pointer; display:none;" title="Eliminar Archivo" onclick="eliminar_adjunto_x_servicio(<?php echo $inscripcion['idinscripcion_evento']; ?>, 'i', '<?php echo $id; ?>');" />
										</div>
									<?php
									}
								?>
							</td>
							<td style="width:17px;">
								<img id="addai<?php echo $inscripcion['idinscripcion_evento']; ?>" src="images/upload32.png" style="width:15px; height:15px; cursor:pointer;" title="Añadir Archivo" onclick="mostrar_url_popup('cargar_archivo_adjunto_por_servicio.php?s=<?php echo $inscripcion['idinscripcion_evento']; ?>&t=i', 200);" />
							</td>
						</tr>
						<?php
							}
						?>
					</table>
					<div class="clear_mayor"></div>
				</div>

				<div class="clear_mayor"></div>
			<?php
			}
		?>

		<?php
			if($publicacion['tipo_publicacion'] == 'E')
			{
			?>
				<div class="columns sixteen is_real0" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1;">
					<div class="columns five" style="font-weight:bold; color:#10689b; font-size:12pt; margin:10px;">TABLA DE INSCRIPCIONES</div>
					<div class="clear"></div>
					<?php
						if($publicacion['imagen_primera'] != '')
						{
						?>
							<img src="<?php echo $ruta_imagen . $publicacion['imagen_primera']; ?>" style="width:80%; margin-left:10%;" />
						<?php
						}
						else
						{
						?>
							<ul class="lista_descripcion"><li><a>NO INGRESADO</a></li></ul>
						<?php
						}
					?>
					<div class="clear_mayor"></div>
				</div>
			<?php
			}
		?>
		

	</div>

<!---->

<!-- VISTA DE PASAJEROS PARA AÑADIR Y QUITAR PAXS -->

	<div id="vista_cotizacion_pasajeros" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#F1F1F1; display:none;" class="sixteen columns tab_cotizacion">
		<div class="fifteen columns" style="font-weight:bold; color:#10689b; font-size:14pt; margin:10px; text-align:center;">PASAJEROS</div>

		<div id="lista_de_pasajeros">
		</div>

		<div id="div_loading" style="text-align:center;display:none;"><img src="../images/24.gif"/><br/><span>Cargando...</span></div>

		<input type="button" value="Agregar Pasajeros" style="float:right; margin-right:20px;" onclick="mostrar_buscar_pax();"/>

	</div>

<!---->

<!-- DOCUMENTOS DE VIAJE (POR DEFINIRSE) -->

	<div id="vista_requisitos_de_viaje" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#F1F1F1; display:none;" class="sixteen columns tab_cotizacion">
		<div class="fifteen columns" style="font-weight:bold; color:#10689b; font-size:14pt; margin:10px; text-align:center;">REQUISITOS DE VIAJE</div>
		<div class="clear_mayor"></div>
		<strong>EN DESARROLLO</strong>
	</div>

<!---->

<!-- REQUISITOS DE VIAJE POR PASAJERO -->

	<div id="vista_checklist_requisitos" style="border:1px solid #AAAAAA; border-radius:10px; background-color:#F1F1F1; display:none;" class="sixteen columns tab_cotizacion">
		<div class="fifteen columns" style="font-weight:bold; color:#10689b; font-size:14pt; margin:10px; text-align:center;">CHECKLIST DE REQUISITOS</div>

		<?php
			$a_ids_paises = array();
			foreach ($array_paises as $key => $value) 
			{
				$a_ids_paises[] 	= $key;
			}
		?>

		<input type="hidden" id="paises_en_cotizacion" value="<?php echo implode(',', $a_ids_paises); ?>" />

		<div id="lista_de_pasajeros_checklist">
		</div>

		<div id="div_loading_checklist" style="text-align:center;display:none;"><img src="../images/24.gif"/><br/><span>Cargando...</span></div>

		<input type="button" value="Actualizar" style="float:right; margin-right:20px;" onclick="ObtenerCheckListPax();"/>

		<!--<div class="fifteen columns" style="font-weight:bold; color:#10689b; font-size:10pt; margin:10px;">
			PAISES A TOMAR EN CUENTA
			<img src="images/plus32.png" class="mini det_paises" onclick="$('.det_paises').toggle();" />
			<img src="images/minus32.png" class="mini hidden det_paises" onclick="$('.det_paises').toggle();" />
		</div>-->

		<table style="width:90%; font-size:8pt; margin-left:3%;" class="tabla-precios hidden det_paises">
			<tr style="background-color:#10689b; color:#FFFFFF;">
				<th>N</th>
				<th>Pais</th>
				<th>Reg</th>
			</tr>
			<?php
				$a_idpaises = $c_mostrar_cotizacion_paquete->paises_por_cotizacion($idcotizacion);

				$i = 0;
				foreach ($array_paises as $key => $value) 
				{
				?>
					<tr>
						<td><?php echo ($i+1); ?></td>
						<td><?php echo $value; ?></td>
						<td>
							<input type="checkbox" class="pais_req" value="<?php echo $key; ?>"  
							<?php
								if(in_array($key, $a_idpaises))
								{
								?>
								 	checked
								<?php
								}
							?>
							 />
						</td>
					</tr>
				<?php
					$i++;
				}
			?>
			<tr>
				<td colspan="3">
					<input type="button" value="Actualizar Registro de Paises" />
				</td>
			</tr>
		</table>

		<div class="clear_mayor"></div>
		<?php //echo "<pre>"; print_r($array_paises); echo "</pre>"; ?>
	</div>

<!---->

<div class="clear_mayor"></div>

<!-- OBSERVACIONES DE LA COTIZACION -->

	<div class="columns sixteen" style="margin-left:8px; border:1px solid #AAAAAA; border-radius:10px; background-color:#f1f1f1; padding-top:10px; padding-bottom:10px;">

		<div class="columns two" style="font-weight:bold; color:#10689b;">
			OBSERVACIONES
		</div>
		<div class="columns one" style="font-weight:bold; color:#10689b;">:</div>
		<div class="columns fifteen">
			<ul class="lista_descripcion">
				<?php
					$observaciones = explode("\n", $cotizacion['observacion']);
					for($i=0; $i<count($observaciones); $i++)
					{
						if($observaciones[$i] != "")
						{
					?>
							<li>
								<a><?php echo $observaciones[$i]; ?></a>
							</li>
					<?php
						}
					}
				?>		
			</ul>
		</div>

	</div>

<!---->

<div class="sixteen columns">
	
	<input type="button" value="Imprimir" style="float:right;"  onclick="Imprimir();" />
	
	<a href="editar_cotizacion_paquete.php?idpaquete=<?php echo $idcotizacion; ?>&vista=1" style="float:right; display:none;" class="boton_cotizacion" >
		<input type="button" value="Editar Cotizacion" />	
	</a>
	<a href="editar_cotizacion_paquete.php?idpaquete=<?php echo $idcotizacion; ?>&vista=0" style="float:right;" class="boton_cotizacion" >
		<input type="button" value="Editar Cotizacion" />	
	</a>

	<a href="mostrar_planilla_cotizacion.php?c=<?php echo $idcotizacion; ?>" target="_blank">
		<input type="button" value="Ver Planilla" style="float:right;" />
	</a>
	
	<a href="ver_detalle_pasajeros_de_paquete.php?cotizacion=<?php echo $idcotizacion; ?>&type=p" target="_blank">
		<input type="button" value="Ver Resumen Precios" style="float:right;" />
	</a>

</div>

<div class="clear"></div>

<script type="text/javascript">
	$(function(){

		window.paxss = new Array();

		<?php
			if($cotizacion['estado_cotizacion'] == 1)
			{
			?>
				$('.vista_pagina').html('VISTA DE COTIZACION DE PAQUETE<br/>(EN PROCESO)'); 
			<?php
			}
			if($cotizacion['estado_cotizacion'] == 0)
			{
			?>
				$('.vista_pagina').html('VISTA DE COTIZACION DE PAQUETE<br/><font style="color:#FF3300;">(ANULADO)<br/><?php echo date("d/m/Y H:i:s", strtotime($cotizacion["fecha_cierre"])); ?></font>');
			<?php
			}
			if($cotizacion['estado_cotizacion'] == 2)
			{
			?>
				$('.vista_pagina').html('VISTA DE COTIZACION DE PAQUETE<br/><font style="color:#009629;">(CERRADA CON EXITO)<br/><?php echo date("d/m/Y H:i:s", strtotime($cotizacion["fecha_cierre"])); ?></font>');
			<?php
			}
		?>
		
		ObtenerCostosPax(); 
		ObtenerCheckListPax();
	});	
</script>


<?php include('iu_vista_cotizacion_paquete_impresion.php'); ?>
<?php include('footer.php'); ?>