<?php
	include('../BD/controladoraBD.php');
	include('../entidad/cotizacion.php');
	include('../entidad/paquete.php');
	include('../entidad/imagen.php');
	include('../entidad/archivo_adjunto.php');
	include('../entidad/operadora.php');
	include('../entidad/destino.php');
	include('../entidad/usuarios_paquetes.php');
	include('../entidad/tarifa_hotel.php');
	include('../entidad/tarifa_otros.php');
	include('../entidad/boleto_cotizado.php');
	include('../entidad/segmento_publicado.php');
	include('../entidad/lineas_aereas.php');
	include('../entidad/aeropuerto.php');
	include('../entidad/pqt_otro_servicio.php');
	include('../control/c_crear_cotizacion_paquete.php');

	$idcotizacion = $_GET['cot'];

	$c_crear_cotizacion_paquete = new c_crear_cotizacion_paquete;

	$cotizacion = cotizacion::obtener_info_cotizacion($idcotizacion);
	$cotizacion = $cotizacion[0];

	//echo "<pre>"; print_r($paquete); echo "</pre>";
	$ciudades = $c_crear_cotizacion_paquete->obtener_ciudades_de_salida($idcotizacion);
?>
<link href="lib/tinybox2/style.css" rel="stylesheet">

<div class="container" style="width:1000px !important;" >
	
	<div id="mensaje_pax" class="sixteen columns" style="position:absolute; color:#FFF; padding:10px; font-size:12pt;">
		
	</div>
	<h3><?php echo $cotizacion['titulo']; ?></h3>
	<div class="clear_mayor"></div>
	<div id="tabs_por_ciudades_generico" style="overflow:auto; max-height:550px;" >
		<?php
			for ($z=0; $z < count($ciudades); $z++) 
			{ 
				$ciudad 			= $ciudades[$z];
				$idciudad_origen 	= $ciudad['idciudad_origen_paquete'];
				$nombre_ciudad 		= $ciudad['ciudad'];
			?>
				<!--<input type="hidden" value="<?php echo $nombre_ciudad; ?>" id="nombre_ciudad_<?php echo $z; ?>" />-->
				
				<div class="sixteen columns" style="color:#FFF; border:1px solid #AAA; border-radius:10px; background-color:#10689b; padding:5px; margin-bottom:5px;">
					<a style="cursor:pointer; font-size:10pt; color:#FFF;" onclick="$('#tarifas_generico_mostrar_<?php  echo $z;?>').toggle();" ><?php echo $nombre_ciudad; ?></a>
					<input type="button" value="Copiar Generico" onclick="PasarCiudadGenericoToCotizacion(<?php echo $z; ?>);" style="float:right;" />
				</div>


				<div id="tarifas_generico_<?php echo $z; ?>" style="display:none;">
					<div style="border:1px solid #AAAAAA; border-radius:10px; background-color:#F1F1F1; display:none;" class="sixteen columns tab_ciudad" id="tab_ciudad__indz_" onmouseover="icon_del_ciudad_show(_indz_);" onmouseout="icon_del_ciudad_hide(_indz_);" >

						<div class="fifteen columns" style="height:20px; text-align:right; margin-top:10px;" >
							<img src="images/cross.png" id="btn_del_ciudad__indz_" style="width:15px; height:15px; cursor:pointer; display:none;" onclick="EliminarCiudadOrigen('_idciudadorigen_', '_type_', '_indz_');" />
							<strong id="lbl_del_ciudad__indz_" style="display:none;">Eliminar Ciudad</strong>
						</div>

						<div class="clear_mayor"></div>

						<div class="columns two" style="font-weight:bold; color:#10689b; height:30px;">
							SALIDA :
						</div>
						<div class="columns four" style="height:30px;" onmouseover="revelar_icono_edicion('fecha_salida__idciudadorigen_');" onmouseout="ocultar_icono_edicion('fecha_salida__idciudadorigen_');">
							<span id="fecha_salida__idciudadorigen_">
								<span id="datos_fecha_salida__idciudadorigen_">&nbsp;
									<?php 
										$fecha_salida = strtotime($ciudad["fecha_salida"]);
										if($fecha_salida)
											$fecha_salida = date('d/m/Y', $fecha_salida);
										else
											$fecha_salida = $ciudad["fecha_salida"];
										echo $fecha_salida;
									?>	
								</span>
								<img id="icon_editar_fecha_salida__idciudadorigen_" src="images/pencil.png" onclick="mostrar_edicion('fecha_salida__idciudadorigen_');" class="icon_edit">
							</span>
							<span id="editar_fecha_salida__idciudadorigen_" style="display:none;">
								<input type="text" placeholder="dd/mm/aaaa" name="fecha_salida__idciudadorigen_" class="time_limit" id="valor_fecha_salida__idciudadorigen_" value="<?php echo $fecha_salida; ?>"  style="width:150px;" />
								<img src="images/cross.png" onclick="ocultar_edicion('fecha_salida__idciudadorigen_');" class="icon_close">
								<img src="images/disk.png" onclick="guardar_atributo_x_ciudad_origen('pqt_ciudad_origen_paquete', 'fecha_salida', 'idciudad_origen_paquete', '_idciudadorigen_', 'fecha_salida__idciudadorigen_');" class="icon_save">
							</span>
						</div>

						<input type="hidden" value="<?php echo $ciudad["fecha_salida"]; ?>" id="fecha_salida_ciudad_<?php echo $z; ?>" />

						<div class="columns two" style="font-weight:bold; color:#10689b; height:30px;">
							RETORNO :
						</div>
						<div class="columns four" style="height:30px;" onmouseover="revelar_icono_edicion('fecha_retorno__idciudadorigen_');" onmouseout="ocultar_icono_edicion('fecha_retorno__idciudadorigen_');">
							<span id="fecha_retorno__idciudadorigen_">
								<span id="datos_fecha_retorno__idciudadorigen_">&nbsp;
									<?php 
										$fecha_retorno = strtotime($ciudad["fecha_retorno"]);
										if($fecha_retorno)
											$fecha_retorno = date('d/m/Y', $fecha_retorno);
										else
											$fecha_retorno = $ciudad["fecha_retorno"];
										echo $fecha_retorno;
									?>	
								</span>
								<img id="icon_editar_fecha_retorno__idciudadorigen_" src="images/pencil.png" onclick="mostrar_edicion('fecha_retorno__idciudadorigen_');" class="icon_edit">
							</span>
							<span id="editar_fecha_retorno__idciudadorigen_" style="display:none;">
								<input type="text" placeholder="dd/mm/aaaa" name="fecha_retorno__idciudadorigen_" class="time_limit" id="valor_fecha_retorno__idciudadorigen_" value="<?php echo $fecha_retorno; ?>" style="width:150px;" />
								<img src="images/cross.png" onclick="ocultar_edicion('fecha_retorno__idciudadorigen_');" class="icon_close">
								<img src="images/disk.png" onclick="guardar_atributo_x_ciudad_origen('pqt_ciudad_origen_paquete', 'fecha_retorno', 'idciudad_origen_paquete', '_idciudadorigen_', 'fecha_retorno__idciudadorigen_');" class="icon_save">
							</span>
						</div>

						<input type="hidden" value="<?php echo $ciudad["fecha_retorno"]; ?>" id="fecha_retorno_ciudad_<?php echo $z; ?>" />

						<div class="clear_mayor"></div>

						<!-- PAQUETE INCLUYE -->
						<div class="columns fifteen" onmouseover="revelar_icono_edicion('paquete_incluye__idciudadorigen_');" onmouseout="ocultar_icono_edicion('paquete_incluye__idciudadorigen_');">
							<font style="font-weight:bold; color:#10689b; float:left;">PAQUETE INCLUYE:</font>
							<img id="icon_editar_paquete_incluye__idciudadorigen_" src="images/pencil.png" onclick="mostrar_edicion('paquete_incluye__idciudadorigen_');" class="icon_edit" style="margin-left:20px; float:left;">
							<br/>
							<span id="paquete_incluye__idciudadorigen_">
								<span id="datos_paquete_incluye__idciudadorigen_">
									<ul class="lista_descripcion" id="lista_paquete_incluye__idciudadorigen_">
										<?php
											$paquete_incluye = explode("\n", $ciudad['paquete_incluye']);
											for($i=0; $i<count($paquete_incluye); $i++)
											{
												if($paquete_incluye[$i] != "")
												{
										?>
													<li>
														<a><?php echo $paquete_incluye[$i]; ?></a>
													</li>
										<?php
												}
											}
										?>		
									</ul>
								</span>
							</span>
							<span id="editar_paquete_incluye__idciudadorigen_" style="display:none;">
								<textarea name="paquete_incluye__idciudadorigen_" id="valor_paquete_incluye__idciudadorigen_" style="width:500px;"><?php echo $ciudad["paquete_incluye"]; ?></textarea>
								<img src="images/cross.png" onclick="ocultar_edicion('paquete_incluye__idciudadorigen_');" class="icon_close">
								<img src="images/disk.png" onclick="guardar_atributo_x_ciudad_origen('pqt_ciudad_origen_paquete', 'paquete_incluye', 'idciudad_origen_paquete', '_idciudadorigen_', 'paquete_incluye__idciudadorigen_');" class="icon_save">
							</span>
						</div>

						<div class="clear_mayor"></div>

						<!-- HOTELES -->
						<div class="columns fifteen">
							<font style="font-weight:bold; color:#10689b; float:left;">TARIFAS DE HOTELES:</font>
							<img src="images/plus32.png" class="right pth" onclick="show_hide_tarifa('pth');" title="HOTELES" />
							<img src="images/minus32.png" class="right hidden pth" onclick="show_hide_tarifa('pth');" title="HOTELES" />
							<div class="clear"></div>
							<div class="ppth hidden" >
								<div id="tarifas_hotel__indz_">
									<?php 
										$tarifas_por_destino =  $c_crear_cotizacion_paquete->obtener_destinos_de_hotel($idciudad_origen);
										
										for ($i=0; $i < count($tarifas_por_destino); $i++) 
										{ 
											$ic 					= '_indz__' . $i;
											$tarifa_hotel 			= $tarifas_por_destino[$i];
											$idth 					= $tarifa_hotel['idtarifa_hotel'];
											$items_tarifa 			= $c_crear_cotizacion_paquete->obtener_hoteles_x_destino($idth);
									?>
										<div id="tabla_tarifas_<?php echo $ic; ?>">
											<table width="100%;" id="tarifa_<?php echo $ic; ?>" class="encabezado_tarifa_hotel">
												<tr>
													<td style="width:20%;">
														<strong>DESTINO :</strong>
													</td>
													<td style="width:20%;">
														<input type="text" style="margin: 0; width: 100px;" id="tdi<?php echo $ic; ?>" placeholder="Codigo Ciudad" value="<?php echo strtoupper($tarifa_hotel['destino']); ?>" onchange="avisar_cambio('H', '<?php echo $ic; ?>');" />
													</td>
													<td style="width:20%;">
														<strong>ORDEN DESTINO:</strong>
													</td>
													<td style="width:20%;">
														<input type="text" style="margin: 0; width: 100px;" id="tdord<?php echo $ic; ?>" value="<?php echo ($i+1); ?>" onchange="avisar_cambio('H', '<?php echo $ic; ?>');" />
													</td>
													<td>
														<img src="images/cross.png" style="width:15px; height:15px; cursor:pointer; float:right;" title="Eliminar" onclick="eliminar_tabla_tarifa('<?php echo $ic; ?>');" />
														<input type="hidden" value="0" id="idth_<?php echo $ic; ?>" />
														<input type="hidden" value="_idciudadorigen_" id="idciudad_<?php echo $ic; ?>" />
														<span style="display:none;" id="items_eliminados<?php echo $ic; ?>"></span>
													</td>
												</tr>
											</table>
											<div class="table_hotel">
											<table style="width:100%; font-size:8pt;" id="detalle_tarifa_<?php echo $ic; ?>">
									<?php
											for ($j=0; $j < count($items_tarifa); $j++) 
											{ 
												$item_tarifa 	= $items_tarifa[$j];
												$iditem 		= $item_tarifa['iditem_hotel'];
												$ind 			= $ic . '_' . $j;

												$is_real 		= $item_tarifa['is_real'];

												$cant_noches 	= floor( (strtotime($item_tarifa['fecha_out'] . ' 00:00:00') - strtotime($item_tarifa['fecha_in'] . ' 00:00:00')) / 86400);
												
												$total_sgl 	= $item_tarifa['precio_single'] * $cant_noches;
												$total_dbl 	= $item_tarifa['precio_doble'] * $cant_noches;
												$total_tpl 	= $item_tarifa['precio_triple'] * $cant_noches;
												$total_cpl 	= $item_tarifa['precio_cuadruple'] * $cant_noches;
												$total_cnn 	= $item_tarifa['precio_menor'] * $cant_noches;
												$total_inf 	= $item_tarifa['precio_infante'] * $cant_noches;

												if($item_tarifa['increment_fee'] == 'P')
												{
													$fee 		= 1 + ($item_tarifa['fee']/100);
													$total_sgl 	*= $fee;
													$total_dbl 	*= $fee;
													$total_tpl 	*= $fee;
													$total_cpl 	*= $fee;
													$total_cnn 	*= $fee;
													$total_inf 	*= $fee;
												}
												else
												{
													$fee 		= $item_tarifa['fee'];
													if($item_tarifa['precio_single'] > 0)
														$total_sgl 	+= $fee;
													else
														$total_sgl 	= 0;
													if($item_tarifa['precio_doble'] > 0)
														$total_dbl 	+= $fee;
													else
														$total_dbl 	= 0;
													if($item_tarifa['precio_triple'] > 0)
														$total_tpl 	+= $fee;
													else
														$total_tpl 	= 0;
													if($item_tarifa['precio_cuadruple'] > 0)
														$total_cpl 	+= $fee;
													else
														$total_cpl 	= 0;
													if($item_tarifa['precio_menor'] > 0)
														$total_cnn 	+= $fee;
													else
														$total_cnn 	= 0;
													if($item_tarifa['precio_infante'] > 0)
														$total_inf 	+= $fee;
													else
														$total_inf 	= 0;
												}

												if($item_tarifa['increment_factura'] == 'P')
												{
													$iva 		= 1 + ($item_tarifa['factura']/100);
													$total_sgl 	*= $iva;
													$total_dbl 	*= $iva;
													$total_tpl 	*= $iva;
													$total_cpl 	*= $iva;
													$total_cnn 	*= $iva;
													$total_inf 	*= $iva;
												}
												else
												{
													$iva 		= $item_tarifa['factura'];
													if($item_tarifa['precio_single'] > 0)
														$total_sgl 	+= $iva;
													else
														$total_sgl 	= 0;
													if($item_tarifa['precio_doble'] > 0)
														$total_dbl 	+= $iva;
													else
														$total_dbl 	= 0;
													if($item_tarifa['precio_triple'] > 0)
														$total_tpl 	+= $iva;
													else
														$total_tpl 	= 0;
													if($item_tarifa['precio_cuadruple'] > 0)
														$total_cpl 	+= $iva;
													else
														$total_cpl 	= 0;
													if($item_tarifa['precio_menor'] > 0)
														$total_cnn 	+= $iva;
													else
														$total_cnn 	= 0;
													if($item_tarifa['precio_infante'] > 0)
														$total_inf 	+= $iva;
													else
														$total_inf 	= 0;
												}
									?>
												<tr class="item_tarifa_hotel_<?php echo $ic; ?> is_real<?php echo $is_real; ?>" id="item_<?php echo $ind; ?>" <?php if($is_real==1) { ?> style="display:none;" <?php } ?> >
													<td class="thirteen columns" style="padding-top:10px;" >
														<div class="one columns">
															<label style="color:#10689B;">HOTEL:</label>
														</div>
														<div class="three columns">
															<input type="text" placeholder="OBLIGATORIO" value="<?php echo $item_tarifa['nombre_hotel']; ?>" onkeyup="buscar_hoteles('<?php echo $ind; ?>');" id="hotel_<?php echo $ind; ?>" style="margin:0px;" />
															<input type="hidden" id="iidhot_<?php echo $ind; ?>" value="<?php echo $item_tarifa['idhotel']; ?>" onchange="avisar_cambio('H', '<?php echo $ind; ?>');" />				
														</div>
														<div class="one columns">
															<label style="color:#10689B;">IN:</label>
														</div>
														<div class="two columns">
															<input type="text" id="tin<?php echo $ind; ?>" class="time_limit" value="<?php echo date('d/m/Y', strtotime($item_tarifa['fecha_in'] . ' 00:00:00')); ?>" style="width:75%; margin:0px;" placeholder="dd/mm/aaaa" onchange="avisar_cambio('H', '<?php echo $ic; ?>'); obtener_totales_hotel('<?php echo $ind; ?>');" />
														</div>
														<div class="one columns">
															<label style="color:#10689B;">OUT:</label>
														</div>
														<div class="two columns">
															<input type="text" id="tou<?php echo $ind; ?>" class="time_limit" value="<?php echo date('d/m/Y', strtotime($item_tarifa['fecha_out'] . ' 00:00:00')); ?>" style="width:75%; margin:0px;" placeholder="dd/mm/aaaa" onchange="avisar_cambio('H', '<?php echo $ic; ?>'); obtener_totales_hotel('<?php echo $ind; ?>');" />
														</div>
														<div class="one columns">
															<label style="color:#10689B;">NOCHES:</label>
														</div>
														<div class="one columns">
															<label id="cantnoches<?php echo $ind; ?>"><?php echo $cant_noches; ?></label>
														</div>
														<div class="clear_mayor"></div>
														<div class="two columns">
															<label>IDENTIFICADOR</label>
															<input type="text" placeholder="OPCIONAL" value="<?php echo $item_tarifa['identificador']; ?>" id="iide_<?php echo $ind; ?>" style="margin:0px;" onchange="avisar_cambio('H', '<?php echo $ic; ?>');" />
															<label>TIME LIMIT</label>
															<input type="text" placeholder="dd/mm/aaaa" class="time_limit" id="itmlm_<?php echo $ind; ?>" value="<?php echo date('d/m/Y', strtotime($item_tarifa['time_limit'])); ?>" style="width:75%; margin:0px;" onchange="avisar_cambio('H', '<?php echo $ic; ?>');" />
														</div>
														<div class="one columns">
															<div id="busqueda_hotel_<?php echo $ind; ?>" style="display:none; position:absolute; width:350px; height:200px; z-index:999999; background-color:#FFFFFF; border:1px solid #AAAAAA;">
																<img src="images/cancel.png" style="width:15px; height:15px; margin-left:330px; margin-top:2px; position:absolute;" onclick="$('#busqueda_hotel_<?php echo $ind; ?>').hide();" />
																<div id="panel_busqueda_hotel_<?php echo $ind; ?>" style="width:350px; height:200px; overflow:auto;">
																</div>
																&nbsp;
															</div>
															<label>PRECIOS</label>
															<input type="text" id="isgl<?php echo $ind; ?>" value="<?php echo $item_tarifa['precio_single']; ?>" placeholder="SGL" style="margin:0px;" onblur="obtener_total_hotel('<?php echo $ind; ?>', 'sgl');" onchange="avisar_cambio('H', '<?php echo $ic; ?>');" />
															<input type="text" id="idbl<?php echo $ind; ?>" value="<?php echo $item_tarifa['precio_doble']; ?>" placeholder="DBL" style="margin:0px;" onblur="obtener_total_hotel('<?php echo $ind; ?>', 'dbl');" onchange="avisar_cambio('H', '<?php echo $ic; ?>');" />
															<input type="text" id="itpl<?php echo $ind; ?>" value="<?php echo $item_tarifa['precio_triple']; ?>" placeholder="TPL" style="margin:0px;" onblur="obtener_total_hotel('<?php echo $ind; ?>', 'tpl');" onchange="avisar_cambio('H', '<?php echo $ic; ?>');" />
															<input type="text" id="icdpl<?php echo $ind; ?>" value="<?php echo $item_tarifa['precio_cuadruple']; ?>" placeholder="CPL" style="margin:0px;" onblur="obtener_total_hotel('<?php echo $ind; ?>', 'cdpl');" onchange="avisar_cambio('H', '<?php echo $ic; ?>');" />
														</div>
														<div class="one columns">
															<label>&nbsp;</label>
															<input type="text" id="imnr<?php echo $ind; ?>" value="<?php echo $item_tarifa['precio_menor']; ?>" placeholder="CNN" style="margin:0px;" onblur="obtener_total_hotel('<?php echo $ind; ?>', 'mnr');" onchange="avisar_cambio('H', '<?php echo $ic; ?>');" />
															<input type="text" id="iinf<?php echo $ind; ?>" value="<?php echo $item_tarifa['precio_infante']; ?>" placeholder="INF" style="margin:0px;" onblur="obtener_total_hotel('<?php echo $ind; ?>', 'inf');" onchange="avisar_cambio('H', '<?php echo $ic; ?>');" />	
															<input type="text" id="icms<?php echo $ind; ?>" value="<?php echo $item_tarifa['fee']; ?>" placeholder="FEE" style="margin:0px;" onblur="obtener_totales_hotel('<?php echo $ind; ?>');" onchange="avisar_cambio('H', '<?php echo $ic; ?>');" class="pc_<?php echo $ic; ?>" />
															<input type="text" id="iiva<?php echo $ind; ?>" value="<?php echo $item_tarifa['factura']; ?>" placeholder="IVA" style="margin:0px;" onblur="obtener_totales_hotel('<?php echo $ind; ?>');" onchange="avisar_cambio('H', '<?php echo $ic; ?>');" />
														</div>
														<div class="three columns">
															<label>ALIMENTACION</label>
															<textarea id="ialm<?php echo $ind; ?>" class="mytextarea" onchange="avisar_cambio('H', '<?php echo $ic; ?>');" ><?php echo strtoupper($item_tarifa['alimentacion']); ?></textarea>
															<label>OBS</label>
															<textarea id="iiex<?php echo $ind; ?>" class="mytextarea" onchange="avisar_cambio('H', '<?php echo $ic; ?>');" ><?php echo strtoupper($item_tarifa['info_extra']); ?></textarea>
														</div>
														<div class="three columns">
															<label>LINK</label>
															<input type="text" id="ilnk<?php echo $ind; ?>" value="<?php echo $item_tarifa['link']; ?>" onchange="avisar_cambio('H', '<?php echo $ic; ?>');" />
															<label>OBS INT</label>
															<textarea id="iobs<?php echo $ind; ?>" class="mytextarea" onchange="avisar_cambio('H', '<?php echo $ic; ?>');" ><?php echo strtoupper($item_tarifa['observacion']); ?></textarea>	
														</div>
														<div class="one columns">
															<label>SGL:</label>
															<label>DBL:</label>
															<label>TPL:</label>
															<label>CPL:</label>
															<label>CNN:</label>
															<label>INF:</label>
														</div>
														<div class="one columns">
															<label id="tot-sgl<?php echo $ind; ?>"><?php echo ceil($total_sgl); ?></label>
															<label id="tot-dbl<?php echo $ind; ?>"><?php echo ceil($total_dbl); ?></label>
															<label id="tot-tpl<?php echo $ind; ?>"><?php echo ceil($total_tpl); ?></label>
															<label id="tot-cdpl<?php echo $ind; ?>"><?php echo ceil($total_cpl); ?></label>
															<label id="tot-mnr<?php echo $ind; ?>"><?php echo ceil($total_cnn); ?></label>
															<label id="tot-inf<?php echo $ind; ?>"><?php echo ceil($total_inf); ?></label>
														</div>
														<div class="clear"></div>
														<div class="two columns">
															<strong>FEE:</strong><input type="radio" name="apfee<?php echo $ind; ?>" value="M" onchange="obtener_totales_hotel('<?php echo $ind; ?>');" <?php if($item_tarifa['increment_fee'] == 'M') echo 'Checked'; ?> />$<input type="radio" name="apfee<?php echo $ind; ?>" value="P" onchange="obtener_totales_hotel('<?php echo $ind; ?>');" <?php if($item_tarifa['increment_fee'] == 'P') echo 'Checked'; ?> />%
														</div>
														<div class="two columns">
															<strong>IVA:</strong><input type="radio" name="apiva<?php echo $ind; ?>" value="M" onchange="obtener_totales_hotel('<?php echo $ind; ?>');" <?php if($item_tarifa['increment_factura'] == 'M') echo 'Checked'; ?> />$<input type="radio" name="apiva<?php echo $ind; ?>" value="P" onchange="obtener_totales_hotel('<?php echo $ind; ?>');" <?php if($item_tarifa['increment_factura'] == 'P') echo 'Checked'; ?>  />%
														</div>
														<hr/>
													</td>
													<td style="vertical-align:top;">
														<center>
															<br/><img src="images/cancel.png" style="width:10px; height:10px; cursor:pointer;" title="Eliminar" onclick="eliminar_item_hotel('<?php echo $ind; ?>');" />
															<br/><br/>Act<br/>
															<input type="checkbox" id="est<?php echo $ind; ?>" <?php if($item_tarifa['estado'] == '1') echo 'checked'; ?> />
															<input type="hidden" id="hot_is_real<?php echo $ind; ?>" value="<?php echo $is_real; ?>" />
															<input type="hidden" id="iditem<?php echo $ind; ?>" value="0" />
														</center>
													</td>
												</tr>
									<?php
											}
									?>
											</table>
											</div>
											<a class="mylink is_real0" onclick="agregar_item_hotel('<?php echo $ic; ?>');">
												<img src="images/add50.png" class="mini" />
												Nuevo Hotel
											</a>
											<a class="mylink is_real1" style="display:none;" onclick="agregar_item_hotel_real('<?php echo $ic; ?>');">
												<img src="images/add50.png" class="mini" />
												Nuevo Hotel(Reserva)
											</a>
											<a class="mylink" onclick="guardar_tarifa('<?php echo $ic; ?>');">
												<img src="images/save32.png" class="mini" />
												Guardar Destino
											</a>
											<input type="hidden" value="<?php echo $j; ?>" id="c_hoteles_<?php echo $ic; ?>" />
											<hr/>
											</div>
									<?
										}				
									?>
								</div>
								<input type="hidden" value="<?php echo $i; ?>" id="c_destinos__indz_" />
								<div>
									<a class="mylinkright is_real0" onclick="agregar_tabla_tarifa('_idciudadorigen_', '_indz_');">
										<img src="images/add50.png" class="mini" />
										Nuevo Destino
									</a>
									<a class="mylinkright" onclick="editar_combinacion_hoteles(_idciudadorigen_);">
										<img src="images/viewing.png" class="mini" />
										Combinacion de Hoteles
									</a>
								</div>
							</div>
						</div>

						<div class="clear_mayor"></div>
						<div class="clear_mayor"></div>
						<div class="clear_mayor"></div>
						<div class="clear_mayor"></div>

						<!-- OTROS SERVICIOS -->
						<div class="columns fifteen">
							<font style="font-weight:bold; color:#10689b; float:left;">OTROS SERVICIOS:</font>
							<img src="images/plus32.png" class="right pto" onclick="show_hide_tarifa('pto');" title="OTROS SERVICIOS" />
							<img src="images/minus32.png" class="right hidden pto" onclick="show_hide_tarifa('pto');" title="OTROS SERVICIOS" />
							<div class="clear"></div>
							<div class="ppto hidden" >
								<div id="otros_servicios__indz_">
									<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" id="detalle_otras_tarifas__indz_" class="con-borde" >
										<tr style="background-color:#10689b; color:#FFFFFF;">
											<th style="width:27%;">DETALLE</th>
											<th style="width:15%;">DESDE / HASTA</th>
											<th style="width:9%;">PRECIOS</th>
											<th style="width:18%;">FEE / IVA / TIME LIMIT</th>
											<th style="width:20%;">MONEDA / OBS</th>
											<th style="width:8%;">TOTALES</th>
											<th></th>
											<th>ACT</th>
										</tr>
										<?php
											$otras_tarifas = $c_crear_cotizacion_paquete->obtener_otros_servicios_x_ciudad($idciudad_origen);
											for ($i=0; $i < count($otras_tarifas); $i++) 
											{ 
												$ic 			= '_indz__' . $i;

												$tarifa_otros 	= $otras_tarifas[$i];
												$idotra_tarifa 	= $tarifa_otros['idtarifa_otros'];
												$is_real 		= $tarifa_otros['is_real'];
												
												$total_adt = $tarifa_otros['precio_adulto'];
												$total_cnn = $tarifa_otros['precio_menor'];
												$total_inf = $tarifa_otros['precio_infante'];

												if($tarifa_otros['increment_fee'] == 'P')
												{
													$fee 		= 1+($tarifa_otros['fee'] / 100);
													$total_adt 	*= $fee;
													$total_cnn 	*= $fee;
													$total_inf 	*= $fee;
												}
												else
												{
													$fee = $tarifa_otros['fee'];
													if($total_adt > 0)
														$total_adt += $fee;
													else
														$total_adt = 0;
													if($total_cnn > 0)
														$total_cnn += $fee;
													else
														$total_cnn = 0;
													if($total_inf > 0)
														$total_inf += $fee;
													else
														$total_inf = 0;
												}

												if($tarifa_otros['increment_factura'] == 'P')
												{
													$iva 		= 1+($tarifa_otros['factura'] / 100);
													$total_adt 	*= $iva;
													$total_cnn 	*= $iva;
													$total_inf 	*= $iva;
												}
												else
												{
													$iva = $tarifa_otros['factura'];
													if($total_adt > 0)
														$total_adt += $iva;
													else
														$total_adt = 0;
													if($total_cnn > 0)
														$total_cnn += $iva;
													else
														$total_cnn = 0;
													if($total_inf > 0)
														$total_inf += $iva;
													else
														$total_inf = 0;
												}
										?>
										<tr class="tarifa_otros_servicios__indz_ is_real<?php echo $is_real; ?>" id="otroservicio_<?php echo $ic; ?>" <?php if($is_real==1) { ?> style="display:none;" <?php } ?> >
											<td style="vertical-align:middle; padding-top:10px; padding-bottom:10px;">
												<?php
													$nombre_servicio = $c_crear_cotizacion_paquete->obtener_nombre_servicio($tarifa_otros['idotro_servicio']);
												?>
												CODIGO :<input type="text" placeholder="OPCIONAL" value="<?php echo $tarifa_otros['codigo']; ?>" id="icodotr<?php echo $ic; ?>" style="width:70%; margin:0;" />
												CONCPT :<input type="text" placeholder="OBLIGATORIO" value="<?php echo $nombre_servicio; ?>" id="inser<?php echo $ic; ?>" style="width:70%; margin:0;" onkeyup="buscar_otros_servicios('<?php echo $ic; ?>');" />*
												DESCRIP :<input type="text" placeholder="OPCIONAL" value="<?php echo $tarifa_otros['descripcion']; ?>" id="idesotr<?php echo $ic; ?>" style="width:70%; margin:0;" />
												<input type="hidden" value="<?php echo $tarifa_otros['idotro_servicio']; ?>" id="iidsr<?php echo $ic; ?>" />
												<input type="hidden" value="_idciudadorigen_" id="iidscd<?php echo $ic; ?>" />
											</td>
											<td style="vertical-align:middle;">
												<input type="text" id="iociudad<?php echo $ic; ?>" value="<?php echo $tarifa_otros['ciudad']; ?>" placeholder="CIUDAD (IATA)" style="margin: 0; width: 70%;" />
												<input type="text" id="iofd<?php echo $ic; ?>" placeholder="dd/mm/aaaa" value="<?php echo date('d/m/Y', strtotime($tarifa_otros['fecha_desde'])); ?>" class="time_limit" style="margin: 0; width: 70%;" />
												<input type="text" id="iofh<?php echo $ic; ?>" placeholder="dd/mm/aaaa" value="<?php echo date('d/m/Y', strtotime($tarifa_otros['fecha_hasta'])); ?>" class="time_limit" style="margin: 0; width: 70%;" />
											</td>
											<td style="vertical-align:middle;">
												<div id="busqueda_otro_servicio_<?php echo $ic; ?>" style="display:none; position:absolute; width:350px; height:200px; z-index:999999; background-color:#FFFFFF; border:1px solid #AAAAAA;">
													<img src="images/cancel.png" style="width:15px; height:15px; margin-left:330px; margin-top:2px; position:absolute;" onclick="$('#busqueda_otro_servicio_<?php echo $ic; ?>').hide();" />
													<div id="panel_busqueda_otro_servicio_<?php echo $ic; ?>" style="width:350px; height:200px; overflow:auto;">
													</div>
													&nbsp;
												</div>
												<input type="text" id="inadt<?php echo $ic; ?>" placeholder="ADT" value="<?php echo $tarifa_otros['precio_adulto']; ?>" style="margin: 0; width: 95%;" onblur="obtener_total_servicio('<?php echo $ic; ?>','adt');" />
												<input type="text" id="inmen<?php echo $ic; ?>" placeholder="CNN" value="<?php echo $tarifa_otros['precio_menor']; ?>" style="margin: 0; width: 95%;" onblur="obtener_total_servicio('<?php echo $ic; ?>','men');" />
												<input type="text" id="ininf<?php echo $ic; ?>" placeholder="INF" value="<?php echo $tarifa_otros['precio_infante']; ?>" style="margin: 0; width: 95%;" onblur="obtener_total_servicio('<?php echo $ic; ?>','inf');" />
											</td>
											<td style="vertical-align:middle;">
												<input type="text" id="iofee<?php echo $ic; ?>" placeholder="FEE" value="<?php echo $tarifa_otros['fee']; ?>" style="margin: 0; width: 25%;" onblur="obtener_totales_servicio('<?php echo $ic; ?>');" />
												<input type="radio" name="sapfee<?php echo $ic; ?>" value="M" <?php if($tarifa_otros['increment_fee'] == 'M') echo 'Checked'; ?> onchange="obtener_totales_servicio('<?php echo $ic; ?>');" />$
												<input type="radio" name="sapfee<?php echo $ic; ?>" value="P" <?php if($tarifa_otros['increment_fee'] == 'P') echo 'Checked'; ?> onchange="obtener_totales_servicio('<?php echo $ic; ?>');" />%
												<input type="text" id="iofac<?php echo $ic; ?>" placeholder="IVA" value="<?php echo $tarifa_otros['factura']; ?>" style="margin: 0; width: 25%;" onblur="obtener_totales_servicio('<?php echo $ic; ?>');" />
												<input type="radio" name="sapiva<?php echo $ic; ?>" value="M" <?php if($tarifa_otros['increment_factura'] == 'M') echo 'Checked'; ?> onchange="obtener_totales_servicio('<?php echo $ic; ?>');" />$
												<input type="radio" name="sapiva<?php echo $ic; ?>" value="P" <?php if($tarifa_otros['increment_factura'] == 'P') echo 'Checked'; ?> onchange="obtener_totales_servicio('<?php echo $ic; ?>');" />%
												<input type="text" id="iotli<?php echo $ic; ?>" placeholder="dd/mm/aaaa" value="<?php echo date('d/m/Y', strtotime($tarifa_otros['time_limit'])); ?>" class="time_limit" style="margin: 0; width: 70%;" />
											</td>
											<td style="vertical-align:middle;">
												<select id="imnda<?php echo $ic; ?>" style="margin: 0; width: 95%;">
													<option value="USD" <?php if($tarifa_otros['moneda'] == 'U') echo 'Selected'; ?> >USD</option>
													<option value="BOB" <?php if($tarifa_otros['moneda'] == 'B') echo 'Selected'; ?> >BOB</option>
												</select>
												<textarea id="ioob<?php echo $ic; ?>" style="margin:0; width:95%; min-height:10px; height:30px;" ><?php echo strtoupper($tarifa_otros['observacion']); ?></textarea>
											</td>
											<td style="vertical-align:middle; line-height:1; font-size:8pt;">
												<span id="tot-oadt<?php echo $ic; ?>"><?php echo ceil($total_adt); ?></span>(ADT)<br/>
												<span id="tot-omen<?php echo $ic; ?>"><?php echo ceil($total_cnn); ?></span>(CNN)<br/>
												<span id="tot-oinf<?php echo $ic; ?>"><?php echo ceil($total_inf); ?></span>(INF)
											</td>
											<td style="vertical-align:middle;">
											</td>
											<td style="vertical-align:middle;">
												<input type="checkbox" id="iest<?php echo $ic; ?>" <?php if($tarifa_otros['estado'] == '1') echo 'checked'; ?> /><br/>
												<img src="images/cancel.png" style="width:10px; height:10px; cursor:pointer;" title="Eliminar" onclick="eliminar_otro_servicio('<?php echo $ic; ?>');" />
												<input type="hidden" id="ser_is_real<?php echo $ic; ?>" value="<?php echo $is_real; ?>" />
												<input type="hidden" id="idotra_tarifa<?php echo $ic; ?>" value="0" />
											</td>
										</tr>
										<?php
											}
										?>
									</table>
									<a class="mylinkright" onclick="guardar_otros_servicios('_indz_');">
										<img src="images/save32.png" class="mini" />
										Guardar Cambios
									</a>
									<a class="mylinkright is_real0" onclick="agregar_otro_servicio('_idciudadorigen_', '_indz_');">
										<img src="images/add50.png" class="mini" />
										Agregar Otro Servicio
									</a>
									<a class="mylinkright is_real1" style="display:none;" onclick="agregar_otro_servicio_real('_idciudadorigen_', '_indz_');">
										<img src="images/add50.png" class="mini" />
										Agregar Servicio (Reserva)
									</a>
									<input type="hidden" id="c_otras_tarifas__indz_" value="<?php echo $i; ?>" />
									<span id="eliminar_otras_tarifas__indz_" style="display:none;"></span>
								</div>
							</div>
						</div>

						<div class="clear_mayor"></div>
						<div class="clear_mayor"></div>
						<div class="clear_mayor"></div>
						<div class="clear_mayor"></div>

						<!-- BOLETOS AEREOS -->
						<div class="columns fifteen">
							<font style="font-weight:bold; color:#10689b; float:left;">BOLETOS AEREOS:</font>
							<img src="images/plus32.png" class="right ptb" onclick="show_hide_tarifa('ptb');" title="BOLETOS AEREOS" />
							<img src="images/minus32.png" class="right hidden ptb" onclick="show_hide_tarifa('ptb');" title="BOLETOS AEREOS" />
							<div class="clear"></div>
							<div class="pptb hidden" >
								<div id="tarifas_aereo__indz_">
									<?php
										$opciones_aereo = $c_crear_cotizacion_paquete->obtener_opcion_boletos_x_ciudad($idciudad_origen);
										
										for ($i=0; $i < count($opciones_aereo); $i++) 
										{ 
											$ic 			= '_indz__' . $i;

											$opcion_aereo 	= $opciones_aereo[$i];
											$idopaereo 		= $opcion_aereo['idopcion_boleto'];
									?>
											<div id="opcionaereo_<?php echo $ic; ?>" style="margin-bottom:15px;">
												<table width="100%;" class="encabezado_tarifa_hotel">
													<tr>
														<td>
															<strong>OPCION :</strong>
														</td>
														<td style="width:30%;">
															<input type="text" style="margin: 0; width: 150px;" id="opcbol<?php echo $ic; ?>" value="<?php echo $opcion_aereo['descripcion']; ?>" />
														</td>
														<td>
															<strong>SALE :</strong>
														</td>
														<td style="width:30%;">
															<input type="text" style="margin: 0; width: 150px;" id="salbol<?php echo $ic; ?>" value="<?php echo $opcion_aereo['salida']; ?>" />
														</td>
														<td>
															<input type="checkbox" id="iestaereo<?php echo $ic; ?>" <?php if($opcion_aereo['estado'] == '1') echo 'checked'; ?> />
															Activo&nbsp;&nbsp;
															<img src="images/cross.png" style="width:15px; height:15px; cursor:pointer;" title="Eliminar" onclick="eliminar_opcion_aereo('<?php echo $ic; ?>');" />
															<input type="hidden" value="0" id="idtb_<?php echo $ic; ?>" />
															<input type="hidden" value="_idciudadorigen_" id="idbcdd<?php echo $ic; ?>" />
														</td>
													</tr>
												</table>
												<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" id="detalle_boletos_cotizados<?php echo $ic; ?>" class="con-borde" >
													<tr style="background-color:#10689b; color:#FFFFFF;">
														<th style="width:10%;">ORD / CODIGO</th>
														<th style="width:22%; line-height:1;">L.A. / RUTA</th>
														<th style="width:10%;">PRECIOS</th>
														<th style="width:18%;">FEE / IVA / TIME LIMIT</th>
														<th style="width:25%;">OBSERVACION</th>
														<th style="width:10%;">TOTALES</th>
														<th></th>
														<th style="width:5%;">ACT</th>
													</tr>
													<?php
														$boletos = $c_crear_cotizacion_paquete->obtener_boletos_x_opcion_aereo($idopaereo);
														for ($j=0; $j < count($boletos); $j++) 
														{ 
															$indbol 			= $ic . '_' . $j;
															$boleto_cotizado 	= $boletos[$j];
															$idboleto 			= $boleto_cotizado['idboleto_cotizado'];
															$is_real 			= $boleto_cotizado['is_real'];
															//$factor   			= (1+($boleto_cotizado['fee']/100))*(1+($boleto_cotizado['factura']/100));
															$total_adt = $boleto_cotizado['precio_adulto'];
															$total_cnn = $boleto_cotizado['precio_menor'];
															$total_inf = $boleto_cotizado['precio_infante'];

															if($boleto_cotizado['increment_fee'] == 'P')
															{
																$fee 		= 1+($boleto_cotizado['fee'] / 100);
																$total_adt 	*= $fee;
																$total_cnn 	*= $fee;
																$total_inf 	*= $fee;
															}
															else
															{
																$fee = $boleto_cotizado['fee'];
																if($total_adt > 0)
																	$total_adt += $fee;
																else
																	$total_adt = 0;
																if($total_cnn > 0)
																	$total_cnn += $fee;
																else
																	$total_cnn = 0;
																if($total_inf > 0)
																	$total_inf += $fee;
																else
																	$total_inf = 0;
															}

															if($boleto_cotizado['increment_factura'] == 'P')
															{
																$iva 		= 1+($boleto_cotizado['factura'] / 100);
																$total_adt 	*= $iva;
																$total_cnn 	*= $iva;
																$total_inf 	*= $iva;
															}
															else
															{
																$iva = $boleto_cotizado['factura'];
																if($total_adt > 0)
																	$total_adt += $iva;
																else
																	$total_adt = 0;
																if($total_cnn > 0)
																	$total_cnn += $iva;
																else
																	$total_cnn = 0;
																if($total_inf > 0)
																	$total_inf += $iva;
																else
																	$total_inf = 0;
															}
													?>
															<tr class="boleto_cotizado<?php echo $ic; ?> is_real<?php echo $is_real; ?>" id="boletocotizado-<?php echo $indbol; ?>" <?php if($is_real==1) { ?> style="display:none;" <?php } ?> >
																<td style="vertical-align:middle;">
																	<input type="text" id="ibord<?php echo $indbol; ?>" value="<?php echo $boleto_cotizado['orden']; ?>" style="margin: 0; width:80%; top;" />	
																	<input type="text" id="icodres<?php echo $indbol; ?>" value="<?php echo $boleto_cotizado['codigo_reserva']; ?>" style="margin: 0; width:95%;" />	
																</td>
																<td style="vertical-align:middle;">
																	<input type="text" placeholder="Linea Aerea" id="ilinaer<?php echo $indbol; ?>" value="<?php echo strtoupper($c_crear_cotizacion_paquete->obtener_linea_aerea($boleto_cotizado['idlineas_aereas'])); ?>" style="margin: 0; width:95%;" onkeyup="buscar_lineas_aereas('<?php echo $indbol; ?>');" />
																	<input type="hidden" id="iidlinaer<?php echo $indbol; ?>" value="<?php echo $boleto_cotizado['idlineas_aereas']; ?>" />
																	<input type="text" placeholder="Ruta" id="iruta<?php echo $indbol; ?>" value="<?php echo $boleto_cotizado['ruta']; ?>" style="margin: 0; width:95%;" onkeyup="input_mask(this,'ruta',false);" />	
																</td>
																<td style="vertical-align:middle;">
																	<div id="busqueda_linea_aerea_<?php echo $indbol; ?>" style="display:none; position:absolute; width:350px; height:200px; z-index:999999; background-color:#FFFFFF; border:1px solid #AAAAAA;">
																		<img src="images/cancel.png" style="width:15px; height:15px; margin-left:330px; margin-top:2px; position:absolute;" onclick="$('#busqueda_linea_aerea_<?php echo $indbol; ?>').hide();" />
																		<div id="panel_busqueda_linea_aerea_<?php echo $indbol; ?>" style="width:350px; height:200px; overflow:auto;">
																		</div>
																		&nbsp;
																	</div>
																	<input type="text" id="ibadt<?php echo $indbol; ?>" placeholder="ADT" value="<?php echo $boleto_cotizado['precio_adulto']; ?>" style="margin: 0; width:95%;" onblur="obtener_total_boleto('<?php echo $indbol; ?>','adt');" />	
																	<input type="text" id="ibmen<?php echo $indbol; ?>" placeholder="CNN" value="<?php echo $boleto_cotizado['precio_menor']; ?>" style="margin: 0; width:95%;" onblur="obtener_total_boleto('<?php echo $indbol; ?>','men');" />	
																	<input type="text" id="ibinf<?php echo $indbol; ?>" placeholder="INF" value="<?php echo $boleto_cotizado['precio_infante']; ?>" style="margin: 0; width:95%;" onblur="obtener_total_boleto('<?php echo $indbol; ?>','inf');" />	
																</td>
																<td style="vertical-align:middle;">
																	<input type="text" id="ibfee<?php echo $indbol; ?>" placeholder="FEE" value="<?php echo $boleto_cotizado['fee']; ?>" style="margin: 0; width:25%;" onblur="obtener_totales_boleto('<?php echo $indbol; ?>');" />	
																	<input type="radio" name="bapfee<?php echo $indbol; ?>" value="M" <?php if($boleto_cotizado['increment_fee'] == 'M') echo 'Checked'; ?> onchange="obtener_totales_boleto('<?php echo $indbol; ?>');" />$
																	<input type="radio" name="bapfee<?php echo $indbol; ?>" value="P" <?php if($boleto_cotizado['increment_fee'] == 'P') echo 'Checked'; ?> onchange="obtener_totales_boleto('<?php echo $indbol; ?>');" />%
																	<input type="text" id="ibfac<?php echo $indbol; ?>" placeholder="IVA" value="<?php echo $boleto_cotizado['factura']; ?>" style="margin: 0; width:25%;" onblur="obtener_totales_boleto('<?php echo $indbol; ?>');" />	
																	<input type="radio" name="bapiva<?php echo $indbol; ?>" value="M" <?php if($boleto_cotizado['increment_factura'] == 'M') echo 'Checked'; ?> onchange="obtener_totales_boleto('<?php echo $indbol; ?>');" />$
																	<input type="radio" name="bapiva<?php echo $indbol; ?>" value="P" <?php if($boleto_cotizado['increment_factura'] == 'P') echo 'Checked'; ?> onchange="obtener_totales_boleto('<?php echo $indbol; ?>');" />%
																	<input type="text" id="iotlires<?php echo $indbol; ?>" value="<?php echo date('d/m/Y', strtotime($boleto_cotizado['time_limit'])); ?>" class="time_limit" style="margin: 0; width: 70%;" />
																</td>
																<td style="vertical-align:middle;">
																	<select id="ibmnda<?php echo $indbol; ?>" style="margin: 0; width:95%;">
																		<option value="USD" <?php if($boleto_cotizado['moneda'] == 'U') echo 'Selected'; ?> >USD</option>
																		<option value="BOB" <?php if($boleto_cotizado['moneda'] == 'B') echo 'Selected'; ?> >BOB</option>
																	</select>
																	<textarea id="ibob<?php echo $indbol; ?>" style="margin:0; width:95%; min-height:10px; height:30px;" ><?php echo $boleto_cotizado['observacion']; ?></textarea>
																</td>
																<td style="vertical-align:middle; line-height:1; font-size:8pt;">
																	<span id="tot-adt<?php echo $indbol; ?>"><?php echo ceil($total_adt); ?></span>(ADT)<br/>
																	<span id="tot-men<?php echo $indbol; ?>"><?php echo ceil($total_cnn); ?></span>(CNN)<br/>
																	<span id="tot-inf<?php echo $indbol; ?>"><?php echo ceil($total_inf); ?></span>(INF)
																</td>
																<td style="vertical-align:middle; line-height:1; font-size:8pt;">
																</td>
																<td style="vertical-align:middle;">
																	<input type="checkbox" id="iestres<?php echo $indbol; ?>" <?php if($boleto_cotizado['estado'] == '1') echo 'checked'; ?> /><br/>
																	<img src="images/cancel.png" style="width:10px; height:10px; cursor:pointer;" title="Eliminar" onclick="eliminar_boleto_cotizado('<?php echo $indbol; ?>');" />
																	<input type="hidden" id="bol_is_real<?php echo $indbol; ?>" value="<?php echo $is_real; ?>" />
																	<input type="hidden" id="idboleto_cotizado<?php echo $indbol; ?>" value="0" />
																</td>
															</tr>
													<?php
														}
													?>
												</table>
												<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" id="detalle_itinerario<?php echo $ic; ?>" class="con-borde" >
													<tr style="background-color:#10689b; color:#FFFFFF;">
														<th style="width:3%;">ORD</th>
														<th style="width:5%;">CODIGO</th>
														<th style="width:19%;">LINEA AEREA</th>
														<th style="width:5%;">VUELO</th>
														<th style="width:10%;">FECHA</th>
														<th style="width:20%;">ORIGEN</th>
														<th style="width:8%;">SALE</th>
														<th style="width:20%;">DESTINO</th>
														<th style="width:8%;">LLEGA</th>
														<th style="width:2%;"></th>
													</tr>
													<?php
														$itinerario = $c_crear_cotizacion_paquete->obtener_itinerario_x_opcion_aereo($idopaereo);
														for ($k=0; $k < count($itinerario); $k++) 
														{ 
															$indseg 	= $ic . '_' . $k;
															$segmento 	= $itinerario[$k];
															$idsegmento = $segmento['idsegmento'];

															$linea_aerea 		= $c_crear_cotizacion_paquete->obtener_linea_aerea($segmento['idlineas_aereas']);
															$aeropuerto_sale 	= $c_crear_cotizacion_paquete->obtener_aeropuerto($segmento['origen']);
															$aeropuerto_llega 	= $c_crear_cotizacion_paquete->obtener_aeropuerto($segmento['destino']);
													?>
															<tr class="segmento_publicado<?php echo $ic; ?>" id="segmento-<?php echo $indseg; ?>" >
																<td>
																	<input type="text" id="iiord<?php echo $indseg; ?>" value="<?php echo $segmento['orden']; ?>" style="margin: 0; width: 95%;" />	
																</td>
																<td>
																	<input type="text" id="iicod<?php echo $indseg; ?>" value="<?php echo $segmento['codigo']; ?>" style="margin: 0; width: 95%;" />	
																</td>
																<td>
																	<input type="text" id="iilin<?php echo $indseg; ?>" value="<?php echo $linea_aerea; ?>" style="margin: 0; width: 95%;" class="input_linea_aerea" />	
																</td>
																<td>
																	<input type="text" id="iivue<?php echo $indseg; ?>" value="<?php echo $segmento['nro_vuelo']; ?>" style="margin: 0; width: 95%;" />	
																</td>
																<td>
																	<input type="text" id="iifec<?php echo $indseg; ?>" value="<?php echo date('d/m/Y', strtotime($segmento['fecha'])); ?>" style="margin: 0; width:95%" placeholder="dd/mm/aaaa" onkeyup="input_mask(this,'fecha',false);" />	
																</td>
																<td>
																	<input type="text" id="iiasa<?php echo $indseg; ?>" value="<?php echo $aeropuerto_sale; ?>" style="margin: 0; width: 95%;" class="input_aeropuerto" />
																</td>
																<td>
																	<input type="text" id="iihsa<?php echo $indseg; ?>" value="<?php echo $segmento['hora_sale']; ?>" style="margin: 0; width: 95%;" placeholder="hh:mm" onkeyup="input_mask(this,'hora',false);" />
																</td>
																<td>
																	<input type="text" id="iiall<?php echo $indseg; ?>" value="<?php echo $aeropuerto_llega; ?>" style="margin: 0; width: 95%;" class="input_aeropuerto" />
																</td>
																<td>
																	<input type="text" id="iihll<?php echo $indseg; ?>" value="<?php echo $segmento['hora_llega']; ?>" style="margin: 0; width: 95%;" placeholder="hh:mm" onkeyup="input_mask(this,'hora',false);" />
																</td>
																<td>
																	<img src="images/cancel.png" style="width:10px; height:10px; cursor:pointer;" title="Eliminar" onclick="eliminar_segmento('<?php echo $indseg; ?>');" />
																	<input type="hidden" id="idsegmento<?php echo $indseg; ?>" value="0" />
																</td>
															</tr>
													<?php
														}
													?>
												</table>

												<span style="display:none;" id="boletos_eliminados<?php echo $ic; ?>"></span>
												<span style="display:none;" id="segmentos_eliminados<?php echo $ic; ?>"></span>

												<input type="hidden" id="c_boletos_cotizados<?php echo $ic; ?>" value="<?php echo $j; ?>" />
												<input type="hidden" id="c_segmentos<?php echo $ic; ?>" value="<?php echo $k; ?>" />

												<a class="mylink" onclick="guardar_tarifa_aereo('<?php echo $ic; ?>');">
													<img src="images/save32.png" class="mini" />
													Guardar Cambios
												</a>
												<a class="mylink is_real0" onclick="agregar_segmento_de_reserva('<?php echo $ic; ?>');">
													<img src="images/cloud127.png" class="mini" />
													Obtener de Reserva
												</a>
												<a class="mylink is_real1" style="display:none;" onclick="agregar_segmento_de_reserva_real('<?php echo $ic; ?>');">
													<img src="images/cloud127.png" class="mini" />
													Obtener de Reserva(Real)
												</a>
												<a class="mylink is_real0" onclick="agregar_boleto_cotizado('<?php echo $ic; ?>');">
													<img src="images/add50.png" class="mini" />
													Agregar Tarifa Boleto
												</a>
												<a class="mylink is_real1" style="display:none;" onclick="agregar_boleto_cotizado_real('<?php echo $ic; ?>');">
													<img src="images/add50.png" class="mini" />
													Agregar Boleto (Real)
												</a>
												<a class="mylink" onclick="agregar_segmento('<?php echo $ic; ?>');">
													<img src="images/add50.png" class="mini" />
													Agregar Segmento
												</a>
											</div>
											<hr/>
									<?php
										}
									?>
								</div>
								<input type="hidden" id="c_tarifa_aereo__indz_" value="<?php echo $i; ?>" />
								<a class="mylinkright is_real0" onclick="agregar_tarifa_aereo('_idciudadorigen_', '_indz_');">
									<img src="images/add50.png" class="mini" />
									Agregar Nueva Tarifa
								</a>
							</div>
						</div>

						<div class="clear_mayor"></div>

						<div class="columns fifteen" onmouseover="revelar_icono_edicion('importante__idciudadorigen_');" onmouseout="ocultar_icono_edicion('importante__idciudadorigen_');">
							<font style="font-weight:bold; color:#10689b; float:left;">IMPORTANTE:</font>
							<img id="icon_editar_importante__idciudadorigen_" src="images/pencil.png" onclick="mostrar_edicion('importante__idciudadorigen_');" class="icon_edit" style="margin-left:20px; float:left;">
							<br/>
							<span id="importante__idciudadorigen_">
								<span id="datos_importante__idciudadorigen_">
									<ul class="lista_descripcion" id="lista_importante__idciudadorigen_">
										<?php
											$importante = explode("\n", $ciudad['importante']);
											for($i=0; $i<count($importante); $i++)
											{
												if($importante[$i] != "")
												{
										?>
													<li>
														<a><?php echo $importante[$i]; ?></a>
													</li>
										<?php
												}
											}
										?>		
									</ul>
								</span>
							</span>
							<span id="editar_importante__idciudadorigen_" style="display:none;">
								<textarea name="importante__idciudadorigen_" id="valor_importante__idciudadorigen_" style="width:500px;"><?php echo $ciudad["importante"]; ?></textarea>
								<img src="images/cross.png" onclick="ocultar_edicion('importante__idciudadorigen_');" class="icon_close">
								<img src="images/disk.png" onclick="guardar_atributo_x_ciudad_origen('pqt_ciudad_origen_paquete', 'importante', 'idciudad_origen_paquete', '_idciudadorigen_', 'importante__idciudadorigen_');" class="icon_save">
							</span>
						</div>

						<div class="clear_mayor"></div>

						<div class="columns fifteen" onmouseover="revelar_icono_edicion('datos_agente__idciudadorigen_');" onmouseout="ocultar_icono_edicion('datos_agente__idciudadorigen_');">
							<font style="font-weight:bold; color:#10689b; float:left;">DATOS AGENTE:</font>
							<img id="icon_editar_datos_agente__idciudadorigen_" src="images/pencil.png" onclick="mostrar_edicion('datos_agente__idciudadorigen_');" class="icon_edit" style="margin-left:20px; float:left;">
							<br/>
							<span id="datos_agente__idciudadorigen_">
								<span id="datos_datos_agente__idciudadorigen_">
									<ul class="lista_descripcion" id="lista_datos_agente__idciudadorigen_">
										<?php
											$datos_agente = explode("\n", $ciudad['datos_agente']);
											for($i=0; $i<count($datos_agente); $i++)
											{
												if($datos_agente[$i] != "")
												{
										?>
													<li>
														<a><?php echo $datos_agente[$i]; ?></a>
													</li>
										<?php
												}
											}
										?>		
									</ul>
								</span>
							</span>
							<span id="editar_datos_agente__idciudadorigen_" style="display:none;">
								<textarea name="datos_agente__idciudadorigen_" id="valor_datos_agente__idciudadorigen_" style="width:500px;"><?php echo $ciudad["datos_agente"]; ?></textarea>
								<img src="images/cross.png" onclick="ocultar_edicion('datos_agente__idciudadorigen_');" class="icon_close">
								<img src="images/disk.png" onclick="guardar_atributo_x_ciudad_origen('pqt_ciudad_origen_paquete', 'datos_agente', 'idciudad_origen_paquete', '_idciudadorigen_', 'datos_agente__idciudadorigen_');" class="icon_save">
							</span>
						</div>

					</div>
				</div>


				<div id="tarifas_generico_mostrar_<?php echo $z; ?>" style="display:none;">
					<div style="border:1px solid #AAAAAA; border-radius:10px; background-color:#F1F1F1;" class="sixteen columns tab_ciudad" id="tab_ciudad__indz_" >

						CIUDAD DE ORIGEN (Puede cambiar el nombre de ciudad): <input type="text" value="<?php echo $nombre_ciudad ?>" id="nombre_ciudad_<?php echo $z; ?>" />

						<hr/>

						<div class="columns two" style="font-weight:bold; color:#10689b; height:30px;">
							SALIDA :
						</div>
						<div class="columns four" style="height:30px;" >
							<span>
								<span>&nbsp;
									<?php 
										$fecha_salida = strtotime($ciudad["fecha_salida"]);
										if($fecha_salida)
											$fecha_salida = date('d/m/Y', $fecha_salida);
										else
											$fecha_salida = $ciudad["fecha_salida"];
										echo $fecha_salida;
									?>	
								</span>
							</span>
						</div>

						<div class="columns two" style="font-weight:bold; color:#10689b; height:30px;">
							RETORNO :
						</div>
						<div class="columns four" style="height:30px;" >
							<span>
								<span>&nbsp;
									<?php 
										$fecha_retorno = strtotime($ciudad["fecha_retorno"]);
										if($fecha_retorno)
											$fecha_retorno = date('d/m/Y', $fecha_retorno);
										else
											$fecha_retorno = $ciudad["fecha_retorno"];
										echo $fecha_retorno;
									?>	
								</span>
							</span>
						</div>

						<div class="clear_mayor"></div>

						<!-- PAQUETE INCLUYE -->
						<div class="columns fifteen" >
							<font style="font-weight:bold; color:#10689b; float:left;">PAQUETE INCLUYE:</font>
							<br/>
							<span>
								<span>
									<ul class="lista_descripcion">
										<?php
											$paquete_incluye = explode("\n", $ciudad['paquete_incluye']);
											for($i=0; $i<count($paquete_incluye); $i++)
											{
												if($paquete_incluye[$i] != "")
												{
										?>
													<li>
														<a><?php echo $paquete_incluye[$i]; ?></a>
													</li>
										<?php
												}
											}
										?>		
									</ul>
								</span>
							</span>
						</div>

						<div class="clear_mayor"></div>

						<!-- HOTELES -->
						<div class="columns fifteen">
							<font style="font-weight:bold; color:#10689b; float:left;">TARIFAS DE HOTELES:</font>
							<div>
								<?php 
									for ($i=0; $i < count($tarifas_por_destino); $i++) 
									{ 
										$ic 					= '_indz__' . $i;
										$tarifa_hotel 			= $tarifas_por_destino[$i];
										$idth 					= $tarifa_hotel['idtarifa_hotel'];
										$items_tarifa 			= $c_crear_cotizacion_paquete->obtener_hoteles_x_destino($idth);
								?>
									<div>
										<table width="100%;" class="encabezado_tarifa_hotel">
											<tr>
												<td style="width:20%;">
													<strong>DESTINO :</strong>
												</td>
												<td style="width:20%;">
													<input type="text" style="margin: 0; width: 100px;" placeholder="Codigo Ciudad" value="<?php echo strtoupper($tarifa_hotel['destino']); ?>" />
												</td>
											</tr>
										</table>
										<div class="table_hotel">
										<table style="width:100%; font-size:8pt;" >
								<?php
										for ($j=0; $j < count($items_tarifa); $j++) 
										{ 
											$item_tarifa 	= $items_tarifa[$j];
											$iditem 		= $item_tarifa['iditem_hotel'];
											$ind 			= $ic . '_' . $j;

											$is_real 		= $item_tarifa['is_real'];

											$cant_noches 	= floor( (strtotime($item_tarifa['fecha_out'] . ' 00:00:00') - strtotime($item_tarifa['fecha_in'] . ' 00:00:00')) / 86400);
											
											$total_sgl 	= $item_tarifa['precio_single'] * $cant_noches;
											$total_dbl 	= $item_tarifa['precio_doble'] * $cant_noches;
											$total_tpl 	= $item_tarifa['precio_triple'] * $cant_noches;
											$total_cpl 	= $item_tarifa['precio_cuadruple'] * $cant_noches;
											$total_cnn 	= $item_tarifa['precio_menor'] * $cant_noches;
											$total_inf 	= $item_tarifa['precio_infante'] * $cant_noches;

											if($item_tarifa['increment_fee'] == 'P')
											{
												$fee 		= 1 + ($item_tarifa['fee']/100);
												$total_sgl 	*= $fee;
												$total_dbl 	*= $fee;
												$total_tpl 	*= $fee;
												$total_cpl 	*= $fee;
												$total_cnn 	*= $fee;
												$total_inf 	*= $fee;
											}
											else
											{
												$fee 		= $item_tarifa['fee'];
												if($item_tarifa['precio_single'] > 0)
													$total_sgl 	+= $fee;
												else
													$total_sgl 	= 0;
												if($item_tarifa['precio_doble'] > 0)
													$total_dbl 	+= $fee;
												else
													$total_dbl 	= 0;
												if($item_tarifa['precio_triple'] > 0)
													$total_tpl 	+= $fee;
												else
													$total_tpl 	= 0;
												if($item_tarifa['precio_cuadruple'] > 0)
													$total_cpl 	+= $fee;
												else
													$total_cpl 	= 0;
												if($item_tarifa['precio_menor'] > 0)
													$total_cnn 	+= $fee;
												else
													$total_cnn 	= 0;
												if($item_tarifa['precio_infante'] > 0)
													$total_inf 	+= $fee;
												else
													$total_inf 	= 0;
											}

											if($item_tarifa['increment_factura'] == 'P')
											{
												$iva 		= 1 + ($item_tarifa['factura']/100);
												$total_sgl 	*= $iva;
												$total_dbl 	*= $iva;
												$total_tpl 	*= $iva;
												$total_cpl 	*= $iva;
												$total_cnn 	*= $iva;
												$total_inf 	*= $iva;
											}
											else
											{
												$iva 		= $item_tarifa['factura'];
												if($item_tarifa['precio_single'] > 0)
													$total_sgl 	+= $iva;
												else
													$total_sgl 	= 0;
												if($item_tarifa['precio_doble'] > 0)
													$total_dbl 	+= $iva;
												else
													$total_dbl 	= 0;
												if($item_tarifa['precio_triple'] > 0)
													$total_tpl 	+= $iva;
												else
													$total_tpl 	= 0;
												if($item_tarifa['precio_cuadruple'] > 0)
													$total_cpl 	+= $iva;
												else
													$total_cpl 	= 0;
												if($item_tarifa['precio_menor'] > 0)
													$total_cnn 	+= $iva;
												else
													$total_cnn 	= 0;
												if($item_tarifa['precio_infante'] > 0)
													$total_inf 	+= $iva;
												else
													$total_inf 	= 0;
											}
								?>
											<tr>
												<td class="thirteen columns" style="padding-top:10px;" >
													<div class="one columns">
														<label style="color:#10689B;">HOTEL:</label>
													</div>
													<div class="three columns">
														<input type="text" placeholder="OBLIGATORIO" value="<?php echo $item_tarifa['nombre_hotel']; ?>" style="margin:0px;" />
													</div>
													<div class="one columns">
														<label style="color:#10689B;">IN:</label>
													</div>
													<div class="two columns">
														<input type="text" value="<?php echo date('d/m/Y', strtotime($item_tarifa['fecha_in'] . ' 00:00:00')); ?>" style="width:75%; margin:0px;" placeholder="dd/mm/aaaa" />
													</div>
													<div class="one columns">
														<label style="color:#10689B;">OUT:</label>
													</div>
													<div class="two columns">
														<input type="text" value="<?php echo date('d/m/Y', strtotime($item_tarifa['fecha_out'] . ' 00:00:00')); ?>" style="width:75%; margin:0px;" placeholder="dd/mm/aaaa" />
													</div>
													<div class="one columns">
														<label style="color:#10689B;">NOCHES:</label>
													</div>
													<div class="one columns">
														<label><?php echo $cant_noches; ?></label>
													</div>
													<div class="clear_mayor"></div>
													<div class="two columns">
														<label>IDENTIFICADOR</label>
														<input type="text" placeholder="OPCIONAL" value="<?php echo $item_tarifa['identificador']; ?>" style="margin:0px;" />
														<label>TIME LIMIT</label>
														<input type="text" placeholder="dd/mm/aaaa" class="time_limit" value="<?php echo date('d/m/Y', strtotime($item_tarifa['time_limit'])); ?>" style="width:75%; margin:0px;" />
													</div>
													<div class="one columns">
														<label>PRECIOS</label>
														<input type="text" value="<?php echo $item_tarifa['precio_single']; ?>" placeholder="SGL" style="margin:0px;" />
														<input type="text" value="<?php echo $item_tarifa['precio_doble']; ?>" placeholder="DBL" style="margin:0px;" />
														<input type="text" value="<?php echo $item_tarifa['precio_triple']; ?>" placeholder="TPL" style="margin:0px;" />
														<input type="text" value="<?php echo $item_tarifa['precio_cuadruple']; ?>" placeholder="CPL" style="margin:0px;" />
													</div>
													<div class="one columns">
														<label>&nbsp;</label>
														<input type="text" value="<?php echo $item_tarifa['precio_menor']; ?>" placeholder="CNN" style="margin:0px;" />
														<input type="text" value="<?php echo $item_tarifa['precio_infante']; ?>" placeholder="INF" style="margin:0px;" />	
														<input type="text" value="<?php echo $item_tarifa['fee']; ?>" placeholder="FEE" style="margin:0px;" />
														<input type="text" value="<?php echo $item_tarifa['factura']; ?>" placeholder="IVA" style="margin:0px;" />
													</div>
													<div class="three columns">
														<label>ALIMENTACION</label>
														<textarea class="mytextarea" ><?php echo strtoupper($item_tarifa['alimentacion']); ?></textarea>
														<label>OBS</label>
														<textarea class="mytextarea" ><?php echo strtoupper($item_tarifa['info_extra']); ?></textarea>
													</div>
													<div class="three columns">
														<label>LINK</label>
														<input type="text" value="<?php echo $item_tarifa['link']; ?>" />
														<label>OBS INT</label>
														<textarea class="mytextarea" ><?php echo strtoupper($item_tarifa['observacion']); ?></textarea>	
													</div>
													<div class="one columns">
														<label>SGL:</label>
														<label>DBL:</label>
														<label>TPL:</label>
														<label>CPL:</label>
														<label>CNN:</label>
														<label>INF:</label>
													</div>
													<div class="one columns">
														<label><?php echo ceil($total_sgl); ?></label>
														<label><?php echo ceil($total_dbl); ?></label>
														<label><?php echo ceil($total_tpl); ?></label>
														<label><?php echo ceil($total_cpl); ?></label>
														<label><?php echo ceil($total_cnn); ?></label>
														<label><?php echo ceil($total_inf); ?></label>
													</div>
													<div class="clear"></div>
													<div class="two columns">
														<strong>FEE:</strong>
														<input type="radio" name="apfee<?php echo $ind; ?>" value="M" <?php if($item_tarifa['increment_fee'] == 'M') echo 'Checked'; ?> />$
														<input type="radio" name="apfee<?php echo $ind; ?>" value="P" <?php if($item_tarifa['increment_fee'] == 'P') echo 'Checked'; ?> />%
													</div>
													<div class="two columns">
														<strong>IVA:</strong>
														<input type="radio" name="apiva<?php echo $ind; ?>" value="M" <?php if($item_tarifa['increment_factura'] == 'M') echo 'Checked'; ?> />$
														<input type="radio" name="apiva<?php echo $ind; ?>" value="P" <?php if($item_tarifa['increment_factura'] == 'P') echo 'Checked'; ?>  />%
													</div>
													<hr/>
												</td>
												<td style="vertical-align:top;">
													<center>
														<br/><br/>Act<br/>
														<input type="checkbox" <?php if($item_tarifa['estado'] == '1') echo 'checked'; ?> />
												</td>
											</tr>
								<?php
										}
								?>
										</table>
										</div>
										</div>
								<?
									}				
								?>
							</div>
						</div>

						<div class="clear_mayor"></div>

						<!-- OTROS SERVICIOS -->
						<div class="columns fifteen">
							<font style="font-weight:bold; color:#10689b; float:left;">OTROS SERVICIOS:</font>
							<div>
								<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" class="con-borde" >
									<tr style="background-color:#10689b; color:#FFFFFF;">
										<th style="width:27%;">DETALLE</th>
										<th style="width:15%;">DESDE / HASTA</th>
										<th style="width:9%;">PRECIOS</th>
										<th style="width:18%;">FEE / IVA / TIME LIMIT</th>
										<th style="width:20%;">MONEDA / OBS</th>
										<th style="width:8%;">TOTALES</th>
										<th></th>
										<th>ACT</th>
									</tr>
									<?php
										$otras_tarifas = $c_crear_cotizacion_paquete->obtener_otros_servicios_x_ciudad($idciudad_origen);
										for ($i=0; $i < count($otras_tarifas); $i++) 
										{ 
											$ic 			= $z . '_' . $i;
											$is_real 		= $tarifa_otros['is_real'];

											$tarifa_otros 	= $otras_tarifas[$i];
											$idotra_tarifa 	= $tarifa_otros['idtarifa_otros'];
											
											$total_adt = $tarifa_otros['precio_adulto'];
											$total_cnn = $tarifa_otros['precio_menor'];
											$total_inf = $tarifa_otros['precio_infante'];

											if($tarifa_otros['increment_fee'] == 'P')
											{
												$fee 		= 1+($tarifa_otros['fee'] / 100);
												$total_adt 	*= $fee;
												$total_cnn 	*= $fee;
												$total_inf 	*= $fee;
											}
											else
											{
												$fee = $tarifa_otros['fee'];
												if($total_adt > 0)
													$total_adt += $fee;
												else
													$total_adt = 0;
												if($total_cnn > 0)
													$total_cnn += $fee;
												else
													$total_cnn = 0;
												if($total_inf > 0)
													$total_inf += $fee;
												else
													$total_inf = 0;
											}

											if($tarifa_otros['increment_factura'] == 'P')
											{
												$iva 		= 1+($tarifa_otros['factura'] / 100);
												$total_adt 	*= $iva;
												$total_cnn 	*= $iva;
												$total_inf 	*= $iva;
											}
											else
											{
												$iva = $tarifa_otros['factura'];
												if($total_adt > 0)
													$total_adt += $iva;
												else
													$total_adt = 0;
												if($total_cnn > 0)
													$total_cnn += $iva;
												else
													$total_cnn = 0;
												if($total_inf > 0)
													$total_inf += $iva;
												else
													$total_inf = 0;
											}
									?>
									<tr>
										<td style="vertical-align:middle; padding-top:10px; padding-bottom:10px;">
											<?php
												$nombre_servicio = $c_crear_cotizacion_paquete->obtener_nombre_servicio($tarifa_otros['idotro_servicio']);
											?>
											CODIGO :<input type="text" placeholder="OPCIONAL" value="<?php echo $tarifa_otros['codigo']; ?>" style="width:70%; margin:0;" />
											CONCPT :<input type="text" placeholder="OBLIGATORIO" value="<?php echo $nombre_servicio; ?>" style="width:70%; margin:0;" />*
											DESCRIP :<input type="text" placeholder="OPCIONAL" value="<?php echo $tarifa_otros['descripcion']; ?>" style="width:70%; margin:0;" />
										</td>
										<td style="vertical-align:middle;">
											<input type="text" value="<?php echo $tarifa_otros['ciudad']; ?>" placeholder="CIUDAD (IATA)" style="margin: 0; width: 70%;" />
											<input type="text" placeholder="dd/mm/aaaa" value="<?php echo date('d/m/Y', strtotime($tarifa_otros['fecha_desde'])); ?>" class="time_limit" style="margin: 0; width: 70%;" />
											<input type="text" placeholder="dd/mm/aaaa" value="<?php echo date('d/m/Y', strtotime($tarifa_otros['fecha_hasta'])); ?>" class="time_limit" style="margin: 0; width: 70%;" />
										</td>
										<td style="vertical-align:middle;">
											<input type="text" placeholder="ADT" value="<?php echo $tarifa_otros['precio_adulto']; ?>" style="margin: 0; width: 95%;" />
											<input type="text" placeholder="CNN" value="<?php echo $tarifa_otros['precio_menor']; ?>" style="margin: 0; width: 95%;" />
											<input type="text" placeholder="INF" value="<?php echo $tarifa_otros['precio_infante']; ?>" style="margin: 0; width: 95%;" />
										</td>
										<td style="vertical-align:middle;">
											<input type="text" placeholder="FEE" value="<?php echo $tarifa_otros['fee']; ?>" style="margin: 0; width: 25%;" />
											<input type="radio" name="sapfee<?php echo $ic; ?>" value="M" <?php if($tarifa_otros['increment_fee'] == 'M') echo 'Checked'; ?> />$
											<input type="radio" name="sapfee<?php echo $ic; ?>" value="P" <?php if($tarifa_otros['increment_fee'] == 'P') echo 'Checked'; ?> />%
											<input type="text" placeholder="IVA" value="<?php echo $tarifa_otros['factura']; ?>" style="margin: 0; width: 25%;" />
											<input type="radio" name="sapiva<?php echo $ic; ?>" value="M" <?php if($tarifa_otros['increment_factura'] == 'M') echo 'Checked'; ?> />$
											<input type="radio" name="sapiva<?php echo $ic; ?>" value="P" <?php if($tarifa_otros['increment_factura'] == 'P') echo 'Checked'; ?> />%
											<input type="text" placeholder="dd/mm/aaaa" value="<?php echo date('d/m/Y', strtotime($tarifa_otros['time_limit'])); ?>" class="time_limit" style="margin: 0; width: 70%;" />
										</td>
										<td style="vertical-align:middle;">
											<select style="margin: 0; width: 95%;">
												<option value="USD" <?php if($tarifa_otros['moneda'] == 'U') echo 'Selected'; ?> >USD</option>
												<option value="BOB" <?php if($tarifa_otros['moneda'] == 'B') echo 'Selected'; ?> >BOB</option>
											</select>
											<textarea style="margin:0; width:95%; min-height:10px; height:30px;" ><?php echo strtoupper($tarifa_otros['observacion']); ?></textarea>
										</td>
										<td style="vertical-align:middle; line-height:1; font-size:8pt;">
											<span><?php echo ceil($total_adt); ?></span>(ADT)<br/>
											<span><?php echo ceil($total_cnn); ?></span>(CNN)<br/>
											<span><?php echo ceil($total_inf); ?></span>(INF)
										</td>
										<td style="vertical-align:middle;">
										</td>
										<td style="vertical-align:middle;">
											<input type="checkbox" <?php if($tarifa_otros['estado'] == '1') echo 'checked'; ?> /><br/>
										</td>
									</tr>
									<?php
										}
									?>
								</table>
							</div>
						</div>

						<div class="clear_mayor"></div>

						<!-- BOLETOS AEREOS -->
						<div class="columns fifteen">
							<font style="font-weight:bold; color:#10689b; float:left;">BOLETOS AEREOS:</font>
							<div class="clear"></div>
							<div>
								<?php
									$opciones_aereo = $c_crear_cotizacion_paquete->obtener_opcion_boletos_x_ciudad($idciudad_origen);
									
									for ($i=0; $i < count($opciones_aereo); $i++) 
									{ 
										$ic 			= $z . '_' . $i;

										$opcion_aereo 	= $opciones_aereo[$i];
										$idopaereo 		= $opcion_aereo['idopcion_boleto'];
								?>
										<div style="margin-bottom:15px; border:1px solid #888;">
											<table width="100%;" class="encabezado_tarifa_hotel">
												<tr>
													<td>
														<strong>OPCION :</strong>
													</td>
													<td style="width:30%;">
														<input type="text" style="margin: 0; width: 150px;" value="<?php echo $opcion_aereo['descripcion']; ?>" />
													</td>
													<td>
														<strong>SALE :</strong>
													</td>
													<td style="width:30%;">
														<input type="text" style="margin: 0; width: 150px;" value="<?php echo $opcion_aereo['salida']; ?>" />
													</td>
												</tr>
											</table>
											<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" class="con-borde" >
												<tr style="background-color:#10689b; color:#FFFFFF;">
													<th style="width:10%;">ORD / CODIGO</th>
													<th style="width:22%; line-height:1;">L.A. / RUTA</th>
													<th style="width:10%;">PRECIOS</th>
													<th style="width:18%;">FEE / IVA / TIME LIMIT</th>
													<th style="width:25%;">OBSERVACION</th>
													<th style="width:10%;">TOTALES</th>
													<th></th>
													<th style="width:5%;">ACT</th>
												</tr>
												<?php
													$boletos = $c_crear_cotizacion_paquete->obtener_boletos_x_opcion_aereo($idopaereo);
													for ($j=0; $j < count($boletos); $j++) 
													{ 
														$indbol 			= $ic . '_' . $j;
														$boleto_cotizado 	= $boletos[$j];
														$idboleto 			= $boleto_cotizado['idboleto_cotizado'];
														$is_real 			= $boleto_cotizado['is_real'];
														//$factor   			= (1+($boleto_cotizado['fee']/100))*(1+($boleto_cotizado['factura']/100));
														$total_adt = $boleto_cotizado['precio_adulto'];
														$total_cnn = $boleto_cotizado['precio_menor'];
														$total_inf = $boleto_cotizado['precio_infante'];

														if($boleto_cotizado['increment_fee'] == 'P')
														{
															$fee 		= 1+($boleto_cotizado['fee'] / 100);
															$total_adt 	*= $fee;
															$total_cnn 	*= $fee;
															$total_inf 	*= $fee;
														}
														else
														{
															$fee = $boleto_cotizado['fee'];
															if($total_adt > 0)
																$total_adt += $fee;
															else
																$total_adt = 0;
															if($total_cnn > 0)
																$total_cnn += $fee;
															else
																$total_cnn = 0;
															if($total_inf > 0)
																$total_inf += $fee;
															else
																$total_inf = 0;
														}

														if($boleto_cotizado['increment_factura'] == 'P')
														{
															$iva 		= 1+($boleto_cotizado['factura'] / 100);
															$total_adt 	*= $iva;
															$total_cnn 	*= $iva;
															$total_inf 	*= $iva;
														}
														else
														{
															$iva = $boleto_cotizado['factura'];
															if($total_adt > 0)
																$total_adt += $iva;
															else
																$total_adt = 0;
															if($total_cnn > 0)
																$total_cnn += $iva;
															else
																$total_cnn = 0;
															if($total_inf > 0)
																$total_inf += $iva;
															else
																$total_inf = 0;
														}
												?>
														<tr>
															<td style="vertical-align:middle;">
																<input type="text" value="<?php echo $boleto_cotizado['orden']; ?>" style="margin: 0; width:80%; top;" />	
																<input type="text" value="<?php echo $boleto_cotizado['codigo_reserva']; ?>" style="margin: 0; width:95%;" />	
															</td>
															<td style="vertical-align:middle;">
																<input type="text" placeholder="Linea Aerea" value="<?php echo strtoupper($c_crear_cotizacion_paquete->obtener_linea_aerea($boleto_cotizado['idlineas_aereas'])); ?>" style="margin: 0; width:95%;" />
																<input type="hidden" value="<?php echo $boleto_cotizado['idlineas_aereas']; ?>" />
																<input type="text" placeholder="Ruta" value="<?php echo $boleto_cotizado['ruta']; ?>" style="margin: 0; width:95%;" />	
															</td>
															<td style="vertical-align:middle;">
																<input type="text" placeholder="ADT" value="<?php echo $boleto_cotizado['precio_adulto']; ?>" style="margin: 0; width:95%;" />	
																<input type="text" placeholder="CNN" value="<?php echo $boleto_cotizado['precio_menor']; ?>" style="margin: 0; width:95%;" />	
																<input type="text" placeholder="INF" value="<?php echo $boleto_cotizado['precio_infante']; ?>" style="margin: 0; width:95%;" />	
															</td>
															<td style="vertical-align:middle;">
																<input type="text" placeholder="FEE" value="<?php echo $boleto_cotizado['fee']; ?>" style="margin: 0; width:25%;" />	
																<input type="radio" name="bapfee<?php echo $indbol; ?>" value="M" <?php if($boleto_cotizado['increment_fee'] == 'M') echo 'Checked'; ?> />$
																<input type="radio" name="bapfee<?php echo $indbol; ?>" value="P" <?php if($boleto_cotizado['increment_fee'] == 'P') echo 'Checked'; ?> />%
																<input type="text" placeholder="IVA" value="<?php echo $boleto_cotizado['factura']; ?>" style="margin: 0; width:25%;" />	
																<input type="radio" name="bapiva<?php echo $indbol; ?>" value="M" <?php if($boleto_cotizado['increment_factura'] == 'M') echo 'Checked'; ?> />$
																<input type="radio" name="bapiva<?php echo $indbol; ?>" value="P" <?php if($boleto_cotizado['increment_factura'] == 'P') echo 'Checked'; ?> />%
																<input type="text" value="<?php echo date('d/m/Y', strtotime($boleto_cotizado['time_limit'])); ?>" style="margin: 0; width: 70%;" />
															</td>
															<td style="vertical-align:middle;">
																<select style="margin: 0; width:95%;">
																	<option value="USD" <?php if($boleto_cotizado['moneda'] == 'U') echo 'Selected'; ?> >USD</option>
																	<option value="BOB" <?php if($boleto_cotizado['moneda'] == 'B') echo 'Selected'; ?> >BOB</option>
																</select>
																<textarea style="margin:0; width:95%; min-height:10px; height:30px;" ><?php echo $boleto_cotizado['observacion']; ?></textarea>
															</td>
															<td style="vertical-align:middle; line-height:1; font-size:8pt;">
																<span><?php echo ceil($total_adt); ?></span>(ADT)<br/>
																<span><?php echo ceil($total_cnn); ?></span>(CNN)<br/>
																<span><?php echo ceil($total_inf); ?></span>(INF)
															</td>
															<td style="vertical-align:middle; line-height:1; font-size:8pt;">
															</td>
															<td style="vertical-align:middle;">
																<input type="checkbox" <?php if($boleto_cotizado['estado'] == '1') echo 'checked'; ?> /><br/>
															</td>
														</tr>
												<?php
													}
												?>
											</table>
											<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" class="con-borde" >
												<tr style="background-color:#10689b; color:#FFFFFF;">
													<th style="width:3%;">ORD</th>
													<th style="width:5%;">CODIGO</th>
													<th style="width:19%;">LINEA AEREA</th>
													<th style="width:5%;">VUELO</th>
													<th style="width:10%;">FECHA</th>
													<th style="width:20%;">ORIGEN</th>
													<th style="width:8%;">SALE</th>
													<th style="width:20%;">DESTINO</th>
													<th style="width:8%;">LLEGA</th>
													<th style="width:2%;"></th>
												</tr>
												<?php
													$itinerario = $c_crear_cotizacion_paquete->obtener_itinerario_x_opcion_aereo($idopaereo);
													for ($k=0; $k < count($itinerario); $k++) 
													{ 
														$indseg 	= $ic . '_' . $k;
														$segmento 	= $itinerario[$k];
														$idsegmento = $segmento['idsegmento'];

														$linea_aerea 		= $c_crear_cotizacion_paquete->obtener_linea_aerea($segmento['idlineas_aereas']);
														$aeropuerto_sale 	= $c_crear_cotizacion_paquete->obtener_aeropuerto($segmento['origen']);
														$aeropuerto_llega 	= $c_crear_cotizacion_paquete->obtener_aeropuerto($segmento['destino']);
												?>
														<tr>
															<td>
																<input type="text" value="<?php echo $segmento['orden']; ?>" style="margin: 0; width: 95%;" />	
															</td>
															<td>
																<input type="text" value="<?php echo $segmento['codigo']; ?>" style="margin: 0; width: 95%;" />	
															</td>
															<td>
																<input type="text" value="<?php echo $linea_aerea; ?>" style="margin: 0; width: 95%;" />	
															</td>
															<td>
																<input type="text" value="<?php echo $segmento['nro_vuelo']; ?>" style="margin: 0; width: 95%;" />	
															</td>
															<td>
																<input type="text" value="<?php echo date('d/m/Y', strtotime($segmento['fecha'])); ?>" style="margin: 0; width:95%" placeholder="dd/mm/aaaa" />	
															</td>
															<td>
																<input type="text" value="<?php echo $aeropuerto_sale; ?>" style="margin: 0; width: 95%;" />
															</td>
															<td>
																<input type="text" value="<?php echo $segmento['hora_sale']; ?>" style="margin: 0; width: 95%;" placeholder="hh:mm" />
															</td>
															<td>
																<input type="text" value="<?php echo $aeropuerto_llega; ?>" style="margin: 0; width: 95%;" />
															</td>
															<td>
																<input type="text" value="<?php echo $segmento['hora_llega']; ?>" style="margin: 0; width: 95%;" placeholder="hh:mm" />
															</td>
															<td>
															</td>
														</tr>
												<?php
													}
												?>
											</table>
										</div>
										<hr/>
								<?php
									}
								?>
							</div>
						</div>

						<div class="clear_mayor"></div>

						<div class="columns fifteen">
							<font style="font-weight:bold; color:#10689b; float:left;">IMPORTANTE:</font>
							<br/>
							<span>
								<span>
									<ul class="lista_descripcion">
										<?php
											$importante = explode("\n", $ciudad['importante']);
											for($i=0; $i<count($importante); $i++)
											{
												if($importante[$i] != "")
												{
										?>
													<li>
														<a><?php echo $importante[$i]; ?></a>
													</li>
										<?php
												}
											}
										?>		
									</ul>
								</span>
							</span>
						</div>

						<div class="clear_mayor"></div>

						<div class="columns fifteen">
							<font style="font-weight:bold; color:#10689b; float:left;">DATOS AGENTE:</font>
							<br/>
							<span>
								<span>
									<ul class="lista_descripcion">
										<?php
											$datos_agente = explode("\n", $ciudad['datos_agente']);
											for($i=0; $i<count($datos_agente); $i++)
											{
												if($datos_agente[$i] != "")
												{
										?>
													<li>
														<a><?php echo $datos_agente[$i]; ?></a>
													</li>
										<?php
												}
											}
										?>		
									</ul>
								</span>
							</span>
						</div>

					</div>
				</div>

				<div class="clear_mayor"></div>

			<?php
			}
		?>
	</div>

</div>