<?
	include '../BD/controladoraBD.php';
	include '../entidad/otros_cotizado.php';

	$idpax 			= $_GET['idpax'];
	$tipo 			= $_GET['tipo'];
	$idcotizacion 	= $_GET['idcotizacion'];
	
	if($tipo == 1)
	{
		$class = "paquete";
		$abrev = "OPC";
	}
	else
	{
		$class = "evento";
		$abrev = "OEC";
	}

	$resp = otros_cotizado::obtenet_otros_cotizado_idpax($idpax, $class);
		
	if(count($resp) > 0)
	{
		for($i = 0; $i < count($resp); $i++)
		{
			$precio 	= 0;
			$fee 		= 0;
			$factura	= 0;
			$r 			= otros_cotizado::obtener_otros_nombre_precio($resp[$i]['idtarifa_otros_'.$class.'_cotizado'], $class);

			if($resp[$i]['tipo_precio'] == 1)
			{
				$precio 	= $r[0]['precio_adulto'];
					$factor 	= (1 + $r[0]['fee']/100) * (1 + $r[0]['factura']/100);
					$precio 	= $precio * $factor;
			}
			else
			{
				if($resp[$i]['tipo_precio'] == 2)
				{
					$precio = $r[0]['precio_menor'];
					$factor 	= (1 + $r[0]['fee']/100) * (1 + $r[0]['factura']/100);
					$precio 	= $precio * $factor;
				}
				else
				{
					$precio = $r[0]['precio_infante'];
					$factor 	= (1 + $r[0]['fee']/100) * (1 + $r[0]['factura']/100);
					$precio 	= $precio * $factor;
				}	
			}
			
			$servicio = $r[0]['nombre_servicio'];
			$codigo = $r[0]['codigo'];
			
			$vec[$i]['idtarifa_otros_'.$class.'_cotizado'] 	= $r[0]['idtarifa_otros_'.$class.'_cotizado'];
			$vec[$i]['servicio'] 							= $servicio . ' :' . $codigo;
			$vec[$i]['precio'] 								= $precio;
			$vec[$i]['moneda'] 								= $r[0]['moneda'];
			$vec[$i]['tipo_cambio'] 						= $r[0]['tipo_cambio'];
			$vec[$i]['estado'] 								= $r[0]['estado'];
		}
	}

?>
    <link href="lib/tinybox2/style.css" rel="stylesheet"/>
    	<div class="sixteen columns" style="color:#FFFFFF; position:absolute; padding:10px; opacity:0.9; display:none;" id="mensaje_pax">
		</div>
		<div class="sixteen columns">
        	<h3>Editar Otros Servicios</h3>
            <label id="nombre_pax_tiny"></label>
            <hr/>
            <span>
            	<select id="cb_otros" onchange="obtener_otros_cotizado(<? echo $idcotizacion;?>,<? echo $tipo;?>)">
                    <option value="0">-- Tipo Pax --</option>
                    <option value="1">Adulto</option>
                    <option value="2">Menor</option>
                    <option value="3">Infante</option>
            	</select>
            </span>
            <span>
            	<select id="cb_otros_nombre">
                    <option value="0">-- Otros Servicios --</option>
                </select>
            </span>
            <span>
            	<input type="button" value="Agregar" onclick="guardar_otros_pax(<? echo $idpax;?>);"/>
            </span>
            <hr/>
            <div class="fourteen columns" id="table_tiny_otros_<? echo $idpax;?>">
           <table style="width:99%; border:1px solid #AAAAAA; font-size:8pt;display:;" class="con-borde user_info_editable">
            <tr style="background-color:#10689b; color:#FFFFFF;border-color:#FFF; height:20px;">
                <th rowspan="2" style="vertical-align:middle;">Nro</th>
                <th rowspan="2" style="vertical-align:middle; border-left:solid 1px #FFFFFF;">Servicios</th>
                <th colspan="2" style="border-bottom: solid 1px #FFFFFF;border-left: solid 1px #FFFFFF;">Precio</th>
                <th rowspan="2" style="width:5px; border-left: solid 1px #FFFFFF;">&nbsp;</th>
                <th rowspan="2" style="width:5px; border-left: solid 1px #FFFFFF;">&nbsp;</th>
            </tr>
            <tr style="background-color:#10689b; color:#FFFFFF;border-color:#FFF;height:20px;">
                <th style="border-left: solid 1px #FFFFFF;border-right: solid 1px #FFFFFF;">BOB</th>
                <th>USD</th>
            </tr>
            	<?
			$total_b = 0;
			$total_u = 0;
			$totales = 0;
			if(count($vec) > 0)
			{
				$nro = 0;
				foreach($vec as $tabla)
				{
					$ident = $tabla['idtarifa_otros_'.$class.'_cotizado'];

					if($tabla['moneda'] == "B")
					{
						$total_b += $tabla['precio'];
						$totales += round($tabla['precio'] / $tabla['tipo_cambio'],2);
					}
					else
					{
						$total_u += $tabla['precio'];
						$totales += round($tabla['precio'],2);
					}
					
			?>
                <tr valign="middle" style="height:15px;<? if($tabla['estado'] == 0) echo "color:#ff2d32;";?>">
                	<td>
                		<input type="hidden" id="hd_otros_cotizado_<? echo $tabla['idtarifa_otros_'.$class.'_cotizado'];?>"/><? echo $nro = $nro +1;?>
                	</td>
                    <td>
                    	<? echo strtoupper($tabla['servicio']); ?>
                    	<div id="archivos<?php echo $ident; ?>" style="display:none; background-color:#f1f1f1; border-radius:5px; border:1px solid #aaaaaa; position:absolute; width:350px; height:200px;">
                		</div>
                    </td>
                    <td style="text-align:right;"><? echo $tabla['moneda'] == "B" ? number_format(round($tabla['precio'],2),2): "0.00";?></td>
                    <td style="text-align:right;"><? echo $tabla['moneda'] == "U" ? number_format(round($tabla['precio'],2),2): "0.00";?></td>
                    <td><img src="images/cross.png" title="Eliminar Otros Servicios" style="cursor:pointer;" onclick="eliminar_otros_pax(<? echo $ident;?>,<? echo $idpax;?>);"/></td>
                    <td><img src="images/folder.png" title="Archivos Adjuntos" style="cursor:pointer;" onclick="mostrar_dialogo_archivos_x_item('<? echo $ident; ?>', '<?php echo $idpax; ?>', '<?php echo $abrev; ?>');"/></td>
                </tr>
			<?
				}
			}
			else
			{
				?>
				<tr style="height:15px;">
                    <td colspan="6">No Existe Otros Servicios Cargados.</td>                    
                </tr>
				<?
					
			}
			?>
            	<tr style="height:15px;">
            		<td colspan="2" style="text-align:right;"><strong>Total</strong></td>
                	<td style="text-align:right;"><? echo number_format(round($total_b,2),2);?></td>
                    <td style="text-align:right;"><? echo number_format(round($total_u,2),2);?></td>
                    <td>&nbsp;</td> 
                    <td>&nbsp;</td> 
            	</tr>
            	</table>
                <div class="clear_mayor"></div><div class="clear_mayor"></div><div class="clear_mayor"></div>
                <div><strong>Total en USD&nbsp;:&nbsp;&nbsp;</strong><span style="width:15px;"><? echo $totales;?></span></div>                
			</div>
            <div class="clear_mayor"></div>
            <div class="nine columns" style="text-align:center;"> 
            	<img id="img_load_otros_pax" src="lib/tinybox2/images/preload.gif" style="display:none;" />
            </div>
        </div>
