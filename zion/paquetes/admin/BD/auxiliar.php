CREATE TABLE tarifa_hotel_evento
(
	idtarifa_hotel_evento 	INT(11) AUTO_INCREMENT PRIMARY KEY,
	idevento 		INT(11),
	destino 		VARCHAR(100),
	comision 		FLOAT,
	cant_noches		INT(11),
	FOREIGN KEY(idevento) REFERENCES evento(idevento)
);

CREATE TABLE item_hotel_evento
(
	iditem_hotel_evento 	INT(11) AUTO_INCREMENT PRIMARY KEY,
	idtarifa_hotel_evento 	INT(11),
	idhotel 				INT(11),
	link 					VARCHAR(100),
	precio_single 			FLOAT DEFAULT 0,
	precio_doble 			FLOAT DEFAULT 0,
	precio_triple 			FLOAT DEFAULT 0,
	precio_cuadruple 		FLOAT DEFAULT 0,
	precio_menor 			FLOAT DEFAULT 0,
	precio_infante 			FLOAT DEFAULT 0,
	observacion 			TEXT,
	FOREIGN KEY(idtarifa_hotel_evento) REFERENCES tarifa_hotel_evento(idtarifa_hotel_evento),
	FOREIGN KEY(idhotel) REFERENCES hotel(idhotel)
);

CREATE TABLE opcion_hoteles_evento
(
	idopcion_hoteles_evento INT(11) AUTO_INCREMENT PRIMARY KEY,
	idevento 				INT(11),
	observacion            	TEXT,
	FOREIGN KEY(idevento) REFERENCES evento(idevento)
);

CREATE TABLE opcion_hoteles_has_item_evento
(
	idopcion_hoteles_evento INT(11),
	iditem_hotel_evento 	INT(11),
	PRIMARY KEY(idopcion_hoteles_evento, iditem_hotel_evento),
	FOREIGN KEY(idopcion_hoteles_evento) REFERENCES opcion_hoteles_evento(idopcion_hoteles_evento)
);

CREATE TABLE tarifa_otros_evento
(
	idtarifa_otros_evento 	INT(11) AUTO_INCREMENT PRIMARY KEY,
	idevento 				INT(11),
	concepto 				VARCHAR(1) DEFAULT 'O' COMMENT  'S:Seguro  T:Traslado  I:Inscripcion  O:Otros  A:Auto  F:Fee  C:Crucero',
	precio_adulto 			FLOAT DEFAULT 0,
	precio_menor 			FLOAT DEFAULT 0,
	precio_infante 			FLOAT DEFAULT 0,
	fee 					FLOAT DEFAULT 0,
	factura 				FLOAT DEFAULT 0,
	moneda 					VARCHAR(1) DEFAULT 'U' COMMENT 'B:Bolivianos  U:USD',
	observacion 			TEXT,
	FOREIGN KEY(idevento) REFERENCES evento(idevento)
);

CREATE TABLE boleto_cotizado_evento
(
	idboleto_cotizado_evento 	INT(11) AUTO_INCREMENT PRIMARY KEY,
	idevento 					INT(11),
	orden 						TINYINT DEFAULT 1,
	ruta 						VARCHAR(100),
	precio_adulto 				FLOAT DEFAULT 0,
	precio_menor 				FLOAT DEFAULT 0,
	precio_infante 				FLOAT DEFAULT 0,
	fee 						FLOAT DEFAULT 0,
	factura 					FLOAT DEFAULT 0,
	moneda 						VARCHAR(1) DEFAULT 'U' COMMENT 'B:Bolivianos  U:USD',
	observacion 				TEXT,
	FOREIGN KEY(idevento) REFERENCES evento(idevento)
);

CREATE TABLE segmento_evento
(
	idsegmento_evento 	INT(11) AUTO_INCREMENT PRIMARY KEY,
	idevento 			INT(11),
	orden 				TINYINT DEFAULT 1,
	idlineas_aereas 	INT(11),
	nro_vuelo 			INT(11) DEFAULT 0,
	fecha 				DATE DEFAULT '0000-00-00',
	origen 				VARCHAR(5),
	hora_sale 			TIME DEFAULT '00:00:00',
	destino 			VARCHAR(5),
	hora_llega 			TIME DEFAULT '00:00:00',
	FOREIGN KEY(idevento) REFERENCES evento(idevento),
	FOREIGN KEY(idlineas_aereas) REFERENCES lineas_aereas(idlineas_aereas)
);