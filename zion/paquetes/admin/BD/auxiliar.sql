CREATE TABLE tarifa_hotel_paquete
(
	idtarifa_hotel_paquete 	INT(11) AUTO_INCREMENT PRIMARY KEY,
	idpaquete 				INT(11),
	destino 				VARCHAR(100),
	comision 				FLOAT,
	cant_noches				INT(11),
	FOREIGN KEY(idpaquete) REFERENCES paquete(idpublicacion)
);

CREATE TABLE item_hotel_paquete
(
	iditem_hotel_paquete 	INT(11) AUTO_INCREMENT PRIMARY KEY,
	idtarifa_hotel_paquete 	INT(11),
	idhotel 				INT(11),
	link 					VARCHAR(100),
	precio_single 			FLOAT DEFAULT 0,
	precio_doble 			FLOAT DEFAULT 0,
	precio_triple 			FLOAT DEFAULT 0,
	precio_cuadruple 		FLOAT DEFAULT 0,
	precio_menor 			FLOAT DEFAULT 0,
	precio_infante 			FLOAT DEFAULT 0,
	observacion 			TEXT,
	FOREIGN KEY(idtarifa_hotel_paquete) REFERENCES tarifa_hotel_paquete(idtarifa_hotel_paquete),
	FOREIGN KEY(idhotel) REFERENCES hotel(idhotel)
);

CREATE TABLE opcion_hoteles_paquete
(
	idopcion_hoteles_paquete 	INT(11) AUTO_INCREMENT PRIMARY KEY,
	idpaquete 					INT(11),
	observacion            		TEXT,
	FOREIGN KEY(idpaquete) REFERENCES paquete(idpublicacion)
);

CREATE TABLE opcion_hoteles_has_item_paquete
(
	idopcion_hoteles_paquete INT(11),
	iditem_hotel_paquete 	INT(11),
	PRIMARY KEY(idopcion_hoteles_paquete, iditem_hotel_paquete),
	FOREIGN KEY(idopcion_hoteles_paquete) REFERENCES opcion_hoteles_paquete(idopcion_hoteles_paquete)
);

CREATE TABLE tarifa_otros_paquete
(
	idtarifa_otros_paquete 	INT(11) AUTO_INCREMENT PRIMARY KEY,
	idpaquete 				INT(11),
	concepto 				VARCHAR(1) DEFAULT 'O' COMMENT  'S:Seguro  T:Traslado  I:Inscripcion  O:Otros  A:Auto  F:Fee  C:Crucero',
	precio_adulto 			FLOAT DEFAULT 0,
	precio_menor 			FLOAT DEFAULT 0,
	precio_infante 			FLOAT DEFAULT 0,
	fee 					FLOAT DEFAULT 0,
	factura 				FLOAT DEFAULT 0,
	moneda 					VARCHAR(1) DEFAULT 'U' COMMENT 'B:Bolivianos  U:USD',
	observacion 			TEXT,
	FOREIGN KEY(idpaquete) REFERENCES paquete(idpublicacion)
);

CREATE TABLE boleto_cotizado_paquete
(
	idboleto_cotizado_paquete 	INT(11) AUTO_INCREMENT PRIMARY KEY,
	idpaquete 					INT(11),
	orden 						TINYINT DEFAULT 1,
	ruta 						VARCHAR(100),
	precio_adulto 				FLOAT DEFAULT 0,
	precio_menor 				FLOAT DEFAULT 0,
	precio_infante 				FLOAT DEFAULT 0,
	fee 						FLOAT DEFAULT 0,
	factura 					FLOAT DEFAULT 0,
	moneda 						VARCHAR(1) DEFAULT 'U' COMMENT 'B:Bolivianos  U:USD',
	observacion 				TEXT,
	FOREIGN KEY(idpaquete) REFERENCES paquete(idpublicacion)
);

CREATE TABLE segmento_paquete
(
	idsegmento_paquete 	INT(11) AUTO_INCREMENT PRIMARY KEY,
	idpaquete 			INT(11),
	orden 				TINYINT DEFAULT 1,
	idlineas_aereas 	INT(11),
	nro_vuelo 			INT(11) DEFAULT 0,
	fecha 				DATE DEFAULT '0000-00-00',
	origen 				VARCHAR(5),
	hora_sale 			TIME DEFAULT '00:00:00',
	destino 			VARCHAR(5),
	hora_llega 			TIME DEFAULT '00:00:00',
	FOREIGN KEY(idpaquete) REFERENCES paquete(idpublicacion),
	FOREIGN KEY(idlineas_aereas) REFERENCES lineas_aereas(idlineas_aereas)
);

/*
* PAQUETES COTIZADOS
*/

CREATE TABLE paquete_cotizado
(
	idpaquete_cotizado 	INT(11) AUTO_INCREMENT PRIMARY KEY,
	fecha_cotizacion 	DATETIME,
	idpaquete 			INT(11),
	paquete_incluye 	TEXT,
	importante 			TEXT,
	observaciones 		TEXT,
	idclientes 			INT(11),
	idusuarios 			INT(11),
	estado_cotizacion 	VARCHAR(1) DEFAULT 'C' COMMENT 'C:cotizacion  V:vendido  A:anulado',
	idsolicitante 		INT(11),
	FOREIGN KEY(idpaquete) REFERENCES paquete(idpublicacion),
	FOREIGN KEY(idclientes) REFERENCES clientes(idclientes),
	FOREIGN KEY(idusuarios) REFERENCES usuarios(idusuarios),
	FOREIGN KEY(idsolicitante) REFERENCES clientes(idclientes)
);

/*
* PRECIOS PARA COTIZACIONES_paquete
*/
CREATE TABLE tarifa_hotel_paquete_cotizado
(
	idtarifa_hotel_paquete_cotizado 	INT(11) AUTO_INCREMENT PRIMARY KEY,
	idpaquete_cotizado 					INT(11),
	destino 							VARCHAR(100),
	comision 							FLOAT,
	cant_noches							INT(11),
	FOREIGN KEY(idpaquete_cotizado) REFERENCES paquete_cotizado(idpaquete_cotizado)
);

CREATE TABLE item_hotel_paquete_cotizado
(
	iditem_hotel_paquete_cotizado 	INT(11) AUTO_INCREMENT PRIMARY KEY,
	idtarifa_hotel_paquete_cotizado INT(11),
	idhotel 						INT(11),
	link 							VARCHAR(100),
	precio_single 					FLOAT DEFAULT 0,
	precio_doble 					FLOAT DEFAULT 0,
	precio_triple 					FLOAT DEFAULT 0,
	precio_cuadruple 				FLOAT DEFAULT 0,
	precio_menor 					FLOAT DEFAULT 0,
	precio_infante 					FLOAT DEFAULT 0,
	observacion 					TEXT,
	FOREIGN KEY(idtarifa_hotel_paquete_cotizado) REFERENCES tarifa_hotel_paquete_cotizado(idtarifa_hotel_paquete_cotizado),
	FOREIGN KEY(idhotel) REFERENCES hotel(idhotel)
);

CREATE TABLE opcion_hoteles_paquete_cotizado
(
	idopcion_hoteles_paquete_cotizado 	INT(11) AUTO_INCREMENT PRIMARY KEY,
	idpaquete_cotizado 					INT(11),
	observacion            				TEXT,
	FOREIGN KEY(idpaquete_cotizado) REFERENCES paquete_cotizado(idpaquete_cotizado)
);

CREATE TABLE opcion_hoteles_has_item_paquete_cotizado
(
	idopcion_hoteles_paquete_cotizado 	INT(11),
	iditem_hotel_paquete_cotizado 		INT(11),
	PRIMARY KEY(idopcion_hoteles_paquete_cotizado, iditem_hotel_paquete_cotizado),
	FOREIGN KEY(idopcion_hoteles_paquete_cotizado) REFERENCES opcion_hoteles_paquete_cotizado(idopcion_hoteles_paquete_cotizado)
);

CREATE TABLE tarifa_otros_paquete_cotizado
(
	idtarifa_otros_paquete_cotizado INT(11) AUTO_INCREMENT PRIMARY KEY,
	idpaquete_cotizado 				INT(11),
	concepto 						VARCHAR(1) DEFAULT 'O' COMMENT  'S:Seguro  T:Traslado  I:Inscripcion  O:Otros  A:Auto  F:Fee  C:Crucero',
	precio_adulto 					FLOAT DEFAULT 0,
	precio_menor 					FLOAT DEFAULT 0,
	precio_infante 					FLOAT DEFAULT 0,
	fee 							FLOAT DEFAULT 0,
	factura 						FLOAT DEFAULT 0,
	moneda 							VARCHAR(1) DEFAULT 'U' COMMENT 'B:Bolivianos  U:USD',
	observacion 					TEXT,
	FOREIGN KEY(idpaquete_cotizado) REFERENCES paquete_cotizado(idpaquete_cotizado)
);

CREATE TABLE boleto_cotizado_paquete_cotizado
(
	idboleto_cotizado_paquete_cotizado 	INT(11) AUTO_INCREMENT PRIMARY KEY,
	idpaquete_cotizado 					INT(11),
	orden 								TINYINT DEFAULT 1,
	ruta 								VARCHAR(100),
	precio_adulto 						FLOAT DEFAULT 0,
	precio_menor 						FLOAT DEFAULT 0,
	precio_infante 						FLOAT DEFAULT 0,
	fee 								FLOAT DEFAULT 0,
	factura 							FLOAT DEFAULT 0,
	moneda 								VARCHAR(1) DEFAULT 'U' COMMENT 'B:Bolivianos  U:USD',
	observacion 						TEXT,
	FOREIGN KEY(idpaquete_cotizado) REFERENCES paquete_cotizado(idpaquete_cotizado)
);

CREATE TABLE segmento_paquete_cotizado
(
	idsegmento_paquete_cotizado INT(11) AUTO_INCREMENT PRIMARY KEY,
	idpaquete_cotizado 			INT(11),
	orden 						TINYINT DEFAULT 1,
	idlineas_aereas 			INT(11),
	nro_vuelo 					INT(11) DEFAULT 0,
	fecha 						DATE DEFAULT '0000-00-00',
	origen 						VARCHAR(5),
	hora_sale 					TIME DEFAULT '00:00:00',
	destino 					VARCHAR(5),
	hora_llega 					TIME DEFAULT '00:00:00',
	FOREIGN KEY(idpaquete_cotizado) REFERENCES paquete_cotizado(idpaquete_cotizado),
	FOREIGN KEY(idlineas_aereas) REFERENCES lineas_aereas(idlineas_aereas)
);


