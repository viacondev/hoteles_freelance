/*
* FORMATO ANTERIOR
*/

	/*
	* TABLAS DE ADMINISTRACION
	*/

		CREATE TABLE hotel
		(
			idhotel INT(11) AUTO_INCREMENT PRIMARY KEY,
			nombre_hotel TEXT,
			categoria INT(1)
		);

		CREATE TABLE pqt_hotel
		(
			idhotel 		INT(11) AUTO_INCREMENT PRIMARY KEY,
			nombre_hotel 	TEXT,
			categoria 		INT(1),
			ciudad 			VARCHAR(3)
		) ENGINE=InnoDB;

		CREATE TABLE alcance_evento
		(
			idalcance_evento 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			descripcion 		VARCHAR(100)
		);

		CREATE TABLE tipo_evento
		(
			idtipo_evento 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			descripcion 	VARCHAR(100)
		);

	/*
	* EVENTO GENERICO MAS SU DATOS DE ENCABEZADO
	*/

		CREATE TABLE evento 
		(
		  idpublicacion 	INT(11) PRIMARY KEY,
		  nombre_generico 	VARCHAR(30) ,
		  ciudad_origen 	VARCHAR(100),
		  salida 			VARCHAR(50) ,
		  retorno 			VARCHAR(50) ,
		  sede 				VARCHAR(150) ,
		  idoperadora 		INT(11) ,
		  precio_desde 		FLOAT ,
		  cant_noches 		INT(11) ,
		  armado_completo 	VARCHAR(1) DEFAULT 'S' COMMENT 'S:Si N:No',
		  FOREIGN KEY(idpublicacion) REFERENCES publicacion(idpublicacion),
		  FOREIGN KEY(idoperadora) REFERENCES operadora(idoperadora)
		);

		CREATE TABLE evento_has_alcance_evento
		(
			idevento 			INT(11),
			idalcance_evento 	INT(11),
			PRIMARY KEY(idevento, idalcance_evento),
			FOREIGN KEY(idevento) REFERENCES evento(idpublicacion),
			FOREIGN KEY(idalcance_evento) REFERENCES alcance_evento(idalcance_evento)
		);

		CREATE TABLE evento_has_destino
		(
			idevento 	INT(11),
			iddestino 	INT(11),
			PRIMARY KEY(idevento, iddestino),
			FOREIGN KEY(idevento) REFERENCES evento(idpublicacion),
			FOREIGN KEY(iddestino) REFERENCES destino(iddestino)
		);

		CREATE TABLE evento_has_especialidades
		(
			idevento 			INT(11),
			idespecialidades 	INT(11),
			PRIMARY KEY(idevento, idespecialidades),
			FOREIGN KEY(idevento) REFERENCES evento(idpublicacion),
			FOREIGN KEY(idespecialidades) REFERENCES especialidades(idespecialidades)
		);

		CREATE TABLE evento_has_temporada
		(
			idevento 		INT(11),
			idtemporada 	INT(11),
			PRIMARY KEY(idevento, idtemporada),
			FOREIGN KEY(idevento) REFERENCES evento(idpublicacion),
			FOREIGN KEY(idtemporada) REFERENCES temporada(idtemporada)
		);

		CREATE TABLE evento_has_tipo_evento
		(
			idevento 		INT(11),
			idtipo_evento 	INT(11),
			PRIMARY KEY(idevento, idtipo_evento),
			FOREIGN KEY(idevento) REFERENCES evento(idpublicacion),
			FOREIGN KEY(idtipo_evento) REFERENCES tipo_evento(idtipo_evento)
		);

		CREATE TABLE contenido_evento
		(
			idevento 		INT(11) PRIMARY KEY,
			paquete_incluye TEXT,
			importante 		TEXT,
			datos_agente 	TEXT,
			paginas_web 	TEXT,
			solo_imagen 	VARCHAR(1) DEFAULT 'N' COMMENT 'S:si  N:no',
			FOREIGN KEY(idevento) REFERENCES evento(idpublicacion)
		);

	/*
	* MANEJO DE PRECIOS
	*/

		CREATE TABLE tarifa_hotel_evento
		(
			idtarifa_hotel_evento 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			idevento 		INT(11),
			destino 		VARCHAR(100),
			comision 		FLOAT,
			cant_noches		INT(11),
			in              DATE,
			out 			DATE,
			idpublicacion_has_ciudad_origen INT(11),
			FOREIGN KEY(idevento) REFERENCES evento(idpublicacion),
			FOREIGN KEY(idpublicacion_has_ciudad_origen) REFERENCES pqt_publicacion_has_ciudad_origen(idpublicacion_has_ciudad_origen)
		);

		CREATE TABLE item_hotel_evento
		(
			iditem_hotel_evento 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			idtarifa_hotel_evento 	INT(11),
			identificador 			VARCHAR(50),
			idhotel 				INT(11),
			link 					VARCHAR(100),
			precio_single 			FLOAT DEFAULT 0,
			precio_doble 			FLOAT DEFAULT 0,
			precio_triple 			FLOAT DEFAULT 0,
			precio_cuadruple 		FLOAT DEFAULT 0,
			precio_menor 			FLOAT DEFAULT 0,
			precio_infante 			FLOAT DEFAULT 0,
			observacion 			TEXT,
			alimentacion 			TEXT,
			comision 				FLOAT DEFAULT 0,
			factura  				FLOAT,
			moneda 					VARCHAR(1) DEFAULT 'U' COMMENT 'B:Bolivianos U:USD',
			tipo_cambio 			FLOAT,
			time_limit 				DATETIME,
			info_extra 				TEXT,
			estado 					CHAR(2) DEFAULT '1' COMMENT '1:activo 0:inactivo',
			FOREIGN KEY(idtarifa_hotel_evento) REFERENCES tarifa_hotel_evento(idtarifa_hotel_evento),
			FOREIGN KEY(idhotel) REFERENCES hotel(idhotel)
		);

		CREATE TABLE opcion_hoteles_evento
		(
			idopcion_hoteles_evento INT(11) AUTO_INCREMENT PRIMARY KEY,
			idevento 				INT(11),
			observacion            	TEXT,
			FOREIGN KEY(idevento) REFERENCES evento(idevento)
		);

		CREATE TABLE opcion_hoteles_has_item_evento
		(
			idopcion_hoteles_evento INT(11),
			iditem_hotel_evento 	INT(11),
			PRIMARY KEY(idopcion_hoteles_evento, iditem_hotel_evento),
			FOREIGN KEY(idopcion_hoteles_evento) REFERENCES opcion_hoteles_evento(idopcion_hoteles_evento)
		);

		CREATE TABLE tarifa_otros_evento
		(
			idtarifa_otros_evento 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			idevento 				INT(11),
			/*concepto 				VARCHAR(1) DEFAULT 'O' COMMENT  'S:Seguro  T:Traslado  I:Inscripcion  O:Otros  A:Auto  F:Fee  C:Crucero',*/
			codigo 					VARCHAR(50),
			idotro_servicio         INT(11),
			descripcion 			VARCHAR(100),
			precio_adulto 			FLOAT DEFAULT 0,
			precio_menor 			FLOAT DEFAULT 0,
			precio_infante 			FLOAT DEFAULT 0,
			fee 					FLOAT DEFAULT 0,
			factura 				FLOAT DEFAULT 0,
			moneda 					VARCHAR(1) DEFAULT 'U' COMMENT 'B:Bolivianos  U:USD',
			tipo_cambio 			FLOAT,
			time_limit 				DATETIME,
			observacion 			TEXT,
			idpublicacion_has_ciudad_origen INT(11),
			estado 					CHAR(2) DEFAULT '1' COMMENT '1:activo 0:inactivo',
			FOREIGN KEY(idevento) REFERENCES evento(idevento),
			FOREIGN KEY(idotro_servicio) REFERENCES pqt_otro_servicio(idotro_servicio),
			FOREIGN KEY(idpublicacion_has_ciudad_origen) REFERENCES pqt_publicacion_has_ciudad_origen(idpublicacion_has_ciudad_origen)
		);

		CREATE TABLE pqt_opcion_boleto_evento
		(
			idopcion_boleto_evento INT(11) AUTO_INCREMENT PRIMARY KEY,
			idevento 		INT(11),
			salida VARCHAR(50),
			descripcion VARCHAR(100),
			idpublicacion_has_ciudad_origen INT(11),
			estado VARCHAR(2),
			FOREIGN KEY(idevento) REFERENCES evento(idpublicacion),
			FOREIGN KEY(idpublicacion_has_ciudad_origen) REFERENCES pqt_publicacion_has_ciudad_origen(idpublicacion_has_ciudad_origen)
		);

		CREATE TABLE boleto_cotizado_evento
		(
			idboleto_cotizado_evento 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			idevento 					INT(11),
			idopcion_boleto_evento 		INT(11),
			orden 						TINYINT DEFAULT 1,
			codigo_reserva 				VARCHAR(10) COMMENT 'Codigo Sabre o de Otra Linea Aerea',
			linea_aerea 				INT(11),
			ruta 						VARCHAR(100),
			precio_adulto 				FLOAT DEFAULT 0,
			precio_menor 				FLOAT DEFAULT 0,
			precio_infante 				FLOAT DEFAULT 0,
			fee 						FLOAT DEFAULT 0,
			factura 					FLOAT DEFAULT 0,
			moneda 						VARCHAR(1) DEFAULT 'U' COMMENT 'B:Bolivianos  U:USD',
			tipo_cambio 				FLOAT,
			time_limit 					DATETIME,
			observacion 				TEXT,
			estado 						CHAR(2) DEFAULT '1' COMMENT '1:activo 0:inactivo',
			FOREIGN KEY(idevento) REFERENCES evento(idevento),
			FOREIGN KEY(idopcion_boleto_evento) REFERENCES pqt_opcion_boleto_evento(idopcion_boleto_evento),
			FOREIGN KEY(linea_aerea) REFERENCES lineas_aereas(idlineas_aereas)
		);

		CREATE TABLE segmento_evento
		(
			idsegmento_evento 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			idevento 			INT(11),
			idopcion_boleto_evento 		INT(11),
			codigo 				VARCHAR(10),
			orden 				TINYINT DEFAULT 1,
			idlineas_aereas 	INT(11),
			nro_vuelo 			INT(11) DEFAULT 0,
			fecha 				DATE DEFAULT '0000-00-00',
			origen 				VARCHAR(5),
			hora_sale 			TIME DEFAULT '00:00:00',
			destino 			VARCHAR(5),
			hora_llega 			TIME DEFAULT '00:00:00',
			FOREIGN KEY(idevento) REFERENCES evento(idevento),
			FOREIGN KEY(idopcion_boleto_evento) REFERENCES pqt_opcion_boleto_evento(idopcion_boleto_evento),
			FOREIGN KEY(idlineas_aereas) REFERENCES lineas_aereas(idlineas_aereas)
		);

	/*
	* EVENTO COTIZADO
	*/
		CREATE TABLE evento_cotizado
		(
			idevento_cotizado 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			fecha_cotizacion 	DATETIME,
			idevento 			INT(11),
			salida 				VARCHAR(100),
			retorno 			VARCHAR(100),
			paquete_incluye 	TEXT,
			importante 			TEXT,
			observaciones 		TEXT,
			idclientes 			INT(11),
			idusuarios 			INT(11),
			estado_cotizacion 	VARCHAR(1) DEFAULT 'C' COMMENT 'C:cotizacion  V:vendido  A:anulado',
			idsolicitante 		INT(11),
			FOREIGN KEY(idevento) REFERENCES evento(idevento),
			FOREIGN KEY(idclientes) REFERENCES clientes(idclientes),
			FOREIGN KEY(idusuarios) REFERENCES usuarios(idusuarios),
			FOREIGN KEY(idsolicitante) REFERENCES clientes(idclientes)
		);

	/*
	* PRECIOS PARA COTIZACIONES_EVENTO
	*/
		CREATE TABLE tarifa_hotel_evento_cotizado
		(
			idtarifa_hotel_evento_cotizado 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			idevento_cotizado 				INT(11),
			destino 						VARCHAR(100),
			comision 						FLOAT,
			cant_noches						INT(11),
			in              				DATE,
			out 							DATE,
			idpublicacion_has_ciudad_origen INT(11),
			FOREIGN KEY(idevento_cotizado) REFERENCES evento_cotizado(idevento_cotizado),
			FOREIGN KEY(idpublicacion_has_ciudad_origen) REFERENCES pqt_publicacion_has_ciudad_origen(idpublicacion_has_ciudad_origen)
		);

		CREATE TABLE item_hotel_evento_cotizado
		(
			iditem_hotel_evento_cotizado 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			idtarifa_hotel_evento_cotizado 	INT(11),
			identificador 					VARCHAR(50),
			idhotel 						INT(11),
			link 							VARCHAR(100),
			precio_single 					FLOAT DEFAULT 0,
			precio_doble 					FLOAT DEFAULT 0,
			precio_triple 					FLOAT DEFAULT 0,
			precio_cuadruple 				FLOAT DEFAULT 0,
			precio_menor 					FLOAT DEFAULT 0,
			precio_infante 					FLOAT DEFAULT 0,
			observacion 					TEXT,
			alimentacion 					TEXT,
			comision 						FLOAT DEFAULT 0,
			factura  						FLOAT,
			moneda 							VARCHAR(1) DEFAULT 'U' COMMENT 'B:Bolivianos U:USD',
			tipo_cambio 					FLOAT,
			time_limit 						DATETIME,
			info_extra 						TEXT,
			estado 							CHAR(2) DEFAULT '1' COMMENT '1:activo 0:inactivo',
			FOREIGN KEY(idtarifa_hotel_evento_cotizado) REFERENCES tarifa_hotel_evento_cotizado(idtarifa_hotel_evento_cotizado),
			FOREIGN KEY(idhotel) REFERENCES hotel(idhotel)
		);

		CREATE TABLE opcion_hoteles_evento_cotizado
		(
			idopcion_hoteles_evento_cotizado 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			idevento_cotizado 					INT(11),
			observacion            				TEXT,
			FOREIGN KEY(idevento_cotizado) REFERENCES evento_cotizado(idevento_cotizado)
		);

		CREATE TABLE opcion_hoteles_has_item_evento_cotizado
		(
			idopcion_hoteles_evento_cotizado 	INT(11),
			iditem_hotel_evento_cotizado 		INT(11),
			PRIMARY KEY(idopcion_hoteles_evento_cotizado, iditem_hotel_evento_cotizado),
			FOREIGN KEY(idopcion_hoteles_evento_cotizado) REFERENCES opcion_hoteles_evento_cotizado(idopcion_hoteles_evento_cotizado)
		);

		CREATE TABLE tarifa_otros_evento_cotizado
		(
			idtarifa_otros_evento_cotizado 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			idevento_cotizado 				INT(11),
			/*concepto 						VARCHAR(1) DEFAULT 'O' COMMENT  'S:Seguro  T:Traslado  I:Inscripcion  O:Otros  A:Auto  F:Fee  C:Crucero',*/
			codigo 							VARCHAR(50),
			idotro_servicio         		INT(11),
			descripcion 					VARCHAR(100),
			precio_adulto 					FLOAT DEFAULT 0,
			precio_menor 					FLOAT DEFAULT 0,
			precio_infante 					FLOAT DEFAULT 0,
			fee 							FLOAT DEFAULT 0,
			factura 						FLOAT DEFAULT 0,
			moneda 							VARCHAR(1) DEFAULT 'U' COMMENT 'B:Bolivianos  U:USD',
			tipo_cambio 					FLOAT,
			time_limit 						DATETIME,
			observacion 					TEXT,
			idpublicacion_has_ciudad_origen INT(11),
			estado 							CHAR(2) DEFAULT '1' COMMENT '1:activo 0:inactivo',
			FOREIGN KEY(idevento_cotizado) REFERENCES evento_cotizado(idevento_cotizado),
			FOREIGN KEY(idotro_servicio) REFERENCES pqt_otro_servicio(idotro_servicio),
			FOREIGN KEY(idpublicacion_has_ciudad_origen) REFERENCES pqt_publicacion_has_ciudad_origen(idpublicacion_has_ciudad_origen)
		);

		CREATE TABLE pqt_opcion_boleto_evento_cotizado
		(
			idopcion_boleto_evento_cotizado INT(11) AUTO_INCREMENT PRIMARY KEY,
			idevento_cotizado INT(11),
			salida VARCHAR(50),
			descripcion VARCHAR(100),
			idpublicacion_has_ciudad_origen INT(11),
			estado VARCHAR(2),
			FOREIGN KEY(idevento_cotizado) REFERENCES evento_cotizado(idevento_cotizado),
			FOREIGN KEY(idpublicacion_has_ciudad_origen) REFERENCES pqt_publicacion_has_ciudad_origen(idpublicacion_has_ciudad_origen)
		);

		CREATE TABLE boleto_cotizado_evento_cotizado
		(
			idboleto_cotizado_evento_cotizado 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			idevento_cotizado 					INT(11),
			idopcion_boleto_evento_cotizado 	INT(11),
			orden 								TINYINT DEFAULT 1,
			codigo_reserva 						VARCHAR(10) COMMENT 'Codigo Sabre o de Otra Linea Aerea',
			linea_aerea 						INT(11),
			ruta 								VARCHAR(100),
			precio_adulto 						FLOAT DEFAULT 0,
			precio_menor 						FLOAT DEFAULT 0,
			precio_infante 						FLOAT DEFAULT 0,
			fee 								FLOAT DEFAULT 0,
			factura 							FLOAT DEFAULT 0,
			moneda 								VARCHAR(1) DEFAULT 'U' COMMENT 'B:Bolivianos  U:USD',
			tipo_cambio 						FLOAT,
			time_limit 							DATETIME,
			observacion 						TEXT,
			estado 								CHAR(2) DEFAULT '1' COMMENT '1:activo 0:inactivo',
			FOREIGN KEY(idevento_cotizado) REFERENCES evento_cotizado(idevento_cotizado),
			FOREIGN KEY(idopcion_boleto_evento_cotizado) REFERENCES pqt_opcion_boleto_evento_cotizado(idopcion_boleto_evento_cotizado),
			FOREIGN KEY(linea_aerea) REFERENCES lineas_aereas(idlineas_aereas)
		);

		CREATE TABLE segmento_evento_cotizado
		(
			idsegmento_evento_cotizado 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			idevento_cotizado 			INT(11),
			idopcion_boleto_evento_cotizado 	INT(11),
			codigo 						VARCHAR(10),
			orden 						TINYINT DEFAULT 1,
			idlineas_aereas 			INT(11),
			nro_vuelo 					INT(11) DEFAULT 0,
			fecha 						DATE DEFAULT '0000-00-00',
			origen 						VARCHAR(5),
			hora_sale 					TIME DEFAULT '00:00:00',
			destino 					VARCHAR(5),
			hora_llega 					TIME DEFAULT '00:00:00',
			FOREIGN KEY(idevento_cotizado) REFERENCES evento_cotizado(idevento_cotizado),
			FOREIGN KEY(idopcion_boleto_evento_cotizado) REFERENCES pqt_opcion_boleto_evento_cotizado(idopcion_boleto_evento_cotizado),
			FOREIGN KEY(idlineas_aereas) REFERENCES lineas_aereas(idlineas_aereas)
		);

		CREATE TABLE pqt_inscripcion_cotizado
		(
			idinscripcion_cotizado  	INT(11) AUTO_INCREMENT PRIMARY KEY,
			categoria  					VARCHAR(100),
			precio 						FLOAT,
			moneda 						VARCHAR(1) DEFAULT 'U' COMMENT 'B:Bolivianos  U:USD',
			fee 						FLOAT DEFAULT 0,
			factura 					FLOAT DEFAULT 0,
			observacion 				TEXT,
			time_limit 					DATETIME,
			estado 						CHAR(2) DEFAULT '1' COMMENT '1:activo 0:inactivo',
			idevento_cotizado 			INT(11),
			tipo_cambio 				FLOAT,
			FOREIGN KEY(idevento_cotizado) REFERENCES evento_cotizado(idevento_cotizado)
		);


	/*
	* PAQUETES PRECIOS DE SERVICIOS
	*/

		CREATE TABLE tarifa_hotel_paquete
		(
			idtarifa_hotel_paquete 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			idpaquete 				INT(11),
			destino 				VARCHAR(100),
			comision 				FLOAT,
			cant_noches				INT(11),
			in              		DATE,
			out 					DATE,
			idpublicacion_has_ciudad_origen INT(11),
			FOREIGN KEY(idpaquete) REFERENCES paquete(idpublicacion),
			FOREIGN KEY(idpublicacion_has_ciudad_origen) REFERENCES pqt_publicacion_has_ciudad_origen(idpublicacion_has_ciudad_origen)
		);

		CREATE TABLE item_hotel_paquete
		(
			iditem_hotel_paquete 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			idtarifa_hotel_paquete 	INT(11),
			identificador 			VARCHAR(50),
			idhotel 				INT(11),
			link 					VARCHAR(100),
			precio_single 			FLOAT DEFAULT 0,
			precio_doble 			FLOAT DEFAULT 0,
			precio_triple 			FLOAT DEFAULT 0,
			precio_cuadruple 		FLOAT DEFAULT 0,
			precio_menor 			FLOAT DEFAULT 0,
			precio_infante 			FLOAT DEFAULT 0,
			observacion 			TEXT,
			alimentacion 			TEXT,
			comision 				FLOAT DEFAULT 0,
			factura  				FLOAT,
			moneda 					VARCHAR(1) DEFAULT 'U' COMMENT 'B:Bolivianos U:USD',
			tipo_cambio 			FLOAT,
			time_limit 				DATETIME,
			info_extra 				TEXT,
			estado 					CHAR(2) DEFAULT '1' COMMENT '1:activo 0:inactivo',
			FOREIGN KEY(idtarifa_hotel_paquete) REFERENCES tarifa_hotel_paquete(idtarifa_hotel_paquete),
			FOREIGN KEY(idhotel) REFERENCES hotel(idhotel)
		);

		CREATE TABLE opcion_hoteles_paquete
		(
			idopcion_hoteles_paquete 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			idpaquete 					INT(11),
			observacion            		TEXT,
			FOREIGN KEY(idpaquete) REFERENCES paquete(idpublicacion)
		);

		CREATE TABLE opcion_hoteles_has_item_paquete
		(
			idopcion_hoteles_paquete INT(11),
			iditem_hotel_paquete 	INT(11),
			PRIMARY KEY(idopcion_hoteles_paquete, iditem_hotel_paquete),
			FOREIGN KEY(idopcion_hoteles_paquete) REFERENCES opcion_hoteles_paquete(idopcion_hoteles_paquete)
		);

		CREATE TABLE tarifa_otros_paquete
		(
			idtarifa_otros_paquete 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			idpaquete 				INT(11),
			/*concepto 				VARCHAR(1) DEFAULT 'O' COMMENT  'S:Seguro  T:Traslado  I:Inscripcion  O:Otros  A:Auto  F:Fee  C:Crucero',*/
			codigo 					VARCHAR(50),
			idotro_servicio         INT(11),
			descripcion 			VARCHAR(100),
			precio_adulto 			FLOAT DEFAULT 0,
			precio_menor 			FLOAT DEFAULT 0,
			precio_infante 			FLOAT DEFAULT 0,
			fee 					FLOAT DEFAULT 0,
			factura 				FLOAT DEFAULT 0,
			moneda 					VARCHAR(1) DEFAULT 'U' COMMENT 'B:Bolivianos  U:USD',
			tipo_cambio 			FLOAT,
			time_limit 				DATETIME,
			observacion 			TEXT,
			idpublicacion_has_ciudad_origen INT(11),
			estado 					CHAR(2) DEFAULT '1' COMMENT '1:activo 0:inactivo',
			FOREIGN KEY(idpaquete) REFERENCES paquete(idpublicacion),
			FOREIGN KEY(idotro_servicio) REFERENCES pqt_otro_servicio(idotro_servicio),
			FOREIGN KEY(idpublicacion_has_ciudad_origen) REFERENCES pqt_publicacion_has_ciudad_origen(idpublicacion_has_ciudad_origen)
		);

		CREATE TABLE pqt_opcion_boleto_paquete
		(
			idopcion_boleto_paquete 		INT(11) AUTO_INCREMENT PRIMARY KEY,
			idpaquete 						INT(11),
			salida 							VARCHAR(50),
			descripcion 					VARCHAR(100),
			idpublicacion_has_ciudad_origen INT(11),
			estado 							VARCHAR(2),
			FOREIGN KEY(idpaquete) REFERENCES paquete(idpublicacion),
			FOREIGN KEY(idpublicacion_has_ciudad_origen) REFERENCES pqt_publicacion_has_ciudad_origen(idpublicacion_has_ciudad_origen)
		);

		CREATE TABLE boleto_cotizado_paquete
		(
			idboleto_cotizado_paquete 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			idpaquete 					INT(11),
			idopcion_boleto_paquete 	INT(11),
			orden 						TINYINT DEFAULT 1,
			codigo_reserva 				VARCHAR(10) COMMENT 'Codigo Sabre o de Otra Linea Aerea',
			linea_aerea 				INT(11),
			ruta 						VARCHAR(100),
			precio_adulto 				FLOAT DEFAULT 0,
			precio_menor 				FLOAT DEFAULT 0,
			precio_infante 				FLOAT DEFAULT 0,
			fee 						FLOAT DEFAULT 0,
			factura 					FLOAT DEFAULT 0,
			moneda 						VARCHAR(1) DEFAULT 'U' COMMENT 'B:Bolivianos  U:USD',
			tipo_cambio 				FLOAT,
			time_limit 					DATETIME,
			observacion 				TEXT,
			estado 						CHAR(2) DEFAULT '1' COMMENT '1:activo 0:inactivo',
			FOREIGN KEY(idpaquete) REFERENCES paquete(idpublicacion),
			FOREIGN KEY(idopcion_boleto_paquete) REFERENCES pqt_opcion_boleto_paquete(idopcion_boleto_paquete),
			FOREIGN KEY(linea_aerea) REFERENCES lineas_aereas(idlineas_aereas)
		);

		CREATE TABLE segmento_paquete
		(
			idsegmento_paquete 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			idpaquete 			INT(11),
			idopcion_boleto_paquete 	INT(11),
			codigo 				VARCHAR(10),
			orden 				TINYINT DEFAULT 1,
			idlineas_aereas 	INT(11),
			nro_vuelo 			INT(11) DEFAULT 0,
			fecha 				DATE DEFAULT '0000-00-00',
			origen 				VARCHAR(5),
			hora_sale 			TIME DEFAULT '00:00:00',
			destino 			VARCHAR(5),
			hora_llega 			TIME DEFAULT '00:00:00',
			FOREIGN KEY(idpaquete) REFERENCES paquete(idpublicacion),
			FOREIGN KEY(idopcion_boleto_paquete) REFERENCES pqt_opcion_boleto_paquete(idopcion_boleto_paquete),
			FOREIGN KEY(idlineas_aereas) REFERENCES lineas_aereas(idlineas_aereas)
		);

	/*
	* PAQUETES COTIZADOS
	*/

		CREATE TABLE paquete_cotizado
		(
			idpaquete_cotizado 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			fecha_cotizacion 	DATETIME,
			idpaquete 			INT(11),
			salida 				VARCHAR(100),
			retorno 			VARCHAR(100),
			paquete_incluye 	TEXT,
			importante 			TEXT,
			observaciones 		TEXT,
			idclientes 			INT(11),
			idusuarios 			INT(11),
			estado_cotizacion 	VARCHAR(1) DEFAULT 'C' COMMENT 'C:cotizacion  V:vendido  A:anulado',
			idsolicitante 		INT(11),
			FOREIGN KEY(idpaquete) REFERENCES paquete(idpublicacion),
			FOREIGN KEY(idclientes) REFERENCES clientes(idclientes),
			FOREIGN KEY(idusuarios) REFERENCES usuarios(idusuarios),
			FOREIGN KEY(idsolicitante) REFERENCES clientes(idclientes)
		);

	/*
	* PRECIOS PARA COTIZACIONES_paquete
	*/
		CREATE TABLE tarifa_hotel_paquete_cotizado
		(
			idtarifa_hotel_paquete_cotizado 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			idpaquete_cotizado 					INT(11),
			destino 							VARCHAR(100),
			comision 							FLOAT,
			cant_noches							INT(11),
			in              					DATE,
			out 								DATE,
			idpublicacion_has_ciudad_origen 	INT(11),
			FOREIGN KEY(idpaquete_cotizado) REFERENCES paquete_cotizado(idpaquete_cotizado),
			FOREIGN KEY(idpublicacion_has_ciudad_origen) REFERENCES pqt_publicacion_has_ciudad_origen(idpublicacion_has_ciudad_origen)
		);

		CREATE TABLE item_hotel_paquete_cotizado
		(
			iditem_hotel_paquete_cotizado 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			idtarifa_hotel_paquete_cotizado INT(11),
			identificador 					VARCHAR(50),
			idhotel 						INT(11),
			link 							VARCHAR(100),
			precio_single 					FLOAT DEFAULT 0,
			precio_doble 					FLOAT DEFAULT 0,
			precio_triple 					FLOAT DEFAULT 0,
			precio_cuadruple 				FLOAT DEFAULT 0,
			precio_menor 					FLOAT DEFAULT 0,
			precio_infante 					FLOAT DEFAULT 0,
			observacion 					TEXT,
			alimentacion 					TEXT,
			comision 						FLOAT DEFAULT 0,
			factura  						FLOAT,
			moneda 							VARCHAR(1) DEFAULT 'U' COMMENT 'B:Bolivianos U:USD',
			tipo_cambio 					FLOAT,
			time_limit 						DATETIME,
			info_extra 						TEXT,
			estado 							CHAR(2) DEFAULT '1' COMMENT '1:activo 0:inactivo',
			FOREIGN KEY(idtarifa_hotel_paquete_cotizado) REFERENCES tarifa_hotel_paquete_cotizado(idtarifa_hotel_paquete_cotizado),
			FOREIGN KEY(idhotel) REFERENCES hotel(idhotel)
		);

		CREATE TABLE opcion_hoteles_paquete_cotizado
		(
			idopcion_hoteles_paquete_cotizado 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			idpaquete_cotizado 					INT(11),
			observacion            				TEXT,
			FOREIGN KEY(idpaquete_cotizado) REFERENCES paquete_cotizado(idpaquete_cotizado)
		);

		CREATE TABLE opcion_hoteles_has_item_paquete_cotizado
		(
			idopcion_hoteles_paquete_cotizado 	INT(11),
			iditem_hotel_paquete_cotizado 		INT(11),
			PRIMARY KEY(idopcion_hoteles_paquete_cotizado, iditem_hotel_paquete_cotizado),
			FOREIGN KEY(idopcion_hoteles_paquete_cotizado) REFERENCES opcion_hoteles_paquete_cotizado(idopcion_hoteles_paquete_cotizado)
		);

		CREATE TABLE tarifa_otros_paquete_cotizado
		(
			idtarifa_otros_paquete_cotizado INT(11) AUTO_INCREMENT PRIMARY KEY,
			idpaquete_cotizado 				INT(11),
			/*concepto 						VARCHAR(1) DEFAULT 'O' COMMENT  'S:Seguro  T:Traslado  I:Inscripcion  O:Otros  A:Auto  F:Fee  C:Crucero',*/
			codigo 							VARCHAR(50),
			idotro_servicio         		INT(11),
			descripcion 					VARCHAR(100),
			precio_adulto 					FLOAT DEFAULT 0,
			precio_menor 					FLOAT DEFAULT 0,
			precio_infante 					FLOAT DEFAULT 0,
			fee 							FLOAT DEFAULT 0,
			factura 						FLOAT DEFAULT 0,
			moneda 							VARCHAR(1) DEFAULT 'U' COMMENT 'B:Bolivianos  U:USD',
			tipo_cambio 					FLOAT,
			time_limit 						DATETIME,
			observacion 					TEXT,
			idpublicacion_has_ciudad_origen INT(11),
			estado 							CHAR(2) DEFAULT '1' COMMENT '1:activo 0:inactivo',
			FOREIGN KEY(idpaquete_cotizado) REFERENCES paquete_cotizado(idpaquete_cotizado),
			FOREIGN KEY(idotro_servicio) REFERENCES pqt_otro_servicio(idotro_servicio),
			FOREIGN KEY(idpublicacion_has_ciudad_origen) REFERENCES pqt_publicacion_has_ciudad_origen(idpublicacion_has_ciudad_origen)
		);

		CREATE TABLE pqt_opcion_boleto_paquete_cotizado
		(
			idopcion_boleto_paquete_cotizado 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			idpaquete_cotizado 					INT(11),
			salida 								VARCHAR(50),
			descripcion 						VARCHAR(100),
			idpublicacion_has_ciudad_origen 	INT(11),
			estado 								VARCHAR(2),
			FOREIGN KEY(idpaquete_cotizado) REFERENCES paquete_cotizado(idpaquete_cotizado),
			FOREIGN KEY(idpublicacion_has_ciudad_origen) REFERENCES pqt_publicacion_has_ciudad_origen(idpublicacion_has_ciudad_origen)
		);

		CREATE TABLE boleto_cotizado_paquete_cotizado
		(
			idboleto_cotizado_paquete_cotizado 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			idpaquete_cotizado 					INT(11),
			idopcion_boleto_paquete_cotizado 	INT(11),
			orden 								TINYINT DEFAULT 1,
			codigo_reserva 						VARCHAR(10) COMMENT 'Codigo Sabre o de Otra Linea Aerea',
			linea_aerea 						INT(11),
			ruta 								VARCHAR(100),
			precio_adulto 						FLOAT DEFAULT 0,
			precio_menor 						FLOAT DEFAULT 0,
			precio_infante 						FLOAT DEFAULT 0,
			fee 								FLOAT DEFAULT 0,
			factura 							FLOAT DEFAULT 0,
			moneda 								VARCHAR(1) DEFAULT 'U' COMMENT 'B:Bolivianos  U:USD',
			tipo_cambio 						FLOAT,
			time_limit 							DATETIME,
			observacion 						TEXT,
			estado 								CHAR(2) DEFAULT '1' COMMENT '1:activo 0:inactivo',
			FOREIGN KEY(idpaquete_cotizado) REFERENCES paquete_cotizado(idpaquete_cotizado),
			FOREIGN KEY(idopcion_boleto_paquete_cotizado) REFERENCES pqt_opcion_boleto_paquete_cotizado(idopcion_boleto_paquete_cotizado),
			FOREIGN KEY(linea_aerea) REFERENCES lineas_aereas(idlineas_aereas)
		);

		CREATE TABLE segmento_paquete_cotizado
		(
			idsegmento_paquete_cotizado INT(11) AUTO_INCREMENT PRIMARY KEY,
			idpaquete_cotizado 			INT(11),
			idopcion_boleto_paquete_cotizado 	INT(11),
			codigo 						VARCHAR(10),
			orden 						TINYINT DEFAULT 1,
			idlineas_aereas 			INT(11),
			nro_vuelo 					INT(11) DEFAULT 0,
			fecha 						DATE DEFAULT '0000-00-00',
			origen 						VARCHAR(5),
			hora_sale 					TIME DEFAULT '00:00:00',
			destino 					VARCHAR(5),
			hora_llega 					TIME DEFAULT '00:00:00',
			FOREIGN KEY(idpaquete_cotizado) REFERENCES paquete_cotizado(idpaquete_cotizado),
			FOREIGN KEY(idopcion_boleto_paquete_cotizado) REFERENCES pqt_opcion_boleto_paquete_cotizado(idopcion_boleto_paquete_cotizado),
			FOREIGN KEY(idlineas_aereas) REFERENCES lineas_aereas(idlineas_aereas)
		);

	/*
	* OTROS SERVICIOS
	*/
		CREATE TABLE pqt_otro_servicio
		(
			idotro_servicio INT(11) AUTO_INCREMENT PRIMARY KEY,
			nombre_servicio VARCHAR(100) NOT NULL
		) ENGINE=InnoDB;

	/*
	* ENVIO MASIVO DE CORREOS
	*/
		CREATE TABLE pqt_envio_masivo
		(
			idenvio_masivo 				INT(11) AUTO_INCREMENT PRIMARY KEY,
			fecha_envio 				DATETIME NOT NULL,
			idusuarios  				INT(11) NOT NULL,
			tipo_envio 					VARCHAR(2) DEFAULT 'O' COMMENT 'P:Paquete E:Evento O:Otro',
			contenido 					TEXT,
			observacion 				TEXT,
			cantidad_envios_correctos 	INT(11),
			cantidad_envios_incorrectos INT(11),
			cantidad_clientes_con_mail 	INT(11),
			cantidad_clientes_sin_mail 	INT(11),
			codigo 						INT(11),
			FOREIGN KEY(idusuarios) REFERENCES usuarios(idusuarios)
		);


	/*
	* SEGUIMIENTOS
	*/

		CREATE TABLE pqt_seguimiento
		(
			idseguimiento 		INT(11) AUTO_INCREMENT PRIMARY KEY,
			fecha_creacion		DATETIME NOT NULL,
			idusuarios_crea 	INT(11),
			origen   			VARCHAR(2) DEFAULT 'N' COMMENT 'E:Evento P:Paquete EC:EventoCotizado PC:PaqueteCotizado N:Ninguno',
			observacion   		TEXT,
			accion  			VARCHAR(2) DEFAULT 'N' COMMENT 'L:Llamada M:Mail C:Cotizacion N:Ninguno',
			fecha_limite 		DATETIME NOT NULL,
			idusuarios_asignado INT(11),
			observacion_final 	TEXT,
			estado   			VARCHAR(1) DEFAULT 'S' COMMENT 'S:SinMov A:Atendido ',
			idseguimiento_ant   INT(11),
			FOREIGN KEY(idusuarios_crea) REFERENCES usuarios(idusuarios),
			FOREIGN KEY(idusuarios_asignado) REFERENCES usuarios(idusuarios),
			FOREIGN KEY(idseguimiento_ant) REFERENCES pqt_seguimiento(idseguimiento)
		);

	/*
	* ARCHIVOS ADJUNTOS
	*/
		CREATE TABLE pqt_item_has_adjunto
		(
			iditem_has_adjunto 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			idarchivo_adjunto 	INT(11) NOT NULL,
			codigo_item 		INT(11),
			idpax 				INT(11),
			tipo 				VARCHAR(4) COMMENT 'H:hotel B:boleto O:otrosservicios I:inscripcion P:paquete E:evento C:cotizacion',
			vista 				VARCHAR(2) COMMENT 'I:interna P:publica',
			FOREIGN KEY(idarchivo_adjunto) REFERENCES archivo_adjunto(idarchivo_adjunto) 
		);

/*
* NUEVO FORMATO
*/

	/*
	* TABLA PUBLICACION
	*/

		CREATE TABLE pqt_publicacion
		(
			-- PRIMARY KEY
			idpublicacion 		INT(11) AUTO_INCREMENT PRIMARY KEY,
			-- FOREIGN KEY
			idusuarios 			INT(11),
			-- ATRIBUTOS
			tipo_publicacion    VARCHAR(1) COMMENT 'P:Paquete E:Evento O:Oferta',
			titulo 				VARCHAR(100),
			fecha_publicacion 	DATE,
			fecha_caducidad 	DATE,
			fecha_salida 		VARCHAR(100),
			fecha_retorno 		VARCHAR(100),
			precio_desde 		FLOAT DEFAULT 0,
			datos_agente        TEXT,
			imagen_primera 		VARCHAR(100),
			imagen_segunda		VARCHAR(100),
			imagen_tercera 		VARCHAR(100),
			publicado 			TINYINT(1) DEFAULT 0 COMMENT '1:Si 0:No',
			resaltar 			TINYINT(1) DEFAULT 0 COMMENT '1:Si 0:No',
			visitas 			INT(255) DEFAULT '0',
			solo_imagen 		TINYINT(1) DEFAULT '0' COMMENT '1:Si 0:No',
			estado 				TINYINT(1) DEFAULT '1' COMMENT '1:Activo 0:Inactivo',

			FOREIGN KEY(idusuarios) REFERENCES user(id)
		) ENGINE=InnoDB;

	/*
	* TABLA OPERADORA
	*/

		CREATE TABLE pqt_operadora
		(
			-- PRIMARY KEY
			idoperadora 		INT(11) AUTO_INCREMENT PRIMARY KEY,
			-- ATRIBUTOS
			nombre_operadora 	VARCHAR(100),
			correos 			TEXT,
			telefonos 			TEXT,
			observaciones 		TEXT
		) ENGINE=InnoDB;

		CREATE TABLE pqt_publicacion_has_operadora
		(
			-- PRIMARY KEY & FOREIGN KEY
			idpublicacion INT(11),
			idoperadora INT(11),
			PRIMARY KEY(idpublicacion, idoperadora),

			FOREIGN KEY(idpublicacion) REFERENCES pqt_publicacion(idpublicacion),
			FOREIGN KEY(idoperadora) REFERENCES pqt_operadora(idoperadora)
		) ENGINE=InnoDB;

	/*
	* TABLAS DE BUSQUEDA
	*/

		CREATE TABLE pqt_alcance_evento
		(
			idalcance_evento 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			descripcion 		VARCHAR(100)
		) ENGINE=InnoDB;

		CREATE TABLE pqt_destino
		(
			iddestino 		INT(11) AUTO_INCREMENT PRIMARY KEY,
			nombre_destino 	VARCHAR(100)
		) ENGINE=InnoDB;

		CREATE TABLE pqt_temporada
		(
			idtemporada 		INT(11) AUTO_INCREMENT PRIMARY KEY,
			nombre_temporada 	VARCHAR(100)
		) ENGINE=InnoDB;

		CREATE TABLE pqt_tipo_evento
		(
			idtipo_evento 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			descripcion 	VARCHAR(100)
		) ENGINE=InnoDB;

		CREATE TABLE pqt_tipo_paquete
		(
			idtipo_paquete 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			descripcion 	VARCHAR(100)
		) ENGINE=InnoDB;

	/*
	* TABLAS DE RELACION DE PAQUETE
	*/

		CREATE TABLE pqt_publicacion_has_alcance_evento
		(
			-- PRIMARY KEY & FOREIGN KEY
			idpublicacion 		INT(11),
			idalcance_evento 	INT(11),
			PRIMARY KEY(idpublicacion, idalcance_evento),

			FOREIGN KEY(idpublicacion) REFERENCES pqt_publicacion(idpublicacion),
			FOREIGN KEY(idalcance_evento) REFERENCES pqt_alcance_evento(idalcance_evento)
		) ENGINE=InnoDB;

		CREATE TABLE pqt_publicacion_has_destino
		(
			-- PRIMARY KEY & FOREIGN KEY
			idpublicacion 	INT(11),
			iddestino 		INT(11),
			PRIMARY KEY(idpublicacion, iddestino),

			FOREIGN KEY(idpublicacion) REFERENCES pqt_publicacion(idpublicacion),
			FOREIGN KEY(iddestino) REFERENCES pqt_destino(iddestino)
		) ENGINE=InnoDB;

		CREATE TABLE pqt_publicacion_has_temporada
		(
			-- PRIMARY KEY & FOREIGN KEY
			idpublicacion 	INT(11),
			idtemporada 	INT(11),
			PRIMARY KEY(idpublicacion, idtemporada),

			FOREIGN KEY(idpublicacion) REFERENCES pqt_publicacion(idpublicacion),
			FOREIGN KEY(idtemporada) REFERENCES pqt_temporada(idtemporada)
		) ENGINE=InnoDB;

		CREATE TABLE pqt_publicacion_has_tipo_evento
		(
			-- PRIMARY KEY & FOREIGN KEY
			idpublicacion 	INT(11),
			idtipo_evento 	INT(11),
			PRIMARY KEY(idpublicacion, idtipo_evento),

			FOREIGN KEY(idpublicacion) REFERENCES pqt_publicacion(idpublicacion),
			FOREIGN KEY(idtipo_evento) REFERENCES pqt_tipo_evento(idtipo_evento)
		) ENGINE=InnoDB;

		CREATE TABLE pqt_publicacion_has_tipo_paquete
		(
			-- PRIMARY KEY & FOREIGN KEY
			idpublicacion 	INT(11),
			idtipo_paquete 	INT(11),
			PRIMARY KEY(idpublicacion, idtipo_paquete),

			FOREIGN KEY(idpublicacion) REFERENCES pqt_publicacion(idpublicacion),
			FOREIGN KEY(idtipo_paquete) REFERENCES pqt_tipo_paquete(idtipo_paquete)
		) ENGINE=InnoDB;

	/*
	* IMAGENES DE LOS PAQUETES
	*/

		CREATE TABLE pqt_imagen
		(
			-- PRIMARY KEY
			idimagen 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			-- ATRIBUTO
			nombre 		VARCHAR(100) NOT NULL
		) ENGINE=InnoDB;

		CREATE TABLE pqt_publicacion_has_imagen
		(
			-- PRIMARY KEY & FOREIGN KEY
			idpublicacion 	INT(11),
			idimagen 		INT(11),
			PRIMARY KEY(idpublicacion, idimagen),

			FOREIGN KEY(idpublicacion) REFERENCES pqt_publicacion(idpublicacion),
			FOREIGN KEY(idimagen) REFERENCES pqt_imagen(idimagen)
		) ENGINE=InnoDB;

	/*
	* ARCHIVOS ADJUNTOS DE LOS PAQUETES
	*/

		CREATE TABLE pqt_archivo_adjunto
		(
			-- PRIMARY KEY
			idarchivo_adjunto 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			-- ATRIBUTOS
			nombre 				VARCHAR(100),
			descripcion 		TEXT
		) ENGINE=InnoDB;

		CREATE TABLE pqt_publicacion_has_adjunto
		(
			-- PRIMARY KEY & FOREIGN KEY
			idpublicacion 		INT(11),
			idarchivo_adjunto 	INT(11),
			PRIMARY KEY(idpublicacion, idarchivo_adjunto),

			FOREIGN KEY(idpublicacion) REFERENCES pqt_publicacion(idpublicacion),
			FOREIGN KEY(idarchivo_adjunto) REFERENCES pqt_archivo_adjunto(idarchivo_adjunto)
		) ENGINE=InnoDB;

	/*
	* TABLA COTIZACION
	*/

		CREATE TABLE pqt_cotizacion
		(
			-- PRIMARY KEY
			idcotizacion 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			-- FOREIGN KEY
			idclientes 		INT(11),
			idusuarios 		INT(11),
			-- ATRIBUTOS
			titulo 			VARCHAR(100),
			fecha_creacion  DATETIME,
			observacion 	TEXT,
			estado 			TINYINT(1) DEFAULT '1' COMMENT '1:Activo 2:Vendido 0:Inactivo',

			FOREIGN KEY(idclientes) REFERENCES clientes(idclientes),
			FOREIGN KEY(idusuarios) REFERENCES usuarios(idusuarios)
		) ENGINE=InnoDB;

		CREATE TABLE pqt_cotizacion_has_publicacion
		(
			-- PRIMARY KEY & FOREIGN KEY
			idpublicacion 	INT(11),
			idcotizacion 	INT(11),
			PRIMARY KEY(idpublicacion, idcotizacion),

			FOREIGN KEY(idpublicacion) REFERENCES pqt_publicacion(idpublicacion),
			FOREIGN KEY(idcotizacion) REFERENCES pqt_cotizacion(idcotizacion)
		) ENGINE=InnoDB;

		CREATE TABLE pqt_cotizacion_has_solicitante
		(
			-- PRIMARY KEY & FOREIGN KEY
			idcotizacion 	INT(11),
			idclientes 		INT(11),
			PRIMARY KEY(idcotizacion, idclientes),
			-- ATRIBUTOS
			estado 			TINYINT(1) DEFAULT '1' COMMENT '1:Activo 0:Inactivo',

			FOREIGN KEY(idcotizacion) REFERENCES pqt_cotizacion(idcotizacion),
			FOREIGN KEY(idclientes) REFERENCES clientes(idclientes)	
		) ENGINE=InnoDB;

	/*
	* TABLA QUE DEFINIRÁ A QUÉ CIUDAD DE ORIGEN PERTENECE UN PAQUETE O EVENTO, GENÉRICO O COTIZADO
	*/

		CREATE TABLE pqt_ciudad_origen_paquete
		(
			-- PRIMARY KEY
			idciudad_origen_paquete 			INT(11) AUTO_INCREMENT PRIMARY KEY,
			-- ATRIBUTOS
			ciudad								VARCHAR(3) NOT NULL,
			fecha_salida 						VARCHAR(100),
			fecha_retorno						VARCHAR(100),
			paquete_incluye 					TEXT,
			importante 							TEXT,
			datos_agente 						TEXT
		) ENGINE=InnoDB;

	/*
	* TABLA ASOCIACION PARA PUBLICACION Y COTIZACION
	*/

		CREATE TABLE pqt_publicacion_has_ciudad_origen
		(
			-- PRIMARY KEY & FOREIGN KEY
			idpublicacion 			INT(11),
			idciudad_origen_paquete INT(11),
			PRIMARY KEY(idpublicacion, idciudad_origen_paquete),

			FOREIGN KEY(idpublicacion) REFERENCES pqt_publicacion(idpublicacion),
			FOREIGN KEY(idciudad_origen_paquete) REFERENCES pqt_ciudad_origen_paquete(idciudad_origen_paquete)
		) ENGINE=InnoDB;

		CREATE TABLE pqt_cotizacion_has_ciudad_origen
		(
			-- PRIMARY KEY & FOREIGN KEY
			idcotizacion 			INT(11),
			idciudad_origen_paquete INT(11),
			PRIMARY KEY(idcotizacion, idciudad_origen_paquete),

			FOREIGN KEY(idcotizacion) REFERENCES pqt_cotizacion(idcotizacion),
			FOREIGN KEY(idciudad_origen_paquete) REFERENCES pqt_ciudad_origen_paquete(idciudad_origen_paquete)
		) ENGINE=InnoDB;

	/*
	* EVENTO
	*/

		CREATE TABLE pqt_evento
		(
			-- PRIMARY KEY
			idevento 		INT(11) AUTO_INCREMENT PRIMARY KEY,
			-- ATRIBUTOS
			nombre_evento 	VARCHAR(200),
			sigla_evento 	VARCHAR(50),
			sede     		VARCHAR(100),
			fecha_inicio 	DATE,
			fecha_fin 		DATE,
			paginas_web 	TEXT
		) ENGINE=InnoDB;

		CREATE TABLE pqt_publicacion_has_evento
		(
			-- PRIMARY KEY & FOREIGN KEY
			idpublicacion 	INT(11),
			idevento 		INT(11),
			PRIMARY KEY(idpublicacion, idevento),

			FOREIGN KEY(idpublicacion) REFERENCES pqt_publicacion(idpublicacion),
			FOREIGN KEY(idevento) REFERENCES pqt_evento(idevento)
		) ENGINE=InnoDB;

	/*
	* PRECIOS DE SERVICIOS GENERICO
	*/

		CREATE TABLE pqt_tarifa_hotel
		(
			idtarifa_hotel 			INT(11) AUTO_INCREMENT PRIMARY KEY,
			destino 				VARCHAR(3),
			idciudad_origen_paquete INT(11),
			FOREIGN KEY(idciudad_origen_paquete) REFERENCES pqt_ciudad_origen_paquete(idciudad_origen_paquete)
		) ENGINE=InnoDB;

		CREATE TABLE pqt_item_hotel
		(
			-- PRIMARY KEY
			iditem_hotel 			INT(11) AUTO_INCREMENT PRIMARY KEY,
			-- FOREIGN KEY
			idtarifa_hotel 			INT(11),
			idhotel 				INT(11),
			-- ATRIBUTOS
			identificador 			VARCHAR(50),
			link 					VARCHAR(100),
			fecha_in              	DATE,
			fecha_out 				DATE,
			precio_single 			FLOAT DEFAULT 0,
			precio_doble 			FLOAT DEFAULT 0,
			precio_triple 			FLOAT DEFAULT 0,
			precio_cuadruple 		FLOAT DEFAULT 0,
			precio_menor 			FLOAT DEFAULT 0,
			precio_infante 			FLOAT DEFAULT 0,
			observacion 			TEXT,
			alimentacion 			TEXT,
			fee 					FLOAT DEFAULT 0,
			increment_fee			VARCHAR(1) DEFAULT 'P' COMMENT 'P:Porcentaje M:Monto',
			factura  				FLOAT,
			increment_factura		VARCHAR(1) DEFAULT 'P' COMMENT 'P:Porcentaje M:Monto',
			moneda 					VARCHAR(1) DEFAULT 'U' COMMENT 'B:Bolivianos U:USD',
			tipo_cambio 			FLOAT,
			time_limit 				DATETIME,
			info_extra 				TEXT,
			is_real 				TINYINT(1) DEFAULT '0' COMMENT '1:Si 0:No',
			estado 					TINYINT(1) DEFAULT '1' COMMENT '1:activo 0:inactivo',

			FOREIGN KEY(idtarifa_hotel) REFERENCES pqt_tarifa_hotel(idtarifa_hotel),
			FOREIGN KEY(idhotel) REFERENCES pqt_hotel(idhotel)
		) ENGINE=InnoDB;

		CREATE TABLE pqt_tarifa_otros
		(
			-- PRIMARY KEY
			idtarifa_otros 			INT(11) AUTO_INCREMENT PRIMARY KEY,
			-- FOREIGN KEY
			idciudad_origen_paquete INT(11),
			idotro_servicio         INT(11),
			-- ATRIBUTOS
			codigo 					VARCHAR(50),
			descripcion 			VARCHAR(100),
			precio_adulto 			FLOAT DEFAULT 0,
			precio_menor 			FLOAT DEFAULT 0,
			precio_infante 			FLOAT DEFAULT 0,
			fee 					FLOAT DEFAULT 0,
			increment_fee 			VARCHAR(1) DEFAULT 'P' COMMENT 'P:Porcentaje M:Monto',
			factura 				FLOAT DEFAULT 0,
			increment_factura 		VARCHAR(1) DEFAULT 'P' COMMENT 'P:Porcentaje M:Monto',
			moneda 					VARCHAR(1) DEFAULT 'U' COMMENT 'B:Bolivianos  U:USD',
			tipo_cambio 			FLOAT,
			time_limit 				DATETIME,
			observacion 			TEXT,
			is_real 				TINYINT(1) DEFAULT '0' COMMENT '1:Si 0:No',
			estado 					TINYINT(1) DEFAULT '1' COMMENT '1:activo 0:inactivo',

			FOREIGN KEY(idotro_servicio) REFERENCES pqt_otro_servicio(idotro_servicio),
			FOREIGN KEY(idciudad_origen_paquete) REFERENCES pqt_ciudad_origen_paquete(idciudad_origen_paquete)
		) ENGINE=InnoDB;

		CREATE TABLE pqt_opcion_boleto
		(
			-- PRIMARY KEY
			idopcion_boleto 		INT(11) AUTO_INCREMENT PRIMARY KEY,
			-- FOREIGN KEY
			idciudad_origen_paquete INT(11),
			-- ATRIBUTOS
			salida 					VARCHAR(50),
			descripcion 			VARCHAR(100),
			estado 					TINYINT(1) DEFAULT '1' COMMENT '1:activo 0:inactivo',

			FOREIGN KEY(idciudad_origen_paquete) REFERENCES pqt_ciudad_origen_paquete(idciudad_origen_paquete)
		) ENGINE=InnoDB;

		CREATE TABLE pqt_boleto_cotizado
		(
			-- PRIMARY KEY
			idboleto_cotizado 	INT(11) AUTO_INCREMENT PRIMARY KEY,
			-- FOREIGN KEY
			idopcion_boleto 	INT(11),
			idlineas_aereas 	INT(11),
			-- ATRIBUTOS
			orden 				TINYINT DEFAULT 1,
			codigo_reserva 		VARCHAR(10) COMMENT 'Codigo Sabre o de Otra Linea Aerea',
			ruta 				VARCHAR(100),
			precio_adulto 		FLOAT DEFAULT 0,
			precio_menor 		FLOAT DEFAULT 0,
			precio_infante 		FLOAT DEFAULT 0,
			fee 				FLOAT DEFAULT 0,
			increment_fee 		VARCHAR(1) DEFAULT 'P' COMMENT 'P:Porcentaje M:Monto',
			factura 			FLOAT DEFAULT 0,
			increment_factura 	VARCHAR(1) DEFAULT 'P' COMMENT 'P:Porcentaje M:Monto',
			moneda 				VARCHAR(1) DEFAULT 'U' COMMENT 'B:Bolivianos  U:USD',
			tipo_cambio 		FLOAT,
			time_limit 			DATETIME,
			observacion 		TEXT,
			is_real 			TINYINT(1) DEFAULT '0' COMMENT '1:Si 0:No',
			estado 				TINYINT(1) DEFAULT '1' COMMENT '1:activo 0:inactivo',

			FOREIGN KEY(idopcion_boleto) REFERENCES pqt_opcion_boleto(idopcion_boleto),
			FOREIGN KEY(idlineas_aereas) REFERENCES lineas_aereas(idlineas_aereas)
		) ENGINE=InnoDB;

		CREATE TABLE pqt_segmento
		(
			-- PRIMARY KEY
			idsegmento 			INT(11) AUTO_INCREMENT PRIMARY KEY,
			-- FOREIGN KEY
			idopcion_boleto 	INT(11),
			idlineas_aereas 	INT(11),
			-- ATRIBUTOS
			codigo 				VARCHAR(10),
			orden 				TINYINT DEFAULT 1,
			nro_vuelo 			INT(11) DEFAULT 0,
			fecha 				DATE DEFAULT '0000-00-00',
			origen 				VARCHAR(5),
			hora_sale 			TIME DEFAULT '00:00:00',
			destino 			VARCHAR(5),
			hora_llega 			TIME DEFAULT '00:00:00',

			FOREIGN KEY(idopcion_boleto) REFERENCES pqt_opcion_boleto(idopcion_boleto),
			FOREIGN KEY(idlineas_aereas) REFERENCES lineas_aereas(idlineas_aereas)
		) ENGINE=InnoDB;

		CREATE TABLE pqt_inscripcion_evento
		(
			-- PRIMARY KEY
			idinscripcion_evento  	INT(11) AUTO_INCREMENT PRIMARY KEY,
			-- FOREIGN KEY
			idevento 				INT(11),
			-- ATRIBUTOS
			categoria  				VARCHAR(100),
			precio 					FLOAT,
			moneda 					VARCHAR(1) DEFAULT 'U' COMMENT 'B:Bolivianos  U:USD',
			fee 					FLOAT DEFAULT 0,
			increment_fee 			VARCHAR(1) DEFAULT 'P' COMMENT 'P:Porcentaje M:Monto',
			factura 				FLOAT DEFAULT 0,
			increment_factura 		VARCHAR(1) DEFAULT 'P' COMMENT 'P:Porcentaje M:Monto',
			observacion 			TEXT,
			time_limit 				DATETIME,
			estado 					TINYINT(1) DEFAULT '1' COMMENT '1:activo 0:inactivo',
			tipo_cambio 			FLOAT,

			FOREIGN KEY(idevento) REFERENCES pqt_evento(idevento)
		) ENGINE=InnoDB;

		CREATE TABLE pqt_cotizacion_has_inscripcion_evento
		(
			-- PRIMARY KEY & FOREIGN KEY
			idcotizacion 			INT(11),
			idinscripcion_evento 	INT(11),
			PRIMARY KEY(idinscripcion_evento, idcotizacion),

			FOREIGN KEY(idcotizacion) REFERENCES pqt_cotizacion(idcotizacion),
			FOREIGN KEY(idinscripcion_evento) REFERENCES pqt_inscripcion_evento(idinscripcion_evento)
		) ENGINE=InnoDB;

	/*
	* COMBINACION DE HOTELES POR DESTINO
	*/

		CREATE TABLE pqt_opcion_hoteles
		(
			-- PRIMARY KEY
			idopcion_hoteles 		INT(11) AUTO_INCREMENT PRIMARY KEY,
			-- FOREIGN KEY
			idciudad_origen_paquete INT(11),
			-- ATRIBUTOS
			observacion            	TEXT,

			FOREIGN KEY(idciudad_origen_paquete) REFERENCES pqt_ciudad_origen_paquete(idciudad_origen_paquete)
		) ENGINE=InnoDB;

		CREATE TABLE pqt_opcion_hoteles_has_item
		(
			-- PRIMARY KEY & FOREIGN KEY
			idopcion_hoteles INT(11),
			iditem_hotel 	INT(11),
			PRIMARY KEY(idopcion_hoteles, iditem_hotel),
			FOREIGN KEY(idopcion_hoteles) REFERENCES pqt_opcion_hoteles(idopcion_hoteles),
			FOREIGN KEY(iditem_hotel) REFERENCES pqt_item_hotel(iditem_hotel)
		) ENGINE=InnoDB;

	/*
	* PASAJEROS DE COTIZACION
	*/

		CREATE TABLE pqt_pax
		(
			-- PRIMARY KEY
			idpax 			INT(11) AUTO_INCREMENT PRIMARY KEY,
			-- FOREIGN KEY
			idcotizacion 	INT(11),
			idclientes 		INT(11),
			FOREIGN KEY(idcotizacion) REFERENCES pqt_cotizacion(idcotizacion),
			FOREIGN KEY(idclientes) REFERENCES clientes(idclientes)
		) ENGINE=InnoDB;

		CREATE TABLE pqt_item_hotel_has_pax
		(
			-- PRIMARY KEY & FOREIGN KEY
			iditem_hotel 	INT(11),
			idpax 			INT(11),
			PRIMARY KEY(iditem_hotel, idpax),
			-- ATRIBUTOS
			tipo_precio 	TINYINT(1) COMMENT '1:SGL 2:DBL 3:TPL 4:CPL 5:CNN 6:INF',
			habitacion   	VARCHAR(50) COMMENT 'IDENTIFICADOR PARA ASOCIAR PASAJEROS',

			FOREIGN KEY(iditem_hotel) REFERENCES pqt_item_hotel(iditem_hotel),
			FOREIGN KEY(idpax) REFERENCES pqt_pax(idpax)
		) ENGINE=InnoDB;

		CREATE TABLE pqt_boleto_cotizado_has_pax
		(
			-- PRIMARY KEY & FOREIGN KEY
			idboleto_cotizado 	INT(11),
			idpax 				INT(11),
			PRIMARY KEY(idboleto_cotizado, idpax),
			-- ATRIBUTOS
			tipo_precio 		TINYINT(1) COMMENT '1:ADT 2:CNN 3:INF',
			FOREIGN KEY(idboleto_cotizado) REFERENCES pqt_boleto_cotizado(idboleto_cotizado),
			FOREIGN KEY(idpax) REFERENCES pqt_pax(idpax)
		) ENGINE=InnoDB;

		CREATE TABLE pqt_tarifa_otros_has_pax
		(
			-- PRIMARY KEY & FOREIGN KEY
			idtarifa_otros 	INT(11),
			idpax 			INT(11),
			PRIMARY KEY(idtarifa_otros, idpax),
			-- ATRIBUTOS
			tipo_precio 	TINYINT(1) COMMENT '1:ADT 2:CNN 3:INF',
			FOREIGN KEY(idtarifa_otros) REFERENCES pqt_tarifa_otros(idtarifa_otros),
			FOREIGN KEY(idpax) REFERENCES pqt_pax(idpax)
		) ENGINE=InnoDB;

		CREATE TABLE pqt_inscripcion_evento_has_pax
		(
			-- PRIMARY KEY & FOREIGN KEY
			idinscripcion_evento 	INT(11),
			idpax 					INT(11),
			PRIMARY KEY(idinscripcion_evento, idpax),
			-- ATRIBUTOS
			tipo_precio 			TINYINT(1) DEFAULT 1 COMMENT 'SE MANEJARA UN SOLO PRECIO',
			FOREIGN KEY(idinscripcion_evento) REFERENCES pqt_inscripcion_evento(idinscripcion_evento),
			FOREIGN KEY(idpax) REFERENCES pqt_pax(idpax)
		) ENGINE=InnoDB;
