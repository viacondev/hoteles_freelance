<?
	include '../BD/controladoraBD.php';
	include '../entidad/otros_cotizado.php';
	
	
	$idpax 				= $_POST['idpax'];
	$consulta 			= $_POST['consulta'];
	
	if($_POST['tipo'] == 1)
	{
		$class = "paquete";
		$abrev = "OPC";
	}
	else
	{
		$class = "evento";
		$abrev = "OEC";
	}
	
	if($consulta == "guardar")
	{
		$idotros_cotizado 	= $_POST['idotros_cotizado'];
		$tipo_precio 		= $_POST['tipo_precio'];
		otros_cotizado::registrar_otros_cotizado_class_cotizado_has_pax_class($idotros_cotizado, $idpax, $tipo_precio, $class);
	}
	else//eliminar
	{
		$idotros_cotizado 	= $_POST['idotros_cotizado'];
		otros_cotizado::eliminar_otros_cotizado_class_cotizado_has_pax_class($idotros_cotizado, $idpax, $class);
	}
	
	$resp = otros_cotizado::obtenet_otros_cotizado_idpax($idpax, $class);

	if(count($resp) > 0)
	{
		for($i = 0; $i < count($resp); $i++)
		{
			$precio 	= 0;
			$fee 		= 0;
			$factura	= 0;
			$r 			= otros_cotizado::obtener_otros_nombre_precio($resp[$i]['idtarifa_otros_'.$class.'_cotizado'], $class);

			if($resp[$i]['tipo_precio'] == 1)
			{
				$precio 	= $r[0]['precio_adulto'];
				/*$fee 		= ($precio * $r[0]['fee'])/100;
					$factura 	= ($precio * $r[0]['factura'])/100;*/
					$factor 	= (1 + $r[0]['fee']/100) * (1 + $r[0]['factura']/100);
					$precio 	= $precio * $factor;
			}
			else
			{
				if($resp[$i]['tipo_precio'] == 2)
				{
					$precio = $r[0]['precio_menor'];
					/*$fee 		= ($precio * $r[0]['fee'])/100;
					$factura 	= ($precio * $r[0]['factura'])/100;*/
					$factor 	= (1 + $r[0]['fee']/100) * (1 + $r[0]['factura']/100);
					$precio 	= $precio * $factor;
				}
				else
				{
					$precio = $r[0]['precio_infante'];
					/*$fee 		= ($precio * $r[0]['fee'])/100;
					$factura 	= ($precio * $r[0]['factura'])/100;*/
					$factor 	= (1 + $r[0]['fee']/100) * (1 + $r[0]['factura']/100);
					$precio 	= $precio * $factor;
				}	
			}
			
			/*switch($r[0]['concepto'])
			{
				case "S":
				{	
					$servicio = "SEGURO";
					break;	
				}
				case "T":
				{	
					$servicio = "TRASLADO";
					break;	
				}
				case "A":
				{	
					$servicio = "AUTO";
					break;	
				}
				case "I":
				{	
					$servicio = "INSCRIPCION";
					break;	
				}
				case "C":
				{	
					$servicio = "CRUCERO";
					break;	
				}
				case "O":
				{	
					$servicio = "OTROS";
					break;	
				}	
			}*/
			$servicio = $r[0]['nombre_servicio'];
			
			$vec[$i]['idtarifa_otros_'.$class.'_cotizado'] 	= $r[0]['idtarifa_otros_'.$class.'_cotizado'];
			$vec[$i]['servicio'] 							= $servicio;
			$vec[$i]['precio'] 								= $precio;
			$vec[$i]['moneda'] 								= $r[0]['moneda'];
			$vec[$i]['tipo_cambio'] 						= $r[0]['tipo_cambio'];
			$vec[$i]['estado'] 								= $r[0]['estado'];
		}
	}
	?>

	<table style="width:99%; border:1px solid #AAAAAA; font-size:8pt;" class="con-borde user_info_editable">
	<tr style="background-color:#10689b; color:#FFFFFF;border-color:#FFF; height:20px;">
        <th rowspan="2" style="vertical-align:middle;">Nro</th>
        <th rowspan="2" style="vertical-align:middle; border-left:solid 1px #FFFFFF;">Servicios</th>
        <th colspan="2" style="border-bottom: solid 1px #FFFFFF;border-left: solid 1px #FFFFFF;">Precio</th>
        <th rowspan="2" style="width:5px; border-left: solid 1px #FFFFFF;">&nbsp;</th>
        <th rowspan="2" style="width:5px; border-left: solid 1px #FFFFFF;">&nbsp;</th>
    </tr>
    <tr style="background-color:#10689b; color:#FFFFFF;border-color:#FFF;height:20px;">
        <th style="border-left: solid 1px #FFFFFF;border-right: solid 1px #FFFFFF;">BOB</th>
        <th>USD</th>
    </tr>
	<?
	$total_b = 0;
	$total_u = 0;
	$totales = 0;
	if(count($vec) > 0)
	{
		$nro = 0;
		foreach($vec as $tabla)
		{
			$ident = $tabla['idtarifa_otros_'.$class.'_cotizado'];

			if($tabla['moneda'] == "B")
			{
				$total_b += $tabla['precio'];
				$totales += round($tabla['precio'] / $tabla['tipo_cambio'],2);
			}
			else
			{
				$total_u += $tabla['precio'];
				$totales += round($tabla['precio'],2);
			}
	?>
	<tr valign="middle" style="height:15px;<? if($tabla['estado'] == 0) echo "color:#ff2d32;";?>">
    	<td><input type="hidden" id="hd_otros_cotizado_<? echo $tabla['idtarifa_otros_'.$class.'_cotizado'];?>"/><? echo $nro = $nro +1;?></td>
        <td>
        	<? echo strtoupper($tabla['servicio'])?>
        	<div id="archivos<?php echo $ident; ?>" style="display:none; background-color:#f1f1f1; border-radius:5px; border:1px solid #aaaaaa; position:absolute; width:350px; height:200px;">
            </div>
        </td>
        <td style="text-align:right;"><? echo $tabla['moneda'] == "B" ? number_format(round($tabla['precio'],2),2): "0.00";?></td>
        <td style="text-align:right;"><? echo $tabla['moneda'] == "U" ? number_format(round($tabla['precio'],2),2): "0.00";?></td>
        <td><img src="images/cross.png" title="Eliminar Otros Servicio" style="cursor:pointer;" onclick="eliminar_otros_pax(<? echo $tabla['idtarifa_otros_'.$class.'_cotizado'];?>,<? echo $idpax;?>);"/></td>
        <td><img src="images/folder.png" title="Archivos Adjuntos" style="cursor:pointer;" onclick="mostrar_dialogo_archivos_x_item('<? echo $ident; ?>', '<?php echo $idpax; ?>', '<?php echo $abrev; ?>');"/></td>
	</tr>
	<?
		}
	}
	else
	{
	?>
	<tr style="height:15px;">
    	<td colspan="6">No Existe Boletos Cargados.</td>                    
	</tr>
	<?		
	}
	?>
	<tr style="height:15px;">
    	<td colspan="2" style="text-align:right;"><strong>Total</strong></td>
        <td style="text-align:right;"><? echo number_format(round($total_b,2),2);?></td>
        <td style="text-align:right;"><? echo number_format(round($total_u,2),2);?></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
	</tr>
    </table>
    <div class="clear_mayor"></div><div class="clear_mayor"></div><div class="clear_mayor"></div>
	<div><strong>Total en USD&nbsp;:&nbsp;&nbsp;</strong><span style="width:15px;"><? echo $totales;?></span></div>
    