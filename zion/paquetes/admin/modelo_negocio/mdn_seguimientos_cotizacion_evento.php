<?php
include('../BD/controladoraBD.php');
include('../entidad/seguimiento_cotizacion_evento.php');

$idevento_cotizado	= $_POST['ct'];
$observacion 		= $_POST['ob'];
$accion 			= $_POST['ac'];
$fecha_prox 		= $_POST['ps'];
$idusuarios			= $_POST['us'];

$prox_seguimiento 	= substr($fecha_prox, 6, 4) . '-' . substr($fecha_prox, 3, 2) . '-' . substr($fecha_prox, 0, 2);

$reg_seguimiento 	= new seguimiento_cotizacion_evento;
$nuevo_seguimiento  = $reg_seguimiento->registrar_seguimiento_cotizacion_evento($idevento_cotizado, $idusuarios, $observacion, $accion, $prox_seguimiento);

if($nuevo_seguimiento === false)
{
	echo 'ERROR';
	exit(0);
}

$seguimientos = $reg_seguimiento->obtener_seguimientos_por_cotizacion($idevento_cotizado);
if($seguimientos !== false)
{
?>
	<table style="width:98%; border:1px solid #000000; margin:10px;">
		<tr style="background-color:#10689b; color:#ffffff; font-weight:bold;">
			<th></th>
			<th>FECHA</th>
			<th>US</th>
			<th>OBSERVACION</th>
			<th>PROX SEG</th>
			<th></th>
		</tr>
		<?php
			for ($i=0; $i < count($seguimientos); $i++) 
			{ 
				$seguimiento 	= $seguimientos[$i];
				$color 			= '#005500';

				if($seguimiento['estado_seguimiento'] == 'P')
				{
					if(date('Y-m-d') == substr($seguimiento['prox_seguimiento'], 0, 10))
					{
						$color 	= '#FF3300';
					}
					else if(strtotime(date('Y-m-d')) > strtotime($seguimiento['prox_seguimiento']))
					{
						$color 	= '#FF3300';
					}
				}
				else if($seguimiento['estado_seguimiento'] == 'A')
				{
					$color 	= '#10689B';
				}
				else
				{
					$color 	= '#008800';
				}
		?>
		<tr style="border:1px solid #000000; margin:10px; color:<?php echo $color; ?>; font-size:8pt;">
			<td>
				<?php
					switch ($seguimiento['accion']) 
					{
						case 'T':
				?>
							<img src="images/telephone.png" title="LLAMADA TELEFONICA" style="width:15px; height:15px; margin:2px;" />
				<?php
						break;
						case 'C':
				?>
							<img src="images/comment.png" title="COMUNICACION VERBAL" style="width:15px; height:15px; margin:2px;" />
				<?php
						break;
						case 'I':
				?>
							<img src="images/print.png" title="IMPRESION DE DOCUMENTO" style="width:15px; height:15px; margin:2px;" />
				<?php
						break;
						case 'M':
				?>
							<img src="images/mailing.png" title="ENVIO DE MAIL" style="width:15px; height:15px; margin:2px;" />
				<?php
						break;
						default:
				?>
							<img src="images/comment.png" title="NO DEFINIDO" style="width:15px; height:15px; margin:2px;" />
				<?php
						break;
					}
				?>
			</td>
			<td ><?php echo strtoupper(date('d/M/Y H:i:s', strtotime($seguimiento['fecha_seguimiento']))); ?></td>
			<td ><?php echo strtoupper($seguimiento['nombre_usuario']); ?></td>
			<td ><?php echo strtoupper($seguimiento['observacion']); ?></td>
			<td ><?php echo strtoupper(date('d/M/Y', strtotime($seguimiento['prox_seguimiento']))); ?></td>
			<td >
				<?php
					if($seguimiento['estado_seguimiento'] == 'P')
					{
				?>
						<img src="images/accept.png" title="ATENDER SEGUIMIENTO" style="width:15px; height:15px; cursor:pointer;" />
						<img src="images/cancel.png" title="ANULAR SEGUIMIENTO" style="width:15px; height:15px; cursor:pointer;" />
				<?php
					} 
					else
					{
						echo strtoupper($seguimiento['comentario']);
					}
				?>
			</td>
		</tr>
		<?php
			}
		?>
	</table>
<?php	
}
?>
