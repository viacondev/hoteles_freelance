<?php
	include("../BD/controladoraBD.php");
	include("../entidad/pax.php");

	$servicio 		= $_POST['s'];
	$type 			= $_POST['t'];
	$tipo_precio	= $_POST['p'];

	$paxss 	= json_decode(stripslashes($_POST['x']));;
	//echo "<pre>"; print_r($paxs); echo "</pre>";

	$aux = 0;
	for ($m=0; $m < count($paxss); $m++) 
	{ 
		if($paxss[$m])
		{
			if($aux % 2 == 0)
				$color = '#980cdf';
			else
				$color = '#b87e0a';

			$paxs = $paxss[$m];
			if($type == 'h')
			{
				for ($i=0; $i < count($paxs); $i++) 
				{ 
					$pax 			= $paxs[$i];
					$idpax 			= $pax->idpax;
					$nombre 		= $pax->nombre;
					$tipo_precio 	= $pax->precio;

					$id  = $idpax . '_' . $servicio;

					switch ($tipo_precio) 
					{
						case 1: $hab='SGL'; break;
						case 2: $hab='DBL'; break;
						case 3: $hab='TPL'; break;
						case 4: $hab='CPL'; break;
						case 5: $hab='CNN'; break;
						case 6: $hab='INF'; break;
						default:$hab='N/N'; break;
					}

					if($i == 0)
						$habitacion = $servicio . '_' . $idpax;

					$registro_pax 	= pax::registrar_pax_hotel($servicio, $idpax, $tipo_precio, $habitacion);
				?>
					<div style="color:<?php echo $color; ?>;" id="paxh<?php echo $id; ?>" onmouseover="$('#delh<?php echo $id; ?>').show();" onmouseout="$('#delh<?php echo $id; ?>').hide();" >
						<?php echo $nombre . '(' . $hab . ')'; ?>
						<img id="delh<?php echo $id; ?>" src="images/userminus32.png" style="width:15px; height:15px; cursor:pointer; display:none;" title="Quitar Pasajero" onclick="QuitarPaxServicio(<?php echo $idpax; ?>, <?php echo $servicio; ?>, 'h');" />			
					</div>
				<?php
				}
			}
			else if($type == 's')
			{
				for ($i=0; $i < count($paxs); $i++) 
				{ 
					$pax 	= $paxs[$i];
					$idpax 	= $pax->idpax;
					$nombre = $pax->nombre;

					$id  = $idpax . '_' . $servicio;

					switch ($tipo_precio) 
					{
						case 1: $hab='ADT'; break;
						case 2: $hab='CNN'; break;
						case 3: $hab='INF'; break;
						default:$hab='N/N'; break;
					}

					$registro_pax 	= pax::registrar_pax_servicio($servicio, $idpax, $tipo_precio);
				?>
					<div id="paxs<?php echo $id; ?>" onmouseover="$('#dels<?php echo $id; ?>').show();" onmouseout="$('#dels<?php echo $id; ?>').hide();">
						<?php echo $nombre . '(' . $hab . ')'; ?>
						<img id="dels<?php echo $id; ?>" src="images/userminus32.png" style="width:15px; height:15px; cursor:pointer; display:none;" title="Quitar Pasajero" onclick="QuitarPaxServicio(<?php echo $idpax; ?>, <?php echo $servicio; ?>, 's');" />			
					</div>
				<?php
				}
			}
			else if($type == 'b')
			{
				for ($i=0; $i < count($paxs); $i++) 
				{ 
					$pax 	= $paxs[$i];
					$idpax 	= $pax->idpax;
					$nombre = $pax->nombre;

					$id  = $idpax . '_' . $servicio;

					switch ($tipo_precio) 
					{
						case 1: $hab='ADT'; break;
						case 2: $hab='CNN'; break;
						case 3: $hab='INF'; break;
						default:$hab='N/N'; break;
					}

					$registro_pax 	= pax::registrar_pax_boleto($servicio, $idpax, $tipo_precio);
				?>
					<div id="paxb<?php echo $id; ?>" onmouseover="$('#delb<?php echo $id; ?>').show();" onmouseout="$('#delb<?php echo $id; ?>').hide();">
						<?php echo $nombre . '(' . $hab . ')'; ?>
						<img id="delb<?php echo $id; ?>" src="images/userminus32.png" style="width:15px; height:15px; cursor:pointer; display:none;" title="Quitar Pasajero" onclick="QuitarPaxServicio(<?php echo $idpax; ?>, <?php echo $servicio; ?>, 'b');" />			
					</div>
				<?php
				}
			}
			else if($type == 'i')
			{
				for ($i=0; $i < count($paxs); $i++) 
				{ 
					$pax 	= $paxs[$i];
					$idpax 	= $pax->idpax;
					$nombre = $pax->nombre;

					$id  = $idpax . '_' . $servicio;

					$tipo_precio = 1;

					$registro_pax 	= pax::registrar_pax_inscripcion($servicio, $idpax, $tipo_precio);
				?>
					<div id="paxi<?php echo $id; ?>" onmouseover="$('#deli<?php echo $id; ?>').show();" onmouseout="$('#deli<?php echo $id; ?>').hide();">
						<?php echo $nombre; ?>
						<img id="deli<?php echo $id; ?>" src="images/userminus32.png" style="width:15px; height:15px; cursor:pointer; display:none;" title="Quitar Pasajero" onclick="QuitarPaxServicio(<?php echo $idpax; ?>, <?php echo $servicio; ?>, 'i');"  />			
					</div>
				<?php
				}
			}

			$aux++;
		}
	}

?>