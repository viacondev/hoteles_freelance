<?php
	include('../BD/controladoraBD.php');
	include('../entidad/archivo_adjunto.php');

	$ruta  			       = "../archivos_adjuntos/";
	$idarchivo_adjunto     = $_POST['a'];
    $iditem_adjunto        = $_POST['i'];
    
    $dato_adjunto   = archivo_adjunto::obtener_adjunto($idarchivo_adjunto);
    
    if($dato_adjunto !== false && count($dato_adjunto)>0)
    {
        $nombre = $dato_adjunto[0]['nombre'];    
    }

    $eliminacion = archivo_adjunto::eliminar_archivo_adjunto_x_pax($iditem_adjunto, $idarchivo_adjunto);
    
    if($eliminacion !== false)
    {
        if(isset($nombre))
        {
            unlink($ruta . $nombre);    
        }
        
        echo 'CORRECTO';
    }
?>