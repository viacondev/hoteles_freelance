<?php
	include("../BD/controladoraBD.php");
	include("../entidad/pax.php");
	include("../entidad/pais.php");
	include("../entidad/clientes.php");
	include("../entidad/requisito_viaje.php");
	include("../control/c_mostrar_cotizacion.php");

	$idcotizacion 			= $_POST['cotizacion'];
	$a_idspaises			= explode(',', $_POST['paises']);
//echo "<pre>"; print_r($a_idspaises); echo "</pre>";
	$paxs 					= c_mostrar_cotizacion::obtener_paxs_mas_datos_checklist($idcotizacion);

	$c_mostrar_cotizacion 	= new c_mostrar_cotizacion;

	$ciudad_actual 			= '';

	// GUARDAMOS LAS OBSERVACIONES DE REQUISITOS QUE ESTEN FALTANDO
	$a_observaciones 	= array();
	// GUARDAMOS LOS PAISES QUE ESTAN EN LA COTIZACION
	$a_nombres_paises 	= array();

?>
	
	<table style="width:90%; font-size:7pt; margin-left:3%;" class="tabla-precios">
		<tr style="background-color:#10689b; color:#FFFFFF;">
			<th>N</th>
			<th>PASAJERO</th>
			<th>NAC</th>
			<th>PASS</th>
			<th>V.PASS</th>
			<th>F.NAC</th>
			<th>EDAD</th>
			<th>ONAMFA</th>
			<th style="line-height:1;" >DOC DE<br/>IDENT</th>
			<th>VAC</th>
			<th>VISA</th>
			<th>OTRO</th>
		</tr>
		<?php //echo "<pre>"; print_r($paxs); echo "</pre>";
			for ($i=0; $i < count($paxs); $i++) 
			{ 
				$pax 	= $paxs[$i];
				$idpax 	= $pax['idpax'];
				$idcli 	= $pax['idclientes'];

				$es_menor = 'N/S';

				if($ciudad_actual != $pax['idciudad_origen_paquete'] || $ciudad_actual == '')
				{
				?>
					<tr>
						<td colspan="12" class="c_origen">
							<strong class="twl_blue">SALE DESDE <?php echo $pax['ciudad']; ?></strong>
						</td>
					</tr>
				<?php
					$ciudad_actual = $pax['idciudad_origen_paquete'];
				}

			?>
				<tr>
					<!-- NRO -->
					<td><?php echo ($i+1); ?></td>
					<!-- NOMBRE PAX -->
					<td>
						<a href="../../crm/interfaces/iu_mostrar_cliente.php?idclientes=<?php echo $idcli; ?>" target="_blank" style="text-decoration:none;" >
							<?php echo strtoupper($pax['nombre_cliente'] . ' ' . $pax['apellido_cliente']); ?>
						</a>
					</td>
					<!-- NACIONALIDAD -->
					<td>
						<?php echo strtoupper($pax['nacionalidad']); ?>
					</td>
					<!-- NRO PASAPORTE -->
					<td <?php if($pax['numero_pasaporte_cliente'] == '') { ?>class="cred" title="Solicitar Numero de Pasaporte"<?php } ?> >
						<?php
							if($pax['numero_pasaporte_cliente'] != '') 
								echo $pax['numero_pasaporte_cliente']; 
							else
								echo 'NO RCBD';
						?>
					</td>
					<!-- VENC PASAPORTE -->
					<td <?php if(strtotime($pax['fecha_vence_pasaporte_cliente']) <= strtotime('now')) { ?> class="cred" title="Verifique Vencimiento Pasaporte" <?php } ?> >
						<?php
							if(strpos($pax['fecha_vence_pasaporte_cliente'], '0000-00-00') === false)
								echo date('d/m/Y', strtotime($pax['fecha_vence_pasaporte_cliente'])); 
							else
								echo 'NO RCBD';
						?>
					</td>
					<!-- FECHA NACIMIENTO -->
					<td <?php if($pax['fecha_nacimiento_cliente'] == '0000-00-00') { ?>class="cred" title="Solicitar Fecha de Nacimiento"<?php } ?> >
						<?php 
							$edad = -1;
							if($pax['fecha_nacimiento_cliente'] != '0000-00-00')
							{
								echo date('d/m/Y', strtotime($pax['fecha_nacimiento_cliente'])); 
								$edad 	= (int)(date('Y') - date('Y', strtotime($pax['fecha_nacimiento_cliente'])));
								$mes 	= date('m', strtotime($pax['fecha_nacimiento_cliente']));
								if( (date('m') < $mes)  ||  ((date('m') == $mes) && (date('d') < date('d', strtotime($pax['fecha_nacimiento_cliente'])))) )
									$edad--;

								if($edad < 18)
									$es_menor = 'REQ';
								else
									$es_menor = 'NO REQ';
							}
							else
								echo 'NO RCBD';
						?>
					</td>
					<!-- EDAD DEL PAX -->
					<td <?php if($edad<0) { ?>class="cred"<?php } ?> >
						<?php
							if($edad>0) 
								echo $edad; 
							else
								echo 'X';
						?>
					</td>
					<!-- VERIFICA SI NECESITA PERMISO ONAMFA -->
					<td <?php if($es_menor=='N/S') { ?>class="cred" title="Solicitar Fecha de Nacimiento"<?php } ?> >
						<?php echo $es_menor; ?>
					</td>
					<!-- EVALUAMOS LOS REQUISITOS DE VIAJE NECESARIOS POR PASAJERO A PARTIR DE SU NACIONALIDAD -->
					<?php
						/*
						* Obtenemos los requisitos de Viaje segun su nacionalidad
						*/
						$idnacionalidad = $pax['nacionalidad_idnacionalidad'];

						$a_visas_necesarias 			= array();
						$a_visas_transito_necesarias 	= array();
						$a_vacunas_necesarias 			= array();
						$a_otros_necesarios 			= array();
						$necesita_pasaporte 			= 0;
						$mensaje_pass_ci 				= 'No es necesario identificacion';

						for ($j=0; $j < count($a_idspaises); $j++) 
						{ 
							$idpais 		= $a_idspaises[$j];
							$requisitos 	= $c_mostrar_cotizacion->obtener_requisitos_x_pais_nacionalidad($idpais, $idnacionalidad);
							
							$nombre_pais 	= $c_mostrar_cotizacion->obtener_pais_x_id($idpais);
							
							// GUARDAMOS EL NOMBRE DEL PAIS EN EL ARREGLO
							if(!array_key_exists($idpais, $a_nombres_paises))
							{
								$a_nombres_paises[$idpais] 	= $nombre_pais;
							}

							if($requisitos == 'No Ingresado')
							{
								// GUARDAMOS EN OBSERVACIONES AQUELLOS REQUISITOS DE VIAJE QUE NO HAN SIDO REGISTRADOS
								if(!array_key_exists($idpais . '_' . $idnacionalidad, $a_observaciones))
								{
									$a_observaciones[$idpais . '_' . $idnacionalidad] = 'Nacionalidad ' . strtoupper($pax['nacionalidad']) . ' en ' . strtoupper($nombre_pais);
								}
							}
							else
							{
								// EVALUAMOS CADA TIPO DE DOCUMENTO Y SE VA CARGANDO A SU ARREGLO CORRESPONDIENTE
								if($requisitos['vacuna'] == 1)
								{
									$a_vacunas_necesarias[$requisitos['idvacuna']] = $requisitos['desc_vacuna'];
								}
								if($requisitos['visa'] == 1)
								{
									$a_visas_necesarias[$requisitos['idvisa']] 	= $requisitos['nombre_visa'];
								}
								if($requisitos['visa_transito'] == 1)
								{
									$a_visas_transito_necesarias[$requisitos['idvisa']] 	= $requisitos['nombre_visa'];
								}
								if($requisitos['otro'] == 1)
								{
									$a_otros_necesarios[] 	= $requisitos['descripcion_otro'];
								}
								if($requisitos['pasaporte'] == 1 && $requisitos['pasaporte_ci'] == 0 && $necesita_pasaporte != 2)
								{
									$necesita_pasaporte = 2;
									$mensaje_pass_ci 	= 'Se necesita obligatoriamente Pasaporte';
								}
								else if($requisitos['pasaporte'] == 1 && $requisitos['pasaporte_ci'] == 1 && $necesita_pasaporte == 0)
								{
									$necesita_pasaporte = 1;
									$mensaje_pass_ci 	= 'Se permite Carnet o Pasaporte';
								}
							}
						}
					?>
					<!-- NECESITA PASAPORTE -->
					<td title="<?php echo $mensaje_pass_ci; ?>" >
						<?php
							if($necesita_pasaporte == 2)
								echo 'Pasaporte';
							else if($necesita_pasaporte == 1)
								echo 'Pasaporte o CI';
						?>
					</td>
					<!-- NECESITA VACUNA -->
					<td style="line-height:1;">
						<?php
							if(count($a_vacunas_necesarias) > 0)
								echo implode('<br/>', $a_vacunas_necesarias);
						?>
					</td>
					<!-- NECESITA VISA -->
					<td style="line-height:1;">
						<?php
							if(count($a_visas_necesarias) > 0)
								echo implode('<br/>', $a_visas_necesarias);
						?>
					</td>
					<!-- NECESITA OTRO REQUISITO -->
					<td style="line-height:1;">
						<?php
							if(count($a_otros_necesarios) > 0)
								echo implode('<br/>', $a_otros_necesarios);
						?>
					</td>
				</tr>
			<?php
			}
		?>
		<tr>
			<td colspan="12">
				<strong style="color:#FF0000;">* ONANFA no requerida si el menor viaja con sus dos padres.</strong>
			</td>
		</tr>
		<tr>
			<td colspan="7" style="line-height:1.2;">
				<strong style="text-decoration:underline;">Verificar Requisitos</strong>
				<br/>
				<?php echo implode('<br/>', $a_observaciones); ?>
			</td>
			<td colspan="5" style="line-height:1.2;">
				<strong style="text-decoration:underline;">Paises en la Cotizacion</strong>
				<br/>
				<?php echo implode('<br/>', $a_nombres_paises); ?>
			</td>
		</tr>
	</table>