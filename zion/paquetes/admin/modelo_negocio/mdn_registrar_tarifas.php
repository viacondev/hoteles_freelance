<?php

	include("../BD/controladoraBD.php");
	include("../entidad/tarifa_hotel.php");
	include("../entidad/hotel.php");
	include("../interfaces/get_tipo_cambio.php");

	$publicacion 	= $_POST['pub'];
	/*$type 			= "";
	if($publicacion == 'evt')
		$type = 'evento';
	if($publicacion == 'evt_cot')
		$type = 'evento_cotizado';
	if($publicacion == 'pqt')
		$type = 'paquete';
	if($publicacion == 'pqt_cot')
		$type = 'paquete_cotizado';*/

//echo "<pre>"; print_r($_POST); echo "</pre>";

	$datos 			= json_decode(stripslashes($_POST['data']));
	$items 			= json_decode(stripslashes($_POST['tarifas']));
	$delete_items 	= $_POST['idel'];

//	echo "<pre>"; print_r($datos); echo "</pre>";
	//echo "<pre>"; print_r($items); echo "</pre>";
	//echo "<pre>"; print_r($delete_items); echo "</pre>";

	$index 						= $datos->indext;
	$idtarifa_hotel 			= $datos->tarifa;
	//$cant_noches 				= $datos->cant_noches;
	$destino 					= $datos->destino;
	$orden 						= $datos->orden;
	/*$v_in 						= $datos->v_in;
	$v_out 						= $datos->v_out;*/
	//$idpublicacion 				= $datos->publicacion;
	$idciudad_origen_paquete 	= $datos->idciudad;

	$idtarifa = 0;

	if($idtarifa_hotel == 0)
	{
		//$registro_tarifa 	= tarifa_hotel::registrar_tarifa_hotel($idpublicacion, $destino, $v_in, $v_out, $type);
		$registro_tarifa 	= tarifa_hotel::registrar_nueva_tarifa_hotel($destino, $orden, $idciudad_origen_paquete);
		$idtarifa 			= mysql_insert_id();
		echo $index . '/' . $idtarifa . '?';
	}
	else
	{
		//$registro_tarifa 	= tarifa_hotel::modificar_tarifa_hotel($idtarifa_hotel, $destino, $v_in, $v_out, $type);	
		$registro_tarifa 	= tarifa_hotel::modificar_tarifa_hotel_x_id($destino, $orden, $idciudad_origen_paquete, $idtarifa_hotel);	
		$idtarifa 			= $idtarifa_hotel;
		echo $index . '/' . $idtarifa . '?';
	}

	$items_eliminar 	= explode(',', $delete_items);
	for ($i=0; $i < count($items_eliminar)-1; $i++) 
	{ 
		$iditem_hotel 		= $items_eliminar[$i];
		$eliminacion_item 	= tarifa_hotel::eliminar_item_hotel_x_id($iditem_hotel);
	}
//echo "<pre>"; print_r($items); echo "</pre>";
	for ($i=0; $i < count($items); $i++) 
	{ 
		$item 	= $items[$i];

		$index_item 		= $item->index;
		$id					= $item->id;
		$idhotel 			= $item->hotel;
		$identificador 		= $item->identif;
		$link 				= $item->link;
		
		//$categoria 			= $item->categoria;
		$fecha_in 			= $item->fecha_in;
		$fecha_out 			= $item->fecha_out;
		$precio_single 		= $item->single;
		$precio_doble 		= $item->doble;
		$precio_triple 		= $item->triple;
		$precio_cuadruple 	= $item->cuadruple;
		$precio_menor 		= $item->menor;
		$precio_infante 	= $item->infante;
		$observacion 		= $item->observacion;
		
		$alimentacion       = $item->alimentacion;
		$fee 				= $item->fee;
		$increment_fee 		= $item->inc_fee;
		$factura 			= $item->factura;
		$increment_factura 	= $item->inc_iva;
		$time_limit 		= $item->tlimit . ' 00:00:00';
		$info_extra 		= $item->info_extra;
		$is_real 			= $item->is_real;
		$estado 			= $item->estado;

/*
		$idhotel = hotel::obtener_id_hotel($hotel);
		if($idhotel!==false)
		{
			if(count($idhotel) > 0)
			{
				$idhotel = $idhotel[0]['idhotel'];
			}
			else
			{
				$registro_hotel = hotel::registrar_nuevo_hotel($hotel, $categoria);
				$idhotel 		= mysql_insert_id();
			}
		}
		else
		{
			$registro_hotel = hotel::registrar_nuevo_hotel($hotel, $categoria);
			$idhotel 		= mysql_insert_id();
		}
*/
		$iditem = 0;

		if($id == 0)
		{
			//$registro_item 	= tarifa_hotel::registrar_item_hotel($idtarifa, $idhotel, $link, $precio_single, $precio_doble, $precio_triple, $precio_cuadruple, $precio_menor, $precio_infante, $observacion, $alimentacion, $comision, $info_extra, $type);
			//$registro_item 	= tarifa_hotel::registrar_item_hotel($idtarifa, $identificador, $hotel, $link, $precio_single, $precio_doble, $precio_triple, $precio_cuadruple, $precio_menor, $precio_infante, $observacion, $alimentacion, $comision, $factura, $tipo_cambio, $time_limit, $info_extra, $estado, $type);
			$registro_item 	= tarifa_hotel::registrar_nuevo_item_hotel($idtarifa, $idhotel, $identificador, $link, $fecha_in, $fecha_out, $precio_single, $precio_doble, $precio_triple, $precio_cuadruple, $precio_menor, $precio_infante, $observacion, $alimentacion, $fee, $increment_fee, $factura, $increment_factura, 'U', $tipo_cambio, $time_limit, $info_extra, $is_real, $estado);
			$iditem 		= mysql_insert_id();
			echo $index_item . '/' . $iditem . ';';
		}
		else
		{
			//$registro_item 	= tarifa_hotel::modificar_item_hotel($id, $idtarifa, $idhotel, $link, $precio_single, $precio_doble, $precio_triple, $precio_cuadruple, $precio_menor, $precio_infante, $observacion, $alimentacion, $comision, $info_extra, $type);
			//$registro_item 	= tarifa_hotel::modificar_item_hotel($id, $idtarifa, $identificador, $hotel, $link, $precio_single, $precio_doble, $precio_triple, $precio_cuadruple, $precio_menor, $precio_infante, $observacion, $alimentacion, $comision, $factura, $tipo_cambio, $time_limit, $info_extra, $estado, $type);
			$registro_item 	= tarifa_hotel::modificar_item_hotel_x_id($id, $idhotel, $identificador, $link, $fecha_in, $fecha_out, $precio_single, $precio_doble, $precio_triple, $precio_cuadruple, $precio_menor, $precio_infante, $observacion, $alimentacion, $fee, $increment_fee, $factura, $increment_factura, 'U', $tipo_cambio, $time_limit, $info_extra, $is_real, $estado);
			$iditem 		= $id;
			echo $index_item . '/' . $iditem . ';';
		}
	}

?>