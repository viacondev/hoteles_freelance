<?php

	include("../BD/controladoraBD.php");
	include("../entidad/boleto_cotizado.php");
	include("../interfaces/get_tipo_cambio.php");

	$boletos 		= json_decode(stripslashes($_POST['bol']));
	$delete_bol 	= $_POST['idel'];

	$a_delete       = explode(',', $delete_bol);
	for ($i=0; $i < count($a_delete) - 1; $i++) 
	{ 
		$idboleto_eliminar = $a_delete[$i];
		$eliminar = boleto_cotizado::eliminar_boleto_x_id($idboleto_eliminar);
	}

	for ($i=0; $i < count($boletos); $i++) 
	{ 
		$boleto_cotizado = $boletos[$i];

		$index    	= $boleto_cotizado->index;
		$idboleto 	= $boleto_cotizado->id;
		$idop_aereo = $boleto_cotizado->op_aereo;
		$orden 		= $boleto_cotizado->orden;
		$codigo 	= $boleto_cotizado->codigo;
		$lin_aerea  = $boleto_cotizado->linea;
		$ruta 		= $boleto_cotizado->ruta;
		$moneda   	= $boleto_cotizado->moneda;
		$adulto   	= $boleto_cotizado->adulto;
		$menor  	= $boleto_cotizado->menor;
		$infante  	= $boleto_cotizado->infante;
		$fee    	= $boleto_cotizado->fee;
		$inc_fee 	= $boleto_cotizado->inc_fee;
		$factura 	= $boleto_cotizado->factura;
		$inc_iva 	= $boleto_cotizado->inc_iva;
		$time_limit = $boleto_cotizado->t_limit;
		$observ 	= $boleto_cotizado->observacion;
		$is_real  	= $boleto_cotizado->is_real;
		$estado     = $boleto_cotizado->estado;

		if($idboleto == 0)
		{
			//$registrar 	= boleto_cotizado::registrar_boleto_cotizado($idpub, $orden, $ruta, $adulto, $menor, $infante, $fee, $factura, substr($moneda, 0, 1), $observ, $type);
			//$registrar 	= boleto_cotizado::registrar_boleto_cotizado($idpub, $idop_aereo, $orden, $codigo, $lin_aerea, $ruta, $adulto, $menor, $infante, $fee, $factura, substr($moneda, 0, 1), $tipo_cambio, $time_limit, $observ, $estado, $type);
			$registrar 	= boleto_cotizado::registrar_nuevo_boleto_cotizado($idop_aereo, $lin_aerea, $orden, $codigo, $ruta, $adulto, $menor, $infante, $fee, $inc_fee, $factura, $inc_iva, substr($moneda, 0, 1), $tipo_cambio, $time_limit, $observ, $is_real, $estado);
			$identi		= mysql_insert_id();
			echo $index . '/' . $identi . ';';
		}
		else
		{
			//$modificar  = boleto_cotizado::modificar_boleto_cotizado($idboleto, $orden, $ruta, $adulto, $menor, $infante, $fee, $factura, substr($moneda, 0, 1), $observ, $type);
			//$modificar 	= boleto_cotizado::modificar_boleto_cotizado($idboleto, $orden, $codigo, $lin_aerea, $ruta, $adulto, $menor, $infante, $fee, $factura, substr($moneda, 0, 1), $tipo_cambio, $time_limit, $observ, $estado, $type);
			$modificar 	= boleto_cotizado::modificar_boleto_cotizado_x_id($idboleto, $lin_aerea, $orden, $codigo, $ruta, $adulto, $menor, $infante, $fee, $inc_fee, $factura, $inc_iva, substr($moneda, 0, 1), $tipo_cambio, $time_limit, $observ, $is_real, $estado);
			echo $index . '/' . $idboleto . ';';
		}
	}

?>