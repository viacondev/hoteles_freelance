<?
	include '../BD/controladoraBD.php';
	include '../entidad/boleto_cotizado.php';
	
	
	$idpax 				= $_POST['idpax'];
	$consulta 			= $_POST['consulta'];
	
	if($_POST['tipo'] == 1)
	{
		$class = "paquete";
		$abrev = "BPC";
	}
	else
	{
		$class = "evento";
		$abrev = "BEC";
	}
	
	if($consulta == "guardar")
	{
		$idboleto_cotizado 	= $_POST['idboleto_cotizado'];
		$tipo_precio 		= $_POST['tipo_precio'];
		boleto_cotizado::registrar_boleto_cotizado_class_cotizado_has_pax_class($idboleto_cotizado, $idpax, $tipo_precio, $class);
	}
	else//eliminar
	{
		$idboleto_cotizado 	= $_POST['idboleto_cotizado'];
		boleto_cotizado::eliminar_boleto_cotizado_class_cotizado_has_pax_class($idboleto_cotizado, $idpax, $class);
	}
	
	$resp = boleto_cotizado::obtenet_boleto_cotizado_idpax($idpax, $class);
	
	if(count($resp) > 0)
	{
		for($i = 0; $i < count($resp); $i++)
		{
			$precio 	= 0;
			$fee 		= 0;
			$factura	= 0;
			$r = boleto_cotizado::obtener_boleto_ruta_precio($resp[$i]['idboleto_cotizado_'.$class.'_cotizado'], $class);
			
			if($resp[$i]['tipo_precio'] == 1)
			{
				$precio 	= $r[0]['precio_adulto'];
				$fee 		= ($precio * $r[0]['fee'])/100;
				$factura 	= ($precio * $r[0]['factura'])/100;
				$precio 	= $precio + $fee + $factura;
			}
			else
			{
				if($resp[$i]['tipo_precio'] == 2)
				{
					$precio = $r[0]['precio_menor'];
					$fee 		= ($precio * $r[0]['fee'])/100;
					$factura 	= ($precio * $r[0]['factura'])/100;
					$precio 	= $precio + $fee + $factura;
				}
				else
				{
					$precio = $r[0]['precio_infante'];
					$fee 		= ($precio * $r[0]['fee'])/100;
					$factura 	= ($precio * $r[0]['factura'])/100;
					$precio 	= $precio + $fee + $factura;
				}	
			}
			
			$vec[$r[0]['orden']]['idboleto_cotizado_'.$class.'_cotizado'] 	= $r[0]['idboleto_cotizado_'.$class.'_cotizado'];
			$vec[$r[0]['orden']]['ruta'] 									= $r[0]['ruta'];
			$vec[$r[0]['orden']]['precio'] 									= $precio;
			$vec[$r[0]['orden']]['moneda'] 									= $r[0]['moneda'];
			$vec[$r[0]['orden']]['tipo_cambio'] 							= $r[0]['tipo_cambio'];
			$vec[$r[0]['orden']]['estado'] 									= $r[0]['estado'];
			
		}
		ksort($vec);
	}
	?>
	<table style="width:99%; border:1px solid #AAAAAA; font-size:8pt;" class="con-borde user_info_editable">
	<tr style="background-color:#10689b; color:#FFFFFF;border-color:#FFF; height:20px;">
        <th rowspan="2" style="vertical-align:middle;">Nro</th>
        <th rowspan="2" style="vertical-align:middle; border-left:solid 1px #FFFFFF;">Ruta</th>
        <th colspan="2" style="border-bottom: solid 1px #FFFFFF;border-left: solid 1px #FFFFFF;">Precio</th>
        <th rowspan="2" style="width:5px; border-left: solid 1px #FFFFFF;">&nbsp;</th>
        <th rowspan="2" style="width:5px; border-left: solid 1px #FFFFFF;">&nbsp;</th>
    </tr>
    <tr style="background-color:#10689b; color:#FFFFFF;border-color:#FFF;height:20px;">
        <th style="border-left: solid 1px #FFFFFF;border-right: solid 1px #FFFFFF;">BOB</th>
        <th>USD</th>
    </tr>
	<?
	$total_b = 0;
	$total_u = 0;
	$totales = 0;
	if(count($vec) > 0)
	{
		$nro = 0;
		foreach($vec as $tabla)
		{
			$ident = $tabla['idboleto_cotizado_'.$class.'_cotizado'];

			if($tabla['moneda'] == "B")
			{
				$total_b += $tabla['precio'];
				$totales += round($tabla['precio'] / $tabla['tipo_cambio'],2);
			}
			else
			{
				$total_u += $tabla['precio'];
				$totales += round($tabla['precio'],2);
			}
	?>
	<tr valign="middle" style="height:15px;<? if($tabla['estado'] == 0) echo "color:#ff2d32;";?>">
    	<td><input type="hidden" id="hd_boleto_cotizado_<? echo $tabla['idboleto_cotizado_'.$class.'_cotizado'];?>"/><? echo $nro = $nro +1;?></td>
        <td>
        	<? echo strtoupper($tabla['ruta'])?>
        	<div id="archivos<?php echo $ident; ?>" style="display:none; background-color:#f1f1f1; border-radius:5px; border:1px solid #aaaaaa; position:absolute; width:350px; height:200px;">
            </div>
        </td>
        <td style="text-align:right;"><? echo $tabla['moneda'] == "B" ? number_format(round($tabla['precio'],2),2): "0.00";?></td>
        <td style="text-align:right;"><? echo $tabla['moneda'] == "U" ? number_format(round($tabla['precio'],2),2): "0.00";?></td>
        <td><img src="images/cross.png" title="Eliminar ruta" style="cursor:pointer;" onclick="eliminar_ruta_pax(<? echo $tabla['idboleto_cotizado_'.$class.'_cotizado'];?>,<? echo $idpax;?>);"/></td>
        <td><img src="images/folder.png" title="Archivos Adjuntos" style="cursor:pointer;" onclick="mostrar_dialogo_archivos_x_item('<? echo $ident; ?>', '<?php echo $idpax; ?>', '<?php echo $abrev; ?>');"/></td>
	</tr>
	<?
		}
	}
	else
	{
	?>
	<tr style="height:15px;">
    	<td colspan="6">No Existe Boletos Cargados.</td>                    
	</tr>
	<?		
	}
	?>
	<tr style="height:15px;">
    	<td colspan="2" style="text-align:right;"><strong>Total</strong></td>
        <td style="text-align:right;"><? echo number_format(round($total_b,2),2);?></td>
        <td style="text-align:right;"><? echo number_format(round($total_u,2),2);?></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
	</tr>
    </table>
    <div class="clear_mayor"></div><div class="clear_mayor"></div><div class="clear_mayor"></div>
	<div><strong>Total en USD&nbsp;:&nbsp;&nbsp;</strong><span style="width:15px;"><? echo $totales;?></span></div>
    