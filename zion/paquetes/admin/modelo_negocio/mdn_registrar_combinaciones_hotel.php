<?php

	include("../BD/controladoraBD.php");
	include("../entidad/tarifa_hotel.php");

	$idciudad 		= $_POST['id'];
	$items 		= json_decode(stripslashes($_POST['it']));
	$eliminar 	= $_POST['dl'];

	$opc_eliminar 	= explode(',', $eliminar);
	for ($i=0; $i < count($opc_eliminar) - 1; $i++) 
	{ 
		$idopcion 		= $opc_eliminar[$i];
		$eliminacion 	= tarifa_hotel::eliminar_opcion_hoteles($idopcion);
	}

	for ($i=0; $i < count($items); $i++) 
	{ 
		$item 			= $items[$i];
		$idopc 			= $item->id;
		$index 			= $item->index;
		$observacion 	= $item->observacion;
		$hoteles 		= $item->hotls;

		if($idopc == 0)
		{
			$registrar 	= tarifa_hotel::registrar_opcion_hoteles($idciudad, $observacion);
			$idopc 		= mysql_insert_id();
			for ($j=0; $j < count($hoteles); $j++) 
			{ 
				$iditem = $hoteles[$j];
				$registrar = tarifa_hotel::registrar_opcion_hoteles_has_item($idopc, $iditem);	
			}
		}
		else
		{
			$modificar 	= tarifa_hotel::modificar_opcion_hoteles($idopc, $observacion);
		}

		echo $index . ',' . $idopc . ';';
	}
?>