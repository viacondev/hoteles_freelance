<?php

	include("../BD/controladoraBD.php");
	include("../entidad/ciudad_origen_paquete.php");

	$ciudad 		= $_POST['c'];
	$publicacion 	= $_POST['p'];
	$type 			= $_POST['t'];

	if($type == 'G')
	{
		$desvincular_con_generico = ciudad_origen_paquete::eliminar_ciudad_origen_paquete_generico($ciudad, $publicacion);
		if($desvincular_con_generico !== false)
		{
			$eliminar_ciudad = ciudad_origen_paquete::eliminar_ciudad_origen_paquete($ciudad);
			if($eliminar_ciudad !== false)
				echo 'CORRECTO';
			else
			{
				$vincular_con_generico = ciudad_origen_paquete::registrar_ciudad_origen_paquete_generico($ciudad, $publicacion);
				echo 'ERROR';
			}
		}
	}
	else
	{
		$desvincular_con_cotizacion = ciudad_origen_paquete::eliminar_ciudad_origen_paquete_cotizacion($ciudad, $publicacion);
		if($desvincular_con_cotizacion !== false)
		{
			$eliminar_ciudad = ciudad_origen_paquete::eliminar_ciudad_origen_paquete($ciudad);
			if($eliminar_ciudad !== false)
				echo 'CORRECTO';
			else
			{
				$vincular_con_cotizacion = ciudad_origen_paquete::registrar_ciudad_origen_paquete_cotizacion($ciudad, $publicacion);
				echo 'ERROR';
			}
		}	
	}

	

	

?>