<?php
	include('../BD/controladoraBD.php');
	include('../entidad/pqt_otro_servicio.php');

	$index 	= $_POST['i'];
	$nombre = $_POST['n'];

	$servicios = pqt_otro_servicio::buscar_otros_servicios($nombre);

	if($servicios === false)
	{
		$servicios = array();
	}

	if(count($servicios) == 0)
	{
		echo "NO SE ENCONTRO NINGUN SERVICIO..";
	}
	else
	{
?>
		<center><table style="width:95%;" class="tabla-simple">
			<tr style="font-weight:bold;">
				<th>SERVICIO</th>
				<th></th>
			</tr>
<?php
		for ($i=0; $i < count($servicios); $i++) 
		{ 
			$servicio 				= $servicios[$i];
			$nombre 	= strtoupper($servicio['nombre_servicio']);
?>
			<tr>
				<td>
					<?php echo $nombre ?>
				</td>
				<td>
					<a style="cursor:pointer;" onclick="seleccionar_servicio('<?php echo $nombre; ?>', '<?php echo $servicio['idotro_servicio']; ?>', '<?php echo $index; ?>');">
						Seleccionar
					</a>
				</td>
			</tr>
<?php
		}
?>
		</table></center>
<?php
	}
?>