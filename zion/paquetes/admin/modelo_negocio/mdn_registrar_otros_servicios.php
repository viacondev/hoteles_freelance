<?php

	include("../BD/controladoraBD.php");
	include("../entidad/tarifa_otros.php");
	include("../interfaces/get_tipo_cambio.php");

	$tarifas 		= json_decode(stripslashes($_POST['tarifas']));
	$delete_srv 	= $_POST['idel'];

	$a_delete       = explode(',', $delete_srv);
	for ($i=0; $i < count($a_delete) - 1; $i++) 
	{ 
		$idtarifa_eliminar = $a_delete[$i];
		$eliminar = tarifa_otros::eliminar_otro_servicio_x_id($idtarifa_eliminar);
	}

	for ($i=0; $i < count($tarifas); $i++) 
	{ 
		$tarifa_otros = $tarifas[$i];

		$index    	= $tarifa_otros->index;
		$idtarifa 	= $tarifa_otros->id;
		$idciudad 	= $tarifa_otros->ciudad;
		//$concepto 	= $tarifa_otros->concepto;
		$codigo 	= $tarifa_otros->codigo;
		$servicio 	= $tarifa_otros->servicio;
		$descripcion= $tarifa_otros->descripcion;
		$ciudad 	= $tarifa_otros->city;
		$fecha_desde= $tarifa_otros->fecha_desde;
		$fecha_hasta= $tarifa_otros->fecha_hasta;
		$moneda   	= $tarifa_otros->moneda;
		$adulto   	= $tarifa_otros->adulto;
		$menor  	= $tarifa_otros->menor;
		$infante  	= $tarifa_otros->infante;
		$fee    	= $tarifa_otros->fee;
		$inc_fee 	= $tarifa_otros->inc_fee;
		$factura 	= $tarifa_otros->factura;
		$inc_iva 	= $tarifa_otros->inc_iva;
		$time_limit = $tarifa_otros->tlimit;
		$observ 	= $tarifa_otros->observacion;
		$is_real    = $tarifa_otros->is_real;
		$estado 	= $tarifa_otros->estado;

		if($idtarifa == 0)
		{
			//$registrar 	= tarifa_otros::registrar_tarifa_otros($idpub, substr($concepto, 0, 1), $adulto, $menor, $infante, $fee, $factura, substr($moneda, 0, 1), $observ, $type);
			//$registrar 	= tarifa_otros::registrar_tarifa_otros($idpub, $codigo, $servicio, $descripcion, $adulto, $menor, $infante, $fee, $factura, substr($moneda, 0, 1), $tipo_cambio, $time_limit, $observ, $estado, $type);
			$registrar 	= tarifa_otros::registrar_nuevo_servicio($idciudad, $servicio, $codigo, $descripcion, $ciudad, $fecha_desde, $fecha_hasta, $adulto, $menor, $infante, $fee, $inc_fee, $factura, $inc_iva, substr($moneda, 0, 1), $tipo_cambio, $time_limit, $observ, $is_real, $estado);
			$identi		= mysql_insert_id();
			echo $index . '/' . $identi . ';';
		}
		else
		{
			//$modificar  = tarifa_otros::modificar_tarifa_otros($idtarifa, substr($concepto, 0, 1), $adulto, $menor, $infante, $fee, $factura, substr($moneda, 0, 1), $observ, $type);
			//$modificar  = tarifa_otros::modificar_tarifa_otros($idtarifa, $codigo, $servicio, $descripcion, $adulto, $menor, $infante, $fee, $factura, substr($moneda, 0, 1), $tipo_cambio, $time_limit, $observ, $estado, $type);
			$modificar  = tarifa_otros::modificar_servicio_x_id($idtarifa, $servicio, $codigo, $descripcion, $ciudad, $fecha_desde, $fecha_hasta, $adulto, $menor, $infante, $fee, $inc_fee, $factura, $inc_iva, substr($moneda, 0, 1), $tipo_cambio, $time_limit, $observ, $is_real, $estado);
			echo $index . '/' . $idtarifa . ';';
		}
	}

?>