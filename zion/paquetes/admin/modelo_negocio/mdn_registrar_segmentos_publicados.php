<?php

	include("../BD/controladoraBD.php");
	include("../entidad/segmento_publicado.php");
	include("../entidad/lineas_aereas.php");

	$segmentos 		= json_decode(stripslashes($_POST['itn']));
	$delete_seg 	= $_POST['idel'];
//echo "<pre>"; print_r($segmentos); echo "</pre><br/>";
	$a_delete       = explode(',', $delete_seg);
	for ($i=0; $i < count($a_delete) - 1; $i++) 
	{ 
		$idsegmento_eliminar 	= $a_delete[$i];
		$eliminar 				= segmento_publicado::eliminar_segmento_x_id($idsegmento_eliminar);
	}

	for ($i=0; $i < count($segmentos); $i++) 
	{ 
		$segmento_publicado = $segmentos[$i];

		$index    		= $segmento_publicado->index;
		$idsegmento 	= $segmento_publicado->id;
		$idopcion_aereo = $segmento_publicado->op_aereo;
		$orden 			= $segmento_publicado->orden;
		$codigo 		= $segmento_publicado->codigo;
		$linea 			= $segmento_publicado->linea;
		$vuelo   		= $segmento_publicado->vuelo;
		$fecha   		= $segmento_publicado->fecha;
		$origen  		= trim($segmento_publicado->aero_sale);
		$hora_sale  	= $segmento_publicado->h_sale;
		$destino 		= trim($segmento_publicado->aero_llega);
		$hora_llega 	= $segmento_publicado->h_llega;

		if(strlen($linea) < 7)
		{
			$codigo_aerolinea 	= trim($linea);
		}
		else
		{
			$aux 				= explode('(', $linea);
			$aux 				= explode(')', $aux[1]);
			$codigo_aerolinea 	= trim($aux[0]);
		}
		$buscar_idaerolinea = lineas_aereas::obtener_linea_aerea_por_codigo_iata($codigo_aerolinea);
		$idlinea_aerea 		= 0;
		if($buscar_idaerolinea !== false)
		{
			if(count($buscar_idaerolinea) > 0)
			{
				$idlinea_aerea = $buscar_idaerolinea[0]['idlineas_aereas'];
			}
		}

		if(strlen($origen) > 3)
		{
			$aux 	= explode('(', $origen);
			$aux 	= explode(')', $aux[1]);
			$origen = trim($aux[0]);
		}

		if(strlen($destino) > 3)
		{
			$aux 		= explode('(', $destino);
			$aux 		= explode(')', $aux[1]);
			$destino 	= trim($aux[0]);
		}

		$fecha_salida = $fecha[6] . $fecha[7] . $fecha[8] . $fecha[9] . '-' . $fecha[3] . $fecha[4] . '-' . $fecha[0] . $fecha[1];

		if($idsegmento == 0)
		{
			//$registrar 	= segmento_publicado::registrar_segmento_publicado($idpub, $idopcion_aereo, $codigo, $orden, $idlinea_aerea, $vuelo, $fecha_salida, $origen, $hora_sale, $destino, $hora_llega, $type);
			$registrar 	= segmento_publicado::registrar_nuevo_segmento_paquete($idopcion_aereo, $idlinea_aerea, $codigo, $orden, $vuelo, $fecha_salida, $origen, $hora_sale, $destino, $hora_llega);
			$identi		= mysql_insert_id();
			echo $index . '/' . $identi . ';';
		}
		else
		{
			//$modificar  = segmento_publicado::modificar_segmento_publicado($idsegmento, $codigo, $orden, $idlinea_aerea, $vuelo, $fecha_salida, $origen, $hora_sale, $destino, $hora_llega, $type);
			$modificar  = segmento_publicado::modificar_segmento_paquete_x_id($idsegmento, $idlinea_aerea, $codigo, $orden, $vuelo, $fecha_salida, $origen, $hora_sale, $destino, $hora_llega);
			echo $index . '/' . $idsegmento . ';';
		}
	}

?>