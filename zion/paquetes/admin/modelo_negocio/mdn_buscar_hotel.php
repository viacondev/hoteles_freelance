<?php
	include('../BD/controladoraBD.php');
	include('../entidad/hotel.php');

	$index 	= $_POST['i'];
	$nombre = $_POST['n'];

	$hoteles = hotel::buscar_hotel_por_nombre($nombre);

	if($hoteles === false)
	{
		$hoteles = array();
	}

	if(count($hoteles) == 0)
	{
		echo "NO SE ENCONTRO NINGUN HOTEL..";
	}
	else
	{
?>
		<center><table style="width:95%;" class="tabla-simple">
			<tr style="font-weight:bold;">
				<th>HOTEL</th>
				<th></th>
			</tr>
<?php
		for ($i=0; $i < count($hoteles); $i++) 
		{ 
			$hotel 				= $hoteles[$i];
			$nombre_completo 	= strtoupper($hotel['nombre_hotel'] . ' (' . $hotel['codigo_hotel'] . ') - ' . $hotel['categoria'] . '*');
?>
			<tr>
				<td>
					<?php echo $nombre_completo ?>
				</td>
				<td>
					<a style="cursor:pointer;" onclick="seleccionar_hotel('<?php echo $nombre_completo; ?>', '<?php echo $hotel['idhotel']; ?>', '<?php echo $index; ?>');">
						Seleccionar
					</a>
				</td>
			</tr>
<?php
		}
?>
		</table></center>
<?php
	}
?>