<?php
include('../BD/controladoraBD.php');
include('../entidad/especialidades.php');

$idprofesion = $_POST['p'];

$especialidades = especialidades::obtener_especialidades_por_profesion($idprofesion);

if($especialidades === false)
{
	$especialidades = array();
}

echo "<option value='0'>Ninguno</option>";
for ($i=0; $i < count($especialidades); $i++) 
{ 
	echo "<option value='" . $especialidades[$i]['idespecialidades'] . "'>" . strtoupper($especialidades[$i]['nombre_especialidad']) . "</option>";
}

?>