<?
	include '../BD/controladoraBD.php';
	include '../entidad/inscripcion_cotizado.php';
	
	
	$idpax 				= $_POST['idpax'];
	$consulta 			= $_POST['consulta'];
	$abrev 				= "IEC";
	
	if($consulta == "guardar")
	{
		$idinscripcion_cotizado 	= $_POST['idinscripcion_cotizado'];
		$tipo_precio 				= $_POST['tipo_precio'];
		inscripcion_cotizado::registrar_inscripcion_cotizado_has_pax_evento($idinscripcion_cotizado, $idpax, $tipo_precio);
	}
	else//eliminar
	{
		$idinscripcion_cotizado 	= $_POST['idinscripcion_cotizado'];
		inscripcion_cotizado::eliminar_inscripcion_cotizado_has_pax_evento($idinscripcion_cotizado, $idpax);
	}
	
	$resp = inscripcion_cotizado::obtener_inscripcion_idpax($idpax);

	if(count($resp) > 0)
	{
		for($i = 0; $i < count($resp); $i++)
		{
			$precio 	= 0;
			$fee 		= 0;
			$factura	= 0;

			$precio 	= $resp[$i]['precio'];
			$fee 		= ($precio * $resp[$i]['fee'])/100;
			$factura 	= ($precio * $resp[$i]['factura'])/100;
			$precio 	= $precio + $fee + $factura;
			
			$vec[$i]['idinscripcion_cotizado'] 	= $resp[$i]['idinscripcion_cotizado'];
			$vec[$i]['categoria'] 				= $resp[$i]['categoria'];
			$vec[$i]['precio'] 					= $precio;
			$vec[$i]['moneda'] 					= $resp[$i]['moneda'];
			$vec[$i]['tipo_cambio'] 			= $resp[$i]['tipo_cambio'];
			$vec[$i]['estado'] 					= $resp[$i]['estado'];
		}
	}
	?>

	<table style="width:99%; border:1px solid #AAAAAA; font-size:8pt;" class="con-borde user_info_editable">
	<tr style="background-color:#10689b; color:#FFFFFF;border-color:#FFF; height:20px;">
        <th rowspan="2" style="vertical-align:middle;">Nro</th>
        <th rowspan="2" style="vertical-align:middle; border-left:solid 1px #FFFFFF;">Categoria</th>
        <th colspan="2" style="border-bottom: solid 1px #FFFFFF;border-left: solid 1px #FFFFFF;">Precio</th>
        <th rowspan="2" style="width:5px; border-left: solid 1px #FFFFFF;">&nbsp;</th>
        <th rowspan="2" style="width:5px; border-left: solid 1px #FFFFFF;">&nbsp;</th>
    </tr>
    <tr style="background-color:#10689b; color:#FFFFFF;border-color:#FFF;height:20px;">
        <th style="border-left: solid 1px #FFFFFF;border-right: solid 1px #FFFFFF;">BOB</th>
        <th>USD</th>
    </tr>
	<?
	$total_b = 0;
	$total_u = 0;
	$totales = 0;
	if(count($vec) > 0)
	{
		$nro = 0;
		foreach($vec as $tabla)
		{
			$ident = $tabla['idinscripcion_cotizado'];

			if($tabla['moneda'] == "B")
			{
				$total_b += $tabla['precio'];
				$totales += round($tabla['precio'] / $tabla['tipo_cambio'],2);
			}
			else
			{
				$total_u += $tabla['precio'];
				$totales += round($tabla['precio'],2);
			}
	?>
	<tr valign="middle" style="height:15px;<? if($tabla['estado'] == 0) echo "color:#ff2d32;";?>">
    	<td><input type="hidden" id="hd_inscripcion_cotizado_<? echo $tabla['idinscripcion_cotizado'];?>"/><? echo $nro = $nro +1;?></td>
        <td>
        	<? echo strtoupper($tabla['categoria'])?>
        	<div id="archivos<?php echo $ident; ?>" style="display:none; background-color:#f1f1f1; border-radius:5px; border:1px solid #aaaaaa; position:absolute; width:350px; height:200px;">
            </div>
        </td>
        <td style="text-align:right;"><? echo $tabla['moneda'] == "B" ? number_format(round($tabla['precio'],2),2): "0.00";?></td>
        <td style="text-align:right;"><? echo $tabla['moneda'] == "U" ? number_format(round($tabla['precio'],2),2): "0.00";?></td>
        <td><img src="images/cross.png" title="Eliminar Categoria" style="cursor:pointer;" onclick="eliminar_inscripcion_pax(<? echo $tabla['idinscripcion_cotizado'];?>,<? echo $idpax;?>);"/></td>
        <td><img src="images/folder.png" title="Archivos Adjuntos" style="cursor:pointer;" onclick="mostrar_dialogo_archivos_x_item('<? echo $ident; ?>', '<?php echo $idpax; ?>', '<?php echo $abrev; ?>');"/></td>
	</tr>
	<?
		}
	}
	else
	{
	?>
	<tr style="height:15px;">
    	<td colspan="6">No Existe Categoria de Inscripcion Cargados.</td>                    
	</tr>
	<?		
	}
	?>
	<tr style="height:15px;">
    	<td colspan="2" style="text-align:right;"><strong>Total</strong></td>
        <td style="text-align:right;"><? echo number_format(round($total_b,2),2);?></td>
        <td style="text-align:right;"><? echo number_format(round($total_u,2),2);?></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
	</tr>
    </table>
    <div class="clear_mayor"></div><div class="clear_mayor"></div><div class="clear_mayor"></div>
	<div><strong>Total en USD&nbsp;:&nbsp;&nbsp;</strong><span style="width:15px;"><? echo $totales;?></span></div>
    