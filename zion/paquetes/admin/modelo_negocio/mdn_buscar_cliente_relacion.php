<?php
include('../BD/controladoraBD.php');
include('../entidad/clientes.php');

$idclientes = $_POST['cliente'];

$clientes = clientes::obtener_clientes_relacionados_con_cliente($idclientes);
$empleados = clientes::obtener_empleados_relacionados_con_empresa($idclientes);

if(count($clientes) > 0)
{
	if(count($empleados) > 0 && $empleados !== false)
		$relacionados 	= array_merge($clientes, $empleados);
	else
		$relacionados 	= $clientes;
}
else
{
	$relacionados 	= $empleados;
}

$clientes 	= array_map("unserialize", array_unique(array_map("serialize", $relacionados)));
$clientes 	= array_values($clientes);

if($clientes!==false)
{
	if(count($clientes)>0)
	{
?>
		<table style="width:100%;" style="border:1px solid #aaa;">
<?php
		for($i=0; $i<count($clientes); $i++)
		{
			$nombre_completo = strtoupper($clientes[$i]['trato_cliente'] . ' ' . $clientes[$i]['nombre_cliente'] . ' ' . $clientes[$i]['apellido_cliente']);
?>
			<tr style="border:1px solid #aaa;">
				<td style="width:70%; padding:5px;"><?php echo $nombre_completo; ?></td>
				<td><?php echo $clientes[$i]['numero_pasaporte_cliente']; ?></td>
				<td><a style="font-weight:bold; cursor:pointer;" onclick="SeleccionarClienteSolicitante('<?php echo $nombre_completo; ?>', <?php echo $clientes[$i]['idclientes']; ?>);">Seleccionar</a></td>
			</tr>
<?php
		}
	}
	else
	{
		echo "NO SE ENCONTR&Oacute;N NING&Uacute;N CLIENTE.";
	}
?>
		</table>
<?php
}
else
{
	echo "ERROR";
}

?>