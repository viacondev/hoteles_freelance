<?php

	include("../BD/controladoraBD.php");
	include("../entidad/clientes.php");

	$criterio = $_POST['c'];
	if($criterio == 'd') 					//BUSCAR CLIENTE POR NOMBRE Y APELLIDO
	{
		$nombre 	= $_POST['n'];
		$apellido 	= $_POST['a'];
		$result 	= clientes::buscar_clientes_por_nombre($nombre, $apellido);
?>
		<center>
			<table style="width:80%; border:1px solid #AAAAAA;">
				<tr style="background-color:#345e8f; color:#FFFFFF;">
					<th></th>
					<th>CLIENTE</th>
					<th></th>	
				</tr>
				<?php
					if($result !== false)
					{
						if(count($result) > 0)
						{
							for ($i=0; $i < count($result); $i++) 
							{ 
				?>
				<tr>
					<td style="border:1px solid #AAAAAA;"><?php echo ($i+1); ?></td>
					<td style="padding:2px; border:1px solid #AAAAAA;"><?php echo strtoupper($result[$i]['trato_cliente'] . ' ' . $result[$i]['nombre_cliente'] . ' ' . $result[$i]['apellido_cliente']); ?></td>
					<td style="border:1px solid #AAAAAA;"><input type="checkbox" value="<?php echo $result[$i]['idclientes']; ?>" name="chk_cliente_<?php echo $i; ?>" /></td>
				</tr>
				<?php
							}
						}
					}
				?>
			</table>	
		</center>
		<input type="hidden" value="<?php echo count($result); ?>" name="cant_clientes" />
		<input type="submit" name="add_cliente" value="Agregar Cliente" />
<?php
		//echo "<pre>"; print_r($clientes); echo "</pre>";
	}
	else if($criterio == 's' ) 				//SEGMENTAR POR OTROS CRITERIOS
	{
		//echo "<pre>"; print_r($result); echo "</pre>";
		if($result !== false)
		{
			if(count($result) > false)
			{
				for ($i=0; $i < count($result); $i++) 
				{ 
					echo $result[$i]['idclientes'] . '/' . $result[$i]['trato_cliente'] . ' ' . $result[$i]['nombre_cliente'] . ' ' . $result[$i]['apellido_cliente'] . ';';
				}
			}
		}
	}
	
?>

