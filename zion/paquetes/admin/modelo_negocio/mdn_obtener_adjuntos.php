<?php
	include('../BD/controladoraBD.php');
	include('../entidad/archivo_adjunto.php');

	$ruta  			= "../archivos_adjuntos/";
	$codigo_item 	= $_POST['i'];
	$codigo_pax 	= $_POST['p'];
	$tipo 			= $_POST['t'];
?>
<img src="images/cancel.png" style="width:15px; height:15px; right:0; cursor:pointer; margin-left:335px;" onclick="$('#archivos<?php echo $codigo_item; ?>').hide();" />
<div class="container" style="width:300px;">
	<div id="adjuntos">
		<?php
			$adjuntos = archivo_adjunto::obtener_adjuntos_x_item($codigo_item, $codigo_pax, $tipo);
			if($adjuntos !== false && count($adjuntos) > 0)
			{
				for ($i=0; $i < count($adjuntos); $i++) 
				{ 
					$nombre 		= $adjuntos[$i]['nombre'];
					$vista 			= $adjuntos[$i]['vista'];
					$descripcion 	= $adjuntos[$i]['descripcion'];
					$idadjunto 		= $adjuntos[$i]['idarchivo_adjunto'];
					$iditem_adj 	= $adjuntos[$i]['iditem_has_adjunto'];
	?>
					<div class="columns five" style="margin:5px;" id="adj<?php echo $iditem_adj; ?>">
						<a href="<?php echo $ruta . $nombre; ?>" target="_blank"><?php echo $nombre; ?></a> - <?php echo $descripcion; ?> - <strong><?php echo $vista; ?></strong>
						<img src="images/cross.png" style="width:10px; height:10px; float:right;" onclick="eliminar_adjunto_pax('<?php echo $idadjunto; ?>', '<?php echo $iditem_adj; ?>');" />
					</div>
	<?php
				}
			}
		?>
	</div>
	<div class="columns three" style="margin:5px;">
		<input id="archivo_adjunto" type="file" name="archivos[]" onchange="guardar_archivo_para_pax();" />
	</div>
	<div class="clear"></div>
	<div class="columns two" style="margin:5px;">
		<textarea id="descripcion_archivo" rows="2" style="min-height:20px;" placeholder="Descripcion del Contenido"></textarea>
		<input type="hidden" value="<?php echo $codigo_pax; ?>" id="pax" />
		<input type="hidden" value="<?php echo $codigo_item; ?>" id="item" />
		<input type="hidden" value="<?php echo $tipo; ?>" id="type" />
	</div>
	<div class="columns three" style="margin:5px;">
		<input type="radio" value="P" name="vista" />Publico
		<input type="radio" value="I" name="vista" checked />Interno
	</div>
</div>
