<?php
	include('../BD/controladoraBD.php');
	include('../entidad/lineas_aereas.php');

	$index 	= $_POST['i'];
	$nombre = $_POST['n'];

	$lineas_aereas = lineas_aereas::buscar_lineas_aereas($nombre);

	if($lineas_aereas === false)
	{
		$lineas_aereas = array();
	}

	if(count($lineas_aereas) == 0)
	{
		echo "NO SE ENCONTRO LINEA AEREA..";
	}
	else
	{
?>
		<center><table style="width:95%;" class="tabla-simple">
			<tr style="font-weight:bold;">
				<th>LINEA AEREA</th>
				<th></th>
			</tr>
<?php
		for ($i=0; $i < count($lineas_aereas); $i++) 
		{ 
			$linea_aerea 	= $lineas_aereas[$i];
			$nombre 		= strtoupper($linea_aerea['nombre_linea_aerea'] . '(' . $linea_aerea['codigo_iata_linea_aerea'] . ')');
?>
			<tr>
				<td>
					<?php echo $nombre ?>
				</td>
				<td>
					<a style="cursor:pointer;" onclick="seleccionar_linea_aerea('<?php echo $nombre; ?>', '<?php echo $linea_aerea['idlineas_aereas']; ?>', '<?php echo $index; ?>');">
						Seleccionar
					</a>
				</td>
			</tr>
<?php
		}
?>
		</table></center>
<?php
	}
?>