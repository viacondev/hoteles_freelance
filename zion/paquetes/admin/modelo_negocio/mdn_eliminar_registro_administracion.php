<?php
	include("../BD/controladoraBD.php");
	include("../entidad/registro_general.php");

	$tabla 		= $_POST['t'];
	$atributo 	= $_POST['i'];
	$valor 		= $_POST['v'];

	/*switch ($tabla) {
		case 'destino':
			{
				$mensaje_error = '';
				$paquetes = registro_general::consulta_general('paquete_has_destino', 'iddestino', $valor);
				if(count($paquetes) > 0 && $paquetes !== false)
				{
					$mensaje_error .= '<br/>Verificar los paquetes ';
					for ($i=0; $i < count($paquetes); $i++) 
					{ 
						$mensaje_error .= $paquetes[$i]['idpaquete'] . ' , ';
					}
				}
				$eventos = registro_general::consulta_general('evento_has_destino', 'iddestino', $valor);
				if(count($eventos) > 0 && $eventos !== false)
				{
					$mensaje_error .= '<br/>Verificar los eventos ';
					for ($i=0; $i < count($eventos); $i++) 
					{ 
						$mensaje_error .= $eventos[$i]['idevento'] . ' , ';
					}
				}
				if($mensaje_error != '')
				{
					$mensaje_error = 'No se pudo eliminar el destino' . $mensaje_error;
					echo $mensaje_error;
					exit(0);
				}
			}
			break;

		case 'temporada':
			{
				$mensaje_error = '';
				$paquetes = registro_general::consulta_general('paquete_has_temporada', 'idtemporada', $valor);
				if(count($paquetes) > 0 && $paquetes !== false)
				{
					$mensaje_error .= '<br/>Verificar los paquetes ';
					for ($i=0; $i < count($paquetes); $i++) 
					{ 
						$mensaje_error .= $paquetes[$i]['idpaquete'] . ' , ';
					}
				}
				$eventos = registro_general::consulta_general('evento_has_temporada', 'idtemporada', $valor);
				if(count($eventos) > 0 && $eventos !== false)
				{
					$mensaje_error .= '<br/>Verificar los eventos ';
					for ($i=0; $i < count($eventos); $i++) 
					{ 
						$mensaje_error .= $eventos[$i]['idevento'] . ' , ';
					}
				}
				if($mensaje_error != '')
				{
					$mensaje_error = 'No se pudo eliminar la temporada' . $mensaje_error;
					echo $mensaje_error;
					exit(0);
				}	
			}
			break;

		case 'hotel':
			{
				$mensaje_error = '';
				$paquetes = registro_general::obtener_registro_item_hotel('paquete', $valor);
				if(count($paquetes) > 0 && $paquetes !== false)
				{
					$mensaje_error .= '<br/>Verificar los paquetes ';
					for ($i=0; $i < count($paquetes); $i++) 
					{ 
						$mensaje_error .= $paquetes[$i]['idpaquete'] . ' , ';
					}
				}
				$eventos = registro_general::obtener_registro_item_hotel('evento', $valor);
				if(count($eventos) > 0 && $eventos !== false)
				{
					$mensaje_error .= '<br/>Verificar los eventos ';
					for ($i=0; $i < count($eventos); $i++) 
					{ 
						$mensaje_error .= $eventos[$i]['idevento'] . ' , ';
					}
				}
				$paquetes_cotizados = registro_general::obtener_registro_item_hotel('paquete_cotizado', $valor);
				if(count($paquetes_cotizados) > 0 && $paquetes_cotizados !== false)
				{
					$mensaje_error .= '<br/>Verificar las cotizaciones de paquetes ';
					for ($i=0; $i < count($paquetes_cotizados); $i++) 
					{ 
						$mensaje_error .= $paquetes_cotizados[$i]['idpaquete_cotizado'] . ' , ';
					}
				}
				$eventos_cotizados = registro_general::obtener_registro_item_hotel('evento_cotizado', $valor);
				if(count($eventos_cotizados) > 0 && $eventos_cotizados !== false)
				{
					$mensaje_error .= '<br/>Verificar las cotizaciones de evento ';
					for ($i=0; $i < count($eventos_cotizados); $i++) 
					{ 
						$mensaje_error .= $eventos_cotizados[$i]['idevento_cotizado'] . ' , ';
					}
				}
				if($mensaje_error != '')
				{
					$mensaje_error = 'No se pudo eliminar el hotel ' . $mensaje_error;
					echo $mensaje_error;
					exit(0);
				}
			}
			break;

		case 'tipo_paquete':
			{
				$mensaje_error = '';
				$paquetes = registro_general::consulta_general('paquete_has_tipo_paquete', 'idtipo_paquete', $valor);
				if(count($paquetes) > 0 && $paquetes !== false)
				{
					$mensaje_error .= '<br/>Verificar los paquetes ';
					for ($i=0; $i < count($paquetes); $i++) 
					{ 
						$mensaje_error .= $paquetes[$i]['idpaquete'] . ' , ';
					}
				}
				if($mensaje_error != '')
				{
					$mensaje_error = 'No se pudo eliminar el tipo de paquete ' . $mensaje_error;
					echo $mensaje_error;
					exit(0);
				}
			}
			break;

		case 'operadora':
			{
				$mensaje_error = '';
				$paquetes = registro_general::consulta_general('paquete', 'idoperadora', $valor);
				if(count($paquetes) > 0 && $paquetes !== false)
				{
					$mensaje_error .= '<br/>Verificar los paquetes ';
					for ($i=0; $i < count($paquetes); $i++) 
					{ 
						$mensaje_error .= $paquetes[$i]['idpaquete'] . ' , ';
					}
				}
				$eventos = registro_general::consulta_general('evento', 'idoperadora', $valor);
				if(count($eventos) > 0 && $eventos !== false)
				{
					$mensaje_error .= '<br/>Verificar los eventos ';
					for ($i=0; $i < count($eventos); $i++) 
					{ 
						$mensaje_error .= $eventos[$i]['idevento'] . ' , ';
					}
				}
				if($mensaje_error != '')
				{
					$mensaje_error = 'No se pudo eliminar la operadora' . $mensaje_error;
					echo $mensaje_error;
					exit(0);
				}	
			}
			break;

		case 'tipo_evento':
			{
				$mensaje_error = '';
				$eventos = registro_general::consulta_general('evento_has_tipo_evento', 'idtipo_evento', $valor);
				if(count($eventos) > 0 && $eventos !== false)
				{
					$mensaje_error .= '<br/>Verificar los eventos ';
					for ($i=0; $i < count($eventos); $i++) 
					{ 
						$mensaje_error .= $eventos[$i]['idevento'] . ' , ';
					}
				}
				if($mensaje_error != '')
				{
					$mensaje_error = 'No se pudo eliminar el tipo de evento ' . $mensaje_error;
					echo $mensaje_error;
					exit(0);
				}		
			}
			break;

		case 'alcance_evento':
			{
				$mensaje_error = '';
				$eventos = registro_general::consulta_general('evento_has_alcance_evento', 'idalcance_evento', $valor);
				if(count($eventos) > 0 && $eventos !== false)
				{
					$mensaje_error .= '<br/>Verificar los eventos ';
					for ($i=0; $i < count($eventos); $i++) 
					{ 
						$mensaje_error .= $eventos[$i]['idevento'] . ' , ';
					}
				}
				if($mensaje_error != '')
				{
					$mensaje_error = 'No se pudo eliminar el alcance de evento ' . $mensaje_error;
					echo $mensaje_error;
					exit(0);
				}			
			}
			break;

		case 'pqt_otro_servicio':
			{
				$mensaje_error = '';
				$paquetes = registro_general::consulta_general('tarifa_otros_paquete', 'idotro_servicio', $valor);
				if(count($paquetes) > 0 && $paquetes !== false)
				{
					$mensaje_error .= '<br/>Verificar los paquetes ';
					for ($i=0; $i < count($paquetes); $i++) 
					{ 
						$mensaje_error .= $paquetes[$i]['idpaquete'] . ' , ';
					}
				}
				$eventos = registro_general::consulta_general('tarifa_otros_evento', 'idotro_servicio', $valor);
				if(count($eventos) > 0 && $eventos !== false)
				{
					$mensaje_error .= '<br/>Verificar los eventos ';
					for ($i=0; $i < count($eventos); $i++) 
					{ 
						$mensaje_error .= $eventos[$i]['idevento'] . ' , ';
					}
				}
				$paquetes_cotizados = registro_general::consulta_general('tarifa_otros_paquete_cotizado', 'idotro_servicio', $valor);
				if(count($paquetes_cotizados) > 0 && $paquetes_cotizados !== false)
				{
					$mensaje_error .= '<br/>Verificar los paquetes cotizados ';
					for ($i=0; $i < count($paquetes_cotizados); $i++) 
					{ 
						$mensaje_error .= $paquetes_cotizados[$i]['idpaquete_cotizado'] . ' , ';
					}
				}
				$eventos_cotizados = registro_general::consulta_general('tarifa_otros_evento_cotizado', 'idotro_servicio', $valor);
				if(count($eventos_cotizados) > 0 && $eventos_cotizados !== false)
				{
					$mensaje_error .= '<br/>Verificar los eventos cotizados ';
					for ($i=0; $i < count($eventos_cotizados); $i++) 
					{ 
						$mensaje_error .= $eventos_cotizados[$i]['idevento_cotizado'] . ' , ';
					}
				}
				if($mensaje_error != '')
				{
					$mensaje_error = 'No se pudo eliminar el servicio' . $mensaje_error;
					echo $mensaje_error;
					exit(0);
				}	
			}
			break;
		
		default:
			# code...
			break;
	}*/

	$eliminacion = registro_general::eliminar_registro_administracion($tabla, $atributo, $valor);

	if($eliminacion !== false)
		echo "CORRECTO";

?>