<?php
	include('../BD/controladoraBD.php');
	include('../entidad/evento.php');

	$index 	= $_POST['i'];
	$nombre = $_POST['n'];

	$eventos = evento::buscar_eventos($nombre);

	if($eventos === false)
	{
		$eventos = array();
	}

	if(count($eventos) == 0)
	{
		echo "NO SE ENCONTRO NINGUN EVENTO..";
	}
	else
	{
?>
		<center><table style="width:95%;" class="tabla-simple">
			<tr style="font-weight:bold;">
				<th>EVENTO</th>
				<th></th>
			</tr>
<?php
		for ($i=0; $i < count($eventos); $i++) 
		{ 
			$evento 	= $eventos[$i];
			$nombre 	= strtoupper($evento['nombre_evento'] . ' (' . $evento['sigla_evento'] . ')');
?>
			<tr>
				<td>
					<?php echo $nombre ?>
				</td>
				<td>
					<a style="cursor:pointer;" onclick="seleccionar_evento('<?php echo $nombre; ?>', '<?php echo $evento['idevento']; ?>', '<?php echo $index; ?>');">
						Seleccionar
					</a>
				</td>
			</tr>
<?php
		}
?>
		</table></center>
<?php
	}
?>