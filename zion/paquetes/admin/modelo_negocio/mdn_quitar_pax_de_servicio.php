<?php
	include("../BD/controladoraBD.php");
	include("../entidad/pax.php");

	$servicio 		= $_POST['s'];
	$pax 			= $_POST['p'];
	$type 			= $_POST['t'];

	if($type == 'h')
	{
		$eliminacion_pax_servicio 	= pax::quitar_pax_hotel($servicio, $pax);

		if($eliminacion_pax_servicio !== false)
			echo 'CORRECTO';
		else
			echo 'ERROR';
	}
	else if($type == 's')
	{
		$eliminacion_pax_servicio 	= pax::quitar_pax_servicio($servicio, $pax);

		if($eliminacion_pax_servicio !== false)
			echo 'CORRECTO';
		else
			echo 'ERROR';	
	}
	else if($type == 'b')
	{
		$eliminacion_pax_servicio 	= pax::quitar_pax_boleto($servicio, $pax);

		if($eliminacion_pax_servicio !== false)
			echo 'CORRECTO';
		else
			echo 'ERROR';		
	}
	else if($type == 'i')
	{
		$eliminacion_pax_servicio 	= pax::quitar_pax_inscripcion($servicio, $pax);

		if($eliminacion_pax_servicio !== false)
			echo 'CORRECTO';
		else
			echo 'ERROR';
	}
?>