<?
	include '../BD/controladoraBD.php';
	include('../control/c_pax_evento.php');
	include('../control/c_mostrar_cotizacion_evento.php');
	include("../entidad/pax_evento.php");
	include '../entidad/boleto_cotizado.php';
	include '../entidad/hotel_cotizado.php';
	include '../entidad/otros_cotizado.php';
	include '../entidad/inscripcion_cotizado.php';

	$c_cotizacion_evento 	= new c_mostrar_cotizacion_evento;
	
	$consulta 		= $_POST['consulta'];
	$idcotizacion 	= $_POST['idevento_cotizado'];

	if($consulta == "delete")
	{
		$idpax 	= $_POST['idpax'];
		c_pax_evento::eliminar_pax_evento_cotizado($idpax);
	}
	
	
	
	$rep_pax_evento = c_pax_evento::obtener_pax_evento_idcotizacion($idcotizacion);
	
	if($rep_pax_evento > -1)
	{
	?>
	<table style="width:100%; border:1px solid #AAAAAA; font-size:8pt;" class="con-borde user_info_editable">
	<tr style="background-color:#10689b; color:#FFFFFF;border-color:#FFF">
    	<th style="width:16px;">&nbsp;</th>
		<th style="width:30px;">Pasajeros</th>
		<th style="width:50px;">Hotel</th>
		<th style="width:50px;">Boleto</th>
		<th style="width:50px;">Otros</th>
        <th style="width:50px;">Inscripcion</th>
        <th style="width:50px;">Totales</th>
	</tr>
	<?
	$total_cotizacion = 0;
	
	for($i = 0; $i < count($rep_pax_evento); $i++)
	{
		$total_hotel 		= $c_cotizacion_evento->obtener_total_monto_pax_hotel($rep_pax_evento[$i]['idpax_evento']);//0;
		$total_boleto 		= $c_cotizacion_evento->obtener_total_monto_pax($rep_pax_evento[$i]['idpax_evento']);
		$total_otros 		= $c_cotizacion_evento->obtener_total_monto_pax_otros($rep_pax_evento[$i]['idpax_evento']);//0;
		$total_inscripcion	= $c_cotizacion_evento->obtener_total_monto_pax_inscripcion($rep_pax_evento[$i]['idpax_evento']);//0;
		$total_pax 			= $total_hotel + $total_boleto + $total_otros + $total_inscripcion;
		$total_cotizacion 	+= $total_pax;
	?>
	<tr id="idclientes_<? echo $rep_pax_evento[$i]['idclientes']?>" class="tr_move">
    	<td style="text-align:center; padding-left:0px; padding-right:0px;">
            <span class="editar_idclientes_<? echo $rep_pax_evento[$i]['idclientes']?>_link editar_campo_link" style="display:none;" >
               <img src="../interfaces/images/cross.png" title="Eliminar Pasajero" style="cursor:pointer;" onclick="eliminar_pax_cotizacion(<? echo $rep_pax_evento[$i]['idpax_evento'];?>);" />     
            </span>
        </td>
		<td id="nombre_pax_<? echo $rep_pax_evento[$i]['idpax_evento']?>"><? echo $rep_pax_evento[$i]['trato_cliente']." ".$rep_pax_evento[$i]['nombre_cliente']." ".$rep_pax_evento[$i]['apellido_cliente']?></td>
		<td style="text-align:right;">
        	<span style=""><? echo number_format(round($total_hotel,2),2);?></span>
            <span style="float:left;display:none;" class="editar_idclientes_<? echo $rep_pax_evento[$i]['idclientes']?>_link editar_campo_link" >
            	<img src="images/pencil.png" title="Editar Hotel" style="cursor:pointer;" onclick="show_editar_hotel(<? echo $rep_pax_evento[$i]['idpax_evento']?>,2,<? echo $idcotizacion;?>);" />
			</span>
		</td>
		<td style="text-align:right;">
        	<span id="total_<? echo $rep_pax_evento[$i]['idpax_evento'];?>"><? echo number_format(round($total_boleto,2),2);?></span>
            <span style="float:left;display:none;" class="editar_idclientes_<? echo $rep_pax_evento[$i]['idclientes']?>_link editar_campo_link" >
            	<img src="images/pencil.png" title="Editar Boleto" style="cursor:pointer;" onclick="show_editar_boleto(<? echo $rep_pax_evento[$i]['idpax_evento']?>,2,<? echo $idcotizacion;?>);" />
			</span>
		</td>
		<td style="text-align:right;">
        	<span style=""><? echo number_format(round($total_otros,2),2);?></span>
            <span style="float:left;display:none;" class="editar_idclientes_<? echo $rep_pax_evento[$i]['idclientes']?>_link editar_campo_link" >
            	<img src="images/pencil.png" title="Editar Otros Servicios" style="cursor:pointer;" onclick="show_editar_otros(<? echo $rep_pax_evento[$i]['idpax_evento']?>,2,<? echo $idcotizacion;?>);" />
			</span>
		</td>
        <td style="text-align:right;">
			<span style=""><? echo number_format(round($total_inscripcion,2),2);?></span>
            <span style="float:left;display:none;" class="editar_idclientes_<? echo $rep_pax_evento[$i]['idclientes']?>_link editar_campo_link" >
            	<img src="images/pencil.png" title="Editar Inscripcion" style="cursor:pointer;" onclick="show_editar_inscripcion(<? echo $rep_pax_evento[$i]['idpax_evento']?>,2,<? echo $idcotizacion;?>)" />
			</span>
		</td>
        <td style="text-align:right;"><span style=""><? echo number_format(round($total_pax,2),2);?></span></td>
	</tr>
	<?	
	}
	?>
    <tr>
    	<td colspan="6" style="text-align:right;"><strong>Totales&nbsp;</strong></td>
		<td style="text-align:right;"><strong><span style=""><? echo number_format(round($total_cotizacion,2),2);?></span></strong></th>
	</tr>
	</table>
	<?
	}
	?>