<?php
include('../BD/controladoraBD.php');
include('../entidad/clientes.php');

$nombre_cliente 		= $_POST['nombre'];
$apellido_cliente 		= $_POST['apellido'];
$pasaporte 				= $_POST['pasaporte'];

$clientes = clientes::obtener_paxs_por_nombre_apellido_pasaporte($nombre_cliente, $apellido_cliente, $pasaporte);
if($clientes!==false)
{
	if(count($clientes)>0)
	{
?>
		<table class="resultado_pax" style="width:100%;">
			<tr style="border:1px solid #aaa; background-color: #10689b; color: #ffffff; font-weight:bold;">
				<td style="width:40%;">NOMBRE</td>
				<td>PASAPORTE</td>
				<td>VENC PASAPORTE</td>
				<td>NACIMIENTO</td>
				<td>SEXO</td>
				<td></td>
			</tr>
<?php
		for($i=0; $i<count($clientes); $i++)
		{
			$idcliente 			= $clientes[$i]['idclientes'];
			$nombre_completo 	= strtoupper($clientes[$i]['trato_cliente'] . ' ' . $clientes[$i]['nombre_cliente'] . ' ' . $clientes[$i]['apellido_cliente']);
			$pasaporte 			= $clientes[$i]['numero_pasaporte_cliente'];
			$venc_pasaporte 	= date('d/m/Y', strtotime($clientes[$i]['fecha_vence_pasaporte_cliente']));
			$fecha_nac 			= date('d/m/Y', strtotime($clientes[$i]['fecha_nacimiento_cliente']));
			$sexo 				= strtoupper($clientes[$i]['sexo_cliente']);

			$s_venc_pas = '';
			if(strtotime($clientes[$i]['fecha_vence_pasaporte_cliente']) < strtotime('+3 months'))
			{
				$s_venc_pas = 'style="color:#FF3300; font-weight:bold;" title="VERIFICAR PASAPORTE"';
			}
?>
			<tr id="tr_pax_<? echo $idcliente;?>" style="border:1px solid #aaa;">
				<td style="padding:5px;"><?php echo $nombre_completo; ?></td>
				<td style="padding:5px;"><?php echo $pasaporte; ?></td>
				<td <?php echo $s_venc_pas; ?> ><?php echo $venc_pasaporte; ?></td>
				<td style="padding:5px;"><?php echo $fecha_nac; ?></td>
				<td style="padding:5px;"><?php echo $sexo; ?></td>
				<td id="td_accion_seleccionar_<? echo $idcliente;?>" style="padding:5px;">
					<a style="font-weight:bold; cursor:pointer;" onclick="SeleccionarPasajero(<?php echo $idcliente; ?>, '<?php echo $nombre_completo; ?>', '<?php echo $pasaporte; ?>', '<?php echo $venc_pasaporte; ?>', '<?php echo $fecha_nac; ?>', '<?php echo $sexo; ?>');">
						Seleccionar
					</a>
				</td>
			</tr>
<?php
		}
	}
	else
	{
		echo "<label class='resultado_pax'>NO SE ENCONTR&Oacute;N NING&Uacute;N CLIENTE CON ESTOS CRITERIOS.</label>";
	}
?>
		</table>
<?php
}
else
{
	echo "ERROR";
}
?>