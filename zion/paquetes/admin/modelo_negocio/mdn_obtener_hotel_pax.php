<?
	include '../BD/controladoraBD.php';
	include '../entidad/hotel_cotizado.php';
	
	$idpax 				= $_POST['idpax'];
	$consulta 			= $_POST['consulta'];
	
	if($_POST['tipo'] == 1)
	{
		$class = "paquete";
		$abrev = "HPC";
	}
	else
	{
		$class = "evento";
		$abrev = "HEC";
	}
	
	if($consulta == "guardar")
	{
		$idhotel_cotizado 	= $_POST['idhotel_cotizado'];
		$tipo_precio 		= $_POST['tipo_precio'];
		hotel_cotizado::registrar_hotel_cotizado_class_cotizado_has_pax_class($idhotel_cotizado, $idpax, $tipo_precio, $class);
	}
	else//eliminar
	{
		$idhotel_cotizado 	= $_POST['idhotel_cotizado'];
		hotel_cotizado::eliminar_hotel_cotizado_class_cotizado_has_pax_class($idhotel_cotizado, $idpax, $class);
	}
	
	$resp = hotel_cotizado::obtenet_hotel_cotizado_idpax($idpax, $class);
	
	if(count($resp) > 0)
	{
		for($i = 0; $i < count($resp); $i++)
		{
			$precio 	= 0;
			$comision 	= 0;
			$factura	= 0;
			
			$r = hotel_cotizado::obtener_item_hotel_class_cotizado_id($resp[$i]['iditem_hotel_'.$class.'_cotizado'], $class);
			
			switch($resp[$i]['tipo_precio'])
			{
				case 1:
				{
					$precio 	= $r[0]['precio_single'];
					$comision	= ($precio * $r[0]['comision'])/100;
					$factura 	= ($precio * $r[0]['factura'])/100;
					$precio 	= $precio + $comision + $factura;
					break;	
				}
				case 2:
				{
					$precio 	= $r[0]['precio_doble'];
					$comision	= ($precio * $r[0]['comision'])/100;
					$factura 	= ($precio * $r[0]['factura'])/100;
					$precio 	= $precio + $comision + $factura;
					break;	
				}
				case 3:
				{
					$precio 	= $r[0]['precio_triple'];
					$comision	= ($precio * $r[0]['comision'])/100;
					$factura 	= ($precio * $r[0]['factura'])/100;
					$precio 	= $precio + $comision + $factura;
					break;	
				}
				case 4:
				{
					$precio 	= $r[0]['precio_cuadruple'];
					$comision	= ($precio * $r[0]['comision'])/100;
					$factura 	= ($precio * $r[0]['factura'])/100;
					$precio 	= $precio + $comision + $factura;
					break;	
				}
				case 5:
				{
					$precio 	= $r[0]['precio_menor'];
					$comision	= ($precio * $r[0]['comision'])/100;
					$factura 	= ($precio * $r[0]['factura'])/100;
					$precio 	= $precio + $comision + $factura;
					break;	
				}
				case 6:
				{
					$precio 	= $r[0]['precio_infante'];
					$comision	= ($precio * $r[0]['comision'])/100;
					$factura 	= ($precio * $r[0]['factura'])/100;
					$precio 	= $precio + $comision + $factura;
					break;	
				}
			}
			$vec[$i]['iditem_hotel_'.$class.'_cotizado'] 	= $r[0]['iditem_hotel_'.$class.'_cotizado'];
			$vec[$i]['nombre_hotel'] 						= $r[0]['nombre_hotel'];
			$vec[$i]['categoria']	 						= $r[0]['categoria'];
			$vec[$i]['precio'] 								= $precio;
			$vec[$i]['moneda'] 								= $r[0]['moneda'];
			$vec[$i]['tipo_cambio'] 						= $r[0]['tipo_cambio'];
			$vec[$i]['estado'] 								= $r[0]['estado'];
			$vec[$i]['destino'] 							= $r[0]['destino'];
		}
	}
?>

	<table style="width:99%; border:1px solid #AAAAAA; font-size:8pt;display:;" class="con-borde user_info_editable">
    <tr style="background-color:#10689b; color:#FFFFFF;border-color:#FFF; height:20px;">
        <th rowspan="2" style="vertical-align:middle;">Nro</th>
        <th rowspan="2" style="vertical-align:middle; border-left:solid 1px #FFFFFF;">Hotel</th>
        <th colspan="2" style="border-bottom: solid 1px #FFFFFF;border-left: solid 1px #FFFFFF;">Precio</th>
        <th rowspan="2" style="width:5px; border-left: solid 1px #FFFFFF;">&nbsp;</th>
        <th rowspan="2" style="width:5px; border-left: solid 1px #FFFFFF;">&nbsp;</th>
    </tr>
    <tr style="background-color:#10689b; color:#FFFFFF;border-color:#FFF;height:20px;">
        <th style="border-left: solid 1px #FFFFFF;border-right: solid 1px #FFFFFF;">BOB</th>
        <th>USD</th>
    </tr>
<?
	$total_b = 0;
	$total_u = 0;
	$totales = 0;
			
	if(count($vec) > 0)
	{
		$nro = 0;
		
		foreach($vec as $tabla)
		{
			$ident = $tabla['iditem_hotel_'.$class.'_cotizado'];

			if($tabla['moneda'] == "B")
			{
				$total_b += $tabla['precio'];
				$totales += round($tabla['precio'] / $tabla['tipo_cambio'],2);
			}
			else
			{
				$total_u += $tabla['precio'];
				$totales += round($tabla['precio'],2);
			}
?>
            <tr valign="middle" style="height:15px;<? if($tabla['estado'] == 0) echo "color:#ff2d32;";?>">
                <td><input type="hidden" id="hd_hotel_cotizado_<? echo $tabla['iditem_hotel_'.$class.'_cotizado'];?>"/><? echo $nro = $nro +1;?></td>
                <td>
                	<? echo strtoupper($tabla['nombre_hotel']." ".$tabla['categoria']."* (".$tabla['destino'].")")?>
                	<div id="archivos<?php echo $ident; ?>" style="display:none; background-color:#f1f1f1; border-radius:5px; border:1px solid #aaaaaa; position:absolute; width:350px; height:200px;">
                	</div>
                </td>
                <td style="text-align:right;"><? echo $tabla['moneda'] == "B" ? number_format(round($tabla['precio'],2),2): "0.00";?></td>
                <td style="text-align:right;"><? echo $tabla['moneda'] == "U" ? number_format(round($tabla['precio'],2),2): "0.00";?></td>
                <td><img src="images/cross.png" title="Eliminar ruta" style="cursor:pointer;" onclick="eliminar_hotel_pax(<? echo $tabla['iditem_hotel_'.$class.'_cotizado'];?>,<? echo $idpax;?>);"/></td>
                <td><img src="images/folder.png" title="Archivos Adjuntos" style="cursor:pointer;" onclick="mostrar_dialogo_archivos_x_item('<? echo $ident; ?>', '<?php echo $idpax; ?>', '<?php echo $abrev; ?>');"/></td>
            </tr>
			<?
		}
	}
	else
	{
	?>
	<tr style="height:15px;">
    	<td colspan="6">No Existe Boletos Cargados.</td>                    
	</tr>
	<?
	}
	?>
    <tr style="height:15px;">
        <td colspan="2" style="text-align:right;"><strong>Total</strong></td>
        <td style="text-align:right;"><? echo number_format(round($total_b,2),2);?></td>
        <td style="text-align:right;"><? echo number_format(round($total_u,2),2);?></td>
        <td>&nbsp;</td> 
        <td>&nbsp;</td> 
    </tr>
    </table>
    <div class="clear_mayor"></div><div class="clear_mayor"></div><div class="clear_mayor"></div>
    <div><strong>Total en USD&nbsp;:&nbsp;&nbsp;</strong><span style="width:15px;"><? echo number_format(round($totales,2),2);?></span></div>   