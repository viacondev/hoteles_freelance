<?php
include('../BD/controladoraBD.php');
include('../entidad/clientes.php');

$nombre_cliente 		= $_POST['nombre'];
$apellido_cliente 		= $_POST['apellido'];
$pasaporte 				= $_POST['pasaporte'];
$tipo_cliente 			= $_POST['tipo_cliente'];

$clientes = clientes::obtener_clientes_por_nombre_apellido_pasaporte($nombre_cliente, $apellido_cliente, $pasaporte, $tipo_cliente);
if($clientes!==false)
{
	if(count($clientes)>0)
	{
?>
		<table style="width:100%;" style="border:1px solid #aaa;">
<?php
		for($i=0; $i<count($clientes); $i++)
		{
			$nombre_completo = strtoupper($clientes[$i]['trato_cliente'] . ' ' . $clientes[$i]['nombre_cliente'] . ' ' . $clientes[$i]['apellido_cliente']);
?>
			<tr style="border:1px solid #aaa;">
				<td style="width:70%; padding:5px;"><?php echo $nombre_completo; ?></td>
				<td><?php echo $clientes[$i]['numero_pasaporte_cliente']; ?></td>
				<td><a style="font-weight:bold; cursor:pointer;" onclick="SeleccionarCliente('<?php echo $nombre_completo; ?>', <?php echo $clientes[$i]['idclientes']; ?>);">Seleccionar</a></td>
			</tr>
<?php
		}
	}
	else
	{
		echo "NO SE ENCONTR&Oacute;N NING&Uacute;N CLIENTE CON ESTOS CRITERIOS.";
	}
?>
		</table>
<?php
}
else
{
	echo "ERROR";
}

?>