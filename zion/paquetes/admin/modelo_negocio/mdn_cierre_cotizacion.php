<?php

	include("../BD/controladoraBD.php");
	include("../entidad/cotizacion.php");
	
	if(isset($_POST['cot']) && isset($_POST['est']))
	{
		$idcotizacion 	= $_POST['cot'];
		$estado 		= $_POST['est'];	

		$cambio_estado = cotizacion::cierre_de_cotizacion($idcotizacion, $estado);

		if($cambio_estado)
			echo 'CORRECTO';
		else
			echo 'ERROR';
	}
	else
		echo 'ERROR';

?>