<?php
    include('admin/BD/controladoraBD.php');
    include('admin/control/c_mostrar_paquete.php');
    include('admin/control/c_buscar_paquete.php');
    include('admin/entidad/destino.php');
    include('admin/entidad/temporada.php');
    include('admin/entidad/tipo_paquete.php');
    include('admin/entidad/publicacion.php');
    include('admin/entidad/ciudad_origen_paquete.php');
    include('admin/entidad/tarifa_hotel.php');
    include('admin/entidad/tarifa_otros.php');
    include('admin/entidad/pqt_otro_servicio.php');
    include('admin/entidad/boleto_cotizado.php');
    include('admin/entidad/lineas_aereas.php');
    include('admin/entidad/aeropuerto.php');

    $c_mostrar_paquete  = new c_mostrar_paquete;
    $c_buscar_paquete = new c_buscar_paquete;

    //$resp_destino   = destino::obtener_datos_destino();
    $resp_temporada = temporada::obtener_datos_temporada();
    $resp_categoria = tipo_paquete::obtener_datos_tipo_paquete();
    $paquete        = publicacion::obtener_info_paquete($_GET['paquete']);
    $aux            = $paquete[0];

    $resp_tags_temporada  = $c_buscar_paquete->traer_publicacion_activo_x_temporada();
    $resp_tags_categoria  = $c_buscar_paquete->traer_publicacion_activo_x_categoria();
    ?>
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <div class="col-md-12 wbox" style="padding: 15px;">
              <h3>Buscar Paquetes</h3>
              <form class="" role="form" method="get" action="">
                <input type="hidden" name="controller" value="package">
            <input type="hidden" name="action" value="search">
                <div class="form-group">
                  <label for="titulo">Titulo</label>
                  <input type="text" class="form-control" id="titulo" name="titulo" value="" placeholder="Titulo">
                </div>
                <div class="form-group">
                  <label for="destino">Destino</label>
                  <input type="text" class="form-control" id="destino_nombre" value="" onkeyUp="verificar_vacio();" onkeypress="auto_completar('destino');" name="destino_nombre" placeholder="Destino" autocomplete="off"/>
                  <input type="hidden" id="destino" name="destino" value=""/>
                </div>
                <div class="form-group">
                  <label for="temporada">Temporada</label>
                   <select id="temporada" name="temporada" class="form-control">
                      <option value="">-- Temporada --</option>
                      <?php
                      foreach ($resp_tags_temporada as $key => $value)
                      {
                      ?>
                        <option value="<?php echo $key;?>" <?php echo ($_GET['temporada'] == $key)?"selected":"";?>><?php echo $value['nombre_temporada'];?></option>
                      <?php
                      }
                      ?>
                    </select>
                </div>
                <div class="form-group">
                  <label for="categoria">Categoria</label>
                  <select id="categoria" name="categoria" class="form-control">
                      <option value="">-- Categoria --</option>
                      <?php
                      foreach ($resp_tags_categoria as $key => $value)
                      {
                      ?>
                        <option value="<?php echo $key;?>" <?php echo ($_GET['categoria'] == $key)?"selected":"";?>><?php echo $value['descripcion'];?></option>
                      <?php
                      }
                      ?>
                    </select>


                </div>
                <button type="submit" class="btn btn-default">Buscar</button>
                <input type="hidden" name="pag" value="0">
              </form>
            </div>
          </div>
          <div class="col-md-9">

            <?php
            if(count($paquete))
            {
              $aux          = $paquete[0];
              $resp_ciudad  = ciudad_origen_paquete::obtener_ciudad_origen_paquete_de_idpublicacion($aux['idpublicacion']);
              $resp_imagen  = publicacion::obtener_imagenes_por_publicacion($aux['idpublicacion']);
            ?>
            <div class="row">
              <div class="col-md-5 wbox">
                <div class="row"><div class="col-md-12 text-center"><h2 class="text-info"><?php echo $aux['titulo'];?></h2></div></div>
                <div class="clearfix">&nbsp;</div>
                <div class="row">
                  <div class="col-md-6">Codigo: <h3><?php echo $aux['idpublicacion'];?></h3></div>
                  <div class="col-md-6">Precio desde:<h3>$&nbsp;<?php echo $aux['precio_desde'];?></h3></div>
                </div>
                <div class="row">
                  <div class="col-md-6">Vigente Desde: <h3><?php echo $aux['fecha_salida']?></h3></div>
                  <div class="col-md-6">Hasta: <h3><?php echo $aux['fecha_retorno']?></h3></div>
                </div>
                <div class="clearfix">&nbsp;</div>
              </div>
              <div class="col-md-7 ">
                <?php
                if (count($resp_imagen) > 0)
                {
                ?>
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <?php
                    for ($i = 0; $i < count($resp_imagen); $i++)
                    {
                    ?>
                    <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i;?>" class="<?php echo ($i == 0)?"active":"";?>"></li>
                    <?php
                    }
                    ?>
                  </ol>
                  <div class="carousel-inner" role="listbox">
                    <?php
                    for ($i = 0; $i < count($resp_imagen); $i++)
                    {
                    ?>
                    <div class="item <?php echo ($i == 0)?"active":"";?>">
                      <img src="http://barrybolivia.com/zion/paquetes/admin/images_paquetes/<?php echo $resp_imagen[$i]['nombre']?>" class="img-responsive" alt="Responsive image">
                      <div class="carousel-caption">&nbsp;</div>
                    </div>
                    <?php
                    }
                    ?>
                  </div>
                  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
                <?php
                }
                else
                {
                  if($aux['imagen_primera'] == "")
                  {
                    $nombre_fichero = "http://barrybolivia.com/zion/paquetes/admin/images_paquetes/no_foto_p.jpg"; 
                  }
                  else
                  {
                    $nombre_fichero = "http://barrybolivia.com/zion/paquetes/admin/images_paquetes/".$aux['imagen_primera'];
                  }

                  echo "<img src='".$nombre_fichero."' class='img-responsive' alt='Responsive image'>";
                }
                ?>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="service_book col-md-12 wbox" style="padding: 15px;">
                <ul class="nav nav-tabs col-md-10">
                  <?php
                  for ($i = 0; $i < count($resp_ciudad); $i++)
                  {
                  ?>
                  <li role="presentation" id="li_destino_<?php echo $i;?>" class="li_destino <?php echo ($i == 0)?"active":""; ?>" style="text-align:center;" onclick="ver_div_origen('<?php echo $i;?>');">
                    <a href="javascript:void(0)">Desde<br><?php echo $resp_ciudad[$i]['ciudad'];?></a>
                  </li>
                  <?php
                  }
                  ?>
                </ul>
                <div class="col-md-2 text-right hidden-print avoid-mailing">
                  <div class="link" onclick="showSendMail('Paquete: <?php  echo $aux['titulo'];?>', 'div_origen_<?php echo 0;?>');">
                    <span class="fa-stack fa-2x">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
                    </span>
                  </div>
                  <div class="link" onclick="printDiv('div_origen_<?php echo 0;?>');">
                    <span class="fa-stack fa-2x">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <i class="fa fa-print fa-stack-1x fa-inverse"></i>
                    </span>
                  </div>
                </div>
                <?php
                for ($i = 0; $i < count($resp_ciudad); $i++)
                {
                ?>

                <div class="row div_origen" id="div_origen_<?php echo $i;?>" style="<?php echo ($i > 0)?"display:none":"";?>">
                  <div class="col-md-12">
                    <div class="hide">
                      <table style="width: 100%;">
                        <tr>
                          <td style="width: 32%;">
                            <?  $_SESSION['current_agency']->showLogo(); ?>
                          </td>
                          <td style="width: 40%; padding-top: 10px; font-size: 12px">
                            <strong><? echo $_SESSION['current_agency']->name; ?></strong><br />
                            <? echo $_SESSION['current_agency']->address; ?><br />
                            <? echo $_SESSION['current_agency']->phone; ?><br />
                            <? echo $_SESSION['current_agency']->mail; ?>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <h3>Paquete Incluye</h3>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <ul>
                        <?php
                          $paquete_incluye = explode("\n", $resp_ciudad[$i]['paquete_incluye']);

                          for($j = 0; $j < count($paquete_incluye); $j++)
                          {
                            if($paquete_incluye[$j] != "")
                            {
                              echo "<li>".$paquete_incluye[$j]."</li>";
                            }
                          }
                        ?>
                        </ul>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <!--HOTELES-->
                        <div class="row"><div class="col-md-12 text-center" style="text-align:center"><h2>Hotel</h2></div></div>
                        <?php
                        $tarifas_por_destino = $c_mostrar_paquete->obtener_destinos_de_hotel($resp_ciudad[$i]['idciudad_origen_paquete']);

                        if (count($tarifas_por_destino) > 0)
                        {
                          for($j = 0; $j < count($tarifas_por_destino); $j++)
                          {
                            $tarifa_hotel = $tarifas_por_destino[$j];
                            $idth         = $tarifa_hotel['idtarifa_hotel'];
                            $items_tarifa = $c_mostrar_paquete->obtener_hoteles_x_destino($idth);
                            $hide_columns = $c_mostrar_paquete->evaluar_precios_hotel($items_tarifa);
                            $cant_col     = count(explode('/', $hide_columns));
                            ?>
                            <div style="margin-top: 5px; padding: 5px; background-color: #fff;" >
                              <div><strong>Destino: </strong><?php echo strtoupper($tarifa_hotel['destino']); ?></div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="table-responsive">
                                  <table class="table table-bordered" style="border-collapse: collapse;width:100%;border:1px solid #B8B7B7;">
                                    <thead>
                                      <tr class="active" style="border:1px solid #B8B7B7;">
                                        <th class="text-center" style="border:1px solid #B8B7B7;">Hotel</th>
                                        <?php if(strpos($hide_columns, 'SGL')===false) { ?><th class="text-center" style="border:1px solid #B8B7B7;">Single</th><?php } ?>
                                        <?php if(strpos($hide_columns, 'DBL')===false) { ?><th class="text-center" style="border:1px solid #B8B7B7;">Doble</th><?php } ?>
                                        <?php if(strpos($hide_columns, 'TPL')===false) { ?><th class="text-center" style="border:1px solid #B8B7B7;">Triple</th><?php } ?>
                                        <?php if(strpos($hide_columns, 'CPL')===false) { ?><th class="text-center" style="border:1px solid #B8B7B7;">Cuadruple</th><?php } ?>
                                        <?php if(strpos($hide_columns, 'CNN')===false) { ?><th class="text-center" style="border:1px solid #B8B7B7;">Menor</th><?php } ?>
                                        <?php if(strpos($hide_columns, 'INF')===false) { ?><th class="text-center" style="border:1px solid #B8B7B7;">Infante</th><?php } ?>
                                        <th class="text-center" style="border:1px solid #B8B7B7;">Alimentacion</th>
                                        <th class="text-center" style="border:1px solid #B8B7B7;">Obs</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                  <?php
                                  for($k = 0; $k < count($items_tarifa); $k++)
                                  {
                                    $item_tarifa  = $items_tarifa[$k];
                                    $iditem       = $item_tarifa['iditem_hotel'];
                                    $cant_noches  = floor((strtotime($item_tarifa['fecha_out'].' 00:00:00') - strtotime($item_tarifa['fecha_in'] . ' 00:00:00')) / 86400);

                                    $total_sgl  = $item_tarifa['precio_single'] * $cant_noches;
                                    $total_dbl  = $item_tarifa['precio_doble'] * $cant_noches;
                                    $total_tpl  = $item_tarifa['precio_triple'] * $cant_noches;
                                    $total_cpl  = $item_tarifa['precio_cuadruple'] * $cant_noches;
                                    $total_cnn  = $item_tarifa['precio_menor'] * $cant_noches;
                                    $total_inf  = $item_tarifa['precio_infante'] * $cant_noches;

                                    if($item_tarifa['increment_fee'] == 'P')
                                    {
                                      $fee    = 1 + ($item_tarifa['fee']/100);
                                      $total_sgl  *= $fee;
                                      $total_dbl  *= $fee;
                                      $total_tpl  *= $fee;
                                      $total_cpl  *= $fee;
                                      $total_cnn  *= $fee;
                                      $total_inf  *= $fee;
                                    }
                                    else
                                    {
                                      $fee    = $item_tarifa['fee'];
                                      if($item_tarifa['precio_single'] > 0)
                                        $total_sgl  += $fee;
                                      else
                                        $total_sgl  = 0;
                                      if($item_tarifa['precio_doble'] > 0)
                                        $total_dbl  += $fee;
                                      else
                                        $total_dbl  = 0;
                                      if($item_tarifa['precio_triple'] > 0)
                                        $total_tpl  += $fee;
                                      else
                                        $total_tpl  = 0;
                                      if($item_tarifa['precio_cuadruple'] > 0)
                                        $total_cpl  += $fee;
                                      else
                                        $total_cpl  = 0;
                                      if($item_tarifa['precio_menor'] > 0)
                                        $total_cnn  += $fee;
                                      else
                                        $total_cnn  = 0;
                                      if($item_tarifa['precio_infante'] > 0)
                                        $total_inf  += $fee;
                                      else
                                        $total_inf  = 0;
                                    }

                                    if($item_tarifa['increment_factura'] == 'P')
                                    {
                                      $iva    = 1 + ($item_tarifa['factura']/100);
                                      $total_sgl  *= $iva;
                                      $total_dbl  *= $iva;
                                      $total_tpl  *= $iva;
                                      $total_cpl  *= $iva;
                                      $total_cnn  *= $iva;
                                      $total_inf  *= $iva;
                                    }
                                    else
                                    {
                                      $iva    = $item_tarifa['factura'];
                                      if($item_tarifa['precio_single'] > 0)
                                        $total_sgl  += $iva;
                                      else
                                        $total_sgl  = 0;
                                      if($item_tarifa['precio_doble'] > 0)
                                        $total_dbl  += $iva;
                                      else
                                        $total_dbl  = 0;
                                      if($item_tarifa['precio_triple'] > 0)
                                        $total_tpl  += $iva;
                                      else
                                        $total_tpl  = 0;
                                      if($item_tarifa['precio_cuadruple'] > 0)
                                        $total_cpl  += $iva;
                                      else
                                        $total_cpl  = 0;
                                      if($item_tarifa['precio_menor'] > 0)
                                        $total_cnn  += $iva;
                                      else
                                        $total_cnn  = 0;
                                      if($item_tarifa['precio_infante'] > 0)
                                        $total_inf  += $iva;
                                      else
                                        $total_inf  = 0;
                                    }

                                    if($item_tarifa['moneda'] == 'B')
                                    {
                                      $tc     = $item_tarifa['tipo_cambio'];
                                      $total_sgl /= $tc;
                                      $total_dbl /= $tc;
                                      $total_tpl /= $tc;
                                      $total_cpl /= $tc;
                                      $total_cnn /= $tc;
                                      $total_inf /= $tc;
                                    }
                                    ?>
                                    <tr>
                                      <td style="border:1px solid #B8B7B7;"><?php echo $item_tarifa['nombre_hotel'] . ' - ' . $item_tarifa['categoria'] . '*'; ?></td>
                                      <?php if(strpos($hide_columns, 'SGL')===false) { ?><td style="text-align:right;border:1px solid #B8B7B7;"><?php if($total_sgl>0) echo ceil($total_sgl); else echo '-'; ?></td><?php } ?>
                                      <?php if(strpos($hide_columns, 'DBL')===false) { ?><td style="text-align:right;border:1px solid #B8B7B7;"><?php if($total_dbl>0) echo ceil($total_dbl); else echo '-'; ?></td><?php } ?>
                                      <?php if(strpos($hide_columns, 'TPL')===false) { ?><td style="text-align:right;border:1px solid #B8B7B7;"><?php if($total_tpl>0) echo ceil($total_tpl); else echo '-'; ?></td><?php } ?>
                                      <?php if(strpos($hide_columns, 'CPL')===false) { ?><td style="text-align:right;border:1px solid #B8B7B7;"><?php if($total_cpl>0) echo ceil($total_cpl); else echo '-'; ?></td><?php } ?>
                                      <?php if(strpos($hide_columns, 'CNN')===false) { ?><td style="text-align:right;border:1px solid #B8B7B7;"><?php if($total_cnn>0) echo ceil($total_cnn); else echo '-'; ?></td><?php } ?>
                                      <?php if(strpos($hide_columns, 'INF')===false) { ?><td style="text-align:right;border:1px solid #B8B7B7;"><?php if($total_inf>0) echo ceil($total_inf); else echo '-'; ?></td><?php } ?>
                                      <td style="border:1px solid #B8B7B7;"><?php echo strtoupper($item_tarifa['alimentacion']); ?></td>
                                      <td style="border:1px solid #B8B7B7;"><?php echo strtoupper($item_tarifa['info_extra']); ?></td>
                                    </tr>
                                    <?php
                                  }
                                  ?>
                                  </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                            <?php
                          }
                        }
                        ?>
                        <!--OTROS SERVICIOS-->
                        <?php
                        $otras_tarifas  = $c_mostrar_paquete->obtener_otros_servicios_x_ciudad($resp_ciudad[$i]['idciudad_origen_paquete']);
                        $serv_x_dest    = array();
                        $total_serv_adt = 0;
                        $total_serv_cnn = 0;
                        $total_serv_inf = 0;
                        if(count($otras_tarifas) > 0)
                        {
                        ?>
                        <div class="clearfix">&nbsp;</div>
                        <div class="row"><div class="col-md-12 text-center" style="text-align:center"><h2>Otros Servicios</h2></div></div>
                        <div class="row">
                          <div class="col-md-12">
                            <table class="table table-bordered" style="border-collapse: collapse;width:100%;border:1px solid #B8B7B7;">
                            <tr class="active">
                              <th rowspan="2" class="text-center" style="vertical-align:middle;border:1px solid #B8B7B7;">Detalle</th>
                              <th colspan="3" class="text-center" style="border:1px solid #B8B7B7;">Precios</th>
                            </tr>
                            <tr class="active">
                              <th class="text-center" style="border:1px solid #B8B7B7;">Adulto</th>
                              <th class="text-center" style="border:1px solid #B8B7B7;">Menor</th>
                              <th class="text-center" style="border:1px solid #B8B7B7;">Infante</th>
                            </tr>
                            <?php
                            for($j = 0; $j < count($otras_tarifas); $j++)
                            {
                              $tarifa_otros   = $otras_tarifas[$j];
                              $idotra_tarifa  = $tarifa_otros['idtarifa_otros'];
                              $ciudad_serv    = $tarifa_otros['ciudad'];
                              $total_adt      = $tarifa_otros['precio_adulto'];
                              $total_cnn      = $tarifa_otros['precio_menor'];
                              $total_inf      = $tarifa_otros['precio_infante'];

                              if($tarifa_otros['increment_fee'] == 'P')
                              {
                                $fee    = 1+($tarifa_otros['fee'] / 100);
                                $total_adt  *= $fee;
                                $total_cnn  *= $fee;
                                $total_inf  *= $fee;
                              }
                              else
                              {
                                $fee = $tarifa_otros['fee'];
                                if($total_adt > 0)
                                  $total_adt += $fee;
                                else
                                  $total_adt = 0;
                                if($total_cnn > 0)
                                  $total_cnn += $fee;
                                else
                                  $total_cnn = 0;
                                if($total_inf > 0)
                                  $total_inf += $fee;
                                else
                                  $total_inf = 0;
                              }

                              if($tarifa_otros['increment_factura'] == 'P')
                              {
                                $iva    = 1+($tarifa_otros['factura'] / 100);
                                $total_adt  *= $iva;
                                $total_cnn  *= $iva;
                                $total_inf  *= $iva;
                              }
                              else
                              {
                                $iva = $tarifa_otros['factura'];
                                if($total_adt > 0)
                                  $total_adt += $iva;
                                else
                                  $total_adt = 0;
                                if($total_cnn > 0)
                                  $total_cnn += $iva;
                                else
                                  $total_cnn = 0;
                                if($total_inf > 0)
                                  $total_inf += $iva;
                                else
                                  $total_inf = 0;
                              }

                              if($tarifa_otros['moneda'] == 'B')
                              {
                                $tc     = $tarifa_otros['tipo_cambio'];
                                $total_adt /= $tc;
                                $total_cnn /= $tc;
                                $total_inf /= $tc;
                              }

                              $total_serv_adt += ceil($total_adt);
                              $total_serv_cnn += ceil($total_cnn);
                              $total_serv_inf += ceil($total_inf);

                              if(!array_key_exists($ciudad_serv, $serv_x_dest))
                              {
                                $serv_x_dest[$ciudad_serv]        = array();
                                $serv_x_dest[$ciudad_serv]['adt'] = 0;
                                $serv_x_dest[$ciudad_serv]['cnn'] = 0;
                                $serv_x_dest[$ciudad_serv]['inf'] = 0;
                              }

                              $serv_x_dest[$ciudad_serv]['adt'] += ceil($total_adt);
                              $serv_x_dest[$ciudad_serv]['cnn'] += ceil($total_cnn);
                              $serv_x_dest[$ciudad_serv]['inf'] += ceil($total_inf);

                              $nombre_servicio = $c_mostrar_paquete->obtener_nombre_servicio($tarifa_otros['idotro_servicio']);
                            ?>
                            <tr>
                              <td style="border:1px solid #B8B7B7;"><?php echo strtoupper($nombre_servicio . ' ' . $tarifa_otros['descripcion']); ?></td>
                              <td style="text-align:right;border:1px solid #B8B7B7;">
                                <?php if($total_adt>0) echo ceil($total_adt); else echo '-'; ?>
                              </td>
                              <td style="text-align:right;border:1px solid #B8B7B7;">
                                <?php if($total_cnn>0) echo ceil($total_cnn); else echo '-'; ?>
                              </td>
                              <td style="text-align:right;border:1px solid #B8B7B7;">
                                <?php if($total_inf>0) echo ceil($total_inf); else echo '-'; ?>
                              </td>
                            </tr>
                            <?php
                            }
                            ?>
                            </table>
                          </div>
                        </div>
                        <?php
                        }
                        ?>
                        <!--BOLETOS-->
                        <?php
                        $opciones_aereo = $c_mostrar_paquete->obtener_opcion_boletos_x_ciudad($resp_ciudad[$i]['idciudad_origen_paquete']);

                        if(count($opciones_aereo) > 0)
                        {
                          for($j = 0; $j < count($opciones_aereo); $j++)
                          {
                            $opcion_aereo = $opciones_aereo[$j];
                            $idopaereo    = $opcion_aereo['idopcion_boleto'];
                          ?>
                          <div class="clearfix">&nbsp;</div>
                          <div class="row"><div class="col-md-12 text-center" style="text-align:center"><h2>Ticket Aereo</h2></div></div>
                          <div class="row">
                            <div class="col-md-8" >
                              <div><strong>Opcion: </strong><?php echo strtoupper($opcion_aereo['descripcion']); ?></div>
                              <div><strong>Sale: </strong><?php echo strtoupper($opcion_aereo['salida']); ?></div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <table class="table table-bordered " style="border-collapse: collapse;width:100%;border:1px solid #B8B7B7;">
                              <tr class="active">
                                <th rowspan="2" class="text-center" style="vertical-align:middle;border:1px solid #B8B7B7;">Linea Aerea</th>
                                <th rowspan="2" class="text-center" style="vertical-align:middle;border:1px solid #B8B7B7;">Ruta</th>
                                <th colspan="3" class="text-center" style="border:1px solid #B8B7B7;">Precio</th>
                              </tr>
                              <tr class="active">
                                <th class="text-center" style="border:1px solid #B8B7B7;">Adulto</th>
                                <th class="text-center" style="border:1px solid #B8B7B7;">Menor</th>
                                <th class="text-center" style="border:1px solid #B8B7B7;">Infante</th>
                              </tr>
                              <?php
                              $boletos  = $c_mostrar_paquete->obtener_boletos_x_opcion_aereo($idopaereo);

                              for ($j=0; $j < count($boletos); $j++)
                              {
                                $boleto_cotizado  = $boletos[$j];
                                $idboleto         = $boleto_cotizado['idboleto_cotizado'];
                                $total_adt        = $boleto_cotizado['precio_adulto'];
                                $total_cnn        = $boleto_cotizado['precio_menor'];
                                $total_inf        = $boleto_cotizado['precio_infante'];

                                if($boleto_cotizado['increment_fee'] == 'P')
                                {
                                  $fee        = 1+($boleto_cotizado['fee'] / 100);
                                  $total_adt  *= $fee;
                                  $total_cnn  *= $fee;
                                  $total_inf  *= $fee;
                                }
                                else
                                {
                                  $fee = $boleto_cotizado['fee'];
                                  if($total_adt > 0)
                                    $total_adt += $fee;
                                  else
                                    $total_adt = 0;
                                  if($total_cnn > 0)
                                    $total_cnn += $fee;
                                  else
                                    $total_cnn = 0;
                                  if($total_inf > 0)
                                    $total_inf += $fee;
                                  else
                                    $total_inf = 0;
                                }

                                if($boleto_cotizado['increment_factura'] == 'P')
                                {
                                  $iva    = 1+($boleto_cotizado['factura'] / 100);
                                  $total_adt  *= $iva;
                                  $total_cnn  *= $iva;
                                  $total_inf  *= $iva;
                                }
                                else
                                {
                                  $iva = $boleto_cotizado['factura'];
                                  if($total_adt > 0)
                                    $total_adt += $iva;
                                  else
                                    $total_adt = 0;
                                  if($total_cnn > 0)
                                    $total_cnn += $iva;
                                  else
                                    $total_cnn = 0;
                                  if($total_inf > 0)
                                    $total_inf += $iva;
                                  else
                                    $total_inf = 0;
                                }

                                if($boleto_cotizado['moneda'] == 'B')
                                {
                                  $tc         = $boleto_cotizado['tipo_cambio'];
                                  $total_adt /= $tc;
                                  $total_cnn /= $tc;
                                  $total_inf /= $tc;
                                }
                                //$total_bol_adt += $total_adt;
                                //$total_bol_cnn += $total_cnn;
                                //$total_bol_inf += $total_inf;

                              ?>
                                <tr>
                                  <td style="border:1px solid #B8B7B7;"><?php echo strtoupper($c_mostrar_paquete->obtener_linea_aerea($boleto_cotizado['idlineas_aereas'])); ?></td>
                                  <td style="border:1px solid #B8B7B7;"><?php echo strtoupper($boleto_cotizado['ruta']); ?></td>
                                  <td style="text-align:right;border:1px solid #B8B7B7;"><?php if($total_adt>0) echo ceil($total_adt); else echo '-'; ?></td>
                                  <td style="text-align:right;border:1px solid #B8B7B7;"><?php if($total_cnn>0) echo ceil($total_cnn); else echo '-'; ?></td>
                                  <td style="text-align:right;border:1px solid #B8B7B7;"><?php if($total_inf>0) echo ceil($total_inf); else echo '-'; ?></td>
                                </tr>
                              <?php
                              }
                              ?>
                              </table>
                            </div>
                          </div>
                          <div class="clearfix">&nbsp;</div>
                          <div class="row"><div class="col-md-12" style="text-align:center"><strong>Detalle Aereo</strong></div></div>
                          <div class="row">
                            <div class="col-md-12">
                              <table class="table table-bordered" style="border-collapse: collapse;width:100%;border:1px solid #B8B7B7;">
                              <tr class="active">
                                <th class="text-center" style="border:1px solid #B8B7B7;">Linea Aerea</th>
                                <th class="text-center" style="border:1px solid #B8B7B7;">Vuelo</th>
                                <th class="text-center" style="border:1px solid #B8B7B7;">Fecha</th>
                                <th class="text-center" style="border:1px solid #B8B7B7;">Sale</th>
                                <th class="text-center" style="border:1px solid #B8B7B7;">Llega</th>
                              </tr>
                              <?php
                              $itinerario = $c_mostrar_paquete->obtener_itinerario_x_opcion_aereo($idopaereo);

                              for ($k=0; $k < count($itinerario); $k++)
                              {
                                $segmento         = $itinerario[$k];
                                $idsegmento       = $segmento['idsegmento'];
                                $linea_aerea      = $c_mostrar_paquete->obtener_linea_aerea($segmento['idlineas_aereas']);
                                $aeropuerto_sale  = $c_mostrar_paquete->obtener_aeropuerto($segmento['origen']);
                                $aeropuerto_llega = $c_mostrar_paquete->obtener_aeropuerto($segmento['destino']);
                              ?>
                              <tr>
                                <td style="border:1px solid #B8B7B7;"><?php echo strtoupper($linea_aerea); ?></td>
                                <td style="border:1px solid #B8B7B7;"><?php echo $segmento['nro_vuelo']; ?></td>
                                <td style="border:1px solid #B8B7B7;"><?php echo date('d/m/Y', strtotime($segmento['fecha'])); ?></td>
                                <td style="border:1px solid #B8B7B7;"><?php echo strtoupper($aeropuerto_sale) . ' <font style="font-size:7pt;">A HRS</font> ' . $segmento['hora_sale']; ?></td>
                                <td style="border:1px solid #B8B7B7;"><?php echo strtoupper($aeropuerto_llega) . ' <font style="font-size:7pt;">A HRS</font> ' . $segmento['hora_llega']; ?></td>
                              </tr>
                              <?php
                              }
                              ?>
                              </table>
                            </div>
                          </div>
                          <?php
                          }
                        }
                        ?>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <h3>Importante</h3>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <ul>
                        <?php
                          $paquete_importante = explode("\n", $resp_ciudad[$i]['importante']);

                          for($j = 0; $j < count($paquete_importante); $j++)
                          {
                            if($paquete_importante[$j] != "")
                            {
                              echo "<li>".$paquete_importante[$j]."</li>";
                            }
                          }
                        ?>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <?php
                }
                ?>
              </div>
            </div>
            <?php
            }
            ?>
          </div>
        </div>

      </div>
    </div>
    <script type="text/javascript">

    function ver_div_origen(id)
    {
      $('.div_origen').hide();
      $(".li_destino").removeClass("active");
      $("#li_destino_"+id).addClass("active");
      $('#div_origen_'+id).show();
    }

    function verificar_vacio()
    {
      if ($('#destino_nombre').val() == "")
      {
        $('#destino').val("0");
      }
    }

    function auto_completar()
    {
      if($('#destino_nombre').val().length > 1)
      {

        $("#destino_nombre").autocomplete(
        {
          source: "paquetes/search_destino.php?texto="+$('#destino_nombre').val(),
          minLength: 2,
          select: function( event, ui )
          {
            $('#destino').val(ui.item.id);
          }
        });
      }
    }
    </script>
