<?php
ob_start();
include_once('includes.php');
session_start();
date_default_timezone_set('America/La_Paz');

UserController::verifySession();

$controller_name = isset($_GET['controller']) ? $_GET['controller'] : 'accommodation';
$action_name = isset($_GET['action']) ? $_GET['action'] : 'home';
?>
  <?php ApplicationController::loadActionCSS($controller_name, $action_name); ?>
  <?php ApplicationController::loadActionJS($controller_name, $action_name); ?>
  <?php ApplicationController::loadAction($controller_name, $action_name); ?>

  </div>

<?php ob_end_flush(); ?>
