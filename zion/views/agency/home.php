<?php global $agencies; ?>

<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-3"><?php include('views/admin/_menu.php'); ?></div>

      <div class="col-md-9 wbox" style="width: 73.7%;">
      <h2>Lista de Agencias</h2>
      <div class="row">
        <form method="get" id="form_status">
        <?php $manager = AgencyManager::find(array('agency_id' => $_SESSION['current_agency']->id)); ?>
        <?php if (count($manager) == 0) { ?>
              <div class="col-md-4">
                <label>Ciudad</label>
                <select name="city" onChange="$('#form_status').submit();">
                  <option>Todos</option>
                  <option value="34" <?php echo isset($_GET['city']) && $_GET['city'] == 34 ? "selected" : "" ; ?> >Santa Cruz</option>
                  <option value="28" <?php echo isset($_GET['city']) && $_GET['city'] == 28 ? "selected" : "" ; ?> >Cochabamba</option>
                  <option value="30" <?php echo isset($_GET['city']) && $_GET['city'] == 30 ? "selected" : "" ; ?> >La Paz</option>
                </select>
              </div>
        <?php } ?>
        <div class="col-md-4">
          
            <input type="hidden" name="controller" value="agency" />
            <input type="hidden" name="action" value="home" />
            <label for="exampleInputEmail2">Estado : </label>
            <select class="" id="status" name="status" onChange="$('#form_status').submit();">
              <option value="1" <?php echo (isset($_GET['status']) && $_GET['status'] == 1) ? 'selected' : '' ?> >Activos</option>
              <option value="0" <?php echo (isset($_GET['status']) && $_GET['status'] == 0) ? 'selected' : '' ?> >Deshabilitados</option>
            </select>
          
        </div>
        </form>
        <div class="col-md-4">
          <a href="?controller=agency&amp;action=add"><i class="fa fa-plus"></i> Agregar Agencia</a>
        </div>
      </div>
      <table class="table table-striped table-hover">
        <thead>
          <tr>
            <th></th>
            <th>Nombre</th>
            <th>Estado</th>
            <th>Ciudad</th>
            <th>Permisos</th>
            <th>Comision</th>
            <!-- <th>Fee Agencia</th> -->
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach ($agencies as $agency) {
            $ciudad = Ciudad::findById($agency->ciudad_id);
            ?>
            <tr>
              <td><div class="mini_logo"><?php //$agency->showLogo(); ?></div></td>
              <td><?php echo $agency->name; ?></td>
              <td><?php echo $agency->status == 1 ? 'Activo' : 'Deshabilitado'; ?></td>
              <td><?php echo isset($ciudad->nombre) ? $ciudad->nombre : ""; ?></td>
              <td><?php echo $agency->booking == 'BOOK' ? 'Reservar' : 'Emitir'; ?></td>
              <td><?php echo $agency->commission; ?>%</td>
              <!-- <td><?php //echo isset($agency->extra_fee) && $agency->extra_fee != 0 ? $agency->extra_fee . '%' : ''; ?></td> -->
              <td>
                <a href="?controller=agency&amp;action=show&amp;id=<?php echo $agency->id; ?>">Ver</a>
              </td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(function() {
    $('#agency_home').addClass('active');
  });
</script>
