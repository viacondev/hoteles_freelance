<?php global $agency; ?>

<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-3"><?php include('views/admin/_menu.php'); ?></div>

      <div class="col-md-9 wbox" style="width:73.7%">
      <h2>Editar Agencia</h2>

      <form method="post" action="?controller=agency&amp;action=update" enctype="multipart/form-data">
        <div class="col-md-7">
          <div class="form-group col-md-12">
            <label>Nombre:</label>
            <input type="text" class="form-control" id="name" name="name" value="<?php echo $agency->name;?>">
          </div>
          <div class="form-group col-md-12">
            <label>Tel&eacute;fonos:</label>
            <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $agency->phone;?>">
          </div>
          <div class="form-group col-md-12">
            <label>Correo:</label>
            <input type="text" class="form-control" id="mail" name="mail" value="<?php echo $agency->mail;?>">
          </div>
          <div class="form-group col-md-12">
            <label>Direcci&oacute;n:</label>
            <textarea class="form-control" id="address" name="address"><?php echo $agency->address;?></textarea>
          </div>

          <div class="form-group col-md-12">
            <label>Ciudad:</label>
            <select class="form-control" name="city">
              <option value="" >Sin Seleccionar</option>
              <option value="34" <?php echo $agency->ciudad_id == 34 ? "selected" : "" ; ?>>Santa Cruz</option>
              <option value="28" <?php echo $agency->ciudad_id == 28 ? "selected" : "" ; ?>>Cochabamba</option>
              <option value="30" <?php echo $agency->ciudad_id == 30 ? "selected" : "" ; ?>>La Paz</option>
            </select>  
          </div>

          <div class="form-group col-md-12">
            <label>Permisos:</label><br />
            <input type="radio" name="booking" value="BOOK" <?php echo $agency->booking == 'BOOK' ? 'checked' : ''; ?>> Reservar</input> &nbsp;
            <input type="radio" name="booking" value="EMIT" <?php echo $agency->booking == 'EMIT' ? 'checked' : ''; ?>> Emitir</input>
          </div>

          <div class="form-group col-md-12">
            <label>Estado:</label><br />
            <input type="radio" name="status" value="1" <?php echo $agency->status == 1 ? 'checked' : ''; ?>> Activo</input> &nbsp;
            <input type="radio" name="status" value="0" <?php echo $agency->status == 0 ? 'checked' : ''; ?>> Deshabilitado</input>
          </div>

          <div class="form-group col-md-12">
            <label>Comisi&oacute;n: </label> &nbsp;
            <input type="text" id="commission" name="commission" value="<?php echo $agency->commission; ?>" size="3" /> %
          </div>

          <div class="form-group col-md-12">
            <input type="hidden" name="id" value="<?php echo $agency->id;?>" />
            <button class="btn btn-info-css">Guardar</button>
          </div>
        </div>
        <div class="col-md-4">
          <label>Logo:</label>
          <div class="small text-muted">Tama&ntilde;o recomentado: 300x80px</div>
          <input type="file" name="logo" class="form-control" />
        </div>
      </form>
    </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(function() {
    $('#agency_home').addClass('active');
  });
</script>
