<?php global $agency, $users; ?>

<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-3"><?php include('views/admin/_menu.php'); ?></div>

      <div class="col-md-9 wbox" style="width:73.7%">
      <h2>Datos Agencia</h2>
      <div class="col-md-12 text-right">
        <a href="?controller=agency&amp;action=edit&amp;id=<?php echo $agency->id; ?>"><i class="fa fa-pencil"></i> Editar Agencia</a>
      </div>
      <div class="col-md-12 logo"><?php //$agency->showLogo(); ?></div>
      <table class="table table-hover">
        <tbody>
          <tr>
            <td><strong>Nombre:</strong></td>
            <td><?php echo $agency->name; ?></td>
          </tr>
          <tr>
            <td><strong>Tel&eacute;fonos:</strong></td>
            <td><?php echo $agency->phone; ?></td>
          </tr>
          <tr>
            <td><strong>Correo:</strong></td>
            <td><?php echo $agency->mail; ?></td>
          </tr>
          <tr>
            <td><strong>Direcci&oacute;n:</strong></td>
            <td><?php echo $agency->address; ?></td>
          </tr>
          <tr>
            <td><strong>Permisos:</strong></td>
            <td><?php echo $agency->booking == 'BOOK' ? 'Reservar' : 'Emitir'; ?></td>
          </tr>
          <tr>
            <td><strong>Estado:</strong></td>
            <td><?php echo $agency->status == 1 ? 'Activo' : 'Deshabilitado'; ?></td>
          </tr>
        </tbody>
      </table>

      <div class="col-md-7 rborder">
        <h3>Lista Usuarios</h3>
        <div class="col-md-12 text-right">
          <div class="link" onclick="setAgencyId(<?php echo $agency->id; ?>)"><i class="fa fa-plus"></i> Agregar Usuario</div>
        </div>
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Usuario</th>
              <th>Rol</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach ($users as $user) {
              echo '<tr>';
              echo '<td>' . $user->name . ' ' . $user->lastname . '</td>';
              echo '<td>' . $user->username . '</td>';
              $role = $user->role == 'ADMIN' ? 'Admin' : 'Counter';
              echo '<td>' . $role . '</td>';
              echo '<td><a href="?controller=user&amp;action=edit&amp;id=' . $user->id . '"><i class="fa fa-pencil"></i> Editar</a></td>';
              echo '</tr>';
            }
            ?>
          </tbody>
        </table>
      </div>

      <div class="col-md-5">
        <h3>Comisi&oacute;n Agencia</h3>
        <strong>Comisi&oacute;n general:</strong> <?php echo $agency->commission; ?>%
        &nbsp; <a href="?controller=agency&amp;action=edit&amp;id=<?php echo $agency->id; ?>#commission"><i class="fa fa-pencil"></i> Editar</a>

        <form action="?controller=agency&amp;action=update">

        </form>
      </div>
    </div>
    </div>
  </div>
</div>

<form method="post" id="add_user_form" action="?controller=user&amp;action=add" class="hidden invisible">
  <input type="hidden" name="agency_id" id="agency_id" />
</form>

<script type="text/javascript">
  $(function() {
    $('#agency_home').addClass('active');
  });
</script>
