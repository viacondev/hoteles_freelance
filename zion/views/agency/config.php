<?php global $agency; ?>

<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-3"><?php include('views/admin/_menu.php'); ?></div>

      <div class="col-md-9 wbox" style="width:73.5%">
      <h2>Configuraci&oacute;n Agencia</h2>

      <form method="post" action="?controller=agency&amp;action=update_config">
        <div class="col-md-8">
          <div class="col-md-12">
            <h4>Service Fee Agencia</h4>
            <p class="small">
              Mediante este m&oacute;dulo podr&aacute;s a&ntilde;adir un <i>service fee</i> a tus clientes.
              El sistema mostrar&aacute; autom&aacute;ticamente el monto final a cobrar a tus clientes, lo cual
              es &uacute;til para enviarlo por mail o cuando tu cliente est&aacute; viendo tu pantalla. El <i>service fee</i>
              no influye en el valor real del servicio ni en el monto a pagar a la operadora.
              <br /><br />
              <strong>Atenci&oacute;n:</strong> Tu cuenta ya incluye una comisi&oacute;n del <strong><?php echo $agency->commission; ?>%</strong>,
              por lo cual no es necesario agregar un service fee. En caso de agregar un service fee se aconseja no usar un valor alto
              para que tu agencia mantega precios competitivos.
            </p>
          </div>
          <div class="form-group col-md-8">
            <div class="col-md-12"><label>Fee:</label></div>
            <div class="col-md-4">
              <input type="text" class="form-control" id="extra_fee" name="extra_fee" value="<?php echo $agency->extra_fee;?>">
            </div>
            <div class="col-md-4">%</div>
            <input type="hidden" name="extra_fee_type" value="PERC">
            <!-- <input type="radio" name="extra_fee_type" value="PERC" <?php echo $agency->extra_fee_type == 'PERC' ? 'checked' : '' ?>> % (porcentaje) &nbsp;
            <input type="radio" name="extra_fee_type" value="VALUE" <?php echo $agency->extra_fee_type == 'VALUE' ? 'checked' : '' ?>> Valor (absoluto) -->
          </div>
          <div class="form-group col-md-12">
            <button class="btn btn-info">Guardar</button>
          </div>
      </form>
    </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(function() {
    $('#agency_config').addClass('active');
  });
</script>
