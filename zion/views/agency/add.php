<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-3"><?php include('views/admin/_menu.php'); ?></div>

      <div class="col-md-9 wbox" style="width:73.7%">
      <h2>Agregar nueva agencia</h2>
      <form method="post" action="?controller=agency&amp;action=create" enctype="multipart/form-data">
        <div class="col-md-7">
          <div class="form-group col-md-12">
            <label>Nombre:</label>
            <input type="text" class="form-control" id="name" name="name">
          </div>
          <div class="form-group col-md-12">
            <label>Tel&eacute;fonos:</label>
            <input type="text" class="form-control" id="phone" name="phone">
          </div>
          <div class="form-group col-md-12">
            <label>Correo:</label>
            <input type="text" class="form-control" id="mail" name="mail">
          </div>
          <div class="form-group col-md-12">
            <label>Direcci&oacute;n:</label>
            <textarea class="form-control" id="address" name="address"></textarea>
          </div>

          <div class="form-group col-md-12">
            <label>Ciudad:</label>
            <select class="form-control" name="city">
              <option value="34">Santa Cruz</option>
              <option value="28">Cochabamba</option>
              <option value="30">La Paz</option>
            </select>  
          </div>

          <div class="form-group col-md-12">
            <label>Permisos:</label><br />
            <input type="radio" name="booking" value="BOOK" checked> Reservar</input> &nbsp;
            <input type="radio" name="booking" value="Emit"> Emitir</input>
          </div>

          <div class="form-group col-md-12">
            <label>Comisi&oacute;n: </label> &nbsp;
            <input type="text" id="commission" name="commission" value="10" size="3" /> %
          </div>

          <div class="form-group col-md-12">
            <button class="btn btn-info-css">Guardar</button>
          </div>
        </div>

        <div class="col-md-4">
          <label>Logo:</label>
          <input type="file" name="logo" class="form-control" />
        </div>
      </form>
    </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(function() {
    $('#agency_home').addClass('active');
  });
</script>
