<?php global $agency, $agencynomapping; ?>
<div class="content">
  <div class="container">
    <div class="wbox" style="padding:20px">
      <div class="form-group">
        <h3>Agencias de BarryBolivia</h3>
        <select id="agency" onchange="buscarUsuario();">
          <?php foreach ($agency as $key => $value) { ?>
              <option value="<?php echo $value->id ?>"><?php echo $value->name; ?></option>
          <?php } ?>
        </select>
        <img src="assets/images/ajax-loader.gif" style="display:none" id="loading">
      </div>
      <div class="form-group">
        <table id="table-user" class="table" border="1">
          <thead>
            <tr>
              <th>Id</th>
              <th>UserName</th>
              <th>Name</th>
              <th>*</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <div class="row" style="padding: 10px;">
        <div class="col-md-6">
          <form style="border: 1px solid #a09999;padding: 15px;">
            <div class="form-group" style="text-align:center;font-size: 18px;">
              <label class="title">Mapear Usuario<label>
            </div>
            <div class="form-group">
              <label for="exampleInputName2">Id_BB</label>
              <input type="text" id="id_bb" class="form-control" />
            </div>
            <div class="form-group">
              <label for="exampleInputName2">Id_Barry</label>
              <input type="text" id="id_barry" class="form-control" />
            </div>
            <div class="form-group">
              <label for="exampleInputName2">Name</label>
              <input type="text" id="name" class="form-control" />
            </div>
            <div class="form-group">
              <input type="button" class="btn btn-primary" value="Guardar" onclick="save()" />
            </div>
          </form>
        </div>
        <div class="col-md-6">
          <form style="border: 1px solid #a09999;padding: 15px;">
            <div class="form-group" style="text-align:center;font-size: 18px;">
              <label class="title">Mapear Agencia<label>
            </div>
            <div class="form-group">
              <label for="exampleInputName2">Agency_Id_BB</label>
              <input type="text" id="agency_id_bb" class="form-control" />
            </div>
            <div class="form-group">
              <label for="exampleInputName2">Agency_Id_Barry</label>
              <input type="text" id="agency_id_barry" class="form-control" />
            </div>
            <div class="form-group">
              <label for="exampleInputName2">Name</label>
              <input type="text" id="agency_name" class="form-control" />
            </div>
            <div class="form-group">
              <input type="button" class="btn btn-primary" value="Guardar" onclick="save_agency()" />
            </div>
          </form>
        </div>
      </div>
      <br/>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group" style="text-align:center;font-size: 18px;">
            <label class="title">Agencias sin Mapear</label>
            <select class="select_sistem" name="select_sistem">
              <option value="bb">BoliviaBooking</option>
              <option value="br">BarryBolivia</option>
            </select>
            <input type="button" name="btn_loadm" value="Cargar Agencias" onclick="ver_mapping()" style="float:right">
          </div>

          <div class="form-group">
            <table id="tb_not_mapping" class="table" border="1">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>*</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
          </div>
        </div>
        <div class="col-md-6">

        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="form-group" style="text-align:center;font-size: 18px;">
              <label class="title">Agencias sin Fee</label>
            </div>

            <div class="form-group">
              <table class="table table-striped" border="1">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>*</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($agencynomapping as $key => $value) { ?>
                      <tr>
                        <td><?php echo $value->id; ?></td>
                        <td><?php echo $value->name; ?></td>
                        <td><button data-info="<?php echo htmlspecialchars(json_encode($value), ENT_QUOTES, 'UTF-8') ?>"  onclick="guardarfee(this)" style='background: #ff3300;border-radius: 0px;color: white;border: 1px solid #ff3300;width: 80px;'>Guardar</button></td>
                      <tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>



    </div>
  </div>
<div>
<script type="text/javascript">
  function guardarfee(element) {
    var obj = $(element).data("info");
    console.log(obj);
    $.ajax({
      url: 'helpers/ajax/agency_user_mapping.php?save_fee="usuario"&id=' + obj.id + "&name="+obj.name ,
      success: function(res) {
        $('#loading').hide();
        console.log(res);

      }
    });
  }

  function buscarUsuario() {
    $('#table-user tbody').html('');
    $('#loading').show();
    $('#agency_id_barry').val($('#agency').val());
    $('#agency_name').val($("#agency option:selected").text());
    $.ajax({
      url: 'helpers/ajax/agency_user_mapping.php?buscar="usuario"&agency_id=' + $('#agency').val() + '',
      success: function(res) {
        console.log(res);
        $('#loading').hide();
        result = JSON.parse(res);
        tb = "";
        for(i = 0; i<result.length; i++) {

          tb += (result[i].ismapping) ? "<tr style='background-color:red;color:white;'>" : "<tr>";
          tb +=   "<td>" + result[i].id +  "</td>" +
                  "<td>" + result[i].username+  "</td>" +
                  "<td>" + result[i].name +  "</td>";
          tb +=  (result[i].ismapping) ? "<td></td>" : "<td><button data-info='" + JSON.stringify(result[i]) + "' onclick='agregar(this)' style='background: #ff3300;border-radius: 0px;color: white;border: 1px solid #ff3300;width: 80px;' >Add</button></td>";
          tb +=  "</tr>";
        }
        $('#table-user tbody').html(tb);
      }
    });
  }

  function agregar(but) {
    value = $(but).data("info");
    $('#id_barry').val(value.id);
    $('#name').val(value.name);
  }

  function save() {
    $('#loading').show();
    id_bb = $('#id_bb').val();
    id_barry = $('#id_barry').val();
    nombre = $('#name').val();

    $.ajax({
      url: 'helpers/ajax/agency_user_mapping.php?save="usuario"&user_bb=' + id_bb + '&user_barry=' + id_barry + '&name=' + nombre,
      success: function(res) {
        $('#loading').hide();
        console.log(res);
        $('#id_barry').val("");
        $('#name').val("");
        $('#id_bb').val("");
      }
    });

  }

  function save_agency() {
    $('#loading').show();
    id_bb = $('#agency_id_bb').val();
    id_barry = $('#agency_id_barry').val();
    nombre = $('#agency_name').val();

    $.ajax({
      url: 'helpers/ajax/agency_user_mapping.php?save_agency="usuario"&agency_bb=' + id_bb + '&agency_barry=' + id_barry + '&agency_name=' + nombre,
      success: function(res) {
        $('#loading').hide();
        console.log(res);
        $('#agency_id_barry').val("");
        $('#agency_name').val("");
        $('#agency_id_bb').val("");
      }
    });
  }

  function ver_mapping() {
    $('#tb_not_mapping tbody').html('');
    sistem = $('.select_sistem').val()
    $.ajax({
      url: 'helpers/ajax/agency_user_mapping.php?get_agency_not_mapping="get_agency_not_mapping"&sistem='+sistem,
      success: function(res) {
        result = JSON.parse(res);
        tb = "";
        for(i = 0; i<result.length; i++) {
          tb += (result[i].ismapping) ? "<tr style='background-color:red;color:white;'>" : "<tr>";
          tb +=   "<td>" + result[i].id +  "</td>" +
                  "<td>" + result[i].nombre +  "</td>";
          if (result[i].ismapping) {
            tb += "<td>" +
                     "<button data-info='" + JSON.stringify(result[i]) + "' class='show_bt_" + i + "' onclick='show_user_agency(this)' style='background: #ff3300;border-radius: 0px;color: white;border: 1px solid white;width: 80px;margin-left: 5px;'>Ver</button>" +
                  "</td>";
          } else {
            tb += "<td>"
            tb += "<button data-info='" + JSON.stringify(result[i]) + "' onclick='save_agency_Mapping(this)' style='background: #ff3300;border-radius: 0px;color: white;border: 1px solid #ff3300;width: 80px;'>Guardar</button>";
            tb += "<button data-info='" + JSON.stringify(result[i]) + "' class='show_bt_" + i + "' onclick='show_user_agency(this)'  style='background: #ff3300;border-radius: 0px;color: white;border: 1px solid #ff3300;width: 80px;margin-left: 5px;'>Ver</button>";
            tb += "</td>"
          }
          tb +=  "</tr>";
        }
        $('#tb_not_mapping tbody').html(tb);
      }
    });
  }

var div_show = 'NULL';
  function show_user_agency(element) {
    if(div_show != 'NULL') {
      $(div_show).children('div').remove();
    }
    sistem = $('.select_sistem').val()
    class_id = $(element).attr('class')
    position = $('.' + class_id).parent().position();
    var data = $(element).data('info');
    tb = "<div style='position:absolute;left:" + (position.left + 180) + "px;top:" + position.top + "px;background:white;color:black;padding:10px;width: 90%;border: 1px solid #ddd;'>" +
              "<label class='title agencia_title'></label>" +
              "<table border=1 style='width:100%'>" +
                "<thead>" +
                  "<tr>" +
                    "<th>Id</th>" +
                    "<th>Nombre</th>" +
                    "<th>*</th>" +
                  "</tr>" +
                "</thead>" +
                "<tbody>";


    $.ajax({
      url: 'helpers/ajax/agency_user_mapping.php?get_user_agency="get_user_agency"&ID_AGENCY=' + data.id + '&sistem=' + sistem,
      success: function(res) {
        result = JSON.parse(res);
        for(i = 0; i< result.length; i++) {
          tb += (result[i].ismapping) ? "<tr style='background-color:red;color:white;'>" : "<tr>";
          tb +=   "<td>" + result[i].id +  "</td>" +
                  "<td>" + result[i].nombre +  "</td>";
          tb += (result[i].ismapping) ? "<td></td>" :  "<td style='padding: 4px;'><button data-info='" + JSON.stringify(result[i]) + "' onclick='save_user_Mapping(this)' style='background: #ff3300;border-radius: 0px;color: white;border: 1px solid #ff3300;width: 80px;'>Guardar</button></td>"
          tb +=  "</tr>";
        }
        tb += "</tbody></table></div>"
        $('#conten_user_agency').show();
        $('.' + class_id).parent().append(tb)
        div_show = $('.' + class_id).parent();
        $('.agencia_title').html(data.nombre)
      }
    });
  }

  function save_agency_Mapping (element) {
    value = JSON.stringify($(element).data("info"));
    console.log($(element).data("info"));
    sistem = $('.select_sistem').val()
    $.ajax({
      url: 'helpers/ajax/agency_user_mapping.php',
      dataType: 'json',
      data: {'save_agency_Mapping':'save_agency_Mapping', 'sistem':sistem, 'agency':value}
    }).done(function(res) {
      console.log(res);
    }).fail(function(res) {
      console.log(res);
    });;
  }

  function save_user_Mapping(element) {
    value = JSON.stringify($(element).data("info"));
    console.log(value);
    sistem = $('.select_sistem').val()
    $.ajax({
      url: 'helpers/ajax/agency_user_mapping.php',
      dataType: 'html',
      type: 'GET',
      data: {save_user_Mapping:'save_user_Mapping', sistem:sistem, agency:value},
    }).done(function(res) {
      console.log(res);
    }).fail(function(res) {
      console.log(res);
    });
  }
  $(document).ready(function(){

  });

</script>
