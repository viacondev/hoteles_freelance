<form method="get" id="refine-form">
  <div id="refine-search" class="wbox">
    <div class="title">B&uacute;squeda:</div>
    <div class="form-group col-md-12">
      <label for="destination">Ciudad</label>
      <input type="text" class="form-control" id="destination" name="destination" placeholder="Destino..." value="<?php echo $_GET['destination'] ?>">
      <input type="hidden" id="destination_code" name="destination_code" value="<?php echo $_GET['destination_code'] ?>" />
    </div>
    <div class="form-group col-md-12">
      <label for="typeIn">Lugar de Recogida</label>
      <select class="form-control" id="typeIn" name="typeIn" onchange="changeTypeIn()">
        <option value="T">Terminal</option>
        <option value="H">Hotel</option>
      </select>
      <div id="terminalIn">
        <select class="form-control" id="terminalCodeIn" name="terminalCodeIn"></select>
      </div>

      <div id="hotelIn">
        <select class="form-control" id="hotelCodeIn" name="hotelCodeIn"></select>
      </div>
    </div>

    <div class="form-group col-md-12">
      <label>Lugar de destino</label>
      <select class="form-control" id="typeOut" name="typeOut" onchange="changeTypeOut()">
        <option value="T">Terminal</option>
        <option value="H">Hotel</option>
      </select>

      <div id="terminalOut">
        <select class="form-control" id="terminalCodeOut" name="terminalCodeOut"></select>
      </div>

      <div id="hotelOut">
        <select class="form-control" id="hotelCodeOut" name="hotelCodeOut"></select>
      </div>
    </div>

    <div id="dateTimeIn">
      <div class="form-group col-md-6 col-xs-6">
        <label for="dateIn">Fecha llegada</label>
        <input type="text" class="form-control" id="dateIn" name="dateIn" placeholder="dd/mm/aa" autocomplete="off">
      </div>
      <div class="form-group col-md-6 col-xs-6">
        <div class="row">
          <div class="col-md-12">
            <label>Hora llegada</label>
          </div>
          <div class="col-md-6 col-xs-6" style="padding-left: 0;">
            <select class="form-control" id="hourIn" name="hourIn" style="padding: 0;">
              <option value="">hh</option><option>00</option><option>01</option><option>02</option>
              <option>03</option><option>04</option><option>05</option><option>06</option><option>07</option>
              <option>08</option><option>09</option><option>10</option><option>11</option><option>12</option>
              <option>13</option><option>14</option><option>15</option><option>16</option><option>17</option>
              <option>18</option><option>19</option><option>20</option><option>21</option><option>22</option>
              <option>23</option>
            </select>
          </div>
          <div class="col-md-6 col-xs-6" style="padding-left: 0;">
            <select class="form-control" id="minsIn" name="minsIn" style="padding: 0;">
              <option value="">mm</option><option>00</option><option>05</option><option>10</option>
              <option>15</option><option>20</option><option>25</option><option>30</option><option>35</option>
              <option>40</option><option>45</option><option>50</option><option>55</option>
            </select>
          </div>
        </div>
      </div>
    </div>

    <div id="roudTripDiv" class="form-group col-md-12">
      <input type="checkbox" id="roundTrip" name="roundTrip" value="true" onclick="changeRoundTrip()" checked /> 
      Traslado de entrada y salida
    </div>

    <div id="dateTimeOut">
      <div class="form-group col-md-6 col-xs-6">
        <label for="dateOut">Fecha salida</label>
        <input type="text" class="form-control" id="dateOut" name="dateOut" placeholder="dd/mm/aa" autocomplete="off">
      </div>
      <div class="form-group col-md-6 col-xs-6">
        <div class="row">
          <div class="col-md-12">
            <label>Hora salida</label>
          </div>
          <div class="col-md-6 col-xs-6" style="padding-left: 0;">
            <select class="form-control" id="hourOut" name="hourOut" style="padding: 0;">
              <option value="">hh</option><option>00</option><option>01</option><option>02</option>
              <option>03</option><option>04</option><option>05</option><option>06</option><option>07</option>
              <option>08</option><option>09</option><option>10</option><option>11</option><option>12</option>
              <option>13</option><option>14</option><option>15</option><option>16</option><option>17</option>
              <option>18</option><option>19</option><option>20</option><option>21</option><option>22</option>
              <option>23</option>
            </select>
          </div>
          <div class="col-md-6 col-xs-6" style="padding-left: 0;">
            <select class="form-control" id="minsOut" name="minsOut" style="padding: 0;">
              <option value="">mm</option><option>00</option><option>05</option><option>10</option>
              <option>15</option><option>20</option><option>25</option><option>30</option><option>35</option>
              <option>40</option><option>45</option><option>50</option><option>55</option>
            </select>
          </div>
        </div>
      </div>
    </div>

    <div class="form-group col-md-6 col-xs-6">
      <label>Adultos</label><br />
      <select class="form-control" id="adultCount" name="adultCount">
        <option>1</option><option selected="selected">2</option><option>3</option><option>4</option>
        <option>5</option><option>6</option><option>7</option><option>8</option>
      </select>
    </div>
    <div class="form-group col-md-6 col-xs-6">
      <label>Ni&ntilde;os</label><br />
      <select class="form-control" id="childCount" name="childCount" onchange="showChildAgesConfig()">
        <option>0</option><option>1</option><option>2</option><option>3</option><option>4</option>
      </select>
    </div>

    <div id="child_ages" class="form-group col-md-12"></div>

    <div class="form-group col-md-12 text-right">
      <input type="hidden" name="controller" value="transfer" />
      <input type="hidden" name="action" value="search" />
      <input type="submit" value="Buscar" />
    </div>
  </div>
</form>


<script type="text/javascript">
  destination_code = "<?php echo $_GET['destination_code']; ?>";
  typeIn = "<?php echo $_GET['typeIn']; ?>";
  typeOut = "<?php echo $_GET['typeOut']; ?>";
  terminalCodeIn = "<?php echo $_GET['terminalCodeIn']; ?>";
  hotelCodeIn = "<?php echo $_GET['hotelCodeIn']; ?>";
  terminalCodeOut = "<?php echo $_GET['terminalCodeOut']; ?>";
  hotelCodeOut = "<?php echo $_GET['hotelCodeOut']; ?>";
  dateIn = "<?php echo $_GET['dateIn']; ?>";
  dateOut = "<?php echo $_GET['dateOut']; ?>";
  hourIn = "<?php echo $_GET['hourIn']; ?>";
  hourOut = "<?php echo $_GET['hourOut']; ?>";
  minsIn = "<?php echo $_GET['minsIn']; ?>";
  minsOut = "<?php echo $_GET['minsOut']; ?>";
  roundTrip = "<?php echo isset($_GET['roundTrip']); ?>";
  adultCount = "<?php echo $_GET['adultCount']; ?>";
  childCount = "<?php echo $_GET['childCount']; ?>";
  childAges = [];
  <?php
  for ($i = 1; $i <= $_GET['childCount']; $i++) {
    echo "childAges[$i] = " . $_GET['childAge' . $i] . ";";
  }
  ?>
</script>