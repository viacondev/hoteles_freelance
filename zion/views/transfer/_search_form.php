<div id="search_form">
  <form method="get">
    <fieldset>
      <h2><i class="fa fa-suitcase"></i> Traslados</h2>

      <div id="where" class="form-group col-md-12">
        <label for="destination">En qu&eacute; ciudad quieres trasladarte?</label>
        <input type="text" class="form-control" id="destination" name="destination" placeholder="Introduce tu ciudad...">
        <input type="hidden" id="destination_code" name="destination_code" />
      </div>

      <div id="in">
        <div class="col-md-12">
          <label>Lugar de recogida</label>
        </div>
        <div class="form-group col-md-4">
          <select class="form-control" id="typeIn" name="typeIn" onchange="changeTypeIn()">
            <option value="T">Terminal</option>
            <option value="H">Hotel</option>
          </select>
        </div>

        <div id="terminalIn" class="form-group col-md-8">
          <select class="form-control" id="terminalCodeIn" name="terminalCodeIn"></select>
        </div>

        <div id="hotelIn" class="form-group col-md-8">
          <select class="form-control" id="hotelCodeIn" name="hotelCodeIn"></select>
        </div>
      </div>
      
      <div id="out">
        <div class="col-md-12">
          <label>Lugar de destino</label>
        </div>
        <div class="form-group col-md-4">
          <select class="form-control" id="typeOut" name="typeOut" onchange="changeTypeOut()">
            <option value="T">Terminal</option>
            <option value="H" selected>Hotel</option>
          </select>
        </div>

        <div id="terminalOut" class="form-group col-md-8">
          <select class="form-control" id="terminalCodeOut" name="terminalCodeOut"></select>
        </div>

        <div id="hotelOut" class="form-group col-md-8">
          <select class="form-control" id="hotelCodeOut" name="hotelCodeOut"></select>
        </div>
      </div>

      <div class="col-md-12 small text-muted" style="margin-top: -10px; margin-bottom: 10px;">
        * Terminal incluye Aeropuertos, Puertos y Estaciones de tren.
      </div>

      <div id="dateTimeIn">
        <div class="form-group col-md-4">
          <label for="dateIn">Fecha de llegada</label>
          <input type="text" class="form-control" id="dateIn" name="dateIn" placeholder="dd/mm/aa" autocomplete="off">
        </div>
        <div class="form-group col-md-8">
          <div class="row">
            <div class="col-md-12">
              <label>Hora de llegada <strong>vuelo</strong></label>
            </div>
            <div class="col-md-4 col-xs-6">
              <select class="form-control" name="hourIn">
                <option value="">hh</option><option>00</option><option>01</option><option>02</option>
                <option>03</option><option>04</option><option>05</option><option>06</option><option>07</option>
                <option>08</option><option>09</option><option>10</option><option>11</option><option>12</option>
                <option>13</option><option>14</option><option>15</option><option>16</option><option>17</option>
                <option>18</option><option>19</option><option>20</option><option>21</option><option>22</option>
                <option>23</option>
              </select>
            </div>
            <div class="col-md-4 col-xs-6">
              <select class="form-control" name="minsIn">
                <option value="">mm</option><option>00</option><option>05</option><option>10</option>
                <option>15</option><option>20</option><option>25</option><option>30</option><option>35</option>
                <option>40</option><option>45</option><option>50</option><option>55</option>
              </select>
            </div>
          </div>
        </div>
      </div>

      <div id="roudTripDiv" class="form-group col-md-12">
        <input type="checkbox" id="roundTrip" name="roundTrip" value="true" onclick="changeRoundTrip()" checked /> 
        Traslado de entrada y salida
      </div>

      <div id="dateTimeOut">
        <div class="form-group col-md-4">
          <label for="dateOut">Fecha de salida</label>
          <input type="text" class="form-control" id="dateOut" name="dateOut" placeholder="dd/mm/aa" autocomplete="off">
        </div>
        <div class="form-group col-md-8">
          <div class="row">
            <div class="col-md-12">
              <label>Hora de salida <strong>vuelo</strong></label>
            </div>
            <div class="col-md-4 col-xs-6">
              <select class="form-control" name="hourOut">
                <option value="">hh</option><option>00</option><option>01</option><option>02</option>
                <option>03</option><option>04</option><option>05</option><option>06</option><option>07</option>
                <option>08</option><option>09</option><option>10</option><option>11</option><option>12</option>
                <option>13</option><option>14</option><option>15</option><option>16</option><option>17</option>
                <option>18</option><option>19</option><option>20</option><option>21</option><option>22</option>
                <option>23</option>
              </select>
            </div>
            <div class="col-md-4 col-xs-6">
              <select class="form-control" name="minsOut">
                <option value="">mm</option><option>00</option><option>05</option><option>10</option>
                <option>15</option><option>20</option><option>25</option><option>30</option><option>35</option>
                <option>40</option><option>45</option><option>50</option><option>55</option>
              </select>
            </div>
          </div>
        </div>
      </div>

      <div class="form-group col-md-12">
        <div class="row">
          <div class="col-md-4">
            <label>Adultos</label><br />
            <select class="form-control" id="adultCount" name="adultCount">
              <option>1</option><option selected="selected">2</option><option>3</option><option>4</option>
              <option>5</option><option>6</option><option>7</option><option>8</option>
            </select>
          </div>
          <div class="col-md-4">
            <label>Ni&ntilde;os</label><br />
            <select class="form-control" id="childCount" name="childCount" onchange="showChildAgesConfig()">
              <option>0</option><option>1</option><option>2</option><option>3</option><option>4</option>
            </select>
          </div>
        </div>
      </div>

      <div id="child_ages" class="form-group col-md-12"></div>

      <div class="form-group col-md-12 text-right">
        <input type="hidden" name="controller" value="transfer" />
        <input type="hidden" name="action" value="search" />
        <input type="submit" class="btn btn-info btn-lg" value="Buscar" />
      </div>
    </fieldset>
  </form>
</div>

<script type="text/javascript">
  // Restore values
  $(function() {
    var destination_code = "<?php echo isset($_SESSION['destination_code']) ? $_SESSION['destination_code'] : ''; ?>";
    var destination = "<?php echo isset($_SESSION['destination']) ? $_SESSION['destination'] : ''; ?>";
    var checkin = "<?php echo isset($_SESSION['checkin']) ? $_SESSION['checkin'] : ''; ?>";
    var checkout = "<?php echo isset($_SESSION['checkout']) ? $_SESSION['checkout'] : ''; ?>";
    $('#destination_code').val(destination_code);
    $('#destination').val(destination);
    $('#dateIn').val(checkin);
    $('#dateOut').val(checkout);
    $('#adultCount').val(2);
    $('#childCount').val(0);

    if (destination_code != '') {
      loadTerminals(destination_code);
      loadHotels(destination_code);
    }
  });
</script>