<?php
global $rs, $childAges, $fee_agency;
?>

<div id="search_results">
  <?php
  if ($rs->getTransferType() == 'OUT') {
    $transfers = $rs->getTransfersAvailOut();
  }
  else {
    $transfers = $rs->getTransfersAvailIn();
  }

  $trf_counter = 0;
  foreach ($transfers as $transfer) {
    $trf_counter++;
    $transferType = $rs->getTransferType();
    if ($transferType == 'IN_OUT') {
      $transferOut = $rs->calculateOutTransferForInTransfer($transfer);
      if ($transferOut == NULL) {
        continue;
      }
    }
    ?>
    <div class="transfer_avail">
      <div class="transfer_info col-md-10">
        <div class="row">
          <div class="col-md-4">
            <img src="<?php echo $transfer->getMainImage(); ?>" style="width: 100%" />
          </div>

          <div class="col-md-8">
            <div class="row">
              <div class="col-md-12 title_transfer">
                <h3><?php echo $transfer->getTransferInfoVehicleType() . ' (' . $transfer->getTransferInfoType() . ')' ?></h3>
              </div>
              <div class="col-md-12 desc">
                <span><?php echo substr($transfer->getDescription(), 0, 180); ?></span>
              </div>
            </div>
          </div>

          <div class="col-md-12">
            <table class="table small">
              <thead>
                <tr>
                  <th>&nbsp;</th>
                  <th>Desde</th>
                  <th>Hasta</th>
                  <th>Fecha</th>
                  <th>Hora</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <?php
                  $precio = $transfer->getTotalAmountWithFee();
                  ?>
                  <td><strong><?php echo $transfer->getTransferType() == 'IN' ? 'Llegada: Aeropuerto - Hotel' : 'Salida: Hotel - Aeropuerto'; ?></strong></td>
                  <td><?php echo $transfer->getPickupLocationName(); ?></td>
                  <td><?php echo $transfer->getDestLocationName(); ?></td>
                  <td><?php echo dateToDDMMYY($transfer->getDateFrom()); ?></td>
                  <td><?php echo $transfer->getTimeFrom(); ?></td>
                </tr>
                <?php
                if ($rs->getTransferType() == 'IN_OUT') {
                  $transferOut = $rs->calculateOutTransferForInTransfer($transfer);
                  if ($transferOut != NULL) {
                    $precio += $transferOut->getTotalAmountWithFee();
                    ?>
                    <tr>
                      <td><strong>Salida: Hotel - Aeropuerto</strong></td>
                      <td><?php echo $transferOut->getPickupLocationName(); ?></td>
                      <td><?php echo $transferOut->getDestLocationName(); ?></td>
                      <td><?php echo dateToDDMMYY($transferOut->getDateFrom()); ?></td>
                      <td><?php echo $transferOut->getTimeFrom(); ?></td>
                    </tr>
                    <?php
                  }
                  
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>

      <div class="book col-md-2">
        <div class="provider">
          <?php if ($_SESSION['admin_role']) {
            echo '<i class="fa fa-folder-open-o"></i> ' . BookingProvider::getProviderName($transfer->getProvider());
          } ?>
        </div>
        <div class="price">
          <?php
          $precio   = get_price_only_fees($precio, $fee_agency);
          $decimal  = round($precio - floor($precio), 2) * 100;
          echo floor($precio);
          ?><span class="price_decimal"><?php echo '.' . $decimal; ?></span>
          <span class="currency"><?php echo $transfer->getCurrency(); ?></span>
        </div>
        <div class="book-btn">
          <form method="get" action="index.php">
            <?php
            $serviceAddData = NULL;
            if ($rs->getTransferType() == 'IN') {
              $serviceAddData = getAddServiceTransferDataJSON($rs->getTransferType(), $transfer, NULL, $childAges, $fee_agency);
            }
            else if ($rs->getTransferType() == 'OUT') {
              $serviceAddData = getAddServiceTransferDataJSON($rs->getTransferType(), NULL, $transfer, $childAges, $fee_agency);
            }
            else if ($rs->getTransferType() == 'IN_OUT' && $transferOut != NULL) {
              $serviceAddData = getAddServiceTransferDataJSON($rs->getTransferType(), $transfer, $transferOut, $childAges, $fee_agency);
            }
            ?>
            <input type="hidden" name="service_add_data" value='<?php echo $serviceAddData; ?>' />
            <input type="hidden" name="travel_number_out" id="travel_number_out_<?php echo $trf_counter; ?>">
            <input type="hidden" name="requestCode" value="<?php echo mt_rand(); ?>" />
            <input type="hidden" name="controller" value="shopping_cart" />
            <input type="hidden" name="action" value="show" />
            <!-- <button>RESERVAR <i class="fa fa-angle-right"></i></button> -->

            <button type="button" class="loadingAction" data-toggle="modal" data-target=".tfr_modal_<?php echo $trf_counter; ?>">Reservar</button>
            <div class="modal fade tfr_modal_<?php echo $trf_counter; ?>" tabindex="-1" role="dialog">
              <div class="modal-dialog modal-md" style="text-align: left; color: #555;">
                <div class="modal-content">
                  <div class="modal-body">
                    <?php if ($rs->getTransferType() == 'IN')  { ?>
                      <label>C&oacute;digo de vuelo llegada:</label><br />
                      <input type="text" name="travel_number_in" id="travel_number_in_<?php echo $trf_counter; ?>" placeholder="BA1760">
                    <?php } else if ($rs->getTransferType() == 'OUT') { ?>
                      <label>C&oacute;digo de vuelo llegada:</label><br />
                      <input type="text" name="travel_number_out" id="travel_number_out_<?php echo $trf_counter; ?>" placeholder="BA1760">
                    <?php } else if ($rs->getTransferType() == 'IN_OUT') { ?>
                      <label>C&oacute;digo de vuelo llegada:</label><br />
                      <input type="text" name="travel_number_in" id="travel_number_in_<?php echo $trf_counter; ?>" placeholder="BA1760"><br /><br />
                      <label>C&oacute;digo de vuelo llegada:</label><br />
                      <input type="text" name="travel_number_out" id="travel_number_out_<?php echo $trf_counter; ?>" placeholder="BA1760">
                    <?php } ?>
                    <!-- <input type="submit" class="btn btn-info">Continuar</submit> -->
                  </div>
                  <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
                    <input type="submit" class="btn btn-info" value="Continuar reserva" onclick="if ($('#travel_number_in_<?php echo $trf_counter; ?>').val() == '' && $('#travel_number_out_<?php echo $trf_counter; ?>').val() == '') { alert('Favor ingresar los n&uacute;meros de vuelo'); return false; }">
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <?php
  }
  if (count($transfers) == 0) {
  ?>
    <div class="col-md-12 wbox" style="padding: 30px 20px; margin-bottom: 20px;">
      <span class="glyphicon glyphicon-info-sign" style="font-size: 27px;color:#f89d0e"></span>
      <label style="position: absolute;margin: 3px 6px;font-size: 14px;">Lo sentimos, no se encontraron resultados.</label>
    </div>
  <?php
  }
  ?>
</div>
