<?php
global $rs, $childAges;
?>
<style type="text/css">
/*! normalize.css v2.1.2 | MIT License | git.io/normalize */article,aside,details,figcaption,figure,footer,header,hgroup,main,nav,section,summary{display:block}audio,canvas,video{display:inline-block}audio:not([controls]){display:none;height:0}[hidden]{display:none}html{font-family:sans-serif;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}body{margin:0}a:focus{outline:thin dotted}a:active,a:hover{outline:0}h1{font-size:2em;margin:.67em 0}abbr[title]{border-bottom:1px dotted}b,strong{font-weight:700}dfn{font-style:italic}hr{-moz-box-sizing:content-box;box-sizing:content-box;height:0}mark{background:#ff0;color:#000}code,kbd,pre,samp{font-family:monospace,serif;font-size:1em}pre{white-space:pre-wrap}q{quotes:"\201C" "\201D" "\2018" "\2019"}small{font-size:80%}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}sup{top:-.5em}sub{bottom:-.25em}img{border:0}svg:not(:root){overflow:hidden}figure{margin:0}fieldset{border:1px solid silver;margin:0 2px;padding:.35em .625em .75em}legend{border:0;padding:0}button,input,select,textarea{font-family:inherit;font-size:100%;margin:0}button,input{line-height:normal}button,select{text-transform:none}button,html input[type=button],input[type=reset],input[type=submit]{-webkit-appearance:button;cursor:pointer}button[disabled],html input[disabled]{cursor:default}input[type=checkbox],input[type=radio]{box-sizing:border-box;padding:0}input[type=search]{-webkit-appearance:textfield;-moz-box-sizing:content-box;-webkit-box-sizing:content-box;box-sizing:content-box}input[type=search]::-webkit-search-cancel-button,input[type=search]::-webkit-search-decoration{-webkit-appearance:none}button::-moz-focus-inner,input::-moz-focus-inner{border:0;padding:0}textarea{overflow:auto;vertical-align:top}table{border-collapse:collapse;border-spacing:0}

#logo {
	background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAYlJREFUeNrcV4FtgzAQBJQB3AlCN0gmiNkgnaBhg25QMUHSCUInKJkgZgNGyAhskP6r58qJsCEUE5qXLMvI8t//359NGDhMytWepk3gz7LQ4VzSdAw8WxTc2WYd9ymlymQop5Td87/IQEVDR137AhAahPNlFZWvtpVgRgtmulcA5CO5BmFyQHou8wJjmm3YB0B9TwAp1fKJ5mcap7EB7Mh5/qNKJTv/GAuAQsSHq+/zMaQ4R6RfzGRqpwJrZvWbbwDaGeuEwLc1hmm6HNshS8BMTxG5aDkjxv5sSAAFIo07nsPR7/p0RuSI6vWGcwQ4UQwFQPaQ6FVDp0z/RTRJAMxovj75rbDsqP0pnmyVjYikIawjR1z/TgA5HaYguRXEqO29qCXa1Y6iiVuRZaNr3dQxrrUTfGg+j8wfBtwBJ+hBm8pxBj7hfNsAWpdncZm60grA5EOG/n5H+mIAEx2y8wtAl/XWLhCIiCW5pPECciZ9ROcvPyacuj3Y/Pg6MDqAzLOP3EZAtm8BBgDqc3bEpb6xyQAAAABJRU5ErkJggg==') no-repeat center center transparent;
	background-size: contain;
	width: 25px;
	height: 25px;
	display: inline-block;	
	position: relative;
	bottom: -4px;
	left: -4px;
}

.elem, .elem * {
	box-sizing: border-box;
	margin: 0 !important;	
}
.elem {
	display: inline-block;
	font-size: 0;
	width: 33%;
	border: 20px solid transparent;
	border-bottom: none;
	background: #fff;
	padding: 10px;
	height: auto;
	background-clip: padding-box;
}
.elem > span {
	display: block;
	cursor: pointer;
	height: 0;
	padding-bottom:	70%;
	background-size: cover;	
	background-position: center center;
}
.img-found{
  display:none;    position: absolute;
  top: 0px;
  color: white;
  text-align: center;
  background: #0000008a;
  width: 100%;
  height: 100%;
}
.img-found-litle {    
  position: absolute;
  top: 160px;
  color: white;
  text-align: center;
  background: #0000008a;
  width: 15%;
  height: 15%;
  left:10px;
  border-radius: 5px;
}
</style>
<div id="results">
  <?php
  if(isset($rs->ticketsAvail) && count($rs->ticketsAvail) > 0) {
    foreach ($rs->ticketsAvail as $key => $ticket) {
      ?>
      <div class="ticket_avail item_avail">
        <div class="item_img col-md-3" onMouseOver="$('#img-found-<?php echo $key; ?>').css('display','block')"
          onMouseOut="$('#img-found-<?php echo $key; ?>').css('display','none')">
          <img src="<?php echo $ticket->mainImage; ?>" />
          <span class="img-found-litle" onclick="$('.elem_<?php echo $key; ?>').click()">
            <div style="position: absolute;right: 5px;top: 4px;">
              <i class="fa fa-camera" style="font-size:20px;"></i><br/>
            </div>
          </span>
          <span id="img-found-<?php echo $key; ?>" class="img-found" onclick="$('.elem_<?php echo $key; ?>').click()">
            <div style="position: absolute;top: 50px;right: 70px;">
              <i class="fa fa-camera" style="font-size: 80px;"></i><br/>
              <label style="font-size: 15px;">M&aacute;s Fotos</label>
            </div>
          </span>
        </div>
        <div class="col-md-9 item_info">
          <div class="row">
            <div class="col-md-12">
              <h3><a href="#"><?php echo $ticket->name; ?></a></h3>
            </div>
            <div class="col-md-12">
              <span><?php echo $ticket->classification; ?></span>
            </div>
            <div class="col-md-12 desc" style="overflow-y: scroll;max-width: 100%;max-height: 129px;">
              <span><?php echo $ticket->description; ?></span>
            </div>
          </div>
        </div>
        <div class="content" id="contentImageModal_<?php echo $key?>" style="display:none">
          <?php foreach ($ticket->imageList as $img) { ?>
                  <a class="elem_<?php echo $key?>" href="<?php echo $img; ?>">
                    <span style="background-image: url(<?php echo $img; ?>);"></span>
                  </a>
          <?php } ?>
        </div>
        <div class="col-md-12 prices-table small">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>Modalidad</th>
                <!--<th>Precio/persona</th>-->
                <th>Fecha</th>
                <th>Días</th>
                <th>Precio total</th>
                <th>&nbsp;</th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach ($ticket->availableModalityList as $availMode) {
                $ticket_avail_code = $ticket->code . '_' . $availMode->code;
                $ticket_avail_code = str_replace('#', '_', $ticket_avail_code);
                // Check prices
                $total_price = 0;
                foreach ($availMode->priceList as $price) {
                  // If we want to show these prices we should add the extra_fee first
                  /*if ($price->getDescription() == 'ADULT PRICE') {
                    $adult_price = $price->getAmount();
                  }
                  else if ($price->getDescription() == 'CHILD PRICE') {
                    $child_price = $price->getAmount();
                  }
                  if ($price->getDescription() == 'INFANT PRICE') {
                    $infant_price = $price->getAmount();
                  }
                  else*/
                  if ($price->description == 'SERVICE PRICE') {
                    $total_price = $price->amountWithFee;
                  }
                }
                ?>
                <tr>
                  <form method="get" action="index.php">
                    <td><?php echo $availMode->name; ?></td>
                    <!--<td><?php echo '' ?></td>-->
                    <td>
                      <select name="dateFrom" onchange="updateDuration(this, '<?php echo $ticket_avail_code ?>')">
                        <?php
                        $operations = $availMode->operationDateList;
                        foreach ($operations as $operationDate) {
                          echo '<option value="' . $operationDate->date . '" data-minduration="'. $operationDate->minimumDuration . '" data-maxduration="' . $operationDate->maximumDuration . '">';
                          echo   dateToDDMMYY($operationDate->date);
                          echo '</option>';
                        }
                        ?>
                      </select>
                    </td>
                    <td><span id="days_txt_<?php echo $ticket_avail_code; ?>"><?php echo $operations[0]->maximumDuration; ?></span></td>
                    <td><?php echo $total_price . ' ' . $ticket->currency; ?></td>
                    <td>
                      <input type="hidden" name="service_add_data" value='<?php echo getTicketValuationDataJSON($ticket, $availMode, $childAges) ?>' />
                      <input type="hidden" name="days" id="days_<?php echo $ticket_avail_code; ?>" value="<?php echo $operations[0]->maximumDuration; ?>" />
                      <input type="hidden" name="requestCode" value="<?php echo mt_rand(); ?>" />
                      <input type="hidden" name="controller" value="shopping_cart" />
                      <input type="hidden" name="action" value="show" />
                      <input type="submit" value="Reservar" class="loadingAction" />
                    </td>
                  </form>
                </tr>
                <?php
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
      <script>
        $(document).ready(function(e) {
            // live handler
            lc_lightbox('.elem_<?php echo $key ?>', {
                wrap_class: 'lcl_fade_oc',
                gallery : true,	
                thumb_attr: 'data-lcl-thumb', 
                autoplay    :false,
                skin: 'minimal',
                radius: 0,
                padding	: 0,
                border_w: 0,
            });	
        });
    </script>
      <?php
    }
  } else {
    ?>
    <div class="col-md-12 wbox" style="padding: 30px 20px; margin-bottom: 20px;">
			<span class="glyphicon glyphicon-info-sign" style="font-size: 27px;color:#f89d0e"></span>
			<label style="position: absolute;margin: 3px 6px;font-size: 14px;">Lo sentimos, no se encontraron resultados.</label>
		</div>
  <?php
  }
  ?>
</div>

<div id="pagination" class="col-md-12 wbox text-center"></div>

<script type="text/javascript">
  totalPages = 0<?php //echo $rs->getTotalPages(); ?>;
  currentPage = 0<?php //echo $rs->getCurrentPage(); ?>;
</script>
