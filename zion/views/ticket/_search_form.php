<div id="search_form">
  <form method="get">
    <fieldset>
      <h2><i class="fa fa-ticket"></i> Buscar Entradas y Excursiones</h2>

      <div id="where" class="form-group col-md-12">
        <label for="destination">D&oacute;nde quieres ir?</label>
        <input type="text" class="form-control" id="destination" name="destination" placeholder="Introduce tu destino...">
        <input type="hidden" id="destination_code" name="destination_code" />
      </div>

      <div class="form-group col-md-5">
        <label for="dateFrom">Fecha de entrada</label>
        <input type="text" class="form-control" id="dateFrom" name="dateFrom" placeholder="dd/mm/aa" autocomplete="off">
      </div>

      <div class="form-group col-md-5">
        <label for="dateTo">Fecha de salida</label>
        <input type="text" class="form-control" id="dateTo" name="dateTo" placeholder="dd/mm/aa" autocomplete="off">
      </div>

      <div id="nights_count" class="form-group col-md-2"></div>

      <div class="form-group col-md-9">
        <div class="row">
          <div class="col-md-4">
            <label>Adultos</label><br />
            <select class="form-control" id="adultCount" name="adultCount">
              <option>1</option><option selected="selected">2</option><option>3</option><option>4</option>
              <option>5</option><option>6</option><option>7</option><option>8</option>
            </select>
          </div>
          <div class="col-md-4">
            <label>Ni&ntilde;os</label><br />
            <select class="form-control" id="childCount" name="childCount" onchange="showChildAgesConfig()">
              <option>0</option><option>1</option><option>2</option><option>3</option><option>4</option>
            </select>
          </div>
        </div>
      </div>

      <div id="child_ages" class="form-group col-md-12"></div>

      <div class="form-group col-md-12 text-right">
        <input type="hidden" id="orderPrice" name="orderPrice" value="">
        <input type="hidden" id="page" name="page" value="1">
        <input type="hidden" name="controller" value="ticket" />
        <input type="hidden" name="action" value="search" />
        <input type="submit" class="btn btn-info btn-lg loadingAction" value="Buscar" />
      </div>
    </fieldset>
  </form>
</div>

<script type="text/javascript">
  // Restore values
  $(function() {
    var destination_code = "<?php echo isset($_SESSION['destination_code']) ? $_SESSION['destination_code'] : ''; ?>";
    var destination = "<?php echo isset($_SESSION['destination']) ? $_SESSION['destination'] : ''; ?>";
    var checkin = "<?php echo isset($_SESSION['checkin']) ? $_SESSION['checkin'] : ''; ?>";
    var checkout = "<?php echo isset($_SESSION['checkout']) ? $_SESSION['checkout'] : ''; ?>";
    $('#destination_code').val(destination_code);
    $('#destination').val(destination);
    $('#dateFrom').val(checkin);
    $('#dateTo').val(checkout);
    $('#adultCount').val(2);
    $('#childCount').val(0);
  });
</script>