<div id="ticket" class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        
        <div class="call-action call-action-boxed call-action-style2 clearfix">
          	<div class="row">
              	<div class="col-sm-9">
					<h2 class="primary">
						<strong class="origen_main"></strong> 
						<?php echo $_GET['destination']; ?> - 
						<span class="ida_main">
						<?php echo $_GET['dateFrom']; ?></span> al 
						<span class="retorno_main"><?php echo $_GET['dateTo']; ?></span>
					</h2>
					<p>
						Ocupaci&oacute;n :
						<span class="ocupacion_main">
							<?php
								$adultCount = $_GET['adultCount'];
								$childCount = $_GET['childCount'];
								echo $adultCount . ' adultos';
								if ($childCount > 0) {
									echo ' - ' . $childCount . ' ni&ntilde;os';
								}
							?>
						</span>
					</p>
              	</div>
				<div class="col-sm-3">
					<div class="button-side" style="margin-top:8px;">
						<a href="#" class="btn-system btn-small" onclick="$('#modal-nueva-busqueda').modal('show');"><i class="fa fa-search"></i> Modificar B&uacute;squeda</a>
					</div>
				</div>
          	</div>
      	</div>

      </div>
	  <div class="mini-hidden-separator"></div>
      <div class="col-md-3"><?php include('_search_options.php'); ?></div>
      <div class="col-md-9"><?php include('_search_results.php'); ?></div>
    </div>
  </div>
</div>