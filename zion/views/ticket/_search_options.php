<form method="get" id="refine-form">
  <input type="hidden" id="orderPrice" name="orderPrice" value="<?php echo $_GET['orderPrice']; ?>">
  <input type="hidden" id="page" name="page" value="<?php echo $_GET['page']; ?>">
  <div id="refine-search" class="wbox" style="padding: 16px 10px;">
    
    <div class="form-group col-md-12">
      <label for="destination">Destino</label>
      <input type="text" class="form-control" id="destination" name="destination" placeholder="Destino..." value="<?php echo $_GET['destination'] ?>">
      <input type="hidden" id="destination_code" name="destination_code" value="<?php echo $_GET['destination_code'] ?>" />
    </div>
    <div class="form-group col-md-6 col-xs-6">
      <label for="dateFrom">Entrada</label>
      <input type="text" class="form-control" id="dateFrom" name="dateFrom" placeholder="dd/mm/aa" autocomplete="off" value="<?php echo $_GET['dateFrom'] ?>">
    </div>
    <div class="form-group col-md-6 col-xs-6">
      <label for="dateTo">Salida</label>        
      <input type="text" class="form-control" id="dateTo" name="dateTo" placeholder="dd/mm/aa" autocomplete="off" value="<?php echo $_GET['dateTo'] ?>">
    </div>

    <div class="col-md-5 col-xs-6">
      <label>Adultos</label>
    </div>
    <div class="col-md-5 col-xs-6">
      <label>Ni&ntilde;os</label>
    </div>
    <div class="form-group col-md-5 col-xs-6">
      <select class="form-control" id="adultCount" name="adultCount">
        <option>1</option><option selected="selected">2</option><option>3</option><option>4</option>
        <option>5</option><option>6</option><option>7</option><option>8</option>
      </select>
    </div>
    <div class="form-group col-md-5 col-xs-6">
      <select class="form-control" id="childCount" name="childCount" onchange="showChildAgesConfig()">
        <option>0</option><option>1</option><option>2</option><option>3</option><option>4</option>
      </select>
    </div>

    <div id="child_ages" class="form-group col-md-12"></div>

    <div class="form-group col-md-12 text-right">
      <input type="hidden" name="controller" value="ticket" />
      <input type="hidden" name="action" value="search" />
      <input type="submit" value="Buscar" />
    </div>
  </div>
</form>


<script type="text/javascript">
  dateFrom = "<?php echo $_GET['dateFrom']; ?>";
  dateTo = "<?php echo $_GET['dateTo']; ?>";
  adultCount = "<?php echo $_GET['adultCount']; ?>";
  childCount = "<?php echo $_GET['childCount']; ?>";
  childAges = [];
  <?php
  for ($i = 1; $i <= $_GET['childCount']; $i++) {
    echo "childAges[$i] = " . $_GET['childAge' . $i] . ";";
  }
  ?>
  orderPrice = "<?php echo isset($_GET['orderPrice']) ? $_GET['orderPrice'] : '' ?>";
</script>