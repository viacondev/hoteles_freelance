<?php
$hotelAvail = $service->serviceInfo;
$SupplementList = $service->supplementList;
$Discount = $hotelAvail->discountList;
$rooms = $hotelAvail->roomsAvail;

?>
<div class="col-md-12 service hotel-service wbox">
  <div class="service-type service-type-hotel"><i class="fa fa-home"></i> Alojamiento</div>
  <div class="service-info">
    <span class="service-title"><?php echo $hotelAvail->name ?></span> &nbsp;
    <span class="service-category" data-toggle="tooltip" data-placement="top" data-original-title="<?php echo $hotelAvail->category; ?>">
      <?php showCategory($hotelAvail->categoryCode); ?>
    </span>
    <?php if ($_SESSION['admin_role']) {
      echo '<span class="text-muted small">(' . BookingProvider::getProviderName($hotelAvail->provider) . ')</span>';
    } ?>
    <div class="service-dates">
      Entrada: <strong><?php echo dateToDDMMYY($service->dateFrom); ?></strong> -
      Salida: <strong><?php echo dateToDDMMYY($service->dateTo); ?></strong>
    </div>
    <div class="rooms-info">
      <table class="table">
        <thead>
          <tr>
            <th>Tipo de habitaci&oacute;n</th>
            <th>R&eacute;gimen</th>
            <th>Ocupaci&oacute;n</th>
            <th class="text-center">Precio</th>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach ($rooms as $room) {
          ?>
            <tr>
              <td><?php echo $room->roomType . ' x ' . $room->roomCount; ?></td>
              <td><?php echo $room->board ?></td>
              <td>
                <?php
                $adult_str = $room->adultCount > 1 ? ' adultos' : ' adulto';
                echo $room->adultCount . $adult_str;
                if ($room->childCount > 0) {
                  $child_str = $room->childCount > 1 ? ' ni&ntilde;os' : ' ni&ntilde;o';
                  echo ' ' . $room->childCount . $child_str;
                }
                ?>
              </td>
              <td class="text-right"><strong><?php echo $room->priceWithFee . ' ' . $hotelAvail->currency; ?></strong></td>
            </tr>
          <?php
          }

          ?>
        </tbody>
      </table>
      <?php if (!empty($SupplementList)) { ?>
        <table class="table">
          <thead>
            <tr>
              <th>Suplementos</th>
              <th class="text-right"></th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach ($SupplementList as $value) {
            ?>
              <tr>
                <td><?php echo $value->description; ?></td>
                <td class="text-right"><strong><?php echo $value->amountWithFee . '  ' . $hotelAvail->currency; ?></strong></td>
              </tr>
            <?php
            }
            ?>
          </tbody>
        </table>
      <?php  }
      if (!empty($Discount)) {
      ?>
        <table class="table">
          <thead>
            <tr>
              <th>Descuentos</th>
              <th class="text-center"></th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($Discount as $value) { ?>
              <tr>
                <td><?php echo $value->description ?></td>
                <td class="text-right"><strong><?php echo "-" . $value->amountWithFee . '  ' . $hotelAvail->currency; ?></strong></td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      <?php  } ?>
    </div>
  </div>
  <hr />

  <div class="contract-info">
    <?php
    if ($hotelAvail->contractComment) {
      echo '<div><strong>Observaciones del contrato</strong></div>';
      echo '<p>' . $hotelAvail->contractComment . '</p>';
      echo '<hr />';
    }
    ?>
  </div>

  <div class="policies">
    <strong>Politicas de Cancelaci&oacute;n</strong>
    <?php
    $sw = false;
    foreach ($rooms as $room) {
      if (count($room->cancellationPolicies) == 0 && $exists_policy_cancellation) {
        $exists_policy_cancellation = false;
      }
      echo '<div class="policy">';
      foreach ($room->cancellationPolicies as $policy) {
        if ($policy->amount == 0 && $policy->description != '') {
          echo '<div class="text-danger"><strong>' . $policy->description . '</strong></div>';
          continue;
        }
        $amount = $policy->amountWithFee;
        echo '<div>' . $room->roomType . ' x ' . $room->roomCount . '</div>';
        $diff = get_diff($policy->dateFrom);
        $Options = get_options($diff, $Options);
        $message = get_message_show($diff, $policy->time, $policy->dateFrom, $amount, $hotelAvail->currency);
        if ($diff >= -3 && $diff <= 0) {
          $mayor = $diff;
        }
        echo '<div class="text-danger"><strong>' . $message . '</strong></div>';
        if ($Options == 'expired_date' && $sw) {
          $msgLastConfirm = $msg;
          $sw = true;
        }
      }
      if (count($room->cancellationPolicies) == 0) {
        echo "<div class='text-danger'><strong>Lo siento, no puede continuar con esta reserva debido a que tiene gastos de cancelaci&oacute;n desde el momento de realizarla.<br>";
        echo "Favor contactarse con BoliviaBooking para proceder con la reserva.<br>";
        echo "Telefono(Fijo):+591 - 3367135 - Emergencia: +591 - 72122266<strong></div>";
      }
      echo '</div>';
    }
    // if(!$exists_policy_cancellation) {
    //   echo "<h3>No existen politicas de cancelaciones</h3>";
    // }
    ?>
  </div>
  <hr />
  <div class="total-price text-right">
    Precio Total:
    <span class="total-price-amount"><?php echo $service->totalAmountWithFee . ' ' . $hotelAvail->currency; ?></span>
  </div>

  <div class="remove-service text-right small">
    <!-- <div class="link" onclick="removeService('<?php //echo $service->scartId; 
                                                    ?>', '<?php //echo $service->getSPUI(); 
                                                          ?>')">
      Eliminar Producto
    </div> -->
  </div>
</div>