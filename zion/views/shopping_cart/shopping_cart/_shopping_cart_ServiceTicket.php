<?php
$serviceInfo = $service->serviceInfo;
?>
<div class="col-md-12 service hotel-service wbox">
	<div class="hidden-separator"></div>
	<div class="col-md-4">
		<img src="<?php echo $serviceInfo->mainImage; ?>" style="width: 100%;"/>
	</div>
  	<div class="col-md-8">
    	<div class="service-type service-type-ticket"><i class="fa fa-ticket"></i> Entrada</div>
		
		<div class="service-info">
			<span class="service-title"><?php echo $serviceInfo->name ?></span> &nbsp;
			<div class="service-dates">
				<?php $dateSelected = isset($serviceInfo->availableModalityList[0]->OperationDateList)?$serviceInfo->availableModalityList[0]->OperationDateList[0]->date:'0000-00-00'; ?>
				Entrada: <strong><?php echo dateToDDMMYY($dateSelected); ?></strong>
			</div>
			<div class="service-dates">
				Destino: <strong><?php echo $serviceInfo->destinationType; ?></strong>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12"><strong>Entradas:</strong></div>
			<div class="col-md-9 col-xs-9">
				<?php
				echo $serviceInfo->adultCount . ' adultos';
				if ($serviceInfo->childCount != 0) {
				echo ', ' . $serviceInfo->childCount . ' ni&ntilde;os';
				}
				echo ' (' . $serviceInfo->availableModalityList[0]->name. ')' ;
				?>
			</div>
			<div class="col-md-3 col-xs-3 text-right">
				<?php
					$availMode = $serviceInfo->availableModalityList;
					$availMode = $availMode[0];
					foreach ($availMode->priceList as $price) {
						if ($price->description == 'SERVICE PRICE' || $price->description == 'Total Price') {
							echo '<strong>' . $price->amountWithFee . ' ' . $serviceInfo->currency . '</strong>';
							break;
						}
					}
				?>
			</div>
		</div>
  	</div>
  	<hr />
	<div class="hidden-separator"></div>
	<div class="col-md-12 contract-info">
		<?php
		if (isset($serviceInfo->description)) {
			echo '<div><strong>Detalles</strong></div>';
			echo '<p>' . $serviceInfo->description . '</p>';
			echo '<hr />';
		}
		?>
	</div>
	
	<div class="col-md-12 contract-info">
		<?php
		if (isset($serviceInfo->contractComment)) {
			echo '<div><strong>Observaciones del contrato</strong></div>';
			echo '<p>' . $serviceInfo->contractComment . '</p>';
			echo '<hr />';
		}
		?>
	</div>

  <div class="service-detail confirm_purchase row">
    <?php
    if (count($serviceInfo->serviceDetailList) > 0) {
      echo '<div class="col-md-12"><strong>Informaci&oacute;n requerida</strong></div>';
    }
    $serviceDetailCount = 0;
    foreach ($serviceInfo->serviceDetailList as $serviceDetail) {
      $serviceDetailCount++;
      ?>
      <div class="col-md-12" style="margin-bottom: 15px;">
        <?php echo $serviceDetail->name; ?>
        <div style="margin-left: 20px;">
          - &nbsp; <input type="text" name="serviceDetail_<?php echo $service_count . '_' . $serviceDetailCount ?>" />
          <input type="hidden" name="serviceDetailName_<?php echo $service_count . '_' . $serviceDetailCount ?>" value="<?php echo $serviceDetail->name; ?>" />
          <input type="hidden" name="serviceDetailCode_<?php echo $service_count . '_' . $serviceDetailCount ?>" value="<?php echo $serviceDetail->code; ?>" />
        </div>
      </div>
      <?php
    }
    ?>
    <input type="hidden" name="serviceDetailCount_<?php echo $service_count; ?>" value="<?php echo $serviceDetailCount; ?>" />
  </div>

  <div class="policies">
    <?php
    $sw = false;
    // __logarr($serviceInfo);
    if (count($serviceInfo->cancellationPolicies) == 0) {
      $exists_policy_cancellation = false;
      $Options = 'expired_date';
    }
    foreach ($serviceInfo->cancellationPolicies as $policy) {
		$amount = $policy->amountWithFee;
      	$diff     = get_diff($policy->dateFrom);
      	$Options  = get_options($diff, $Options);
      	$message  = get_message_show($diff, $policy->time, $policy->dateFrom, $amount, $serviceInfo->currency);
      	// if ( $diff >= -3 && $diff <=0 ) { $mayor = $diff; }
    	// echo '<div class="text-danger"><strong>' . $policy->messagePolicy . '</strong></div>';
		echo '<div class="text-danger"><strong>' . $message . '</strong></div>';
    }
    ?>
  </div>
  <hr />

  <div class="total-price text-right">
    Total Precio Neto:
    <span class="total-price-amount"><?php echo $service->totalAmountWithFee . ' ' . $serviceInfo->currency; ?></span>
  </div>

  <div class="remove-service text-right small">
    <!-- <div class="link" onclick="removeService('<? //echo $service->scartId; ?>', '<?php //echo $service->getSPUI(); ?>')">
      Eliminar Producto
    </div> -->
  </div>
</div>
