<?php global $services, $book_params; ?>
<style>
    .btn_Errors{
        border-color: blue;
        color: blue;
    }
    .btn_Errors:hover{
        background-color: blue;
        color: white;
    }
</style>
<div id="error-extra-info-modal" class="modal fade" style ="z-index: 999999;" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header" style="background-color: blue;">
                <h4 class="modal-title" style="color:white;font-weight:bold;">¡Alerta!</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-1">
                        <span class="fa fa-exclamation-triangle"
                            style="font-size: 51px;color:blue;position: absolute;margin-top:7px"></span>
                    </div>
                    <div class="col-md-11" style="padding-left: 32px;">
                        <span style="font-size: 14px;">
                            Lo sentimos es posible que la tarifa ya no este disponible.<br />
                            Por favor intente con una diferente reserva.<br />
                        </span>
                    </div>
                </div>


                <div class="msg-errors"></div>
            </div>
            <div class="modal-footer">
                <form method="post" action="https://app.boliviabooking.com/boliviabooking/public/aereo">
                    <button class="btn_Errors" style="width: 120px;font-size: 16px;">Aceptar</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div id="shopping_cart_services" class="content">
  <div class="container">
    <div class="col-md-12 box">
      <div id="shopping_cart_title" class="col-md-12 text-info wbox">
        <i class="fa fa-info-circle fa-1x"></i> Informaci&oacute;n y Politica de Cancelaciones
      </div>
      <div class="row">
        <div class="col-md-8">
          <!-- Show services -->
          <?php
          $Options = "";
          $mayor = -4;
          if (isset($services[0]->errors) && count($services[0]->errors) == 0 || property_exists($services[0], 'getErrors') && count($services[0]->getErrors()) == 0) {
       
            $total_services = count($services);
            $service_count = 0;
            $total_price = 0;
            $arr_service    = ['ServiceHotel','ServiceTicket'];
            $currency       = isset($services[0]->serviceType) && in_array($services[0]->serviceType, $arr_service) ? $services[0]->currency : $services[0]->getCurrency();
            $msgLastConfirm = '';
            $exists_policy_cancellation = true;
            foreach ($services as $service) {
              $service_count++;
              $total_price += isset($services[0]->serviceType) && in_array($services[0]->serviceType, $arr_service) ? $service->totalAmountWithFee : Agency::getUserPrice($service->getTotalAmount());
              include('shopping_cart/_shopping_cart_' . $service->serviceType . '.php');
            }
            ?>
            <div id="reservation_actions" class="col-md-12 text-right wbox">
              <div id="purchase_total_amount">
                Valoraci&oacute;n total de la reserva:
                <span class="amount"><?php echo $total_price . ' ' . $currency; ?></span>
              </div>
              <div>
                <!-- <a href="?controller=shopping_cart&amp;action=show" onclick="return confirm('Estas seguro que deseas vaciar el carrito?');">Vaciar carrito</a> -->
                <button class="btn btn-info-css"  data-toggle="modal" data-target="#pax-info-modal">
                  Continuar con la reserva
                </button>

                        </div>
                    </div>
                    <?php
          } else {
            ?>
            <script type='text/javascript'>
                console.log(<?php echo json_encode($services[0]->errors); ?>);
            $('#error-extra-info-modal').modal();
            $('.btn_Errors').click(function() {
                window.close();
            });
            </script>
            <?php
          }
          ?>
                </div>

                <div id="services_suggestions" class="col-md-4">
                    <!-- <div class="title">A&ntilde;adir nuevos servicios</div> -->
                    <!-- <div class="col-md-7 add_new_service"><a href="?controller=accommodation&amp;action=home"><i class="fa fa-home"></i> A&ntilde;adir Alojamiento</a></div> -->
                    <!-- <div class="col-md-7 add_new_service"><a href="?controller=ticket&amp;action=home"><i class="fa fa-ticket"></i> A&ntilde;adir Excursiones</a></div> -->
                    <!-- <div class="col-md-7 add_new_service"><a href="?controller=transfer&amp;action=home"><i class="fa fa-suitcase"></i> A&ntilde;adir Traslados</a></div> -->
                </div>
            </div>
        </div>
    </div>
</div>

<form id="service_remove_form" method="post" action="?controller=shopping_cart&amp;action=remove"
    class="hidden invisible">
    <input type="hidden" id="remove_scartId" name="scartId">
    <input type="hidden" id="remove_SPUI" name="SPUI">
    <input type="hidden" name="requestCode" value="<?php echo mt_rand(); ?>" />
</form>

<form id="confirm_purchase_form" method="get">
    <!-- Modal -->
    <div id="pax-info-modal" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Datos Pasajeros </h4>
                </div>
                <div class="modal-body">
                    <div id="titular_reserva">
                        <input type="radio" name="include_all_pax" checked onclick="$('.secondary_pax').hide()" />
                        Incluir solo los datos del pasajero principal y/o requeridos<br />
                        <input type="radio" name="include_all_pax" onclick="$('.secondary_pax').show()" /> Incluir los
                        datos de todos los pasajeros
                        <hr />
                        <?php
            $service_count = 0;
            $requiresCustomerExtraInfo = false;
            if (isset($services[0]->errors) && count($services[0]->errors) == 0 || count($services) > 0) {
              foreach ($services as $service) {
                $service_count++;
                $serviceType = isset($services[0]->serviceType) && in_array($services[0]->serviceType, $arr_service) ? $service->serviceType : $service->getServiceType();
                $serviceScartId = isset($service->scartId) ? $service->scartId : $service->getScartId();
                include('pax_info/_pax_info_' . $serviceType . '.php');
                echo '<input type="hidden" name="serviceType_' . $service_count . '" value="' . $serviceType . '" />';
                echo '<input type="hidden" name="scartId_' . $service_count . '" value="' . $service->scartId . '" />';
              }
              ?>
              <input type="hidden" name="servicesCount" value="<?php echo $service_count; ?>" />
              <input type="hidden" name="requestCode" value="<?php echo mt_rand(); ?>" />
              <input type="hidden" name="controller" value="booking" />
              <input type="hidden" name="action" value="purchase" />
              <input type="hidden" name="current_user" value="<?php echo $GLOBALS['current_user']->name; ?>" />
              <input type="hidden" name="agency_name" value="<?php echo $_SESSION['agency_name']; ?>" />
              <input type="hidden" name="userId" value="<?php echo $GLOBALS['current_user']->id; ?>" />
              <input type="hidden" name="book_params" value="<?php echo htmlspecialchars(json_encode($GLOBALS['book_params']), ENT_QUOTES, 'UTF-8'); ?>" />
              <?php
            }
            ?>
          </div>
          <!-- DESDE ACA  -->
          <div class="alert alert-danger" id="divdanger">
              <h4><i class="fa fa-warning"></i> <span class="text-danger">Atenci&oacute;n!</span></h4>
            <?php
            if ($Options == 'expired_date') {
              $msgLastConfirm .= "No puede continuar con esta reserva debido a que tiene gastos de cancelaci&oacute;n desde el momento en la que se realiza.<br>";
              $msgLastConfirm .= "Favor contactarse con Barry para proceder con la reserva.<br>";
              $msgLastConfirm .= "Telefono(Fijo):+591 - 3367135 - Emergencia: +591 - 72122266";
            }
            // if ($GLOBALS['current_agency']->booking == 'BOOK' && $Options == 'expired_date') {
            //   $message .= "No puede continuar con esta reserva debido a que tiene gastos de cancelaci&oacute;n desde el momento en la que se realiza.<br>";
            //   $message .= "Favor contactarse con barry para proceder con la reserva.<br>";
            // }
            // else if ($GLOBALS['current_agency']->booking == 'EMIT' && $Options == 'expired_date') {
            //   $message .= "La reserva tiene gastos de cancelacion desde el momento en que se realiza.<br>";
            //   $message .= "En caso de continuar y cancelar la reserva asumir&aacute los gastos de cancelaci&oacute;n.<br>";
            // }
            //else if ($Options != 'expired_date') { $message .= "EMMIT<br>"; }
            /*if ($Options == 'now_expired' && $Options != 'expired_date') {
                $message .= "Tienes Hasta hoy para Confirmar tus Reservas Pendiente<br>";
              } else */
              if($mayor >= -3 && $mayor <= 0 && $Options != 'expired_date') {
                $msgLastConfirm .= "Esta reserva entrara en gastos en los proximos dias.<br>";
                $msgLastConfirm .= "Si no es confirmada ser&aacute cancelada autom&aacute;ticamente.<br>";
              }
              $msgLastConfirm .= "</strong>";
              echo $msgLastConfirm;
            ?>
          </div>
          <div>
            <input type="checkbox" id="agreement_check" /> He le&iacute;do y acepto las pol&iacute;ticas de cancelaciones de la reserva.
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
          <?php $can_book_cancellation = array(); ?>
          <?php if ($Options != "expired_date" || $GLOBALS['current_agency']->id == 1 || array_key_exists($_SESSION['current_user']->id, $can_book_cancellation)) { ?>
              <?php if ($exists_policy_cancellation || array_key_exists($_SESSION['current_user']->id, $can_book_cancellation)) { ?>
                    <button class="btn btn-info-css" id="btn_continue" onclick="return confirmPurchase();">Realizar Reserva</button>
              <?php } ?>
          <?php } ?>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <?php if ($requiresCustomerExtraInfo) { ?>
    <!-- Modal Extra Info for Main Pax -->
    <div id="holder-extra-info-modal" class="modal fade">
      <div class="modal-dialog modal-md">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Datos Pasajero Principal</h4>
          </div>
          <div class="modal-body">
            <h4>Datos del pasajero <span id="mainPaxName" class="strong"></span></h4>
            <div class="small text-muted">Para poder confirmar la reserva los siguientes datos son requeridos:</div>
            <table>
              <tr>
                <td>Tel&eacute;fono:</td>
                <td style="padding: 5px;"><input class="required_pax" name="holderPhone" required></td>
              </tr>
              <tr>
                <td>Direcci&oacute;n:</td>
                <td style="padding: 5px;"><input class="required_pax" name="holderAddress" required></td>
              </tr>
              <tr>
                <td>Cod. Postal:</td>
                <td style="padding: 5px;"><input class="required_pax" name="holderPostalCode" required></td>
              </tr>
              <tr>
                <td>Ciudad:</td>
                <td style="padding: 5px;"><input class="required_pax" name="holderCity" value="Santa Cruz" required></td>
              </tr>
              <tr>
                <td>Pa&iacute;s:</td>
                <td style="padding: 5px;"><?php include('_country_list.php'); ?></td>
              </tr>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button class="btn btn-info-css" onclick="return confirmPurchasePaxEtraInfo()">Realizar Reserva</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <?php } ?>
</form>
<!-- Modal de Error -->
<div id="error-extra-info-modal" class="modal fade" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header" style="background-color: blue;">
                <h4 class="modal-title" style="color:white;font-weight:bold;">¡Alerta!</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-1">
                        <span class="fa fa-exclamation-triangle"
                            style="font-size: 51px;color:blue;position: absolute;margin-top:7px"></span>
                    </div>
                    <div class="col-md-11" style="padding-left: 32px;">
                        <span style="font-size: 14px;">
                            Lo sentimos es posible que la tarifa ya no este disponible.<br />
                            Por favor intente con una diferente reserva.<br />
                        </span>
                    </div>
                </div>


        <div class="msg-errors"></div>
      </div>
      <div class="modal-footer">
        <form method="post" action="http://yocounter.com/freelance/public/aereo">
          <button class="btn_Errors" style="width: 120px;font-size: 16px;">Aceptar</button>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
<?php if (count($services) == 0 || isset($services[0]->errors) && count($services[0]->errors) != 0) { ?>
$('#error-extra-info-modal').modal();
$('.btn_Errors').click(function() {
    window.close();
});
<?php } ?>

function confirmPurchase() {
    if (checkAgreement()) {
      <?php if ($requiresCustomerExtraInfo) { ?>
        var name = $('input[name="customerName_1_1"]').val() + ' ' + $('input[name="customerLastName_1_1"]').val();
        $('#mainPaxName').html(name)
        $('#holder-extra-info-modal').modal();
        return false;
      <?php } else { ?>
        if (checkValidForm()) {
          showProgress();
        }
        return true;
      <?php } ?>
    }
    return false;
  }

  function confirmPurchasePaxEtraInfo() {
    if (checkValidForm()) {
      showProgress();
    }
    else {
      return false;
    }
    return true;
  }

  $('#confirm_purchase_form').submit(function() {
    var form = $(this);
    $.ajax({
        url         : 'rest_from_js.php',
        method      : 'POST',
        data        : form.serialize(),
        success     : function(data) {
          console.log(data)
          book = JSON.parse(data);
          if(book.hasOwnProperty('errors')) {
            console.log(book.hasOwnProperty('errors'));
            hideProgress();
            $('#error-extra-info-modal').modal();
                $('.btn_Errors').click(function() {
                    window.close();
                });
          }
          else {
            window.location.href = getLinkGlobal() + "/zion/mia.php?controller=booking&action=show&id=" + book.bookId;
          }
        },
        error       : function(errors) {
          console.log(errors)
          hideProgress();
          alert('Error, hubo un problema al confirmar la reserva. Si el problema persiste intente hacer una nueva búsqueda.');
        }
      });
    return false;
  });
</script>

<?php
  if ($Options == 'expired_date') { /*Disable_button();*/ }
  if ($Options == "" && $mayor < -3 ) { echo "<script>$('#divdanger').hide()</script>"; }
  function Disable_button()
  {
      // echo "<script>$('#btn_continue').hide()</script>";
  }
?>
