<?php
$serviceInfo = $service->serviceInfo;
?>

<div class="row">
  <div class="col-md-12">
    <span class="hotel-title"><?php echo $serviceInfo->name; ?></span>
    <table class="table small">
      <thead>
        <tr>
          <th></th>
          <th>Nombre</th>
          <th>Apellido</th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <?php
        $i = 0;
        $first = true;
        foreach ($serviceInfo->guestList as $customer) {
          $i++;
          // $required = $i == 1;
          $required = $first || $serviceInfo->allPaxRequired;
          $required_str = $required ? 'required' : '';
          $required_pax_class = $required ? 'required_pax' : '';
          ?>
          <tr class="<?php echo $required ? '' : 'secondary_pax' ?>">
            <?php
            $pax_count_str = $required ? 'principal' : '#' . $i;
            ?>
            <td>Datos pasajero <?php echo $pax_count_str; ?></td>
            <td><input class="<?php echo $required_pax_class; ?>" name="customerName_<?php echo $service_count . '_' . $i; ?>" <?php echo $required_str; ?> /></td>
            <td><input class="<?php echo $required_pax_class; ?>" name="customerLastName_<?php echo $service_count . '_' . $i; ?>" <?php echo $required_str; ?> /></td>
            <?php $pax_type_str = $customer->type == 'CH' ? 'Ni&ntilde;o' : 'Adulto'; ?>
            <td><small class="text-muted"><?php echo $pax_type_str; ?></small></td>
            <td></td>
            <input type="hidden" name="customerType_<?php echo $service_count . '_' . $i; ?>" value="<?php echo $customer->type ?>" />
            <input type="hidden" name="customerAge_<?php echo $service_count . '_' . $i; ?>" value="<?php echo $customer->age ?>" />
            <input type="hidden" name="CustomerId_<?php echo $service_count . '_' . $i; ?>" value="<?php echo $customer->customerId ?>" />
          </div>
          <?php
        }
        if ($service_count == 1 && isset($rs) && count($rs->bookingServices) > 1) {
          ?>
          <div class="row" style="margin-top: 5px;">
            <div class="col-md-9 col-md-offset-3 small">
              <a href="#" onclick="copyFirstCustomerData(<?php echo count($rs->bookingServices); ?>, <?php echo $i; ?>); return false;">Copiar datos a las siguientes reservas</a>
            </div>
          </div>
          <?php
        }
        ?>
      </tbody>
    </table>
    <?php $availMode = $serviceInfo->availableModalityList[0]; ?>
        <?php if(is_array($availMode->question) && count($availMode->question) > 0) { ?>
                <div id="service_comments_<?php echo $service_count; ?>">
                  <div class="strong">Informaci&oacute;n Obligatoria:</div>
                  <div class="mini-hidden-separator"></div>
                  <div class="col-md-12" style="padding: 15px;margin-bottom: 20px;border: 1px solid #a19494;border-radius: 4px;">
                    <div class="col-md-1">
                        <i class="fa fa-warning" style="color:#e39524;font-size:35px "></i>
                    </div>
                    <div class="col-md-11">
                        Por favor, ten en cuenta que si no proporcionas la información correcta, 
                        el proveedor no se responsabilizará de la correcta prestación del servicio y 
                        puede derivar en cargos de cancelación/no-show.
                    </div>
                  </div>
                  <?php foreach ($availMode->question as $key => $qs) { ?>
                          <div class="strong"><?php echo $qs->text; ?></div>
                          <input name="<?php echo $qs->code . '_' . $service_count;?>" class="required_pax" required />
                  <?php } ?>
                  <div class="mini-hidden-separator"></div>
                </div>
        <?php } ?>
  </div>
</div>

<input type="hidden" name="customerCount_<?php echo $service_count; ?>" value="<?php echo $i; ?>" />
<input type="hidden" name="SPUI_<?php echo $service_count; ?>" value="<?php echo $service->SPUI; ?>" />
