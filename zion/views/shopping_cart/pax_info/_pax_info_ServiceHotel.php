<?php
$hotelAvail = $service->serviceInfo;
$rooms = $hotelAvail->roomsAvail;

if ($hotelAvail->xtraDataRequired) {
  $requiresCustomerExtraInfo = true;
}
?>
<div class="row">
  <div class="col-md-12">
    <span class="hotel-title"><?php echo $hotelAvail->name . ' - ' . $hotelAvail->category; ?></span>
    <table class="table small" style="margin-bottom: 0;">
      <thead>
        <tr>
          <th></th>
          <th>Nombre</th>
          <th>Apellido</th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <?php
        $i = 0;
        foreach ($rooms as $room) {
          $first = true;
          foreach ($room->guestList as $customer) {
            $i++;
            $pax_count_str = $first ? 'principal' : '#' . $i;
            $pax_type_str = 'Adulto';
            if ($customer->type == 'CH') {
              $pax_type_str = 'Ni&ntilde;o (' . $customer->age . ' a&ntilde;os)';
            }
            $required = $first || $hotelAvail->allPaxRequired;
            $required_str = $required ? 'required' : '';
            $required_pax_class = $required ? 'required_pax' : '';
            echo $required ? '<tr>' : '<tr class="secondary_pax">';
            echo '<td>Datos pasajero ' . $pax_count_str . '</td>';
            echo '<td><input class="' . $required_pax_class . '" name="customerName_' . $service_count . '_' . $i . '" ' . $required_str . ' /></td>';
            echo '<td><input class="' . $required_pax_class . '" name="customerLastName_' . $service_count . '_' . $i . '" ' . $required_str . ' /></td>';
            echo '<td class="text-muted">' . $pax_type_str . '</td>';
            echo '<td>' . $room->roomType . '</td>';
            echo '</tr>';
            $first = false;
        ?>
            <input type="hidden" name="customerType_<?php echo $service_count . '_' . $i; ?>" value="<?php echo $customer->type ?>" />
            <input type="hidden" name="customerAge_<?php echo $service_count . '_' . $i; ?>" value="<?php echo $customer->age ?>" />
            <input type="hidden" name="CustomerId_<?php echo $service_count . '_' . $i; ?>" value="<?php echo $customer->customerId ?>" />
          <?php
          }
        }
        if ($service_count == 1 && count($services) > 1) {
          ?>
          <tr>
            <td></td>
            <td colspan="4"><a href="#" onclick="copyFirstCustomerData(<?php echo count($services); ?>, <?php echo $i; ?>); return false;">Copiar datos a las siguientes reservas</a></td>
          </tr>
        <?php
        }
        ?>
        <tr>
          <td colspan="5">
            <div class="link" onclick="$('#service_comments_<?php echo $service_count; ?>').slideToggle();">
              <i class="fa fa-plus"></i> Agregar observaciones para el hotel
            </div>
          </td>
        </tr>
      </tbody>
    </table>

    <div id="service_comments_<?php echo $service_count; ?>" class="collapse">
      <div class="small strong">Observaciones para el hotel (opcionales):</div>
      <div class="col-md-4 small">
        <input type="checkbox" name="room_attr_comment_<?php echo $service_count; ?>[]" value="Habitación fumador / Smoking room. "> Habitaci&oacute;n fumador / Smoking room <br />
        <input type="checkbox" name="room_attr_comment_<?php echo $service_count; ?>[]" value="Dos camas / Two beds. "> Dos camas / Two beds <br />
        <input type="checkbox" name="room_attr_comment_<?php echo $service_count; ?>[]" value="Habitación no fumador / Non-smoking room. "> Habitaci&oacute;n no fumador / Non-smoking room <br />
        <input type="checkbox" name="room_attr_comment_<?php echo $service_count; ?>[]" value="Cliente llegará tarde / Late Arrival. "> Cliente llegar&aacute; tarde / Late Arrival <br />
      </div>
      <div class="col-md-4 small">
        <input type="checkbox" name="room_attr_comment_<?php echo $service_count; ?>[]" value="Cama de matrimonio / Double bed. "> Cama de matrimonio / Double bed <br />
        <input type="checkbox" name="room_attr_comment_<?php echo $service_count; ?>[]" value="Planta baja / Ground floor. "> Planta baja / Ground floor <br />
        <input type="checkbox" name="room_attr_comment_<?php echo $service_count; ?>[]" value="Habitaciones personas con discapacidades / Rooms for disabled guests. "> Habitaciones personas con discapacidades / Rooms for disabled guests <br />
        <input type="checkbox" name="room_attr_comment_<?php echo $service_count; ?>[]" value="Planta alta / Except ground floor. "> Planta alta / Except ground floor <br />
      </div>
      <div class="col-md-4 small">
        <input type="checkbox" name="room_attr_comment_<?php echo $service_count; ?>[]" value="Cuna / Cot. "> Cuna / Cot <br />
        <input type="checkbox" name="room_attr_comment_<?php echo $service_count; ?>[]" value="Luna de miel / Honeymoon. "> Luna de miel / Honeymoon <br />
        <input type="checkbox" name="room_attr_comment_<?php echo $service_count; ?>[]" value="Habitaciones contiguas / Adjacent rooms. "> Habitaciones contiguas / Adjacent rooms <br />
        <input type="checkbox" name="room_attr_comment_<?php echo $service_count; ?>[]" value="Cliente sin bono / Client without voucher. "> Cliente sin bono / Client without voucher <br />
      </div>
      <div class="col-md-12 small">
        <div>Otros comentarios:</div>
        <textarea name="client_comments_<?php echo $service_count; ?>"></textarea>
        <span class="text-muted"> Las observaciones para el establecimiento son solo indicativas, &eacute;stas no pueden ser garantizadas.</span>
        <hr />
      </div>
    </div>
  </div>
</div>

<?php if ($hotelAvail->isDataRequired) { ?>
  <input type="hidden" name="dataRequired_<?php echo $service_count; ?>" value="<?php echo $hotelAvail->availToken; ?>" />
<?php } ?>

<input type="hidden" name="customerCount_<?php echo $service_count; ?>" value="<?php echo $i; ?>" />
<input type="hidden" name="SPUI_<?php echo $service_count; ?>" value="<?php echo $service->SPUI; ?>" />