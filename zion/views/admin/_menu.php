<?php global $current_user; ?>

<?php   if (isset($_SESSION['admin_role']) && $_SESSION['admin_role']) { ?>
            <div class="col-md-12 wbox" style="padding: 15px;">
                <h3>Menu Admin</h3>
                <ul class="nav nav-pills nav-stacked" style="max-width: 300px;">
                    <li id="booking_books"><a href="./?controller=booking&amp;action=books">Reserva de Agencias</a></li>
                    <li id="manual_notes"><a href="./?controller=booking&amp;action=add">Notas Manuales</a></li>
                    <li id="provider_home"><a href="./?controller=provider&amp;action=home">Proveedores</a></li>
                    <li id="admin_edit"><a href="./?controller=booking&amp;action=purchase_detail_list">Reporte de Proveedor</a></li>
                    <?php
                    if ($_SESSION['admin_role'] == 'ADMIN') {
                    ?>
                        <li id="admin_users"><a href="./?controller=admin&amp;action=users">Administradores</a></li>
                        <li id="admin_edit"><a href="./?controller=admin&amp;action=edit">Conf. Sistema</a></li>
                        <li id="admin_edit"><a href="./?controller=admin&amp;action=reports">Reportes</a></li>
                    <?php
                    }
                    $can_show_report = array();
                    if(in_array($current_user->id, $can_show_report)) {
                    ?>
                        <li id="agency_config"><a href="?controller=booking&amp;action=show_books_table">Reporte Contable</a></li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
<?php   } ?>
