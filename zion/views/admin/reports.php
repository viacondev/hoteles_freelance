<link rel="stylesheet" type="text/css" href="../lib/extjs/resources/css/ext-all.css">
<script type="text/javascript" src="../lib/extjs/ext-all.js"></script>

<style type="text/css">
  /* Override bootstrap for ExtJS charts */
  [hidden], template { display: block }
</style>

<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div id="reports"></div>
      </div>
    </div>
  </div>
</div>
