<?
include_once('../../../config/Config.php');
include_once('../../database/DB.php');
include_once('../../model/Model.php');
include_once('../../model/User.php');
include_once('../../model/Agency.php');
include_once('../../model/Booking.php');
include_once('../../model/Service.php');
include_once('../../model/ServiceType.php');
include_once('../../model/ServiceStatus.php');
include_once('../../model/Config.php');
include_once('../../model/Provider.php');
include_once('../../helpers/application_helper.php');
include_once('../../helpers/logger.php');
include_once('../../model/Log.php');

$fecha_in = $_GET['fromDate'];
$fecha_fi = $_GET['toDate'];
$usuarios = $_GET['usuarios'];
$reporte_ventas = $_GET['ventas'];
$tendencias = $_GET['tendencias'] == 'true' ? true : false;
$months = array('', 'En', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ag', 'Sept', 'Oct', 'Nov', 'Dic');

$agency_filter = '';
if (substr($usuarios, 0, 2) == 'AG' && $usuarios != 'AG00') {
  $agency_filter = ' AND agency_id = ' . substr($usuarios, 2, 4); // TODO: allow bulk filter
}

$provider_filter = 0;
if (substr($usuarios, 0, 2) == 'PR' && $usuarios != 'PR00') {
  $provider_filter = intval(substr($usuarios, 2, 4)); // TODO: allow bulk filter
}

$books = Booking::findByQuery("SELECT * FROM Booking
          WHERE confirm_date BETWEEN '$fecha_in 00:00:00' AND '$fecha_fi 23:59:59' $agency_filter
            AND status != " . ServiceStatus::ANNULLED . " AND status != " . ServiceStatus::PENDING);

$data = array();
foreach ($books as $book) {
  $services = $book->getServices();
  foreach ($services as $service) {
    // Filtros
    if ($provider_filter != 0 && $service->provider_id != $provider_filter) {
      continue;
    }

    // Buscar llave del dato
    $item = '';
    if ($tendencias) {
      $date_array = explode('-', $book->confirm_date);
      $month = intval($date_array[1]);
      $year = $date_array[0];
      $item = $months[$month] . " '" . $year;
    }
    else if ($reporte_ventas == 'tipo_ventas') {
      $item = $service->type;
    }
    else if ($reporte_ventas == 'destinos') {
      $dest = strtoupper($service->destination_code);
      $item = $dest != '' ? $dest : 'OTROS';
    }
    else if ($reporte_ventas == 'ventas') {
      if ($usuarios == 'AG00') {
        $item = $book->agency_id;
      }
      else if (substr($usuarios, 0, 2) == 'AG') {
        $item = $book->user_id;
      }
      else if (substr($usuarios, 0, 2) == 'PR') {
        $item = $service->provider_id;
      }
    }

    if (!isset($data[$item])) {
      $data[$item] = array('total' => 0, 'neto' => 0, 'comision' => 0, 'insentivo' => 0, 'ganancia' => 0, 'cantidades' => 0);
    }

    $total = $service->price;
    $neto = $service->net_price;
    $comision = $service->commission;
    $insentivo = $service->incentive;
    $ganancia = $total - $neto - $comision - $insentivo;

    if ($service->currency == 'BOB') {
      $tipo_cambio = '6.96';
      $total /= $tipo_cambio;
      $neto /= $tipo_cambio;
      $comision /= $tipo_cambio;
      $insentivo /= $tipo_cambio;
      $ganancia /= $tipo_cambio;
    }
    else if ($service->currency == 'EUR') {
      $tipo_cambio = '0.882480122';
      $total /= $tipo_cambio;
      $neto /= $tipo_cambio;
      $comision /= $tipo_cambio;
      $insentivo /= $tipo_cambio;
      $ganancia /= $tipo_cambio;
    }

    $data[$item]['total'] += $total;
    $data[$item]['neto'] += $neto;
    $data[$item]['comision'] += $comision;
    $data[$item]['insentivo'] += $insentivo;
    $data[$item]['ganancia'] += $ganancia;
    $data[$item]['cantidades'] += 1;
  }
}

$res = array();
foreach ($data as $key => $value) {
  if ($reporte_ventas == 'destinos' || $tendencias)  {
    $item = $key;
  }
  else if ($reporte_ventas == 'tipo_ventas') {
    $item = ServiceType::getName($key);
  }
  else if ($reporte_ventas == 'ventas') {
    if ($key == 0) {
      $item = 'OTROS';
    }
    else if ($usuarios == 'AG00') {
      $item = Agency::findById($key)->name;
    }
    else if (substr($usuarios, 0, 2) == 'AG') {
      $user = User::findById($key);
      $item =  $user->name . ' ' . $user->lastname;
    }
    else if (substr($usuarios, 0, 2) == 'PR') {
      $item = Provider::findById($key)->name;
    }
  }

  $arr = $value;
  $arr['id'] = $item;
  $res[] = $arr;
}

// Add an item on tendencies to see the graph correctly
if ($tendencias && count($res) > 0) {
  $arr = $res[count($res) - 1];
  $arr['id'] = '';
  $res[] = $arr;
}

echo json_encode($res);
?>