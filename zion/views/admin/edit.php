<?php global $config, $fees; ?>

<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-3"><?php include('views/admin/_menu.php'); ?></div>

      <div class="col-md-9 wbox" style="padding-bottom: 30px;width:73.7%">
      <h2>Configuraci&oacute;n General</h2>

      <form method="post" action="?controller=admin&amp;action=update_fee" class="col-md-7">
        <div class="form-group col-md-12">
          <label>Service Fee General:</label> <span class="small text-muted">(dividir el precio entre)</span>
        </div>
        <div class="col-md-8">
          <input type="text" class="form-control" id="fee" name="fee" value="<?php echo $config->fee;?>">
        </div>
        <div class="form-group col-md-4">
          <button class="btn btn-info-css">Guardar</button>
        </div>
      </form>

      <div class="col-md-12"><hr /></div>
      <div class="col-md-7">
        <div class="form-group col-md-12">
          <label>Service Fee Espec&iacute;ficos:</label>
          <table class="table">
            <thead>
              <th>Destino</th>
              <th>Fee</th>
              <th>&nbsp;</th>
            </thead>
            <tbody>
              <?php
              foreach ($fees as $fee) {
                echo '<tr>';
                echo '<td>' . $fee->destination_code . '</td>';
                echo '<td>' . $fee->fee . '</td>';
                echo '<td><a href="?controller=fee&action=remove&id=' . $fee->id . '" class="text-danger"><i class="fa fa-times-circle"></i></a></td>';
                echo '</tr>';
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>

      <form method="post" action="?controller=fee&amp;action=create" class="col-md-7">
        <div class="col-md-12">
          <label>Agregar fee a destino espec&iacute;fico:</label>
        </div>
        <div class="col-md-6">
          <input type="text" class="form-control" id="destination" name="destination" placeholder="Introduce tu destino...">
          <input type="hidden" id="destination_code" name="destination_code" />
        </div>
        <div class="col-md-3">
          <input type="text" class="form-control" id="fee" name="fee" placeholder="fee...">
        </div>
        <div class="col-md-3">
          <button class="btn btn-info-css">Agregar</button>
        </div>
      </form>

      <div class="col-md-12"><hr /></div>
      <form method="post" action="?controller=admin&amp;action=update_mail" class="col-md-9">
        <div class="col-md-12">
          <label>Mails de la operadora:</label> <span class="small text-muted">(donde se recibiran las confirmaciones)</span>
          <div class="col-md-5">
            <input type="text" class="form-control" id="mail" name="mail" value="<?php echo $config->mail; ?>" placeholder="mail principal">
          </div>
          <div class="col-md-5">
            <input type="text" class="form-control" id="mail2" name="mail2" value="<?php echo $config->mail2; ?>" placeholder="mail secundario (opcional)">
          </div>
          <div class="col-md-2">
            <button class="btn btn-info-css">Guardar</button>
          </div>
        </div>
      </div>

      <div class="col-md-12"><hr /></div>
    </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(function() {
    $('#admin_edit').addClass('active');
  });
</script>
