<?php  ?>

<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-3"><?php include('views/admin/_menu.php'); ?></div>

      <div class="col-md-9 wbox" style="width:73.7%">
      <h2>Administradores del sistema</h2>

      <div class="col-md-5">
        <table class="table">
          <thead>
            <tr>
              <th>Usuario</th>
              <th>Nombre</th>
              <th>Rol</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $admins = Admin::findAll();
            foreach ($admins as $admin) {
              echo '<tr>';
              $user = User::find(array('id' => $admin->user_id));
              echo '<td>' . $user->username . '</td>';
              echo '<td>' . $user->name . ' ' . $user->lastname . '</td>';
              echo '<td>' . $admin->role . '</td>';
              echo '</tr>';
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(function() {
    $('#admin_users').addClass('active');
  });
</script>
