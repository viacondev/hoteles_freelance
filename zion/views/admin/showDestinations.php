
<?
  global $country;
?>
<style>
  tr:hover{
    background-color:#888686;
    color: white;
  }
</style>
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-12 wbox">
        <div class="col-md-12" style="padding:15px">
          <div class="row">
            <!-- <div class="col-md-6">
              <div class="form-group">
                <input name="search" id="auto">
                <input type="button" name="" value="Buscar Destinos" onclick="searchDestinos()">
                <input type="hidden" name="country_code" value="" id="country_code">
              </div>
            </div> -->
            <div class="col-md-12">
              <div class="resultDest" id="resultDest">
                <table class="table">
                  <tr>
                      <th>Codigo Destino</th>
                      <th>Destino</th>
                      <th>Codigo Pais</th>
                      <th>Pais</th>
                  </tr>
                  <?
                    foreach ($country as $value) {
                  ?>
                    <tr>
                      <td><? echo $value->destination_code; ?></td>
                      <td><? echo $value->destination_name; ?></td>
                      <td><? echo $value->country_code; ?></td>
                      <td><? echo $value->country_name; ?></td>
                    </tr>
                  <?
                    }
                  ?>
                </table>
              </div>
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>
</div>
<script>
$(function(){
  var search = $('#auto').val()
  $("#auto").autocomplete({
      minLength: 3,
      source: function(request, response) {
            $.getJSON(
                '../zion/helpers/ajax/searchDest.php',
                { term:'country', extraParams:$('#auto').val() },
                response
            );
        },
      select: function(event, ui) {
        $('#country_code').val(ui['item']['id']);
      }
  });

})
function searchDestinos() {
  if($('#country_code').val() == '') {
    alert("Debes Seleccionar un Pais")
    return;
  }
  var filter = $('#country_code').val();
  $.ajax({
    url: '../zion/helpers/ajax/searchDest.php',
    type: 'GET',
    data: {term:'Dest', extraParams:filter},
    success: function(r) {
      var res = JSON.parse(r);
      var strDestino = '<table class="table">' +
                        "<tr>" +
                          "<th>Codigo</th>" +
                          "<th>Nombre</th>" +
                        "</tr>";

      for (var i = 0; i < res.length; i++) {
        strDestino += "<tr>" +
                        "<td>" + res[i].destid + "</td>" +
                        "<td>" + res[i].destname + "</td>" +
                      "</tr>";
      }
      strDestino += "</table>";
      $('#resultDest').html(strDestino);
    }
  })
}
</script>
