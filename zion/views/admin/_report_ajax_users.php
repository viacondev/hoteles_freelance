<?
include_once('../../../config/Config.php');
include_once('../../database/DB.php');
include_once('../../model/Model.php');
include_once('../../model/User.php');
include_once('../../model/Agency.php');
include_once('../../model/Booking.php');
include_once('../../model/Service.php');
include_once('../../model/ServiceType.php');
include_once('../../model/Config.php');
include_once('../../model/Provider.php');
include_once('../../helpers/application_helper.php');
include_once('../../helpers/logger.php');
include_once('../../model/Log.php');

$users = array();
$type = '';
if ($_GET['id'] == 'AG00') {
  $users = Agency::findAll();
  $type = 'AG';
}
else if ($_GET['id'] == 'PR00') {
  $users = Provider::findAll();
  $type = 'PR';
}

$data = array();
foreach ($users as $user) {
  $data[] = array(
    'text'  => $user->name,
    'id'    => $type . $user->id,
    'leaf'  => 'true',
    'icon'  => 'assets/images/user16.ico');
}

echo json_encode($data);
?>