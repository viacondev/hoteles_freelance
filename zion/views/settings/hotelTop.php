<? global $res ?>
<div id="shopping_cart_confirm" class="content">
  <div class="container">
  	<div class="wbox" style="padding: 10px;">
  		<div class="row">
  			<div class="col-md-12">
  				<form id="fmrDest" method="get">
		  			<select id="destCode" name="destCode" onchange="$('#fmrDest').submit()">
		  				<option>Default</option>
			  			<option value="CUN" <? echo $_GET['destCode'] == "CUN" ? "selected" : ""; ?> >CANCUN</option>
			  			<option value="PUJ" <? echo $_GET['destCode'] == "PUJ" ? "selected" : ""; ?> >PUNTA CANA</option>
			  			<option value="CTG" <? echo $_GET['destCode'] == "CTG" ? "selected" : ""; ?> >CARTAGENA</option>
			  			<option value="MIA" <? echo $_GET['destCode'] == "MIA" ? "selected" : ""; ?> >MIAMI</option>
			  			<option value="MCO" <? echo $_GET['destCode'] == "MCO" ? "selected" : ""; ?> >ORLANDO</option>
			  			<option value="BUE" <? echo $_GET['destCode'] == "BUE" ? "selected" : ""; ?> >BUENOS AIRES</option>
			  			<option value="SLT" <? echo $_GET['destCode'] == "SLT" ? "selected" : ""; ?> >SALTA</option>
			  			<option value="BCH" <? echo $_GET['destCode'] == "BCH" ? "selected" : ""; ?> >BARILOCHE</option>
			  			<option value="MEZ" <? echo $_GET['destCode'] == "MEZ" ? "selected" : ""; ?> >MENDOZA</option>
			  			<option value="SAO" <? echo $_GET['destCode'] == "SAO" ? "selected" : ""; ?> >SAO PAULO</option>
			  			<option value="RIO" <? echo $_GET['destCode'] == "RIO" ? "selected" : ""; ?> >RIO DE JANEIRO</option>
			  			<option value="RCF" <? echo $_GET['destCode'] == "RCF" ? "selected" : ""; ?> >RECIFE</option>
			  			<option value="NAT" <? echo $_GET['destCode'] == "NAT" ? "selected" : ""; ?> >NATAL</option>
			  			<option value="SVD" <? echo $_GET['destCode'] == "SVD" ? "selected" : ""; ?> >SALVADOR BAHIA</option>
			  			<option value="SCL" <? echo $_GET['destCode'] == "SCL" ? "selected" : ""; ?> >SANTIAGO</option>
			  			<option value="IQQ" <? echo $_GET['destCode'] == "IQQ" ? "selected" : ""; ?> >IQUIQUE</option>
			  			<option value="ARI" <? echo $_GET['destCode'] == "ARI" ? "selected" : ""; ?> >ARICA</option>
			  			<option value="BOG" <? echo $_GET['destCode'] == "BOG" ? "selected" : ""; ?> >BOGOTA</option>
			  			<option value="MDE" <? echo $_GET['destCode'] == "MDE" ? "selected" : ""; ?> >MEDELLIN</option>
			  			<option value="CLO" <? echo $_GET['destCode'] == "CLO" ? "selected" : ""; ?> >CALI</option>
			  			<option value="CTG" <? echo $_GET['destCode'] == "CTG" ? "selected" : ""; ?> >CARTAGENA</option>
			  			<option value="ADZ" <? echo $_GET['destCode'] == "ADZ" ? "selected" : ""; ?> >SAN ANDRES</option>
			  			<option value="HAV" <? echo $_GET['destCode'] == "HAV" ? "selected" : ""; ?> >HABANA</option>
			  			<option value="VRA" <? echo $_GET['destCode'] == "VRA" ? "selected" : ""; ?> >VARADERO</option>
			  			<option value="UIO" <? echo $_GET['destCode'] == "UIO" ? "selected" : ""; ?> >QUITO</option>
			  			<option value="GYE" <? echo $_GET['destCode'] == "GYE" ? "selected" : ""; ?> >GUAYAQUIL</option>
			  			<!-- <option value="MCO" <? //echo $_GET['destCode'] == "MCO" ? "selected" : ""; ?> >GALAPAGOS</option> -->
			  			<option value="ONX" <? echo $_GET['destCode'] == "ONX" ? "selected" : ""; ?> >PANAMA</option>
			  			<option value="ASU" <? echo $_GET['destCode'] == "ASU" ? "selected" : ""; ?> >ASUNCION</option>
			  			<!-- <option value="MCO" <? //echo $_GET['destCode'] == "MCO" ? "selected" : ""; ?> >FOZ DE IGUAZU</option> -->
			  			<option value="LIM" <? echo $_GET['destCode'] == "LIM" ? "selected" : ""; ?> >LIMA</option>
			  			<option value="CUZ" <? echo $_GET['destCode'] == "CUZ" ? "selected" : ""; ?> >CUZCO</option>
			  			<option value="MVD" <? echo $_GET['destCode'] == "MVD" ? "selected" : ""; ?> >MONTEVIDEO</option>
			  			<option value="PDP" <? echo $_GET['destCode'] == "PDP" ? "selected" : ""; ?> >PUNTA DEL ESTE</option>
			  		</select>
			  		INTRODUCE TU DESTINO
			  		<input type="hidden" name="controller" value="settings">
			  		<input type="hidden" name="action" value="hotelTop">
		  		</form>
  			</div>
  			
	  		<br/>
	  		<div class="col-md-6">
	  			<table class="table">
	  				<tr>
	  					<th>Codigo</th>
	  					<th>Nombre</th>
	  					<th>*</th>
	  					<th>*</th>
	  				</tr>
			  	<?
			  		$con = 0;
			  		foreach ($res as $hotel) {
			  	?>
				  		<tr>
				  			<td><? echo $hotel['zion_code']; ?></td>
				  			<td><? echo $hotel['hotel_name']; ?></td>
				  			<td>
				  		<? 	if (!$hotel['exist']) { 	?>
				  				<input 	type="button" id="btn_save_<? echo $con ?>" name="btn_save" 
				  						value="Guardar" data-s="<? echo htmlspecialchars(json_encode($hotel), ENT_QUOTES, 'UTF-8'); ?>" onclick="save('btn_save_<? echo $con ?>')" >
				  		<?	}	?>
				  			</td>
				  			<td>
				  		<?	if($hotel['exist']) {	?>
				  				<input 	type="button" id="btn_delete_<? echo $con ?>" name="btn_delete" 
				  						value="Eliminar" data-d="<? echo htmlspecialchars(json_encode($hotel), ENT_QUOTES, 'UTF-8'); ?>" onclick="fDelete('btn_delete_<? echo $con ?>')" >
				  		<?	}	?>
				  			</td>
				  		</tr>
			  	<?		$con++;
			  		}
			  	?>
		  		</table>
	  		</div>
  		</div>
  	</div>
  </div>
 </div>

 <script type="text/javascript">
 	function save(id) {
 		console.log(id)
 		params = $('#' + id).data('s');
 		$.ajax({
	        url: "../zion/helpers/ajax/save_topten.php",
	        dataType : "html",
	        type : "post",
	        data:{params:params, action:"save"}
	      })
	      .done(function(response){
	        $('.loading').html('');
	        console.log(response);
	        $('#fmrDest').submit()
	      });
 	}
 	function fDelete(id) {
 		console.log(id)
 		params = $('#' + id).data('d');
 		$.ajax({
	        url: "../zion/helpers/ajax/save_topten.php",
	        dataType : "html",
	        type : "post",
	        data:{params:params, action:"delete"}
	      })
	      .done(function(response){
	        $('.loading').html('');
	        console.log(response);
	        $('#fmrDest').submit()
	      });
 	}
 </script>