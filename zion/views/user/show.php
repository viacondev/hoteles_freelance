<?php global $user; ?>

<div class="content">
  <div class="container">
    <div class="col-md-3"><?php include('views/admin/_menu.php'); ?></div>

    <div class="col-md-9 wbox">
      <h2>Datos Usuario</h2>
      <div class="col-md-12 text-right">
        <a href="?controller=user&amp;action=edit&amp;id=<?php echo $user->id; ?>"><i class="fa fa-pencil"></i> Editar Usuario</a>
      </div>
      <table class="table table-hover">
        <tbody>
          <tr>
            <td><strong>Usuario:</strong></td>
            <td><?php echo $user->username; ?></td>
          </tr>
          <tr>
            <td><strong>Rol:</strong></td>
            <td><?php echo $user->role == 'ADMIN' ? 'Administrador' : 'Counter'; ?></td>
          </tr>
          <tr>
            <td><strong>Nombre:</strong></td>
            <td><?php echo $user->name . ' ' . $user->lastname; ?></td>
          </tr>
          <tr>
            <td><strong>Tel&eacute;fonos:</strong></td>
            <td><?php echo $user->phone; ?></td>
          </tr>
          <tr>
            <td><strong>Correo:</strong></td>
            <td><?php echo $user->mail; ?></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>