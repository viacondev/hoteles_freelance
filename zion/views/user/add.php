<?php global $agency; ?>

<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-3"><?php include('views/admin/_menu.php'); ?></div>

      <div class="col-md-9 wbox" style="width:73.7%">
      <h2>Agregar Usuario</h2>
      <form method="post" action="?controller=user&amp;action=create">
        <div class="form-group col-md-12">
          <label>Agencia:</label>
          <?php echo $agency->name; ?>
        </div>
        <div class="form-group col-md-12">
          <?
            if (!empty($_GET['error'])){
              echo "<div class='alert alert-dismissable alert-danger'>
                      <button type='button' class='close' data-dismiss='alert'>×</button>
                      <strong>Usuario Existente!</strong> Intente Otra Vez
                    </div>";
            }
          ?>
        </div>
        <div class="col-md-6 rborder">
          <h4>Datos de acceso</h4>
          <div class="form-group col-md-12">
            <label>Usuario:</label><span class="small text-muted"> (Nombre para acceder al sistema)</span>
            <input type="text" class="form-control" id="username" name="username" />
          </div>
          <div class="form-group col-md-12">
            <label>Password:</label>
            <input type="password" class="form-control" id="password" name="password" />
          </div>
          <div class="form-group col-md-12">
            <label>Rol:</label><br />
            <select class="form-control" id="role" name="role">
              <option value="ADMIN">Administrador</option>
              <option value="COUNTER" selected="selected">Counter</option>
            </select>
          </div>
        </div>

        <div class="col-md-6">
          <h4>Datos Personales</h4>
          <div class="form-group col-md-12">
            <label>Nombre:</label>
            <input type="text" class="form-control" id="name" name="name">
          </div>
          <div class="form-group col-md-12">
            <label>Apellido:</label>
            <input type="text" class="form-control" id="lastname" name="lastname">
          </div>
          <div class="form-group col-md-12">
            <label>Tel&eacute;fono de contacto:</label>
            <input type="text" class="form-control" id="phone" name="phone">
          </div>
          <div class="form-group col-md-12">
            <label>Correo de contacto:</label>
            <input type="text" class="form-control" id="mail" name="mail">
          </div>
        </div>

        <div class="form-group col-md-12">
          <input type="hidden" name="agency_id" value="<?php echo $agency->id; ?>" />
          <button class="btn btn-info-css">Guardar</button>
        </div>
      </form>
    </div>
    </div>
  </div>
</div>
