<?php global $agency, $user; ?>

<div class="content">
  <div class="container">
    <div class="col-md-3"><?php include('views/admin/_menu.php'); ?></div>

    <div class="col-md-9 wbox">
      <h2>Editar Usuario</h2>
      <form method="post" action="?controller=user&amp;action=update">
        <div class="form-group col-md-12">
          <label>Agencia:</label>
          <?php echo $agency->name; ?>
        </div>

        <div class="col-md-6 rborder">
          <h4>Datos de acceso</h4>
          <div class="form-group col-md-12">
            <label>Usuario:</label><span class="small text-muted"> (Nombre para acceder al sistema)</span>
            <input type="text" class="form-control" id="username" name="username" value="<?php echo $user->username; ?>" />
          </div>
          <div class="form-group col-md-12">
            <label>Password:</label><span class="small text-muted"> (En blanco para mantener el password actual)</span>
            <input type="password" class="form-control" id="password" name="password" />
          </div>
          <div class="form-group col-md-12">
            <label>Rol:</label> <?php echo $user->role == 'ADMIN' ? 'Administrador' : 'Counter'; ?>
            <input type="hidden" id="role" name="role" value="<?php echo $user->role; ?>" />
          </div>
        </div>

        <div class="col-md-6">
          <h4>Datos Personales</h4>
          <div class="form-group col-md-12">
            <label>Nombre:</label>
            <input type="text" class="form-control" id="name" name="name" value="<?php echo $user->name; ?>">
          </div>
          <div class="form-group col-md-12">
            <label>Apellido:</label>
            <input type="text" class="form-control" id="lastname" name="lastname" value="<?php echo $user->lastname; ?>">
          </div>
          <div class="form-group col-md-12">
            <label>Tel&eacute;fono de contacto:</label>
            <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $user->phone; ?>">
          </div>
          <div class="form-group col-md-12">
            <label>Correo de contacto:</label>
            <input type="text" class="form-control" id="mail" name="mail" value="<?php echo $user->mail; ?>">
          </div>
        </div>

        <div class="form-group col-md-12">
          <input type="hidden" name="id" value="<?php echo $user->id; ?>" />
          <input type="hidden" name="agency_id" value="<?php echo $agency->id; ?>" />
          <button class="btn btn-info-css">Guardar</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(function() {
    $('#user_profile').addClass('active');
  });
</script>
