<?php
session_start();
include(__DIR__ . '../../../../config/Config.php');
include(__DIR__ . '../../../database/DB.php');
include(__DIR__ . '../../../model/Model.php');
include(__DIR__ . '../../../model/User.php');
include(__DIR__ . '../../Pqt_Integracion/ViaconMapping.php');
include(__DIR__ . '../../Pqt_Integracion/integration_function.php');

$user_viacon = user_cotizacion::getViaconid($_SESSION['current_user']->id);
$params = json_decode($_POST['params']);
?>
  <div id="maps_hotel" class="modal ">
    <div class="modal-dialog modal-lg pantalla_completa" id="resize">
      <div class="modal-content" style="">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Localizaci&oacute;n de Hoteles</h4>
        </div>
        <div class="modal-body" style="padding:0px">
          <div class="col-md-12" id="options_markers" style="margin-top: 11px;padding: 7px 0px 0px 0px;">
              <div class="col-md-1">
              </div>

              <div class="col-md-2" style="">
                <label>
                  <input type="checkbox" id="showRadius" onchange="toggleRadiusMarker();" > Radio m&aacute;ximo
                </label>
              </div>

              <div class="col-md-3">
                <label>
                  <input type="checkbox" id="showOnlyWithinRadio" onchange="toggleMarkersOutsideRadius();"> S&oacute;lo hoteles dentro del radio
                </label>
              </div>

              <div class="col-md-2 solo-viacon">
                <label>
                  <input type="checkbox" id="solo_cotizados" onchange="mostrar_cotizados();"> <?php echo ($user_viacon) ? "S&oacute;lo Cotizados" : "S&oacute;lo Seleccionados"; ?>
                </label>
              </div>

              <div class="col-md-2">
                Radio: <input type="number" id="maxDist" name="maxDist" onchange="updateMaxDistRadius()" value="5" style="width: 40px;"> Km.
              </div>

              <div class="col-md-2 link div-print" >
                <span class="fa-stack fa-2x" onclick="printMap()">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-print fa-stack-1x fa-inverse"></i>
                </span>
                <?php  if($user_viacon) { ?>
                      <button class="btn btn-info" name="cotizar" onclick="view_select();">Cotizar</button>
                <?php  } ?>
              </div>
          </div>

          <div id="content-information" style="display:none">
            <div class="text-center" style="text-align:center"><h2 class="ct_destino"><?php echo $params->hotel_destination; ?></h2></div>
            <div class="col-md-12" style="margin-top:10px;padding: 10px;font-size:15px;border: 1px solid #7B7777;border-radius: 5px;margin-bottom: 5px;">
              <div class="col-md-12">
                <strong>Ocupaci&oacute;n :</strong>&nbsp;
                <span id="pr_habitaciones"></span>
                <span id="pr_adultos"></span>
                <span id="pr_ninos"></span>
              </div>
              <div class="col-md-12">
                <strong>Entrada:&nbsp;</strong>&nbsp;<span ><?php echo date('d/m/Y',strtotime($params->checkin)); ?></span>&nbsp;&nbsp;
                <strong>Salida:&nbsp;</strong> &nbsp;<span ><?php echo date('d/m/Y',strtotime($params->checkout)); ?></span>
                  <small id="pr_noches">
                    <?php
                      if (isset($_GET['checkin']) && isset($_GET['checkout'])) {
                        $from = new DateTime(dateFormatForDB(_get('checkin')));
                        $to = new DateTime(dateFormatForDB(_get('checkout')));
                        $nights = $to->diff($from)->days;
                        echo "($nights noches)";
                      }
                    ?>
                  </small>
              </div>
            </div>
            <div id="list-hotel-select" style="margin-top:10px;padding: 10px;font-size:15px;border: 1px solid #7B7777;border-radius: 5px;margin-bottom: 5px;"></div>
          </div>

          <div class="clearfix"></div>
          <div id="googlemapview_container" class="mapNormalScreen">
            <span style='color:red;margin-left:20px;' id='mensaje-map'><strong>Nota:</strong> Tomar en cuenta que la ubicaci&oacute;n de cada hotel es proporcionada por los proveedores</span>
            <input id="search-input" class="controls hidden-print" type="text" placeholder="Buscar un lugar...">
            <div id="googlemapview" class="map" style="height:600px"></div>
          </div>
        </div>
      </div>
    </div>
  </div>

<script type="text/javascript">
habitaciones = $('#habitaciones option:selected').text();
var adultos = 0;
var ninos = 0;
for(i = 1; i <= habitaciones;i++) {
  adultos += parseInt($('#adultos'+i).val());
  ninos   += parseInt($('#ninos'+i).val());
}
str_hab = (habitaciones == 1) ? (habitaciones + " Habitaci&oacute;n") : (habitaciones + " Habitaciones");
str_adult = (adultos == 1) ? (adultos + " Adulto") : (adultos + " Adultos");
if(ninos != 0) {
  str_ninos = (ninos == 1) ? (ninos + " ni&ntilde;o") : (ninos + " ni&ntilde;os");
  $('#pr_ninos').append(str_ninos);
}
$('#pr_habitaciones').append('<span>' + str_hab + '</span>');
$('#pr_adultos').append('<span>' + str_adult +'</span>');
var hotelAvailRes = <?php echo $_POST['data'];  ?>;
var centerLat = '';
var centerLon = '';
var hotels_maps    = [];
var marcadores     = [];
var Items_visible  = [];
var hotel_pos = 1;
for (i = 0;i < hotelAvailRes.length;i++) {
  if(hotelAvailRes[i].latitude != '' && hotelAvailRes[i].longitude != '') {
    centerLat = hotelAvailRes[i].latitude;
    centerLon = hotelAvailRes[i].longitude;
    break;
  }
}
$('#googlemap img').attr('src','http://maps.googleapis.com/maps/api/staticmap?center='+centerLat+','+centerLon+'&zoom=11&size=415x360&sensor=false');
function initialize() {
  var mapOptions = {
    zoom: 11,
    center: new google.maps.LatLng(centerLat, centerLon)
  };
  map = new google.maps.Map(document.getElementById('googlemapview'), mapOptions);

  radiusMarker = new google.maps.Marker({
    map       : map,
    position  : new google.maps.LatLng(centerLat, centerLon),
    draggable : true,
    title     : 'Puedes arrastrar para buscar hoteles en una nueva area',
    icon      : 'http://chart.apis.google.com/chart?chst=d_map_spin&chld=.7|0|1|10|_|R',
    zIndex   : 1
  });
  circle = new google.maps.Circle({
    map: map,
    radius: maxDist * 1000,
    strokeOpacity: 0.6,
    strokeWeight: 1,
    fillColor: "#EA7F8C",
    fillOpacity: 0.4
  });
  circle.bindTo('center', radiusMarker, 'position');
  toggleRadiusMarker();

  searchInput = (document.getElementById('search-input'));
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(searchInput);

  searchBox = new google.maps.places.SearchBox((searchInput));

  google.maps.event.addListener(searchBox, 'places_changed', function() {
    var places = searchBox.getPlaces();
    if (places.length == 0) {
      return;
    }
    place = places[0];
    radiusMarker.setPosition(place.geometry.location);
    $('#showRadius').prop('checked', true);
    toggleRadiusMarker();

    map.setCenter(radiusMarker.getPosition())
  });

  google.maps.event.addListener(map, 'bounds_changed', function() {
    var bounds = map.getBounds();
    searchBox.setBounds(bounds);
  });


  google.maps.event.addListener(radiusMarker, 'dragend', function() { toggleMarkersOutsideRadius(); } );

  cargar_hoteles();
}

function cargar_hoteles() {
  for (i = 0;i < hotelAvailRes.length;i++) {
    if (hotelAvailRes[i].longitude != '') {
      hotel = {
        pos: hotel_pos,
        hotelCode : hotelAvailRes[i].code,
        name      : hotelAvailRes[i].name,
        category  : '',
        price     : getGroupedRoomsPrice(hotelAvailRes[i].groupedRooms[0]),
        board     : hotelAvailRes[i].groupedRooms[0][0].board,
        roomType  : hotelAvailRes[i].groupedRooms[0][0].roomType,
        currency  : hotelAvailRes[i].currency,
        lat       : hotelAvailRes[i].latitude,
        lon       : hotelAvailRes[i].longitude
      }
      hotels_maps[hotel.pos]   = hotel;
      Items_visible[hotel.pos] = false;
      adicionar_marcadores(hotel);
      hotel_pos++;
    }  
  }
}

function adicionar_marcadores(hotel) {
  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(hotel.lat, hotel.lon),
    map: map,
    color: 'azul',
    title: hotel.name + ' - Desde ' + hotel.price + ' ' + hotel.currency,
    icon: 'http://chart.apis.google.com/chart?chst=d_map_spin&chld=.6|0|54ABD5|10|_|' + hotel.pos
  });
  marcadores[hotel.pos] = marker;

  google.maps.event.addListener(marker, 'click', function() {
    <?php if ($user_viacon) { ?>
          cotizar_hotel_select(hotel);
    <?php }
       else{
    ?>
          seleccionar_hotel(hotel);
    <?php
       }
    ?>
  });
}

function cotizar_hotel_select(hotel) {
  if ($('#main_hotel_'+hotel.hotelCode).is(':checked')) {
    marcadores[hotel.pos].setIcon('http://chart.apis.google.com/chart?chst=d_map_spin&chld=.6|0|54ABD5|10|_|'+hotel.pos);
  }
  else {
    marcadores[hotel.pos].setIcon('http://chart.apis.google.com/chart?chst=d_map_spin&chld=.6|0|FACC2E|10|_|'+hotel.pos);
  }
  $('#main_hotel_'+hotel.hotelCode).click();
}

function seleccionar_hotel(hotel) {
  if (marcadores[hotel.pos].color == 'azul') {
    marcadores[hotel.pos].setIcon('http://chart.apis.google.com/chart?chst=d_map_spin&chld=.6|0|FACC2E|10|_|' + hotel.pos);
    marcadores[hotel.pos].set('color', 'amarillo');
    Items_visible[hotel.pos] = true;
  }
  else {
    marcadores[hotel.pos].setIcon('http://chart.apis.google.com/chart?chst=d_map_spin&chld=.6|0|54ABD5|10|_|' + hotel.pos);
    marcadores[hotel.pos].set('color', 'azul');
    Items_visible[hotel.pos] = false;
  }
}

function toggleRadiusMarker() {
  visible = $('#showRadius').is(':checked');
  circle.setVisible(visible);
  radiusMarker.setVisible(visible);

  if ($('#showOnlyWithinRadio').is(':checked')) {
    toggleMarkersOutsideRadius();
  }
}

function toggleMarkersOutsideRadius() {
  <?php if ($user_viacon) { ?>
      if ($('#solo_cotizados').is(':checked')) {
        $('#solo_cotizados').click();
      }
  <?php } ?>
  for (var i = 1; i < marcadores.length; i++) {
    if (marcadores[i] != undefined) {
      var hide = $('#showOnlyWithinRadio').is(':checked')  && $('#showRadius').is(':checked') && !circle.getBounds().contains(marcadores[i].getPosition());
      marcadores[i].setVisible(!hide);
    }
  }
}

function habilitar_marcador(id){
  for (i = 1; i < hotels_maps.length; i++) {
     if (hotels_maps[i].hotelCode == id) {
        if ($('#main_hotel_'+id).is(':checked')) {
          marcadores[hotels_maps[i].pos].setIcon('http://chart.apis.google.com/chart?chst=d_map_spin&chld=.6|0|FACC2E|10|_|' + hotels_maps[i].pos);
          Items_visible[hotels_maps[i].pos] = true;
        }
        else {
          marcadores[hotels_maps[i].pos].setIcon('http://chart.apis.google.com/chart?chst=d_map_spin&chld=.6|0|54ABD5|10|_|' + hotels_maps[i].pos);
          Items_visible[hotels_maps[i].pos] = false;
        }
     }
  }
}

function showDistanceSearchModal(title, lat, lon) {
   var centerLat = lat;
   var centerLon = lon;
   if (centerLat != '') {
     $('#maps_hotel').modal();
     $('#showRadius').prop('checked', true);
      var newLatLng = new google.maps.LatLng(lat, lon);
      radiusMarker.setPosition(newLatLng);

      toggleRadiusMarker();
      google.maps.event.trigger(map, "resize");
      map.setCenter(newLatLng);
   }
}

function cargar_html_hotel() {
  var html = "<table class='small' id='list_table'>" +
                '<tr>' +
                  '<th>N#</th>' +
                  '<th>Nombre</th>' +
                  '<th>Tipo de Hab.</th>' +
                  '<th>Regimen</th>' +
                  '<th>Precio</th>' +
                '</tr>';
  for (i = 0;i < hotels_maps.length; i++) {
    if (Items_visible[i]) {
      html += '<tr>' +
                "<td>" + hotels_maps[i].pos + "</td>" +
                "<td><span style='color: #3399f3;'>" + hotels_maps[i].name + "</span></td>" +
                "<td>" + hotels_maps[i].roomType + "</td>" +
                "<td>" + hotels_maps[i].board + "</td>" +
                "<td>" + hotels_maps[i].price + ' ' + hotels_maps[i].currency + "</td>" +
                "<td></td>" +
              "</tr>";
    }
  }
  html += "</table>";
  $('#list-hotel-select').html(html);
}

function printMap() {
  if($('#load-cotizacion').is(':visible')) {
    $('#load-cotizacion').hide();
  }
  cargar_html_hotel();
  $('#content-information').show();
  $('#resize').removeClass('pantalla_completa');
  $('#header').hide();
  $('footer').hide();
  $('#resultbar').hide();
  $('#refine-form').hide();
  $('#jivo_top_wrap').hide();
  $('.hotel_avail').hide();
  $('#googlemap').hide();
  $('#mensaje-map').hide();
  $('#options_markers, .modal-header').hide();
  $('#ui-datepicker-div').addClass('hidden-print');
  window.print();

  // Restore
  $('#options_markers, .modal-header').show();
  $('#googlemap').show();
  $('.hotel_avail').show();
  $('#header').show();
  $('footer').show();
  $('#resultbar').show();
  $('#refine-form').show();
  $('#jivo_top_wrap').show();
  $('#filter_information').hide();
  $('#content-information').hide();
  $('#mensaje-map').show();
  $('#resize').addClass('pantalla_completa');
}

function getGroupedRoomsPrice(groupedRooms) {
  var price = 0;
  for (var i = 0; i < groupedRooms.length; i++) {
    price += groupedRooms[i].price;
  }

  return Math.round(price * 100) / 100;
}

function abrirmodal() {
  $('#maps_hotel').modal();
  var center = map.getCenter();
  google.maps.event.trigger(map, "resize");
  map.setCenter(center);
}

var ready = function() {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyD56vxIspKesbI0tRFvXe9Txrb3pWaBwps&v=3.exp&libraries=places&callback=initialize';
  document.body.appendChild(script);
};
$(document).ready(ready);

</script>
