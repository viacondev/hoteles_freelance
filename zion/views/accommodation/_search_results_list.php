<div class="col-md-12 wbox">
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Hotel</th>
        
        <th>Zona</th>
        <th>Tipo Hab.</th>
        <th>R&eacute;gimen</th>
        <th>Precio desde</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
      </tr>
    </thead>
    <tbody class="small">
      <?php
      $hotelCodes = array();
      foreach ($rs->getHotelsAvail() as $hotelAvail) {
        if (in_array($hotelAvail->getCode(), $hotelCodes)) {
          continue;
        }
        $hotelCodes[] = $hotelAvail->getCode();
        $groupedRooms = $hotelAvail->getGroupedRooms();

        $price = 0;
        foreach ($groupedRooms[0] as $room) {
          $price += floatval($room->getPrice());
        }
        $price = round(Agency::getUserPrice($price), 2);
        $hotelLink = getHotelLink($hotelAvail);
        ?>
        <tr>
          <td>
            <img src="<?php echo $hotelAvail->getMainImage(); ?>" id="hotel-image-<?php echo $hotelAvail->getCode(); ?>" style="display: none;" />
            <a href="<?php echo $hotelLink; ?>" class="loadingAction">
              <?php echo $hotelAvail->getName() ?>
            </a>
            <br /><?php showCategory($hotelAvail->getCategoryCode()); ?>
          </td>
          <td><?php echo $hotelAvail->getZone(); ?></td>
          <td><?php echo $groupedRooms[0][0]->getRoomType(); ?></td>
          <td><?php echo $groupedRooms[0][0]->getBoard(); ?></td>
          <td><strong><?php echo $price; ?> <?php echo $hotelAvail->getCurrency(); ?></strong></td>
          <td><a href="<?php echo $hotelLink; ?>#search_avail" class="btn btn-info btn-xs loadingAction" target="_blank">Ver Tarifas</a></td>
          <td>
            <input type="hidden" id="provider-<?php echo $hotelAvail->getCode(); ?>" value="<?php echo $hotelAvail->getProvider(); ?>" />
            <input type="hidden" id="price-<?php echo $hotelAvail->getCode(); ?>" value="<?php echo $price; ?>" />
            <input type="hidden" id="currency-<?php echo $hotelAvail->getCode(); ?>" value="<?php echo $hotelAvail->getCurrency(); ?>" />
            <input type="hidden" id="type-<?php echo $hotelAvail->getCode(); ?>" value="<?php echo $groupedRooms[0][0]->getRoomType(); ?>" />
            <input type="hidden" id="board-<?php echo $hotelAvail->getCode(); ?>" value="<?php echo $groupedRooms[0][0]->getBoard(); ?>" />
            <? if ($hotelAvail->getProvider() != BookingProvider::DOMITUR) { // No domitur comparation for the moment ?>
              <input type="checkbox" class="compare-box" value="<?php echo $hotelAvail->getCode(); ?>" onclick="toggleComparePreview()">
            <? } ?>
          </td>
        </tr>
        <?php
      }
      ?>
    </tbody>
  </table>
</div>