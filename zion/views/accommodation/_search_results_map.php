<? $hotels = $rs->getHotelsAvail(); ?>

<style>
  .map { margin: 15px 0; height: 100%; }
  .mapNormalScreen { height: 450px; }
  .mapFullScreen { position: fixed; top: 0; bottom: 0; left: 0; right: 200px; z-index: 10; background-color: #fff; padding-top: 0; }
  #hotelsPreviewFullScreen { display: none; position: fixed; top: 0; bottom: 0; right: 0px; width: 200px; z-index: 10; background-color: #fff; padding-top: 15px; }
  #search-input { font-size: 15px; margin-top: 10px; font-weight: 300; text-overflow: ellipsis; width: 350px; }
  #search-input:focus { border-color: #4d90fe; }
  .highlight { background-color: rgb(255, 255, 154); }
  #preview_table { width: 100%; margin-bottom: 15px; }
  #preview_table th { text-align: left; }
  #preview_table td { text-align: left; padding: 5px; border-top: 1px solid #eaeaea; }

  @media print {
    .hidden-print, .hidden, .invisible { display: none; }
    #preview_table { font-size: 11px; }
  }
</style>

<div class="col-md-12 wbox">
  <div class="text-right" style="margin-top: 5px">
    <div class="link" onclick="printMap()">
      <span class="fa-stack fa-2x">
        <i class="fa fa-circle fa-stack-2x"></i>
        <i class="fa fa-print fa-stack-1x fa-inverse"></i>
      </span>
    </div>
  </div>

  <!-- <div class="wbox" id="filter_information" style="padding: 15px 5px;display:none"> -->
  <div id="filter_information" style="display:none">
    <div class="text-center" style="text-align:center"><h2 id="pr_hotel_destination"></h2></div>
    <div style="padding: 10px;font-size:15px;border: 1px solid #7B7777;border-radius: 5px;margin-bottom: 5px;">
      <table style="width:60%">
        <tr>
          <td>
            <strong>Entrada:&nbsp;</strong>&nbsp;<span ><?php echo date('d/m/Y',strtotime($params['checkin'])); ?></span>&nbsp;
          </td>
          <td>
            <strong>Salida:&nbsp;</strong> &nbsp;<span ><?php echo date('d/m/Y',strtotime($params['checkout'])); ?></span>
            <small id="pr_noches"></small>&nbsp;
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <strong>Ocupaci&oacute;n :</strong>&nbsp;
            <span id="pr_habitaciones"></span>
            <span id="pr_adultos"></span>
            <span id="pr_ninos"></span>
          </td>
        </tr>
      </table>
    </div>
  </div>

  <!-- </div> -->

  <div class="wbox" id="hotelInfo" style="padding: 15px 5px;">
    <table id="preview_table" class="small">
      <tbody>
        <tr>
          <th>N#</th>
          <th>Hotel</th>
          <th>Tipo Hab.</th>
          <th>R&eacute;gimen</th>
          <th>Precio</th>
          <th></th>
          <th></th>
        </tr>
      </tbody>
    </table>
    <div class="showOnlySelected hidden-print" style="padding-right: 15px; text-align: right; display: none;" onclick="showOnlySelectedPreview();"><div class="link"><i class="fa fa-filter"></i> Mostrar s&oacute;lo seleccionados</div></div>
    <div class="showAllItems hidden-print" style="padding-right: 15px; text-align: right; display: none;" onclick="showAllPreview();"><div class="link">Mostrar todos</div></div>
  </div>

  <div id="googlemapview_container" class="mapNormalScreen">
    <input id="search-input" class="controls hidden-print" type="text" placeholder="Buscar un lugar...">
    <div id="googlemapview" class="map"></div>
  </div>

  <div class="link hidden-print" onclick="toggleFullScreen()">Ver en pantalla completa</div>
  <div id="hotelsPreviewFullScreen">
    <div class="link" onclick="toggleFullScreen()"><i class="fa fa-times-circle"></i> Cerrar pantalla completa</div>
    <table id="fullscreen_preview_table" class="table">
      <tbody></tbody>
    </table>
    <div class="showOnlySelected" style="padding-right: 15px; text-align: right; display: none;" onclick="showOnlySelectedPreview();"><div class="link"><i class="fa fa-filter"></i> Mostrar s&oacute;lo seleccionados</div></div>
    <div class="showAllItems" style="padding-right: 15px; text-align: right; display: none;" onclick="showAllPreview();"><div class="link">Mostrar todos</div></div>
  </div>
  <div class="wbox hidden-print" style="margin-top: 30px">
    <input type="text" id="filter_hotels_input" placeholder="Filtrar hoteles..." style="margin: 5px; padding: 5px 8px; width: 350px;" onkeyup="filterHotelsList();">
    <table id="hotels_list" class="table">
      <tbody>
        <tr>
          <th>N#</th>
          <th>Hotel</th>
          <th>Precio desde</th>
          <th></th>
        </tr>
      </tbody>
    </table>
  </div>
</div>

<script type="text/javascript">
  $('#pr_hotel_destination, #pr_noches, #pr_checkin').empty();
  $('#pr_hotel_destination').append($('#destination').val());
  $('#pr_noches').append($('#nights_count').html());
  //$('#pr_checkin').append($('#checkin').val());
  //$('#pr_checkout').append($('#checkout').val());
  habitaciones = $('#habitaciones option:selected').text();
  str_hab = (habitaciones == 1) ? (habitaciones + " Habitaci&oacute;n") : (habitaciones + " Habitaciones");
  $('#pr_habitaciones').append('<span>' + str_hab + '</span>');
  adultos = 0;ninos = 0;
  for(i = 1; i <= habitaciones;i++){
    adultos += parseInt($('#adultos'+i).val());
    ninos += parseInt($('#ninos'+i).val());
  }
  str_adult = (adultos == 1) ? (adultos + " Adulto") : (adultos + " Adultos");
  $('#pr_adultos').append('<span>' + str_adult +'</span>');
  if(ninos != 0) {
    str_ninos = (ninos == 1) ? (ninos + " ni&ntilde;o") : (ninos + " ni&ntilde;os");
    $('#pr_ninos').append(str_ninos);
  }


  var map;
  var circle;
  var markers = [];
  var centerLat = <? echo _get('maxDist') != '' && _get('centerLat') != '' ? _get('centerLat') : $hotels[0]->getLatitude(); ?>;
  var centerLon = <? echo _get('maxDist') != '' && _get('centerLon') != '' ? _get('centerLon') : $hotels[0]->getLongitude(); ?>;
  var maxDist = <? echo _get('maxDist') != '' && _get('centerLat') != '' && _get('centerLon') != '' ? _get('maxDist') : 5; ?>;
  var showItemsFlags = [];
  function initialize() {
    var mapOptions = {
      zoom: 11,
      center: new google.maps.LatLng(centerLat, centerLon)
    };
    map = new google.maps.Map(document.getElementById('googlemapview'), mapOptions);

    radiusMarker = new google.maps.Marker({
      map: map,
      position: new google.maps.LatLng(centerLat, centerLon),
      draggable: true,
      title: 'Puedes arrastrar para buscar hoteles en una nueva area',
      //icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'
      icon: 'http://chart.apis.google.com/chart?chst=d_map_spin&chld=.7|0|1|10|_|R'
    });
    circle = new google.maps.Circle({
      map: map,
      radius: maxDist * 1000,
      strokeOpacity: 0.6,
      strokeWeight: 1,
      fillColor: "#EA7F8C",
      fillOpacity: 0.4
    });
    circle.bindTo('center', radiusMarker, 'position');
    toggleRadiusMarker();

     // Search input box
    searchInput = (document.getElementById('search-input'));
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(searchInput);

    searchBox = new google.maps.places.SearchBox((searchInput));

    google.maps.event.addListener(searchBox, 'places_changed', function() {
      var places = searchBox.getPlaces();
      if (places.length == 0) {
        return;
      }
      place = places[0];
      radiusMarker.setPosition(place.geometry.location);
      $('#showRadius').attr('checked', 'checked');
      toggleRadiusMarker();

      map.setCenter(radiusMarker.getPosition())
    });

    google.maps.event.addListener(map, 'bounds_changed', function() {
      var bounds = map.getBounds();
      searchBox.setBounds(bounds);
    });

    google.maps.event.addListener(radiusMarker, 'dragend', function() { toggleMarkersOutsideRadius(); } );

    initHotels();
  }

  var hotels = [];
  function initHotels() {
    <?
    $pos = 0;
    foreach ($hotels as $hotel) {
      $pos++;
      if ($hotel->getLatitude() == '' || $hotel->getLongitude() == '') continue;
      $groupedRooms = $hotel->getGroupedRooms();
      $hotelLink = getHotelLink($hotel);
      ?>
      hotel = {
        pos: <? echo $pos; ?>,
        link: '<? echo $hotelLink; ?>',
        // img: '<? echo $hotel->getMainImage(); ?>',
        name: '<? echo addslashes($hotel->getName()); ?>',
        category: '<? echo $hotel->getCategoryCode(); ?>',
        price: '<? echo Agency::getUserPrice($hotel->calculateGroupedRoomPrice($groupedRooms[0])); ?>',
        board: '<? echo $groupedRooms[0][0]->getBoard(); ?>',
        roomType: '<? echo htmlspecialchars($groupedRooms[0][0]->getRoomType()); ?>',
        currency: '<? echo $hotel->getCurrency(); ?>',
        lat: '<? echo $hotel->getLatitude(); ?>',
        lon: '<? echo $hotel->getLongitude(); ?>',
      }
      hotels[hotel.pos] = hotel;
      AddHotelToList(hotel);
      addMarker(hotel);
      showItemsFlags[hotel.pos] = false;
    <? } ?>
  }

  function AddHotelToList(hotel) {
    str = '<tr id="hotels-row-' + hotel.pos + '" class="hotels_list_item">';
    str +=  '<td>' + hotel.pos + '</td>';
    str += '<td>' + hotel.name + ' - ' + hotel.category + '</td>';
    str += '<td>' + hotel.price + ' ' + hotel.currency + '</td>';
    str += '<td>'
    if (hotel.lat != '' && hotel.lon != '') {
      str += '<i class="fa fa-plus-circle big text-success" onclick="addHotelPreview(' + hotel.pos + ')" style="cursor: pointer"></i>';
    }
    str += '</td>';
    str += '</tr>';
    $('#hotels_list > tbody:last').append(str);
  }


  function addMarker(hotel) {
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(hotel.lat, hotel.lon),
      map: map,
      title: hotel.name + ' - Desde' + hotel.price + ' ' + hotel.currency,
      icon: 'http://chart.apis.google.com/chart?chst=d_map_spin&chld=.6|0|54ABD5|10|_|' + hotel.pos
    });
    markers[hotel.pos] = marker;
    google.maps.event.addListener(marker, 'click', function() { addHotelPreview(hotel.pos); });
  }

  function toggleMarkersOutsideRadius() {
    for (var i = 0; i < markers.length; i++) {
      if (markers[i] != undefined) {
        var hide = $('#showOnlyWithinRadio').is(':checked')  && $('#showRadius').is(':checked') && !circle.getBounds().contains(markers[i].getPosition());
        markers[i].setVisible(!hide);
      }
    }
  }

  function toggleRadiusMarker() {
    visible = $('#showRadius').is(':checked');
    circle.setVisible(visible);
    radiusMarker.setVisible(visible);

    if ($('#showOnlyWithinRadio').is(':checked')) {
      toggleMarkersOutsideRadius();
    }
  }

  function updateMaxDistRadius() {
    circle.setRadius(parseInt($('#maxDist').val()) * 1000);
    toggleMarkersOutsideRadius();
  }

  function addHotelPreview(pos) {
    hotel = hotels[pos];
    str = '<tr class="hotel_preview_' + hotel.pos + '">';
    str +=  '<td>' + hotel.pos + '</td>';
    str += '<td><span style="color: #3399f3;">' + hotel.name + ' - ' + hotel.category + '</span></td>';
    str += '<td>' + hotel.roomType + '</td>';
    str += '<td>' + hotel.board + '</td>';
    str += '<td>Desde <strong style="white-space: nowrap;">' + hotel.price + ' ' + hotel.currency + '</strong></td>';
    str += '<td class="hidden-print"><a href="' + hotel.link + '" target="_blank" class="btn btn-info btn-xs">Ver Tarifas</a></td>';
    str += '<td class="text-right text-danger big"><i class="fa fa-times-circle" style="cursor: pointer;" onclick="removePreviewItem(' + hotel.pos + ')"></i></td>';
    str += '</tr>';
    $('#preview_table > tbody:last').append(str);

    fullscreen_str = '<tr class="hotel_preview_' + hotel.pos + '"><td>';
    fullscreen_str += '<div class="pull-right text-right text-danger big"><i class="fa fa-times-circle" style="cursor: pointer;" onclick="removePreviewItem(' + hotel.pos + ')"></i></div>';
    fullscreen_str += '<div><span style="color: #3399f3;">' + hotel.name + '</span></div>';
    fullscreen_str += '<div>Desde <strong>' + hotel.price + ' ' + hotel.currency + '</strong></div>';
    fullscreen_str += '<div><a href="' + hotel.link + '" target="_blank" class="btn btn-info btn-xs">Ver Tarifas</a></div>';
    fullscreen_str += '</td></tr>';
    $('#fullscreen_preview_table > tbody:last').append(fullscreen_str);

    showItemsFlags[pos] = true;
    $('#hotels-row-' + pos).addClass('highlight');

    if (isShowOnlySelectedEnabled()) {
      showOnlySelectedPreview();
    }
    else {
      $('.showOnlySelected').show();
    }
  }

  function toggleFullScreen() {
    if ($('#googlemapview_container').hasClass('mapNormalScreen')) {
      $('#googlemapview_container').removeClass('mapNormalScreen');
      $('#googlemapview_container').addClass('mapFullScreen');
      $('#hotelsPreviewFullScreen').show();
    }
    else {
      $('#googlemapview_container').addClass('mapNormalScreen');
      $('#googlemapview_container').removeClass('mapFullScreen');
      $('#hotelsPreviewFullScreen').hide();
    }
    google.maps.event.trigger(map, "resize");
  }

  function showOnlySelectedPreview() {
    for (var i = 0; i < markers.length; i++) {
      if (markers[i] != undefined) {
        markers[i].setVisible(showItemsFlags[i]);
      }
    }
    $('.showAllItems').show();
    $('.showOnlySelected').hide();
  }

  function isShowOnlySelectedEnabled() {
    return $('.showAllItems').is(':visible')
  }

  function showAllPreview() {
    toggleMarkersOutsideRadius();
    $('.showAllItems').hide();
    $('.showOnlySelected').show();
  }

  function removePreviewItem(i) {
    $('.hotel_preview_' + i).remove();
    showItemsFlags[i] = false;
    $('#hotels-row-' + i).removeClass('highlight');

    if (isShowOnlySelectedEnabled()) {
      showOnlySelectedPreview();
    }
  }

  function filterHotelsList() {
    var search = $('#filter_hotels_input').val().toLowerCase();
    for (var i = 1; i < hotels.length; i++) {
      if (search == '' || hotels[i].name.toLowerCase().indexOf(search) > -1) {
        $('#hotels-row-' + i).show();
      }
      else {
        $('#hotels-row-' + i).hide();
      }
    }
  }

  function printMap() {
    // Hide no useful stuff
    if($('.showAllItems').is(':visible')) {
      showOnlySelectedPreview();
    }
    else {
      showAllPreview();
    }
    $('#header').hide();
    $('footer').hide();
    $('#resultbar').hide();
    $('#refine-form').hide();
    $('#jivo_top_wrap').hide();
    $('#filter_information').show();
    hilight_css = { "border":"1px",
                    "border-style":"solid",
                    "border-color":"#7B7777",
                    "border-radius":"5px"};
    $("#hotelInfo").css(hilight_css);
    //$('#hotelInfo').css({"border":"1px solid #7B7777","border-radius":"5px","margin-bottom","5px"});
    $('#ui-datepicker-div').addClass('hidden-print');
    window.print();

    // Restore
    $('#header').show();
    $('footer').show();
    $('#resultbar').show();
    $('#refine-form').show();
    $('#jivo_top_wrap').show();
    $('#filter_information').hide();
    hilight_css = { "border":"none",
                    "border-radius":"5px"};
    $("#hotelInfo").css(hilight_css);
    $('#ui-datepicker-div').removeClass('hidden-print');
  }

  var ready = function() {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&callback=initialize';
    document.body.appendChild(script);
  };
  $(document).ready(ready);

  // google.maps.event.addDomListener(window, 'load', initialize);
</script>
