<div id="search_form">
  <form action="mia.php" method="get">
    <fieldset>
      <h2><i class="fa fa-home"></i> Buscar Alojamientos</h2>
      <!-- <div id="where" class="form-group col-md-12">
        <label for="destination">D&oacute;nde quieres ir?</label>
        <input type="text" class="form-control" id="destination" name="destination" placeholder="Introduce tu destino...">
        <input type="hidden" id="destination_code" name="destination_code" />
      </div> -->

      <div id="where" class="form-group col-md-12">
        <label for="destination">D&oacute;nde quieres ir?</label>
        <? include('_destination_search_form.php'); ?>
      </div>
      <!--<div class="form-group col-md-5">
        <label for="zones">Zona</label> <small><em>(opcional)</em></small>
        <select class="form-control" id="zones" name="zones" placeholder="Destino, zona o nombre de hotel">
          <option></option>
        </select>
      </div>-->

      <div class="form-group col-md-5">
        <label for="checkin">Fecha de entrada</label>
        <input type="text" class="form-control" id="checkin" name="checkin" placeholder="dd/mm/aa" autocomplete="off" required>
      </div>

      <div class="form-group col-md-5">
        <label for="checkout">Fecha de salida</label>
        <input type="text" class="form-control" id="checkout" name="checkout" placeholder="dd/mm/aa" autocomplete="off" required>
      </div>

      <div id="nights_count" class="form-group col-md-2"></div>

      <div class="form-group col-md-3">
        <label for="habitaciones">Habitaciones</label>
        <select class="form-control" id="habitaciones" name="habitaciones" onchange="updateRoomsConfigPanel(); showChildAgesConfig()">
          <option selected="selected">1</option><option>2</option><option>3</option><option>4</option><option>5</option>
          <option>6</option><option>7</option><option>8</option><option>9</option><option>10</option>
        </select>
      </div>
      <div id="rooms_config" class="form-group col-md-9">
        <div class="row">
          <div class="col-md-4 col-md-offset-4">
            <label>Adultos</label>
          </div>
          <div class="col-md-4">
            <label>Ni&ntilde;os</label>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 habitacion_count">&nbsp;</div>
          <div class="col-md-4">
            <select class="form-control" id="adultos1" name="adultos1">
              <option>1</option><option selected="selected">2</option><option>3</option><option>4</option>
              <option>5</option><option>6</option><option>7</option><option>8</option>
              <option>9</option><option>10</option><option>11</option><option>12</option>
            </select>
          </div>
          <div class="col-md-4">
            <select class="form-control" id="ninos1" name="ninos1" onchange="showChildAgesConfig()">
              <option>0</option><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option>
            </select>
          </div>
        </div>
      </div>

      <div id="child_ages" class="form-group col-md-12"></div>

      <div class="form-group">
        <input type="hidden" id="orderPrice" name="orderPrice" value="ASC">
        <input type="hidden" id="view_mode" name="view_mode" value="resume">
        <input type="hidden" id="page" name="page" value="1">
        <input type="hidden" id="zoneCode" name="zoneCode" value="">
        <input type="hidden" name="controller" value="accommodation" />
        <input type="hidden" name="action" value="search_ajax" />
        <div class="col-md-8">
          <div class="pull-left">
            <input type="radio" name="search" id="search_fast" style="height: 13px;" checked> B&uacute;squeda r&aacute;pida
          </div>
          <div class="pull-left" style="margin-left: 15px;">
            <input type="radio" name="search" id="search_all" style="height: 13px;"> Todos los Proveedores
          </div>
        </div>

        <div class="col-md-4 text-right">
          <input type="submit" class="btn btn-info-css btn-lg loadingAction" value="Buscar" />
        </div>

      </div>

      <div id="advanced_options_link" class="col-md-12" onclick="$(this).hide(); $('#advanced_options').slideDown();">
        Mostrar opciones avanzadas &nbsp; <i class="fa fa-caret-down"></i>
      </div>
      <!--  -->
      <div id="advanced_options">
        <div class="col-md-12 col-xs-12">
          <div class="row">
            <div class="form-group col-md-6">
              <label>Nombre hotel</label>
              <input type="text" class="form-control" id="hotelName" name="hotelName">
            </div>
            <div class="form-group col-md-6">
              <label>Zona</label>
              <select class="form-control" id="zones" name="zoneCode" placeholder="Destino, zona o nombre de hotel">
              <option></option>
              </select>
              <!-- <span id="zones"></span> -->
            </div>
          </div>
        </div>

        <div class="col-md-12 col-xs-12">
          <div class="row">
            <div class="form-group col-md-12">

              <div class="row">

                <div class="col-md-2" style="width:11%">
                  <label style="font-size:15px">Vista</label>
                </div>

                <div class="col-md-2" style="width:24%">
                  <span style=""><input type="radio" name="view_mode" value="resume" style="height: 13px;" checked> <i class="fa fa-th-list" style="font-size: 15px;"></i></span>&nbsp;<span style="font-size:12px">Completo</span>
                </div>
                <div class="col-md-2" style="width:19%">
                  <span style=""><input type="radio" name="view_mode" value="list" style="height: 13px;"> <i class="fa fa-align-justify" style="font-size: 15px;"></i></span>&nbsp;<span style="font-size:12px">Lista</span>
                </div>
                <div class="col-md-2" style="width:24%">
                  <span style=""><input type="radio" name="view_mode" value="photos" style="height: 13px;"> <i class="fa fa-th" style="font-size: 15px;"></i></span>&nbsp;<span style="font-size:12px">Columna</span>
                </div>
                <!--<div class="col-md-2" style="width:19%">
                  <span style=""><input type="radio" name="view_mode" value="map" style="height: 13px;"> <i class="fa fa-map-marker" style="font-size: 15px;"></i></span>&nbsp;<span style="font-size:12px">Mapa</span>
                </div> -->
              </div>
            </div>

            <div class="form-group col-md-12">
              <label style="font-size:15px">R&eacute;gimen</label>
              <div class="row">
                <div class="col-md-4">
                  <div class="" style="margin-top:5px;margin-bottom:5px">
                      <input type="checkbox" name="regimen[]" value="FB" onclick="$('#refine-form').submit();" > <span style="font-size:12px">Pensi&oacute;n Completa<span>
                  </div>
                  <div class="" style="margin-top:5px;margin-bottom:5px">
                      <input type="checkbox" name="regimen[]" value="AI,TI" onclick="$('#refine-form').submit();" > <span style="font-size:12px">Todo Incluido<span>
                  </div>
                </div>

                <div class="col-md-4" style="width:28%">
                  <div class="" style="margin-top:5px;margin-bottom:5px">
                    <input type="checkbox" name="regimen[]" value="HB,MP" onclick="$('#refine-form').submit();" > <span style="font-size:12px">Media Pensi&oacute;n<span>
                  </div>

                  <div class="" style="margin-top:5px;margin-bottom:5px">
                      <input type="checkbox" name="regimen[]" value="RO,SA" onclick="$('#refine-form').submit();" > <span style="font-size:12px">Solo Habitaci&oacute;n<span>
                  </div>
                </div>

                <div class="col-md-4" style="width:36%">
                  <div class="" style="margin-top:5px;margin-bottom:5px">
                    <input type="checkbox" name="regimen[]" value="BB,AD" onclick="$('#refine-form').submit();" > <span style="font-size:12px">Alojamiento y Desayuno<span>
                  </div>
                </div>

              </div>
            </div>

            <div class="form-group col-md-12">
                <label style="font-size:15px">Estrellas</label>
                <div class="row">
                  <div class="col-md-2">
                    <input type="checkbox" name="star[]" value="'1EST','1LL','APTH','H1_5','HR','HS','HSR1'">&nbsp;<span>1 Est.</span>
                  </div>
                  <div class="col-md-2">
                    <input type="checkbox" name="star[]" value="'2EST','2LL','APTH2','H2S','H2_5','HR2','HS2','HSR2'">&nbsp;<span>2 Est.</span>
                  </div>
                  <div class="col-md-2">
                    <input type="checkbox" name="star[]" value="'3EST','3LL','APTH3','BB3','H3S','H3_5','HR3','HS3'">&nbsp;<span>3 Est.</span>
                  </div>
                  <div class="col-md-2">
                    <input type="checkbox" name="star[]" value="'4EST','4LL','4LUX','APTH4','BB4','H4_5','HR4','HS4','SUP'">&nbsp;<span>4 Est.</span>
                  </div>
                  <div class="col-md-2">
                    <input type="checkbox" name="star[]" value="'5EST','5EST','5LL','5LUX','APTH5','BB5','H5_5','HR5','HRS','HS5'">&nbsp;<span>5 Est.</span>
                  </div>
                </div>
            </div>

            <? if ($_SESSION['admin_role']) { ?>
              <!--<div class="form-group col-md-12">
                <label style="font-size:15px">Proveedores</label>
                <div class="">
                  <input type="checkbox" name="providers[]" value="<?php //echo BookingProvider::HOTELBEDS; ?>" id="p1" checked>
                  <span id="provider_<?php //echo BookingProvider::HOTELBEDS; ?>">Hotelbeds</span><br />
                </div>
                <div class="checkbox">
                  <input type="checkbox" name="providers[]" value="<?php //echo BookingProvider::DOMITUR; ?>" id="p2" checked>
                  <span id="provider_<?php //echo BookingProvider::DOMITUR; ?>">Domitur</span><br />
                </div>
                <div class="checkbox">
                  <input type="checkbox" name="providers[]" value="<?php //echo BookingProvider::METHABOOK; ?>" id="p3">
                  <span id="provider_<?php //echo BookingProvider::METHABOOK; ?>">Methabook</span><br />
                </div>
              </div>-->
            <? } else { ?>
                <!-- <input type="hidden" name="providers[]" value="<?php //echo BookingProvider::HOTELBEDS; ?>" id="p1" />
                <input type="hidden" name="providers[]" value="<?php //echo BookingProvider::DOMITUR; ?>" id="p2" /> -->
                <input type="hidden" name="providers[]" value="<?php echo BookingProvider::TOURICO; ?>" id="p1" />
                <input type="hidden" name="providers[]" value="" id="p3" />
            <? } ?>

          </div>
        </div>

      </div>
    </fieldset>
  </form>
</div>

<!-- Raw base for the rooms config -->
<div id="rooms_config_raw" class="hidden invisible">
  <div class="row">
    <div class="col-md-4 col-md-offset-4">
      <select class="form-control" id="adultos__X" name="adultos__X">
        <option>1</option><option selected="selected">2</option><option>3</option><option>4</option>
        <option>5</option><option>6</option><option>7</option><option>8</option>
      </select>
    </div>
    <div class="col-md-4">
      <select class="form-control" id="ninos__X" name="ninos__X" onchange="showChildAgesConfig()">
        <option>0</option><option>1</option><option>2</option><option>3</option><option>4</option>
      </select>
    </div>
  </div>
</div>
<div id="rooms_config_header_raw" class="hidden invisible">
  <div class="row">
    <div class="col-md-4 col-md-offset-4">
      <label>Adultos</label>
    </div>
    <div class="col-md-4">
      <label>Ni&ntilde;os</label>
    </div>
  </div>
</div>

<script type="text/javascript">
  // Restore values
  $(function() {
    var destination_code = "<?php echo isset($_SESSION['destination_code']) ? $_SESSION['destination_code'] : ''; ?>";
    var destination = "<?php echo isset($_SESSION['destination']) ? $_SESSION['destination'] : ''; ?>";
    var checkin = "<?php echo isset($_SESSION['checkin']) ? $_SESSION['checkin'] : ''; ?>";
    var checkout = "<?php echo isset($_SESSION['checkout']) ? $_SESSION['checkout'] : ''; ?>";
    $('#destination_code').val(destination_code);
    $('#destination').val(destination);
    $('#checkin').val(checkin);
    $('#checkout').val(checkout);
    $('#habitaciones').val(1);
    $('#adultos1').val(2);
    $('#ninos1').val(0);
    $('#search_fast').click(function(){
      <? if ($_SESSION['admin_role']) { ?>
          $('#p1, #p2').prop('checked', true);
          $('#p3').prop('checked', false);
      <? }else{ ?>
          $('#p1').val('<?php echo BookingProvider::HOTELBEDS; ?>');
          $('#p2').val('<?php echo BookingProvider::DOMITUR; ?>');
          $('#p3').val('');
      <? } ?>
    });
    $('#search_all').click(function(){
      <? if ($_SESSION['admin_role']) { ?>
          $('#p1, #p2, #p3').prop('checked', true);
      <? }else{ ?>
          $('#p1').val('<?php echo BookingProvider::HOTELBEDS; ?>');
          $('#p2').val('<?php echo BookingProvider::DOMITUR; ?>');
          $('#p3').val('<?php echo BookingProvider::METHABOOK; ?>');
      <? } ?>
    });
  });
</script>
