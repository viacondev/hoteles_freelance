<?php
//ES PARTE DE LA INTEGRACION CON COTIZACION
include(__DIR__ . '../Pqt_Integracion/ViaconMapping.php');
include(__DIR__ . '../Pqt_Integracion/integration_function.php');
$user_viacon = user_cotizacion::getViaconid($_SESSION['current_user']->id);
//FIN DE LA INTEGRACION CON COTIZACION -->

$hotelCodes = array();
$hotelCount = 0;
$providersCount = array();
$hotels_map     = $rs->getHotelsAvail();
foreach ($rs->getHotelsAvail() as $hotelAvail) {
  // TODO: Temporary avoid showing repeated hotels. We should merge rooms insted of ignoring.
  if (in_array($hotelAvail->getCode(), $hotelCodes)) {
    //continue;
  }
  $hotelCodes[] = $hotelAvail->getCode();

  $hotelCount++;
  $groupedRooms = $hotelAvail->getGroupedRooms();
  $hotelLink = getHotelLink($hotelAvail);

  if (!isset($providersCount[$hotelAvail->getProvider()])) {
    $providersCount[$hotelAvail->getProvider()] = 0;
  }
  $providersCount[$hotelAvail->getProvider()]++;
  // Group the hotels in groups of 20 to allow show them in groups
  $hotelGroup = intval($hotelCount / 20);
  // Grouping show currently disabled...
  // $groupDisplay = $hotelGroup == 0 ? 'block' : 'none';
  $groupDisplay = 'block';
  ?>

  <div class="hotel_avail item_avail hotel_group_<? echo $rs->getCurrentPage() . '_' . $hotelGroup; ?> hotel-li-<? echo $hotelAvail->getCode();  ?>"  style="display: <? echo $groupDisplay; ?>">
    <div class="item_img col-md-3">
      <img src="<?php echo $hotelAvail->getMainImage(); ?>" id="hotel-image-<?php echo $hotelAvail->getCode(); ?>" />
    </div>
    <div class="col-md-7 item_info">
      <div class="row">
        <div class="col-md-9">
          <div class="row">
            <div class="col-md-12">
              <h3>
                <a href="<?php echo $hotelLink; ?>" target="_blank" id="hotel-link-<? echo $hotelAvail->getCode(); ?>">
                  <?php echo $hotelAvail->getName() ?>
                </a>
              </h3>
            </div>
          </div>
        </div>
        <div class="hotel-category col-md-3 hotel_category_<? echo $hotelAvail->getCode(); ?>"><!-- PARA TOMAS EL HTML DE LAS ESTRELLAS EN COTIZACION -->
          <span data-toggle="tooltip" data-placement="top" data-container="body" data-original-title="<?php echo $hotelAvail->getCategory(); ?>">
            <?php showCategory($hotelAvail->getCategoryCode()); ?>
          </span>
        </div>
        <div class="col-md-12">
          <i class="fa fa-map-marker"></i> <?php echo $hotelAvail->getLocation(); ?>
           - <small><a href="<?php echo $hotelLink; ?>#googlemap" class="loadingAction">Ver mapa</a></small>
        </div>

        <div class="col-md-12 desc">
          <p>
            <?php echo substr($hotelAvail->getDescription(), 0, 120); ?>...
            <a href="<? echo $hotelLink; ?>" class="loadingAction">Ver m&aacute;s</a>
          </p>
        </div>

        <div class="col-md-12 small room-info">
          <strong>Habitaci&oacute;n: </strong>
          <?php
          echo $groupedRooms[0][0]->getRoomType();
          for ($i = 1; $i < count($groupedRooms[0]); $i++) {
            echo '; ' . $groupedRooms[0][$i]->getRoomType();
          }
          ?> &nbsp; - &nbsp;
          <strong>Servicios: </strong>
          <?php echo $groupedRooms[0][0]->getBoard(); ?>

          <input type="hidden" id="type-<?php echo $hotelAvail->getCode(); ?>" value="<?php echo $groupedRooms[0][0]->getRoomType(); ?>" />
          <input type="hidden" id="board-<?php echo $hotelAvail->getCode(); ?>" value="<?php echo $groupedRooms[0][0]->getBoard(); ?>" />
        </div>

        <div class="bottom-info col-md-12 small">
          <div class="row">
            <div class="compare hidden-xs col-md-4 col-xs-3" onclick="">
              <? if ($hotelAvail->getProvider() != BookingProvider::DOMITUR) { // No domitur comparation for the moment ?>
                <input type="checkbox" class="compare-box" value="<?php echo $hotelAvail->getCode(); ?>" onclick="toggleComparePreview()" style="margin-top: 0;" /> Comparar<br>
              <? } ?>
              <? if($user_viacon) { ?><!-- ES PARTE DE LA INTEGRACION CON COTIZACION -->
                <input type="checkbox" class="cotizar-check" id="main_hotel_<?php echo $hotelAvail->getCode(); ?>" value="<?php echo $hotelAvail->getCode(); ?>" onchange="enable_cotizacion('<?php echo $hotelAvail->getCode(); ?>');habilitar_marcador('<?php echo $hotelAvail->getCode(); ?>');" style="margin-top: 0;" /> Cotizar
              <? } ?> <!-- FIN DE LA INTEGRACION CON COTIZACION -->
            </div>

            <div class="col-md-3 col-xs-3">
            <? if($hotelAvail->getLatitude() != '' && $hotelAvail->getLongitude() != '') { ?>
                  <span class="search_near_hotels" onclick="showDistanceSearchModal('<? echo addslashes($hotelAvail->getName()); ?>', '<? echo $hotelAvail->getLatitude(); ?>', '<? echo $hotelAvail->getLongitude(); ?>');">
                    <i class="fa fa-map-marker"></i> Buscar cercanos
                  </span>
            <? } ?>
            </div>

            <div class="col-md-5 col-xs-6 view-extra-rooms" onclick="$('#extra-rooms-<?php echo $hotelAvail->getCode() . '_' . $hotelCount; ?>').slideToggle();">
              Ver todas las habitaciones (<? echo count($groupedRooms); ?>) <i class="fa fa-angle-down"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="book col-md-2">
      <div class="provider">
        <? if ($_SESSION['admin_role']) {
          echo '<i class="fa fa-folder-open-o"></i> ' . BookingProvider::getProviderName($groupedRooms[0][0]->getProvider())."<br>";
          $provide_see = array($groupedRooms[0][0]->getProvider());
          foreach ($groupedRooms as $rooms) {
              if(!in_array($rooms[0]->getProvider(), $provide_see)) {
                echo "<span style='font-size:10px'>+".BookingProvider::getProviderName($rooms[0]->getProvider())."</span><br>";
                $provide_see[] = $rooms[0]->getProvider();
              }
          }
        } ?>
      </div>
      <div class="price">
        <?php
        $price = 0;
        foreach ($groupedRooms[0] as $room) {
          $price += floatval($room->getPrice());
        }
        $price = Agency::getUserPrice($price);
        // $decimal = round($price - floor($price), 2) * 100;
        $arr_number = explode(".", $price);
        $decimal = (isset($arr_number[1])) ? $arr_number[1] : '0';
        echo floor($price);
        ?><span class="price_decimal"><?php echo '.' . $decimal; ?></span>
        <span class="currency"><?php echo $hotelAvail->getCurrency(); ?></span>
        <?php
        $from = new DateTime($hotelAvail->getDateFrom());
        $to = new DateTime($hotelAvail->getDateTo());
        $nights = $to->diff($from)->days;
        $priceAverage = round($price / $nights, 2);
        ?>
        <div class="price_average"><?php echo $priceAverage; ?><span>/noche</span></div>

        <input type="hidden" id="provider-<?php echo $hotelAvail->getCode(); ?>" value="<?php echo $hotelAvail->getProvider(); ?>" />
        <input type="hidden" id="currency-<?php echo $hotelAvail->getCode(); ?>" value="<?php echo $hotelAvail->getCurrency(); ?>" />
        <input type="hidden" id="price-<?php echo $hotelAvail->getCode(); ?>" value="<?php echo $price; ?>" />
      </div>
      <div class="book-btn">
        <form method="get">
          <input type="hidden" name="service_add_data" value='<?php echo getServiceAddDataJSON($groupedRooms[0], $childAges); ?>' />
          <input type="hidden" name="requestCode" value="<?php echo mt_rand(); ?>" />
          <input type="hidden" name="controller" value="shopping_cart" />
          <input type="hidden" name="action" value="show" />
          <button class="loadingAction">RESERVAR <i class="fa fa-angle-right"></i></button>
        </form>
      </div>
    </div>

    <div id="extra-rooms-<?php echo $hotelAvail->getCode() . '_' . $hotelCount; ?>" class="extra-rooms col-md-10">
      <table class="table table-striped table-hover small">
        <thead>
          <tr>
            <th>Tipo Habitaci&oacute;n</th>
            <th>Servicios</th>
            <th>Precio</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <?php
          $cont = 0;// ES PARTE DE LA INTEGRACION CON COTIZACION
          foreach ($groupedRooms as $key => $group) {
            $roomTypeStr = '';
            $totalPrice = 0;
            $pricesStr = '';
            $first = true;
            $cont++;//ES PARTE DE LA INTEGRACION CON COTIZACION
            foreach ($group as $room) {
              if (!$first) {
                $roomTypeStr .= '<br />';
              }
              $roomPrice = Agency::getUserPrice($room->getPrice());
              $pricesStr .= '<br /><span class="small">' . round($roomPrice, 2) . ' ' . $hotelAvail->getCurrency() . '</span>';
              $roomTypeStr .= $room->getRoomType();
              if ($room->getRoomCount() > 1) {
                $roomTypeStr .= ' x ' . $room->getRoomCount();
              }
              $first = false;

              $totalPrice += $room->getPrice();
            }
            $totalPrice = Agency::getUserPrice($totalPrice);
            $priceStr = '<span class="big strong">' . round($totalPrice, 2) . ' ' . $hotelAvail->getCurrency() . '</span>';
            $priceStr .= count($group) > 1 ? $pricesStr : '';
            $boardStr = $group[0]->getBoard();
            ?>
            <tr>
              <td>
                <?  if($user_viacon) { ?><!-- ES PARTE DE LA INTEGRACION CON COTIZACION -->
                      <span>
                        <input type="checkbox" id="<? echo $hotelAvail->getCode().'_'.$cont; ?>" class="list_check_<? echo $hotelAvail->getCode(); ?>" onclick="verify_check('<? echo $hotelAvail->getCode(); ?>', '<? echo $hotelAvail->getCode().'_'.$cont; ?>');" value='<?php echo getServiceAddDataJSON_c($group, $childAges); ?>' style="display:none;float: left;" />
                      </span>
                <?  } ?><!-- fin INTEGRACION CON COTIZACION -->
                <?
                $provider = '';
                if ($_SESSION['admin_role']) {
                  $provider = '<span class="text-muted small">(' . BookingProvider::getProviderName($group[0]->getProvider()) . ') </span>';
                }
                echo count($group) > 1 ? '<div class="strong" style="margin-bottom:8px;">Habitaciones: ' . $provider . '</div>' : $provider;
                echo $roomTypeStr;
                ?>
              </td>
              <td><?php echo $boardStr; ?></td>
              <td><?php echo $priceStr; ?></td>
              <td>
                <form method="get">
                  <input type="hidden" name="service_add_data" value='<?php echo getServiceAddDataJSON($group, $childAges); ?>' />
                  <input type="hidden" name="requestCode" value="<?php echo mt_rand(); ?>" />
                  <input type="hidden" name="controller" value="shopping_cart" />
                  <input type="hidden" name="action" value="show" />
                  <button class="loadingAction">RESERVAR <i class="fa fa-angle-right"></i></button>
                </form>
              </td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>

    <div class="clearfix"></div>
  </div>
  <?
} ?>
<!-- ES PARTE DE LA INTEGRACION CON COTIZACION -->
<input type="hidden" name="id_user_viacon" id="id_user_viacon" value="<? echo $user_viacon; ?>">
<!-- FIN -->
<!--<div id="searchNearHotels" class="modal modal-dialog-center">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <label>Hoteles</label>
      </div>
      <div class="modal-body" style="height: 200px;">
        <div class="service-title">
          <i class="fa fa-map-marker"></i> Buscar hoteles cerca de '<span id="nearFromHotelName"></span>'
        </div>
        <div style="padding: 20px;">
          Distancia m&aacute;xima:
          <input type="number" id="searchNearHotelsMaxDist" value="5" style="width: 50px;"> Km.
          <div><button class="btn btn-sm btn-info" onclick="showHotelsByDistance()">Buscar</button></div>
        </div>
      </div>
    </div>
  </div>
</div> -->
<? include('_maps.php'); ?>

<script type="text/javascript">
  // function showDistanceSearchModal(title, lat, lon) {
  //   $('#nearFromHotelName').html(title);
  //   $('#centerLat').val(lat);
  //   $('#centerLon').val(lon);
  //   $('#showRadio').val('1');
  //   $('#searchNearHotels').modal();
  // }


  <? // Update the results count by provider
  foreach ($providersCount as $key => $value) {
    echo "$('#provider_$key').append(' ($value)');";
  }
  ?>
</script>

<script type="text/javascript" src="views/Pqt_Integracion/integration_functions.js">
</script>
