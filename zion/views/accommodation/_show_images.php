<div class="row">
  <div class="col-md-12" id="slider">
    <div id="hotel-images" class="carousel slide">
      <!-- main slider carousel items -->
      <div class="carousel-inner">
        <?php
        $images = $detail->imageList;
        for ($i = 0; $i < count($images); $i++) {
          $class = $i == 0 ? 'active item' : 'item avoid-mailing';
          $url = $images[$i];
          echo "<div class='$class' data-slide-number='$i'><img src='$url' class='img-big'></div>";
        }
        ?>
      </div>
      <!-- main slider carousel nav controls -->
      <a class="carousel-control left hidden-md hidden-lg avoid-mailing" href="#hotel-images" data-slide="prev">‹</a>
      <a class="carousel-control right hidden-md hidden-lg avoid-mailing" href="#hotel-images" data-slide="next">›</a>
    </div>
  </div>
  <!-- thumb navigation carousel -->
  <div class="col-md-12 hidden-print avoid-mailing" id="slider-thumbs">
    <!-- thumb navigation carousel items -->
    <ul class="list-inline">
      <?php
      $images = $detail->imageList;
      for ($i = 0; $i < count($images); $i++) {
        $class = $i == 0 ? 'active item' : 'item';
        $url = $images[$i];
        echo "<li class='col-md-3'><a id='carousel-selector-$i'><img src='$url' class='img-thumb'></a></li>";
      }
      ?>
    </ul>
  </div>
</div>