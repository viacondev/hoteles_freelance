<?php
global $rs, $params, $childAges;
// include('views/Pqt_Integracion/ViaconMapping.php');
// include('views/Pqt_Integracion/integration_function.php');
// echo __DIR__;
include(__DIR__ . '/../Pqt_Integracion/ViaconMapping.php');
include(__DIR__ . '/../Pqt_Integracion/integration_function.php');
$user_viacon = user_cotizacion::getViaconid($_SESSION['current_user']->id);

?>
<!-- <link href="assets/css/accommodation.css?201709100459" rel="stylesheet" media="screen"> -->

<input type="hidden" name="id_user_viacon" id="id_user_viacon" value="<?php echo $user_viacon; ?>">
<input type="hidden" name="id_agencia" id="id_agencia" value="<?php echo $_SESSION['agency_id']; ?>">
<div id="accomodation" class="content">
  <div class="container">

    <div class="call-action call-action-boxed call-action-style2 clearfix">
      <div class="row">
        <div class="col-sm-9">
          <h2 class="primary"><strong class="origen_main"></strong> <?php echo $_GET['destination']; ?> - <span class="ida_main"><?php echo $_GET['checkin']; ?></span> al <span class="retorno_main"><?php echo $_GET['checkout']; ?></span></h2>
          <p>
            Ocupaci&oacute;n :
            <span class="ocupacion_main">
              <?php
              $roomsCount = intval($_GET['habitaciones']);
              $adultCount = 0;
              $childCount = 0;
              for ($i = 1; $i <= $roomsCount; $i++) {
                $adultCount += intval($_GET['adultos' . $i]);
                $childCount += intval($_GET['ninos' . $i]);
              }
              echo $adultCount . ' adultos';
              if ($childCount > 0) {
                echo ' - ' . $childCount . ' ni&ntilde;os';
              }
              echo ' (' . $roomsCount;
              echo $roomsCount == 1 ? ' habitaci&oacute;n)' : ' habitaciones)';
              ?>
            </span>
          </p>
        </div>
        <div class="col-sm-3">
          <div class="button-side" style="margin-top:8px;">
            <a href="#" class="btn-system btn-small" onclick="$('#modal-nueva-busqueda').modal('show');"><i class="fa fa-search"></i> Modificar B&uacute;squeda</a>
          </div>
        </div>
      </div>
    </div>

    <div class="mini-hidden-separator"></div>

    <div class="row">
      <div class="col-md-3"><?php include('_search_options.php'); ?></div>
      <div class="col-md-9">

        <div id="contentNoResult" class="col-md-12 wbox" style="padding: 30px 20px; margin-bottom: 20px;display:none;">
          Lo sentimos, no se encontr&oacute; ning&uacute;n hotel para tu b&uacute;squeda.
        </div>

        <div class="hotelLoadingRes hotel_avail item_avail" style="background-color: transparent;display:none;">
          <div class="item_img col-md-3 animated-background" style="height:200px;border-bottom-left-radius:15px;border-top-left-radius:15px;">
          </div>
          <div class="col-md-9 item_info" style="z-index:1;border-bottom-right-radius:15px;border-top-right-radius:15px;">
            <div class="animated-background" style="height:172px;margin-top: 9px;">
              <div class="background-masker primero"></div>
              <div class="background-masker primero_blanco"></div>
              <div class="background-masker "></div>
              <div class="background-masker segundo_blanco"></div>
              <div class="background-masker tercer"></div>
              <div class="background-masker tercer_blanco"></div>
              <div class="background-masker cuarto"></div>
              <div class="background-masker cuarto_blanco"></div>
              <div class="background-masker quinto"></div>
              <div class="background-masker quinto_blanco"></div>
              <div class="background-masker sexto"></div>
              <div class="background-masker sexto_blanco"></div>
              <div class="background-masker "></div>
            </div>
          </div>
        </div>

        <div id="hotel-avail-res" class="hotel-avail-res">
          <div class="hotel_avail item_avail" style="background-color: transparent;">
            <div class="item_img col-md-3 animated-background" style="height:200px;border-bottom-left-radius:15px;border-top-left-radius:15px;">
            </div>
            <div class="col-md-9 item_info" style="z-index:1;border-bottom-right-radius:15px;border-top-right-radius:15px;">
              <div class="animated-background" style="height:172px;margin-top: 9px;">
                <div class="background-masker primero"></div>
                <div class="background-masker primero_blanco"></div>
                <div class="background-masker "></div>
                <div class="background-masker segundo_blanco"></div>
                <div class="background-masker tercer"></div>
                <div class="background-masker tercer_blanco"></div>
                <div class="background-masker cuarto"></div>
                <div class="background-masker cuarto_blanco"></div>
                <div class="background-masker quinto"></div>
                <div class="background-masker quinto_blanco"></div>
                <div class="background-masker sexto"></div>
                <div class="background-masker sexto_blanco"></div>
                <div class="background-masker "></div>
              </div>
            </div>
          </div>
        </div>

        <div id="content-paginate" class="content-paginate">
          <nav aria-label="Page navigation example">
            <ul class="pagination">

            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="compare-preview">
  <div class="close" onclick="$('#compare-preview').hide('drop', {direction: 'right'});">
    <i class="fa fa-times"></i>
  </div>
  <div id="compare-item-1" class="compare-item">
    <?php loadImage('no-item.png'); ?>
    <div id="del_1" style="display: none" onclick="deleteItemCompare(1)">
      <i class="fa fa-minus-circle btn_compare_del" title="Eliminar Item"></i>
    </div>
  </div>
  <div id="compare-item-2" class="compare-item">
    <?php loadImage('no-item.png'); ?>
    <div id="del_2" style="display: none" onclick="deleteItemCompare(2)">
      <i class="fa fa-minus-circle btn_compare_del" title="Eliminar Item"></i>
    </div>
  </div>
  <div id="compare-item-3" class="compare-item">
    <?php loadImage('no-item.png'); ?>
    <div id="del_3" style="display: none" onclick="deleteItemCompare(3)">
      <i class="fa fa-minus-circle btn_compare_del" title="Eliminar Item"></i>
    </div>
  </div>
  <div id="compare-item-4" class="compare-item">
    <?php loadImage('no-item.png'); ?>
    <div id="del_4" style="display: none" onclick="deleteItemCompare(4)">
      <i class="fa fa-minus-circle btn_compare_del" title="Eliminar Item"></i>
    </div>
  </div>
  <button type="button" class="btn btn-success btn-lg" onclick="showComparation()">Comparar</button>
</div>

<div id="new_cotizacion">
  <!--<span id="total_cotizacion"></span> Cotizacion -->
  <button type="button" class="btn-cot" onclick="view_select()">Cotizaci&oacute;n</button>
</div>

<div id="load-cotizacion" style="display:none">
  <div class="close" onclick="$('#load-cotizacion').hide('drop', {direction: 'right'});">
    <i class="fa fa-times"></i>
  </div>
  <div id="cotizacion-item-1" class="compare-item"><?php loadImage('no-item.png'); ?></div>
  <div id="cotizacion-item-2" class="compare-item"><?php loadImage('no-item.png'); ?></div>
  <div id="cotizacion-item-3" class="compare-item"><?php loadImage('no-item.png'); ?></div>
  <div id="cotizacion-item-4" class="compare-item">
    <label style="color:white;color: white;font-size: 22px;background-color: #455;padding: 12px 10px;text-align:center;min-width: 50px;display:none">+10</label>
    <?php loadImage('no-item.png'); ?>
  </div>

  <button type="button" class="btn btn-success btn-lg" onclick="view_select()">Cotizar</button>
</div>

<div id="compare-modal" class="modal fade hidden-print">
  <div class="modal-dialog modal-lg" style="width: 1000px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Comparaci&oacute;n Hoteles</h4>
      </div>
      <div class="modal-body">
        <div id="hotels-comparation"></div>
        <div class="clearfix"></div>
        <div class="col-md-12 text-right">
          <div class="link" onclick="showSendMail('Hoteles en <?php echo $_GET['destination'] . '. Del ' . $_GET['checkin'] . ' al ' . $_GET['checkout'] ?>.', 'hotels-comparation');">
            <span class="fa-stack fa-2x">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
            </span>
          </div>
          <div class="link" onclick="$('#hotels-comparation').printThis()">
            <span class="fa-stack fa-2x">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-print fa-stack-1x fa-inverse"></i>
            </span>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="modal-footer">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

<!-- MODAL D COTIZACIONES -->
<div id="cotizacion-modal" class="modal fade hidden-print" style="z-index:2000">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Modal</h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-danger" id="div_infante" style="display:none">
          Notese que en esta cotizaci&oacute;n hay menores, por el cual pueden alterar los precios.
        </div>

        <div>
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="">
              <a href="#publicacion" aria-controls="publicacion" role="tab" data-toggle="tab" onclick="$('#div_btn_publicacion').show();$('#div_btn_cotcrear').hide();$('#div_btn_cotactual').hide();">Publicaci&oacute;n</a>
            </li>
            <li role="presentation" class="active">
              <a href="#home" aria-controls="home" role="tab" data-toggle="tab" onclick="$('#div_btn_publicacion').hide();$('#div_btn_cotcrear').show();$('#div_btn_cotactual').hide();">Nuevo Cotizaci&oacute;n</a>
            </li>
            <li role="presentation">
              <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" onclick="$('#div_btn_publicacion').hide();$('#div_btn_cotcrear').hide();$('#div_btn_cotactual').show();">Cotizaci&oacute;n Existente</a>
            </li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <!-- Tabs de Publicacion -->
            <div role="tabpanel" class="tab-pane" id="publicacion">
              <div class="col-md-12" style=" min-height: 160px;padding: 10px;font-size:15px;  border-right: 1px solid #aaa;border-bottom: 1px solid #aaa;border-left: 1px solid #aaa;;margin-bottom: 5px;">
                <div class="col-md-4">
                  <label>Publicaci&oacute;n</label>
                  <input type="hidden" name="id_publicacion" id="id_publicacion">
                  <input type="text" id="numero_publicacion" class="form-control  requiere-validar-pub" onkeyup="validar_input_vacios(this.id)" placeholder="Titulo o Numero">
                  <a onclick="$('#numero_publicacion, #p_titulo').val('');" style="cursor:pointer">Limpiar Seleccion</a>
                </div>

                <div class="col-md-4" style="margin-top:25px">
                  <input type="button" class="btn btn-default" value="Buscar" onclick="buscar_publicacion()">
                </div>

                <div class="clearfix"></div>
                <div class="col-md-12" style="">
                  <h3>Ciudades de Origen</h3>
                  <div class="btn-group" data-toggle="buttons" id="contenido_ciudades_publicacion">
                  </div>
                </div>

                <div class="col-md-12" id="container_publicacion" style="display:none;padding-top:10px;top:86px;position: absolute;z-index: 100;background-color: white;border: 1px solid #ddd;height:500px;overflow:scroll">
                  <div id="load-buscar-publicacion" class="load-buscar-publicacion" style="display:block;text-align:center;">
                    <?php loadImage('ajax-loader.gif'); ?><br>
                    Buscando....
                  </div>
                  <i class="fa fa-times pull-right" style="color: red;font-size: 20px;cursor: pointer;" onclick="$('#container_publicacion').hide()">Ocultar</i>
                  <div id="resultado_publicacion"></div>
                </div>
              </div>
            </div>
            <!-- Tabs de Nueva cotizacion -->
            <div role="tabpanel" class="tab-pane active" id="home">
              <div class="col-md-12" style="padding: 10px;font-size:15px;  border-right: 1px solid #aaa;border-bottom: 1px solid #aaa;border-left: 1px solid #aaa;margin-bottom: 5px;">
                <div class="col-md-4">
                  <label>Titulo de Cotizaci&oacute;n</label>
                  <input type="text" id="ntitulo_cotizacion" class="form-control requiere" placeholder="Titulo" onkeyup="validar_input_vacios(this.id);">
                </div>
                <div class="col-md-4">
                  <input type="hidden" id="ncod_cliente" value="">
                  <label>Cliente</label>
                  <input type="text" id="name_cliente" class="form-control requiere">
                  <div id="load-cliente" class="load-cliente" style="display:none;top: 33px;position: absolute;right: 28px;">
                    <?php loadImage('ajax-loader.gif'); ?><br>
                  </div>
                  <label><input type="radio" name="check_cliente" id="check_natural" checked="checked" onclick="$('#solicitante').hide();$('#name_cliente, #ncod_cliente, #name_solicitante, #ncod_solicitante').val('');"> Natural</label>&nbsp;&nbsp;
                  <label><input type="radio" name="check_cliente" id="check_agencia" onclick="$('#solicitante').show();$('#name_cliente, #ncod_cliente').val('');"> Empresa o Agencia</label>
                </div>

                <div class="col-md-4" id="solicitante" style="display:none">
                  <input type="hidden" id="ncod_solicitante" value="">
                  <label>Solicitante</label>
                  <input type="text" id="name_solicitante" class="form-control requiere">
                </div>

                <div class="clearfix"></div>
                <div class="col-md-4">
                  <label>Ciudad de Salida</label>
                  <input type="text" id="nciudad_salida" class="form-control requiere" placeholder="COD (IATA)" onkeyup="validar_input_vacios(this.id);">
                </div>

                <div class="col-md-4">
                  <label>Salida</label>
                  <input type="text" id="nsalida_fecha" class="form-control requiere" placeholder="dd/mm/aa" autocomplete="off">
                </div>

                <div class="col-md-4">
                  <label>Retorno</label>
                  <input type="text" id="nentrada_fecha" class="form-control requiere" placeholder="dd/mm/aa" autocomplete="off">
                </div>

                <div class="col-md-6">
                  <label>Observacion</label>
                  <textarea name="observacion_main" id="observacion_main" class="form-control" cols="10"></textarea>
                </div>

              </div>
            </div>
            <!-- Tabs de Cotizacion -->
            <div role="tabpanel" class="tab-pane" id="profile">
              <div class="col-md-12" style=" min-height: 160px;padding: 10px;font-size:15px;  border-right: 1px solid #aaa;border-bottom: 1px solid #aaa;border-left: 1px solid #aaa;margin-bottom: 5px;">
                <div class="col-md-4">
                  <label>Cotizacion o numero</label>
                  <input type="hidden" name="id_cotizacion" id="id_cotizacion" placeholder="Nombre o numero">
                  <input type="text" id="name_cotizacion" class="form-control  requiere-validar" onkeyup="validar_input_vacios(this.id)">
                  <a onclick="$('#name_cotizacion, #b_name_cliente').val('');" style="cursor:pointer">Limpiar Seleccion</a>
                </div>

                <div class="col-md-4">
                  <label>Cliente</label>
                  <input type="text" id="b_name_cliente" class="form-control  requiere-validar" placeholder="Nombre Cliente" onkeyup="validar_input_vacios(this.id)">
                </div>

                <div class="col-md-4" style="margin-top:25px">
                  <input type="button" class="btn btn-default" value="Buscar" onclick="BuscarCotizacion()">
                </div>

                <div class="clearfix"></div>
                <div class="col-md-12" style="">
                  <h3>Ciudades de Origen</h3>
                  <div class="btn-group" data-toggle="buttons" id="contenido_ciudades">
                  </div>
                </div>

                <div class="col-md-12" style="margin-top: 10px;">
                  <label>
                    <input type="checkbox" name="sobre_escribir" id="sobre_escribir"> Sobreescibir
                  </label>
                </div>

                <div class="col-md-12" id="container_cotizacion" style="display:none;padding-top:10px;top:86px;position: absolute;z-index: 100;background-color: white;border: 1px solid #ddd;height:500px;overflow:scroll">
                  <div id="load-buscar-cotizacion" class="load-buscar-cotizacion" style="display:block;text-align:center;">
                    <?php loadImage('ajax-loader.gif'); ?><br>
                    Buscando....
                  </div>
                  <i class="fa fa-times pull-right" style="color: red;font-size: 20px;cursor: pointer;" onclick="$('#container_cotizacion').hide()">Ocultar</i>
                  <div id="resultado_cotizacion"></div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-12" style="margin-top:10px;padding: 10px;font-size:15px;border: 1px solid #aaa;border-radius: 5px;margin-bottom: 5px;">
          <div class="col-md-12">
            <strong>Destino:</strong>&nbsp;
            <span id="ct_destino"><?php echo _get('hotel_destination'); ?></span>
          </div>
          <div class="col-md-12">
            <strong>Entrada:&nbsp;</strong>&nbsp;<span><?php echo _get('checkin'); ?></span>&nbsp;&nbsp;
            <strong>Salida:&nbsp;</strong> &nbsp;<span><?php echo _get('checkout'); ?></span>
            <small id="pr_noches">
              <?php
              if (isset($_GET['checkin']) && isset($_GET['checkout'])) {
                $from = new DateTime(dateFormatForDB(_get('checkin')));
                $to = new DateTime(dateFormatForDB(_get('checkout')));
                $nights = $to->diff($from)->days;
                echo "($nights noches)";
              }
              ?>
            </small>&nbsp;<input type="hidden" name="noche" id="noche" value="<?php echo $nights; ?>"><!--ocupado para cotizacion -->
          </div>
        </div>

        <div id="hotels-new-cotizacion"></div>
        <div id="hotels-cotizacion" style="display:none"></div>
        <div id="alert_moneda" class="col-md-12" style="color:red;font-weight:bold;display:none">
          Tomar en cuenta el tipo de moneda al enviar esta cotizaci&oacute;n
        </div>

        <div class="col-md-4" id="div_btn_publicacion" style="margin-top:20px;display:none">
          <input type="button" id="actualizar_pub" class="btn btn_success" value="Actualizar Publicaci&oacute;n" onclick="update_publicacion()">
        </div>

        <div class="col-md-4" id="div_btn_cotcrear" style="margin-top:20px;">
          <input type="button" id="actualizar_cot" class="btn btn_success" value="Crear Cotizacion" onclick="crear_nueva_cotizacion()">
        </div>

        <div class="col-md-4" id="div_btn_cotactual" style="margin-top:20px;display:none">
          <input type="button" class="btn btn_success" value="Actualizar Cotizaci&oacute;n" onclick="Update_Cotizacion()">
        </div>
        <div class="col-md-12" id="loading" style="text-align:center;display:none">
          <?php loadImage('ajax-loader.gif'); ?><br>
          Guardando Cotizacion....
        </div>

        <div class="col-md-12" id="confirm" style="margin-top: 20px;background-color: #3cb521;border-radius: 5px;padding: 10px;text-align: center;color: white;display:none">
          se guardaron todos los hoteles
        </div>

        <div class="col-md-12" id="error" style="margin-top: 20px;background-color: #EC3E1F;border-radius: 5px;padding: 10px;text-align: center;color: white;display:none">
          Hubo problemas al guardar la cotizacion
        </div>

        <div class="clearfix"></div>
      </div>

      <div class="modal-footer">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<!-- end de modal de cotizaciones -->

<!-- Modal de Notificacion TimeLimit -->
<div id="expiredTime" class="modal fade" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #e7ae49;">
        <h4 class="modal-title" style="color:white;font-weight:bold;">Notificación!</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-1">
            <span class="fa fa-exclamation-circle" style="font-size: 51px;color:#e7ae49;position: absolute;margin-top:7px"></span>
          </div>
          <div class="col-md-11" style="padding-left: 32px;">
            <span style="font-size: 14px;">
              El tiempo de tu busqueda expiro,<br />
              por favor vuelve a realizar tu consulta.<br />
            </span>
          </div>
        </div>
        <div class="msg-errors"></div>
      </div>
      <div class="modal-footer">
        <?php $url_abs = $_SERVER["PHP_SELF"]; ?>
        <form method="get" action="<?php echo $url_abs; ?>/../../../freelance/public/aereo">
          <button class="btn-not" style="">Aceptar</button>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="modal-nueva-busqueda">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="primary">Nueva B&uacute;squeda</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="tabs-new-design modal-body">
        <div id="search_paq" class="form-booking tab-content conten-form-pq">
          <div class="tab-pane tab-tipo-vuelo fade in active" id="tab-b">
            <form method="get" id="refine-form" onsubmit="return saveDatesSubmit();">
              <div class="col-sm-12">
                <label for="destination">Destino</label>
                <?php include('_destination_search_form.php'); ?>
              </div>
              <div class="col-sm-6">
                <label for="checkin">Entrada</label>
                <input type="text" class="form-control" id="checkin" name="checkin" placeholder="dd/mm/aa" autocomplete="off" value="<?php echo _get('checkin'); ?>">
              </div>
              <div class="col-sm-6">
                <label for="checkout">Salida</label>
                <input type="text" class="form-control" id="checkout" name="checkout" placeholder="dd/mm/aa" autocomplete="off" value="<?php echo _get('checkout'); ?>">
              </div>
              <div class="separator"></div>


              <div class="form-group col-md-12 small" style="color: #fff;">
                <strong>Ocupacion:</strong><br />
                <?php
                $roomsCount = intval($_GET['habitaciones']);
                $adultCount = 0;
                $childCount = 0;
                for ($i = 1; $i <= $roomsCount; $i++) {
                  $adultCount += intval($_GET['adultos' . $i]);
                  $childCount += intval($_GET['ninos' . $i]);
                }
                echo $adultCount . ' adultos';
                if ($childCount > 0) {
                  echo ' - ' . $childCount . ' ni&ntilde;os';
                }
                echo ' (' . $roomsCount;
                echo $roomsCount == 1 ? ' habitaci&oacute;n)' : ' habitaciones)';
                ?>
                <div class="link" onclick="$('#change-accommodation').slideDown(); $(this).hide();">Cambiar ocupaci&oacute;n</div>
              </div>

              <div id="change-accommodation">
                <div class="form-group col-md-4">
                  <label for="habitaciones">Hab.</label>
                  <select class="form-control" id="habitaciones" name="habitaciones" onchange="updateRoomsConfigPanel(); showChildAgesConfig()" value="3">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                    <option>7</option>
                    <option>8</option>
                    <option>9</option>
                    <option>10</option>
                  </select>
                </div>
                <div id="rooms_config" class="form-group col-md-8">
                  <div class="row">
                    <div class="col-md-6 col-xs-6">
                      <label>Adultos</label>
                    </div>
                    <div class="col-md-6 col-xs-6">
                      <label>Ni&ntilde;os</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6 col-xs-6">
                      <select class="form-control" id="adultos1" name="adultos1">
                        <option>1</option>
                        <option selected="selected">2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                        <option>7</option>
                        <option>8</option>
                        <option>9</option>
                        <option>10</option>
                        <option>11</option>
                        <option>12</option>
                      </select>
                    </div>
                    <div class="col-md-6 col-xs-6">
                      <select class="form-control" id="ninos1" name="ninos1" onchange="showChildAgesConfig()">
                        <option>0</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div id="child_ages" class="form-group col-md-12"></div>
              </div>

              <div class="col-md-12 small">
                <label class="container-check">
                  <ti>Listado Completo</ti>
                  <input type="checkbox" id="last-version" name="last-version" value="allResult" <?php echo (isset($_GET['last-version']) && $_GET['last-version'] == 'allResult') ? 'checked' : ''; ?>>
                  <span class="checkmark"></span>
                </label>
              </div>

              <div class="mini-hidden-separator"></div>

              <?php if ($_SESSION['admin_role']) { ?>
                <?php
                $includeHB  = false;
                $includeDT  = false;
                $includeMB2 = false;
                $includeTH  = false;
                $includeNM  = false;
                $includeTB  = false;
                $includeNM  = false;
                if (isset($_GET['providers'])) {
                  $providers  = $_GET['providers'];
                  $includeHB  = in_array(BookingProvider::HOTELBEDS, $providers);
                  $includeDT  = in_array(BookingProvider::DOMITUR2, $providers);
                  $includeMB2 = in_array(BookingProvider::METHABOOK2, $providers);
                  $includeTH  = in_array(BookingProvider::TOURICO, $providers);
                  $includeNM  = in_array(BookingProvider::NEMO, $providers);
                  $includeTB  = in_array(BookingProvider::TAILORBEDS, $providers);
                  $includeNM  = in_array(BookingProvider::NEMO, $providers);
                }
                ?>
                <div class="col-md-12" style="color: #fff; border-bottom: 2px solid white;font-size: 18px;padding: 5px;cursor: pointer;font-weight: bold;" onclick="$('#filter_hotels_pq').slideToggle('slow');">
                  Proveedores &nbsp; <i class="fa fa-caret-down"></i>
                </div>

                <div class="mini-hidden-separator"></div>

                <div id="filter_hotels_pq" class="col-md-12 items" style="display: none;">
                  <div>
                    <label class="container-check">
                      <ti class="provider_1">Hotelbeds</ti>
                      <input type="checkbox" name="providers[]" id="p1" value="<?php echo BookingProvider::HOTELBEDS; ?>" <?php if ($includeHB) echo 'checked="true"'; ?>>
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div>
                    <label class="container-check">
                      <ti class="provider_136">TailorBeds</ti>
                      <input type="checkbox" name="providers[]" id="p6" value="<?php echo BookingProvider::TAILORBEDS; ?>" <?php if ($includeTB) echo 'checked="true"'; ?> />
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div>
                    <label class="container-check">
                      <ti class="provider_187">Nemo</ti>
                      <input type="checkbox" name="providers[]" id="p7" value="<?php echo BookingProvider::NEMO; ?>" <?php if ($includeNM) echo 'checked="true"'; ?>>
                      <span class="checkmark"></span>
                    </label>
                  </div>
                </div>
              <?php   } else { ?>
                <input type="hidden" name="providers[]" value="<?php echo BookingProvider::HOTELBEDS; ?>" id="p1" />
                <input type="hidden" name="providers[]" value="<?php echo BookingProvider::TAILORBEDS; ?>" id="p6" />
                <input type="hidden" name="providers[]" value="<?php echo BookingProvider::NEMO; ?>" id="p7" />
              <?php   } ?>

              <!-- HIDDEN DE FILTRO CATEGORIA -->
              <div class="hidden">
                <?php
                $star_1 = false;
                $star_2 = false;
                $star_3 = false;
                $star_4 = false;
                $star_5 = false;
                $star_other = false;
                if (isset($_GET['star'])) {
                  $star = $_GET['star'];
                  $star_1 = in_array("'1EST','1LL','APTH','H1_5','HR','HS','HSR1'", $star);
                  $star_2 = in_array("'2EST','2LL','APTH2','H2S','H2_5','HR2','HS2','HSR2'", $star);
                  $star_3 = in_array("'3EST','3LL','APTH3','BB3','H3S','H3_5','HR3','HS3'", $star);
                  $star_4 = in_array("'4EST','4LL','4LUX','APTH4','BB4','H4_5','HR4','HS4','SUP'", $star);
                  $star_5 = in_array("'5EST','5EST','5LL','5LUX','APTH5','BB5','H5_5','HR5','HRS','HS5'", $star);
                }
                ?>
                <input type="checkbox" name="star[]" id="star_s1"
                  value="'1EST','1LL','APTH','H1_5','HR','HS','HSR1'"
                  style="display: none;" <?php echo $star_1 ? 'checked' : ''; ?> />

                <input type="checkbox" name="star[]" id="star_s2"
                  value="'2EST','2LL','APTH2','H2S','H2_5','HR2','HS2','HSR2'"
                  style="display: none;" <?php echo $star_2 ? 'checked' : ''; ?> />

                <input type="checkbox" name="star[]" id="star_s3"
                  value="'3EST','3LL','APTH3','BB3','H3S','H3_5','HR3','HS3'"
                  style="display: none;" <?php echo $star_3 ? 'checked' : ''; ?> />

                <input type="checkbox" name="star[]" id="star_s4"
                  value="'4EST','4LL','4LUX','APTH4','BB4','H4_5','HR4','HS4','SUP'"
                  style="display: none;" <?php echo $star_4 ? 'checked' : ''; ?> />

                <input type="checkbox" name="star[]" id="star_s5"
                  value="'5EST','5EST','5LL','5LUX','APTH5','BB5','H5_5','HR5','HRS','HS5'"
                  style="display: none;" <?php echo $star_5 ? 'checked' : ''; ?> />
              </div>
              <!-- HIDDEN DE FILTRO REGIMEN -->
              <div class="hidden">
                <?php
                $pen_compl  = false;
                $aloj_des   = false;
                $med_pensi  = false;
                $solo_habit = false;
                $todo_inclu = false;
                if (isset($_GET['regimen'])) {
                  $regimen    = $_GET['regimen'];
                  $pen_compl  = in_array("FB", $regimen);
                  $aloj_des   = in_array("BB,AD", $regimen);
                  $med_pensi  = in_array("HB,MP", $regimen);
                  $solo_habit = in_array("RO,SA", $regimen);
                  $todo_inclu = in_array("AI,TI", $regimen);
                }
                ?>
                <div>
                  <input type="checkbox" name="regimen[]" id="reg_r1" value="FB" <?php echo $pen_compl ? 'checked' : ''; ?> /> 1
                  <input type="checkbox" name="regimen[]" id="reg_r2" value="BB,AD" <?php echo $aloj_des ? 'checked' : ''; ?> /> 2
                  <input type="checkbox" name="regimen[]" id="reg_r3" value="HB,MP" <?php echo $med_pensi ? 'checked' : ''; ?> /> 3
                  <input type="checkbox" name="regimen[]" id="reg_r4" value="RO,SA" <?php echo $solo_habit ? 'checked' : ''; ?> /> 4
                  <input type="checkbox" name="regimen[]" id="reg_r5" value="AI,TI" <?php echo $todo_inclu ? 'checked' : ''; ?> /> 5
                </div>
              </div>

              <div class="hidden">
                <!-- HIDDEN DE FILTRO NOMBRE DE HOTEL -->
                <input type="hidden" id="filter_hotelName" name="hotelName" value="<?php echo isset($_GET['hotelName']) ? $_GET['hotelName'] : ''; ?>">
                <!-- HIDDEN DE FILTRO ZONA -->
                <input type="hidden" id="zone_name" name="zone_name" value="<?php echo (isset($_GET['zone_name']) && $_GET['zone_name'] != 'Todas las zonas') ? $_GET['zone_name'] : ''; ?>">
                <!-- HIDDEN DE FILTRO DE ORDEN DE PRECIO -->
                <input type="hidden" id="orderPrice" name="orderPrice" value="<?php echo _get('orderPrice'); ?>">
              </div>
              <div class="col-sm-3 fright">
                <input type="hidden" name="controller" value="accommodation" />
                <input type="hidden" name="action" value="search_ajax" />
                <input type="hidden" name="page" value="1" />
                <input type="hidden" id="tokenAvail" name="tokenAvail" value="">
                <input type="submit" value="Buscar" onclick="verifySearch();" />
              </div>

            </form>

            <div class="clear"></div>

          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<?php include('_maps.php'); ?>

<script type="text/javascript">
  var current_user = <?php echo json_encode($_SESSION['current_user']); ?>;
  var arr_count_provider = {
    HotelBeds: 0,
    Domitur: 0,
    Methabook: 0,
    Tourico: 0,
    Restel: 0,
    TailorBeds: 0
  };
  var allHotelAvails = []
  arr_html = []
  hotelAvailRes = {};
  _params = {};
  _childAges = {};
  canLoadMoreHotels = false;
  currentPage = 0;
  totalPages = 0;

  $(function() {
    data = <?php echo json_encode($_GET); ?>;
    console.log('Read Correos', data)
    data.current_user = current_user
    data.action = "search"

    $.ajax({
      async: true,
      url: 'indexrest.php',
      type: 'GET',
      data: data,
      success: function(res) {
        $('.loader').hide();
        $('.hotel-avail-res').html('');
        hotelAvailRes = JSON.parse(res);
        $('#tokenAvail').val(hotelAvailRes.rs.tokenAvail)
        if (hotelAvailRes.rs.hotelsAvail.length == 0) {
          $('#contentNoResult').show();
        } else {
          _params = hotelAvailRes.params;
          _childAges = hotelAvailRes.childAges;
          currentPage = parseInt(hotelAvailRes.rs.currentPage);
          totalPages = parseInt(hotelAvailRes.rs.totalPages);
          createView(hotelAvailRes.rs.hotelsAvail);
          cargar_hoteles(hotelAvailRes.rs.hotelsAvail);
          center = getCenterPosition();
          $('#googlemap img').show();
          $('.loadin_action').hide();
          $('#googlemap img').attr('src', 'http://maps.googleapis.com/maps/api/staticmap?center=' + center.lat() + ',' + center.lng() + '&zoom=11&size=415x360&sensor=false&key=AIzaSyAN2JTbJ5ALoBrHk0DlB4TgpQI30qUhyjA');
          canLoadMoreHotels = true;
          paginate(currentPage, totalPages)
          verificar_seleccionados()
          verifySelectCompare()
          retoreMarkers()
        }
      },
      error: function(err) {
        console.log(err)
        alert('Lo sentimos, la pagina no pudo ser cargada correctamente, favor intente nuevamente o ponganse en contacto con nosotros.');
      }
    });

  });


  function paginate(currentPage, totalPages) {
    var inicio = ((currentPage - 3) > 1) ? (currentPage - 3) : 1;
    if ((currentPage + 3) > totalPages && (totalPages >= 7)) {
      disminuir = (currentPage + 3) - totalPages;
      inicio -= disminuir;
    }
    var fin = ((currentPage + 3) <= totalPages) ? (totalPages <= 7) ? totalPages : (currentPage + 3) : totalPages
    if ((currentPage - 3) < 1 && totalPages > 7) {
      valorFin = (currentPage - 3)
      con = 0
      for (var i = valorFin; i < 1; i++) {
        con++;
      }
      fin += con
    }
    var cad = ''
    if (currentPage > 4) {
      data['action'] = 'search_ajax'
      data['page'] = 1
      var paramquery = queryParams(data)
      cad += "<li class='page-item'><a class='page-link pageNormal' href='#' onclick='filtrarResult(1);paginate(1, " + totalPages + ")' style='padding: 12px 15px;color:#ff3300;'>Inicio</a></li>";
    }
    for (var i = inicio; i <= fin; i++) {
      data['action'] = 'search_ajax'
      data['page'] = i
      var paramquery = (i == currentPage) ? "" : "href='#'"
      var current = (i == currentPage) ? "disabled" : "";
      var styleActive = (i == currentPage) ? "pageActive" : "pageNormal"
      var actionClick = (i == currentPage) ? "" : 'filtrarResult(' + i + ');paginate(' + i + ', ' + totalPages + ')'
      cad += '<li class="page-item ' + current + '"><a class="page-link ' + styleActive + '"  ' + paramquery + ' onclick="' + actionClick + '" >' + (i) + '</a></li>';
    }

    if (fin != totalPages) {
      data['action'] = 'search_ajax'
      data['page'] = totalPages
      var paramquery = queryParams(data)
      cad += "<li class='page-item'><a class='page-link pageNormal' href='#' onclick='filtrarResult(" + totalPages + ");paginate(" + totalPages + ", " + totalPages + ")' >Fin</a></li>";
    }
    $(".pagination").html(cad);
  }

  function queryParams(source) {
    var array = [];

    for (var key in source) {
      if (Array.isArray(source[key])) {
        var arr = source[key];
        for (var i = 0; i < arr.length; i++) {
          array.push(encodeURIComponent(key + "[]") + "=" + encodeURIComponent(arr[i]).replace(/'/g, "%27"));
        }
      } else {
        array.push(encodeURIComponent(key) + "=" + encodeURIComponent(source[key]).replace(/'/g, "%27"));
      }
    }

    return array.join("&");
  }

  function createView(hotels) {
    arr_count_provider = {
      HotelBeds: 0,
      Domitur: 0,
      Methabook: 0,
      Tourico: 0,
      Restel: 0,
      TailorBeds: 0
    };
    var index;
    var count = 0;
    html = ""
    for (index = 0; index < hotels.length; ++index) {
      count++;
      hotel = hotels[index];
      // allHotelAvails.push(hotels[index]);
      html += createHotelView(hotel, count);

      if (hotels[index].provider == 1) {
        arr_count_provider.HotelBeds += 1;
      }
      if (hotels[index].provider == 150) {
        arr_count_provider.Methabook += 1;
      }
      if (hotels[index].provider == 159) {
        arr_count_provider.Domitur += 1;
      }
      if (hotels[index].provider == 80) {
        arr_count_provider.Tourico += 1;
      }
      if (hotels[index].provider == 3) {
        arr_count_provider.Restel += 1;
      }
      if (hotels[index].provider == 136) {
        arr_count_provider.TailorBeds += 1;
      }
    }
    $('.provider_1').html('Hotelbeds (' + arr_count_provider.HotelBeds + ')');
    $('.provider_150').html('Methabook (' + arr_count_provider.Methabook + ')');
    $('.provider_159').html('Domitur (' + arr_count_provider.Domitur + ')');
    $('.provider_80').html('Tourico (' + arr_count_provider.Tourico + ')');
    $('.provider_3').html('Restel (' + arr_count_provider.Restel + ')');
    $('.provider_136').html('TailorBeds (' + arr_count_provider.TailorBeds + ')');
    // document.getElementById('hotel-avail-res').inner_HTML = arr_html.join('');
    $('.hotel-avail-res').append(html);
  }
  var destTypingTimer;

  function buscar_hotel() {
    $(window).scrollTop(0);
    $('.hotel-avail-res').html("");
    $('#contentNoResult').hide()
    $('.hotelLoadingRes').show();
    var userToken = <?php echo $_SESSION['current_user']->id; ?>;
    nombre = $('#search-hotel').val();
    orderPrice = $('#orderPriceCombo').val()
    tokenAvail = $('#tokenAvail').val()
    page = 1;
    star = $('input:checkbox[class=star]:checked')
    estrellas = []
    for (var i = 0; i < star.length; i++) {
      estrellas.push($(star[i]).val())
    }
    regimen = $('input:checkbox[class=regimen]:checked')
    regimenArr = []
    for (var i = 0; i < regimen.length; i++) {
      regimenArr.push($(regimen[i]).val())
    }
    last_version = ''
    if ($('#last-version').prop('checked')) {
      last_version = 'allResult'
    }
    zone_code = ''
    if ($('#zoneCodeCombo').val() != '') {
      zone_code = $('#zoneCodeCombo').val()
    }

    $.ajax({
      url: 'helpers/ajax/paginate_results.php',
      type: 'GET',
      data: {
        page: page,
        tokenAvail: tokenAvail,
        userToken: userToken,
        hotelName: nombre,
        orderPrice: orderPrice,
        star: estrellas,
        regimen: regimenArr,
        last_version: last_version,
        zoneCode: zone_code
      },
      success: function(res) {
        hotelAvailRes = JSON.parse(res);
        if (hotelAvailRes.rs == 'false') {
          $('#expiredTime').modal()
        } else if (hotelAvailRes.rs.hotelsAvail.length == 0) {
          paginate(1, 1)
          $('#contentNoResult').show();
        } else {
          cleanMarkers()
          $('#contentNoResult').hide();
          cleanMarkers()
          _params = hotelAvailRes.params;
          _childAges = hotelAvailRes.childAges;
          currentPage = parseInt(hotelAvailRes.rs.currentPage);
          totalPages = parseInt(hotelAvailRes.rs.totalPages);
          createView(hotelAvailRes.rs.hotelsAvail);
          cargar_hoteles(hotelAvailRes.rs.hotelsAvail);
          center = getCenterPosition();
          $('#googlemap img').show();
          $('.loadin_action').hide();
          $('#googlemap img').attr('src', 'http://maps.googleapis.com/maps/api/staticmap?center=' + center.lat() + ',' + center.lng() + '&zoom=11&size=415x360&sensor=false&key=AIzaSyAN2JTbJ5ALoBrHk0DlB4TgpQI30qUhyjA');
          paginate(currentPage, totalPages)

          $('.hotelLoadingRes').hide();
        }

      },
      error: function(err) {
        alert('Lo sentimos, la pagina no pudo ser cargada correctamente, favor intente nuevamente o ponganse en contacto con nosotros.');
      }
    });
  }

  function createHotelView(hotelAvail, index) {
    hotelAvail.name = hotelAvail.name.replace(/'/g, "\\'"); //"
    var hotelGroup = Math.round(index / 20);
    var groupedRoomsTypeStr = hotelAvail.groupedRooms[0][0].roomType;
    for (var i = 1; i < hotelAvail.groupedRooms[0].length; i++) {
      groupedRoomsTypeStr += '; ' + hotelAvail.groupedRooms[0][i].roomType;
    }
    var price = getGroupedRoomsPrice(hotelAvail.groupedRooms[0]);
    var categoryTypeStr = showCategory(hotelAvail.categoryCode, false);
    var roomsTyperStr = createRoomsView(hotelAvail, index);
    var serviceAddDataJSON = jsontoHotelAvail(hotelAvail, hotelAvail.groupedRooms[0], true);
    var serviceAddDataCotizacion = jsontoHotelAvail(hotelAvail, hotelAvail.groupedRooms[0], false);
    var ofertHtml = ''
    var id_input = hotelAvail.code + '_' + hotelAvail.provider
    var hotelLink = hotelAvail.provider != 136 ? 'href="' + getHotelLink(hotelAvail) + '"' : '';
    var target = hotelAvail.provider != 136 ? '_blank' : ' ';

    var html = '' +
      '<div class="hotel_avail item_avail hotel_group_' + hotelAvailRes.rs.currentPage + '_' + hotelGroup + ' hotel-li-' + hotelAvail.code + '">' +
      '<div class="item_img col-md-3">' +
      "<img src='" + hotelAvail.mainImage + "'  id='hotel-image-" + hotelAvail.code + "_" + index + "' onerror='this.onerror=null;this.src=\"http://barrybolivia.com/zion/assets/images/no-photo.jpg\";' />" +
      '</div>' +
      '<div class="col-md-7 item_info">' +
      '<div class="row">' +
      '<div class="col-md-12">' +
      '<div class="row">' +
      '<div class="col-md-12" >' +
      '<h3>' +
      '<a  ' + hotelLink + '  target="' + target + '" id="hotel-link-' + hotelAvail.code + '_' + index + '" class="name-hotel" style="font-weight:bold;" title="' + hotelAvail.name + '">' + maximoPermitido(hotelAvail.name) + '</a>' +
      '</h3>' +
      ofertHtml +
      '</div>' +
      '</div>' +
      '</div>' +
      '<div class="col-md-12">' +
      '<span class="hotel_category_' + hotelAvail.code + '">' +
      '<span data-toggle="tooltip" data-placement="top" data-container="body" data-original-title="' + hotelAvail.code + '">' +
      categoryTypeStr +
      '</span>' +
      '</span>';
    if (hotelAvail.location != ", ") {
      html += '<i class="fa fa-map-marker"></i> ' + hotelAvail.location;
    }
    html += '' +
      '</div>' +
      '<div class="col-md-12 desc">' +
      '<p>' +
      hotelAvail.description.substr(0, 120) + '...' +
      '<a ' + hotelLink + ' class="loadingAction">Ver m&aacute;s</a>' +
      '</p>' +
      '</div>' +
      '<div class="col-md-12 small room-info">' +
      '<strong>Habitaci&oacute;n: </strong>' + groupedRoomsTypeStr + '&nbsp; - &nbsp;' +
      '<strong>Servicios: </strong>' + hotelAvail.groupedRooms[0][0].board +
      '<input type="hidden" id="type-' + hotelAvail.code + '_' + index + '" value="' + hotelAvail.groupedRooms[0][0].roomType + '" />' +
      '<input type="hidden" id="board-' + hotelAvail.code + '_' + index + '" value="' + hotelAvail.groupedRooms[0][0].board + '" />' +
      '</div>' +
      '<div class="bottom-info col-md-12 small">' +
      '<div class="row">' +
      '<div class="compare hidden-xs col-md-4 col-xs-3" onclick="">' +
      '<input type="checkbox" class="compare-box" id="' + hotelAvail.code + '_' + index + '" value="' + hotelAvail.code + '_' + index + '" data-hotelcode="' + hotelAvail.code + '" onclick="toggleComparePreview(\'' + hotelAvail.code + '_' + index + '\')" style="margin-top: 0;" /> Comparar<br>';
    <?php if ($user_viacon) { ?>
      html += '<input type="checkbox" class="cotizar-check" id="main_hotel_' + id_input + '" value="' + id_input + '" onchange="enable_new_cotizacion(\'' + id_input + '\');habilitar_marcador(\'' + id_input + '\');" style="margin-top: 0;" /> Cotizar';
    <?php   } ?>
    html += '' +
      '</div>' +
      '<div class="col-md-3 col-xs-3">';
    if (hotelAvail.latitude != '' && hotelAvail.longitude != '') {
      html += '' +
        '<span class="search_near_hotels" onclick="showDistanceSearchModal(\'' + hotelAvail.name + '\', \'' + hotelAvail.latitude + '\', \'' + hotelAvail.longitude + '\');">' +
        '<i class="fa fa-map-marker"></i> Buscar cercanos' +
        '</span>';
    }
    html += '' +
      '</div>' +
      '<div class="col-md-5 col-xs-6 view-extra-rooms" onclick="$(\'#extra-rooms-' + hotelAvail.code + '_' + index + '\').slideToggle();">' +
      'Ver todas las habitaciones (' + hotelAvail.groupedRooms.length + ') <i class="fa fa-angle-down"></i>' +
      '</div>' +
      '</div>' +
      '</div>' +
      '</div>' +
      '</div>' +
      '<div class="book col-md-2">' +
      '<div class="provider">';
    <?php $can_access = array(); ?>
    <?php if ($_SESSION['admin_role']  || $_SESSION['agency_id'] == 9 || array_key_exists($_SESSION['current_user']->id, $can_access)) { ?>
      html += showHotelProviders(hotelAvail);
    <?php   } ?>
    html += '' +
      '</div>' +
      '<div class="price">' +
      price + ' <span class="currency">' + hotelAvail.currency + '</span>' +
      '<div class="price_average">' + (Math.round((price / hotelAvail.nights) * 100) / 100) + '<span>/noche</span></div>' +
      '</div>' +
      '<input type="hidden" id="provider-' + hotelAvail.code + '_' + index + '" value="' + hotelAvail.provider + '" />' +
      '<input type="hidden" id="currency-' + hotelAvail.code + '_' + index + '" value="' + hotelAvail.currency + '" />' +
      '<input type="hidden" id="price-' + hotelAvail.code + '_' + index + '" value="' + price + '" />' +
      '<div class="book-btn">';
    <?php if ($_SESSION['current_agency']->id != 196) {  ?>
      html += '' +
        '<form action="./" method="get" target="_blank">' +
        "<input type='hidden' name='service_add_data' value='" + JSON.stringify(serviceAddDataJSON) + "' />" +
        '<input type="hidden" name="requestCode" value="' + Math.random() + '" />' +
        '<input type="hidden" name="controller" value="shopping_cart" />' +
        '<input type="hidden" name="action" value="show" />' +
        '<button class="loadingAction">RESERVAR <i class="fa fa-angle-right"></i></button>' +
        '</form>'
    <?php   } else { ?>
      html += '<button class="loadingAction">RESERVAR <i class="fa fa-angle-right"></i></button>'
    <?php   } ?>
    html += '' +
      '</div>' +
      '</div>' +
      roomsTyperStr +
      '</div>';
    // $('.hotel-avail-res').append(html);
    // verifyExistImage("hotel-image-" + hotelAvail.code + "_" + index, hotelAvail.mainImage)
    return html;
  }

  function createRoomsView(hotelAvail, index) {
    var pos = 1;
    var serviceAddDataJSON = [];
    var providerStr = '';
    var roomsCad = `
      <div id="extra-rooms-${hotelAvail.code}_${index}" class="extra-rooms col-md-10">
        <table class="table table-striped table-hover small">
          <thead>
            <tr>
              <th>Tipo Habitacion</th>
              <th>Servicios</th>
              <th>Precio</th>
              <th></th>
            </tr>
          </thead>`;
    for (i = 0; i < hotelAvail.groupedRooms.length; i++) {
      <?php if ($_SESSION['admin_role']) { ?>
        providerStr = '<span class="text-muted small">(' + hotelAvail.groupedRooms[i][0].providerName + ') </span>';
      <?php } ?>
      priceRoomNet = getGroupedRoomsPrice(hotelAvail.groupedRooms[i])

      var roomstr = (hotelAvail.groupedRooms[i].length != 1) ? '<div class="strong" style="margin-bottom:8px">Habitaciones ' + providerStr + '</div>' : providerStr;
      //currentHotel = JSON.parse(hotelAvail.groupedRooms[i][0].hotel)
      var pricestr = '<span class="big strong">' + getGroupedRoomsPrice(hotelAvail.groupedRooms[i]) + ' ' + hotelAvail.currency + '</span><br>';
      for (j = 0; j < hotelAvail.groupedRooms[i].length; j++) {
        var room = hotelAvail.groupedRooms[i];
        if (parseInt(room[j].roomCount) > 1) {
          roomstr += room[j].roomType + ' x ' + room[j].roomCount + '<br>';
        } else {
          roomstr += room[j].roomType + '<br>';
        }
        priceDetails = room[j].priceWithFee
        //currentHotel = JSON.parse(hotelAvail.groupedRooms[i][j].hotel)
        pricestr += (hotelAvail.groupedRooms[i].length != 1) ? '<span class="small">' + room[j].priceWithFee + ' ' + hotelAvail.currency + '</span><br>' : '';
      }
      serviceAddDataJSON = jsontoHotelAvail(hotelAvail, hotelAvail.groupedRooms[i], true);
      serviceAddDataCotizacion = jsontoHotelAvail(hotelAvail, hotelAvail.groupedRooms[i], false);
      roomid_input = hotelAvail.code + '_' + hotelAvail.provider + "_" + pos
      roomclass_id = hotelAvail.code + '_' + hotelAvail.provider
      serviceAddDataCotizacion.inputCode = roomclass_id
      roomsCad += '<tr>' +
        '<td>' +
        "<span>" +
        "<input type='checkbox' id='" + roomid_input + "'" +
        "class='list_check_" + roomclass_id + "'" +
        "data-main='main_hotel_" + roomclass_id + "'" +
        "value='" + JSON.stringify(serviceAddDataCotizacion) + "'" +
        'onclick="add_item_room(\'' + roomid_input + '\')"' +
        "style='float:left;display:none;' />" +
        "</span>" +
        roomstr +
        '</td>' +
        '<td>' + hotelAvail.groupedRooms[i][0].board + '</td>' +
        '<td>' + pricestr + '</td>' +
        '<td>';
      <?php if ($_SESSION['current_agency']->id != 196) {  ?>
        roomsCad += '<form action="./" method="get" target="_blank">' +
          "<input type='hidden' name='service_add_data' value='" + JSON.stringify(serviceAddDataJSON) + "' />" +
          '<input type="hidden" name="requestCode" value="' + Math.random() + '" />' +
          '<input type="hidden" name="controller" value="shopping_cart" />' +
          '<input type="hidden" name="action" value="show" />' +
          '<button class="btnLoadingAction" style="width: 90px;">RESERVAR <i class="fa fa-angle-right"></i></button>' +
          '</form>'
      <?php } else { ?>
        roomsCad += '<button class="btnLoadingAction" style="width: 90px;">RESERVAR <i class="fa fa-angle-right"></i></button>'
      <?php } ?>
      roomsCad += '</td>' +
        '</tr>';
      pos++;
    }
    roomsCad += '</table>' +
      '</div>';
    return roomsCad;
  }

  function getHotelLink(hotelAvail) {
    var hotelLink = document.URL;
    var providers = _params.providers
    var proveedor = 0
    var proveedores = '';
    for (var i = 0; i < providers.length; i++) {
      hotelLink = hotelLink.replace("&providers%5B%5D=" + providers[i], '');
    }
    arrProvider = [];
    for (var j = 0; j < hotelAvail.groupedRooms.length; j++) {
      if (!arrProvider[hotelAvail.groupedRooms[j][0].provider]) {
        arrProvider[hotelAvail.groupedRooms[j][0].provider] = true;
        proveedores += '&amp;providers%5B%5D=' + hotelAvail.groupedRooms[j][0].provider;
      }
    }
    provider = '&amp;provider=' + hotelAvail.provider
    if (hotelAvail.hotelZionCode != hotelAvail.code) {
      provider = '&amp;provider=' + 1
    }
    proveedor = provider

    hotelLink = hotelLink.replace('search_ajax', 'show');
    hotelLink += proveedor;
    hotelLink += proveedores;
    hotelcode = '&hotelCode=' + hotelAvail.code;
    if (hotelAvail.hotelZionCode != hotelAvail.code) {
      hotelcode = '&hotelCode=' + hotelAvail.hotelZionCode
    }
    hotelLink += hotelcode
    hotelLink = hotelLink.replace("#", '');
    return hotelLink;
  }

  function verifyExistImage(obj, img) {
    var imgsrc = img;
    $('<img src="' + img + '" />').load(function() {
      $('#' + obj).removeClass("animated-background");
      $('#' + obj).attr("src", img);
    }).error(function() {
      $('#' + obj).removeClass("animated-background");
      $('#' + obj).attr("src", "assets/images/no-photo.jpg")
    });
  }

  function showCategory(categoryCode, onlyNumber) {
    var stars = {
      '1EST': 1,
      '1LL': 1,
      'APTH': 1,
      'HR': 1,
      'HS': 1,
      'HSR1': 1,
      '2EST': 2,
      '2LL': 2,
      'APTH2': 2,
      'H2S': 2,
      'HR2': 2,
      'HS2': 2,
      'HSR2': 2,
      '3EST': 3,
      '3LL': 3,
      'APTH3': 3,
      'BB3': 3,
      'H3S': 3,
      'HR3': 3,
      'HS3': 3,
      '4EST': 4,
      '4LL': 4,
      '4LUX': 4,
      'APTH4': 4,
      'BB4': 4,
      'HR4': 4,
      'HS4': 4,
      'SUP': 4,
      '5EST': 5,
      '5LL': 5,
      '5LUX': 5,
      'APTH5': 5,
      'BB5': 5,
      'HR5': 5,
      'HRS': 5,
      'HS5': 5,
      'AG': 0,
      'ALBER': 0,
      'AT1': 0,
      'AT2': 0,
      'AT3': 0,
      'BB': 0,
      'BOU': 0,
      'CAMP1': 0,
      'CAMP2': 0,
      'CHUES': 0,
      'HIST': 0,
      'LODGE': 0,
      'MINI': 0,
      'PENDI': 0,
      'PENSI': 0,
      'POUSA': 0,
      'RESID': 0,
      'RSORT': 0,
      'SPC': 0,
      'STD': 0,
      'VILLA': 0,
      'VTV': 0,
      'H1_5': 1.5,
      'H2_5': 2.5,
      'H3_5': 3.5,
      'H4_5': 4.5,
      'H5_5': 5.5
    };
    if (onlyNumber) {
      return stars[categoryCode];
    } else {
      return showCategoryhtml(stars[categoryCode])
    }
  }

  function showCategoryhtml(number_star) {
    var star_str = '<span style="white-space: nowrap;font-size:14px;">'
    var number = 0
    if (number_star == 0) {
      star_str += '<i class="fa fa-home" aria-hidden="true"></i> '
    }
    if ((number_star % 1) == 0) {
      var i = 0
      number = number_star
      while (i < number_star) {
        star_str += '<i class="fa fa-star" aria-hidden="true"></i> '
        i++
      }
    }
    if ((number_star % 1) != 0 && number_star != undefined) { //#cuando la cantidad  es decimal
      number = (number_star).toFixed()
      i = 0
      while (i < number - 1) {
        star_str += '<i class="fa fa-star" aria-hidden="true"></i> '
        i++
      }
      star_str += '<i class="fa fa-star-half-o" aria-hidden="true"></i> '
    }
    var dif = 5 - number
    i = 0
    while (i < dif) {
      star_str += '<i class="fa fa-star-o" aria-hidden="true" style="opacity: 0.3;"></i> '
      i++
    }
    star_str += '</span>';
    return star_str
  }

  function showHotelProviders(hotelAvail) {
    var html = '';
    if (true) { // TODO: if (hasAccessToViewProviders)
      html += '<i class="fa fa-folder-open-o"></i> ' + hotelAvail.groupedRooms[0][0].providerName + '<br>';
      alreadyPresent = [];
      alreadyPresent[hotelAvail.groupedRooms[0][0].provider] = true;
      for (var i = 1; i < hotelAvail.groupedRooms.length; i++) {
        var provider = hotelAvail.groupedRooms[i][0].provider;
        if (!alreadyPresent[provider]) {
          html += '<span style="font-size: 10px"> + ' + hotelAvail.groupedRooms[i][0].providerName + '</span><br>';
          alreadyPresent[provider] = true;
        }
      }
    }
    return html;
  }

  function getGroupedRoomsPrice(groupedRooms) {
    var price = 0;
    for (var i = 0; i < groupedRooms.length; i++) {
      price += groupedRooms[i].priceWithFee;
    }
    return Math.round(price * 100) / 100;
  }

  function jsontoHotelAvail(hab, rooms, type) {
    var room = getJsonRoom(rooms, type);
    var hotel = hab;
    request = {}
    request['availToken'] = hotel.availToken
    request['dateFrom'] = _params.checkin
    request['dateTo'] = _params.checkout
    //request['dateFrom'] = hotel.dateFrom
    //request['dateTo'] = hotel.dateTo
    //request['contractName'] = hotel.contractName
    //request['contractIncomingOfficeCode'] = hotel.contractIncomingOfficeCode
    request['hotelCode'] = rooms[0].hotelCode
    request['destCode'] = _params.destCode
    //request['destCode'] = hotel.destinationCode
    //request['destType'] = hotel.destinationType
    request['hotelName'] = hotel.name.replace(/'/g, '')
    request['category'] = hotel.categoryCode
    request['lat'] = hotel.latitude
    request['lng'] = hotel.longitude
    if (type) { // type for pricewithfee or only priceraw (service_add_data)
      request['currency'] = hotel.currency
    } else {
      request['moneda'] = hotel.currency
      request['estrella'] = showCategory(hotel.categoryCode, true)
    }
    request['rooms'] = room;
    request['childAges'] = _childAges
    request['requestType'] = 1
    request['provider'] = rooms[0].provider
    return request;
  }

  function getJsonRoom(rooms, type) {
    var roomarray = new Array();
    for (f = 0; f < rooms.length; f++) {
      roomrequest = {}
      roomrequest['roomCount'] = rooms[f].roomCount
      roomrequest['adultCount'] = rooms[f].adultCount
      roomrequest['childCount'] = rooms[f].childCount
      roomrequest['childAges'] = rooms[f].childAges
      if (type) {
        roomrequest['price'] = rooms[f].price
      } else {
        roomrequest['price'] = rooms[f].priceWithFee
      }
      roomrequest['SHRUI'] = rooms[f].sHRUI
      //roomrequest['onRequest'] = rooms[f].onRequest
      roomrequest['board'] = rooms[f].board
      //roomrequest['boardCode'] = rooms[f].boardCode
      //roomrequest['boardType'] = rooms[f].boardType
      roomrequest['roomType'] = rooms[f].roomType
      //roomrequest['roomTypeType'] = rooms[f].roomTypeType
      roomrequest['roomTypeCode'] = rooms[f].roomTypeCode
      //roomrequest['roomTypeCharacteristic'] = rooms[f].roomTypeCharacteristic
      roomarray.push(roomrequest);
    }
    return roomarray;
  }

  function maximoPermitido(texto) {
    if (texto.length > 35) {
      return texto.substring(0, 35) + '...';
    }
    return texto
  }
</script>

<!-- <script type="text/javascript" src="views/Pqt_Integracion/integration_functions.js?201809060639">
</script>

<script type="text/javascript" src="assets/javascript/accommodation_search.js">
</script>

<script type="text/javascript" src="assets/javascript/accommodation.js?201706131023">
</script> -->