<div id="search_destinations">
  <input type="text" class="form-control" id="hotel_destination" name="hotel_destination" placeholder="Introduce tu destino, zona o hotel..." autocomplete="off" value="<?php echo _get('hotel_destination'); ?>" required>
  <input type="hidden" id="destination" name="destination" value="<?php echo _get('destination'); ?>" />
  <input type="hidden" id="destination_code" name="destination_code" value="<?php echo _get('destination_code'); ?>" />
  <input type="hidden" id="zoneCode" name="zoneCode" value="<?php echo _get('zoneCode'); ?>" />
  <input type="hidden" id="hotelCode" name="hotelCode" value="<?php echo _get('hotelCode'); ?>" />  
  <div class="loading"></div>
</div>
<div class="destinations_result"></div>

<style type="text/css">
  #search_destinations { position: relative; }
  #search_destinations .loading { position: absolute; top: 15px; right: 15px; width: 16px; height: 16px; background-image: url(assets/images/ajax-loader.gif); display: none; }
  .destinations_result { position: absolute; color: #777; background-color: #fff; display: none; width: 620px; z-index: 99; border: 1px solid #ccc; }
  .destinations_result ul { padding: 0; list-style-type: none; }
  .destinations_result ul .header { float: right; font-size: 11px; display: inline; margin: 3px 5px; }
  .destinations_result li { padding: 5px 10px; font-size: 13px; border-bottom: 1px dotted #CCC; cursor: pointer; }
  .destinations_result li.group { padding: 5px 10px; font-size: 13px; border-top: 1px solid #555; }
  .destinations_result li.selected { color: #fff; background-color: #1393d0; }
</style>

<script type="text/javascript">
  function searchHotelDestinations() {
    var search = $('#hotel_destination').val();
    if (search.length < 3) {
      $('.destinations_result').hide();
      $('#search_destinations .loading').hide();
      return;
    }
    $.getJSON(
      "helpers/ajax/search_hotel_destinations.php",
      { q: search },
      function(result) {
        $('#search_destinations .loading').hide();
        var html = '';
        var regexp = new RegExp('(' + search + ')', 'ig');

        var firstDest = true;
        $.each(result['dest'], function() {
          var label = this.destination_name + ' (' + this.destination_code + ') - ' + this.country_name;
          label = label.replace(regexp, '<strong>$1</strong>');
          label = '<i class="fa fa-map-marker" style="opacity: 0.8"></i> &nbsp;<span class="item">' + label + '</span>';
          var data = 'data-dest="' + this.destination_name + ' - ' + this.country_name + '" data-dcode="' + this.destination_code + '" data-zcode="" data-hcode=""';
          if (firstDest) {
            var header = '<span class="header"><i class="fa fa-map-marker"></i> Destinos</span>';
            html += '<li class="group dest-item" ' + data + '>' + header + label + '</li>';
            firstDest = false;
          }
          else {
            html += '<li class="dest-item" ' + data + '>' + label + '</li>';
          }
        });

        var firstZone = true;
        $.each(result['zones'], function() {
          var label = this.zone_name + ', ' + this.destination_name + ' - ' + this.country_name;
          label = label.replace(regexp, '<strong>$1</strong>');
          label = '<i class="fa fa-crosshairs" style="opacity: 0.8"></i> &nbsp;<span class="item">' + label + '</span>';
          var data = 'data-dest="' + this.destination_name + ' - ' + this.country_name + '" data-dcode="' + this.destination_code + '" data-zcode="' + this.zone_code + '" data-hcode=""';
          if (firstZone) {
            var header = '<span class="header"><i class="fa fa-crosshairs"></i> Zonas</span>';
            html += '<li class="group zone-item" ' + data + '>' + header + label + '</li>';
            firstZone = false;
          }
          else {
            html += '<li class="zone-item" ' + data + '>' + label + '</li>';
          }
        });

        var firstHotel = true
        $.each(result['hotels'], function() {
          var label = this.hotel_name + ', ' + this.zone_name + ', ' + this.destination_name + ' - ' + this.country_name;
          label = label.replace(regexp, '<strong>$1</strong>');
          label = '<i class="fa fa-h-square" style="opacity: 0.8"></i> &nbsp;<span class="item">' + label + '</span>';
          var data = 'data-dest="' + this.destination_name + ' - ' + this.country_name + '" data-dcode="' + this.destination_code + '" data-zcode="' + this.zone_code + '" data-hcode="' + this.hotel_code + '"';
          if (firstHotel) {
            var header = '<span class="header"><i class="fa fa-map-marker"></i> Hoteles</span>';
            html += '<li class="group hotel-item" ' + data + '>' + header + label + '</li>';
            firstHotel = false;
          }
          else {
            html += '<li class="hotel-item" ' + data + '>' + label + '</li>';
          }
        });

        if (html == '') {
          html = '<li><i class="text-muted">No se encontraron resultados para tu b&uacute;squeda</i></li>';
        }

        html = '<ul>' + html + '</ul>';

        $('.destinations_result').html(html);
        $(".destinations_result").show();
        $(".destinations_result li").mouseover(function() {
          $(".destinations_result li").removeClass("selected");
          $(this).addClass("selected");
        }).click(function() {
          selectOption();
          if ($('select#zones').length) {
            loadZones($('.selected').data('dcode'));
          }
          
        });
      });
  }

  var destTypingTimer;
  $("#hotel_destination").on("input", function (e) {
    var value = $(this).val();
    if (value.length >= 3) {
      clearTimeout(destTypingTimer);
      destTypingTimer = setTimeout(function() {
        $('#search_destinations .loading').show();
        searchHotelDestinations();
      }, 500);
    } else {
      $(".destinations_result").hide();
    }
  });

  $("#hotel_destination").keydown(function(e) {
    if ((e.keyCode == 13 || e.keyCode == 9) && $(".destinations_result").is(':visible')) { // enter
      selectOption();
      if ($('select#zones').length) {
        loadZones($('.selected').data('dcode'));
      }
      e.stopPropagation();
      return false;
    }
    if (e.keyCode == 27) { // escape
      $(".destinations_result").hide();
    }
    else if (e.keyCode == 38) { // up
      var selected = $(".selected");
      $(".destinations_result li").removeClass("selected");
      if (selected.length == 0) {
        $('.destinations_result li:last').addClass("selected");
      }
      else if (selected.prev('li').length == 0) {
        selected.siblings('li').last().addClass("selected");
      } else {
        selected.prev('li').addClass("selected");
      }
      $('#hotel_destination').val($('#hotel_destination').val());
      // Avoid put the cursor at the begin of the table
      return false;
    }
    else if (e.keyCode == 40) { // down
      var selected = $(".selected");
      $(".destinations_result li").removeClass("selected");
      if (selected.length == 0) {
        $('.destinations_result li:first').addClass("selected");
      }
      else if (selected.next('li').length == 0) {
        selected.siblings('li').first().addClass("selected");
      } else {
        selected.next('li').addClass("selected");
      }
    }
  });

  function selectOption() {
    var item = $('.selected');
    $("#hotel_destination").val($('.selected .item').text());

    $('#destination').val(item.data('dest'));
    $('#destination_code').val(item.data('dcode'));
    $('#zoneCode').val(item.data('zcode'));
    $('#hotelCode').val(item.data('hcode'));

    $(".destinations_result").hide();
  }

  $('#hotel_destination').bind("keyup keypress", function(e) {
    var code = e.keyCode || e.which;
    if (code == 13) {
      //e.preventDefault();
      //return false;
    }
  });
</script>