<?php
global $rs, $detail, $childAges;
?>

<div id="accommodation_show" class="content">
  <div class="container">
    <div class="col-md-12 wbox">
      <div class="col-xs-6">
        <div class="link" onclick="window.history.back();">
          <span class="fa-stack fa-2x">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-arrow-left fa-stack-1x fa-inverse"></i>
          </span>
          Volver
        </div>
      </div>
      <div class="col-xs-6 text-right">
        <div class="link" onclick="showSendMail('<?php echo $detail->name; ?>', 'hotel');">
          <span class="fa-stack fa-2x">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
          </span>
        </div>
        <div class="link" onclick="$('#hotel').printThis()">
          <span class="fa-stack fa-2x">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-print fa-stack-1x fa-inverse"></i>
          </span>
        </div>
      </div>

      <div id="hotel" class="hotel">
        <style media="print">
          .hotel {
            font-size: 11px;
          }

          .hotel td {
            font-size: 11px;
          }
        </style>
        <table style="width: 100%">
          <tr>
            <!-- left side -->
            <td style="width: 40%; vertical-align: top; border-right: 1px solid #ddd; padding: 0 15px;">
              <?php include('_show_images.php'); ?>
              <?php $dir = $detail->addressStreetName . ', ' . $detail->addressNumber; ?>
              <?php if ($_SESSION['admin_role']) { ?>
                <div style="margin: 30px 0;">
                  <table>
                    <tbody>
                      <tr style="border-top: 1px solid #ddd;">
                        <td style="font-weight: bold; padding: 8px 5px;">Direcci&oacute;n:</td>
                        <?php
                        if ($detail->addressPostalCode) {
                          $dir .= ' - ' . $detail->addressPostalCode;
                        }
                        ?>
                        <td style="word-wrap: break-word; word-break:break-all; padding: 8px 0;">
                          <?php echo $dir. ' - ' .  $rs->hotelsAvail[0]->zone . ', ' . $rs->hotelsAvail[0]->destinationType; ?>
                        </td>
                      </tr>
                      <tr style="border-top: 1px solid #ddd;">
                        <td style="font-weight: bold; padding: 8px 5px;">Emails:</td>
                        <td style="word-wrap: break-all; word-break:break-all; padding: 8px 0;">
                          <?php
                          foreach ($detail->emailList as $mail) {
                            echo $mail . '<br />';
                          }
                          ?>
                        </td>
                      </tr>
                      <tr style="border-top: 1px solid #ddd;">
                        <td style="font-weight: bold; padding: 8px 5px;">Tel&eacute;fonos:</td>
                        <td style="word-wrap: break-word; word-break:break-all; padding: 8px 0;">
                          <?php
                          foreach ($detail->phoneList as $item) {
                            echo $item . '<br />';
                          }
                          ?>
                        </td>
                      </tr>
                      <tr style="border-top: 1px solid #ddd;">
                        <td style="font-weight: bold; padding: 8px 5px;">Fax:</td>
                        <td style="word-wrap: break-word; word-break:break-all; padding: 8px 0;">
                          <?php
                          foreach ($detail->faxList as $item) {
                            echo $item . '<br />';
                          }
                          ?>
                        </td>
                      </tr>
                      <tr style="border-top: 1px solid #ddd;">
                        <td style="font-weight: bold; padding: 8px 5px;">Web:</td>
                        <td style="word-wrap: break-word; word-break:break-all; padding: 8px 0;">
                          <?php
                          foreach ($detail->webList as $item) {
                            echo $item . '<br />';
                          }
                          ?>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              <?php } ?>
              <?php
              if ($detail->latitude != '' && $detail->longitude != '') {
              ?>
                <div id="googlemap" style="width: 100%; height: 360px; margin: 30px 0;">
                  <img src="http://maps.googleapis.com/maps/api/staticmap?zoom=16&amp;size=415x360&amp;markers=color:red|label:H|<?php echo $detail->latitude; ?>,<?php echo $detail->longitude; ?>&amp;sensor=false&amp;key=AIzaSyAN2JTbJ5ALoBrHk0DlB4TgpQI30qUhyjA" style="width: 100%;">
                </div>
                <div class="hidden-print">
                  <a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=es&amp;q=<?php echo $detail->latitude; ?>,<?php echo $detail->longitude; ?>&amp;ll=<?php echo $detail->latitude; ?>,<?php echo $detail->longitude; ?>&amp;z=16&amp;markers=color:red|label:H|<?php echo $detail->latitude; ?>,<?php echo $detail->longitude; ?>" target="_blank">Ver en el mapa</a>
                </div>
              <?php
              } else {
              ?>
                <div id="googlemap" style="width: 100%; height: 360px; margin: 30px 0;">
                  <img src="http://maps.googleapis.com/maps/api/staticmap?center=<?php echo $detail->location ?>&amp;zoom=16&amp;size=415x360&amp;sensor=false&amp;key=AIzaSyAN2JTbJ5ALoBrHk0DlB4TgpQI30qUhyjA" style="width: 100%;">
                </div>
                <div class="hidden-print">
                  <a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=es&amp;q=<?php echo $detail->location ?>&amp;ll=<?php echo $detail->latitude; ?>,<?php echo $detail->longitude; ?>&amp;z=16&amp;markers=color:red|label:H|<?php echo $detail->latitude; ?>,<?php echo $detail->longitude; ?>" target="_blank">Ver en el mapa</a>
                </div>
              <?php
              }
              ?>
            </td>
            <!-- right side -->
            <td style="width: 60%; vertical-align: top; padding: 0 15px;">
              <div class="hidden-print avoid-mailing link" style="margin-top: 10px; float: right;" data-toggle="modal" data-target=".tripadvisor-modal">
                <?php loadImage('resources/tripadvisor.png');   ?>
              </div>

              <h3 style="margin: 5px 0; color: #3399f3;"><?php echo $detail->name; ?> &nbsp; <?php showCategory($detail->categoryCode); ?></h3>
              <?php echo $dir . ' - ' . $detail->destinationZone . ', ' . $detail->destinationName; ?>
              <div style="color: #555; text-align: justify; padding: 20px 30px; ">
                <?php echo $detail->description; ?>
              </div>

              <!-- Search avail -->
              <div id="search_avail" class="col-md-12 gbox hidden-print avoid-mailing" style="margin-bottom: 15px; padding: 10px 15px;">
                <div style="font-size: 18px; color: #036d9f;">Buscar disponibilidad</div>
                <form method="get">
                  <input type="hidden" name="controller" value="accommodation" />
                  <input type="hidden" name="action" value="show" />
                  <input type="hidden" name="hotelCode" value="<?php echo $_GET['hotelCode']; ?>" />
                  <input type="hidden" name="search_avail" value="1">
                  <input type="hidden" name="destination_code" value="<?php echo $detail->destinationCode; ?>" />
                  <input type="hidden" name="page" value="1" />
                  <input type="hidden" name="providers[]" value="<?php echo BookingProvider::HOTELBEDS; ?>" />
                  <input type="hidden" name="providers[]" value="<?php echo BookingProvider::DOMITUR; ?>" />
                  <input type="hidden" name="provider" value="<?php echo $_GET['provider']; ?>">
                  <div class="row">
                    <div class="col-md-5">
                      <div class="row">
                        <div class="form-group col-md-6">
                          <label for="checkin">Entrada</label>
                          <input type="text" class="form-control" id="checkin" name="checkin" placeholder="dd/mm/aa" autocomplete="off">
                        </div>
                        <div class="form-group col-md-6">
                          <label for="checkout">Salida</label>
                          <input type="text" class="form-control" id="checkout" name="checkout" placeholder="dd/mm/aa" autocomplete="off">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-5">
                      <div class="row">
                        <div class="form-group col-md-4">
                          <label for="habitaciones">Hab.</label>
                          <select class="form-control" id="habitaciones" name="habitaciones" onchange="updateRoomsConfigPanel(); showChildAgesConfig()" value="3">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>10</option>
                          </select>
                        </div>
                        <div id="rooms_config" class="col-md-8">
                          <div class="row">
                            <div class="form-group col-md-6">
                              <label>Adultos</label>
                              <select class="form-control" id="adultos1" name="adultos1">
                                <option>1</option>
                                <option selected="selected">2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                                <option>6</option>
                                <option>7</option>
                                <option>8</option>
                              </select>
                            </div>
                            <div class="form-group col-md-6">
                              <label>Ni&ntilde;os</label>
                              <select class="form-control" id="ninos1" name="ninos1" onchange="showChildAgesConfig()">
                                <option>0</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group col-md-2">
                      <label>&nbsp;</label><br />
                      <button class="btn"><i class="fa fa-search"></i></button>
                    </div>
                    <div id="child_ages" class="form-group col-md-12"></div>
                  </div>
                </form>
                <?php
                if (isset($rs)) { ?>
                  <table class="table table-striped table-hover small">
                    <thead>
                      <tr>
                        <th>Tipo Habitaci&oacute;n</th>
                        <th>Servicios</th>
                        <th>Precio</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      foreach ($rs->hotelsAvail as $hotelAvail) {
                        $groupedRooms = $hotelAvail->groupedRooms;
                        foreach ($groupedRooms as $key => $group) {
                          $roomTypeStr  = count($group) > 1 ? '<div class="strong" style="margin-bottom:8px;">Habitaciones:</div>' : '';
                          $totalPrice   = 0;
                          $pricesStr    = '';
                          $first        = true;
                          foreach ($group as $room) {
                            if (!$first) {
                              $roomTypeStr .= '<br />';
                            }
                            // $roomPrice = Agency::getUserPrice($room->getPrice());
                            $roomPrice    = $room->priceWithFee;
                            $pricesStr    .= '<br /><span class="small">' . round($roomPrice, 2) . ' ' . $hotelAvail->currency . '</span>';
                            $roomTypeStr  .= $room->roomType;
                            $first = false;

                            $totalPrice += $room->priceWithFee;
                          }
                          // $totalPrice = Agency::getUserPrice($totalPrice);
                          $priceStr = '<span class="strong" style="white-space: nowrap;"><span class="big">' . round($totalPrice, 2) . '</span> ' . $hotelAvail->currency . '</span>';
                          $priceStr .= count($group) > 1 ? $pricesStr : '';
                          $boardStr = $group[0]->board;
                      ?>
                          <tr>
                            <td>
                              <?php if ($_SESSION['admin_role']) {
                                echo '<span class="text-muted small">(' . BookingProvider::getProviderName($group[0]->provider) . ')</span>';
                              } ?>
                              <?php echo $roomTypeStr; ?>
                            </td>
                            <td><?php echo $boardStr; ?></td>
                            <td><?php echo $priceStr; ?></td>
                            <td>
                              <form method="get" action="./" target="_blank">
                                <input type="hidden" name="service_add_data" value='<?php echo getServiceAddDataJSON($group, $childAges); ?>' />
                                <input type="hidden" name="requestCode" value="<?php echo mt_rand(); ?>" />
                                <input type="hidden" name="controller" value="shopping_cart">
                                <input type="hidden" name="action" value="show">
                                <button class="btnLoadingAction">RESERVAR</button>
                              </form>
                            </td>
                          </tr>
                      <?php
                        }
                      }
                      ?>
                    </tbody>
                  </table>
                <?php } ?>
              </div>
              <hr />

              <div style="font-size: 16px;">
                <h3 style="color: #036d9f;">Informaci&oacute;n Extra del Hotel</h3>
                <table>
                  <tbody>
                    <?php if (isset($detail->buildingFacilities)) { ?>
                      <tr style="border-top: 1px solid #ddd;">
                        <td style="font-size: 18px; color: #036d9f; padding: 8px 0;">Informaci&oacute;n</td>
                        <td style="padding: 8px 0;">
                          <ul>
                            <?php
                            foreach ($detail->buildingFacilities as $value) {
                              echo '<li>' . $value . '</li>';
                            }
                            ?>
                          </ul>
                        </td>
                      </tr>
                    <?php  } ?>
                    <?php if (isset($detail->hotelTypeFacilities)) { ?>
                      <tr style="border-top: 1px solid #ddd;">
                        <td style="font-size: 18px; color: #036d9f; padding: 8px 0;">Tipo de alojamiento</td>
                        <td style="padding: 8px 0;">
                          <ul>
                            <?php
                            foreach ($detail->hotelTypeFacilities as $value) {
                              echo '<li>' . $value . '</li>';
                            }
                            ?>
                          </ul>
                        </td>
                      </tr>
                    <?php  } ?>
                    <?php if (isset($detail->credCardsFacilities)) { ?>
                      <tr style="border-top: 1px solid #ddd;">
                        <td style="font-size: 18px; color: #036d9f; padding: 8px 0;">M&eacute;todos de pago</td>
                        <td style="padding: 8px 0;">
                          <ul>
                            <?php
                            foreach ($detail->credCardsFacilities as $value) {
                              echo '<li>' . $value . '</li>';
                            }
                            ?>
                          </ul>
                        </td>
                      </tr>
                    <?php  } ?>
                    <?php if (isset($detail->roomFacilities)) { ?>
                      <tr style="border-top: 1px solid #ddd;">
                        <td style="font-size: 18px; color: #036d9f; padding: 8px 0;">Equipamiento de habitaciones</td>
                        <td style="padding: 8px 0;">
                          <ul>
                            <?php
                            foreach ($detail->roomFacilities as $value) {
                              echo '<li>' . $value . '</li>';
                            }
                            ?>
                          </ul>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2" style="color: #999; font-size: 0.8em; padding: 5px 0;">(*) Algunos servicios serán abonados en el establecimiento</td>
                      </tr>
                    <?php  } ?>
                    <?php if (isset($detail->servicesFacilities)) { ?>
                      <tr style="border-top: 1px solid #ddd;">
                        <td style="font-size: 18px; color: #036d9f; padding: 8px 0;">Instalaciones y servicios</td>
                        <td style="padding: 8px 0;">
                          <ul>
                            <?php
                            foreach ($detail->servicesFacilities as $key => $value) {
                              echo '<li>' . $value . '</li>';
                            }
                            ?>
                          </ul>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2" style="color: #999; font-size: 0.8em; padding: 5px 0;">(*) Algunos servicios serán abonados en el establecimiento</td>
                      </tr>
                    <?php  } ?>
                    <?php if (isset($detail->cateringFacilities)) { ?>
                      <tr style="border-top: 1px solid #ddd;">
                        <td style="font-size: 18px; color: #036d9f; padding: 8px 0;">Comidas</td>
                        <td style="padding: 8px 0;">
                          <ul>
                            <?php
                            foreach ($detail->cateringFacilities as $key => $value) {
                              echo '<li>' . $value . '</li>';
                            }
                            ?>
                          </ul>
                        </td>
                      </tr>
                    <?php  } ?>
                    <?php if (isset($detail->businessFacilities)) { ?>
                      <tr style="border-top: 1px solid #ddd;">
                        <td style="font-size: 18px; color: #036d9f; padding: 8px 0;">Negocios</td>
                        <td style="padding: 8px 0;">
                          <ul>
                            <?php
                            foreach ($detail->businessFacilities as $key => $value) {
                              echo '<li>' . $value . '</li>';
                            }
                            ?>
                          </ul>
                        </td>
                      </tr>
                    <?php  } ?>
                    <?php if (isset($detail->healthBeautyFacilities)) { ?>
                      <tr style="border-top: 1px solid #ddd;">
                        <td style="font-size: 18px; color: #036d9f; padding: 8px 0;">Salud y belleza</td>
                        <td style="padding: 8px 0;">
                          <ul>
                            <?php
                            foreach ($detail->healthBeautyFacilities as $key => $value) {
                              echo '<li>' . $value . '</li>';
                            }
                            ?>
                          </ul>
                        </td>
                      </tr>
                    <?php  } ?>
                    <?php
                    if (isset($detail->distancesFacilities) > 0) { ?>
                      <tr style="border-top: 1px solid #ddd;">
                        <td style="font-size: 18px; color: #036d9f; padding: 8px 0;">Distancia</td>
                        <td style="padding: 8px 0;">
                          <ul>
                            <?php
                            foreach ($detail->distancesFacilities as $key => $value) {
                              echo "<li>$key: <strong>$value</strong></li>";
                            }
                            ?>
                          </ul>
                        </td>
                      </tr>
                    <?php } ?>

                    <?php
                    
                    if (isset($detail->highLightFacilities) > 0) { ?>
                      <tr style="border-top: 1px solid #ddd;">
                        <td style="font-size: 18px; color: #036d9f; padding: 8px 0;">Puntos de inter&eacute;s</td>
                        <td style="padding: 8px 0;">
                          <ul>
                            <?php
                            foreach ($detail->highLightFacilities as $key => $value) {
                              echo "<li>$key: <strong>$value</strong></li>";
                            }
                            ?>
                          </ul>
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- Raw base for the rooms config -->
<div id="rooms_config_raw" class="hidden invisible">
  <div class="row">
    <div class="col-md-6 col-xs-6">
      <select class="form-control" id="adultos__X" name="adultos__X">
        <option>1</option>
        <option selected="selected">2</option>
        <option>3</option>
        <option>4</option>
        <option>5</option>
        <option>6</option>
        <option>7</option>
        <option>8</option>
      </select>
    </div>
    <div class="col-md-6 col-xs-6">
      <select class="form-control" id="ninos__X" name="ninos__X" onchange="showChildAgesConfig()">
        <option>0</option>
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
      </select>
    </div>
  </div>
</div>
<div id="rooms_config_header_raw" class="hidden invisible">
  <div class="row">
    <div class="col-md-6 col-xs-6">
      <label>Adultos</label>
    </div>
    <div class="col-md-6 col-xs-6">
      <label>Ni&ntilde;os</label>
    </div>
  </div>
</div>

<div class="modal fade tripadvisor-modal" tabindex="-1" role="dialog" aria-labelledby="tripAdvisorModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Opiniones TripAdvisor</h4>
      </div>
      <div class="modal-body">
        <iframe style="width: 100%; height: 500px;"></iframe>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  habitaciones = <?php echo isset($_GET['habitaciones']) ? $_GET['habitaciones'] : 1; ?>;
  rooms = [];
  childAges = [];
  <?php
  $childsCount = 0;
  if (isset($_GET['habitaciones'])) {
    for ($i = 1; $i <= $_GET['habitaciones']; $i++) {
      echo "rooms[$i] = [" . $_GET["adultos$i"] . ", " . $_GET["ninos$i"] . "];";
      $childsCount += intval($_GET["ninos$i"]);
    }
    for ($i = 1; $i <= $childsCount; $i++) {
      echo "childAges[$i] = " . $_GET['childAge' . $i] . ";";
    }
  } else {
    echo "rooms[1] = [2, 0];";
  }
  ?>

  checkin = "<?php echo isset($_GET['checkin']) ? $_GET['checkin'] : ''; ?>";
  checkout = "<?php echo isset($_GET['checkout']) ? $_GET['checkout'] : ''; ?>";
  orderPrice = false;
  restoreAccommodationSearchValues();
  $('.tripadvisor-modal').on('shown.bs.modal', function() {
    $(this).find('iframe').attr('src', 'https://www.tripadvisor.com/WidgetEmbed-cdspropertydetail?display=true&partnerId=6CA95E2409844D6183003CEFCDDC2BDE&lang=es&locationId=<?php echo '630541'; ?>');
  })
</script>