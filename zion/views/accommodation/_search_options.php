<div class="call-action call-action-boxed call-action-style2 clearfix resumenAereo">
    <div class="row">
        <div class="col-sm-9">
            <input type="text" id="search-hotel" class="inputField" placeholder="Buscar Hotel" value="<?php echo isset($_GET['hotelName']) ? $_GET['hotelName'] : ''; ?>" style="margin-bottom: 0px;width: 100%;" />
        </div>
        <div class="col-sm-3">
            <a href="#" class="btn-system btn-medium btn-enviar" onclick="buscar_hotel();">
                <i class="fa fa-search"></i>
            </a>
        </div>
        <div class="col-md-12">
            <label for="select" class="control-label" style="margin-top:9px;">Ordenar por:</label>
            <span  style="padding-right: 0px;">
                <select id="orderPriceCombo" name="orderPriceCombo" class="form-control"  onchange="filtrarResult(1,'')">
                    <option value="ASC">Precio (menor a mayor)</option>
                    <option value="DESC">Precio (mayor a menor)</option>
                    <option value="REC">Recomendado</option>
                </select>
            </span>
        </div>
        <div class="col-md-12">
            <label for="select" class="control-label" style="margin-top:9px;">Zonas:</label>
            <span id="zones" style="padding-right:0px;"></span>
        </div>
    </div>
</div>

<div class="mini-hidden-separator"></div>

<div class="call-action call-action-boxed call-action-style2 clearfix resumenAereo">
    <div class="row">
        <div class="col-md-12">
            <style type="text/css">
              .loader { position: absolute;top: 33px;left: 66px;margin: 10px auto 150px auto; border: 16px solid #ccc; border-top: 16px solid #3498db; border-radius: 50%; width: 120px; height: 120px; animation: spin 2s linear infinite; }
              @keyframes spin { 0% { transform: rotate(0deg); } 100% { transform: rotate(360deg); } }
            </style>

            <div class="item">
                <div id="googlemap" style="cursor:pointer;" onclick="abrirmodal()">
                  <?php $total= strpos(_get('hotel_destination'),')'); $text = substr(_get('hotel_destination'), 0,($total+1))?>
                  <div class="div-mapa" style=""><?php echo $text; ?></div>
                  <img src="" id="img_maps" style="display:none;width:100%;border-radius: 15px;">
                </div>
                <div class="loadin_action" style="height:170px;">
                  <img src="../zion/assets/images/progress.gif" style="position:absolute;top:63px;left:85px;" />
                </div>
            </div>
        </div>

        <div class="mini-hidden-separator"></div>

        <div class="col-md-12">
            <div class="legend">
                <span style="font-weight:600;letter-spacing: .03em;">Estrellas</span>
            </div>
            <div class="items">
                <?php
                    $star_1 = false;
                    $star_2 = false;
                    $star_3 = false;
                    $star_4 = false;
                    $star_5 = false;
                    $star_other = false;
                    if (isset($_GET['star'])) {
                      $star = $_GET['star'];
                      $star_1 = in_array("1", $star);
                      $star_2 = in_array("2", $star);
                      $star_3 = in_array("3", $star);
                      $star_4 = in_array("4", $star);
                      $star_5 = in_array("5", $star);
                    }
                ?>
                <div>
                    <label class="container-check"> <?php showCategory('1EST'); ?>
                        <input type="checkbox" name="star[]" id="s1" class="star" value="1" <?php if ($star_1) echo 'checked="true"'; ?> >
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div>
                    <label class="container-check"> <?php showCategory('2EST'); ?>
                        <input type="checkbox" name="star[]" id="s2" class="star" value="2" <?php if ($star_2) echo 'checked="true"'; ?> >
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div>
                    <label class="container-check"> <?php showCategory('3EST'); ?>
                        <input type="checkbox" name="star[]" id="s3" class="star" value="3" <?php if ($star_3) echo 'checked="true"'; ?> >
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div>
                    <label class="container-check"> <?php showCategory('4EST'); ?>
                        <input type="checkbox" name="star[]" id="s4" class="star" value="4" <?php if ($star_4) echo 'checked="true"'; ?> >
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div>
                    <label class="container-check"> <?php showCategory('5EST'); ?>
                        <input type="checkbox" name="star[]" id="s5" class="star" value="5" <?php if ($star_5) echo 'checked="true"'; ?> >
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>
        </div>

        <div class="mini-hidden-separator"></div>

        <div class="col-md-12">
            <?php
                $pen_compl  = false;
                $aloj_des   = false;
                $med_pensi  = false;
                $solo_habit = false;
                $todo_inclu = false;

                if(isset($_GET['regimen'])) {
                  $regimen    = $_GET['regimen'];
                  $pen_compl  = in_array("FB", $regimen);
                  $aloj_des   = in_array("BB,AD", $regimen);
                  $med_pensi  = in_array("HB,MP", $regimen);
                  $solo_habit = in_array("RO,SA", $regimen);
                  $todo_inclu = in_array("AI,TI", $regimen);
                }
            ?>
            <div class="legend">
              <span style="font-weight:600;letter-spacing: .03em;">Regimen</span>
            </div>
            <div class="items">
              <div>
                <label class="container-check"> Pensi&oacute;n Completa
                  <input type="checkbox" name="regimen[]" id="r1" class="regimen" value="FB" <?php echo ($pen_compl) ? "checked" : ''; ?> />
                  <span class="checkmark"></span>
                </label>
              </div>
              <div>  
                <label class="container-check"> Alojamiento y Desayuno
                  <input type="checkbox" name="regimen[]" id="r2" class="regimen" value="BB,AD" <?php echo ($aloj_des) ? "checked" : ''; ?> />
                  <span class="checkmark"></span>
                </label>
              </div>
              <div>  
                <label class="container-check"> Media Pensi&oacute;n
                  <input type="checkbox" name="regimen[]" id="r3" class="regimen" value="HB,MP" <?php echo ($med_pensi) ? "checked" : ''; ?> />
                  <span class="checkmark"></span>
                </label>
              </div>
              <div>  
                <label class="container-check"> Solo Habitaci&oacute;n
                  <input type="checkbox" name="regimen[]" id="r4" class="regimen" value="RO,SA" <?php echo ($solo_habit) ? "checked" : ''; ?> />
                  <span class="checkmark"></span>
                </label>
              </div>
              <div>  
                <label class="container-check"> Todo Incluido
                  <input type="checkbox" name="regimen[]" id="r5" class="regimen" value="AI,TI" <?php echo ($todo_inclu) ? "checked" : ''; ?> />
                  <span class="checkmark"></span>
                </label>
              </div>
            </div>
        </div>

        <div class="item col-md-12">
            <input type="submit" class="btn-fil" name="filter-btn" id="filter-btn" value="Filtrar" onclick="buscar_hotel()">
        </div>
    </div>
</div>

<div class="mini-hidden-separator"></div>

<!-- Raw base for the rooms config -->
<div id="rooms_config_raw" class="hidden invisible">
    <div class="row">
        <div class="col-md-6 col-xs-6">
            <select class="form-control" id="adultos__X" name="adultos__X">
                <option>1</option><option selected="selected">2</option><option>3</option><option>4</option>
                <option>5</option><option>6</option><option>7</option><option>8</option>
            </select>
        </div>
        <div class="col-md-6 col-xs-6">
            <select class="form-control" id="ninos__X" name="ninos__X" onchange="showChildAgesConfig()">
                <option>0</option><option>1</option><option>2</option><option>3</option><option>4</option>
            </select>
        </div>
    </div>
</div>
<div id="rooms_config_header_raw" class="hidden invisible">
    <div class="row">
        <div class="col-md-6 col-xs-6">
          <label>Adultos</label>
        </div>
        <div class="col-md-6 col-xs-6">
          <label>Ni&ntilde;os</label>
        </div>
    </div>
</div>

<script type="text/javascript">
  habitaciones  = parseInt(<?php echo $_GET['habitaciones']; ?>);
  rooms         = [];
  childAges     = [];
  <?php
  $childsCount = 0;
  for ($i = 1; $i <= $_GET['habitaciones']; $i++) {
    echo "rooms[$i] = [" . $_GET["adultos$i"] . ", " . $_GET["ninos$i"] . "];";
    $childsCount += $_GET["ninos$i"];
  }
  for ($i = 1; $i <= $childsCount; $i++) {
    echo "childAges[$i] = " . $_GET['childAge' . $i] . ";";
  }
  ?>
  checkin = "<?php echo _get('checkin'); ?>";
  checkout = "<?php echo _get('checkout'); ?>";
  zoneCode = "<?php echo _get('zoneCode'); ?>";
  orderPrice = "<?php echo _get('orderPrice'); ?>";

  function verifySearch() {
    // Clear hotelName if Destination has changed
    if ($('#destination_code').val() != '<?php echo $_GET["destination_code"]; ?>') {
      $('#hotelName').val('');
      $('#centerLat').val('');
      $('#centerLon').val('');
    }
  }

  function saveDatesSubmit() {
    hotelName = $('#search-hotel').val();
    $('#filter_hotelName').val(hotelName);
    // para leer los filtros de Board
    inputReg = $('input[class=regimen]')
    for (var i = 0;i < inputReg.length;i++) {
      id = $(inputReg[i]).attr('id');
      $('#reg_' + id).prop('checked', false)
      if($(inputReg[i]).prop('checked')) {
        $('#reg_' + id).prop('checked', true)
      }
    }
    // end
    // para leer los filtros de Category
    inputStar = $('input[class=star]')
    for (var i = 0;i < inputStar.length;i++) {
      id = $(inputStar[i]).attr('id');
      $('#star_' + id).prop('checked', false)
      if($(inputStar[i]).prop('checked')) {
        $('#star_' + id).prop('checked', true)
      }
    }
    // end
    //alert("PRUEBA");
    return true;
  }

  function filtrarResult(page, nombre = '') {
    $('.hotel-avail-res').html("");
    $('#contentNoResult').hide()
    $('.hotelLoadingRes').show();
    var userToken = <?php echo $_SESSION['current_user']->id; ?>;
    nombre        = $('#search-hotel').val();
    orderPrice    = $('#orderPriceCombo').val()
    tokenAvail    = $('#tokenAvail').val()
    star      = $('input:checkbox[class=star]:checked')
    estrellas = []
    for(var i = 0; i < star.length; i++) {
      estrellas.push($(star[i]).val())
    }
    regimen     = $('input:checkbox[class=regimen]:checked')
    regimenArr  = []
    for(var i = 0; i < regimen.length; i++) {
      regimenArr.push($(regimen[i]).val())
    }
    last_version = ''
    if ($('#last-version').prop('checked')) {
     last_version = 'allResult'
    }
    $.ajax({
        url: 'helpers/ajax/paginate_results.php',
        type: 'GET',
        data: {
                page:page, tokenAvail:tokenAvail, $userToken:userToken, nombre:nombre, 
                orderPrice:orderPrice,star:estrellas, regimen:regimenArr, last_version:last_version
              },
          success: function(res) {
            hotelAvailRes = JSON.parse(res);
            if (hotelAvailRes.rs == 'false') {
              $('#expiredTime').modal()
            }
            else {
              cleanMarkers()
              _params     = hotelAvailRes.params;
              _childAges  = hotelAvailRes.childAges;
              currentPage = parseInt(hotelAvailRes.rs.currentPage);
              totalPages  = parseInt(hotelAvailRes.rs.totalPages);
              createView(hotelAvailRes.rs.hotelsAvail);
              cargar_hoteles(hotelAvailRes.rs.hotelsAvail);
              center = getCenterPosition();
              $('#googlemap img').show();$('.loadin_action').hide();
              $('#googlemap img').attr('src','http://maps.googleapis.com/maps/api/staticmap?center='+center.lat()+','+center.lng()+'&zoom=11&size=415x360&sensor=false&key=AIzaSyAN2JTbJ5ALoBrHk0DlB4TgpQI30qUhyjA');
              verifySelectCompare()
              $('.hotelLoadingRes').hide();
            } 
            
          },
          error: function(err) {
            alert('Lo sentimos, la pagina no pudo ser cargada correctamente, favor intente nuevamente o ponganse en contacto con nosotros.');
          }
      });
  }

  function inputSelectOption(id, prefijo, valor) {
    if ($('#' + id).is(':checked')) {
      $('#' + prefijo + '_' + id).attr('checked', true)
    }
    else {
      $('#' + prefijo + '_' + id).removeAttr('checked', true)
    }
  }

</script>
