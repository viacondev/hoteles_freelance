<?
include_once('../../../config/Config.php');
include_once('../../database/DB.php');
include_once('../../model/Model.php');
include_once('../../model/User.php');
include_once('../../model/Agency.php');
include_once('../../model/DomiturHotelMapping.php');
include_once('../../../booking/BookingEngine.php');
include_once('../../helpers/application_helper.php');
include_once('../../helpers/accommodation_helper.php');
include_once('../../helpers/logger.php');
session_start();
date_default_timezone_set('America/La_Paz');

$params = $_GET['params'];
$childAges = isset($_GET['childAges']) ? $_GET['childAges'] : array();
$booking = new BookingEngine();
$rs = $booking->execRequest($params);

if (count($rs->getHotelsAvail()) == 0) {
  ?>
  <div class="col-md-12 wbox" style="padding: 30px 20px; margin-bottom: 20px;">
    Lo sentimos, no se encontr&oacute; ning&uacute;n hotel para tu b&uacute;squeda.
  </div>
  <?
}
else {
  if (!isset($_GET['view_mode']) || $_GET['view_mode'] == 'resume') {
    include('_search_results_resume.php');
  }
  else if ($_GET['view_mode'] == 'list') {
    include('_search_results_list.php');
  }
  else if ($_GET['view_mode'] == 'photos') {
    include('_search_results_photos.php');
  }
  // else if ($_GET['view_mode'] == 'map') {
  //   include('_search_results_map.php');
  // }
}
?>

<script type="text/javascript">
  totalPages = <?php echo $rs->getTotalPages(); ?>;
  currentPage = <?php echo $rs->getCurrentPage(); ?>;
  initLoadingMsg();
</script>
