<div class="col-md-12">
  <?php
  $hotelCodes = array();
  foreach ($rs->getHotelsAvail() as $hotelAvail) {
    if (in_array($hotelAvail->getCode(), $hotelCodes)) {
      continue;
    }
    $hotelCodes[] = $hotelAvail->getCode();
    $groupedRooms = $hotelAvail->getGroupedRooms();

    $price = 0;
    foreach ($groupedRooms[0] as $room) {
      $price += floatval($room->getPrice());
    }
    $price = round(Agency::getUserPrice($price), 2);
    $hotelLink = getHotelLink($hotelAvail);
    ?>
    <div class="col-md-4 hotel_avail">
      <div style="height: 340px; background-color: #fff; border-radius: 8px; margin-bottom: 20px; overflow: hidden;">
        <img src="<?php echo $hotelAvail->getMainImage(); ?>" style="width: 100%; height: 160px;"/>
        <div style="height: 140px; padding-top: 15px;">
          <div class="col-md-12">
            <a href="<?php echo $hotelLink; ?>"><?php echo $hotelAvail->getName() ?></a>
          </div>
          <div class="col-md-12">
            <span class="hotel-category" data-toggle="tooltip" data-placement="top" data-original-title="<?php echo $hotelAvail->getCategory(); ?>">
              <?php showCategory($hotelAvail->getCategoryCode()); ?>
            </span>
          </div>

          <div class="col-md-12 small">
            <i class="fa fa-map-marker"></i> <?php echo $hotelAvail->getLocation(); ?>
          </div>

          <div class="col-md-12">
            Precio desde: <big><strong><?php echo $price . ' ' . $hotelAvail->getCurrency(); ?></strong></big>
          </div>
        </div>

        <div style="background-color: #036d9f; height: 40px; text-align: center; font-size: 18px; padding-top: 10px; font-weigth: bold;">
          <a href="<?php echo $hotelLink ;?>#search_avail" style="color: #fff;" class="loadingAction">Ver Tarifas</a>
        </div>
      </div>
    </div>
    <?
  }
  ?>
</div>