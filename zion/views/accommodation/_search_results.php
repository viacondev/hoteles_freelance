<div id="resultbar" class="wbox">
  <div class="row">
    <div class="col-md-3">
      <div class="view_mode_menu">
        Vista:
        <span class="fa-stack <?php if ($_GET['view_mode'] == 'resume') echo 'active'; ?>" onclick="$('#view_mode').val('resume'); view_mode = 'resume'; loadHotels();">
          <i class="fa fa-square fa-stack-2x"></i><i class="fa fa-th-list fa-stack-1x fa-inverse"></i>
        </span>
        <span class="fa-stack <?php if ($_GET['view_mode'] == 'list') echo 'active'; ?>" onclick="$('#view_mode').val('list'); view_mode = 'list'; loadHotels();">
          <i class="fa fa-square fa-stack-2x"></i><i class="fa fa-align-justify fa-stack-1x fa-inverse"></i>
        </span>
        <span class="fa-stack <?php if ($_GET['view_mode'] == 'photos') echo 'active'; ?>" onclick="$('#view_mode').val('photos'); view_mode = 'photos'; loadHotels();">
          <i class="fa fa-square fa-stack-2x"></i><i class="fa fa-th fa-stack-1x fa-inverse"></i>
        </span>
        <!-- <span class="fa-stack <?php //if ($_GET['view_mode'] == 'map') echo 'active'; ?>" onclick="$('#view_mode').val('map'); view_mode = 'map'; loadHotels();">
          <i class="fa fa-square fa-stack-2x"></i><i class="fa fa-map-marker fa-stack-1x fa-inverse"></i>
        </span> -->
      </div>
    </div>

    <div class="col-md-9 text-right">
      Zonas:
      <span id="zones"></span>
       <span class="small text-muted">&nbsp; / &nbsp; </span>
      Ordenar por:
      <select id="orderPriceCombo" onchange="$('#orderPrice').val(value); $('#page').val(1); $('#refine-form').submit();">
        <option value="ASC">Precio (menor a mayor)</option>
        <option value="DESC">Precio (mayor a menor)</option>
      </select>
    </div>

    <div class="clearfix"></div>

    <div class="col-md-3" style="margin-top: 10px;">
      <input type="text" id="search-hotel" placeholder="Buscar Hotel" onkeyup="buscar_hotel()">
    </div>

  </div>

</div>

<div id="loadingHotels" style="display: none;">
  <div class="wbox" style="margin-bottom: 15px; padding: 15px;"><? loadImage('ajax-loader.gif'); ?>   &nbsp; Cargando hoteles... <span class="small text-muted">La vista se actualizar&aacute; cuando la carga termine</span></div>
</div>

<div id="results_container">
  <? if (count($rs->getHotelsAvail()) == 0) {
    ?>
    <div class="col-md-12 wbox" style="padding: 30px 20px; margin-bottom: 20px;">
      Lo sentimos, no se encontr&oacute; ning&uacute;n hotel para tu b&uacute;squeda.
    </div>
    <?
  }
  else {
    if (!isset($_GET['view_mode']) || $_GET['view_mode'] == 'resume') {
      include('_search_results_resume.php');
    }
    else if ($_GET['view_mode'] == 'list') {
      include('_search_results_list.php');
    }
    else if ($_GET['view_mode'] == 'photos') {
      include('_search_results_photos.php');
    }
    // else if ($_GET['view_mode'] == 'map') {
    //   include('_search_results_map.php');
    // }
  }
  ?>
</div>

<style type="text/css">
  .modal-dialog-center { margin-top: 200px; }
</style>
<div id="loading-search" class="modal modal-dialog-center">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" style="overflow: hidden;">
      <div class="modal-body" style="background: url(assets/images/wait.jpg); height: 200px; position: relative;">
        <div style="font-size: 22px; text-align: center;">
          Cargando...
          <div style="">
            <div class="progress progress-striped active" style="margin-bottom: 0">
              <div class="progress-bar progress-bar-info" role="progressbar"
                   aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                   style="width: 100%">
              </div>
            </div>
            <div id="loading_status" style="font-size: 12px;">Buscando hoteles...</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="scroll_down_loading" class="col-md-3 col-md-offset-5 wbox text-center" style="padding: 15px; margin-top: 20px; display: none;">
  <? loadImage('ajax-loader.gif'); ?>   &nbsp; &nbsp; Cargando m&aacute;s hoteles...
</div>

<!-- <div id="pagination" class="col-md-12 wbox text-center collapse"></div> -->
<div id="load-cotizacion" style="display:none">
  <div class="close" onclick="$('#load-cotizacion').hide('drop', {direction: 'right'});">
    <i class="fa fa-times"></i>
  </div>
  <div id="cotizacion-item-1" class="compare-item"><?php loadImage('no-item.png'); ?></div>
  <div id="cotizacion-item-2" class="compare-item"><?php loadImage('no-item.png'); ?></div>
  <div id="cotizacion-item-3" class="compare-item"><?php loadImage('no-item.png'); ?></div>
  <div id="cotizacion-item-4" class="compare-item">
    <label style="color:white;color: white;font-size: 22px;background-color: #455;padding: 12px 10px;text-align:center;min-width: 50px;display:none">+10</label>
    <?php loadImage('no-item.png'); ?>
  </div>

  <button type="button" class="btn btn-success btn-lg" onclick="view_select()">Cotizar</button>
</div>

<div id="compare-preview">
  <div class="close" onclick="$('#compare-preview').hide('drop', {direction: 'right'});">
    <i class="fa fa-times"></i>
  </div>
  <div id="compare-item-1" class="compare-item"><?php loadImage('no-item.png'); ?></div>
  <div id="compare-item-2" class="compare-item"><?php loadImage('no-item.png'); ?></div>
  <div id="compare-item-3" class="compare-item"><?php loadImage('no-item.png'); ?></div>
  <div id="compare-item-4" class="compare-item"><?php loadImage('no-item.png'); ?></div>
  <button type="button" class="btn btn-success btn-lg" onclick="showComparation()">Comparar</button>
</div>

<div id="compare-modal" class="modal fade hidden-print">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Comparaci&oacute;n Hoteles</h4>
      </div>
      <div class="modal-body">
        <div class="col-md-12 text-right">
          <div class="link" onclick="showSendMail('Hoteles en <?php echo $_GET['destination'] . '. Del ' . $_GET['checkin'] . ' al ' . $_GET['checkout'] ?>.', 'hotels-comparation');">
            <span class="fa-stack fa-2x">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
            </span>
          </div>
          <div class="link" onclick="$('#hotels-comparation').printThis()">
            <span class="fa-stack fa-2x">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-print fa-stack-1x fa-inverse"></i>
            </span>
          </div>
        </div>
        <div id="hotels-comparation"></div>
        <div class="clearfix"></div>
      </div>
      <div class="modal-footer">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- MODAL D COTIZACIONES -->
<div id="cotizacion-modal" class="modal fade hidden-print">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Modal</h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-danger" id="div_infante" style="display:none">
            Notese que en esta cotizaci&oacute;n hay menores, por el cual pueden alterar los precios.
        </div>
        <div>
          <h3 class="text-center" style="color:#3399f3">Cotizaci&oacute;n de Hoteles</h3>
        </div>

      <div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab" onclick="$('#div_btn_cotcrear').show();$('#div_btn_cotactual').hide();" >Nuevo</a></li>
          <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" onclick="$('#div_btn_cotcrear').hide();$('#div_btn_cotactual').show();">Existente</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="home">
            <div class="col-md-12" style="padding: 10px;font-size:15px;  border-right: 1px solid #ddd;border-bottom: 1px solid #ddd;border-left: 1px solid #ddd;border-radius: 5px;margin-bottom: 5px;">
              <div class="col-md-4">
                <label>Titulo de Cotizaci&oacute;n</label>
                <input type="text" id="ntitulo_cotizacion" class="form-control requiere" placeholder="Titulo" onkeyup="validar_input_vacios(this.id);">
              </div>
              <div class="col-md-4">
                <input type="hidden" id="ncod_cliente" value="">
                <label>Cliente</label>
                <input type="text" id="name_cliente" class="form-control requiere">
                <div id="load-cliente" class="load-cliente"  style="display:none;top: 33px;position: absolute;right: 28px;">
                  <? loadImage('ajax-loader.gif'); ?><br>
                </div>
                <label><input type="radio" name="check_cliente" id="check_natural" checked="checked" onclick="$('#solicitante').hide();$('#name_cliente, #ncod_cliente, #name_solicitante, #ncod_solicitante').val('');"> Natural</label>&nbsp;&nbsp;
                <label><input type="radio" name="check_cliente" id="check_agencia" onclick="$('#solicitante').show();$('#name_cliente, #ncod_cliente').val('');"> Empresa o Agencia</label>
              </div>

              <div class="col-md-4" id="solicitante" style="display:none">
              <input type="hidden" id="ncod_solicitante" value="">
                <label>Solicitante</label>
                <input type="text" id="name_solicitante" class="form-control requiere">
              </div>

              <div class="clearfix"></div>
              <div class="col-md-4">
                <label>Ciudad de Salida</label>
                <input type="text" id="nciudad_salida" class="form-control requiere" placeholder="COD (IATA)" onkeyup="validar_input_vacios(this.id);">
              </div>

              <div class="col-md-4">
                <label>Salida</label>
                <input type="text" id="nsalida_fecha" class="form-control requiere" placeholder="dd/mm/aa" autocomplete="off">
              </div>

              <div class="col-md-4">
                <label>Retorno</label>
                <input type="text" id="nentrada_fecha" class="form-control requiere" placeholder="dd/mm/aa" autocomplete="off">
              </div>

              <div class="col-md-6">
                <label>Observacion</label>
                <textarea name="observacion_main" id="observacion_main" class="form-control" cols="10"></textarea>
              </div>

            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="profile">
            <div class="col-md-12" style=" min-height: 160px;padding: 10px;font-size:15px;  border-right: 1px solid #ddd;border-bottom: 1px solid #ddd;border-left: 1px solid #ddd;border-radius: 5px;margin-bottom: 5px;">
              <div class="col-md-4">
                <label>Cotizacion o numero</label>
                <input type="hidden" name="id_cotizacion" id="id_cotizacion" placeholder="Nombre o numero">
                <input type="text" id="name_cotizacion" class="form-control  requiere-validar" onkeyup="validar_input_vacios(this.id)">
                <a onclick="$('#name_cotizacion, #b_name_cliente').val('');" style="cursor:pointer">Limpiar Seleccion</a>
              </div>

              <div class="col-md-4">
                <label>Cliente</label>
                <input type="text" id="b_name_cliente" class="form-control  requiere-validar" placeholder="Nombre Cliente" onkeyup="validar_input_vacios(this.id)">
              </div>

              <div class="col-md-4" style="margin-top:25px">
                <input type="button" class="btn btn-default" value="Buscar" onclick="BuscarCotizacion()">
              </div>

              <div class="clearfix"></div>
              <div class="col-md-12" style="">
                <h3>Ciudades de Origen</h3>
                <div class="btn-group" data-toggle="buttons" id="contenido_ciudades">
                </div>
              </div>

              <div class="col-md-12" style="margin-top: 10px;">
                <label>
                  <input type="checkbox" name="sobre_escribir" id="sobre_escribir"> Sobreescibir
                </label>
              </div>

              <div class="col-md-12" id="container_cotizacion" style="display:none;padding-top:10px;top:86px;position: absolute;z-index: 100;background-color: white;border: 1px solid #ddd;height:500px;overflow:scroll">
                <div id="load-buscar-cotizacion" class="load-buscar-cotizacion"  style="display:block;text-align:center;">
                  <? loadImage('ajax-loader.gif'); ?><br>
                  Buscando....
                </div>
                <i class="fa fa-times pull-right" style="color: red;font-size: 20px;cursor: pointer;" onclick="$('#container_cotizacion').hide()">Ocultar</i>
                <div id="resultado_cotizacion"></div>
              </div>

            </div>
          </div>
        </div>

      </div>

      <div class="col-md-12" style="margin-top:10px;padding: 10px;font-size:15px;border: 1px solid #ddd;border-radius: 5px;margin-bottom: 5px;">
        <div class="col-md-12">
          <strong>Destino:</strong>&nbsp;
          <span id="ct_destino"><?php echo _get('hotel_destination'); ?></span>
        </div>
        <div class="col-md-12">
          <strong>Entrada:&nbsp;</strong>&nbsp;<span ><?php echo date('d/m/Y',strtotime($params['checkin'])); ?></span>&nbsp;&nbsp;
          <strong>Salida:&nbsp;</strong> &nbsp;<span ><?php echo date('d/m/Y',strtotime($params['checkout'])); ?></span>
                  <small id="pr_noches">
                    <?php
                      if (isset($_GET['checkin']) && isset($_GET['checkout'])) {
                        $from = new DateTime(dateFormatForDB(_get('checkin')));
                        $to = new DateTime(dateFormatForDB(_get('checkout')));
                        $nights = $to->diff($from)->days;
                        echo "($nights noches)";
                      }
                    ?>
                  </small>&nbsp;<input type="hidden" name="noche" id="noche" value="<? echo $nights; ?>"><!--ocupado para cotizacion -->
        </div>
      </div>

      <div id="hotels-new-cotizacion"></div>
      <div id="hotels-cotizacion" style="display:none"></div>
      <div id="alert_moneda" class="col-md-12" style="color:red;font-weight:bold;display:none">
        Tomar en cuenta el tipo de moneda al enviar esta cotizaci&oacute;n
      </div>
      <div class="col-md-4" id="div_btn_cotcrear" style="margin-top:20px;">
        <input type="button" id="actualizar_cot" class="btn btn-success" value="Crear Cotizacion" onclick="crear_nueva_cotizacion()">
      </div>

      <div class="col-md-4" id="div_btn_cotactual" style="margin-top:20px;display:none">
        <input type="button" class="btn btn-success" value="Actualizar" onclick="Update_Cotizacion()">
      </div>
      <div class="col-md-12" id="loading" style="text-align:center;display:none">
        <? loadImage('ajax-loader.gif'); ?><br>
        Guardando Cotizacion....
      </div>
      <div class="col-md-12" id="confirm" style="margin-top: 20px;background-color: #3cb521;border-radius: 5px;padding: 10px;text-align: center;color: white;display:none">
        se guardaron todos los hoteles
      </div>

      <div class="col-md-12" id="error" style="margin-top: 20px;background-color: #EC3E1F;border-radius: 5px;padding: 10px;text-align: center;color: white;display:none">
          Hubo problemas al guardar la cotizacion
      </div>

      <div class="clearfix"></div>

      </div>
      <div class="modal-footer">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- end de modal de cotizaciones -->

<script type="text/javascript">
  totalPages = <?php echo $rs->getTotalPages(); ?>;
  currentPage = <?php echo $rs->getCurrentPage(); ?>;
  initLoadingMsg();
</script>

<!-- <script type="text/javascript">
  search_params = <? echo json_encode($params); ?>;
  view_mode = '<? echo _get("view_mode"); ?>';

  function loadHotels() {
    centerLat = $('#centerLat').val();
    centerLon = $('#centerLon').val();
    maxDist = $('#maxDist').val();
    $('#loadingHotels').show();

    // profilingInit = new Date().toISOString().slice(0, 19).replace('T', ' ');
    $.ajax({
      url: "views/accommodation/_ajax_results.php",
      data: {
        view_mode: view_mode,
        params: search_params,
        childAges: <? echo json_encode($childAges); ?>,
        centerLat: centerLat,
        centerLon: centerLon,
        maxDist: maxDist
      }
    })
      .done(function(r) {
        $('#loading-search').modal('hide');
        $('#results_container').html(r);
        $('#loadingHotels').hide();
        // initPagination();
        if (view_mode == 'map') {
          $('#map-filter').show();
        }
        else {
          $('#map-filter').hide();
        }

        // Profiling
        // profilingEnd = new Date().toISOString().slice(0, 19).replace('T', ' ');
        // $.ajax({
        //   url: "helpers/ajax/profiling.php",
        //   data: {
        //     init: profilingInit,
        //     end: profilingEnd,
        //     action: 'SearchHotelAvail',
        //     origin: 'Client',
        //     size: r.length
        //   }
        // })
      });
  }

  $(function() {
    $('#loading-search').modal();
    loadHotels();

    setTimeout(function() { $('#loading_status').html('Comparando mejores tarifas...') }, 5000);
    setTimeout(function() { $('#loading_status').html('Verificando disponibilidad...') }, 2000);
  });

  var can_load_more_hotels = true;
  var exists_hidden_hotel_groups = true;
  var current_hotel_group = 0;
  $(window).scroll(function() {
    // Header stick - Exec it again to avoid overriding the event.
    stickHeaderOnScroll();
    // show more hidden hoteles
    // 'Lazy' showing the hoteles currently disabled
    exists_hidden_hotel_groups = false;
    // if(exists_hidden_hotel_groups && $(window).scrollTop() > $(document).height() - $(window).height() - 380) {
    //   current_hotel_group++;
    //   if ($('.hotel_group_' + currentPage + '_' + current_hotel_group).length > 0) {
    //     $('.hotel_group_' + currentPage + '_' + current_hotel_group).fadeIn();
    //   }
    //   else {
    //     exists_hidden_hotel_groups = false;
    //   }
    // }

    // Load more hotels
    if(!exists_hidden_hotel_groups && can_load_more_hotels && view_mode != 'map' && $(window).scrollTop() > $(document).height() - $(window).height() - 800) {
      if (typeof currentPage != 'undefined' && currentPage < totalPages) {
        var nextPage = currentPage + 1;
        can_load_more_hotels = false;
        search_params.page = nextPage;
        $('#scroll_down_loading').show();
        $.ajax({
          url: "views/accommodation/_ajax_results.php",
          data: {
            view_mode: view_mode,
            params: search_params,
            childAges: <? echo json_encode($childAges); ?>
          }
        })
          .done(function(r) {
            $('#scroll_down_loading').hide();
            $('#results_container').append(r);
            can_load_more_hotels = currentPage < totalPages ? true : false;
            exists_hidden_hotel_groups = true;
            current_hotel_group = 0;
          });
      }
    }
  });
</script> -->
