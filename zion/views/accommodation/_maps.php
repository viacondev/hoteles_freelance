<!--modal de localizacion de mapas -->


<div id="maps_hotel" class="modal ">
    <div class="modal-dialog modal-lg pantalla_completa" id="resize">
        <div class="modal-content" style="">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Localizaci&oacute;n de Hoteles</h4>
            </div>
            <div class="modal-body" style="padding:0px">
                <div class="col-md-12" id="options_markers" style="margin-top: 11px;padding: 7px 0px 0px 0px;">
                    <div class="col-md-1">
                    </div>

                    <div class="col-md-2" style="">
                        <label>
                            <input type="checkbox" id="showRadius" onchange="toggleRadiusMarker();" > Radio m&aacute;ximo
                        </label>
                    </div>

                    <div class="col-md-3">
                        <label>
                          <input type="checkbox" id="showOnlyWithinRadio" onchange="toggleMarkersOutsideRadius();"> S&oacute;lo hoteles dentro del radio
                        </label>
                    </div>

                    <div class="col-md-2 solo-viacon">
                        <label>
                            <input type="checkbox" id="solo_cotizados" onchange="mostrar_cotizados();"> <?php echo ($user_viacon) ? "S&oacute;lo Cotizados" : "S&oacute;lo Seleccionados"; ?>
                        </label>
                    </div>

                    <div class="col-md-2">
                        Radio: <input type="number" id="maxDist" name="maxDist" onchange="updateMaxDistRadius()" value="5" style="width: 40px;"> Km.
                    </div>

                    <div class="col-md-2 link div-print" >
                        <span class="fa-stack fa-2x" onclick="printMap()">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-print fa-stack-1x fa-inverse"></i>
                        </span>
                        <?php  if($user_viacon) { ?>
                              <button class="btn btn-info-css" name="cotizar" onclick="view_select();">Cotizar</button>
                        <?php  } ?>
                    </div>
                </div>

                <div id="content-information" style="display:none">
                    <div class="text-center" style="text-align:center"><h2 class="ct_destino"><?php echo _get('hotel_destination'); ?></h2></div>
                    <div class="col-md-12" style="margin-top:10px;padding: 10px;font-size:15px;border: 1px solid #7B7777;border-radius: 5px;margin-bottom: 5px;">
                        <div class="col-md-12">
                            <strong>Ocupaci&oacute;n :</strong>&nbsp;
                            <span id="pr_habitaciones"></span>
                            <span id="pr_adultos"></span>
                            <span id="pr_ninos"></span>
                        </div>
                        <div class="col-md-12">
                            <strong>Entrada:&nbsp;</strong>&nbsp;<span ><?php echo _get('checkin');  ?></span>&nbsp;&nbsp;
                            <strong>Salida:&nbsp;</strong> &nbsp;<span ><?php echo _get('checkout'); ?></span>
                            <small id="pr_noches">
                                <?php
                                  if (isset($_GET['checkin']) && isset($_GET['checkout'])) {
                                    $from = new DateTime(dateFormatForDB(_get('checkin')));
                                    $to = new DateTime(dateFormatForDB(_get('checkout')));
                                    $nights = $to->diff($from)->days;
                                    echo "($nights noches)";
                                  }
                                ?>
                            </small>
                        </div>
                    </div>
                    <div id="list-hotel-select" style="margin-top:10px;padding: 10px;font-size:15px;border: 1px solid #7B7777;border-radius: 5px;margin-bottom: 5px;"></div>
                </div>

                <div class="clearfix"></div>
                <div id="googlemapview_container" class="mapNormalScreen">
                    <span style='color:red;margin-left:20px;' id='mensaje-map'><strong>Nota:</strong> Tomar en cuenta que la ubicaci&oacute;n de cada hotel es proporcionada por los proveedores</span>
                    <input id="search-input" class="controls hidden-print" type="text" placeholder="Buscar un lugar...">
                    <div id="googlemapview" class="map" style="height:500px"></div>
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">

//Seccion de Mapas
var centerLat      = '';
var centerLon      = '';
var hotels_maps    = [];
var marcadores     = [];
var Items_visible  = [];
var maxDist        = 5;
var hotel_pos      = 1;
var canLoadHotels  = true;
function abrirmodal() {
  $('#maps_hotel').modal()
  canLoadHotels = false;
  var center    = map.getCenter();
  google.maps.event.trigger(map, "resize");
  map.setCenter(getCenterPosition());
}

function getCenterPosition() {
  hotelAvail = hotelAvailRes.rs.hotelsAvail;
  for (i = 0;i < hotelAvail.length;i++) {
    if(hotelAvail[i].latitude != '' && hotelAvail[i].longitude != '') {
      centerLat = hotelAvail[i].latitude;
      centerLon = hotelAvail[i].longitude;
      break;
    }
  }
  return new google.maps.LatLng(centerLat, centerLon);
}

function initialize() {
  var mapOptions = {
    zoom: 11,
    center: new google.maps.LatLng(centerLat, centerLon),
    gestureHandling: 'greedy',
    mapTypeControl: false,
    fullscreenControl: false
  };
  map = new google.maps.Map(document.getElementById('googlemapview'), mapOptions);

  radiusMarker = new google.maps.Marker({
    map       : map,
    position  : new google.maps.LatLng(centerLat, centerLon),
    draggable : true,
    title     : 'Puedes arrastrar para buscar hoteles en una nueva area',
    icon      : 'http://chart.apis.google.com/chart?chst=d_map_spin&chld=.7|0|1|10|_|R',
    zIndex   : 1
  });
  circle = new google.maps.Circle({
    map: map,
    radius: maxDist * 1000,
    strokeOpacity: 0.6,
    strokeWeight: 1,
    fillColor: "#EA7F8C",
    fillOpacity: 0.4
  });
  circle.bindTo('center', radiusMarker, 'position');
  toggleRadiusMarker();

  searchInput = (document.getElementById('search-input'));
  searchInput.style.margin = '10px';
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(searchInput);

  searchBox = new google.maps.places.SearchBox((searchInput));

  google.maps.event.addListener(searchBox, 'places_changed', function() {
    var places = searchBox.getPlaces();
    if (places.length == 0) {
      return;
    }
    place = places[0];
    radiusMarker.setPosition(place.geometry.location);
    $('#showRadius').prop('checked', true);
    toggleRadiusMarker();
    map.setCenter(radiusMarker.getPosition())
  });

  google.maps.event.addListener(map, 'bounds_changed', function() {
    var bounds = map.getBounds();
    searchBox.setBounds(bounds);
  });


  google.maps.event.addListener(radiusMarker, 'dragend', function() { toggleMarkersOutsideRadius(); } );

  // cargar_hoteles();
}

function cargar_hoteles(hotelAvail) {
  for (i = 0;i < hotelAvail.length;i++) {
    if (hotelAvail[i].longitude != '') {
      hotel = {
        pos: hotel_pos,
        hotelCode : hotelAvail[i].code + '_' + hotelAvail[i].provider,
        name      : hotelAvail[i].name,
        category  : showCategory(hotelAvail[i].categoryCode),
        img : hotelAvail[i].mainImage,
        price     : getGroupedRoomsPrice(hotelAvail[i].groupedRooms[0]),
        board     : hotelAvail[i].groupedRooms[0][0].board,
        roomType  : hotelAvail[i].groupedRooms[0][0].roomType,
        habCount  : hotelAvail[i].groupedRooms.length,
        currency  : hotelAvail[i].currency,
        lat       : hotelAvail[i].latitude,
        lon       : hotelAvail[i].longitude
      }
      hotels_maps[hotel.pos]   = hotel;
      Items_visible[hotel.pos] = false;
      adicionar_marcadores(hotel);
    }
    else {
      hotel = {
        pos: hotel_pos,
        hotelCode : hotelAvail[i].code + '_' + hotelAvail[i].provider,
        name      : hotelAvail[i].name,
        category  : showCategory(hotelAvail[i].categoryCode),
        img : hotelAvail[i].mainImage,
        price     : getGroupedRoomsPrice(hotelAvail[i].groupedRooms[0]),
        board     : hotelAvail[i].groupedRooms[0][0].board,
        roomType  : hotelAvail[i].groupedRooms[0][0].roomType,
        habCount  : hotelAvail[i].groupedRooms.length,
        currency  : hotelAvail[i].currency,
        lat       : '',
        lon       : ''
      }
      hotels_maps[hotel.pos]   = hotel;
      Items_visible[hotel.pos] = false;
    }
    hotel_pos++;
  }
}

function cargar_html_hotel() {
  var html = "<table class='small' id='list_table'>" +
                '<tr>' +
                  '<th>N#</th>' +
                  '<th>Nombre</th>' +
                  '<th>Tipo de Hab.</th>' +
                  '<th>Regimen</th>' +
                  '<th>Precio</th>' +
                '</tr>';
  for (i = 0;i < hotels_maps.length; i++) {
    if (Items_visible[i]) {
      html += '<tr>' +
                "<td>" + hotels_maps[i].pos + "</td>" +
                "<td><span style='color: #3399f3;'>" + hotels_maps[i].name + "</span></td>" +
                "<td>" + hotels_maps[i].roomType + "</td>" +
                "<td>" + hotels_maps[i].board + "</td>" +
                "<td>" + hotels_maps[i].price + ' ' + hotels_maps[i].currency + "</td>" +
                "<td></td>" +
              "</tr>";
    }
  }
   
  items   = JSON.parse(localStorage.getItem('cotizacion'));
  if (items != null) {
    for(var i = 0;i < items.length; i++) {
      if ($('#main_hotel_' + items[i].inputCode).length == 0) {
        hotelAvail = items[i]
        html += '<tr>' +
                  "<td>H</td>" +
                  "<td><span style='color: #3399f3;'>" + hotelAvail.hotelName + "</span></td>" +
                  "<td>" + hotelAvail.rooms[0].roomType + "</td>" +
                  "<td>" + hotelAvail.rooms[0].board + "</td>" +
                  "<td>" + getRoomPrice(hotelAvail.rooms) + ' ' + hotelAvail.moneda + "</td>" +
                  "<td></td>" +
                "</tr>";
      }
    }
  }

  html += "</table>";
  $('#list-hotel-select').html(html);
}

function toggleMarkersOutsideRadius() {
  <?php if ($user_viacon) { ?>
      if ($('#solo_cotizados').is(':checked')) {
        $('#solo_cotizados').click();
      }
  <?php } ?>
  for (var i = 1; i < marcadores.length; i++) {
    if (marcadores[i] != undefined) {
      var hide = $('#showOnlyWithinRadio').is(':checked')  && $('#showRadius').is(':checked') && !circle.getBounds().contains(marcadores[i].getPosition());
      marcadores[i].setVisible(!hide);
    }
  }
}

function toggleRadiusMarker() {
  visible = $('#showRadius').is(':checked');
  circle.setVisible(visible);
  radiusMarker.setVisible(visible);

  if ($('#showOnlyWithinRadio').is(':checked')) {
    toggleMarkersOutsideRadius();
  }
}

function seleccionar_hotel(hotel) {
  if (marcadores[hotel.pos].color == 'azul') {
    marcadores[hotel.pos].setIcon('http://chart.apis.google.com/chart?chst=d_map_spin&chld=.6|0|FACC2E|10|_|' + hotel.pos);
    marcadores[hotel.pos].set('color', 'amarillo');
    Items_visible[hotel.pos] = true;
  }
  else {
    marcadores[hotel.pos].setIcon('http://chart.apis.google.com/chart?chst=d_map_spin&chld=.6|0|54ABD5|10|_|' + hotel.pos);
    marcadores[hotel.pos].set('color', 'azul');
    Items_visible[hotel.pos] = false;
  }
}

function mostrar_cotizados() {
  for (var i = 0; i < marcadores.length; i++) {
    if (marcadores[i] != undefined) {
      marcadores[i].setVisible(Items_visible[i]);
    }
  }
  if(!$('#solo_cotizados').is(":checked")) {
    toggleMarkersOutsideRadius();
  }
}

function adicionar_marcadores(hotel) {
  // content = '<div id="content">' +
  //             '<div style="float:left">' +
  //               '<img src=' + hotel.img + ' style="width:70px;height:70px;border-radius:150px;border: 2px solid black;" />' +
  //             '</div>' +
  //             '<div style="float:left;padding:5px;padding-top: 0px;" >' +
  //               '<div style="font-weight:bold;color:#3399f3;">' + hotel.name + '</div>' +
  //               '<div>' + hotel.category + '</div>' +
  //               '<div> +' + hotel.habCount + ' habitaciones. </div>' +
  //               '<div style="position: absolute;right: 4px;top: 48px;">' +
  //                 '<span style="font-weight:bold">' + hotel.price + ' ' + hotel.currency + '</span>' +
  //               '</div>' +
  //             '</div>' +
  //           '</div>';

  content =  '<div id="iw-container">' +
                '<div class="iw-title"><img src="' + hotel.img + '" style="width:319px;height:150px" onerror="this.onerror=null;this.src=&quot;http://barrybolivia.com/zion/assets/images/no-photo.jpg&quot;"/></div>' +
                '<div class="iw-content">' +
                  '<div>' +
                    '<div class="iw-subTitle" style="font-weight:bold;color:#3399f3;">' + hotel.name + '</div>' +
                    '<div>' + hotel.category + '</div>' +
                    '<div> +' + hotel.habCount + ' habitaciones. </div>' +
                  '</div>' +
                  '<div style="position: absolute;right: 18px;top: 200px;font-weight:bold;">' +
                    '<span class="iw-price">' + hotel.price + ' ' + hotel.currency + '</span>' +
                  '</div>' +
                '</div>' +
                '<div class="iw-bottom-gradient"></div>' +
              '</div>';
  var infoWindow = new google.maps.InfoWindow({
    pixelOffset:new google.maps.Size(-14, -10),
    content : content,
    closeBoxMargin: "10px 100px 0px 100px",
    maxWidth: 350
  });

  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(hotel.lat, hotel.lon),
    map: map,
    color: 'azul',
    title: hotel.name + ' - Desde ' + hotel.price + ' ' + hotel.currency,
    icon: 'http://chart.apis.google.com/chart?chst=d_map_spin&chld=.6|0|54ABD5|10|_|' + hotel.pos
  });
  marcadores[hotel.pos] = marker;
  marker.addListener('mouseover', function() {
    infoWindow.open(map, this);
  });

  marker.addListener('mouseout', function() {
      infoWindow.close();
  });

  google.maps.event.addListener(infoWindow, 'domready', function() {

    // Reference to the DIV that wraps the bottom of infowindow
    var iwOuter = $('.gm-style-iw');

    /* Since this div is in a position prior to .gm-div style-iw.
     * We use jQuery and create a iwBackground variable,
     * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
    */
    var iwBackground = iwOuter.prev();

    // Removes background shadow DIV
    iwBackground.children(':nth-child(2)').css({'display' : 'none'});

    // Removes white background DIV
    iwBackground.children(':nth-child(4)').css({'display' : 'none'});

    // Moves the infowindow 115px to the right.
    iwOuter.parent().parent().css({left: '115px'});

    // Moves the shadow of the arrow 76px to the left margin.
    iwBackground.children(':nth-child(1)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

    // Moves the arrow 76px to the left margin.
    iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

    // Changes the desired tail shadow color.
    iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px', 'z-index' : '1'});

    // Reference to the div that groups the close button elements.
    var iwCloseBtn = iwOuter.next();

    var iwclosebutton = iwOuter.next();

    iwclosebutton.css({'right':'63px','top':'23px'});
    iwclosebutton.next().css({'right':'63px','top':'23px'});
    // Apply the desired effect to the close button
    // iwCloseBtn.css({opacity: '1', right: '56px', top: '16px', 'border-radius': '13px', 'box-shadow': '0 0 5px #3990B9'});

    // If the content of infowindow not exceed the set maximum height, then the gradient is removed.
    if($('.iw-content').height() < 140){
      $('.iw-bottom-gradient').css({display: 'none'});
    }

    // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
    iwCloseBtn.mouseout(function(){
      $(this).css({opacity: '1'});
    });
  });
  google.maps.event.addListener(marker, 'click', function() {
    <?php if ($user_viacon) { ?>
          cotizar_hotel_select(hotel);
    <?php }
       else{
    ?>
          seleccionar_hotel(hotel);
    <?php
       }
    ?>
  });
}

function retore_adicionar_marcadores(hotel) {
  // content = '<div id="content">' +
  //             '<div style="float:left">' +
  //               '<img src=' + hotel.img + ' style="width:70px;height:70px;border-radius:150px;border: 2px solid black;" />' +
  //             '</div>' +
  //             '<div style="float:left;padding:5px;padding-top: 0px;" >' +
  //               '<div style="font-weight:bold;color:#3399f3;">' + hotel.name + '</div>' +
  //               '<div>' + hotel.category + '</div>' +
  //               '<div> +' + hotel.habCount + ' habitaciones. </div>' +
  //               '<div style="position: absolute;right: 4px;top: 48px;">' +
  //                 '<span style="font-weight:bold">' + hotel.price + ' ' + hotel.currency + '</span>' +
  //               '</div>' +
  //             '</div>' +
  //           '</div>';

  content =  '<div id="iw-container">' +
                '<div class="iw-title"><img src="' + hotel.img + '" style="width:319px;height:150px"/></div>' +
                '<div class="iw-content">' +
                  '<div>' +
                    '<div class="iw-subTitle" style="font-weight:bold;color:#3399f3;">' + hotel.name + '</div>' +
                    '<div>' + hotel.category + '</div>' +
                    '<div> +' + hotel.habCount + ' habitaciones. </div>' +
                  '</div>' +
                  '<div style="position: absolute;right: 18px;top: 200px;font-weight:bold;">' +
                    '<span class="iw-price">' + hotel.price + ' ' + hotel.currency + '</span>' +
                  '</div>' +
                '</div>' +
                '<div class="iw-bottom-gradient"></div>' +
              '</div>';
  var infoWindow = new google.maps.InfoWindow({
    pixelOffset:new google.maps.Size(-14, -10),
    content : content,
    closeBoxMargin: "10px 100px 0px 100px",
    maxWidth: 350
  });

  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(hotel.lat, hotel.lon),
    map: map,
    color: 'azul',
    title: hotel.name + ' - Desde ' + hotel.price + ' ' + hotel.currency,
    icon: 'http://chart.apis.google.com/chart?chst=d_map_spin&chld=.6|0|ff3300|10|_|' + hotel.pos
  });
  marcadores[hotel.pos] = marker;
  marker.addListener('mouseover', function() {
    infoWindow.open(map, this);
  });

  marker.addListener('mouseout', function() {
      infoWindow.close();
  });

  google.maps.event.addListener(infoWindow, 'domready', function() {

    // Reference to the DIV that wraps the bottom of infowindow
    var iwOuter = $('.gm-style-iw');

    /* Since this div is in a position prior to .gm-div style-iw.
     * We use jQuery and create a iwBackground variable,
     * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
    */
    var iwBackground = iwOuter.prev();

    // Removes background shadow DIV
    iwBackground.children(':nth-child(2)').css({'display' : 'none'});

    // Removes white background DIV
    iwBackground.children(':nth-child(4)').css({'display' : 'none'});

    // Moves the infowindow 115px to the right.
    iwOuter.parent().parent().css({left: '115px'});

    // Moves the shadow of the arrow 76px to the left margin.
    iwBackground.children(':nth-child(1)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

    // Moves the arrow 76px to the left margin.
    iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

    // Changes the desired tail shadow color.
    iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px', 'z-index' : '1'});

    // Reference to the div that groups the close button elements.
    var iwCloseBtn = iwOuter.next();

    var iwclosebutton = iwOuter.next();

    iwclosebutton.css({'right':'63px','top':'23px'});
    iwclosebutton.next().css({'right':'63px','top':'23px'});
    // Apply the desired effect to the close button
    // iwCloseBtn.css({opacity: '1', right: '56px', top: '16px', 'border-radius': '13px', 'box-shadow': '0 0 5px #3990B9'});

    // If the content of infowindow not exceed the set maximum height, then the gradient is removed.
    if($('.iw-content').height() < 140){
      $('.iw-bottom-gradient').css({display: 'none'});
    }

    // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
    iwCloseBtn.mouseout(function(){
      $(this).css({opacity: '1'});
    });
  });
  google.maps.event.addListener(marker, 'click', function() {
    <?php if ($user_viacon) { ?>
          cotizar_hotel_select(hotel);
    <?php }
       else{
    ?>
          seleccionar_hotel(hotel);
    <?php
       }
    ?>
  });
}

function updateMaxDistRadius() {
  circle.setRadius(parseInt($('#maxDist').val()) * 1000);
  toggleMarkersOutsideRadius();
}

function showDistanceSearchModal(title, lat, lon) {
   var centerLat = lat;
   var centerLon = lon;
   if (centerLat != '') {
     $('#maps_hotel').modal();
     $('#showRadius').prop('checked', true);
      var newLatLng = new google.maps.LatLng(lat, lon);
      radiusMarker.setPosition(newLatLng);

      toggleRadiusMarker();
      google.maps.event.trigger(map, "resize");
      map.setCenter(newLatLng);
   }
}

function habilitar_marcador(id) {
  for (i = 1; i < hotels_maps.length; i++) {
     if (hotels_maps[i].hotelCode == id && hotels_maps[i].lat != '') {
        if ($('#main_hotel_'+id).is(':checked')) {
          marcadores[hotels_maps[i].pos].setIcon('http://chart.apis.google.com/chart?chst=d_map_spin&chld=.6|0|FACC2E|10|_|' + hotels_maps[i].pos);
          Items_visible[hotels_maps[i].pos] = true;
        }
        else {
          marcadores[hotels_maps[i].pos].setIcon('http://chart.apis.google.com/chart?chst=d_map_spin&chld=.6|0|54ABD5|10|_|' + hotels_maps[i].pos);
          Items_visible[hotels_maps[i].pos] = false;
        }
     }
  }
}

function cotizar_hotel_select(hotel) {
  if ($('#main_hotel_'+hotel.hotelCode).is(':checked')) {
    marcadores[hotel.pos].setIcon('http://chart.apis.google.com/chart?chst=d_map_spin&chld=.6|0|54ABD5|10|_|'+hotel.pos);
  }
  else {
    marcadores[hotel.pos].setIcon('http://chart.apis.google.com/chart?chst=d_map_spin&chld=.6|0|FACC2E|10|_|'+hotel.pos);
  }
  $('#main_hotel_'+hotel.hotelCode).click();
}

function printMap() {
  if($('#load-cotizacion').is(':visible')) {
    $('#load-cotizacion').hide();
  }
  cargar_html_hotel();
  autoCompleteMapHtml();
  $('#content-information').show();
  $('#resize').removeClass('pantalla_completa');
  $('header').hide();
  $('#header').hide();
  $('#top_header').hide();
  $('#content-paginate, #new_cotizacion').hide()
  $('footer').hide();
  $('.filter-panel').hide();
  $('#resultbar').hide();
  $('#refine-form').hide();
  $('#jivo_top_wrap').hide();
  $('.hotel_avail').hide();
  $('#googlemap').hide();
  $('#mensaje-map').hide();
  $('#options_markers, .modal-header').hide();
  $('#filter-search').hide();
  $('#expiredTime').hide();
  $('#ui-datepicker-div').addClass('hidden-print');
  $('.call-action-boxed').hide();
  $('#modal-nueva-busqueda').hide();
  
  $('.hidden-header').hide();
  window.print();

  // Restore
  $('#options_markers, .modal-header').show();
  $('#googlemap').show();
  $('.hotel_avail').show();
  $('.hotelLoadingRes').hide();
  $('#header').show();
  $('#top_header').show();
  $('#content-paginate, #new_cotizacion').show()
  $('footer').show();
  $('.filter-panel').show();
  $('#resultbar').show();
  $('#refine-form').show();
  $('#jivo_top_wrap').show();
  $('#filter_information').hide();
  $('#content-information').hide();
  $('header').show();
  $('#mensaje-map').show();
  $('#filter-search').show();
  $('#resize').addClass('pantalla_completa');
  $('.call-action-boxed').show();
  $('.hidden-header').show();

}

function retoreMarkers() {
  items   = JSON.parse(localStorage.getItem('cotizacion'));
  if (items != null) {
    for(var i = 0;i < items.length; i++) {
      if ($('#main_hotel_' + items[i].inputCode).length == 0) {
        hotelAvail = items[i]
        hotel = {
          pos: 'H',
          hotelCode : hotelAvail.code + '_' + hotelAvail.provider,
          name      : hotelAvail.hotelName,
          category  : showCategory(hotelAvail.category),
          img : "",
          price     : getRoomPrice(hotelAvail.rooms),
          board     : /*hotelAvail[i].groupedRooms[0][0].board*/"",
          roomType  : /*hotelAvail[i].groupedRooms[0][0].roomType*/"",
          habCount  : /*hotelAvail[i].groupedRooms.length*/"",
          currency  : hotelAvail.moneda,
          lat       : hotelAvail.lat,
          lon       : hotelAvail.lng
        }
        retore_adicionar_marcadores(hotel)
      }
    } 
  }
}

function getRoomPrice(rooms) {
  var price = 0
  for (var i = 0; i < rooms.length; i++) {
    price += rooms[i].price
  }
  return price;
}

function cleanMarkers() {
  for (var i = 1; i < marcadores.length; i++) {
    marcadores[i].setMap(null)
  }
  hotel_pos       = 1;
  marcadores      = []
  Items_visible   = [];
}

function autoCompleteMapHtml() {
  hab_maps  = $('#habitaciones option:selected').text();
  str_hab   = (hab_maps == 1) ? (hab_maps + " Habitaci&oacute;n") : (hab_maps + " Habitaciones");
  $('#pr_habitaciones').append('<span>' + str_hab + '</span>');
  adultos_ = 0;
  ninos   = 0;
  for(i = 1; i <= hab_maps; i++) {
    console.log("Conteo " + $('#adultos'+i).val())
    adultos_ += parseInt($('#adultos'+i).val());
    ninos   += parseInt($('#ninos'+i).val());
  }

  str_adult = (adultos_ == 1) ? (adultos_ + " Adulto") : (adultos_ + " Adultos");
  console.log("FOR " + adultos_)
  $('#pr_adultos').append('<span>' + str_adult +'</span>');
  if(ninos != 0) {
    str_ninos = (ninos == 1) ? (ninos + " ni&ntilde;o") : (ninos + " ni&ntilde;os");
    $('#pr_ninos').append(str_ninos);
  }
}

var ready = function() {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyD56vxIspKesbI0tRFvXe9Txrb3pWaBwps&v=3.exp&libraries=places&callback=initialize';
  document.body.appendChild(script);
};
$(document).ready(ready);

</script>
