<?php 
  $user_name_mail = (is_array($current_user)) ? $current_user['name'] . " " . $current_user['lastname'] : $current_user->name . " " . $current_user->lastname;
  
  $agency_name = "(" . $_SESSION['agency_name'] . ")";
?>
<div id="mailing-modal" class="modal fade hidden-print">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Enviar Mail</h4>
      </div>
      <div class="modal-body">
        <div class="col-md-9">
          <div class="form-group">
            <label>Para:</label>
            <input id="mailing-to" type="text" class="form-control" />
            <span class="small text-muted">(Para enviar a varios destinatarios separar por una coma ',')</span>
          </div>
          <div class="form-group">
            <label>Asunto:</label>
            <input id="mailing-subject" type="text" class="form-control" />
          </div>
          <div class="form-group">
            <label>Mensaje:</label>
            <textarea id="mailing-msg" class="form-control"></textarea>
          </div>
          <div class="form-group">
            <label>Enviar desde:</label><br />
            <?php
            if (isset($_SESSION['agency_mail']) && $_SESSION['agency_mail'] != '') {
              echo '<input type="radio" name="mailing-from" value="' . $_SESSION['agency_mail'] . '" checked> ';
              echo $_SESSION['agency_mail'] . '<br />';
            }
            if (isset($current_user->mail) && $current_user->mail != '') {
              echo '<input type="radio" name="mailing-from" value="' . $current_user->mail . '" checked> ';
              echo $current_user->mail . '<br />';
            }
            ?>
            <input type="radio" name="mailing-from" value="0">
            Otro: <input type="text" id="mailing-from-other">
          </div>

          <div class="form-group">
            <input type="checkbox" id="mailing-copy"> Enviarme un copia
          </div>

          <input type="hidden" id="mailing-div" />
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" onclick="sendMail()">Enviar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
  function sendMail() {
    to = $('#mailing-to').val();
    subject = $('#mailing-subject').val();
    msg = $('#mailing-msg').val();
    div = $('#mailing-div').val();
    from = $('input[name=mailing-from]:checked').val();
    if (from == '0') {
      from = $('#mailing-from-other').val();
    }

    fromName = '<?php echo $user_name_mail . $agency_name;  ?>';

    body_div = $('#' + div).clone();
    body_div.find('.avoid-mailing').remove();
    body = body_div.html();
    copy = $('#mailing-copy').is(':checked') ? 1 : 0;
    $.ajax({
      type: "POST",
      url: "helpers/ajax/mailing.php",
      data: {
        subject: subject,
        from: from,
        fromName: fromName,
        to: to,
        msg: msg,
        body: body,
        copy: copy
      }
    })
      .done(function(r) {
      });
    $('#mailing-modal').modal('hide');
  }
</script>
