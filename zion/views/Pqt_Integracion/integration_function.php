
<?php
function getServiceAddDataJSON_c($roomsGroup, $childAges) {
  // $hotelAvail = $roomsGroup[0]->getHotel();
  $service_data = HotelAvailToArray_c($hotelAvail);
  $rooms_arr = array();
  foreach ($roomsGroup as $room) {
    $rooms_arr[] = RoomAvailToArray_c($room);
  }
  $service_data['rooms'] = $rooms_arr;
  $service_data['childAges'] = $childAges;

  return htmlspecialchars(json_encode($service_data), ENT_QUOTES, 'UTF-8');
}

function RoomAvailToArray_c($roomAvail) {
  $arr = array(
    'roomCount' => $roomAvail->getRoomCount(),
    'adultCount' => $roomAvail->getAdultCount(),
    'childCount' => $roomAvail->getChildCount(),
    'price' => Agency::getUserPrice($roomAvail->getPrice()),
    'board' => $roomAvail->getBoard(),
    'boardCode' => $roomAvail->getBoardCode(),
    'boardType' => $roomAvail->getBoardType(),
    'roomType' => $roomAvail->getRoomType(),
    'roomTypeType' => $roomAvail->getRoomTypeType(),
    'roomTypeCode' => $roomAvail->getRoomTypeCode(),
    'roomTypeCharacteristic' => $roomAvail->getRoomTypeCharacteristic()
  );

  return $arr;
}

function HotelAvailToArray_c($hotelAvail) {
  $destCode  = $hotelAvail->getDestinationCode();
  $hotelCode = $hotelAvail->getCode();
  $provider  = $hotelAvail->getProvider();
  if($hotelAvail->getProvider() == BookingProvider::DOMITUR) {
    $result    = Domitur_getdDest_Code($hotelAvail->getDestinationCode());
    $destCode  = ($result != NULL) ? $result : $hotelAvail->getDestinationCode();
    $code = DomiturHotelMapping::getMappedCode($hotelAvail->getCode());
    if($code != NULL) {
      $hotelCode = $code;
      $provider  = BookingProvider::HOTELBEDS;
    }
  }

  if($hotelAvail->getProvider() == BookingProvider::METHABOOK) {
    $result = Methabook_getDest_Code($hotelAvail->getDestinationCode());
    $destCode = ($result != NULL) ? $result : $hotelAvail->getDestinationCode();
  }

  $number_star = obtener_estrella($hotelAvail->getCategoryCode());
  $arr = array(
    'dateFrom'  => $hotelAvail->getDateFrom(),
    'dateTo'    => $hotelAvail->getDateTo(),
    'hotelCode' => $hotelCode,
    'destCode'  => $destCode,
    'destType'  => $hotelAvail->getDestinationType(),
    'hotelName' => $hotelAvail->getName(),
    'category'  => $hotelAvail->getCategoryCode(),
    'estrella'  => $number_star,
    'moneda'    => $hotelAvail->getCurrency(),
    'latitude'  => $hotelAvail->getLatitude(),
    'longitude' => $hotelAvail->getLongitude(),
    'provider'  => $provider
  );

  return $arr;
}

function Domitur_getdDest_Code($c) {
    $map = array(
      'PTC' => 'PUJ',
      'PAN' => 'PTY',
      'CUNN-ZHT' => 'CUN',
      'cun' => 'PCM',
      'SDQ' => 'DOM'
      );

    if (isset($map[$c])) {
      return $map[$c];
    }

    return NULL;
}

function Methabook_getDest_Code($c) {
  $map = array(
      'SLA' => 'SLT',
      '65449'  => 'VA',
      '64306' => 'FLO'
      );

    if (isset($map[$c])) {
      return $map[$c];
    }

    return NULL;
}

function obtener_estrella($category) {
  $stars = array(
    '1EST' => 1,
    '1LL' => 1,
    '2EST' => 2,
    '2LL' => 2,
    '3EST' => 3,
    '3LL' => 3,
    '4EST' => 4,
    '4LL' => 4,
    '4LUX' => 4,
    '5EST' => 5,
    '5LL' => 5,
    '5LUX' => 5,
    'AG' => 0,
    'ALBER' => 0,
    'APTH' => 1,
    'APTH2' => 2,
    'APTH3' => 3,
    'APTH4' => 4,
    'APTH5' => 5,
    'AT1' => 0,
    'AT2' => 0,
    'AT3' => 0,
    'BB' => 0,
    'BB3' => 3,
    'BB4' => 4,
    'BB5' => 5,
    'BOU' => 0,
    'CAMP1' => 0,
    'CAMP2' => 0,
    'CHUES' => 0,
    'H1_5' => 1.5,
    'H2S' => 2,
    'H2_5' => 2.5,
    'H3S' => 3,
    'H3_5' => 3.5,
    'H4_5' => 4.5,
    'H5_5' => 5.5,
    'HIST' => 0,
    'HR' => 1,
    'HR2' => 2,
    'HR3' => 3,
    'HR4' => 4,
    'HR5' => 5,
    'HRS' => 5,
    'HS' => 1,
    'HS2' => 2,
    'HS3' => 3,
    'HS4' => 4,
    'HS5' => 5,
    'HSR1' => 1,
    'HSR2' => 2,
    'LODGE' => 0,
    'MINI' => 0,
    'PENDI' => 0,
    'PENSI' => 0,
    'POUSA' => 0,
    'RESID' => 0,
    'RSORT' => 0,
    'SPC' => 0,
    'STD' => 0,
    'SUP' => 4,
    'VILLA' => 0,
    'VTV' => 0);
  $num_stars = array_key_exists($category . '', $stars) ? $stars[$category . ''] : 0;
  return $num_stars;
}


?>
