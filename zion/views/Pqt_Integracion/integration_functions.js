
function BuscarCotizacion() {
  $('#container_cotizacion').slideDown('fast');
  var titulo_cotizacion = $('#name_cotizacion').val();
  var cliente = $('#b_name_cliente').val();
  $.ajax({
    url: "views/Pqt_Integracion/conexion_cotizacion.php",
    data: {selector : "Buscar_cotizacion",cotizacion : titulo_cotizacion, name_cliente : cliente}
  })
  .done(function(r){
    $('#load-buscar-cotizacion').hide();
    $('#resultado_cotizacion').html(r);

  });
}

function buscar_publicacion() {
  $('#container_publicacion').slideDown('fast');
  var criterio = $('#numero_publicacion').val();
  $.ajax({
    url: "views/Pqt_Integracion/conexion_cotizacion.php",
    data: {selector : "Buscar_publicacion",criterio : criterio}
  })
  .done(function(r){
    $('#load-buscar-publicacion').hide();
    $('#resultado_publicacion').html(r);

  });
}

function selec_cotizacion(id) {
  var obtenido = $('#'+id).data("informacion");
  var cliente  = obtenido.nombre_cliente + ' ' + obtenido.apellido_cliente;
  $('#name_cotizacion').val(obtenido.titulo);
  $('#id_cotizacion').val(obtenido.idcotizacion);
  $('#b_name_cliente').val(cliente);
  var ciudades = obtenido.ciudades;
  var htm = "";

  for(i = 0;i < ciudades.length; i++) {
    htm += (i == 0) ? '<label class="btn btn-primary active">' : '<label class="btn btn-primary">';
    htm += (i == 0) ? '<input type="radio" name="city_destinos" data-idcity="'+ ciudades[i].id_ciudad_origen_paquete +'" autocomplete="off" checked>' : '<input type="radio" name="city_destinos" data-idcity="'+ ciudades[i].id_ciudad_origen_paquete +'" autocomplete="off">';
    htm += ciudades[i].ciudad;
    htm += '</label>';
  }
  $('#contenido_ciudades').html(htm);
  $('#container_cotizacion').hide();
  $('#name_cotizacion').css({'border-color':'#BEBBBB'});
  $('#b_name_cliente').css({'border-color':'#BEBBBB'});
}

function selec_publicacion(id) {
  var obtenido = $('#'+id).data("informacion");
  var titulo_publicacion  = obtenido.titulo;
  $('#numero_publicacion').val(obtenido.idpublicacion);
  $('#id_publicacion').val(obtenido.idpublicacion);
  $('#p_titulo').val(titulo_publicacion);
  var ciudades = obtenido.ciudades;
  var htm = "";

  for(i = 0;i < ciudades.length; i++) {
    htm += (i == 0) ? '<label class="btn btn-primary active">' : '<label class="btn btn-primary">';
    htm += (i == 0) ? '<input type="radio" name="dest_publicacion" data-idcity="'+ ciudades[i].id_ciudad_origen_paquete +'" autocomplete="off" checked>' : '<input type="radio" name="city_destinos" data-idcity="'+ ciudades[i].id_ciudad_origen_paquete +'" autocomplete="off">';
    htm += ciudades[i].ciudad;
    htm += '</label>';
  }
  $('#contenido_ciudades_publicacion').html(htm);
  $('#container_publicacion').hide();
  $('#numero_publicacion').css({'border-color':'#BEBBBB'});
  $('#p_titulo').css({'border-color':'#BEBBBB'});
}

function enable_cotizacion(id_hotel) {//revisar esta parte
  var select = $('input:checkbox[class=cotizar-check]:checked');
  $('#cotizacion-item-4 label').html(select.length);
  for (var i = 0; i < select.length; i++) {
    var img = $('#hotel-image-' + $(select[i]).val());
    $('#cotizacion-item-' + (i + 1) + ' img').attr('src', img.attr('src'));
  }
  for (var i = 4; i > select.length; i--) {
    $('#cotizacion-item-' + i + ' img').attr('src', 'assets/images/no-item.png');
  }

  if(select.length > 4 ) {
    $('#cotizacion-item-4 img').hide();
    $('#cotizacion-item-4 label').show();
  }
  if(select.length == 4) {
    $('#cotizacion-item-4 label').hide();
    $('#cotizacion-item-4 img').show();
  }

  if($('#main_hotel_'+id_hotel).is(":checked")) {
    $('.list_check_'+id_hotel).show();
    $('#'+id_hotel+'_1').prop("checked", true);
  }
  else {
    $('.list_check_'+id_hotel).hide();
    $('#'+id_hotel+'_1').prop("checked", false);
  }

  if(select.length == 0) {
    $('#load-cotizacion').hide('drop', {direction: 'right'});
  }
  else {
    $('#load-cotizacion').show('drop', {direction: 'right'});
  }
}

function enable_new_cotizacion(id_hotel) {//revisar esta parte
  var select    = $('input:checkbox[class=cotizar-check]:checked');
  var item      = JSON.parse($('#' + id_hotel + '_1').val()) 
  var itemSaved = JSON.parse(localStorage.getItem('cotizacion'))
  var arr       = new Array()
  if($('#main_hotel_' + id_hotel).is(":checked")) {
    $('.list_check_' + id_hotel).show();
    $('#' + id_hotel + '_1').prop("checked", true);
    if (itemSaved != null && itemSaved.length > 0) {
      agregar_item(id_hotel + '_1')
    }
    else {
      agregar_item(id_hotel + '_1')
    }
  }
  else {
    
    var items = $('.list_check_' + id_hotel)
    var con   = 1
    for(var f = 0;f < items.length; f++) {
      var item_id = $(items[f]).attr('id')
      if ($('#' + item_id).is(":checked")) {
        delete_item(item_id)
        $('#' + item_id).prop("checked", false);
      }
      con++
    }
    $('.list_check_' + id_hotel).hide();
  }

  update_count_cotizacion()
}

function agregar_item(id) {
  var item    = JSON.parse($('#' + id).val()) 
  item['input_room_code'] = id
  var itemSaved = JSON.parse(localStorage.getItem('cotizacion'))
  itemSaved     =  (itemSaved != null && itemSaved.length > 0) ? itemSaved : new Array()
  var arr       = new Array()
  itemSaved.push(item)
  localStorage.setItem('cotizacion', JSON.stringify(itemSaved)) 
  update_count_cotizacion()
}

function verify_check(id_hotel,id_element) {
  var can_check = $('input:checkbox[class=list_check_'+id_hotel+']:checked');

  if(can_check.length > 4) {
    $('#'+id_element).prop("checked", false);
  }
}

function view_select() {
  $('#hotels-new-cotizacion').empty();
  $('#cotizacion-modal').modal();
  var alert_moneda = '';
  var htm_new      = "";
  var item_checks  = $('input:checked[class=cotizar-check]:checked');

  htm_new       += "<div class='col-md-12' style='margin-top: 10px;border: 1px solid #aaa;padding: 10px;border-radius: 7px;'>";
  htm_new       += "<h4>Lista de Hoteles</h4>";
  single_html    = "";
  double_html    = "";
  triple_html    = "";
  cuadruple_html = "";
  infante_html   = "";
  // hoteles_array  = armar_envio_cotizacion();
  hoteles_array  = armar_new_envio_cotizacion()
  for(i = 0;i < hoteles_array.length; i++) {
    if(hoteles_array[i]['single'] != '0.00')
      single_html = "<th>Single</th>";
    if(hoteles_array[i]['doble'] != '0.00')
      double_html = "<th>Doble</th>";
    if(hoteles_array[i]['triple'] != '0.00')
      triple_html = "<th>Triple</th>";
    if(hoteles_array[i]['cuadruple'] != '0.00')
      cuadruple_html = "<th>Cuadruple</th>";
    if(hoteles_array[i]['menor'] != '0.00') {
      $('#div_infante').show();
      infante_html = "<th>Menor</th>";
    }
  }
  htm_new += "<table class='table'>" +
                '<tr>' +
                  "<th>Hotel</th>" +
                  "<th>Categoria</th>" +
                    single_html + double_html + triple_html + cuadruple_html + infante_html +
                  "<th>Moneda</th>" +
                  "<th>Alimentaci&oacute;n</th>" +
                  "<th>Obs</th>" +
                  "<th>Obs Int</th>" +
                  "<th></th>" +
                "</tr>";
  if(single_html != "" || double_html != "" || triple_html != "" || cuadruple_html != "")
  {
    for(d = 0;d < hoteles_array.length;d++) {
      htm_new += "<tr id='cotiza-" + d + "'>" +
                   "<td>" + hoteles_array[d]['nombre_hotel'] + "</td>" +
                   "<td>" + hoteles_array[d]['star_number'] + "EST</td>";
      if(single_html != '') {
        htm_new += "<td>" + hoteles_array[d]['single'] + "</td>";
      }
      if(double_html != '') {
        htm_new += "<td>" + hoteles_array[d]['doble'] + "</td>";
      }
      if(triple_html != '') {
        htm_new += "<td>" + hoteles_array[d]['triple'] + "</td>";
      }
      if(cuadruple_html != '') {
        htm_new += "<td>" + hoteles_array[d]['cuadruple'] + "</td>";
      }
      if(infante_html != '') {
        htm_new += "<td>" + hoteles_array[d]['menor'] + "</td>";
      }
      htm_new +=  "<td>" + hoteles_array[d]['moneda'] + "</td>" +
                  "<td>" + hoteles_array[d]['alimentacion'] + "</td>" +
                  "<td style='position:relative'>" +
                    "<i class='fa fa-pencil-square-o' style='font-size:18px;cursor:pointer;' onclick='$(\"#observacion_" + hoteles_array[d]['cod_seleccion'] + "\").slideDown(\"fast\")'></i>" +
                    "<div id='observacion_"+ hoteles_array[d]['cod_seleccion'] +"' style='display:none;position:absolute;top:0px;background-color: #CFC8C8;padding: 6px;border-radius: 6px;z-index: 1;right:0px'>" +
                      "<textarea rows='4' cols='35' id='tex-observacion-" + hoteles_array[d]['cod_seleccion'] + "' >"+ hoteles_array[d]['room_type'] +"</textarea><br>" +
                      "<button style='width:100%' onclick='$(\"#observacion_" + hoteles_array[d]['cod_seleccion'] + "\").slideUp(\"fast\")'>Ocultar</button>" +
                    "</div>" +
                  "</td>" +
                  "<td style='position:relative'>" +
                    "<i class='fa fa-pencil-square-o' style='font-size:18px;cursor:pointer;' onclick='$(\"#observacion_int" + hoteles_array[d]['cod_seleccion'] + "\").slideDown(\"fast\")'></i>" +
                    "<div id='observacion_int"+ hoteles_array[d]['cod_seleccion'] +"' style='display:none;position:absolute;top:0px;background-color: #CFC8C8;padding: 6px;border-radius: 6px;z-index: 1;right:0px'>" +
                      "<textarea rows='4' cols='35' id='tex-observacion-int-" + hoteles_array[d]['cod_seleccion'] + "' ></textarea><br>" +
                      "<button style='width:100%' onclick='$(\"#observacion_int" + hoteles_array[d]['cod_seleccion'] + "\").slideUp(\"fast\")'>Ocultar</button>" +
                    "</div>" +
                 "</td>" +
                 "<td>" +
                    "<i class='fa fa-minus-circle' style='font-size:15px;color:red;cursor:pointer;' onclick='delete_select(\"" + d + "\", \"" + hoteles_array[d]['cod_seleccion'] + "\")'></i>" +
                  "</td>" +
                "</tr>";
      if(hoteles_array[d]['moneda'] != 'USD' && alert_moneda == '') {
        alert_moneda += "Tomar en cuenta que el tipo de moneda esta basado en "+hoteles_array[d]['moneda'];
      }
    }
  }
  if(alert_moneda != '') { $('#alert_moneda').show().html(alert_moneda);  }
  $('#hotels-new-cotizacion').html(htm_new);
}

function quitar_seleccion(d, id) {
  $('#cotiza-' + d).remove();
  $('#' + id).prop('checked', false);
  clase_nexto = $('#' + id).attr('class');
  checkeds    = $('input:checkbox[class=' + clase_nexto + ']:checked').length
  if(checkeds == 0) {
    id_main_hotel = $('#' + id).data('main');
    $('#' + id_main_hotel).click();
  }
}

function get_rooms_cotizacion_new(items_hotel, id_hotel) {
  var room_htm = "";
  room_htm += "<div class='col-md-12'>";
   for (var j = 0; j < items_hotel.length; j++) {
      item_value = $(items_hotel[j]).val();
      id_check   = $(items_hotel[j]).attr("id");
      str = JSON.parse(item_value);
      room_htm += "<table class='table' id='room_"+ j +"_"+ id_hotel +"'>";
      room_htm += '<tr> ' +
                    '<th>Tipo de Habtacion</th>'+
                    '<th>Servicio</th>'+
                    '<th>Habitaciones</th>'+
                    '<th>Ocupacion</th>'+
                    '<th>Precio<i class="fa fa-times pull-right" style="color: red;font-size: 20px;cursor: pointer;" onclick="$(\'#'+id_check+'\').prop(\'checked\',false);$(\'#room_'+j+'_'+id_hotel+'\').remove();"></i></th>'+
                  '</tr>';
      rooms = str.rooms;
      for (var i = 0; i < rooms.length; i++) {
        adult = (str.rooms[i].adultCount == 1) ? str.rooms[i].adultCount+" Adulto " : str.rooms[i].adultCount+" Adultos ";
        adult += (str.rooms[i].childCount == 1) ? str.rooms[i].childCount+" ni&ntilde;o" : (str.rooms[i].childCount != 0) ? str.rooms[i].childCount + " ni&ntilde;os" : '';

        room_htm += "<tr>" +
                      "<td>"+ rooms[i].roomType +"</td>" +
                      "<td>"+ str.rooms[i].board +"</td>" +
                      "<td>"+ str.rooms[i].roomCount +"</td>" +
                      "<td>"+ adult + "</td>" +
                      "<td>"+ str.rooms[i].price +"</td>" +
                    "</tr>";
      }
      room_htm += "</table>";

    }

  // return room_htm += "</div>";
  return room_htm += "</div>";
}

function armar_new_envio_cotizacion() {
  var rooms_selects = JSON.parse(localStorage.getItem('cotizacion'))
  var noches        = $('#noche').val();
  var link_search   = "";
  
  var hoteles_array = new Array();
  var infante       = 0;
  var total         = 0;
  var contador_nino = 0;
  for (var j = 0; j < rooms_selects.length; j++) {
    // infante = 0;
    datos_rooms                 = rooms_selects[j];
    var hotel_array             = new Array();
    hotel_array['nombre_hotel'] = datos_rooms.hotelName;
    hotel_array['estrellas']    = datos_rooms.category;
    var cantidad_rooms          = datos_rooms.rooms;
    var moneda                  = datos_rooms.moneda;
    var cod_seleccion           = datos_rooms.input_room_code;
    var single                  = 0;
    var doble                   = 0;
    var Triple                  = 0;
    var cuadruple               = 0;
    var infante                 = 0
    var observacion             = '';
    var observacion_int         = '';
    var room_type               = '';
    for(f = 0; f < cantidad_rooms.length; f++) {
      
      total += cantidad_rooms[f].price;
      // infante = (cantidad_rooms[f].childCount != 0) ? 10.5 : 0;
      if(cantidad_rooms[f].childCount != 0) {
        $('#div_infante').show();
      }
      adult_count = cantidad_rooms[f].adultCount;
      if(datos_rooms.provider == 75){
        if(cantidad_rooms[f].roomCount > 1) {
          adult_count = (parseInt(cantidad_rooms[f].adultCount) / parseInt(cantidad_rooms[f].roomCount));
        }
      }
      if (adult_count == 1 && cantidad_rooms[f].childCount == 0 && single == 0) {
         single  = (cantidad_rooms[f].price / parseInt(noches));
      }
      else if(adult_count == 1 && single == 0) {
        single    = ((cantidad_rooms[f].price / (parseInt(cantidad_rooms[f].adultCount) + parseInt(cantidad_rooms[f].childCount)))/ parseInt(noches));
        infante  += ((cantidad_rooms[f].price / (parseInt(cantidad_rooms[f].adultCount) + parseInt(cantidad_rooms[f].childCount)))/ parseInt(noches));
      }
      if (adult_count == 2 && cantidad_rooms[f].childCount == 0 && doble == 0) {
        price = cantidad_rooms[f].price / cantidad_rooms[f].roomCount;
        doble = ((price/2) / parseInt(noches));
      }
      else if(adult_count == 2 && doble == 0) {
        doble     = ((cantidad_rooms[f].price / (parseInt(cantidad_rooms[f].adultCount) + parseInt(cantidad_rooms[f].childCount)))/ parseInt(noches));
        infante  += ((cantidad_rooms[f].price / (parseInt(cantidad_rooms[f].adultCount) + parseInt(cantidad_rooms[f].childCount)))/ parseInt(noches));
      }
      if (adult_count == 3 && cantidad_rooms[f].childCount == 0 && Triple == 0) {
        price   = cantidad_rooms[f].price / cantidad_rooms[f].roomCount;
        Triple  = ((price/3) / parseInt(noches));
      }
      else if(adult_count == 3 && Triple == 0) {
        Triple    = ((cantidad_rooms[f].price / (parseInt(cantidad_rooms[f].adultCount) + parseInt(cantidad_rooms[f].childCount)))/ parseInt(noches));
        infante  += ((cantidad_rooms[f].price / (parseInt(cantidad_rooms[f].adultCount) + parseInt(cantidad_rooms[f].childCount)))/ parseInt(noches));
      }
      if (adult_count >= 4 && cantidad_rooms[f].childCount == 0 && cuadruple == 0) {
        price     = cantidad_rooms[f].price / cantidad_rooms[f].roomCount;
        cuadruple = ((price/cantidad_rooms[f].adultCount) / parseInt(noches));
      }
      else if(adult_count == 4 && cuadruple == 0) {
        cuadruple   =  ((cantidad_rooms[f].price / (parseInt(cantidad_rooms[f].adultCount) + parseInt(cantidad_rooms[f].childCount)))/ parseInt(noches));
        infante    += ((cantidad_rooms[f].price / (parseInt(cantidad_rooms[f].adultCount) + parseInt(cantidad_rooms[f].childCount)))/ parseInt(noches));
      }

      room_type += ((f+1) == cantidad_rooms.length) ? cantidad_rooms[f].roomType : cantidad_rooms[f].roomType + ", ";
    }
    observacion     = $('#tex-observacion-' + cod_seleccion).val();
    observacion_int = $('#tex-observacion-int-' + cod_seleccion).val();
    longitude = (datos_rooms.longitude) ? datos_rooms.longitude : '';
    latitude  = (datos_rooms.latitude) ? datos_rooms.latitude : '';
    nameHotel = datos_rooms.hotelName
    if(nameHotel.search("Casino")){
      nameHotel = nameHotel.replace("Casino","Casinó");
    }
    hotel_array = {
                    hotelCode    : datos_rooms.hotelCode,
                    provider     : datos_rooms.provider,
                    nombre_hotel : nameHotel,
                    room_type    : room_type,
                    estrellas    : datos_rooms.category,
                    star_number  : datos_rooms.estrella,
                    dest_Code    : datos_rooms.destCode,
                    fecha_from   : $('#checkin').val(),
                    fecha_to     : $('#checkout').val(),
                    single       : single.toFixed(2),
                    doble        : doble.toFixed(2),
                    triple       : Triple.toFixed(2),
                    cuadruple    : cuadruple.toFixed(2),
                    menor        : infante.toFixed(2),
                    alimentacion : cantidad_rooms[0].board,
                    observacion  : observacion,
                    observacion_int  : observacion_int,
                    link_search  : link_search,
                    moneda       : moneda,
                    cod_seleccion:cod_seleccion,
                    latitude     : latitude,
                    longitude    : longitude,
                    total        : total.toFixed(2)
                  };
    hoteles_array.push(hotel_array);
    // console.log(hoteles_array)
  }
  return hoteles_array;

}
function armar_envio_cotizacion() {

  //hoteles seleccionados para cotizar
  var hoteles_selec = $('input:checkbox[class=cotizar-check]:checked');
  var noches        = $('#noche').val();
  var hoteles_array = new Array();
  for (var i = 0; i < hoteles_selec.length; i++) {
    // habitaciones seleccionados en cada hotel
    var id_hotel      = $(hoteles_selec[i]).val();
    var rooms_selects = $('input:checkbox[class=list_check_'+id_hotel+']:checked');
    // var link_search   = 'http://barrybolivia.com/zion/' + $('#hotel-link-'+id_hotel).attr("href");
    var link_search   = $('#hotel-link-'+id_hotel).attr("href");
    var hotel_array   = new Array();
    var infante       = 0;
    var total         = 0;
    var contador_nino = 0;
    for (var j = 0; j < rooms_selects.length; j++) {
      datos_rooms                 = JSON.parse($(rooms_selects[j]).val());
      hotel_array['nombre_hotel'] = datos_rooms.hotelName;
      hotel_array['estrellas']    = datos_rooms.category;
      var cantidad_rooms          = datos_rooms.rooms;
      var moneda                  = datos_rooms.moneda;
      var cod_seleccion           = $(rooms_selects[j]).attr("id");
      var single                  = 0;
      var doble                   = 0;
      var Triple                  = 0;
      var cuadruple               = 0;
      var observacion             = '';
      var observacion_int         = '';
      var room_type               = '';

      for(f = 0; f < cantidad_rooms.length; f++) {
        total += cantidad_rooms[f].price;
        // infante = (cantidad_rooms[f].childCount != 0) ? 10.5 : 0;
        if(cantidad_rooms[f].childCount != 0) {
          $('#div_infante').show();
        }
        adult_count = cantidad_rooms[f].adultCount;
        if(datos_rooms.provider == 75){
          if(cantidad_rooms[f].roomCount > 1) {
            adult_count = (parseInt(cantidad_rooms[f].adultCount) / parseInt(cantidad_rooms[f].roomCount));
          }
        }
        if (adult_count == 1 && cantidad_rooms[f].childCount == 0 && single == 0) {
           single  = (cantidad_rooms[f].price / parseInt(noches));
        }
        else if(adult_count == 1 && single == 0) {
          single    = ((cantidad_rooms[f].price / (parseInt(cantidad_rooms[f].adultCount) + parseInt(cantidad_rooms[f].childCount)))/ parseInt(noches));
          infante  += ((cantidad_rooms[f].price / (parseInt(cantidad_rooms[f].adultCount) + parseInt(cantidad_rooms[f].childCount)))/ parseInt(noches));
        }
        if (adult_count == 2 && cantidad_rooms[f].childCount == 0 && doble == 0) {
          price = cantidad_rooms[f].price / cantidad_rooms[f].roomCount;
          doble = ((price/2) / parseInt(noches));
        }
        else if(adult_count == 2 && doble == 0) {
          doble     = ((cantidad_rooms[f].price / (parseInt(cantidad_rooms[f].adultCount) + parseInt(cantidad_rooms[f].childCount)))/ parseInt(noches));
          infante  += ((cantidad_rooms[f].price / (parseInt(cantidad_rooms[f].adultCount) + parseInt(cantidad_rooms[f].childCount)))/ parseInt(noches));
        }
        if (adult_count == 3 && cantidad_rooms[f].childCount == 0 && Triple == 0) {
          price   = cantidad_rooms[f].price / cantidad_rooms[f].roomCount;
          Triple  = ((price/3) / parseInt(noches));
        }
        else if(adult_count == 3 && Triple == 0) {
          Triple    = ((cantidad_rooms[f].price / (parseInt(cantidad_rooms[f].adultCount) + parseInt(cantidad_rooms[f].childCount)))/ parseInt(noches));
          infante  += ((cantidad_rooms[f].price / (parseInt(cantidad_rooms[f].adultCount) + parseInt(cantidad_rooms[f].childCount)))/ parseInt(noches));
        }
        if (adult_count >= 4 && cantidad_rooms[f].childCount == 0 && cuadruple == 0) {
          price     = cantidad_rooms[f].price / cantidad_rooms[f].roomCount;
          cuadruple = ((price/cantidad_rooms[f].adultCount) / parseInt(noches));
        }
        else if(adult_count == 4 && cuadruple == 0) {
          cuadruple   =  ((cantidad_rooms[f].price / (parseInt(cantidad_rooms[f].adultCount) + parseInt(cantidad_rooms[f].childCount)))/ parseInt(noches));
          infante    += ((cantidad_rooms[f].price / (parseInt(cantidad_rooms[f].adultCount) + parseInt(cantidad_rooms[f].childCount)))/ parseInt(noches));
        }

        room_type += ((f+1) == cantidad_rooms.length) ? cantidad_rooms[f].roomType : cantidad_rooms[f].roomType + ", ";
      }
      observacion     = $('#tex-observacion-' + cod_seleccion).val();
      observacion_int = $('#tex-observacion-int-' + cod_seleccion).val();
      longitude = (datos_rooms.longitude) ? datos_rooms.longitude : '';
      latitude  = (datos_rooms.latitude) ? datos_rooms.latitude : '';
      nameHotel = datos_rooms.hotelName
      if(nameHotel.search("Casino")){
        nameHotel = nameHotel.replace("Casino","Casinó");
      }
      hotel_array = {
                      hotelCode    : datos_rooms.hotelCode,
                      provider     : datos_rooms.provider,
                      nombre_hotel : nameHotel,
                      room_type    : room_type,
                      estrellas    : datos_rooms.category,
                      star_number  : datos_rooms.estrella,
                      dest_Code    : datos_rooms.destCode,
                      fecha_from   : $('#checkin').val(),
                      fecha_to     : $('#checkout').val(),
                      single       : single.toFixed(2),
                      doble        : doble.toFixed(2),
                      triple       : Triple.toFixed(2),
                      cuadruple    : cuadruple.toFixed(2),
                      menor        : infante.toFixed(2),
                      alimentacion : cantidad_rooms[0].board,
                      observacion  : observacion,
                      observacion_int  : observacion_int,
                      link_search  : link_search,
                      moneda       : moneda,
                      cod_seleccion:cod_seleccion,
                      latitude     : latitude,
                      longitude    : longitude,
                      total        : total.toFixed(2)
                    };
      hoteles_array.push(hotel_array);
    }
  }
  return hoteles_array;
}

function Update_Cotizacion() {
  valores      = $('.requiere-validar');
  contar_vacio = 0;
  for (var i = valores.length - 1; i >= 0; i--) {
    if($(valores[i]).val() == '') {
      $(valores[i]).css({'border-color':'#ff3300'});
      contar_vacio++;
    }
  }
  if(contar_vacio != 0) {
    return;
  }
  var id_cotizacion        = $('#id_cotizacion').val();
  var id_ciudad_cotizacion = $('input[name=city_destinos]:checked');
  var id_ciudad_cotizacion = $(id_ciudad_cotizacion).data("idcity");
  var sobre_escribir       = $('#sobre_escribir').is(":checked");
  // var hoteles              =  JSON.stringify(armar_envio_cotizacion());
  var hoteles              =  JSON.stringify(armar_new_envio_cotizacion());
  $('#div_btn_cotactual').hide();
  $('#loading').show();
  $.ajax({
    url: "views/Pqt_Integracion/conexion_cotizacion.php",
    dataType : "html",
    type : "post",
    data:{selector : "update_cotizacion", id_ciudad : id_ciudad_cotizacion, id_cotizacion : id_cotizacion, sobre_escribir : sobre_escribir, hoteles_array : hoteles}
  })
  .done(function(response){
    var result = JSON.parse(response);
      $('#loading').hide();
      if(result[0] == 'no') {
        $('#error').show().html(result[1]);
        setTimeout(function(){ $('#error').slideUp(); $('#div_btn_cotactual').show();}, 3000);
      }
      else if(result[0] == 'si') {
        $('#confirm').show().html(result[1]);
        setTimeout(function(){ $('#confirm').slideUp(); $('#div_btn_cotactual').show();}, 3000);
      }
  });
}

function update_publicacion() {
  valores      = $('.requiere-validar-pub');
  contar_vacio = 0;
  for (var i = valores.length - 1; i >= 0; i--) {
    if($(valores[i]).val() == '') {
      $(valores[i]).css({'border-color':'#ff3300'});
      contar_vacio++;
    }
  }
  if(contar_vacio != 0) {
    return;
  }
  var id_publicacion        = $('#id_publicacion').val();
  var id_ciudad_publicacion = $('input[name=dest_publicacion]:checked');
  var id_ciudad_publicacion = $(id_ciudad_publicacion).data("idcity");
  // var sobre_escribir       = $('#sobre_escribir').is(":checked");
  // var hoteles              =  JSON.stringify(armar_envio_cotizacion());
  var hoteles              =  JSON.stringify(armar_new_envio_cotizacion());

  $('#div_btn_publicacion').hide();
  $('#loading').show();
  $.ajax({
    url: "views/Pqt_Integracion/conexion_cotizacion.php",
    dataType : "html",
    type : "post",
    data:{selector : "update_publicacion", id_ciudad : id_ciudad_publicacion, id_publicacion : id_publicacion, hoteles_array : hoteles}
  })
  .done(function(response) {
    var result = JSON.parse(response);
      $('#loading').hide();
      if(result[0] == 'no') {
        $('#error').show().html(result[1]);
        setTimeout(function(){ $('#error').slideUp(); $('#div_btn_publicacion').show();}, 3000);
      }
      else if(result[0] == 'si') {
        $('#confirm').show().html(result[1]);
        setTimeout(function(){ $('#confirm').slideUp(); $('#div_btn_publicacion').show();}, 3000);
      }
  });
}

function validar_input_vacios(id) {
  if($("#"+id).val().length > 2) {
    $("#"+id).css({'border-color' : '#BEBBBB'});
  }
}

function crear_nueva_cotizacion() {
  //Verificacion de valores vacios al crear una cotizacion
  valores      = $('.requiere');
  contar_vacio = 0;
  for (var i = valores.length - 1; i >= 0; i--) {
    if($(valores[i]).val() == '' && $(valores[i]).attr("id") != 'name_solicitante') {
      $(valores[i]).css({'border-color':'#ff3300'});
      contar_vacio++;
    }
    else if($(valores[i]).val() == '' && $(valores[i]).attr("id") == 'name_solicitante' && $('#check_agencia').is(":checked")) {
      $(valores[i]).css({'border-color':'#ff3300'});
      contar_vacio++;
    }
  }
  if(contar_vacio != 0) {
    return;
  }
  // asta aca la veriicacion

  var titulo          = $('#ntitulo_cotizacion').val();
  var cod_usuario_v   = $('#id_user_viacon').val();
  var cod_cliente     = $('#ncod_cliente').val();
  var cod_solicitante = $('#ncod_solicitante').val();
  var ciudad_salida   = $('#nciudad_salida').val();
  var salida_fecha    = $('#nsalida_fecha').val();
  var retorno_fecha   = $('#nentrada_fecha').val();
  var observacion     = $('#observacion_main').val();
  // var hoteles         =  JSON.stringify(armar_envio_cotizacion());
  var hoteles         =  JSON.stringify(armar_new_envio_cotizacion());

  $('#div_btn_cotcrear').hide();
  $('#loading').show();
  $.ajax({
    url :"views/Pqt_Integracion/conexion_cotizacion.php",
    dataType: "html",
    type: "post",
    data :{ selector : "crear_cotizacion",title : titulo, cod_client : cod_cliente, idsolicitante : cod_solicitante, cod_usuario_v : cod_usuario_v,
            city_out : ciudad_salida, date_out : salida_fecha, date_return : retorno_fecha,observacion : observacion, hoteles_array : hoteles
          },
    success: function(response){
      var result = JSON.parse(response);
      $('#loading').hide();
      if(result[0] == 'no') {
        $('#error').show().html(result[1]);
        setTimeout(function(){ $('#error').slideUp(); $('#div_btn_cotcrear').show();}, 3000);
      }
      else if(result[0] == 'si') {
        $('#confirm').show().html(result[1]);
        setTimeout(function(){ $('#confirm').slideUp(); $('#div_btn_cotcrear').show();}, 3000);
      }
      else {
        // console.log(result);
        $('#confirm').show().html(result);
        $('#div_btn_cotcrear').show();
      }

    }
  });
}

function saveCotizacion(id) {

  var contenido = new Array()
  var item      = $('#' + id).data('value')
  var itemSaved = JSON.parse(localStorage.getItem('cotizacion'))

  if (itemSaved != null && itemSaved.length > 0) {
    itemSaved.push(item)
    localStorage.setItem('cotizacion',JSON.stringify(itemSaved)) 

  }
  else {
    contenido.push(item)
    localStorage.setItem('cotizacion', JSON.stringify(contenido)) 
  }
  update_count_cotizacion()
}

function update_count_cotizacion() {
  var select = JSON.parse(localStorage.getItem('cotizacion')) 
  if(select !== null && select.length != 0) {
    $('#new_cotizacion').show('drop', {direction: 'right'});
    $('.btn-cot').html('Cotizaci&oacute;n (' + select.length + ')')
  }
  else {
    $('#new_cotizacion').hide('drop', {direction: 'right'});
  }
}

function delete_item(id) {
  var item  = id
  var items = JSON.parse(localStorage.getItem('cotizacion'))
  for(var i = 0; i < items.length; i++) {
    if(items[i].input_room_code == item) {
      items.splice(i, 1)
    }
  }
  localStorage.setItem('cotizacion', JSON.stringify(items))
  update_count_cotizacion()
}

function delete_select(d, index) {
  delete_item(index)
  $('#cotiza-' + d).remove();
  $('#' + index).prop('checked', false)
  update_count_cotizacion()
}

function add_item_room(id) {
  if($('#' + id).is(":checked")) {
    agregar_item(id)
  }
  else {
    delete_item(id)
  }
}

function verificar_parametros(params_new) {
  params_olds   = JSON.parse(localStorage.getItem('search_params'));
  hubo_cambios  = false
  if (params_olds == null) {
    localStorage.setItem('search_params', JSON.stringify(params_new))
    return false
  }

  if (params_new.habitaciones != params_olds.habitaciones) {
    hubo_cambios = true;
  }
  else if (params_new.checkin != params_olds.checkin || params_new.checkout != params_olds.checkout) {
    hubo_cambios = true
  }
  else if (params_new.destination_code != params_olds.destination_code) {
    hubo_cambios = true
  }
  else {
    hubo_cambios = verificar_rooms(params_new, params_olds)  
  }
  
  if (hubo_cambios) {
    localStorage.setItem('search_params', JSON.stringify(params_new))
    localStorage.setItem('cotizacion', null)
    localStorage.setItem('compare', null)
  }
  
  return hubo_cambios
}

function verificar_rooms(params_new, params_olds) {
  if(params_new.habitaciones != params_olds.habitaciones) return false;
  for(var i = 1;i <= params_new.habitaciones; i++) {
    if (params_new['adultos' + i] != params_olds['adultos' + i] ) {
      return true;
    }
  }
}

function verificar_seleccionados() {
  items   = JSON.parse(localStorage.getItem('cotizacion'));
  if (items != null) {
    for (var i = 0; i < items.length; i++) {
      var id = items[i].inputCode
      if($('#main_hotel_' + items[i].inputCode).length) {
        var itemrooms = $('.list_check_' + id);
        var con       = 1
        for(var d = 0;d < itemrooms.length; d++) {
          var price       = 0
          var price_save  = 0
          var id_it       = JSON.parse($(itemrooms[d]).val())
          for(var f = 0; f < id_it.rooms.length; f++) {
            price_save  += items[i].rooms[f].price
            price       += id_it.rooms[f].price
          }
          var ac = items[i].inputCode + '_' + con
          if(price == price_save && ac == items[i].input_room_code) {
            $('#main_hotel_' + items[i].inputCode).prop("checked", true)
            $('.list_check_' + items[i].inputCode).show()
            $('#' + items[i].inputCode + '_' + con).prop("checked", true)
            habilitar_marcador(items[i].inputCode)
          }
          con++
        }  
      }
    }
  }
}

function initComponentsCotizacion() {
  $("#name_cliente").autocomplete({
    source: function( request, response ) {
    var tipo_cliente = $('#check_natural').is(':checked') ? 1 : 2;
    $('#load-cliente').show();
    $.ajax({
        url: "views/Pqt_Integracion/conexion_cotizacion.php",
        dataType: "json",
        data: {selector : "Buscar_cliente", tipo_client : tipo_cliente ,term: request.term},
        success: function(data) {
                    $('#load-cliente').hide();
                    response($.map(data, function(item) {
                    return {
                              label: item.label,
                              value: item.value,
                              id_cliente: item.id_cliente
                            };
                }));
            }
        });
    },
    minLength: 4,
    select: function(event, ui) {
        $('#ncod_cliente').val(ui.item.id_cliente);
        $("#name_cliente").css({'border-color' : '##BEBBBB'});
    }
  });

  $("#name_solicitante").autocomplete({
      source: function( request, response ) {
      var tipo_cliente  = $('#check_natural').is(':checked') ? 1 : 2;
      var cod_cliente   = $('#ncod_cliente').val();

      $.ajax({
          url: "views/Pqt_Integracion/conexion_cotizacion.php",
          dataType: "json",
          data: {selector : "Buscar_solicitante", id_cliente : cod_cliente, term: request.term},
          success: function(data) {
                      response($.map(data, function(item) {
                      return  {
                                label: item.label,
                                value: item.value,
                                id_solicitante : item.id_solicitante
                              };
                  }));
              }
          });
      },
      minLength: 1,
      select: function(event, ui) {
          $('#ncod_solicitante').val(ui.item.id_solicitante);
          $("#name_solicitante").css({'border-color' : '##BEBBBB'});
      }
  });

  $('#nsalida_fecha').datepicker({
    numberOfMonths: 2,
    onClose: function(selectedDate) {
      $("#nentrada_fecha").datepicker("option", "minDate", selectedDate);
      $("#nentrada_fecha").datepicker('show');
      $('#nsalida_fecha').css({'border-color' : '#BEBBBB'});
    }
  });

  $('#nsalida_fecha').datepicker("option", "dateFormat", 'dd/mm/yy');

  $('#nentrada_fecha').datepicker({
    numberOfMonths: 2,
    onClose: function(selectedDate){
      $('#nentrada_fecha').css({'border-color':'#BEBBBB'});
    }
  });

  $('#nentrada_fecha').datepicker("option", "dateFormat", 'dd/mm/yy');
}

$(function() {
  // verificar_parametros(data)
  update_count_cotizacion() 

});
