<?php global $rs; ?>
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-3"><?php include('views/admin/_menu.php'); ?></div>
      <div class="col-md-9">
      <div id="filter" class="col-md-12 wbox" style="padding: 15px;">
        <form id="search_books_form" class="col-md-12">
          <div class="form-group col-md-3">
            <label for="checkout">Proveedor</label>
            <select class="form-control" name="provider_id" id="user_id">
              <option value="150" <?php echo (isset($_GET['provider_id']) && $_GET['provider_id'] == Provider::METHABOOK2) ? 'selected' : ''; ?>>Methabook2</option>
              <option value="159" <?php echo (isset($_GET['provider_id']) && $_GET['provider_id'] == Provider::DOMITUR2) ? 'selected' : ''; ?>>Domitur2</option>
              <option value="1" <?php echo (isset($_GET['provider_id']) && $_GET['provider_id'] == Provider::HOTELBEDS) ? 'selected' : ''; ?>>Hotel Beds</option>
              <option value="80" <?php echo (isset($_GET['provider_id']) && $_GET['provider_id'] == Provider::TOURICO) ? 'selected' : ''; ?>>Tourico</option>
               <option value="3" <?php echo (isset($_GET['provider_id']) && $_GET['provider_id'] == Provider::RESTEL) ? 'selected' : ''; ?>>Restel</option>
            </select>
          </div>
          <div class="form-group col-md-3">
            <label for="checkin">Desde</label>
            <input type="text" class="form-control" id="from" name="from" placeholder="dd/mm/aa" autocomplete="off">
          </div>

          <div class="form-group col-md-3">
            <label for="checkout">Hasta</label>
            <input type="text" class="form-control" id="to" name="to" placeholder="dd/mm/aa" autocomplete="off">
          </div>
          <div class="form-group col-md-1">
            <label>&nbsp;</label><br />
            <button class="btn" id="search_books" name="search_books"><i class="fa fa-search"></i></button>
          </div>
          <input type="hidden" name="controller" value="booking" />
          <input type="hidden" name="action" value="purchase_detail_list" />
        </form>
      </div>
      <div class="">
      <?php  if($rs != '') {
              $books_arr = array();
              foreach ($rs as  $value) {
                $books = array();
                if (!empty($value['localizer'])) {
                  $query = "SELECT Booking.id,Booking.holder,Booking.agency_id,Booking.status FROM Booking, Service WHERE Booking.id = Service.booking_id AND Service.localizer like '%" . $value['localizer'] . "%'";
                  $books = Booking::execQuery($query);
                }
                if (count($books) > 0) {
                  foreach ($books as $book) {
                    $books_list               = array();
                    $books_list['localizer']  = $value['localizer'];
                    $books_list['locSub']     = (isset($value['locSub'])) ? $value['locSub'] : '';
                    $books_list['agencyName'] = Agency::findById($book['agency_id'])->name;
                    $books_list['id']         = $book['id'];
                    $books_list['status_provider']     = $value['status_provider'];
                    $books_list['statu_zion']  = $book['status'];
                    $arrPx = array();
                    foreach ($value['holder'] as $pax) {
                      $arrPx[] = $pax;
                    }
                    $books_list['pax']  = $arrPx;
                    $books_arr[]        = $books_list;
                  }
                }
                else {
                  $books_list               = array();
                  $books_list['localizer']  = $value['localizer'];
                  $books_list['locSub']     = (isset($value['locSub'])) ? $value['locSub'] : '';
                  $books_list['agencyName'] = "";
                  $books_list['id']         = "";
                  $books_list['status_provider']     = $value['status_provider'];
                  $books_list['statu_zion']  = "";
                  $arrPx = array();
                  foreach ($value['holder'] as $pax) {
                    $arrPx[] = $pax;
                  }
                  $books_list['pax']  = $arrPx;
                  $books_arr[] = $books_list;
                }
              }
              foreach ($books_arr as  $value) { 
                $style = ($value['id'] == "") ? 'border:1px solid red' : '';
                $color = ($value['id'] == "") ? 'color:red' : 'color:green';
                $icon =  ($value['id'] == "")  ? 'fa-exclamation-circle' : 'fa-check-circle';
          ?>
                <div class="col-md-12 box" style="margin-top:15px;<?php echo $style; ?>">
                  <div style="position: absolute;right: 10px;top: 5px;font-size: 27px;<?php echo $color; ?>">
                    <i class="fa <?php echo $icon; ?>"></i>
                  </div>
                  <div class="col-md-2">
                    <label>Localizador: </label>&nbsp;<?php echo $value['localizer']; ?>
                    <?php
                      if (strpos($value['locSub'], '-') && $value['locSub'] != '') 
                        echo  '<br/><strong>' . $value['locSub'] . '</strong>';
                    ?>
                  </div>
                  <div class="col-md-4">
                  <label>Pasajero: </label><br/>
                    <?php  
                        foreach ($value['pax'] as $pax) {
                          echo $pax . "<br/>";
                        } 
                    ?>
                  </div>
                  <div class="col-md-3">
                    <?php 
                      $cad = "<label>Mensaje: </label>&nbsp;Esta reserva no se encuentra registrado en el sistema.";
                      if ($value['agencyName'] != "") {
                        $cad = "<label>Agencia: </label>&nbsp;" . $value['agencyName'];   
                      }
                      echo $cad;
                    ?>
                  </div>
                  <div class="col-md-3">
                    <label></label>&nbsp;
                    <?php  if($value['id'] != "") { ?>
                    <a href='http://boliviabooking.com/hotelesboliviabooking/zion/?controller=booking&action=show&id=<?php echo $value['id'] ?>' target="_blank" style="border: 1px solid #ff3300;padding: 6px 20px;border-radius: 4px;">Ver</a>
                    <?php  } ?>
                  </div>
                  <div class="col-md-12">
                    <table class="table">
                      <tr>
                      <?php if (isset($book) && count($book) > 0) { ?>
                            <th>Estado Proveedor</th>
                            <th>Estado Barry</th>
                      <?php } else { ?>
                            <th>Estado Proveedor</th>
                            <th>Estado Barry</th>
                      <?php } ?>
                      </tr>
                      <tr>
                        <td>
                          <?php
                            if ($value['status_provider'] == ServiceStatus::CONFIRMED) {
                              echo '<span class="label label-success"><i class="fa fa-check-circle"></i> Confirmada</span> ';
                            }
                            else if ($value['status_provider'] == ServiceStatus::PENDING) {
                              echo '<span class="label label-default"><i class="fa fa-exclamation-circle"></i> Pendiente</span>';
                            }
                            else if ($value['status_provider'] == ServiceStatus::CANCELLED) {
                              echo '<span class="label label-danger"><i class="fa fa-times-circle"></i> Cancelada</span>';
                            }
                            else {
                              echo $value['status_provider'];
                            }
                          ?>
                        </td>
                        <td>
                          <?php
                            if ($value['statu_zion'] == ServiceStatus::PAYED) {
                              echo '<span class="label label-success"><i class="fa fa-check-circle"></i> Confirmada</span> ';
                              echo '<span class="label label-success"><i class="fa fa-check-circle"></i> Pagada</span>';
                            }
                            if ($value['statu_zion'] == ServiceStatus::CONFIRMED) {
                              echo '<span class="label label-success"><i class="fa fa-check-circle"></i> Confirmada</span> ';
                              echo '<span class="label label-warning"><i class="fa fa-exclamation-circle"></i> Pago pendiente</span>';
                            }
                            if ($value['statu_zion'] == ServiceStatus::PENDING) {
                              echo '<span class="label label-default"><i class="fa fa-exclamation-circle"></i> Pendiente</span>';
                            }
                            if ($value['statu_zion'] == ServiceStatus::CANCELLED) {
                              echo '<span class="label label-danger"><i class="fa fa-times-circle"></i> Cancelada</span>';
                            }
                            if ($value['statu_zion'] == ServiceStatus::ANNULLED) {
                              echo '<span class="label label-danger"><i class="fa fa-times-circle"></i> Anulado</span>'; 
                            }
                          ?>
                        </td>
                      </tr>
                    </table>
                  </div>
                </div>
          <?php  } ?>

      <?php  }  ?>
    </div>
    </div>
  </div>
  </div>
</div>
<script text="javascript">
  $('#from').datepicker({
    numberOfMonths: 2,
    dateFormat: "dd/mm/yy",
    onClose: function(selectedDate) {
    }
  });

  $('#to').datepicker({
    numberOfMonths: 2,
    dateFormat: "dd/mm/yy",
    onClose: function(selectedDate) {

    }
  });
  date = new Date();
  primer = new Date(date.getFullYear(), date.getMonth(), 1);
  ultimoDia = new Date(date.getFullYear(), date.getMonth() + 1, 0);
  <?php if (!isset($_GET['from'])) { ?>
      $( "#from" ).datepicker( "setDate", primer);
      $( "#to" ).datepicker( "setDate", ultimoDia);
  <?php }else { ?>
      $('#from').val('<?php echo _get('from'); ?>');
      $('#to').val('<?php echo _get('to'); ?>');
  <?php } ?>


</script>
