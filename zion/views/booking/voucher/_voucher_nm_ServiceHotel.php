<?php
$rooms = $serviceInfo->getRoomsAvail();
$detail = NULL;
foreach ($details as $key => $value) {
  if (!empty($value) && $value->getCode() == $serviceInfo->getCode()) {
    $detail = $value;
  }
}
?>

<div class="voucher-content">
  <table style="width: 100%;">
    <tr>
      <td style="width: 35%;">
        <img src="<?php echo $agency->logo; ?>" onerror='this.onerror=null;this.src=\"https://yocounter.com/hoteles_freelance/zion/assets/images/no-photo.jpg\";' />
      </td>
      <td style="width: 40%; padding-top: 10px; padding-left: 50px; font-size: 12px">
        <strong><?php echo $agency->name; ?></strong><br />
        <?php echo $agency->address; ?><br />
        <?php echo $agency->phone; ?><br />
        <?php echo $agency->mail; ?>
      </td>
      <td style="width: 30%;">
        <div class="text-right hidden-print avoid-mailing">
          <div class="link" onclick="showSendMail('Voucher <?php echo htmlspecialchars($serviceInfo->getName()); ?>', 'voucher_<?php echo $service_count; ?>_content');">
            <span class="fa-stack fa-2x">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
            </span>
          </div>
          <div class="link" onclick="printDiv('voucher_<?php echo $service_count; ?>_content')">
            <span class="fa-stack fa-2x">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-print fa-stack-1x fa-inverse"></i>
            </span>
          </div>
        </div>
      </td>
    </tr>
  </table>

  <div style="text-align: center; margin-top: 10px; padding: 15px; background-color: #fff; border-radius: 5px; border: 1px solid #999; ">
    <strong>Bono - Alojamiento</strong> / Voucher - Accommodation<br />
    <strong>Reserva Confirmada y Garantizada - Hotel</strong> / Booking confirmed and guaranteed - Hotel
  </div>

  <div style="margin-top: 10px; padding: 15px; background-color: #fff; border-radius: 5px; border: 1px solid #999; ">
    <table style="width: 100%;">
      <tr>
        <td style="border-right: 1px solid #ccc; width: 40%;">
          <strong>Localizador</strong> / Reference number :
          <div style="color: #036d9f; font-size: 25px;">
            <?php echo $service->getLocalizer(); ?>
            <br/>
            <?php echo $service->getSPUI(); ?>
          </div>
          <div style="font-size:0.85em; color: #999;"><strong>V&aacute;lido para el hotel</strong> / Valid for the hotel</div>
        </td>
        <td style="width: 60%; padding-left: 15px;">
          <span style="color: #3399f3; font-size: 18px;"><?php echo $serviceInfo->getName() ?></span> &nbsp;
          <span>
            &nbsp; <?php if ($detail != NULL) { showCategory($detail->getCategoryCode()); } ?>
          </span><br />
          <table border="0">
            <tr>
              <td style="vertical-align: top;"><strong>Nombre del Pasajero</strong> / Pax name :</td>
              <td>
                <?php
                $firstPax = true;
                foreach ($rooms as $room) {
                  $guestList = $room->getGuestList();
                  foreach ($guestList as $guest) {
                    if ($guest->getName() != '') {
                      if (!$firstPax) {
                        echo '<br />';
                      }
                      echo ' &nbsp; ' . $guest->getName() . ' ' . $guest->getLastName();
                      $firstPax = false;
                    }
                  }
                }
                ?>
              </td>
            </tr>
          </table>
          <strong>Fecha confirmaci&oacute;n reserva</strong> / Booking date : <?php echo dateFormatFromDB($book->confirm_date); ?><br />
          <strong>Ref. Agencia </strong> / Agency ref. : <?php echo $booking_user_name; ?>/<?php echo $book->agency ? $book->agency : $agency->name; ?><br />
        </td>
      </tr>
    </table>
  </div>

  <div style="margin-top: 10px; padding: 15px; background-color: #fff; border-radius: 5px; border: 1px solid #999; ">
    <div>
      <strong>Desde</strong> / From : <?php echo dateToDDMMYY($service->getDateFrom()); ?> -
      <strong>Hasta</strong> / To : <?php echo dateToDDMMYY($service->getDateTo()); ?>
      <?php $from = new DateTime($service->getDateFrom());$to = new DateTime($service->getDateTo()); echo "(" . $to->diff($from)->days . " noches)"; ?>
    </div>
  </div>

  <div style="margin-top: 10px; padding: 15px; background-color: #fff; border-radius: 5px; border: 1px solid #999; ">
    <div>
      <table style="width: 100%;">
        <thead>
          <tr>
            <td><strong>Tipo de habitaci&oacute;n</strong><br />Room type</td>
            <td><strong>R&eacute;gimen</strong><br />Board</td>
            <td><strong>Adultos</strong><br />Adults</td>
            <td><strong>Ni&ntilde;os</strong><br />Children</td>
            <td><strong>Edad Ni&ntilde;os</strong><br />Children ages</td>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach ($rooms as $room) {
            ?>
            <tr style="border-top: 1px solid #ccc;">
              <td style="padding: 10px 0;"><?php echo $room->getRoomType() . ' x ' . $room->getRoomCount(); ?></td>
              <td style="padding: 10px 0;"><?php echo $room->getBoard(); ?></td>
              <td style="padding: 10px 0;">
                <?php
                $adult_str = $room->getAdultCount() > 1 ? ' adultos' : ' adulto';
                echo $room->getAdultCount() . $adult_str;
                ?>
              </td>
              <td style="padding: 10px 0;">
                <?php
                if ($room->getChildCount() > 0) {
                  $child_str = $room->getChildCount() > 1 ? ' ni&ntilde;os' : ' ni&ntilde;o';
                  echo $room->getChildCount() . $child_str;
                }
                else {
                  echo "-";
                }
                ?>
              </td>
              <td style="padding: 10px 0;">
                <?php
                $first = true;
                foreach ($room->getGuestList() as $guest) {
                  if ($guest->getType() == 'CH') {
                    echo $first ? '' : ', ';
                    $first = false;
                    echo $guest->getAge();
                  }
                }
                ?>
              </td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
      <?php if ($serviceInfo->getContractComment() != '') { ?>
        <div>
          <big><strong>Observaciones</strong> / Remarks</big>
          <div>phpphp echo $serviceInfo->getContractComment(); ?></div>
        </div>
      <?php } ?>
      <?php if ($serviceInfo->getClientComment() != '') { ?>
        <div>
          <big><strong>Comentarios del cliente</strong> / Customer remarks</big>
          <div>
            <?php echo $serviceInfo->getClientComment(); ?>.
            <span>Las observaciones para el establecimiento son solo indicativas, &eacute;stas no pueden ser garantizadas.</span>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>

  <?php if ($detail != NULL) { ?>
    <div style="margin-top: 10px; padding: 15px; background-color: #fff; border-radius: 5px; border: 1px solid #999; ">
      <table style="width: 100%;">
        <tr>
          <td style="width: 40%;">
            <div style="width: 100%; height: 250px; max-height: 250px;">
              <img src="http://maps.googleapis.com/maps/api/staticmap?zoom=16&amp;size=350x280&amp;markers=color:red|label:H|<?php echo $detail->getLatitude(); ?>,<?php echo $detail->getLongitude(); ?>&amp;sensor=false&amp;key=AIzaSyAN2JTbJ5ALoBrHk0DlB4TgpQI30qUhyjA" style="width: 100%;">
            </div>
          </td>
          <td style="width: 60%; padding-left: 15px; vertical-align: top;">
            <div>
              <span style="color: #3399f3; font-size: 18px;"><?php echo $serviceInfo->getName() ?></span> &nbsp;
              <span>
                &nbsp; <?php showCategory($detail->getCategoryCode()); ?>
              </span><br />
            </div>
            <table>
              <tbody>
                <tr>
                  <td style="font-weight: bold; padding: 2px 20px; vertical-align: top;">Direcci&oacute;n:</td>
                  <?php $dir = $detail->getAddressStreetName() . ', ' . $detail->getAddressNumber(). ' - ' . $detail->getAddressPostalCode(); ?>
                  <td>
                    <?php echo $dir ?><br />
                    <?php echo $detail->getDestinationZone() . ', ' . $detail->getDestinationName(); ?><br />
                  </td>
                </tr>
                <tr style="border-top: 1px solid #ccc;">
                  <td style="font-weight: bold; padding: 2px 20px;">Emails:</td>
                  <td>
                    <?php
                    foreach ($detail->getEmailList() as $mail) {
                      echo $mail . '<br />';
                    }
                    ?>
                   </td>
                </tr>
                <tr style="border-top: 1px solid #ccc;">
                  <td style="font-weight: bold; padding: 2px 20px;">Tel&eacute;fonos:</td>
                  <td>
                    <?php
                    foreach ($detail->getPhoneList() as $item) {
                      echo $item . '<br />';
                    }
                    ?>
                   </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </table>
    </div>
  <?php } ?>

  <div style="margin-top: 10px;">
    <strong>Reservado y pagadero por</strong> / Bookable and payable by
    <?php echo $serviceInfo->getSupplier() . ' ' . $serviceInfo->getSupplierVatNumber(); ?>
    <div style="color: #777; padding-top: 5px;">
      <i>Con el respaldo de <strong>Barry Top Services S.R.L.</strong></i><br/>
      <strong>NUMERO DE EMERGENCIAS</strong>
      (+591) 72122266 BARRY SANTA CRUZ.
    </div>
  </div>
</div>
