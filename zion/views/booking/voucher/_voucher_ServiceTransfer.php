<?php
$transferType_str = $serviceInfo->getTransferType() == 'IN' ? 'Llegada' : 'Salida';
?>
<div class="voucher-content">
  <table style="width: 100%;">
    <tr>
      <td style="width: 35%;">
        <img src="<?php echo $agency->logo; ?>" onerror="this.onerror=null;this.src='https://yocounter.com/hoteles_freelance/zion/assets/images/agency_no_logo.png';" />
      </td>
      <td style="width: 40%; padding-top: 10px; padding-left: 50px; font-size: 12px">
        <strong><?php echo $agency->name; ?></strong><br />
        <?php echo $agency->address; ?><br />
        <?php echo $agency->phone; ?><br />
        <?php echo $agency->mail; ?>
      </td>
      <td style="width: 30%;">
        <div class="text-right hidden-print avoid-mailing">
          <div class="link" onclick="showSendMail('Voucher <?php echo htmlspecialchars($serviceInfo->getName()); ?>', 'voucher_<?php echo $service_count; ?>_content');">
            <span class="fa-stack fa-2x">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
            </span>
          </div>
          <div class="link" onclick="printDiv('voucher_<?php echo $service_count; ?>_content')">
            <span class="fa-stack fa-2x">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-print fa-stack-1x fa-inverse"></i>
            </span>
          </div>
        </div>
      </td>
    </tr>
  </table>

  <div style="text-align: center; margin-top: 10px; padding: 15px; background-color: #fff; border-radius: 5px; border: 1px solid #999; ">
    <strong>Bono - Traslado</strong> / Voucher - Transfer<br />
    <strong>Reserva Confirmada y Garantizada - Traslado</strong> / Booking confirmed and guaranteed - Transfer
  </div>

  <div style="margin-top: 10px; padding: 15px; background-color: #fff; border-radius: 5px; border: 1px solid #999; ">
    <table style="width: 100%;">
      <tr>
        <td style="border-right: 1px solid #ccc; width: 40%;">
          <strong>Localizador</strong> / Reference number :
          <div style="color: #036d9f; font-size: 35px;">
            <?php echo $HBService->getReferenceIncomingOffice() . '-' . $HBService->getReferenceFileNumber(); ?>
          </div>
        </td>
        <td style="width: 60%; padding-left: 15px;">
          <div style="color: #3399f3; font-size: 18px;"><?php echo $serviceInfo->getName() ?></div>
          <table border="0">
            <tr>
              <td style="vertical-align: top;"><strong>Nombre del Pasajero</strong> / Pax name :</td>
              <td>
                <?php
                $firstPax = true;
                foreach ($serviceInfo->getGuestList() as $guest) {
                  if ($guest->getName() != '') {
                    if (!$firstPax) {
                      echo '<br />';
                    }
                    echo ' &nbsp; ' . $guest->getName() . ' ' . $guest->getLastName();
                    $firstPax = false;
                  }
                }
                ?>
              </td>
            </tr>
          </table>
          <strong>Pasajeros</strong> / Paxs :
            <?php
              $adt_str = ($serviceInfo->getAdultCount() > 1) ? ' Adultos/Adults' : ' Adulto/Adult';
              echo $serviceInfo->getAdultCount() . $adt_str;
              $child_str = ($serviceInfo->getChildCount() > 1) ? ' ni&ntilde;os/children' : ' ni&ntilde;o/childs';
              if($serviceInfo->getChildCount() != 0) { echo ' - ' . $serviceInfo->getChildCount() . $child_str; }
            ?>
          <br />
          <strong>Fecha confirmaci&oacute;n reserva</strong> / Booking date : <?php echo dateToDDMMYY($HBService->getCreationDate()); ?><br />
          <strong>Ref. Agencia: </strong> / Agency ref. : <?php echo $HBService->getAgencyReference(); ?><br />
        </td>
      </tr>
    </table>
  </div>

  <div style="margin-top: 10px; padding: 15px; background-color: #fff; border-radius: 5px; border: 1px solid #999; ">
    <div>
      <strong>Desde</strong> / from: <?php echo $serviceInfo->getPickupLocationName(); ?></strong> &#8594;
      <strong>Hasta</strong> / to: <?php echo $serviceInfo->getDestLocationName(); ?>
    </div>
    <div>
      <strong>Fecha del servicio</strong> / Service date: <?php echo dateToDDMMYY($serviceInfo->getDateFrom()); ?>,
      <strong>Hora de recogida</strong> / Pick-up time: <?php echo formatTime($serviceInfo->getTimeFrom()); ?>
    </div>
    <div>
      <strong>Datos de viaje</strong> / Travel info: <br />
       &nbsp; &nbsp; <strong>Vuelo de salida</strong> / Departure Flight: <?php echo $serviceInfo->getTravelNumber(); ?>
       &nbsp; &nbsp; <strong>Hora de <?php echo $transferType_str; ?>: </strong> <?php echo formatTime($serviceInfo->getTravelInfoDepartTime()); ?>
    </div>
  </div>

  <div style="margin-top: 10px; padding: 15px; background-color: #fff; border-radius: 5px; border: 1px solid #999; ">
    <div>
      <?php if ($serviceInfo->getContractComment() != '') { ?>
        <div>
          <big><strong>Observaciones</strong> / Remarks</big>
          <div><?php echo nl2br($serviceInfo->getContractComment()); ?></div>
        </div>
      <?php } ?>
    </div>
  </div>

  <div style="margin-top: 10px;">
    <strong>Reservado y pagadero por</strong> / Bookable and payable by
    <?php echo $serviceInfo->getSupplier() . ' ' . $serviceInfo->getSupplierVatNumber(); ?>
    <div style="margin-top: 10px">
      <strong>NUMERO DE EMERGENCIAS</strong>
      591 72122266 BARRY SANTA CRUZ.
    </div>
    <div style="color: #777; padding-top: 5px;">
      <i>Con el respaldo de <strong>Barry Top Services S.R.L.</strong></i>
    </div>
  </div>
</div>
