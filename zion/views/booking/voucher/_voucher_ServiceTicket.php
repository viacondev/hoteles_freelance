<?php
$serviceInfo = $service->getServiceInfo();
?>
<div class="col-md-12 service hotel-service">
  <table style="width: 100%;">
    <tr>
      <td style="width: 35%;">
        <img src="<?php echo $agency->logo; ?>" />
      </td>
      <td style="width: 40%; padding-top: 10px; padding-left: 50px; font-size: 12px">
        <strong><?php echo $agency->name; ?></strong><br />
        <?php echo $agency->address; ?><br />
        <?php echo $agency->phone; ?><br />
        <?php echo $agency->mail; ?>
      </td>
      <td style="width: 30%;">
        <div class="text-right hidden-print avoid-mailing">
          <div class="link" onclick="showSendMail('Voucher <?php echo htmlspecialchars($serviceInfo->getName()); ?>', 'voucher_<?php echo $service_count; ?>_content');">
            <span class="fa-stack fa-2x">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
            </span>
          </div>
          <div class="link" onclick="printDiv('voucher_<?php echo $service_count; ?>_content')">
            <span class="fa-stack fa-2x">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-print fa-stack-1x fa-inverse"></i>
            </span>
          </div>
        </div>
      </td>
    </tr>
  </table>

  <div style="text-align: center; margin-top: 10px; padding: 15px; background-color: #fff; border-radius: 5px; border: 1px solid #999; ">
    <strong> Voucher - Ticket</strong> / Voucher - Ticket<br />
    <strong>Reserva Confirmada y Garantizada - Ticket</strong>/ Booking confirmed and guaranteed - Ticket
  </div>

  <div style="margin-top: 10px; padding: 15px; background-color: #fff; border-radius: 5px; border: 1px solid #999; ">
    <table style="width: 100%;">
      <tr>
        <td style="border-right: 1px solid #999; width: 40%;">
          <strong>Localizador</strong> / Reference number :
          <div style="color: #036d9f; font-size: 35px;">
            <?php echo $HBService->getReferenceIncomingOffice() . '-' . $HBService->getReferenceFileNumber(); ?>
          </div>
        </td>
        <td style="width: 60%; padding-left: 15px;">
          <span style="color: #3399f3; font-size: 18px;"><?php echo $serviceInfo->getName() ?></span> &nbsp;
          &nbsp;
          <table border="0">
            <tr>
              <td style="vertical-align: top;"><strong>Nombre del Pasajero</strong> / Pax name :&nbsp;</td>
              <td>
                <?php echo $HBService->getHolder()->getName() . ' ' . $HBService->getHolder()->getLastName(); ?>
              </td>
            </tr>
          </table>
          <strong>Fecha confirmaci&oacute;n reserva</strong> / Booking date : <?php echo dateToDDMMYY($HBService->getCreationDate()); ?><br />
          <strong>Ref. Agencia </strong> / Agency ref. :&nbsp;<?php echo $HBService->getAgencyReference(); ?><br />
        </td>
      </tr>
    </table>
  </div>

  <div style="margin-top: 10px; padding: 15px; background-color: #fff; border-radius: 5px; border: 1px solid #999; ">
    <div class="row tickets-info">
      <div class="col-md-12"><strong>Entradas:</strong><?php echo dateToDDMMYY($service->getDateFrom()); ?></div>
      <div class="col-md-9 col-xs-9">
        <?php
        echo $serviceInfo->getAdultCount() . ' adultos';
        if ($serviceInfo->getChildCount() != 0) {
          echo ', ' . $serviceInfo->getChildCount() . ' ni&ntilde;os';
        }
        echo ' (' . $serviceInfo->getName() . ')' ;
        ?>
      </div>
    </div><br />
    <div>
      <?php if ($serviceInfo->getContractComment() != '') { ?>
        <div>
          <big><strong>Observaciones</strong> / Remarks</big>
          <div><?php echo nl2br($serviceInfo->getContractComment()); ?></div>
        </div>
      <?php } ?>
    </div>
  </div>

  <div style="margin-top: 10px;">
    <strong>Reservado y pagadero por</strong> / Bookable and payable by
    <?php echo $serviceInfo->getSupplier() . ' ' . $serviceInfo->getSupplierVatNumber(); ?>
    <div style="margin-top: 10px">
      <strong>NUMERO DE EMERGENCIAS</strong>
      591 72122266 BARRY SANTA CRUZ.
    </div>
    <div style="color: #777; padding-top: 5px;">
      <i>Con el respaldo de <strong>Barry Top Services S.R.L.</strong></i>
    </div>
  </div>
</div>
