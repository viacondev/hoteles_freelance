<?php 
global $rs;
?>
<div id="shopping_cart_confirm" class="content">
  <div class="container">
    <div class="col-md-12 box">
      <div class="row">
        <div class="col-md-8 col-md-offset-1 col-xs-12 invoice">
          <!-- Invoice header -->
          <div class="row">
            <div id="status" class="col-md-8 col-xs-8">
              <?php
              if ($rs->getStatus() == "BOOKING") {
                echo "Factura proforma";
              }
              else {
                echo "Error al confirmar la reserva. Favor vuelva a intentar.";
              }
              ?>
            </div>
            <div class="col-md-4 text-right col-xs-4">
              <!-- TODO: should this come here or the current date? -->
              <?php echo dateToDDMMYY($rs->getCreationDate()); ?>
            </div>
          </div>

          <!-- Agency Info -->
          <div class="row" style="margin-top: 15px;">
            <div id="agency_logo" class="col-md-6 col-xs-6"><?php loadImage('resources/viacon.png'); ?></div>
            <div class="col-md-6 col-xs-6" style="padding-top: 10px; padding-left: 50px;">
              <strong>Viacon Tours</strong><br />
              Calle Rene Moreno N&ordm;215<br />
              (591) 336-3610 / (591) 72659538<br />
              info@viacontours.com
            </div>
          </div>

          <!-- Invoce data -->
          <div class="col-md-12 service">
            <div class="box-header">Datos de la reserva</div>
            <div class="row">
              <div class="col-md-6 col-xs-6">
                <strong>Localizador: </strong><?php echo $rs->getReferenceIncomingOffice() . '-' . $rs->getReferenceFileNumber(); ?><br />
                <strong>Nombre del Pasajero: </strong><?php echo $rs->getHolder()->getName() . ' ' . $rs->getHolder()->getLastName(); ?><br />
              </div>
              <div class="col-md-6 col-xs-6">
                <strong>Fecha confirmaci&oacute;n reserva: </strong><?php echo dateToDDMMYY($rs->getCreationDate()); ?><br />
                <strong>Ref. Agencia: </strong><?php echo $rs->getAgencyReference(); ?><br />
              </div>
            </div>
          </div>

          <!-- Show services -->
          <?php
          $policies_str = '';
          $service_count = 0;
          foreach ($rs->getBookingServices() as $service) {
            $service_count++;
            include('shopping_cart/_shopping_cart_' . $service->getServiceType() . '.php');
          }
          ?>

          <div class="col-md-12 service well">
            <p><?php echo $rs->getPaymentDataDescription(); ?></p>
          </div>

          <div class="col-md-12 text-right visible-print text-muted small">
            Con el respaldo de BARRY TOP SERVICES S.R.L. <?php loadImage('resources/barry.png'); ?>
          </div>
        </div>
        <div id="actions" class="col-md-3 text-right">
            <a href="#">
              <span class="fa-stack fa-2x">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
              </span>
            </a>
            <a href="#" >
              <span class="fa-stack fa-2x">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-print fa-stack-1x fa-inverse"></i>
              </span>
            </a>
          </div>
      </div>
    </div>
  </div>
</div>