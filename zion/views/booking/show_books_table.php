<?php global $bookings, $agencies, $agenci_user; ?>
<div class="content">
  <div class="container">
    <div id="filter" class="col-md-12 wbox" style="padding: 15px;">
      <form id="search_books_form" class="col-md-12">
        <input type="hidden" name="controller" value="booking" />
        <input type="hidden" name="action" value="show_books_table" />
        <input type="hidden" name="all_books" value="show_all_books" />
        <div class="row">
          <div class="form-group col-md-3">
            <label>Agencia</label>
            <select class="form-control" name="agency_id" id="agency_id" onchange="loadAgencyUsers_books(value);">
              <option value="">Todas</option>
              <?php foreach ($agencies as $agency) {
                $selected = (_get('agency_id') == $agency->id ) ? 'selected' : "";
                echo '<option value="' . $agency->id . '" ' . $selected . ' >'. $agency->name . '</option>';
              } ?>
            </select>
          </div>

          <div class="form-group col-md-3">
            <label>Agente</label>
            <select class="form-control" name="user_id" id="user_id">
              <option value="">Todos</option>
              <? foreach ($agenci_user as $key => $value) {
                echo "<option value='". $value->id ."' >" . $value->name .' '. $value->lastname . "</option>";
              } ?>
            </select>
          </div>

          <div class="form-group col-md-3">
            <label for="checkin">Estado</label><br />
            <select class="form-control" name="status" id="status">
              <option value="">Todas</option>
              <option value="<? echo ServiceStatus::CONFIRMED; ?>" <? echo (_get('status') == ServiceStatus::CONFIRMED) ? 'selected' : '' ; ?> >Confirmadas</option>
              <option value="<? echo ServiceStatus::PENDING; ?>" <? echo (_get('status') == ServiceStatus::PENDING) ? 'selected' : '' ; ?> >Pendientes</option>
              <option value="<? echo ServiceStatus::CANCELLED; ?>" <? echo (_get('status') == ServiceStatus::CANCELLED) ? 'selected' : '' ; ?>>Canceladas</option>
              <option value="NO_CANCELLED">No canceladas</option>
            </select>
          </div>

          <div class="form-group col-md-3">
            <label for="checkin">Mostrar por:</label><br />
            <select class="form-control" name="status_show" id="status">
              <option value="by_service" <? echo (_get('status_show') == 'by_service') ? 'selected' : '' ; ?>>Servicio</option>
              <option value="by_booking" <? echo (_get('status_show') == 'by_booking') ? 'selected' : '' ; ?>>Reserva</option>
            </select>

          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-4">
            <label>Nro. Reserva</label>
            <input type="text" class="form-control" name="note_number" id="note_number" value="<?php echo _get('note_number'); ?>">
          </div>

          <div class="form-group col-md-3">
            <label>Fecha</label>
            <select class="form-control" name="datesFilter" id="datesFilter">
              <option value="checkin">Checkin</option>
              <option value="confirm">Confirmacion de la reserva</option>
              <option value="cancellation">Gastos de cancelacion</option>
            </select>
          </div>

          <div class="form-group col-md-2">
            <label for="checkin">Desde</label>
            <input type="text" class="form-control" id="from" name="from" placeholder="dd/mm/aa" autocomplete="off">
          </div>

          <div class="form-group col-md-2">
            <label for="checkout">Hasta</label>
            <input type="text" class="form-control" id="to" name="to" placeholder="dd/mm/aa" autocomplete="off">
          </div>

          <div class="form-group col-md-1">
            <label>&nbsp;</label><br />
            <button class="btn" id="search_books" name="search_books"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </form>
    </div>

    <div class="service_book wbox col-md-12">
        <? if(_get('status_show') == 'by_service') { ?>
              <table class="table table-striped small">
                <!-- <thead> -->
                  <tr>
                    <th>Nro.Confirmacion</th>
                    <th>Nro.</th>
                    <th>Agencia</th>
                    <th>Agente</th>
                    <th>Pasajero</th>
                    <th>Fecha Modificaci&oacute;n</th>
                    <th>Fecha Reserva</th>
                    <th>Fecha Confirmaci&oacute;n</th>
                    <th>Fecha Cancelaci&oacute;n</th>
                    <th>Procesado</th>
                    <th>Gastos desde</th>
                    <th>Estado</th>
                    <th>Servicio</th>
                    <th>Tipo Servicio</th>
                    <th>Fecha Entrada</th>
                    <th>Fecha Salida</th>
                    <th>Localizador</th>
                    <th>Precio</th>
                    <th>Nombre Proveedor</th>
                    <th>Pago proveedor</th>
                    <th>Moneda</th>
                    <th>Comision agencia</th>
                    <th>Incentivo Counter</th>
                    <th>Comision Operadora</th>
                    <th>Total</th>
                  </tr>
                <!-- </thead> -->
                  <?
                  foreach ($bookings as $b) {
                    $agency_name = $b->agency;
                    if ($b->agency_id) {
                      $agency_name = Agency::findById($b->agency_id)->name;
                    }

                    $agent_name = $b->agent;
                    if ($b->user_id) {
                      $user = User::findById($b->user_id);
                      $agent_name = $user->name . ' ' . $user->lastname;
                    }
                      $policies_str = '';
                      $services = $b->getServices();
                      $total_services = count($services);
                      $service_count = 0;
                      $total_price = 0;
                      $total_commission = 0;
                      $total_agency_fee = 0;
                      $total_incentive = 0;
                      $total_net_price = 0;
                      $total_net_price_by_provider = array();
                      // $currency = $services[0]->currency;
                      foreach ($b->getServices() as $service) {
                        $currency = $service->currency;
                        $total_price += $service->price;
                        $total_commission += $service->commission;
                        $total_incentive += $service->incentive;
                        $total_agency_fee += $service->agency_fee;
                        $total_net_price += $service->net_price;
                        if (!isset($total_net_price_by_provider[$service->provider_id])) {
                          $total_net_price_by_provider[$service->provider_id] = 0;
                        }
                        $total_net_price_by_provider[$service->provider_id] += $service->net_price;

                      }
                      $total_payable = round($total_price - $total_commission, 2);
                      $operator_commission = $total_payable - $total_net_price - $total_incentive;
                      if (count($b->getServices()) == 0) {
                      ?>
                        <tr style="background-color: white;opacity: 0.8;">
                          <td class="strong">
                            <? echo $b->getDebitNoteNumber(); ?>
                          </td>
                          <td class="strong">
                            <? echo $b->getBookingCode(); ?>
                          </td>
                          <td><?php echo $agency_name; ?></td>
                          <td><?php echo $agent_name; ?></td>
                          <td><?php echo $b->holder; ?></td>
                          <td><?php  echo dateFormatFromDB($b->book_date); ?></td>
                          <td><? echo (isset($b->confirm_date)) ? date('d/m/Y', strtotime($b->confirm_date)) : ''; ?></td>
                          <td>
                            <? $operator_agent = User::findById($b->operator_id);
                               echo $operator_agent ? $operator_agent->name . ' ' . $operator_agent->lastname : 'Sin procesar';
                            ?>
                          </td>
                          <td>
                            <?
                              $sc = $b->getServiceWithNearstCancellation();
                              if (isset($sc)) {
                                echo dateFormatFromDB($sc->cancellation_date);
                              }
                            ?>
                          </td>
                          <td>
                            <?php
                              if ($b->status == ServiceStatus::PAYED) {
                                echo 'Confirmada';
                              }
                              if ($b->status == ServiceStatus::CONFIRMED) {
                                echo 'Confirmada<br />';
                                echo ' Pago pendiente';
                              }
                              else if ($b->status == ServiceStatus::PENDING) {
                                echo ' Pendiente';
                              }
                              if ($b->status == ServiceStatus::CANCELLED) {
                                echo ' Cancelada';
                              }
                              if ($b->status == ServiceStatus::ANNULLED) {
                                echo ' Anulado';
                              }
                            ?>
                          </td>
                          <td colspan="15"></td>
                        </tr>
                      <?
                      }
                      foreach ($b->getServices() as $service) {

                      ?>
                        <tr style="background-color: white;opacity: 0.8;">
                          <td class="strong">
                            <? echo $b->getDebitNoteNumber(); ?>
                          </td>
                          <td class="strong">
                            <? echo $b->getBookingCode(); ?>
                          </td>
                          <td><?php echo $agency_name; ?></td>
                          <td><?php echo $agent_name; ?></td>
                          <td><?php echo $b->holder; ?></td>
                          <td><? $logs = Log::findByQuery("SELECT * FROM Log WHERE action LIKE 'BOOK UPDATE $b->booking_code' ORDER BY date DESC LIMIT 1");
                                echo isset($logs[0]->date) ? date('d/m/Y',strtotime($logs[0]->date)) : '';
                              ?>
                          </td>
                          <td><?php  echo dateFormatFromDB($b->book_date); ?></td>
                          <td><? echo (isset($b->confirm_date)) ? date('d/m/Y', strtotime($b->confirm_date)) : ''; ?></td>
                          <td><? echo (isset($b->cancel_date)) ? date('d/m/Y', strtotime($b->cancel_date)) : ''; ?></td>
                          <td>
                            <? $operator_agent = User::findById($b->operator_id);
                               echo $operator_agent ? $operator_agent->name . ' ' . $operator_agent->lastname : 'Sin procesar';
                            ?>
                          </td>
                          <td>
                            <?
                              $sc = $b->getServiceWithNearstCancellation();
                              if (isset($sc)) {
                                echo dateFormatFromDB($sc->cancellation_date);
                              }
                            ?>
                          </td>
                          <td>
                            <?php
                              if ($b->status == ServiceStatus::PAYED) {
                                echo 'Confirmada';
                              }
                              if ($b->status == ServiceStatus::CONFIRMED) {
                                echo 'Confirmada<br />';
                                echo ' Pago pendiente';
                              }
                              else if ($b->status == ServiceStatus::PENDING) {
                                echo ' Pendiente';
                              }
                              if ($b->status == ServiceStatus::CANCELLED) {
                                echo ' Cancelada';
                              }
                              if ($b->status == ServiceStatus::ANNULLED) {
                                echo ' Anulado';
                              }
                            ?>
                          </td>
                          <td><? echo $service->name; ?></td>
                          <td><? echo ServiceType::getName($service->type); ?></td>
                          <td><? echo dateFormatFromDB($service->checkin); ?></td>
                          <td><? echo dateFormatFromDB($service->checkout); ?></td>
                          <td><? echo $service->localizer; ?></td>
                          <td><? echo number_format($service->price, 2); ?></td>
                          <td>
                            <?
                              $provider = Provider::findById($service->provider_id);
                              echo isset($provider->name) ? $provider->name : '';
                            ?>
                          </td>
                          <td><? echo number_format($total_net_price_by_provider[$service->provider_id], 2);//Pago Proveedor ?></td>
                          <td><? echo $service->currency; ?></td>
                          <td><? echo number_format($total_commission, 2);//Comision Agencia ?></td>
                          <td><? echo number_format($total_incentive, 2);//Incentivo Counter?></td>
                          <td><? echo number_format($operator_commission, 2);//Comision Operadora?></td>
                          <td><? echo number_format($total_price + $total_agency_fee, 2); ?></td>
                        </tr>
                      <?
                      }
                  }
                  ?>
              </table>
        <? } else { ?>
              <table class="table table-striped small">
                  <tr style="background-color: white;opacity: 0.8;">
                    <th>Nro.Confirmacion</th>
                    <th>Nro.</th>
                    <th>Agencia</th>
                    <th>Agente</th>
                    <th>Pasajero</th>
                    <th>Fecha Reserva</th>
                    <th>Fecha Confirmaci&oacute;n</th>
                    <th>Fecha Cancelaci&oacute;n</th>
                    <th>Fecha de Entrada</th>
                    <th>Procesado</th>
                    <th>Gastos desde</th>
                    <th>Estado</th>
                    <th>Moneda</th>
                    <th>Precio Total</th>
                    <th>Comision agencia</th>
                    <th>Precio Barry</th>
                    <th>Incentivo Counter</th>
                    <th>Comision Operadora</th>
                    <th>Precio Neto</th>
                    <!-- <th>Total</th> -->
                  </tr>
                  <? foreach ($bookings as $b) { ?>
                      <? $agency_name = $b->agency;
                          if ($b->agency_id) {
                            $agency_name = Agency::findById($b->agency_id)->name;
                          }

                          $agent_name = $b->agent;
                          if ($b->user_id) {
                            $user = User::findById($b->user_id);
                            $agent_name = $user->name . ' ' . $user->lastname;
                          }
                          $moneda = array();
                          $policies_str = '';
                          $services = $b->getServices();
                          $total_services = count($services);
                          $service_count = 0;
                          $total_price = 0;
                          $total_commission = 0;
                          $total_agency_fee = 0;
                          $total_incentive = 0;
                          $total_net_price = 0;
                          $total_net_price_by_provider = array();
                          $date_checkin = '';
                          foreach ($b->getServices() as $service) {
                            $currency = $service->currency;
                            $total_price += $service->price;
                            $total_commission += $service->commission;
                            $total_incentive += $service->incentive;
                            $total_agency_fee += $service->agency_fee;
                            $total_net_price += $service->net_price;
                            if (!array_key_exists($service->currency, $moneda)) {
                              $moneda[$service->currency] = $service->currency;
                            }
                            if (!isset($total_net_price_by_provider[$service->provider_id])) {
                              $total_net_price_by_provider[$service->provider_id] = 0;
                            }
                            $total_net_price_by_provider[$service->provider_id] += $service->net_price;

                            if($service->checkin == '0000-00-00' || $service->checkin == '') continue;
                            if($date_checkin == '') {
                              $date_checkin = $service->checkin;
                            }
                            else {
                              $result = date_diff(new DateTime(($date_checkin)), new DateTime($service->checkin)); // date_diff(fecha_comparada, fecha menor encontrada);
                              $diferencia = (int) $result->format('%R%a');
                              if($diferencia < 0) {
                                $date_checkin = $service->checkin;
                              }
                            }
                          }
                          $total_payable = round($total_price - $total_commission, 2);
                          $operator_commission = $total_payable - $total_net_price - $total_incentive;
                        ?>
                        <tr style="background-color: white;opacity: 0.8;">
                          <td><a href="./?controller=booking&action=show&id=<? echo $b->id; ?>" style="color:black;" target="_blank"><? echo $b->getDebitNoteNumber(); ?></a></td>
                          <td><? echo $b->getBookingCode(); ?></td>
                          <td><?php echo $agency_name; ?></td>
                          <td><?php echo $agent_name; ?></td>
                          <td><?php echo $b->holder; ?></td>
                          <td><?php  echo dateFormatFromDB($b->book_date); ?></td>
                          <td><? echo (isset($b->confirm_date)) ? date('d/m/Y', strtotime($b->confirm_date)) : ''; ?></td>
                          <td><? echo (isset($b->cancel_date) && $b->cancel_date != '0000-00-00 00:00:00') ? date('d/m/Y', strtotime($b->cancel_date)) : ''; ?></td>
                          <td><? echo dateFormatFromDB($date_checkin); ?></td>
                          <td>
                            <? $operator_agent = User::findById($b->operator_id);
                               echo $operator_agent ? $operator_agent->name . ' ' . $operator_agent->lastname : 'Sin procesar';
                            ?>
                          </td>
                          <td>
                            <?
                              $sc = $b->getServiceWithNearstCancellation();
                              if (isset($sc)) {
                                echo dateFormatFromDB($sc->cancellation_date);
                              }
                            ?>
                          </td>
                          <td>
                            <?php
                              if ($b->status == ServiceStatus::PAYED) {
                                echo 'Confirmada';
                              }
                              if ($b->status == ServiceStatus::CONFIRMED) {
                                echo 'Confirmada<br />';
                                echo ' Pago pendiente';
                              }
                              else if ($b->status == ServiceStatus::PENDING) {
                                echo ' Pendiente';
                              }
                              if ($b->status == ServiceStatus::CANCELLED) {
                                echo ' Cancelada';
                              }
                              if ($b->status == ServiceStatus::ANNULLED) {
                                echo ' Anulado';
                              }
                            ?>
                          </td>
                          <td><? echo implode(",", $moneda); ?></td>
                          <td><? echo number_format($total_price + $total_agency_fee, 2);//Precio Total ?></td>
                          <td><? echo number_format($total_commission, 2);//Comission Agencia?></td>
                          <td><? echo number_format($total_payable, 2);//Precio Barry(Total a Cobrar)?></td>
                          <td><? echo number_format($total_incentive, 2);//Incentivo Counter ?></td>
                          <td><? echo number_format($operator_commission, 2);//Comision Operadora?></td>
                          <td><? echo number_format($total_net_price, 2);//Precio Neto ?></td>
                        </tr>
                  <? } ?>
                </table>
        <? } ?>
    </div>
  </div>
</div>

<script text="javascript">
  $('#from').datepicker({
    numberOfMonths: 2,
    dateFormat: "dd/mm/yy",
    onClose: function(selectedDate) {
      // $("#to").datepicker("option", "minDate", selectedDate);
      // $("#to").datepicker('show');
    }
  });

  $('#to').datepicker({
    numberOfMonths: 2,
    dateFormat: "dd/mm/yy",
    onClose: function(selectedDate) {

    }
  });
  date = new Date();
  primer = new Date(date.getFullYear(), date.getMonth(), 1);
  ultimoDia = new Date(date.getFullYear(), date.getMonth() + 1, 0);
  <? if (!isset($_GET['from'])) { ?>
      $( "#from" ).datepicker( "setDate", primer);
      $( "#to" ).datepicker( "setDate", ultimoDia);
  <? }else { ?>
      $('#from').val('<?php echo _get('from'); ?>');
      $('#to').val('<?php echo _get('to'); ?>');
  <? } ?>
    $('#user_id').val('<? echo _get('user_id'); ?>');
</script>
