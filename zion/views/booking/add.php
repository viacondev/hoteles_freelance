<?php global $agencies;$select="add"; ?>
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-3"><?php include('views/admin/_menu.php'); ?></div>

      <div class="col-md-9 wbox" style="width: 73.7%;">
        <h2>Agregar nota de confirmaci&oacute;n manual</h2>
        <?php include('_form.php'); ?>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(function() {
    $('#estado').append('<?php echo Formar_Stados(1); ?>');
    $('#estado_id').val('1');
    $('#manual_notes').addClass('active');
    <?php
      if ($select == "add") {
        ?>
          $('#file').show();
          $('#download, #ver_voucher').hide();
        <?php
      }
    ?>
  });
</script>
