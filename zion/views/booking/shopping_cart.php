<?php 
global $rs;
?>

<div id="pax-info-modal" class="modal fade">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Datos Pasajeros</h4>
      </div>
      <form method="post" action="?controller=booking&amp;action=confirm_purchase">
        <div class="modal-body">
          <div id="titular_reserva">
            <input type="radio" name="include_all_pax" checked onclick="$('.secondary_pax').hide()" /> Incluir solo los datos del pasajero principal<br />
            <input type="radio" name="include_all_pax" onclick="$('.secondary_pax').show()" /> Incluir los datos de todos los pasajeros
            <hr />
            <?php
            $service_count = 0;
            if (isset($rs)) {
              foreach ($rs->getBookingServices() as $service) {
                $service_count++;

                include('pax_info/_pax_info_' . $service->getServiceType() . '.php');
                echo '<input type="hidden" name="serviceType_' . $service_count . '" value="' . $service->getServiceType() . '" />';
              }
              ?>
              <input type="hidden" name="agencyReference" value="<?php echo $GLOBALS['current_user']->name . '/' . $_SESSION['agency_name']; ?> " />
              <input type="hidden" name="purchaseToken" value="<?php echo $rs->getPurchaseToken(); ?>" />
              <input type="hidden" name="servicesCount" value="<?php echo $service_count; ?>" />
              <input type="hidden" name="requestCode" value="<?php echo mt_rand(); ?>" />
              <?php
            }
            ?>
          </div>
          <div>
            <input type="checkbox" id="agreement_check" /> He le&iacute;do y acepto las
            <a href="assets/pdf/terms.pdf" target="_blank">condiciones generales</a>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
          <button class="btn btn-info" onclick="return checkAgreement(event);">Confirmar Reserva</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div id="shopping_cart_services" class="content">
  <div class="container">
    <div class="col-md-12 box">
      <div id="shopping_cart_title" class="col-md-12 text-info wbox">
        <i class="fa fa-shopping-cart fa-1x"></i> Carrito de Compras
      </div>
      <div class="row">
        <div class="col-md-8">
          <!-- Show services -->
          <?php
          if (isset($rs)) {
            $total_services = count($rs->getBookingServices());
            $service_count = 0;
            foreach ($rs->getBookingServices() as $service) {
              $service_count++;

              include('shopping_cart/_shopping_cart_' . $service->getServiceType() . '.php');
            }
            ?>

            <div id="reservation_actions" class="col-md-12 text-right wbox">
              <div id="purchase_total_amount">
                Valoraci&oacute;n total de la reserva:
                <span class="amount"><?php echo Agency::getUserPrice($rs->getTotalPrice()) . ' ' . $rs->getCurrency(); ?></span>
              </div>
              <div>
                <div class="link" onclick="emptyCart('<?php echo $rs->getPurchaseToken(); ?>')">Vaciar carrito</div>
                <button class="btn btn-info" data-toggle="modal" data-target="#pax-info-modal">
                  Continuar con la reserva
                </button>
              </div>
            </div>
            <?php
          }
          else {
            echo '<h3>Carrito Vac&iacute;o</h3>';
          }
          ?>
        </div>

        <div id="services_suggestions" class="col-md-4">
          <div class="title">A&ntilde;adir nuevos servicios</div>
            <div class="col-md-7 add_new_service"><a href="?controller=accommodation&amp;action=home"><i class="fa fa-home"></i> A&ntilde;adir Alojamiento</a></div>
            <div class="col-md-7 add_new_service"><a href="?controller=ticket&amp;action=home"><i class="fa fa-ticket"></i> A&ntilde;adir Excursiones</a></div>
            <div class="col-md-7 add_new_service"><a href="?controller=transfer&amp;action=home"><i class="fa fa-suitcase"></i> A&ntilde;adir Traslados</a></div>
        </div>
      </div>
    </div>
  </div>
</div>

<form id="service_remove_form" method="post" action="?controller=booking&amp;action=service_remove" class="hidden invisible">
  <input type="hidden" id="remove_purchaseToken" name="purchaseToken">
  <input type="hidden" id="remove_SPUI" name="SPUI">
  <input type="hidden" name="requestCode" value="<?php echo mt_rand(); ?>" />
</form>

<form id="empty_cart_form" method="post" action="?controller=booking&amp;action=empty_cart" class="hidden invisible">
  <input type="hidden" id="empty_cart_purchaseToken" name="purchaseToken">
</form>