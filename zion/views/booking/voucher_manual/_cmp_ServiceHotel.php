<div class="voucher-content">

  <table style="width: 100%;">
    <tr>
      <?php if($service->provider_id == Provider::DOMITUR){ ?>
        <td style="width: 35%">
          <?php loadImage('resources/domitur.jpg'); ?>
        </td>
      <?php } ?>
      <td style="width: 35%;">
        <img src="<?php echo $agency->logo; ?>" onerror="this.onerror=null;this.src='https://yocounter.com/hoteles_freelance/zion/assets/images/agency_no_logo.png';" />
      </td>
      <td style="width: 40%; padding-top: 10px; padding-left: 50px; font-size: 12px">
        <strong><?php echo $agency->name; ?></strong><br />
        <?php echo $agency->address; ?><br />
        <?php echo $agency->phone; ?><br />
        <?php echo $agency->mail; ?>
      </td>
      <td style="width: 30%;">
        <div class="text-right hidden-print avoid-mailing">
          <div class="link" onclick="showSendMail('Voucher <?php echo htmlspecialchars($service->name); ?>', 'voucher_<?php echo $service_count; ?>_content');">
            <span class="fa-stack fa-2x">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
            </span>
          </div>
          <div class="link" onclick="printDiv('voucher_<?php echo $service_count; ?>_content')">
            <span class="fa-stack fa-2x">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-print fa-stack-1x fa-inverse"></i>
            </span>
          </div>
        </div>
      </td>
    </tr>
  </table>

  <div style="text-align: center; margin-top: 10px; padding: 15px; background-color: #fff; border-radius: 5px; border: 1px solid #999; ">
    <strong>Bono - Alojamiento</strong> / Voucher - Accommodation<br />
    <strong>Reserva Confirmada y Garantizada - Hotel</strong> / Booking confirmed and guaranteed - Hotel
  </div>

  <div style="margin-top: 10px; padding: 15px; background-color: #fff; border-radius: 5px; border: 1px solid #999; ">
    <table style="width: 100%;">
      <tr>
        <td style="border-right: 1px solid #999; width: 40%;">
          <strong>Localizador</strong> / Reference number :
          <div style="color: #036d9f; font-size: 35px;">
            <?php echo $service->localizer; ?>
          </div>
          <div style="font-size:0.85em; color: #999;"><strong>V&aacute;lido para el hotel</strong> / Valid for the hotel</div>
        </td>
        <td style="width: 60%; padding-left: 15px;">
          <span style="color: #3399f3; font-size: 18px;"><?php echo $service->name; ?></span> &nbsp;
          <span>
            &nbsp; <?php showCategory($service->category); ?>
          </span>&nbsp;
          <span> <?php echo "( $service->destination_code )"; ?></span><br />
          <table border="0">
            <tr>
              <td style="vertical-align: top;"><strong>Nombre del Pasajero</strong> / Pax name :&nbsp;</td>
              <td>
                <?php
                  echo $global_holder_name;
                ?>
              </td>
            </tr>
          </table>
          <strong>Fecha confirmaci&oacute;n reserva</strong> / Booking date : <?php echo dateFormatFromDB($book->confirm_date); ?><br />
          <strong>Ref. Agencia </strong> / Agency ref. :&nbsp;<?php echo $booking_user_name . " / " . $agency->name; ?><br />
        </td>
      </tr>
    </table>
  </div>

  <div style="margin-top: 10px; padding: 15px; background-color: #fff; border-radius: 5px; border: 1px solid #999; ">
    <div>
      <strong>Desde</strong> / From : <?php echo dateFormatFromDB($service->checkin); ?> -
      <strong>Hasta</strong> / To : <?php echo dateFormatFromDB($service->checkout); ?>
    </div>
  </div>
  <div style="margin-top: 10px; padding: 15px; background-color: #fff; border-radius: 5px; border: 1px solid #999; ">
    <table style="width: 100%;">
      <thead>
        <tr>
          <td><strong>Tipo de habitaci&oacute;n</strong><br />Room type</td>
          <td><strong>R&eacute;gimen</strong><br />Board</td>
          <td><strong>Adultos</strong><br />Adults</td>
          <td><strong>Ni&ntilde;os</strong><br />Children</td>
          <td><strong>Edad Ni&ntilde;os</strong><br />Children ages</td>
        </tr>
      </thead>
      <tbody>
        <?php
          $rooms=$service->rooms;
          foreach ($rooms as $room) {
        ?>
            <tr style="border-top: 1px solid #999">
              <td style="padding: 10px 0;"><?php echo $room->type; ?></td>
              <td style="padding: 10px 0;"><?php echo $room->board; ?></td>
              <td style="padding: 10px 0;">
                <?php
                  $adult_str = $room->adult_count > 1 ? ' adultos' : ' adulto';
                  echo $room->adult_count . $adult_str;
                ?>
              </td>
              <td style="padding: 10px 0;">
              <?php
                if ($room->child_count > 0) {
                  $child_str = $room->child_count > 1 ? ' ni&ntilde;os' : ' ni&ntilde;o';
                  echo $room->child_count . $child_str;
                }
                else {
                  echo "-";
                }
              ?>
              </td>
              <td style="padding: 10px 0 ;">
                <?php
                  if ($room->childs_ages != 0) {
                    echo $room->childs_ages . ' a&ntilde;os';
                  }
                ?>
              </td>
            </tr>
        <?php
          }
        ?>
      </tbody>
    </table><br />
    <?php if ($service->includes != '') {
          echo  '<div>'.
                '<strong>Observaciones </strong> / Remarks :'.
                '<pre style="background-color:white;border:none;font-family:inherit;padding: 0">' .
                $service->includes .
                '</pre></div>';
        }
    ?>
    <div>
      <big><strong>Observaciones</strong> / Remarks</big>
      <div>
        <strong>Tomar en cuenta que el hotel puede solicitar un depósito en efectivo o tarjeta de crédito como garantía a la hora del check in, de acuerdo a sus políticas.</strong>
      </div>
    </div>
  </div>

  <?php if (!empty($service->provider_id)) { ?>
    <div style="margin-top: 10px;">
      <strong>Reservado y pagadero por</strong> / Bookable and payable by:
      <?php echo Provider::findById($service->provider_id)->name; ?>
    </div>
  <?php } ?>
  <div style="margin-top: 10px">
      <strong>NUMERO DE EMERGENCIAS</strong>
      591 72122266 BARRY SANTA CRUZ.
    </div>
  <div style="color: #777; padding-top: 5px;">
    <i>Con el respaldo de <strong>Barry Top Services S.R.L.</strong></i>
  </div>
</div>
