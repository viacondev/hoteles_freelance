<?php
$transfer = ServiceTransfer::find(array('service_id' => $service->id));
$transferType_str = $transfer->type == 'OUT' ? 'Salida' : 'Llegada';
?>
<div class="voucher-content">
  <table style="width: 100%;">
    <tr>
      <?php if($service->provider_id == 140){ ?>
        <td style="width: 35%">
          <?php loadImage('resources/boldon.jpg'); ?>
        </td>
      <?php } ?>
      <td style="width: 35%;">
        <img src="<?php echo $agency->logo; ?>" />
      </td>
      <td style="width: 40%; padding-top: 10px; padding-left: 50px; font-size: 12px">
        <strong><?php echo $agency->name; ?></strong><br />
                <?php echo $agency->address; ?><br />
                <?php echo $agency->phone; ?><br />
                <?php echo $agency->mail; ?>
      </td>
      <td style="width: 30%;">
        <div class="text-right hidden-print avoid-mailing">
          <div class="link" onclick="showSendMail('Voucher <?php echo htmlspecialchars($service->name); ?>', 'voucher_<?php echo $service_count; ?>_content');">
            <span class="fa-stack fa-2x">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
            </span>
          </div>
          <div class="link" onclick="printDiv('voucher_<?php echo $service_count; ?>_content')">
            <span class="fa-stack fa-2x">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-print fa-stack-1x fa-inverse"></i>
            </span>
          </div>
        </div>
      </td>
    </tr>
  </table>

  <div style="text-align: center; margin-top: 10px; padding: 15px; background-color: #fff; border-radius: 5px; border: 1px solid #999; ">
    <strong>Bono - Traslado</strong> / Voucher - Transfer<br />
    <strong>Reserva Confirmada y Garantizada - Traslado</strong> / Booking confirmed and guaranteed - Transfer
  </div>

  <div style="margin-top: 10px; padding: 15px; background-color: #fff; border-radius: 5px; border: 1px solid #999; ">
    <table style="width: 100%;">
      <tr>
        <td style="border-right: 1px solid #999; width: 40%;">
          <strong>Localizador</strong> / Reference number :
          <div style="color: #036d9f; font-size: 35px;">
            <?php echo $service->localizer; ?>
          </div>
        </td>
        <td style="width: 60%; padding-left: 15px;">
          <span style="color: #3399f3; font-size: 18px;"><?php echo $service->name; ?></span> &nbsp;
          &nbsp;
          <span> <?php echo "( $service->destination_code )"; ?></span><br />
          <table border="0">
            <tr>
              <td style="vertical-align: top;"><strong>Nombre del Pasajero</strong> / Pax name :&nbsp;</td>
              <td>
                <?php
                    echo $global_holder_name
                ?>
              </td>
            </tr>
          </table>
          <strong>Fecha confirmaci&oacute;n reserva</strong> / Booking date : <?php echo dateFormatFromDB($book->confirm_date); ?><br />
          <strong>Ref. Agencia </strong> / Agency ref. :&nbsp;<?php echo $booking_user_name . " / " . $agency->name; ?><br />
        </td>
      </tr>
    </table>
  </div>

  <div style="margin-top: 10px; padding: 15px; background-color: #fff; border-radius: 5px; border: 1px solid #999; ">
    <div>
      <strong>Datos del Traslado</strong> / Transfer info: <br />
    </div><br />
    <div>
      <strong>Desde</strong> / from: <?php echo $transfer->origin ?></strong> &#8594;
      <strong>Hasta</strong> / to: <?php echo $transfer->destination; ?>
    </div>
    <div>
      <strong>Fecha de llegada</strong> / Service date: <?php echo dateFormatFromDB($service->checkin); ?>,
      <strong>Nro. Vuelo</strong> / Service date: <?php echo $transfer->flight_in; ?> ,
      <strong>Hora de <?php echo $transferType_str; ?></strong> / Pick-up time: <?php echo $transfer->time_in; ?>
    </div><br/>

     <?php if ($transfer->type == 'IN_OUT') { ?>
          <div>
            <strong>Desde</strong> / from: <?php echo $transfer->destination; ?></strong> &#8594;
            <strong>Hasta</strong> / to: <?php echo $transfer->origin; ?>
          </div>
          <div>
            <strong>Fecha de Salida</strong> / Service date: <?php echo dateFormatFromDB($service->checkout); ?>,
            <strong>Nro. Vuelo</strong> / Service date: <?php echo $transfer->flight_out; ?>,
            <strong>Hora de Salida</strong> / Pick-up time: <?php echo $transfer->time_out ?>
          </div><br />
    <?php  } ?>
    <?php if ($service->includes != '') {
          echo  '<div>'.
                '<strong>Observaciones </strong> / Remarks :'.
                '<pre style="background-color:white;border:none;font-family:inherit;padding: 0">' .
                $service->includes .
                '</pre></div>';
        }
    ?>
  </div>

  <?php if (!empty($service->provider_id)) { ?>
    <div style="margin-top: 10px;">
      <strong>Reservado y pagadero por</strong> / Bookable and payable by:
      <?php echo Provider::findById($service->provider_id)->name; ?>
    </div>
  <?php } ?>
  <div style="margin-top: 10px">
    <strong>NUMERO DE EMERGENCIAS</strong>
    591 72122266 BARRY SANTA CRUZ.
  </div>
  <div style="color: #777; padding-top: 5px;">
    <i>Con el respaldo de <strong>Barry Top Services S.R.L.</strong></i>
  </div>
</div>
