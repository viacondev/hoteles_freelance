<?php global $bookings, $agency_users, $agencies;
    $manager = array();
    if (isset($_SESSION['current_agency'])) {
      $manager = AgencyManager::find(array('agency_id' => $_SESSION['current_agency']->id)); 
    }
?>
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <div id="filter" class="col-md-12 wbox" style="padding: 15px;">
          <form id="search_books_form" class="col-md-12">
            <input type="hidden" name="controller" value="booking" />
            <input type="hidden" name="action" value="agency_books" />
            <div class="row">
            <?php if (is_array($manager) && count($manager) > 0) { ?>
              <div class="form-group col-md-3">
                <label>Agencia</label>
                <select class="form-control" name="agency_id" id="agency_id" onchange="loadAgencyUsers_books(value);">
                  <option value="">Todas</option>
                  <?php foreach ($agencies as $agency) {
                    echo '<option value="' . $agency->id . '">'. $agency->name . '</option>';
                  } ?>
                </select>
              </div>

              <div class="form-group col-md-3">
                <label>Agente</label>
                <select class="form-control" name="user_id" id="user_id">
                  <option value="">Todos</option>
                  <?php foreach ($agency_users as $key => $value) {
                    echo "<option value='". $value->id ."' >" . $value->name .' '. $value->lastname . "</option>";
                  } ?>
                </select>
              </div>
           <?php } ?>
              <div class="form-group <?php echo is_array($manager) && count($manager) > 0 ? 'col-md-3' : 'col-md-4'; ?>">
                <label for="checkin">Estado</label><br />
                <select class="form-control" name="status" id="status">
                  <option value="">Todas</option>
                  <option value="<?php echo ServiceStatus::CONFIRMED; ?>">Confirmadas</option>
                  <option value="<?php echo ServiceStatus::PENDING; ?>">Pendientes</option>
                  <option value="<?php echo ServiceStatus::CANCELLED; ?>">Canceladas</option>
                  <option value="NO_CANCELLED">No canceladas</option>
                </select>
              </div>
          <?php if (is_array($manager) && count($manager) == 0) { ?>   
              <div class="form-group col-md-3">
                <label>Agente</label>
                <select class="form-control" name="user_id" id="user_id">
                  <option value="">Todos</option>
                  <?php foreach ($agency_users as $key => $value) {
                    echo "<option value='". $value->id ."' >" . $value->name .' '. $value->lastname . "</option>";
                  } ?>
                </select>
              </div>
          <?php } ?>
              <div class="form-group <?php echo is_array($manager) && count($manager) > 0 ? 'col-md-3' : 'col-md-5'; ?>">
                <label>Destino</label>
                <input type="text" class="form-control" id="destination" name="destination" placeholder="Destino..." value="<?php echo _get('destination'); ?>">
                <input type="hidden" id="destination_code" name="destination_code" value="<?php echo _get('destination_code'); ?>">
              </div>
            </div>

            <div class="row">
              <div class="form-group col-md-4">
                <label>Nro. Reserva</label>
                <input type="text" class="form-control" name="note_number" id="note_number" value="<?php echo _get('note_number'); ?>">
              </div>

              <div class="form-group col-md-3">
                <label>Fecha</label>
                <select class="form-control" name="datesFilter" id="datesFilter">
                  <option value="checkin">Checkin</option>
                  <option value="confirm">Confirmacion de la reserva</option>
                  <option value="cancellation">Gastos de cancelacion</option>
                </select>
              </div>

              <div class="form-group col-md-2">
                <label for="checkin">Desde</label>
                <input type="text" class="form-control" id="checkin" name="checkin" placeholder="dd/mm/aa" autocomplete="off">
              </div>

              <div class="form-group col-md-2">
                <label for="checkout">Hasta</label>
                <input type="text" class="form-control" id="checkout" name="checkout" placeholder="dd/mm/aa" autocomplete="off">
              </div>

              <div class="form-group col-md-1">
                <label>&nbsp;</label><br />
                <button class="btn" id="search_books" name="search_books"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </form>
        </div>

        <?php include('_show_books.php'); ?>
      </div>

      <div class="col-md-3"><?php include('views/admin/_menu.php'); ?></div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(function() {
    <?php if (!isset($_GET['agency_books'])) { ?>
      $('#booking_agency_books').addClass('active');
    <?php } ?>
    $('#agency_id').val('<?php echo _get('agency_id'); ?>');
    $('#user_id').val('<?php echo _get('user_id'); ?>');
    $('#status').val('<?php echo _get('status'); ?>');
    $('#datesFilter').val('<?php echo _get('datesFilter'); ?>');
    $('#checkin').val('<?php echo _get('checkin'); ?>');
    $('#checkout').val('<?php echo _get('checkout'); ?>');
  });
</script>
