<?php if (!isset($_GET['search_books'])) {
  $today = new DateTime();
  $tomorrow = date_add(new DateTime(), new DateInterval('P1D'));
  $days_ago_7 = date_sub(new DateTime(), new DateInterval('P7D'));
  $days_ago_30 = date_sub(new DateTime(), new DateInterval('P30D'));
  $days_ahead_7 = date_add(new DateTime(), new DateInterval('P7D'));

  $today = $today->format('d/m/Y');
  $tomorrow = $tomorrow->format('d/m/Y');
  $days_ago_7 = $days_ago_7->format('d/m/Y');
  $days_ago_30 = $days_ago_30->format('d/m/Y');
  $days_ahead_7 = $days_ahead_7->format('d/m/Y');
  ?>
  <div class="service_book wbox col-md-12">
    <div class="col-md-12">
      <h3>Accesos directos</h3>
      <div class="col-md-6">
        <div class="search_book_shortcut" onclick="filterBooks('', 'confirm', '<?php echo $days_ago_7; ?>', '<?php echo $tomorrow; ?>')">
          <strong>Reservas realizadas</strong> (&uacute;ltimos 7 d&iacute;as)
        </div>
      </div>
      <div class="col-md-6">
        <div class="search_book_shortcut" onclick="filterBooks('', 'confirm', '<?php echo $days_ago_30; ?>', '<?php echo $tomorrow; ?>')">
          <strong>Reservas realizadas</strong> (&uacute;ltimos 30 d&iacute;as)
        </div>
      </div>
      <div class="col-md-6">
        <div class="search_book_shortcut" onclick="filterBooks('NO_CANCELLED', 'cancellation', '<?php echo $today; ?>', '<?php echo $days_ahead_7; ?>')">
          <strong>Reservas pr&oacute;ximas a gastos</strong> (pr&oacute;ximos 7 d&iacute;as)
        </div>
      </div>
      <div class="col-md-6">
        <div class="search_book_shortcut" onclick="filterBooks('NO_CANCELLED', 'checkin', '<?php echo $today; ?>', '<?php echo $days_ahead_7; ?>')">
          <strong>Fecha de check-in</strong> (pr&oacute;ximos 7 d&iacute;as)
        </div>
      </div>
    </div>
  </div>
<?php } ?>

<?php

$expired = "";
$cancell = false;
foreach ($bookings as $b) {
  // $b = stdToObject($b, 'Booking');
  // __logarr($b);
  $agency_name = $b->agency;
  if ($b->agency_id) {
    $agency_name = Agency::findById($b->agency_id)->name;
  }

  $agent_name = $b->agent;
  if ($b->user_id) {
    $user = User::findById($b->user_id);
    $agent_name = $user->name . ' ' . $user->lastname;
  }
  $global_holder_name = $b->holder;
  if (!empty($b->name))
    $global_holder_name = $b->name . ' ' . $b->last_name;
  ?>
  <div class="service_book wbox col-md-12">
    <table class="table" style="background-color: #f9f9f9; margin-bottom: 0;">
      <thead>
        <th>Nro.</th>
        <th>Agencia</th>
        <th>Agente</th>
        <th>Pasajero</th>
        <th>Gastos desde</th>
        <th>Estado</th>
      </thead>
      <tbody>
        <?php $user = User::findById($b->user_id); ?>
        <td class="strong">
          <?php echo $b->debit_note; ?>
          <span class="small" style="font-weight: normal;"> (<?php echo $b->booking_code; ?>)</span>
        </td>
        <td><?php echo $agency_name; ?></td>
        <td><?php echo $agent_name; ?></td>
        <td><?php echo $global_holder_name; ?></td>
        <td class="strong text-danger">
          <?php
            $sc = $b->cancellation_date;
            if (isset($sc)) {

              echo dateFormatFromDB($sc->cancellation_date);
              $interval = date_diff(new DateTime($sc->cancellation_date), new DateTime('now'));
              $daydiff = (int) $interval->format('%R%a');
              if ($daydiff > 1) {
                $expired = "si";
              }
              else {
                $expired = "no";
              }
            }
          ?>
        </td>
        <td>
          <?php
          if ($b->status == ServiceStatus::PAYED) {
            echo '<span class="label label-success"><i class="fa fa-check-circle"></i> Confirmada</span>';
          }
          if ($b->status == ServiceStatus::CONFIRMED) {
            echo '<span class="label label-success"><i class="fa fa-check-circle"></i> Confirmada</span><br />';
            //echo '<span class="label label-warning"><i class="fa fa-exclamation-circle"></i> Pago pendiente</span>';
          }
          else if ($b->status == ServiceStatus::PENDING) {
            echo '<span class="label label-default"><i class="fa fa-exclamation-circle"></i> Pendiente</span>';
          }
          if ($b->status == ServiceStatus::CANCELLED) {
            echo '<span class="label label-danger"><i class="fa fa-times-circle"></i> Cancelada</span>';
          }
          ?>
        </td>
      </tbody>
    </table>

    <div class="col-md-12">
      <table class="table">
        <thead>
          <tr>
            <th>Servicio</th>
            <th>Entrada</th>
            <th>Salida</th>
            <th class="text-right">Precio</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $currency         = '';
          $total            = 0;
          $localizers       = array();
          $juniperProvider  = array(159);
          
          foreach ($b->services as $service) {

            if($service->provider_id == BookingProvider::HOTELBEDS
                || $service->provider_id == BookingProvider::DOMITUR
                || $service->provider_id == BookingProvider::METHABOOK
                || $service->provider_id == BookingProvider::METHABOOK2
                || $service->provider_id == BookingProvider::TOURICO 
                || $service->provider_id == BookingProvider::RESTEL
                || $service->provider_id == BookingProvider::TAILORBEDS
                || in_array($service->provider_id, $juniperProvider)) {
              if (!$service->is_manual)
                $cancell = true;
            }
            $service_type = '';
            if ($service->type == ServiceType::HOTEL) {
              $service_type = '<span style="color:#0e86ef"><i class="fa fa-home"></i> </span>';
            }
            else if ($service->type == ServiceType::TICKET) {
              $service_type = '<span style="color:#0e86ef"><i class="fa fa-ticket"></i> </span>';
            }
            else if ($service->type == ServiceType::TRANSFER) {
              $service_type = '<span style="color:#0e86ef"><i class="fa fa-suitcase"></i> </span>';
            }
            else if ($service->type == ServiceType::FLIGHT) {
              $service_type = '<span style="color:#0e86ef"><i class="fa fa-plane"></i> </span>';
            }
            else if ($service->type == ServiceType::OTHER) {
              $service_type = '<span style="color:#0e86ef"><i class="fa fa-book"></i> </span>';
            }
            $fees     = $service->fee_monto;
            echo '<tr>';
            echo '<td>' . $service_type . $service->name . Provider::showNameIfAdmin($service->provider_id, true) . '</td>';
            echo '<td>' . dateFormatFromDB($service->checkin) . '</td>';
            echo '<td>' . dateFormatFromDB($service->checkout) . '</td>';
            echo '<td class="text-right">' . number_format($service->price + $fees, 2) . ' ' . $service->currency . '</td>';
            echo '</tr>';

            
            $total    += $service->price + $fees;
            $currency = $service->currency;
            $localizers[$service->localizer] = $service->localizer;
          }          
          ?>
          <tr>
            <td colspan="2" class="strong">
              Localizador: <?php echo implode(', ', $localizers); ?>
            </td>
            <td colspan="2" class="text-right strong">
              Total: &nbsp; <?php echo number_format($total, 2) . " $currency"; ?>
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <div class="booking-actions col-md-4">
      <?php if ($b->status == ServiceStatus::CONFIRMED && $b->status != ServiceStatus::PAYED && $b->status != ServiceStatus::CANCELLED && isset($_SESSION['admin_role'])) { ?>
        <!-- <span><a class="btn btn-info-css btn-sm" href="?controller=booking&amp;action=pay&amp;id=<?php echo $b->id; ?>" >Pagar</a></span> -->
      <?php }else if($b->status != ServiceStatus::CONFIRMED && $b->status != ServiceStatus::PAYED && $b->status != ServiceStatus::CANCELLED && isset($_SESSION['admin_role'])) { ?>
            <!-- <span><a class="btn btn-info-css btn-sm disabled" >Pagar</a></span> -->
      <?php } ?>
    </div>
    <div class="booking-actions col-md-8 text-right">
      <span><a class="btn btn-info-css btn-sm loadingAction" href="?controller=booking&amp;action=show&amp;id=<?php echo $b->id; ?>">Vouchers y Nota de debito</a></span>
      <?php if ($b->status != ServiceStatus::CANCELLED && $cancell == true ) { ?>
          <?php  //usuarios que pueden cancelar una reservas confirmada
              $arrUser = array(677 => 'Luis Choque');
          ?>
          <?php if ($b->status != ServiceStatus::CONFIRMED || $GLOBALS['current_agency']->booking == 'EMIT' || array_key_exists($_SESSION['current_user']->id, $arrUser)){ ?>
            <button class='btn-cancelar btn btn-info-css btn-sm' data-id="<?php echo $b->id ?>" data-campo="<?php echo $expired ?>" data-toggle="modal" data-target="#cancelModal">Cancelar</button>
          <?php } ?>
      <?php } $cancell=false; ?>
    </div>
  </div>
<?php } ?>

<!-- Modal -->
<div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="cancelModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="cancelModalLabel">Cancelar Reserva</h4>
      </div>
      <div class="modal-body">
          <div class="msg-cancel"></div><br>
          <div class="alert alert-danger" id="information">
              <h4><i class="fa fa-warning"></i> Atenci&oacute;n!</h4>
              <div class="msg-fecha-vencida"></div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="btn-no" data-dismiss="modal">NO</button>
         <form method="get" id="formCancelBook" style="display: inline-block;">
          <input type="hidden" id="btn-hconfirm" name="id" value="">
          <input type="hidden" name="request_action" value="cancel_book">
          <input type="hidden" name="controller" value="booking">
          <input type="hidden" name="action" value="cancel_book">
          <button class="btn-confirm btn btn-primary" >SI</button>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

$(function(){
  $('.btn-cancelar').on('click',function(){
      $('.msg-fecha-vencida, .msg-cancel').empty();
      $('#btn-no').text("NO");
      $('#btn-hconfirm').val($(this).data("id"));
      $('.btn-confirm, .msg-cancel, #information').show();
      data=$(this).data("campo");
      if (data == 'si'){
            <?php if ($GLOBALS['current_agency']->booking == 'BOOK' && !array_key_exists($_SESSION['current_user']->id, $arrUser)) { ?>
                $('.msg-fecha-vencida').append("No puedes cancelar esta reserva debido a que tiene gastos de cancelaci&oacute;n.<br> Favor comunicarse con Barry para proceder con la cancelaci&oacute;n.");
                $('#btn-no').text("Cerrar");
                $('.btn-confirm, .msg-cancel').hide();
            <?php
              }
              else{
            ?>
                $('.msg-cancel').append("<strong>Estas seguro de que quieres cancelar esta reserva?</strong>");
                $('.msg-fecha-vencida').append("<strong>Esta reserva tiene gastos de cancelaci&oacute;n.<br> En caso de proceder asumir&aacute; dichos gastos.</strong>");
            <?php
              }
            ?>
      }
      else{
          $('#information').hide();
          $('.msg-cancel').append("<strong>Estas seguro de que quieres cancelar esta reserva?</strong>");
      }
  });

  $('#formCancelBook').submit(function() {
    showProgress();
    var form = $(this);
    $.ajax({
        url         : 'rest_from_js.php',
        data        : form.serialize(),
        success     : function(data) {
            book = JSON.parse(data);
            window.location.href = getLinkGlobal() + "/zion/index.php?controller=booking&action=show&id=" + book.bookId;
        },
        error       : function(errors) {
          alert('Error, hubo un problema.');
        }
    });
    return false;
  });
});

</script>
