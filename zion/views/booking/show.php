<?php
global $HBServices, $MBServices, $MBBServices, $DTServices, $THServices, $RTServices, $TBServices, $NMServices, $JPServices, $details, $book, $agency;

// $debit_code = $book->getDebitNoteNumber();
$book              = json_decode(json_encode($book));
$agency            = json_decode(json_encode($agency));
$debit_code        = $book->debit_note;
$booking_user_name = $book->agent;
if ($book->user_id) {
  $booking_user      = User::findById($book->user_id);
  $booking_user_name = $booking_user->name . ' ' . $booking_user->lastname;
}
$operator_agent       = User::findById($book->operator_id);
$operator_agent_name  = $operator_agent ? $operator_agent->name . ' ' . $operator_agent->lastname : 'Sin procesar';
if (strlen($operator_agent_name) > 17) {
   $operator_agent_name = substr($operator_agent_name, 0, 17) . '...';
}

$agency_book                = Agency::findById($book->agency_id);
$book_error_confirm         =  false;
$can_show_create_debit_note = array();
$global_holder_name         = $book->holder;
if (!empty($book->name))
  $global_holder_name = $book->name . ' ' . $book->last_name;

?>
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-9" >
        <div class="wbox" style="padding:15px;">
          <a href="?controller=booking&amp;action=agency_books"><i class="fa fa-arrow-circle-left"></i> Ver todas mis reservas</a>
          <div class="row">
          <div class="col-md-12 invoice">
          <!-- Invoice header -->
          <div class="row">
            <div id="status" class="col-md-12">
              <?php
              if (isset($HBServices[0]) && $HBServices[0]->getStatus() == "CANCELLED") {
                $book_error_confirm = true;
                ?>
                <div class="alert alert-danger">
                  <strong><i class="fa fa-warning"></i> Reserva cancelada</strong>
                </div>
                <?php
              }
              else if (isset($HBServices[0]) && $HBServices[0]->getStatus() != "BOOKING") {
                  $book_error_confirm = true;
                ?>
                <div class="alert alert-danger">
                  <i class="fa fa-warning"></i> Error al confirmar la reserva. Favor vuelva a intentar - Reserva No v&aacute;lida.
                </div>
              <?php } ?>
              <?php
              foreach ($MBServices as $MBService) {
                foreach ($MBService->getBookingServices() as $service) {
                  if ($service->getStatus() != ServiceStatus::CONFIRMED) {
                    $book_error_confirm = true;
                    ?>
                    <div class="alert alert-danger">
                      <i class="fa fa-warning"></i> La reserva no pudo ser confirmada. Favor comunicarse con Barry. - Reserva No v&aacute;lida.
                    </div>
                    <?php
                  }
                }
              }
              ?>
              <?php
              foreach ($MBBServices as $MBBService) {
                foreach ($MBBService->getBookingServices() as $service) {
                  if ($service->getStatus() != ServiceStatus::CONFIRMED) {
                    $book_error_confirm = true;
                    ?>
                    <div class="alert alert-danger">
                      <i class="fa fa-warning"></i> La reserva no pudo ser confirmada. Favor comunicarse con Barry. - Reserva No v&aacute;lida.
                    </div>
                    <?php
                  }
                }
              }
              ?>
              <?php
                foreach ($DTServices as $DTService) {
                  foreach ($DTService->getBookingServices() as $service) {
                    if ($service->getStatus() != ServiceStatus::CONFIRMED && $service->getStatus() != '') {
                      $book_error_confirm = true;
                      ?>
                      <div class="alert alert-danger">
                        <i class="fa fa-warning"></i> La reserva no pudo ser confirmada. Favor comunicarse con Barry. - Reserva No v&aacute;lida.
                      </div>
                      <?php
                    }
                  }
                }
              ?>
              <?php
              foreach ($RTServices as $RTService) {
                foreach ($RTService->getBookingServices() as $service) {
                  if ($service->getStatus() != ServiceStatus::CONFIRMED) {
                    $book_error_confirm = true;
                    ?>
                    <div class="alert alert-danger">
                      <i class="fa fa-warning"></i> La reserva no pudo ser confirmada. Favor comunicarse con Barry. - Reserva No v&aacute;lida.
                    </div>
                    <?php
                  }
                }
              }
              ?>
              <?php
              foreach ($JPServices as $JPService) {
                foreach ($JPService->getBookingServices() as $service) {
                  if ($service->getStatus() != ServiceStatus::CONFIRMED) {
                    $book_error_confirm = true;
                    ?>
                    <div class="alert alert-danger">
                      <i class="fa fa-warning"></i> La reserva no pudo ser confirmada. Favor comunicarse con Barry. - Reserva No v&aacute;lida.
                    </div>
                    <?php
                  }
                }
              }
              ?>
              <?php
              foreach ($TBServices as $TBService) {
                foreach ($TBService->getBookingServices() as $service) {
                  if ($service->getStatus() != ServiceStatus::CONFIRMED) {
                    $book_error_confirm = true;
                    ?>
                    <div class="alert alert-danger">
                      <i class="fa fa-warning"></i> La reserva no pudo ser confirmada. Favor comunicarse con Barry. - Reserva No v&aacute;lida.
                    </div>
                    <?php
                  }
                }
              }
              ?>
              <?php
                foreach ($NMServices as $NMService) {
                  foreach ($NMService->getBookingServices() as $service) {
                    if ($service->getStatus() != ServiceStatus::CONFIRMED) {
                      $book_error_confirm = true;
                      ?>
                      <div class="alert alert-danger">
                        <i class="fa fa-warning"></i> La reserva no pudo ser confirmada. Favor comunicarse con Barry. - Reserva No v&aacute;lida.
                      </div>
                      <?php
                    }
                  }
                }
              ?>
            </div>
            <div class="col-md-12">
              Estado Reserva:
              <?php
                if ($book->status == ServiceStatus::PAYED && !$book_error_confirm) {
                  echo '<span class="label label-success"><i class="fa fa-check-circle"></i> Confirmada</span> ';
                  echo '<span class="label label-success"><i class="fa fa-check-circle"></i> Pagada</span>';
                }
                if ($book->status == ServiceStatus::CONFIRMED && !$book_error_confirm) {
                  echo '<span class="label label-success"><i class="fa fa-check-circle"></i> Confirmada</span> ';
                  //echo '<span class="label label-warning"><i class="fa fa-exclamation-circle"></i> Pago pendiente</span>';
                }
                if ($book->status == ServiceStatus::PENDING && !$book_error_confirm) {
                  echo '<span class="label label-default"><i class="fa fa-exclamation-circle"></i> Pendiente</span>';
                }
                if ($book->status == ServiceStatus::CANCELLED || $book_error_confirm) {
                  echo '<span class="label label-danger"><i class="fa fa-times-circle"></i> Cancelada</span>';
                }
              ?>
            </div>
          </div>

          <?php
          $serviceCancellacion = getServiceWithNearstCancellation($book->services);
          if ($book->status == ServiceStatus::PENDING) {
            ?>
              <div id="confirmBook" class="modal fade">
                <div class="modal-dialog modal-md">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title">Confirmar Reserva</h4>
                    </div>
                    <div class="modal-body">
                      <p>
                        Una vez confirmada la reserva tendr&aacute;s acceso a los vouchers.<br />
                        Nuestro departamento de contabilidad se pondr&aacute; en contacto con
                        ustedes para efectuar el cobro de la reserva.
                      </p>
                      <?php if (isset($serviceCancellacion)) { ?>
                        <div class="alert alert-danger">
                          <h4><i class="fa fa-warning"></i> Atenci&oacute;n!</h4>
                          <p>En caso de cancelar la reserva despues del <strong><?php echo dateFormatFromDB($serviceCancellacion->cancellation_date); ?></strong>,
                          se aplicar&aacute;n los gastos de cancelaci&oacute;n estipulados en las condiciones de la reserva
                          <?php
                          if (isset($serviceCancellacion->cancellation_fee) && $serviceCancellacion->cancellation_fee != 0) {
                            echo ' (a partir de <strong>' . $serviceCancellacion->cancellation_fee . ' ' . $serviceCancellacion->currency . '</strong> seg&uacute;n la fecha de cancelaci&oacute;n)';
                          } ?>.
                          </p>
                        </div>
                      <?php } ?>
                      <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                      <form method="get" id="formConfirmBook">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <input type="hidden" id="booking_id" name="id" value="<?php echo $book->id; ?>">
                        <input type="hidden" name="request_action" value="confirm_book" />
                        <input type="hidden" name="controller" value="booking" />
                        <input type="hidden" name="action" value="confirm_book" />
                        <button class="btn btn-info-css" onclick="showProgress()">Confirmar Reserva</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <?php   $arr_can_confirm = array(); ?>

              <?php if($GLOBALS['current_agency']->booking != 'BOOK' && !$book_error_confirm || array_key_exists($_SESSION['current_user']->id, $arr_can_confirm) ){ ?>
                      <div class="col-md-6" style="margin-top: 20px;">
                        <button class="btn btn-info-css btn-sm" data-toggle="modal" data-target="#confirmBook">
                          <i class="fa fa-check-circle"></i> Confirmar Reserva
                        </button>
                      </div>
              <?php } ?>
  <?php   } else { ?>
              <!-- id necesario para el guardado de usuario en la reserva -->
              <input type="hidden" id="booking_id" name="id" value="<?php echo $book->id; ?>">
              <?php   $can_create_dbn = array_key_exists($_SESSION['current_user']->id, $can_show_create_debit_note); ?>

              <?php   if($book->status == ServiceStatus::CONFIRMED && $book->debit_number_conta == 0 && $can_create_dbn) { ?>
                        <div class="col-md-6" style="margin-top: 20px;">
                          <button class="btn btn-info-css btn-sm" onclick="createDebitNoteContable()">
                            <i class="fa fa-file"></i> CREAR N.D. CONTABLE
                          </button>
                        </div>
              <?php   } ?>
  <?php   } ?>

          <!-- Invoce data -->
          <div class="col-md-12 service">
            <div class="pull-right" style="margin-top: 10px;">
              <?php $admin = Admin::find(array('user_id' => $_SESSION['current_user']->id)); ?>
              <?php if ($book->status != ServiceStatus::ANNULLED) {
                    if ($_SESSION['current_user']->id == $book->operator_id || isset($admin->edit) && $admin->edit) { ?>
                      <a href="index.php?controller=booking&amp;action=edit&amp;id=<?php echo $book->id; ?>">
                        <i class="fa fa-pencil"></i> Editar reserva
                      </a>
                    <?php
                    }
                  } else { ?>
                    <span style="color: red; font-weight: bold">Nota anulada por la NC A<?php echo getCloneDebitNoteNumber($book); ?></span>
              <?php } ?>
            </div>
            <div class="box-header">Datos de la reserva</div>
            <table width="100%;">
              <tr>
                <td>
                  <table>
                    <tr>
                      <td style="padding-right: 15px;"><strong>Se&ntilde;ores: </strong></td>
                      <td>
                        <?php echo isset($book->agency) && !empty($book->agency) ? $book->agency : $agency->name; ?>
                      </td>
                    </tr>
                    <tr>
                      <td style="padding-right: 15px;"><strong>Agente: </strong></td>
                      <td> <?php echo $booking_user_name; ?></td>
                    </tr>
                    <tr>
                      <td style="padding-right: 15px;"><strong>Referencia: </strong></td>
                      <td><?php echo $global_holder_name; ?></td>
                    </tr>
                  <?php if($book->holder_phone != '') { ?>
                      <tr>
                        <td><strong>Tel&eacute;fono: </strong></td>
                        <td><?php echo $book->holder_phone; ?></td>
                      </tr>
                  <?php } ?>
                  <?php if($book->holder_mail != '') { ?>
                    <tr>
                      <td><strong>Email: </strong></td>
                      <td><?php echo $book->holder_mail; ?></td>
                    </tr>
                  <?php } ?>
                  </table>
                </td>
                <td>
                  <table>
                    <tr>
                      <td style="padding-right: 15px;"><strong>Nro. Confirmaci&oacute;n:</strong></td>
                      <td><?php echo $debit_code != '' ? 'A' . $debit_code : '<i class="text-muted">Sin confirmar</i>'; ?></td>
                    </tr>
                    <tr>
                      <td style="padding-right: 15px;"><strong>Nro. Reserva:</strong></td>
                      <td><?php echo $book->booking_code; ?>
                      </td>
                    </tr>
                    <tr>
                      <td style="padding-right: 15px;"><strong>Fecha Reserva:</strong></td>
                      <td><?php echo dateFormatFromDB($book->book_date); ?></td>
                    </tr>
                    <?php if ($book->status == ServiceStatus::CONFIRMED) {?>
                      <tr>
                        <td style="padding-right: 15px;"><strong>Fecha Confirmaci&oacute;n:</strong></td>
                        <td><?php echo dateFormatFromDB($book->confirm_date); ?></td>
                      </tr>
                    <?php } ?>
                    <?php if ($book->status == ServiceStatus::CANCELLED) {?>
                      <tr>
                        <td style="padding-right: 15px;"><strong>Fecha Cancelaci&oacute;n:</strong></td>
                        <td><?php echo dateFormatFromDB($book->cancel_date); ?></td>
                      </tr>
                    <?php } ?>
                  <?php  if($book->status == ServiceStatus::CONFIRMED && $book->debit_number_conta != 0) { ?>
                    <?php  if(array_key_exists($_SESSION['current_user']->id, $can_show_create_debit_note)) { ?>
                    <tr>
                      <td><strong>Nota Contable</strong></td>
                      <td><?php echo $book->debit_number_conta; ?></td>
                    </tr>
                    <?php  } ?>
                  <?php  } ?>
                    <tr>
                      <td style="padding-right: 15px;"><strong>Procesado por:</strong></td>
                      <td>
                        <div style="white-space: nowrap;">
                          <?php
                          echo $operator_agent ? $operator_agent_name : 'Sin procesar';
                          if (!$operator_agent && $_SESSION['admin_role']) { ?>
                            <form method="get" id="formProcessBook" style="display: inline-block">
                              <input type="hidden" name="id" value="<?php echo $book->id; ?>">
                              <input type="hidden" name="request_action" value="process" />
                              <input type="hidden" name="controller" value="booking" />
                              <input type="hidden" name="action" value="process" />
                              <button class="btn btn-info-css btn-sm">Procesar</button>
                            </form>
                          <?php } ?>
                        </div>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <?php if (isset($serviceCancellacion)) { ?>
                <tr>
                  <td colspan="2" class="strong">
                    <strong>Gastos de cancelaci&oacute;n desde: </strong>
                    <span class="text-danger">
                      <?php
                        echo dateFormatFromDB($serviceCancellacion->cancellation_date);
                        if (isset($serviceCancellacion->cancellation_fee) && $serviceCancellacion->cancellation_fee != 0) {
                          echo ' (desde ' . $serviceCancellacion->cancellation_fee . ' ' . $serviceCancellacion->currency . ')';
                        }
                      ?>
                    </span>
                  </td>
                </tr>
              <?php } ?>
            </table>
          </div>

          <?php
          $showVoucher = false;
          if ($book->status != ServiceStatus::CANCELLED) {
            ?>
            <div class="col-md-12 service">
              <div class="box-header">Vouchers</div>
              <?php
              $showVoucher = !($book->status == ServiceStatus::PENDING && !$_SESSION['admin_role']);
              if (!$showVoucher) {
                echo '<div class="text-danger strong"><i class="fa fa-warning"></i> La reserva debe ser confirmada para acceder a los vouchers.</div>';
              }
              ?>
              <table class="table table-hover">
                <tbody>
                  <?php
                  $service_count = 0;
                  foreach ($HBServices as $HBService) {
                    foreach ($HBService->getBookingServices() as $service) {
                      $service_count++;
                      $serviceInfo = $service->getServiceInfo();
                      if (isset($serviceInfo)) {
                      ?>
                      <tr>
                        <td>
                          <span class="service-title"><?php echo $serviceInfo->getName(); ?></span>
                          <span class="text-muted">
                            - <?php echo $HBService->getReferenceIncomingOffice() . '-' . $HBService->getReferenceFileNumber(); ?>
                            <?php if ($_SESSION['admin_role']) { ?>
                              - (HOTELBEDS)
                            <?php } ?>
                          </span>
                          </td>
                        <td class="text-right">
                          <?php
                          if ($showVoucher && $HBServices[0]->getStatus() == "BOOKING" && $HBServices[0]->getStatus() != "CANCELLED") {  ?>
                            <button class="btn btn-info-css btn-sm" data-toggle="modal" data-target="#voucher_<?php echo $service_count; ?>">
                              <i class="fa fa-file"></i> Ver Voucher
                            </button>
                            <?php }
                           ?>
                        </td>
                      </tr>
                      <?php
                      }//If
                    }
                  }

                  foreach ($MBServices as $MBService) {
                    foreach ($MBService->getBookingServices() as $service) {
                      $service_count++;
                      $serviceInfo = $service->getServiceInfo();
                      ?>
                      <tr>
                        <td>
                          <span class="service-title"><?php echo $serviceInfo->getName(); ?></span>
                          <span class="text-muted">
                            - <?php echo $service->getLocalizer(); ?>
                            <?php if ($_SESSION['admin_role']) { ?>
                              - (METHABOOK)
                            <?php } ?>
                          </span>
                          </td>
                        <td class="text-right">
                          <?php
                          if ($showVoucher && $service->getStatus() == ServiceStatus::CONFIRMED) {  ?>
                            <button class="btn btn-info-css btn-sm" data-toggle="modal" data-target="#voucher_<?php echo $service_count; ?>">
                              <i class="fa fa-file"></i> Ver Voucher
                            </button>
                            <?php }
                           ?>
                        </td>
                      </tr>
                      <?php
                    }
                  }

                  foreach ($MBBServices as $MBBService) {
                    foreach ($MBBService->getBookingServices() as $service) {
                      $service_count++;
                      $serviceInfo = $service->getServiceInfo();
                      ?>
                      <tr>
                        <td>
                          <span class="service-title"><?php echo $serviceInfo->getName(); ?></span>
                          <span class="text-muted">
                            - <?php echo $service->getLocalizer(); ?>
                            <?php if ($_SESSION['admin_role']) { ?>
                              - (METHABOOK2)
                            <?php } ?>
                          </span>
                          </td>
                        <td class="text-right">
                          <?php
                          if ($showVoucher && $service->getStatus() == ServiceStatus::CONFIRMED) {  ?>
                            <button class="btn btn-info-css btn-sm" data-toggle="modal" data-target="#voucher_<?php echo $service_count; ?>">
                              <i class="fa fa-file"></i> Ver Voucher
                            </button>
                            <?php }
                           ?>
                        </td>
                      </tr>
                      <?php
                    }
                  }

                  foreach ($DTServices as $DTService) {
                      foreach ($DTService->getBookingServices() as $service) {
                        $service_count++;
                        $serviceInfo = $service->getServiceInfo();
                        ?>
                        <tr>
                          <td>
                            <span class="service-title"><?php echo $serviceInfo->getName(); ?></span>
                            <span class="text-muted">
                              - <?php echo $service->getLocalizer(); ?>
                              <?php if ($_SESSION['admin_role']) { ?>
                                - (DOMITUR)
                              <?php } ?>
                            </span>
                            </td>
                          <td class="text-right">
                            <?php
                            if ($showVoucher && $service->getStatus() == ServiceStatus::CONFIRMED || $service->getStatus() == '') {  ?>
                              <button class="btn btn-info-css btn-sm" data-toggle="modal" data-target="#voucher_<?php echo $service_count; ?>">
                                <i class="fa fa-file"></i> Ver Voucher
                              </button>
                              <?php }
                             ?>
                          </td>
                        </tr>
                        <?php
                      }
                  }

                  foreach ($THServices as $THService) {
                    foreach ($THService->getBookingServices() as $service) {
                      $service_count++;
                      $serviceInfo = $service->getServiceInfo();
                      ?>
                      <tr>
                        <td>
                          <span class="service-title"><?php echo $serviceInfo->getName(); ?></span>
                          <span class="text-muted">
                            - <?php echo $service->getLocalizer(); ?>
                            <?php if ($_SESSION['admin_role']) { ?>
                              - (TOURICO)
                            <?php } ?>
                          </span>
                          </td>
                        <td class="text-right">
                          <?php
                          if ($showVoucher && $service->getStatus() == ServiceStatus::CONFIRMED || $service->getStatus() == '') {  ?>
                            <button class="btn btn-info-css btn-sm" data-toggle="modal" data-target="#voucher_<?php echo $service_count; ?>">
                              <i class="fa fa-file"></i> Ver Voucher
                            </button>
                            <?php }
                           ?>
                        </td>
                      </tr>
                      <?php
                    }
                  }

                  foreach ($JPServices as $JPService) {
                    foreach ($JPService->getBookingServices() as $service) {
                      $service_count++;
                      $serviceInfo = $service->getServiceInfo();
                      ?>
                      <tr>
                        <td>
                          <span class="service-title"><?php echo $serviceInfo->getName(); ?></span>
                          <span class="text-muted">
                            - <?php echo $service->getLocalizer(); ?>
                            <?php if ($_SESSION['admin_role']) { ?>
                              - (DOMTIRU2)
                            <?php } ?>
                          </span>
                          </td>
                        <td class="text-right">
                          <?php
                          if ($showVoucher && $service->getStatus() == ServiceStatus::CONFIRMED) {  ?>
                            <button class="btn btn-info-css btn-sm" data-toggle="modal" data-target="#voucher_<?php echo $service_count; ?>">
                              <i class="fa fa-file"></i> Ver Voucher
                            </button>
                            <?php }
                           ?>
                        </td>
                      </tr>
                      <?php
                    }
                  }

                  foreach ($RTServices as $RTService) {
                    foreach ($RTService->getBookingServices() as $service) {
                      $service_count++;
                      $serviceInfo = $service->getServiceInfo();
                      ?>
                      <tr>
                        <td>
                          <span class="service-title"><?php echo $serviceInfo->getName(); ?></span>
                          <span class="text-muted">
                            - <?php echo $service->getLocalizer(); ?>
                            <?php if ($_SESSION['admin_role']) { ?>
                              - (RESTEL)
                            <?php } ?>
                          </span>
                          </td>
                        <td class="text-right">
                          <?php
                          if ($showVoucher && $service->getStatus() == ServiceStatus::CONFIRMED) {  ?>
                            <button class="btn btn-info-css btn-sm" data-toggle="modal" data-target="#voucher_<?php echo $service_count; ?>">
                              <i class="fa fa-file"></i> Ver Voucher
                            </button>
                            <?php }
                           ?>
                        </td>
                      </tr>
                      <?php
                    }
                  }

                  foreach ($TBServices as $TBService) {
                    foreach ($TBService->getBookingServices() as $service) {
                      $service_count++;
                      $serviceInfo = $service->getServiceInfo();
                      ?>
                      <tr>
                        <td>
                          <span class="service-title"><?php echo $serviceInfo->getName(); ?></span>
                          <span class="text-muted">
                            - <?php echo $service->getLocalizer(); ?>
                            <?php if ($_SESSION['admin_role']) { ?>
                              - (TAILORBEDS)
                            <?php } ?>
                          </span>
                          </td>
                        <td class="text-right">
                          <?php
                          // __log($service->getStatus() . "--" . ServiceStatus::CONFIRMED);
                          if ($showVoucher && $service->getStatus() == ServiceStatus::CONFIRMED) {  ?>
                            <button class="btn btn-info-css btn-sm" data-toggle="modal" data-target="#voucher_<?php echo $service_count; ?>">
                              <i class="fa fa-file"></i> Ver Voucher
                            </button>
                            <?php }
                           ?>
                        </td>
                      </tr>
                      <?php
                    }
                  }
                  
                  foreach ($NMServices as $NMService) {
                    foreach ($NMService->getBookingServices() as $service) {
                      $service_count++;
                      $serviceInfo = $service->getServiceInfo();
                      ?>
                      <tr>
                        <td>
                          <span class="service-title"><?php echo $serviceInfo->getName(); ?></span>
                          <span class="text-muted">
                            - <?php echo $service->getLocalizer(); ?>
                            <?php if ($_SESSION['admin_role']) { ?>
                              - (NEMO)
                            <?php } ?>
                          </span>
                          </td>
                        <td class="text-right">
                          <?php
                          if ($showVoucher && $service->getStatus() == ServiceStatus::CONFIRMED) {  ?>
                            <button class="btn btn-info-css btn-sm" data-toggle="modal" data-target="#voucher_<?php echo $service_count; ?>">
                              <i class="fa fa-file"></i> Ver Voucher
                            </button>
                            <?php }
                           ?>
                        </td>
                      </tr>
                      <?php
                    }
                  }

                  $juniperProvider  = array(159);
                  $services         = $book->services;
                  foreach ($services as $service) {
                    if (
													$service->provider_id == Provider::HOTELBEDS || $service->provider_id == Provider::RATEHAWK
													|| $service->provider_id == Provider::NEMO || $service->provider_id == Provider::TRESBTOUR
													|| in_array($service->provider_id, $juniperProvider)
												) {
													if (!$service->is_manual)
														continue;
												}

                    $service_count++;
                    ?>
                    <tr>
                      <td>
                        <span class="service-title"><?php echo $service->name; ?></span>
                        <?php if(!empty($service->localizer)){ ?>
                              <span class="text-muted"> - <?php echo $service->localizer; ?></span>
                        <?php }
                           if ($_SESSION['admin_role'] && !empty($service->provider_id)) {
                        ?>
                              <span class="text-muted"> - <?php echo "(" . Provider::findById($service->provider_id)->name . ")"; ?> </span>
                        <?php } ?>
                      </td>
                      <td class="text-right">
                        <?php  if ($showVoucher && $service->show_voucher) {
                              if(Options_file($service->id, "search")) { ?>
                                <div>
                                  <a href="http://barrybolivia.com/zion/<?php echo Options_file($service->id, "link") ?>" id="download" class="btn-sm"  download><i class="fa fa-file"></i> Download Voucher</a>
                                  <?php $admin = Admin::find(array('user_id' => $_SESSION['current_user']->id)); ?>
                                  <?php if($admin->edit) { ?>
                                      <form class="frm_delete_<?php echo $service->id ?>" method="get" style="float:right;">
                                        <i class="fa fa-minus-circle" aria-hidden="true" title="Eliminar Archivo" style="color:red;font-size:15px;cursor:pointer;" onclick="delete_file('frm_delete_<?php echo $service->id ?>')"></i>
                                        <input type="hidden" name="action" value="delete_file">
                                        <input type="hidden" name="service_id" value="<?php echo $service->id; ?>">
                                        <input type="hidden" name="bookingId" value="<?php echo $book->id; ?>">
                                        <input type="hidden" name="controller" value="booking">
                                      </form>
                                  <?php } ?>
                                </div>
                        <?php    } else { ?>
                                    <?php if ($service->url_voucher === '') { ?>
                                      <button class="btn btn-info-css btn-sm" data-toggle="modal" data-target="#myModal_<?php echo $service_count ?>">
                                        <i class="fa fa-file"></i> Ver Voucher
                                      </button>
                                    <?php   } else { ?>
                                      <a href="<?php echo $service->url_voucher; ?>" target="_blank" style="font-size: 14px;font-weight: bold;">
                                        <i class="fa fa-file" style="color:red;font-size:15px;cursor:pointer;"></i> Ver Voucher
                                      </a>
                                    <?php   } ?>
                                    <?php if (
                                      $service->provider_id == Provider::SPECIALTOURS && $_SESSION['current_user']->id == $book->operator_id
                                      || $service->provider_id == Provider::SPECIALTOURS && isset($admin->edit) && $admin->edit
                                    ) { ?>

                                      <a href="?controller=booking&action=updateVoucherSpecial&id=<?php echo $_GET['id']; ?>&serviceId=<?php echo $service->id; ?>" onClick="showProgress()" class="btn btn-info-css btn-sm">
                                        <span class="glyphicon glyphicon-refresh"></span>
                                      </a>

                                    <?php	} ?>
                        <?php    }
                            }
                        ?>
                      </td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
            <?php
          }
          ?>

          <div class="col-md-12 service">
            <div >
              <div class="col-md-6 col-xs-6 box-header">Notas de d&eacute;bido</div>
              <div class="col-md-6 col-xs-6 text-right strong box-header"><?php echo getBookNumberOrDebitNumber($book); ?></div>
            </div>
            <div class="col-md-7">
              <button class="btn btn-info-css btn-sm" data-toggle="modal" data-target="#invoice" onclick="showClientDebitNote()">
                <i class="fa fa-file"></i> N.D. Cliente
              </button>

              <button class="btn btn-info-css btn-sm" data-toggle="modal" data-target="#invoice" onclick="showAgencyDebitNote()">
                <i class="fa fa-file"></i> N.D. Freelance
              </button>
              <?php if ($_SESSION['admin_role']) { ?>
                <button class="btn btn-info-css btn-sm" data-toggle="modal" data-target="#invoice" onclick="showOperatorDebitNote()">
                  <i class="fa fa-file"></i> N.D. Operadora
                </button>
              <?php } ?>

              <?php
              $book_thread_arr = explode('-', $book->debit_note);
              $book_threads = Booking::findByQuery('SELECT * FROM Booking WHERE debit_note LIKE "' . $book_thread_arr[0] . '%"');
              if ($book->debit_note != '' && count($book_threads) > 1 && $_SESSION['admin_role']) {
              ?>
                <div class="row" style="margin-top: 20px;">
                  <div class="col-md-12 box-header">Historial Notas</div>
                  <div class="col-md-12">
                    <?php
                    foreach ($book_threads as $thread) {
                      if ($thread->debit_note == $book->debit_note) {
                        echo '<button class="btn btn-info btn-sm" disabled>' . $book->debit_note . '</button>';
                      } else {
                        ?>
                        <a class="btn btn-info btn-sm" href="?controller=booking&amp;action=show&amp;id=<?php echo $thread->id; ?>"><?php echo $thread->debit_note; ?></a>
                        <?php
                      }
                    } ?>
                  </div>
                </div>
              <?php } ?>
            </div>
            <div id="prices_resume" class="col-md-5 text-right">
              <table id="total_price_resume" style="width: 100%;"></table>
              <div class="link" onclick="$('#total_price_resume').toggle(); $('#extra_price_resume').fadeToggle();">Ver m&aacute;s informaci&oacute;n <i class="fa fa-caret-down"></i></div>
              <table class="col-md-12" id="extra_price_resume" style="display:none; width: 100%;"></table>
            </div>
          </div>
          <?php
            if (isset($admin->edit) && $admin->edit) {
          ?>
              <div class="clearfix"></div>
              <div id="load_content_vitacora" class="text-center">Cargando Historial de Modificaciones.....</div>
              <div id="content_vitacora" ></div>
        <?php  } ?>
        </div>
        </div>
      </div>
      </div>
      <div class="col-md-3"><?php include('views/admin/_menu.php'); ?></div>
    </div>
  </div>
</div>

<!-- Modals for the services vouchers -->
<?php
if ($showVoucher) {
  $service_count = 0;
  // Hotelbeds vouchers
  foreach ($HBServices as $HBService) {
    foreach ($HBService->getBookingServices() as $service) {
      $service_count++;
      $serviceInfo = $service->getServiceInfo();
      if (isset($serviceInfo)) {
      ?>
      <div id="voucher_<?php echo $service_count; ?>" class="voucher modal fade">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Voucher <?php echo $serviceInfo->getName(); ?></h4>
            </div>
            <div class="modal-body">
              <div id="voucher_<?php echo $service_count; ?>_content">
                <?php include('voucher/_voucher_' . $service->getServiceType() . '.php'); ?>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <?php
      }
    }
  }

  // Methabook vouchers
  foreach ($MBServices as $MBService) {
    foreach ($MBService->getBookingServices() as $service) {
      $service_count++;
      $serviceInfo = $service->getServiceInfo();
      ?>
      <div id="voucher_<?php echo $service_count; ?>" class="voucher modal fade">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Voucher <?php echo $serviceInfo->getName(); ?></h4>
            </div>
            <div class="modal-body">
              <div id="voucher_<?php echo $service_count; ?>_content">
                <?php include('voucher/_voucher_mb_' . $service->getServiceType() . '.php'); ?>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <?php
    }
  }

  foreach ($MBBServices as $MBBService) {
    foreach ($MBBService->getBookingServices() as $service) {
      $service_count++;
      $serviceInfo = $service->getServiceInfo();
      ?>
      <div id="voucher_<?php echo $service_count; ?>" class="voucher modal fade">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Voucher <?php echo $serviceInfo->getName(); ?></h4>
            </div>
            <div class="modal-body">
              <div id="voucher_<?php echo $service_count; ?>_content">
                <?php include('voucher/_voucher_mb_' . $service->getServiceType() . '.php'); ?>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <?php
    }
  }

  foreach ($THServices as $THService) {
    foreach ($THService->getBookingServices() as $service) {
      $service_count++;
      $serviceInfo = $service->getServiceInfo();
      ?>
      <div id="voucher_<?php echo $service_count; ?>" class="voucher modal fade">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Voucher <?php echo $serviceInfo->getName(); ?></h4>
            </div>
            <div class="modal-body">
              <div id="voucher_<?php echo $service_count; ?>_content">
                <?php include('voucher/_voucher_th_' . $service->getServiceType() . '.php'); ?>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <?php
    }
  }

  // Domitur vouchers
  foreach ($DTServices as $DTService) {
    foreach ($DTService->getBookingServices() as $service) {
      $service_count++;
      $serviceInfo = $service->getServiceInfo();
      ?>
      <div id="voucher_<?php echo $service_count; ?>" class="voucher modal fade">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Voucher <?php echo $serviceInfo->getName(); ?></h4>
            </div>
            <div class="modal-body">
              <div id="voucher_<?php echo $service_count; ?>_content">
                <?php include('voucher/_voucher_dt_' . $service->getServiceType() . '.php'); ?>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <?php
    }
  }

  foreach ($JPServices as $JPService) {
    foreach ($JPService->getBookingServices() as $service) {
      $service_count++;
      $serviceInfo = $service->getServiceInfo();
      ?>
      <div id="voucher_<?php echo $service_count; ?>" class="voucher modal fade">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Voucher <?php echo $serviceInfo->getName(); ?></h4>
            </div>
            <div class="modal-body">
              <div id="voucher_<?php echo $service_count; ?>_content">
                <?php include('voucher/_voucher_mb_' . $service->getServiceType() . '.php'); ?>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <?php
    }
  }

  foreach ($RTServices as $RTService) {
    foreach ($RTService->getBookingServices() as $service) {
      $service_count++;
      $serviceInfo = $service->getServiceInfo();
      ?>
      <div id="voucher_<?php echo $service_count; ?>" class="voucher modal fade">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Voucher <?php echo $serviceInfo->getName(); ?></h4>
            </div>
            <div class="modal-body">
              <div id="voucher_<?php echo $service_count; ?>_content">
                <?php include('voucher/_voucher_rt_' . $service->getServiceType() . '.php'); ?>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <?php
    }
  }

  foreach ($TBServices as $TBService) {
    foreach ($TBService->getBookingServices() as $service) {
      $service_count++;
      $serviceInfo = $service->getServiceInfo();
      ?>
      <div id="voucher_<?php echo $service_count; ?>" class="voucher modal fade">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Voucher <?php echo $serviceInfo->getName(); ?></h4>
            </div>
            <div class="modal-body">
              <div id="voucher_<?php echo $service_count; ?>_content">
                <?php include('voucher/_voucher_th_' . $service->getServiceType() . '.php'); ?>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <?php
    }
  }

  foreach ($NMServices as $NMService) {
		foreach ($NMService->getBookingServices() as $service) {
			$service_count++;
			$serviceInfo = $service->getServiceInfo();
			?>
			<div id="voucher_<?php echo $service_count; ?>" class="voucher modal fade">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Voucher <?php echo $serviceInfo->getName(); ?></h4>
						</div>
						<div class="modal-body">
							<div id="voucher_<?php echo $service_count; ?>_content">
								<?php include('voucher/_voucher_nm_' . $service->getServiceType() . '.php'); ?>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="modal-footer">
						</div>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</div><!-- /.modal -->
			<?php
		}
	}
  
  // Manual vouchers
  $services = $book->services;
  foreach ($services as $key => $service) {
    if ($service->is_manual && $service->provider_id != Provider::HOTELBEDS) {
      $service_count++;
      ?>
      <div class="cmp modal fade" id="myModal_<?php echo $service_count ?>" >
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Voucher <?php echo $agency->name; ?></h4>
            </div>
            <div class="modal-body">
              <div id="voucher_<?php echo $service_count; ?>_content">
                <?php include('voucher_manual/_cmp_' . ServiceType::getTypeName($service->type) . '.php'); ?>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
            </div>
          </div>
        </div>
      </div>
    <?php
    }
  }
}
?>

<!-- Modals for the invoice -->
<div id="invoice" class="modal fade">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Nota de D&eacute;bito</h4>
      </div>
      <div class="modal-body">
        <div id="invoice_content">
          <table style="width: 100%">
            <tr>
              <td style="width: 60%; vertical-align: bottom;">
                <table>
                  <tr>
                    <td><div id="invoice_logo"></div></td>
                    <td><div id="invoice_header_info" style="padding-top: 10px; padding-left: 30px; font-size: 12px"></div></td>
                  </tr>
                </table>
              </td>
              <td style="width: 40%; text-align: right; vertical-align: bottom;">
                <div class="text-right hidden-print avoid-mailing">
                  <div class="link" onclick="showSendMail('Nota de debito', 'invoice_content');">
                    <span class="fa-stack fa-2x">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
                    </span>
                  </div>
                  <div class="link" onclick="printDiv('invoice_content');">
                    <span class="fa-stack fa-2x">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <i class="fa fa-print fa-stack-1x fa-inverse"></i>
                    </span>
                  </div>
                </div>
                <div style="text-align: right; font-size: 16px; font-weight: bold;">
                  <?php if ($book->debit_note == '') { ?>
                    Nota de Reserva:
                  <?php } else { ?>
                    Nota de Confirmaci&oacute;n:
                  <?php } ?>
                </div>
                <?php
                if (strpos($book->debit_note, '-') !== false) {
                  echo '<div style="color: red; font-weight: bold; font-size: 14px;">Esta nota anula la NC A' . getClonePrevDebitNoteNumber($book) . '</div>';
                }
                ?>
              </td>
            </tr>
          </table>

          <div style="padding: 10px 15px; background-color: #fff; border-radius: 5px; border: 1px solid #999; font-size: 12px;">
            <?php if ($book->debit_note == '') { ?>
              <div class="text-danger" style="font-size: 13px;">
                La siguiente es una Nota de Reserva. Debes confirmar la reserva para obtener una Nota de Confirmaci&oacute;n
              </div>
            <?php } ?>
            <table style="width: 100%;">
              <tr>
                <td>
                  <table>
                    <tr>
                      <td style="padding-right: 15px;"><strong>Se&ntilde;ores: </strong></td>
                      <td> <?php echo $book->agency ? $book->agency : $agency->name; ?></td>
                    </tr>
                    <tr>
                      <td style="padding-right: 15px;"><strong>Agente: </strong></td>
                      <td> <?php echo $booking_user_name; ?></td>
                    </tr>
                    <tr>
                      <td style="padding-right: 15px;"><strong>Referencia: </strong></td>
                      <td> <?php echo $global_holder_name; ?></td>
                    </tr>
                  </table>
                </td>
                <td style="text-align: right;">
                  <table>
                    <?php if(getDebitNoteNumber($book) != '') { ?>
                        <tr>
                          <td style="padding-right: 15px;">
                            <strong>Nro Confirmaci&oacute;n:</strong>
                          </td>
                          <td style="text-align: left;">
                            <strong><?php echo getBookNumberOrDebitNumber($book);?></strong>
                          </td>
                        </tr>
                    <?php } ?>
                    <tr>
                      <td style="padding-right: 15px;">
                        <strong>Nro Reserva:</strong>
                      </td>
                      <td style="text-align: left;">
                        <?php echo $book->booking_code; ?>
                      </td>
                    </tr>
                    <?php if ($book->status == ServiceStatus::CONFIRMED) { ?>
                          <td style="padding-right: 15px;"><strong>Fecha Confirmaci&oacute;n:</strong></td> <td style="text-align: left;"><?php echo dateFormatFromDB($book->confirm_date); ?></td>
                    <?php } else if ($book->status == ServiceStatus::CANCELLED || $book->status == ServiceStatus::ANNULLED) { ?>
                          <td style="padding-right: 15px;"><strong>Fecha Cancelaci&oacute;n:</strong></td> <td style="text-align: left;"><?php echo ($book->cancel_date != "0000-00-00 00:00:00") ? dateFormatFromDB($book->cancel_date) : 'Sin Fecha'; ?></td>
                    <?php } else {?>
                          <td style="padding-right: 15px;"><strong>Fecha Reserva: </strong></td> <td style="text-align: left;"><?php echo dateFormatFromDB($book->book_date); ?></td>
                    <?php  } ?>
                    <tr>
                      <td style="padding-right: 15px;">
                        <strong>Procesado por:</strong>
                      </td>
                      <td style="text-align: left;">
                        <?php echo $operator_agent ? $operator_agent_name : 'Sin procesar'; ?>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </div>

          <!-- Show services -->
          <div style="margin-top: 10px; padding: 15px; background-color: #fff; border-radius: 5px; border: 1px solid #999; font-size: 12px;">
            <table style="width: 100%;">
              <tr>
                <td><div style="font-size: 14px; font-weight: bold;"><?php echo ($book->view_mode == 2) ? "Paquete" : "Servicio" ?> </div></td>
                <td style="text-align: right; font-weight: bold; padding-right: 20px;"><?php echo ($book->view_mode != 1 && $book->view_mode != 2) ? "Precio" : " " ?></td>
              </tr>
            </table>
            <?php
            $policies_str     = '';
            $services         = $book->services;
            $total_services   = count($services);
            $service_count    = 0;
            $total_price      = 0;
            $total_commission = 0;
            $total_agency_fee = 0;
            $total_incentive  = 0;
            $total_net_price  = 0;
            $total_com_suc    = 0;
            $total_net_price_by_provider = array();
            $currency         = $services[0]->currency;
            if ($book->view_mode == 2 ) {
              echo "<div style='padding: 5px 10px; background-color: #fff; border-top: 1px solid #999;'><pre style='font-size: 11px;border: none;padding-left: 0px;background: white;font-family:inherit'>". ($book->package_description) ."</pre></div>";
            }
            foreach ($services as $service) {
              $service_count++;
              if ($book->view_mode != 2) {
                include('invoice/_invoice_' . ServiceType::getTypeName($service->type) . '.php');
              }
              $total_price      += $service->price;
              $total_commission += $service->commission;
              $total_incentive  += $service->incentive;
              $total_agency_fee += $service->agency_fee;
              $total_net_price  += $service->net_price;
              $total_com_suc    += $service->commission_suc;
              if (!isset($total_net_price_by_provider[$service->provider_id])) {
                $total_net_price_by_provider[$service->provider_id] = 0;
              }
              $total_net_price_by_provider[$service->provider_id] += $service->net_price;
            }
            ?>


            <div style="font-weight: bold; vertical-align: top; text-align: left; padding: 0px 10px; color: #F22030">
              <span>
                <?php if (isset($serviceCancellacion)) { ?>
                  NOTA: En caso de cancelar despu&eacute;s del <?php echo dateFormatFromDB($serviceCancellacion->cancellation_date); ?>
                  se aplicar&aacute;n penalidades seg&uacute;n pol&iacute;ticas del hotel.
                <?php } else { ?>
                  NOTA: En caso de cancelar la reserva se aplicar&aacute;n penalidades seg&uacute;n pol&iacute;ticas del hotel.
                <?php } ?>
              </span>
            </div>
          </div>

          <div>
            <table style="width: 100%;">
              <tr>
                <td style="width: 60%; vertical-align: top; text-align: left; padding: 15px;">
                  <?php if ($book->remarks != '') { ?>
                    <div>
                      <div style="color: red; font-weight: bold; font-size: 12px">Observaciones:</div>
                      <div><?php echo nl2br($book->remarks); ?></div>
                    </div>
                  <?php }
                  if ($book->operator_remarks != '') { ?>
                    <div id="operator_remarks"></div>
                  <?php } ?>
                </td>
                <td style="width: 40%; vertical-align: top;">
                  <table id="total_price_table" style="float: right; font-size: 14px; text-align: right; margin-top: 10px;"></table>
                </td>
              </tr>
            </table>
          </div>
          <div style="clear: both;"></div>
    <?php  if ($_SESSION['current_agency']->id == 9) { ?>
          <div id="conten_price_details" style="margin-top: 10px; padding: 15px; background-color: #fff; border-radius: 5px; border: 1px solid #999; font-size: 12px;">
              <div style="font-size: 14px; font-weight: bold;padding: 5px 5px 0px 0px;border-bottom: 1px solid #111;">
                Precios Detallados
              </div>
              <div id="price_details" style="margin-top: 10px;">
              </div>
          </div>
    <?php  } ?>
          <div style="clear: both;"></div>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Images and agency info for the invoice -->
<?php
$operator = Agency::findById(1);
?>
<div id="invoice_operator_logo" class="hidden"><?php $operator->showLogo(); ?></div>
<div id="invoice_agency_logo" class="hidden"><img src="<?php echo $agency->logo; ?>" /></div>
<div id="invoice_operator_info" class="hidden">
  <strong><?php echo $operator->name; ?></strong><br />
  <?php echo $operator->address; ?><br />
  <?php echo $operator->phone; ?><br />
  <?php echo $operator->mail; ?>
</div>
<div id="invoice_agency_info" class="hidden">
  <strong><?php echo $agency->name; ?></strong><br />
  <?php echo $agency->address; ?><br />
  <?php echo $agency->phone; ?><br />
  <?php echo $agency->mail; ?>
</div>

<!-- Raw data for the price table -->
<div class="invisible hidden">
  <table id="total_user_price">
    <tr style="font-weight: bold;">
      <td style="padding-right: 15px;">Precio Total:</td>
      <td><?php echo number_format($total_price + $total_agency_fee, 2) . " $currency"; ?></td>
    </tr>
  </table>
  <table id="agency_fee">
    <tr>
      <td style="padding-right: 15px;">
        - Fee agencia
         (<?php echo $total_agency_fee != 0 ? round(($total_agency_fee / $total_price) * 100, 1) : '0' ?>%):
      </td>
      <td><?php echo number_format($total_agency_fee, 2) . " $currency"; ?></td>
    </tr>
  </table>
  <table id="agency_commission">
    <tr>
      <td style="padding-right: 15px;">
        <?php $perc = $total_price != 0 ? round(($total_commission / $total_price) * 100, 1) : 0 ?>
        - Fee Counter <?php echo "($perc%)"; ?>:
      </td>
      <td><?php echo number_format($total_commission, 2) . " $currency"; ?></td>
    </tr>
  </table>
  <table id="total_price_agency">
    <tr style="font-weight: bold;">
      <td style="padding-right: 15px; padding-top: 5px;">Total a pagar:</td>
      <td style="border-top: 1px solid #555; padding-top: 5px;">
        <?php
        $total_payable = round($total_price - $total_commission, 2);
        echo number_format($total_payable, 2) . " $currency"; ?>
      </td>
    </tr>
  </table>
  <?php

  if ($_SESSION['admin_role']) { ?>
    <table id="total_price_operator">
      <tr style="font-weight: bold;">
        <td style="padding-right: 15px; padding-top: 10px;">Total a cobrar:</td>
        <td style="border-top: 1px solid #555; padding-top: 10px;">
          <?php echo number_format($total_payable, 2) . " $currency"; ?>
        </td>
      </tr>
    </table>

    <table id="counter_incentive">
      <tr>
        <td style="padding-right: 15px;">
          - Incentivo counter:
        </td>
        <td><?php echo number_format($total_incentive, 2) . " $currency"; ?></td>
      </tr>
    </table>
    <table id="operator_commission">
      <tr>
        <td style="padding-right: 15px;">
          <?php
          $operator_commission = $total_payable - $total_net_price - $total_incentive;
          ?>
          - Comisi&oacute;n operadora:
        </td>
        <td><?php echo number_format($operator_commission, 2) . " $currency"; ?></td>
      </tr>
    </table>
    <table id="net_price">
      <?php
      foreach ($total_net_price_by_provider as $key => $value) {
        ?>
        <tr style="font-weight: bold;">
          <td style="padding-right: 15px; padding-top: 5px;">
            <?php if ($key) {
              $provider = Provider::findById($key);
              echo 'Pago a ' . $provider->name;
            }
            else {
              echo "Pago a proveedor";
            } ?>
          </td>
          <td style="border-top: 1px solid #555; padding-top: 5px;">
            <?php echo number_format($value, 2) . " $currency"; ?>
          </td>
        </tr>
        <?php
      }
      ?>
    </table>

    <div id="operator_remarks_raw">
      <div style="color: red; font-weight: bold; font-size: 12px; margin-top: 10px;">Observaciones Operadora:</div>
      <div><?php echo nl2br($book->operator_remarks); ?></div>
    </div>
  <?php } ?>
</div>
<!-- MODAL PARA MENSAJE DE ERRORES -->
<div id="msg_errors_modal" class="modal fade" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #e74949;">
        <h4 class="modal-title" style="color:white;font-weight:bold;">Notificación!</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-1">
            <span class="fa fa-exclamation-circle" style="font-size: 51px;color:#e74949;position: absolute;margin-top:7px"></span>
          </div>
          <div class="col-md-11" style="padding-left: 32px;">
            <span id="msg_errors_ajax" style="font-size: 14px;">

            </span>
          </div>
        </div>


        <div class="msg-errors"></div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-danger" onclick="$('#msg_errors_modal').modal('hide')">Aceptar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
  function delete_file(id) {
    if(!confirm("Confirme para eliminar el Archivo?")) {
      return false;
    }
    showProgress();
    var form = $('.' + id);
    $.ajax({
        url         : 'rest_from_js.php',
        data        : form.serialize(),
        success     : function(data) {
            book = JSON.parse(data);
            window.location.href = getLinkGlobal() + "/zion/index.php?controller=booking&action=show&id=" + book.bookId;
        },
        error       : function(errors) {
          hideProgress();
          alert('Error, hubo un problema.');
        }
    });
    return false;
  }
  $(function() {
    $('#booking_agency_books').addClass('active');

    // Set prices resume table
    <?php

    if ($_SESSION['admin_role']) {
        echo "showOperatorDebitNote();";
    }
    else {
        echo "showAgencyDebitNote();";
    } ?>

    $('#total_price_resume').html($('#total_user_price').html());
    $('#extra_price_resume').html($('#total_price_table').html());

    $('#formConfirmBook').submit(function() {
      showProgress();
      var form = $(this);
      $.ajax({
          url         : 'rest_from_js.php',
          data        : form.serialize(),
          success     : function(data) {
              book = JSON.parse(data);
              window.location.href = getLinkGlobal() + "/zion/index.php?controller=booking&action=show&id=" + book.bookId;
          },
          error       : function(errors) {
            hideProgress();
            alert('Error, hubo un problema.');
          }
      });
      return false;
    });

    $('#formProcessBook').submit(function() {
      showProgress();
      var form = $(this);
      $.ajax({
          url         : 'rest_from_js.php',
          data        : form.serialize(),
          success     : function(data) {
              book = JSON.parse(data);
              window.location.href = getLinkGlobal() + "/zion/index.php?controller=booking&action=show&id=" + book.bookId;
          },
          error       : function(errors) {
            hideProgress();
            alert('Error, hubo un problema.');
          }
      });
      return false;
    });
  });

  function showClientDebitNote() {
    var table = $('#total_user_price').html();
    $('#total_price_table').html(table);

    $('#invoice_logo').html($('#invoice_agency_logo').html());
    $('#invoice_header_info').html($('#invoice_agency_info').html());
    $('#operator_remarks').html('');
    $('#price_details').html('')
    $('#conten_price_details').hide()
  }

  function showAgencyDebitNote() {
    var table = $('#total_user_price').html();
    <?php if ($total_agency_fee != 0) { ?>
      table += $('#agency_fee').html();
    <?php } ?>
    table +=  $('#agency_commission').html() +
              $('#total_price_agency').html();
    $('#total_price_table').html(table);

    $('#invoice_logo').html($('#invoice_operator_logo').html());
    $('#invoice_header_info').html($('#invoice_operator_info').html());
    $('#operator_remarks').html('');

  }

  <?php if ($_SESSION['admin_role']) { ?>
    function showOperatorDebitNote() {
        var table = $('#total_user_price').html();
        var operator_commission = $('#operator_commission').html();
        <?php if ($total_agency_fee != 0) { ?>
            table += $('#agency_fee').html();
        <?php } ?>
        table +=    $('#agency_commission').html() +
                    $('#total_price_operator').html() +
                    operator_commission +
                    $('#net_price').html();
        $('#total_price_table').html(table);

        $('#invoice_logo').html($('#invoice_operator_logo').html());
        $('#invoice_header_info').html($('#invoice_operator_info').html());
        $('#operator_remarks').html($('#operator_remarks_raw').html());
        $('#price_details').html('')
        $('#conten_price_details').hide()
    }

  <?php
  }
  if (isset($admin->edit) && $admin->edit) {
  ?>
      var book = {'book_number':'<?php echo $book->booking_code; ?>'}
      $.ajax({
        url : 'helpers/ajax/vitacora_books.php',
        type : 'POST',
        data : book,
        success : function(data) {
          $('#load_content_vitacora').hide();
          $('#content_vitacora').html(data);
        }
      });
<?php } ?>
    function createDebitNoteContable() {
      if (confirm("Se Creara la N.D. Contable. Continuar?")) {
        id    = $('#booking_id').val()
        book  = {'id':id, 'controller':'booking','action':'create_debit_note'}
        showProgress()
        $.ajax({
          url : 'rest_from_js.php',
          type : 'POST',
          data : book,
          success : function(data) {
            data = JSON.parse(data)
            hideProgress()
            if (data.success) {
              location.reload()
            }
            else {
              $('#msg_errors_ajax').html(data.message)
              $('#msg_errors_modal').modal()
            }
          },
          error: function(error) {
            console.log(error)
            hideProgress()
          }
        });
      }

    }
</script>
