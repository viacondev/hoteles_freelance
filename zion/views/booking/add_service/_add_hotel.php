<?php
include_once('../../../../config/Config.php');
include_once('../../../database/DB.php');
include_once('../../../model/Model.php');
include_once('../../../model/Service.php');
include_once('../../../model/Provider.php');
include_once('../../../model/ServiceType.php');
include_once('../../../model/ServiceStatus.php');
include_once('../../../model/HotelRoom.php');
include_once('../../../helpers/application_helper.php');
$s = $_GET['s'];
?>
<div id="add_service_<?php echo $s; ?>" class="add_service gbox">
  <div class="remove_service" onclick="$('#add_service_<?php echo $s; ?>').remove()">
    <i class="fa fa-times-circle"></i>
  </div>
  
  <div class="row">
    <div class="col-md-3 form-group">
      <label>Localizador:</label>
      <input type="text" class="form-control" name="localizer_<?php echo $s; ?>" id="localizer_<?php echo $s; ?>">
    </div>
    <div class="col-md-3">
      <label>Proveedor:</label>
      <select class="form-control" name="provider_<?php echo $s; ?>" id="provider_<?php echo $s; ?>">
        <option value="0"></option>
        <?php
        $providers = Provider::findAll(array('status' => 1));
        foreach ($providers as $provider) {
          echo '<option value="' . $provider->id . '">' . $provider->name . '</option>';  
        } ?>
      </select>
    </div>
    <div class="col-md-3 form-group">
      <label>Nombre hotel:</label>
      <input type="text" class="form-control" name="name_<?php echo $s; ?>" id="name_<?php echo $s; ?>">
    </div>
    <div class="col-md-3 form-group">
      <label>Categor&iacute;a:</label>
      <select class="form-control" name="category_<?php echo $s; ?>" id="category_<?php echo $s; ?>">
        <option value="ALBER">ALBERGUE</option>
        <option value="APTH">APART HOTEL</option>
        <option value="BB">BED AND BREAKFAST</option>
        <option value="1EST">1 ESTRELLA</option>
        <option value="2EST">2 ESTRELLAS</option>
        <option value="3EST">3 ESTRELLAS</option>
        <option value="H3_5">3 ESTRELLAS Y MEDIA</option>
        <option value="4EST" selected="selected">4 ESTRELLAS</option>
        <option value="H4_5">4 ESTRELLAS Y MEDIA</option>
        <option value="5EST">5 ESTRELLAS</option>
        <option value="H5_5">5 ESTRELLAS Y MEDIA</option>
      </select>
    </div>

    <div class="col-md-3 form-group">
      <label for="destination_<?php echo $s; ?>">Destino:</label>
      <input type="text" class="form-control" id="destination_<?php echo $s; ?>" name="destination_<?php echo $s; ?>" placeholder="destino...">
      <input type="hidden" id="destination_code_<?php echo $s; ?>" name="destination_code_<?php echo $s; ?>" />
    </div>

    <div class="col-md-3 form-group">
      <label for="checkin_<?php echo $s; ?>">Entrada:</label>
      <input type="text" class="form-control" id="checkin_<?php echo $s; ?>" name="checkin_<?php echo $s; ?>" placeholder="dd/mm/aa" autocomplete="off">
    </div>

    <div class="col-md-3 form-group">
      <label for="checkout_<?php echo $s; ?>">Salida:</label>
      <input type="text" class="form-control" id="checkout_<?php echo $s; ?>" name="checkout_<?php echo $s; ?>" placeholder="dd/mm/aa" autocomplete="off">
    </div>

    <div class="col-md-3 form-group">
      <label for="cancellation_date_<?php echo $s; ?>">Gastos desde:</label>
      <input type="text" class="form-control" id="cancellation_date_<?php echo $s; ?>" name="cancellation_date_<?php echo $s; ?>" placeholder="dd/mm/aa" autocomplete="off" style="z-index: 100">
    </div>

    <div class="col-md-12">
      <table id="rooms_<?php echo $s; ?>" class="table small" style="margin-bottom: 0;">
        <thead>
          <tr>
            <th>Tipo hab.</th>
            <th>R&eacute;gimen</th>
            <th>Adultos</th>
            <th>Ni&ntilde;os</th>
            <th>Edad ni&ntilde;os</th>
            <th>Precio Neto / Total</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
      <div class="text-right small">
        <div class="link" onclick="addRoom(<?php echo $s; ?>)"><i class="fa fa-plus"></i> Agregar habitaci&oacute;n</div>
      </div>
      <input type="hidden" id="room_count_<?php echo $s; ?>" name="room_count_<?php echo $s; ?>" value="0">
    </div>

    <div id="includes_div_<?php echo $s; ?>" class="col-md-8 form-group collapse">
      <label>Descripci&oacute;n servicio:</label>
      <textarea name="includes_<?php echo $s; ?>" id="includes_<?php echo $s; ?>" class="form-control" rows="3"></textarea>
    </div>
    <input type="hidden" name="is_manual_<?php echo $s; ?>" id="is_manual_<?php echo $s; ?>" value="1">
    <div class="col-md-12">
      <?php
      $service_type = '<i class="fa fa-home"></i> HOTEL';
      include('_price_data.php');
      ?>
    </div>
    <div class="col-md-12" style="margin-top:15px">
      <?php include('_show_data.php'); ?>
    </div>

    <input type="hidden" name="type_<?php echo $s; ?>" id="type_<?php echo $s; ?>" value="<? echo ServiceType::HOTEL; ?>">
    <input type="hidden" name="status_<?php echo $s; ?>" value="<?php echo ServiceStatus::CONFIRMED; ?>">
  </div>
</div>

<?php if (isset($_GET['id'])) {
  $service = Service::findById($_GET['id']);
  ?>
  <input type="hidden" name="service_id_<?php echo $s; ?>" value="<?php echo $service->id; ?>">
  <script>
    <?php foreach ($service->getRooms() as $room) { ?>
      addRoom(<?php echo $s; ?>, <?php echo $room->id; ?>);
    <?php } ?>
  </script>
<?php } ?>