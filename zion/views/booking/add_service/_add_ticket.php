<?php
include_once('../../../../config/Config.php');
include_once('../../../database/DB.php');
include_once('../../../model/Model.php');
include_once('../../../model/Service.php');
include_once('../../../model/Provider.php');
include_once('../../../model/ServiceType.php');
include_once('../../../model/ServiceStatus.php');
include_once('../../../helpers/application_helper.php');
$s = $_GET['s'];
?>
<div id="add_service_<?php echo $s; ?>" class="add_service gbox">
  <div class="remove_service" onclick="$('#add_service_<?php echo $s; ?>').remove()">
    <i class="fa fa-times-circle"></i>
  </div>
  
  <div class="row">
    <div class="col-md-3 form-group">
      <label>Localizador:</label>
      <input type="text" class="form-control" name="localizer_<?php echo $s; ?>" id="localizer_<?php echo $s; ?>">
    </div>
    <div class="col-md-3">
      <label>Proveedor:</label>

      <select class="form-control" name="provider_<?php echo $s; ?>" id="provider_<?php echo $s; ?>">
        <option value="0"></option>
        <?php
        $providers = Provider::findAll(array('status' => 1));
        foreach ($providers as $provider) {
          echo '<option value="' . $provider->id . '">' . $provider->name . '</option>';  
        } ?>
      </select>
    </div>
    <div class="col-md-3 form-group">
      <label>Nombre Ticket:</label>
      <input type="text" class="form-control" name="name_<?php echo $s; ?>" id="name_<?php echo $s; ?>">
    </div>

    <div class="col-md-3 form-group">
      <label for="destination_<?php echo $s; ?>">Destino:</label>
      <input type="text" class="form-control" id="destination_<?php echo $s; ?>" name="destination_<?php echo $s; ?>" placeholder="destino...">
      <input type="hidden" id="destination_code_<?php echo $s; ?>" name="destination_code_<?php echo $s; ?>" />
    </div>

    <div class="col-md-9">
      <div class="row">
        <div class="col-md-3 form-group">
          <label for="checkin_<?php echo $s; ?>">Fecha:</label>
          <input type="text" class="form-control" id="checkin_<?php echo $s; ?>" name="checkin_<?php echo $s; ?>" placeholder="dd/mm/aa" autocomplete="off">
        </div>

        <div class="col-md-3 form-group">
          <label for="adult_count_<?php echo $s; ?>">Adultos:</label>
          <select name="adult_count_<?php echo $s; ?>" id="adult_count_<?php echo $s; ?>" class="form-control">
            <option>0</option><option>1</option><option selected="selected">2</option><option>3</option><option>4</option>
            <option>5</option><option>6</option><option>7</option><option>8</option><option>9</option>
            <option>10</option><option>11</option><option>12</option><option>13</option><option>14</option><option>15</option>
            <option>16</option><option>17</option><option>18</option><option>19</option><option>20</option><option>21</option>
            <option>22</option><option>23</option><option>24</option><option>25</option><option>26</option><option>27</option>
            <option>28</option><option>29</option><option>30</option>
          </select>
        </div>

        <div class="col-md-3 form-group">
          <label for="child_count_<?php echo $s; ?>">Ni&ntilde;os:</label>
          <select name="child_count_<?php echo $s; ?>" id="child_count_<?php echo $s; ?>" class="form-control">
            <option>0</option><option>1</option><option>2</option><option>3</option><option>4</option>
            <option>5</option><option>6</option><option>7</option><option>8</option><option>9</option>
            <option>10</option><option>11</option><option>12</option><option>13</option><option>14</option>
            <option>15</option>
          </select>
        </div>

        <div class="col-md-3 form-group">
          <label for="child_ages_<?php echo $s; ?>">Edad ni&ntilde;os:</label>
          <input type="text" size="3" name="child_ages_<?php echo $s; ?>" id="child_ages_<?php echo $s; ?>" class="form-control" placeholder="Ej: 3, 5">
        </div>
      </div>
    </div>

    <div class="col-md-3 form-group">
      <label for="cancellation_date_<?php echo $s; ?>">Gastos desde:</label>
      <input type="text" class="form-control" id="cancellation_date_<?php echo $s; ?>" name="cancellation_date_<?php echo $s; ?>" placeholder="dd/mm/aa" autocomplete="off" style="z-index: 100">
    </div>

    <div id="includes_div_<?php echo $s; ?>" class="col-md-8 form-group collapse">
      <label>Descripci&oacute;n servicio:</label>
      <textarea name="includes_<?php echo $s; ?>" id="includes_<?php echo $s; ?>" class="form-control" rows="3"></textarea>
    </div>
    <input type="hidden" name="is_manual_<?php echo $s; ?>" id="is_manual_<?php echo $s; ?>" value="1">
    <div class="col-md-12">
      <?php
      $service_type = '<i class="fa fa-ticket"></i> TICKET';
      include('_price_data.php');
      ?>
    </div>
    <div class="col-md-12" style="margin-top:15px">
      <?php include('_show_data.php'); ?>
    </div>
    <input type="hidden" name="type_<?php echo $s; ?>" id="type_<?php echo $s; ?>" value="<?php echo ServiceType::TICKET; ?>">
    <input type="hidden" name="status_<?php echo $s; ?>" value="<?php echo ServiceStatus::CONFIRMED; ?>">
  </div>
</div>