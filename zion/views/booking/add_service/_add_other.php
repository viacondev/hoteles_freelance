<?php
include_once('../../../../config/Config.php');
include_once('../../../database/DB.php');
include_once('../../../model/Model.php');
include_once('../../../model/Service.php');
include_once('../../../model/Provider.php');
include_once('../../../model/ServiceType.php');
include_once('../../../model/ServiceStatus.php');
include_once('../../../helpers/application_helper.php');
$s = $_GET['s'];
?>
<div id="add_service_<?php echo $s; ?>" class="add_service gbox">
  <div class="remove_service" onclick="$('#add_service_<?php echo $s; ?>').remove()">
    <i class="fa fa-times-circle"></i>
  </div>

  <div class="row">
    <div class="col-md-3 form-group">
      <label>Localizador:</label>
      <input type="text" class="form-control" name="localizer_<?php echo $s; ?>" id="localizer_<?php echo $s; ?>">
    </div>
    <div class="col-md-3">
      <label>Proveedor:</label>

      <select class="form-control" name="provider_<?php echo $s; ?>" id="provider_<?php echo $s; ?>">
        <option value="0"></option>
        <?php
        $providers = Provider::findAll(array('status' => 1));
        foreach ($providers as $provider) {
          echo '<option value="' . $provider->id . '">' . $provider->name . '</option>';
        } ?>
      </select>
    </div>

    <div class="col-md-6 form-group">
      <label>Nombre servicio:</label>
      <input type="text" class="form-control" name="name_<?php echo $s; ?>" id="name_<?php echo $s; ?>">
    </div>

    <div class="col-md-8 form-group">
      <label>Descripci&oacute;n servicio:</label>
      <textarea name="includes_<?php echo $s; ?>" id="includes_<?php echo $s; ?>" class="form-control" rows="3"></textarea>
    </div>

    <div class="col-md-4 form-group">
      <label for="cancellation_date_<?php echo $s; ?>">Gastos desde:</label>
      <input type="text" class="form-control" id="cancellation_date_<?php echo $s; ?>" name="cancellation_date_<?php echo $s; ?>" placeholder="dd/mm/aa" autocomplete="off" style="z-index: 100">
    </div>

    <div class="col-md-12">
      <?php
      $service_type = '<i class="fa fa-book"></i> OTRO SERVICIO';
      include('_price_data.php');
      ?>
    </div>

    <div class="col-md-12" style="margin-top:15px">
     <?php include('_show_data.php'); ?>
    </div>

    <input type="hidden" name="type_<?php echo $s; ?>" id="type_<?php echo $s; ?>" value="<?php echo ServiceType::OTHER; ?>">
    <input type="hidden" name="status_<?php echo $s; ?>" value="<?php echo ServiceStatus::CONFIRMED; ?>">
    <input type="hidden" name="is_manual_<?php echo $s; ?>" value="1">
  </div>
</div>
