<?php
include_once('../../../../config/Config.php');
include_once('../../../database/DB.php');
include_once('../../../model/Model.php');
include_once('../../../model/Service.php');
include_once('../../../model/Provider.php');
include_once('../../../model/ServiceType.php');
include_once('../../../model/ServiceStatus.php');
include_once('../../../model/ServiceTransfer.php');
include_once('../../../helpers/application_helper.php');
$s = $_GET['s'];
?>
<div id="add_service_<?php echo $s; ?>" class="add_service gbox">
  <div class="remove_service" onclick="$('#add_service_<?php echo $s; ?>').remove()">
    <i class="fa fa-times-circle"></i>
  </div>
  
  <div class="row">
    <div class="col-md-3 form-group">
      <label>Localizador:</label>
      <input type="text" class="form-control" name="localizer_<?php echo $s; ?>" id="localizer_<?php echo $s; ?>">
    </div>
    <div class="col-md-3">
      <label>Proveedor:</label>

      <select class="form-control" name="provider_<?php echo $s; ?>" id="provider_<?php echo $s; ?>">
        <option value="0"></option>
        <?php
        $providers = Provider::findAll(array('status' => 1));
        foreach ($providers as $provider) {
          echo '<option value="' . $provider->id . '">' . $provider->name . '</option>';  
        } ?>
      </select>
    </div>

    <div class="col-md-3 form-group">
      <label>Nombre Transfer:</label>
      <input type="text" class="form-control" name="name_<?php echo $s; ?>" id="name_<?php echo $s; ?>" value="Transfer">
    </div>

    <div class="col-md-3 form-group">
      <label for="destination_<?php echo $s; ?>">Destino:</label>
      <input type="text" class="form-control" id="destination_<?php echo $s; ?>" name="destination_<?php echo $s; ?>" placeholder="destino...">
      <input type="hidden" id="destination_code_<?php echo $s; ?>" name="destination_code_<?php echo $s; ?>" />
    </div>

    <div class="col-md-5 form-group">
      <label>Desde:</label>
      <input type="text" class="form-control" id="from_<?php echo $s; ?>" name="from_<?php echo $s; ?>" placeholder="Lugar de recogida...">
    </div>

    <div class="col-md-5 form-group">
      <label>Hasta:</label>
      <input type="text" class="form-control" id="to_<?php echo $s; ?>" name="to_<?php echo $s; ?>" placeholder="Lugar de dejada...">
    </div>

    <div class="col-md-2 form-group">
      <label for="checkin_<?php echo $s; ?>">Tipo:</label>
      <select class="form-control" name="transfer_type_<?php echo $s; ?>" id="transfer_type_<?php echo $s; ?>" onchange="changeInOut(<?php echo $s; ?>)">        
        <option value="IN_OUT">IN &amp; OUT</option>
        <option value="IN">IN</option>
        <option value="OUT">OUT</option>
      </select>
    </div>

    <div class="row">
      <div class="col-md-6 form-group">
        <div class="col-md-5 col-xs-5">
          <label>Llegada:</label>
          <input type="text" class="form-control" id="checkin_<?php echo $s; ?>" name="checkin_<?php echo $s; ?>" placeholder="dd/mm/aa" autocomplete="off">
        </div>
        <div class="col-md-4 col-xs-4">
          <label>Hora:</label><br />
          <select name="hourIn_<?php echo $s; ?>" id="hourIn_<?php echo $s; ?>">
            <option value="">hh</option><option>00</option><option>01</option><option>02</option>
            <option>03</option><option>04</option><option>05</option><option>06</option><option>07</option>
            <option>08</option><option>09</option><option>10</option><option>11</option><option>12</option>
            <option>13</option><option>14</option><option>15</option><option>16</option><option>17</option>
            <option>18</option><option>19</option><option>20</option><option>21</option><option>22</option>
            <option>23</option>
          </select>
          <select name="minsIn_<?php echo $s; ?>" id="minsIn_<?php echo $s; ?>">
            <option value="">mm</option><option>00</option><option>05</option><option>10</option>
            <option>15</option><option>20</option><option>25</option><option>30</option><option>35</option>
            <option>40</option><option>45</option><option>50</option><option>55</option>
          </select>
        </div>
        <div class="col-md-3 col-xs-3">
          <label>Vuelo:</label>
          <input type="text" class="form-control" id="flight_in_<?php echo $s; ?>" name="flight_in_<?php echo $s; ?>" placeholder="BA1525">
        </div>
      </div>
      <div class="col-md-6 form-group" style="border-left: 1px solid #ccc;">
        <div id="transfer_out_<?php echo $s; ?>">
          <div class="col-md-5 col-xs-5">
            <label>Salida:</label>
            <input type="text" class="form-control" id="checkout_<?php echo $s; ?>" name="checkout_<?php echo $s; ?>" placeholder="dd/mm/aa" autocomplete="off">
          </div>
          <div class="col-md-4 col-xs-4">
            <label>Hora:</label><br />
            <select name="hourOut_<?php echo $s; ?>" id="hourOut_<?php echo $s; ?>">
              <option value="">hh</option><option>00</option><option>01</option><option>02</option>
              <option>03</option><option>04</option><option>05</option><option>06</option><option>07</option>
              <option>08</option><option>09</option><option>10</option><option>11</option><option>12</option>
              <option>13</option><option>14</option><option>15</option><option>16</option><option>17</option>
              <option>18</option><option>19</option><option>20</option><option>21</option><option>22</option>
              <option>23</option>
            </select>
            <select name="minsOut_<?php echo $s; ?>" id="minsOut_<?php echo $s; ?>">
              <option value="">mm</option><option>00</option><option>05</option><option>10</option>
              <option>15</option><option>20</option><option>25</option><option>30</option><option>35</option>
              <option>40</option><option>45</option><option>50</option><option>55</option>
            </select>
          </div>
          <div class="col-md-3 col-xs-3">
            <label>Vuelo:</label>
            <input type="text" class="form-control" id="flight_out_<?php echo $s; ?>" name="flight_out_<?php echo $s; ?>" placeholder="BA1525">
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-2 form-group">
      <label for="adult_count_<?php echo $s; ?>">Adultos:</label>
      <select class="form-control" name="adult_count_<?php echo $s; ?>" id="adult_count_<?php echo $s; ?>">
        <option>0</option><option>1</option><option selected="selected">2</option><option>3</option><option>4</option>
        <option>5</option><option>6</option><option>7</option><option>8</option><option>9</option>
        <option>10</option><option>11</option><option>12</option><option>13</option><option>14</option><option>15</option>
        <option>16</option><option>17</option><option>18</option><option>19</option><option>20</option><option>21</option>
        <option>22</option><option>23</option><option>24</option><option>25</option><option>26</option><option>27</option>
        <option>28</option><option>29</option><option>30</option>
      </select>
    </div>

    <div class="col-md-2 form-group">
      <label for="child_count_<?php echo $s; ?>">Ni&ntilde;os:</label>
      <select class="form-control" name="child_count_<?php echo $s; ?>" id="child_count_<?php echo $s; ?>">
        <option>0</option><option>1</option><option>2</option><option>3</option><option>4</option>
        <option>5</option><option>6</option><option>7</option><option>8</option><option>9</option>
        <option>10</option><option>11</option><option>12</option><option>13</option><option>14</option>
        <option>15</option>
      </select>
    </div>

    <div class="col-md-2 form-group">
      <label for="child_ages_<?php echo $s; ?>">Edad ni&ntilde;os:</label>
      <input type="text" size="3" name="child_ages_<?php echo $s; ?>" id="child_ages_<?php echo $s; ?>" class="form-control" placeholder="Ej: 3, 5">
    </div>

    <div class="col-md-3 form-group">
      <label for="cancellation_date_<?php echo $s; ?>">Gastos desde:</label>
      <input type="text" class="form-control" id="cancellation_date_<?php echo $s; ?>" name="cancellation_date_<?php echo $s; ?>" placeholder="dd/mm/aa" autocomplete="off" style="z-index: 100">
    </div>

    <div id="includes_div_<?php echo $s; ?>" class="col-md-8 form-group collapse">
      <label>Descripci&oacute;n servicio:</label>
      <textarea name="includes_<?php echo $s; ?>" id="includes_<?php echo $s; ?>" class="form-control" rows="3"></textarea>
    </div>
    <input type="hidden" name="is_manual_<?php echo $s; ?>" id="is_manual_<?php echo $s; ?>" value="1">
    <div class="col-md-12">
      <?php
      $service_type = '<i class="fa fa-suitcase"></i> TRANSFER';
      include('_price_data.php');
      ?>
    </div>

    <div class="col-md-12" style="margin-top:15px">
      <?php include('_show_data.php'); ?>
    </div>
    
    <input type="hidden" name="type_<?php echo $s; ?>" id="type_<?php echo $s; ?>" value="<?php echo ServiceType::TRANSFER; ?>">
    <input type="hidden" name="status_<?php echo $s; ?>" value="<?php echo ServiceStatus::CONFIRMED; ?>">
  </div>
</div>

<?php if (isset($_GET['id'])) {
  $service = Service::findById($_GET['id']);
  $transfer = ServiceTransfer::find(array('service_id' => $service->id));
  $timeIn = $transfer->time_in ? explode(':', $transfer->time_in) : array('', '');
  $timeOut = $transfer->time_out ? explode(':', $transfer->time_out) : array('', '');
  ?>
  <input type="hidden" name="service_id_<?php echo $s; ?>" value="<?php echo $service->id; ?>">
  <script>
    $('#from_<?php echo $s; ?>').val('<?php echo $transfer->origin; ?>');
    $('#to_<?php echo $s; ?>').val('<?php echo $transfer->destination; ?>');
    $('#transfer_type_<?php echo $s; ?>').val('<?php echo $transfer->type; ?>');
    $('#hourIn_<?php echo $s; ?>').val('<?php echo $timeIn[0]; ?>');
    $('#minsIn_<?php echo $s; ?>').val('<?php echo $timeIn[1]; ?>');
    $('#hourOut_<?php echo $s; ?>').val('<?php echo $timeOut[0]; ?>');
    $('#minsOut_<?php echo $s; ?>').val('<?php echo $timeOut[1]; ?>');
    $('#flight_in_<?php echo $s; ?>').val('<?php echo $transfer->flight_in; ?>');
    $('#flight_out_<?php echo $s; ?>').val('<?php echo $transfer->flight_out; ?>');
    changeInOut(<?php echo $s; ?>);
  </script>
<?php } ?>