<table class="small" style="width: 100%; margin-top: 10px;">
  <tr>
    <th></th>
    <th>Precio Neto:</th>
    <th>Fee:</th>
    <th style="white-space: nowrap;">Comisi&oacute;n agencia:</th>
    <th style="white-space: nowrap;">Incentivo:</th>
    <th class="enable_com_<?php echo $s; ?>" style="display: none">Com. Suc.</th>
    <th>Precio total:</th>
    <th>Moneda:</th>
  </tr>
  <tr>
    <td>
      <div class="link" onclick="$('#includes_div_<?php echo $s; ?>').toggle()"><i class="fa fa-plus"></i> Descripcion</div>
      <div class="text-left"><?php echo isset($service_type) ? $service_type : ''; ?></div>
    </td>
    <td><input type="text" id="net_price_<?php echo $s; ?>" name="net_price_<?php echo $s; ?>" size="4" onchange="updateServicePrice(<?php echo $s; ?>)"></td>
    <td><input type="text" id="fee_<?php echo $s; ?>" name="fee_<?php echo $s; ?>" value="0.79" size="4" onchange="updateServicePrice(<?php echo $s; ?>)"></td>
    <td style="white-space: nowrap;">
      <input type="text" id="commission_<?php echo $s; ?>" name="commission_<?php echo $s; ?>" value="0" size="3">
      <input type="radio" id="commission_type_perc_<?php echo $s; ?>" name="commission_type_<?php echo $s; ?>" value="perc" checked="checked">% &nbsp;
      <input type="radio" id="commission_type_value_<?php echo $s; ?>" name="commission_type_<?php echo $s; ?>" value="amount">$
    </td>
    <td>
      <input type="text" id="incentive_<?php echo $s; ?>" name="incentive_<?php echo $s; ?>" value="0" size="3">
      <input type="radio" id="incentive_type_perc_<?php echo $s; ?>" name="incentive_type_<?php echo $s; ?>" value="perc" checked>% &nbsp;
      <input type="radio" id="incentive_type_value_<?php echo $s; ?>" name="incentive_type_<?php echo $s; ?>" value="amount">$
    </td>
    <td class="enable_com_<?php echo $s; ?>" style="display: none">
      <input type="text" id="comis_suc_<?php echo $s; ?>" name="comis_suc_<?php echo $s; ?>" value="6" size="3">
    </td>
    <td><input type="text" id="price_<?php echo $s; ?>" name="price_<?php echo $s; ?>" size="4"></td>
    <td>
      <select name="currency_<?php echo $s; ?>" id="currency_<?php echo $s; ?>">
        <option value="USD">USD</option>
        <option value="BOB">BOB</option>
        <option value="EUR">EUR</option>
      </select>
    </td>
  </tr>
</table>


<?php if (isset($_GET['id'])) {
  $service = Service::findById($_GET['id']);
  ?>
  <input type="hidden" class="service_id_<?php echo $s; ?>" name="service_id_<?php echo $s; ?>" value="<?php echo $service->id; ?>">
  <script>
    $('#is_manual_<?php echo $s; ?>').val("<?php echo $service->is_manual; ?>");
    $('#localizer_<?php echo $s; ?>').val('<?php echo $service->localizer; ?>');
    $('#provider_<?php echo $s; ?>').val('<?php echo $service->provider_id; ?>');
    $('#name_<?php echo $s; ?>').val('<?php echo addslashes($service->name); ?>');
    $('#category_<?php echo $s; ?>').val('<?php echo $service->category; ?>');
    $('#destination_<?php echo $s; ?>').val('<?php echo $service->destination_code; ?>');
    $('#destination_code_<?php echo $s; ?>').val('<?php echo $service->destination_code; ?>');
    $('#adult_count_<?php echo $s; ?>').val('<?php echo $service->adult_count; ?>');
    $('#child_count_<?php echo $s; ?>').val('<?php echo $service->child_count; ?>');
    $('#child_ages_<?php echo $s; ?>').val('<?php echo $service->child_ages; ?>');
    $('#checkin_<?php echo $s; ?>').val('<?php echo dateFormatFromDB($service->checkin); ?>');
    $('#checkout_<?php echo $s; ?>').val('<?php echo dateFormatFromDB($service->checkout); ?>');
    $('#cancellation_date_<?php echo $s; ?>').val('<?php echo dateFormatFromDB($service->cancellation_date); ?>');
    $('#net_price_<?php echo $s; ?>').val('<?php echo $service->net_price; ?>');
    $('#price_<?php echo $s; ?>').val('<?php echo $service->price; ?>');
    <?php  if (isset($service->price) && $service->price != 0) { ?>
          $('#fee_<?php echo $s; ?>').val('<?php echo round($service->net_price / $service->price, 2); ?>');
          $('#commission_<?php echo $s; ?>').val('<?php echo round(($service->commission / $service->price)*100,1); ?>');
          //$('#commission_<?php echo $s; ?>').val('<?php //echo $service->commission; ?>');
          $("#commission_type_perc_<?php echo $s; ?>").prop("checked", true);
          $('#commission_type_value_<?php echo $s; ?>').change(function(){
            $('#commission_<?php echo $s; ?>').val('<?php echo round($service->commission, 2); ?>');
          });
          $("#commission_type_perc_<?php echo $s; ?>").change(function(){
            $('#commission_<?php echo $s; ?>').val('<?php echo round(($service->commission / $service->price)*100,1); ?>');
          });
          $('#incentive_<?php echo $s; ?>').val('<?php echo round(($service->incentive / $service->price) * 100, 1); ?>');
          $('#incentive_type_perc_<?php echo $s; ?>').change(function(){
            $('#incentive_<?php echo $s; ?>').val('<?php echo round(($service->incentive / $service->price) *100 , 1); ?>');
          });
          $('#incentive_type_value_<?php echo $s; ?>').change(function(){
            $('#incentive_<?php echo $s; ?>').val('<?php echo round($service->incentive, 2); ?>');
          });
    <?php  } ?>

    $('#currency_<?php echo $s; ?>').val('<?php echo $service->currency; ?>');
    <?php if ($service->includes != '') { ?>
      include = <?php echo json_encode($service->includes); ?>;
      $('#includes_<?php echo $s; ?>').val(include);
      $('#includes_div_<?php echo $s; ?>').toggle();
    <?php } ?>
  </script>
<?php } ?>
