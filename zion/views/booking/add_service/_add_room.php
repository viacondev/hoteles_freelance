<?php
include_once('../../../../config/Config.php');
include_once('../../../database/DB.php');
include_once('../../../model/Model.php');
include_once('../../../model/HotelRoom.php');

$service_number = isset($_GET['service_number']) ? $_GET['service_number'] : 1;
$room_number = isset($_GET['room_number']) ? $_GET['room_number'] : 1;
$rid = $service_number . '_' . $room_number;
?>
<tr id="room_<?php echo $rid; ?>">
  <td><input type="text" name="type_<?php echo $rid; ?>" id="type_<?php echo $rid; ?>"></td>
  <td><input type="text" name="board_<?php echo $rid; ?>" id="board_<?php echo $rid; ?>"></td>
  <td>
    <select name="adult_count_<?php echo $rid; ?>" id="adult_count_<?php echo $rid; ?>"  style="overflow:auto;">
      <option>0</option><option>1</option><option selected="selected">2</option><option>3</option><option>4</option>
      <option>5</option><option>6</option><option>7</option><option>8</option><option>9</option>
      <option>10</option><option>11</option><option>12</option><option>13</option><option>14</option><option>15</option>
      <option>16</option><option>17</option><option>18</option><option>19</option><option>20</option><option>21</option>
      <option>22</option><option>23</option><option>24</option><option>25</option><option>26</option><option>27</option>
      <option>28</option><option>29</option><option>30</option>
    </select>
  </td>
  <td>
    <select name="child_count_<?php echo $rid; ?>" id="child_count_<?php echo $rid; ?>">
      <option>0</option><option>1</option><option>2</option><option>3</option><option>4</option>
      <option>5</option><option>6</option><option>7</option><option>8</option><option>9</option>
      <option>10</option><option>11</option><option>12</option><option>13</option><option>14</option>
      <option>15</option>
    </select>
  </td>
  <td><input type="text" size="3" name="child_ages_<?php echo $rid; ?>" id="child_ages_<?php echo $rid; ?>" placeholder="Ej: 3, 5"></td>
  <td><input type="text" size="3" id="net_price_<?php echo $rid; ?>" name="net_price_<?php echo $rid; ?>" onchange="updateRoomPrice(<?php echo $service_number; ?>, <?php echo $room_number; ?>);" placeholder="Neto"> / 
  <input type="text" size="3" id="price_<?php echo $rid; ?>" name="price_<?php echo $rid; ?>" onchange="updateRoomNetPrice(<?php echo $service_number; ?>, <?php echo $room_number; ?>);" placeholder="Total"></td>
  <td><div onclick="$('#room_<?php echo $rid; ?>').remove()" style="color: #cd0200; cursor: pointer;"><i class="fa fa-times-circle"></i></div></td>
</tr>

<?php if (isset($_GET['id'])) {
  $room = HotelRoom::findById($_GET['id']);
  ?>
  <input type="hidden" name="room_id_<?php echo $rid; ?>" value="<?php echo $room->id; ?>">
  <script>
    $('#type_<?php echo $rid; ?>').val('<?php echo $room->type; ?>');
    $('#board_<?php echo $rid; ?>').val('<?php echo $room->board; ?>');
    $('#adult_count_<?php echo $rid; ?>').val('<?php echo $room->adult_count; ?>');
    $('#child_count_<?php echo $rid; ?>').val('<?php echo $room->child_count; ?>');
    $('#child_ages_<?php echo $rid; ?>').val('<?php echo $room->childs_ages; ?>');
    $('#net_price_<?php echo $rid; ?>').val('<?php echo $room->net_price; ?>');
    $('#price_<?php echo $rid; ?>').val('<?php echo $room->price; ?>');
  </script>
<?php } ?>