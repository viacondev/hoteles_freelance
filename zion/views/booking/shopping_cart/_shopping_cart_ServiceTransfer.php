<?php
$serviceInfo = $service->getServiceInfo();
$transferType_str = $serviceInfo->getTransferType() == 'IN' ? 'Llegada' : 'Salida';
?>
<div class="col-md-12 service transfer-service wbox">
  <div class="service-type service-type-transfer">
    <i class="fa fa-suitcase"></i> Traslado (<?php echo $transferType_str; ?>)
  </div>
  <div class="service-info">
    <span class="service-title"><?php echo $serviceInfo->getName() ?></span>
    <div class="service-locations">
      Desde: <strong><?php echo $serviceInfo->getPickupLocationName(); ?></strong>, 
      Hasta: <strong><?php echo $serviceInfo->getDestLocationName(); ?></strong>
    </div>
    <div class="service-dates">
      Fecha recogida: <strong><?php echo dateToDDMMYY($serviceInfo->getDateFrom()); ?></strong>, 
      Hora de <?php echo $transferType_str; ?> (vuelo, bus, tren): <strong><?php echo $serviceInfo->getTimeFrom(); ?></strong>
    </div>
  </div>
  <hr />

  <div class="row transfer-info">
    <div class="col-md-9 col-xs-9">
      <?php
      echo $serviceInfo->getAdultCount() . ' adultos';
      if ($serviceInfo->getChildCount() != 0) {
        echo ', ' . $serviceInfo->getChildCount() . ' ni&ntilde;os';
      }
      ?>
    </div>
    <div class="col-md-3 col-xs-3 text-right strong">
      <?php echo Agency::getUserPrice($serviceInfo->getTotalAmount()) . ' ' . $serviceInfo->getCurrency(); ?>
    </div>
  </div>
  <hr />

  <div class="contract-info">
    <?php
    if ($serviceInfo->getContractComment()) {
      echo '<div><strong>Observaciones del contrato</strong></div>';
      echo '<p>' . $serviceInfo->getContractComment() . '</p>';
      echo '<hr />';
    }
    ?>
  </div>

  <div class="service-detail confirm_purchase row">
    <strong>N&uacute;mero de vuelo</strong> <small style="text-muted">(En caso de tren, bus o barco, añade una referencia)</small>:
    <input type="text" name="serviceTravelNumber_<?php echo $service_count; ?>" />
  </div>

  <div class="policies">
    <?php
    foreach ($serviceInfo->getCancellationPolicies() as $policy) {
      $amount = getPriceWithFee($policy->getAmount());
      echo '<div class="text-danger"><strong>';
      echo 'Si cancelas despu&eacute;s de las ' . $policy->getTime() . 'hrs del ' . $policy->getDateFrom() . ' se aplicar&aacute;n gastos de: ' . $amount . ' ' . $serviceInfo->getCurrency();
        echo '. Si la reserva no es re-confirmada hasta dicha fecha &eacute;sta se cancelar&aacute; autom&aacute;nticamente.';
      echo '</strong></div>';
      echo '<small class="text-muted">Las horas y fechas se calculan en base al horario local en el pa&iacute;s de destino</small>';
    }
    ?>
  </div>
  <hr />

  <div class="total-price text-right">
    Total Precio Neto: 
    <span class="total-price-amount"><?php echo Agency::getUserPrice($service->getTotalAmount()) . ' ' . $serviceInfo->getCurrency(); ?></span>
  </div>

  <div class="remove-service text-right small">
    <div class="link" onclick="removeService('<?php echo $rs->getPurchaseToken(); ?>', '<?php echo $service->getSPUI(); ?>')">
      Eliminar Producto
    </div>
  </div>
</div>