<?php
$hotelAvail = $service->getServiceInfo();
$rooms = $hotelAvail->getRoomsAvail();
?>
<div class="col-md-12 service hotel-service wbox">
  <div class="service-type service-type-hotel"><i class="fa fa-home"></i> Alojamiento</div>
  <div class="service-info">
    <span class="service-title"><?php echo $hotelAvail->getName() ?></span> &nbsp;
    <span class="service-category" data-toggle="tooltip" data-placement="top" data-original-title="<?php echo $hotelAvail->getCategory(); ?>">
      <?php showCategory($hotelAvail->getCategoryCode()); ?>
    </span>
    <div class="service-dates">
      Entrada: <strong><?php echo dateToDDMMYY($service->getDateFrom()); ?></strong> - 
      Salida: <strong><?php echo dateToDDMMYY($service->getDateTo()); ?></strong>
    </div>
    <div class="rooms-info">
      <table class="table">
        <thead>
          <tr>
            <th>Tipo de habitaci&oacute;n</th>
            <th>R&eacute;gimen</th>
            <th>Ocupaci&oacute;n</th>
            <th>Precio</th>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach ($rooms as $room) {
            ?>
            <tr>
              <td><?php echo $room->getRoomType() . ' x ' . $room->getRoomCount(); ?></td>
              <td><?php echo $room->getBoard() ?></td>
              <td>
                <?php
                $adult_str = $room->getAdultCount() > 1 ? ' adultos' : ' adulto';
                echo $room->getAdultCount() . $adult_str;
                if ($room->getChildCount() > 0) {
                  $child_str = $room->getChildCount() > 1 ? ' ni&ntilde;os' : ' ni&ntilde;o';
                  echo ' ' . $room->getChildCount() . $child_str;
                }
                ?>
              </td>
              <td><strong><?php echo Agency::getUserPrice($room->getPrice()) . ' ' . $hotelAvail->getCurrency(); ?></strong></td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <hr />

  <div class="contract-info">
    <?php
    if ($hotelAvail->getContractComment()) {
      echo '<div><strong>Observaciones del contrato</strong></div>';
      echo '<p>' . $hotelAvail->getContractComment() . '</p>';
      echo '<hr />';
    }
    ?>
  </div>

  <div class="policies">
    <?php
    foreach ($rooms as $room) {
      echo '<div class="policy">';
      foreach ($room->getCancellationPolicies() as $policy) {
        $amount = getPriceWithFee($policy->getAmount());
        echo '<div>' . $room->getRoomType() . ' x ' . $room->getRoomCount() . '</div>';
        echo '<div class="text-danger"><strong>';
        echo 'Si cancelas despu&eacute;s de las ' . $policy->getTime() . 'hrs del ' . $policy->getDateFrom() . ' se aplicar&aacute;n gastos de: ' . $amount . ' ' . $hotelAvail->getCurrency();
        echo '. Si la reserva no es re-confirmada hasta dicha fecha &eacute;sta se cancelar&aacute; autom&aacute;nticamente.';
        echo '</strong></div>';
      }
      echo '</div>';
    }
    ?>
  </div>
  <hr />

  <div class="total-price text-right">
    Total Precio Neto: 
    <span class="total-price-amount"><?php echo Agency::getUserPrice($service->getTotalAmount()) . ' ' . $hotelAvail->getCurrency(); ?></span>
  </div>

  <div class="remove-service text-right small">
    <div class="link" onclick="removeService('<?php  ?>', '<?php echo $service->getSPUI(); ?>')">
      Eliminar Producto
    </div>
  </div>
</div>