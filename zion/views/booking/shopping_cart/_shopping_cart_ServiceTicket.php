<?php
$serviceInfo = $service->getServiceInfo();
?>
<div class="col-md-12 service hotel-service wbox">
  <div class="service-type service-type-ticket"><i class="fa fa-ticket"></i> Entrada</div>
  <div class="service-info">
    <span class="service-title"><?php echo $serviceInfo->getName() ?></span> &nbsp;
    <div class="service-dates">
      Entrada: <strong><?php echo dateToDDMMYY($service->getDateFrom()); ?></strong>
    </div>
  </div>
  <hr />

  <div class="row tickets-info">
    <div class="col-md-12"><strong>Entradas:</strong></div>
    <div class="col-md-9 col-xs-9">
      <?php
      echo $serviceInfo->getAdultCount() . ' adultos';
      if ($serviceInfo->getChildCount() != 0) {
        echo ', ' . $serviceInfo->getChildCount() . ' ni&ntilde;os';
      }
      echo ' (' . $serviceInfo->getName() . ')' ;
      ?>
    </div>
    <div class="col-md-3 col-xs-3 text-right">
      <?php
      $availMode = $serviceInfo->getAvailableModalityList();
      $availMode = $availMode[0];
      foreach ($availMode->getPriceList() as $price) {
        if ($price->getDescription() == 'SERVICE PRICE' || $price->getDescription() == 'Total Price') {
          echo '<strong>' . Agency::getUserPrice($price->getAmount()) . ' ' . $serviceInfo->getCurrency() . '</strong>';
          break;
        }
      }
      ?>
    </div>
  </div>
  <hr />

  <div class="contract-info">
    <?php
    if ($serviceInfo->getContractComment()) {
      echo '<div><strong>Observaciones del contrato</strong></div>';
      echo '<p>' . $serviceInfo->getContractComment() . '</p>';
      echo '<hr />';
    }
    ?>
  </div>

  <div class="service-detail confirm_purchase row">
    <?php
    if (count($serviceInfo->getServiceDetailList()) > 0) {
      echo '<div class="col-md-12"><strong>Informaci&oacute;n requerida</strong></div>';
    }
    $serviceDetailCount = 0;
    foreach ($serviceInfo->getServiceDetailList() as $serviceDetail) {
      $serviceDetailCount++;
      ?>
      <div class="col-md-12" style="margin-bottom: 15px;">
        <?php echo $serviceDetail->getName(); ?>
        <div style="margin-left: 20px;">
          - &nbsp; <input type="text" name="serviceDetail_<?php echo $service_count . '_' . $serviceDetailCount ?>" />
          <input type="hidden" name="serviceDetailName_<?php echo $service_count . '_' . $serviceDetailCount ?>" value="<?php echo $serviceDetail->getName(); ?>" />
          <input type="hidden" name="serviceDetailCode_<?php echo $service_count . '_' . $serviceDetailCount ?>" value="<?php echo $serviceDetail->getCode(); ?>" />
        </div>
      </div>
      <?php
    }
    ?>
    <input type="hidden" name="serviceDetailCount_<?php echo $service_count; ?>" value="<?php echo $serviceDetailCount; ?>" />
  </div>

  <div class="policies">
    <?php
    foreach ($serviceInfo->getCancellationPolicies() as $policy) {
      $amount = getPriceWithFee($policy->getAmount());
      echo '<div class="text-danger"><strong>';
      echo 'Si cancelas despu&eacute;s de las ' . $policy->getTime() . 'hrs del ' . $policy->getDateFrom() . ' se aplicar&aacute;n gastos de: ' . $amount . ' ' . $serviceInfo->getCurrency();
        echo '. Si la reserva no es re-confirmada hasta dicha fecha &eacute;sta se cancelar&aacute; autom&aacute;nticamente.';
      echo '</strong></div>';
      echo '<small class="text-muted">Las horas y fechas se calculan en base al horario local en el pa&iacute;s de destino</small>';
    }
    ?>
  </div>
  <hr />

  <div class="total-price text-right">
    Total Precio Neto: 
    <span class="total-price-amount"><?php echo Agency::getUserPrice($service->getTotalAmount()) . ' ' . $serviceInfo->getCurrency(); ?></span>
  </div>

  <div class="remove-service text-right small">
    <div class="link" onclick="removeService('<?php echo $rs->getPurchaseToken(); ?>', '<?php echo $service->getSPUI(); ?>')">
      Eliminar Producto
    </div>
  </div>
</div>