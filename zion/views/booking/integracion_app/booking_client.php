<?
	// include('../config/Config.php');
	// include('database/DB.php');
	// include('model/Model.php');
	include('model/BookingUser.php');

	$booking_user = BookingUser::findAll(array('booking_id' => $book->id));
?>
<div class="col-md-12">
	<div class="col-md-12">
		<h3 style="border-bottom: 1px solid #aaa;"><span style='border-color:blue'>Clientes Asociados de Viacon</span></h3>
		<div class="col-md-12" style="margin-bottom:10px;padding-left: 0px;">
			<a class="text-right btn btn-info-css btn-sm" onclick="agregar_cliente();" style="cursor:pointer"><i class="fa fa-plus"></i> Agregar</a>
			<a class="text-right btn btn-info-css btn-sm" onclick="guardar_cliente();" style="cursor:pointer;margin-left:10px;"><i class="fa fa-floppy-o"></i> Guardar</a>
			<a class="text-right btn btn-info-css btn-sm" onclick="send_booking()" style="cursor:pointer;margin-left:10px;"><i class="fa fa-bell"></i> Notificar</a>
		</div>
	</div>
	<div class="col-md-12">
		<table class="table" id="tabla_clientes">
			<tr style="background-color: #474949;color: white;opacity: 0.8;">
				<th>Origen</th>
				<th>Nombre</th>
				<th>Acci&oacute;n</th>
				<th>Notificaci&oacute;n</th>
			</tr>
			<? $con = 1;
				foreach ($booking_user as  $user) {
			?>
				<tr id="fil_<? echo $con; ?>">
					<td>
						<i class='fa <? echo ($user->user_id != '') ? 'fa-mobile' : 'fa-users' ?>' style='<? echo ($user->user_id != '') ? 'font-size:20px' : '' ?>'></i>
					</td>
					<td>
						<input type="hidden"
									 name="id_cliente_<? echo $con; ?>"
									 id="id_cliente_<? echo $con; ?>"
									 class='codigo_cliente'
									 value="<? echo ($user->user_id != '') ? $user->user_id : $user->id_cliente; ?>"
									 data-user="<?  echo ($user->user_id != '') ? 'user_id' : 'id_cliente'; ?>"
									 data-name="<? echo $user->nombre_apellido; ?>" ><? echo $user->nombre_apellido; ?>
					</td>
					<td>
						<? if($user->user_id == '') { ?>
								<img src="http://viacontours.com/pqt/interfaces/images/cross.png" style="cursor:pointer" onclick="eliminar_cliente(<? echo $con; ?>)">
						<? } ?>
					</td>
					<td class="notificacion id_cliente_<? echo $con; ?>" >
					</td>
				</tr>
			<? $con++;
				}
			?>
		</table>
	</div>

</div>

<script type="text/javascript">
	contador  = '<? echo $con; ?>';

	function agregar_cliente() {
		html = "<tr id='fil_" + contador + "'>" +
							"<td>" +
								"<i class='fa fa-users'></i>" +
							"</td>" +
							"<td>" +
								"<input type='hidden' name='id_cliente_" + contador + "' class='codigo_edit' id='id_cliente_" + contador + "' value=''>" +
								"<input type='text' name='" + contador + "' data-codigo='" + contador + "' class='name_cliente' id='name_cliente_" + contador + "'>" +
							"</td>" +
							"<td>" +
								"<img src='http://viacontours.com/pqt/interfaces/images/cross.png' style='cursor:pointer' onclick='eliminar_cliente(" + contador + ")''>" +
							"</td>" +
							"<td class='notificacion id_cliente_" + contador + "'>" +
							"</td>" +
						"</tr>";
		$('#tabla_clientes').append(html);

		$('#name_cliente_'+contador).autocomplete({
      	source: function( request, response ) {
      	$.ajax({
	          url: "views/Pqt_Integracion/conexion_cotizacion.php",
	          dataType: "json",
	          data: {selector : "Buscar_cliente", tipo_client : '1' ,term: request.term},
	          success: function(data) {
	              response($.map(data, function(item) {
	                  return {
	                      label: item.label,
	                      value: item.value,
	                      id_cliente: item.id_cliente
                    };
	              }));
              }
          });
	      },
	      minLength: 4,
	      select: function(event, ui) {
	        $('#id_cliente_'+event.target.name).val(ui.item.id_cliente);
	      }
    	});

    	contador++;
	}

	function eliminar_cliente(codigo) {
		if($('#id_cliente_' + codigo).val() != '' && $('#name_cliente_' + codigo).length == 0) {
			id_cliente = $('#id_cliente_'+codigo).val();
			booking_id = $('#booking_id').val();
			$.ajax({
				url : "helpers/ajax/save_booking_user.php",
				dataType : "html",
    			type : "post",
				data : { selector : "eliminar_cliente", id_cliente : id_cliente, booking_id : booking_id},
				success : function(data) {

				}
			});
		}
		$('#fil_' + codigo).remove();
	}

	function guardar_cliente() {
		var id_clientes = $('input[class = codigo_edit]');
		var name_clientes  = $('.name_cliente');
		for (var i = id_clientes.length - 1; i >= 0; i--) {
			if($(id_clientes[i]).val() != '' && $(name_clientes[i]).val() != '') {
				id_cliente     = $(id_clientes[i]).val();
				nombre_cliente = $(name_clientes[i]).val();
				booking_id     = $('#booking_id').val();
				$.ajax({
					url : "helpers/ajax/save_booking_user.php",
					dataType : "html",
					type : "post",
					data : { selector : "guardar_clientes", id_cliente : id_cliente, nombre_cliente : nombre_cliente, booking_id : booking_id},
					success : function(data) {

					}
				});
				var td   = $(name_clientes[i]).parent();
				var html = "<input type='hidden' name='id_cliente_"+$(name_clientes[i]).data('codigo')+"' id='id_cliente_"+$(name_clientes[i]).data('codigo')+"' class='codigo_cliente' value='"+$(id_clientes[i]).val()+"' data-user='id_cliente' data-name='" + nombre_cliente + "'>"+nombre_cliente;
				$(td).html(html);
			}
			else{
				$('#fil_' + $(name_clientes[i]).data('codigo')).remove();
			}
		};

	}

	function enviar_notificacion() {
		var id_clientes = $('input[class = codigo_cliente]');
		for (var i = id_clientes.length - 1; i >= 0; i--) {
			var id = id_clientes[i];
		};
	}

	function send_booking() {
	    var book_information = new Array();
	    $('.notificacion').html('<img src="assets/images/ajax-loader.gif">');
	    id = '<? echo $book->id; ?>';
	    var clientes  = $('.codigo_cliente');
	    var book_send =  JSON.stringify(book_information);
	    user_send     = new Array();
	    for(i = 0; i < clientes.length; i++) {
	      codigo_    = $(clientes[i]).val();
	      id_control = $(clientes[i]).attr("id");
	      type_user  = $(clientes[i]).data("user");
	      name_user  = $(clientes[i]).data("name");
	      user_send.push(new Array(type_user, codigo_, id_control));
	    }

      $.ajax({
         url      : 'helpers/ajax/send_book.php',
         datatype : 'html',
         type     : 'post',
         data : {id : id, users : JSON.stringify(user_send)},
         success: function(data) {
         	$('.notificacion').html('');
         	var result_ = JSON.parse(data);
         	for(i = 0; i < result_.length; i++) {
         		if(result_[i]['si'] != '') {
	         		si = "<img src='http://barrybolivia.com/zion/assets/images/check-circle.png' data-toggle='tooltip' data-placement='top' title='"+result_[i]['si']+"'>";
	         		$('.' + result_[i]['id_control']).append(si);
	         	}
	         	if(result_[i]['no'] != '') {
	         		no = "<img src='http://barrybolivia.com/zion/assets/images/check-fail.png' data-toggle='tooltip' data-placement='top' title='"+result_[i]['no']+"'>";
	         		$('.' + result_[i]['id_control']).append(no);
	         	}
         	}
         }
       });
	}
</script>
