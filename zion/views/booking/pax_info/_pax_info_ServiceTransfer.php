<?php
$serviceInfo = $service->getServiceInfo();
?>
<span class="hotel-title"><?php echo $serviceInfo->getName(); ?></span>
<div class="row">
  <div class="col-md-3 col-xs-3"></div>
  <div class="col-md-3 col-xs-3">Nombre</div>
  <div class="col-md-3 col-xs-3">Apellido</div>
  <div class="col-md-3 col-xs-3"></div>
</div>
<?php
$i = 0;
foreach ($serviceInfo->getGuestList() as $customer) {
  $i++;
  ?>
  <div class="row <?php echo $i != 1 ? 'secondary_pax' : '' ?>" style="margin-top: 5px;">
    <?php
    $pax_count_str = $i == 1 ? 'principal' : '#' . $i;
    ?>
    <div class="col-md-3 col-xs-3 text-right small">Datos pasajero <?php echo $pax_count_str; ?></div>
    <div class="col-md-3 col-xs-3"><input name="customerName_<?php echo $service_count . '_' . $i; ?>" /></div>
    <div class="col-md-3 col-xs-3"><input name="customerLastName_<?php echo $service_count . '_' . $i; ?>" /></div>
    <?php $pax_type_str = $customer->getType() == 'CH' ? 'Ni&ntilde;o' : 'Adulto'; ?>
    <div class="col-md-3 col-xs-3"><small class="text-muted"><?php echo $pax_type_str; ?></small></div>
    <input type="hidden" name="customerType_<?php echo $service_count . '_' . $i; ?>" value="<?php echo $customer->getType() ?>" />
    <input type="hidden" name="customerAge_<?php echo $service_count . '_' . $i; ?>" value="<?php echo $customer->getAge() ?>" />
    <input type="hidden" name="CustomerId_<?php echo $service_count . '_' . $i; ?>" value="<?php echo $customer->getCustomerId() ?>" />
  </div>
  <?php
}
if ($service_count == 1 && count($rs->getBookingServices()) > 1) {
  ?>
  <div class="row" style="margin-top: 5px;">
    <div class="col-md-9 col-md-offset-3 small">
      <a href="#" onclick="copyFirstCustomerData(<?php echo count($rs->getBookingServices()); ?>, <?php echo $i; ?>); return false;">Copiar datos a las siguientes reservas</a>
    </div>
  </div>
  <?php
}
?>
<hr />
<input type="hidden" name="customerCount_<?php echo $service_count; ?>" value="<?php echo $i; ?>" />
<input type="hidden" name="SPUI_<?php echo $service_count; ?>" value="<?php echo $service->getSPUI(); ?>" />