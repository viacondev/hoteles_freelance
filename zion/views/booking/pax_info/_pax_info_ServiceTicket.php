<?php
$serviceInfo = $service->getServiceInfo();
?>

<div class="row">
  <div class="col-md-12">
    <span class="hotel-title"><?php echo $serviceInfo->getName(); ?></span>
    <table class="table small">
      <thead>
        <tr>
          <th></th>
          <th>Nombre</th>
          <th>Apellido</th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <?php
        $i = 0;
        foreach ($serviceInfo->getGuestList() as $customer) {
          $i++;
          ?>
          <tr class="<?php echo $i != 1 ? 'secondary_pax' : '' ?>">
            <?php
            $pax_count_str = $i == 1 ? 'principal' : '#' . $i;
            ?>
            <td>Datos pasajero <?php echo $pax_count_str; ?></td>
            <td><input name="customerName_<?php echo $service_count . '_' . $i; ?>" /></td>
            <td><input name="customerLastName_<?php echo $service_count . '_' . $i; ?>" /></td>
            <?php $pax_type_str = $customer->getType() == 'CH' ? 'Ni&ntilde;o' : 'Adulto'; ?>
            <td><small class="text-muted"><?php echo $pax_type_str; ?></small></td>
            <td></td>
            <input type="hidden" name="customerType_<?php echo $service_count . '_' . $i; ?>" value="<?php echo $customer->getType() ?>" />
            <input type="hidden" name="customerAge_<?php echo $service_count . '_' . $i; ?>" value="<?php echo $customer->getAge() ?>" />
            <input type="hidden" name="CustomerId_<?php echo $service_count . '_' . $i; ?>" value="<?php echo $customer->getCustomerId() ?>" />
          </div>
          <?php
        }
        if ($service_count == 1 && count($rs->getBookingServices()) > 1) {
          ?>
          <div class="row" style="margin-top: 5px;">
            <div class="col-md-9 col-md-offset-3 small">
              <a href="#" onclick="copyFirstCustomerData(<?php echo count($rs->getBookingServices()); ?>, <?php echo $i; ?>); return false;">Copiar datos a las siguientes reservas</a>
            </div>
          </div>
          <?php
        }
        ?>
      </tbody>
    </table>
  </div>
</div>

<input type="hidden" name="customerCount_<?php echo $service_count; ?>" value="<?php echo $i; ?>" />
<input type="hidden" name="SPUI_<?php echo $service_count; ?>" value="<?php echo $service->getSPUI(); ?>" />