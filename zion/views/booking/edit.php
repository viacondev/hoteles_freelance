<?php global $book, $agencies;$select="edit"; ?>
<?php
  $name       = $book->holder;
  $last_name  = '';
  if (!empty($book->name))
    $name       = $book->name;
    $last_name  = $book->last_name;
?>
<div class="content">
  <div class="container">
    <div class="col-md-3"><?php include('views/admin/_menu.php'); ?></div>

    <div class="col-md-9 wbox">
      <div class="pull-right" style="margin-right: 15px;">
        <a href="?controller=booking&amp;action=show&amp;id=<?php echo $book->id; ?>&amp;request_action=show_book">&#8592; Volver</a>
      </div>
      <h2>Editar nota de confirmaci&oacute;n A<?php echo $book->debit_note; ?></h2>
      <?php include('_form.php'); ?>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="ModalVoucher" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Voucher</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>



<script type="text/javascript">
  // Restore values
  $(function() {
    $('#name').val('<?php echo $name; ?>');
    $('#last_name').val('<?php echo $last_name; ?>');
    $('#agency_id').val('<?php echo $book->agency_id; ?>');
    $('#estado').append('<?php echo Formar_Stados($book->status); ?>');
    <?php  if ($book->view_mode == 2) { ?>
          $('#package_container').show();
    <?php  } ?>
    $('#estado_id').val('<?php echo $book->status ?>');
    $('#view_mode option[value='+ <?php echo $book->view_mode ?> +']').attr("selected",true);
    <?php if ($book->view_mode == 2) { ?>
          description = <?php echo json_encode($book->package_description); ?>;
          rows1 =description.split('\n');
          $('#package_description').attr("rows", rows1.length);
          $('#package_description').val(description);
    <?php } ?>
    loadAgencyUsers('<?php echo $book->agency_id; ?>', '<?php echo $book->user_id; ?>');
    <?php if (!$book->agency_id) { ?>
      $('#manual_agency').prop('checked', true);
      $('#agency').show();
      $('#agency').val('<?php echo $book->agency; ?>');
    <?php } ?>
    <?php if (!$book->user_id) { ?>
      $('#manual_agent').prop('checked', true);
      $('#agent').show();
      $('#agent').val('<?php echo $book->agent; ?>');
    <?php } ?>
    <?php
    foreach ($book->getServices() as $service) {
      $id = $service->id;
      switch ($service->type) {
        case ServiceType::HOTEL:
          echo "addService('hotel', $id);";
          break;
        case ServiceType::TICKET:
          echo "addService('ticket', $id);";
          break;
        case ServiceType::TRANSFER:
          echo "addService('transfer', $id);";
          break;
        case ServiceType::FLIGHT:
          echo "addService('flight', $id);";
          break;
        case ServiceType::OTHER:
          echo "addService('other', $id);";
          break;
        default:
          # code...
          break;
      }
    }
    ?>
  });
</script>
