<?php global $bookings; ?>

<div class="container">
  <?php
  foreach ($bookings as $b) {
    ?>
    <div class="booking row">
      <div class="col-md-2">
        <strong>Localizador:</strong> <?php echo $b->localizer; ?>
      </div>
      <div class="col-md-2">
        <strong>Agente:</strong> <?php echo $b->user_id; ?>
      </div>
      <div class="col-md-3">
        <strong>Pasajero:</strong> <?php echo $b->holder; ?>
      </div>
      <div class="col-md-3">
        <strong>Gastos de cancelaci&oacute;n desde:</strong> <?php echo $b->cancellation_date; ?>
      </div>
      <div class="col-md-2">
        <strong>Estado:</strong> <?php echo $b->status; ?>
      </div>

      <div class="services col-md-8">
        <table class="table table-striped table-hover">
          <thead>
            <tr>
              <th>Servicio</th>
              <th>Entrada</th>
              <th>Salida</th>
              <th>Precio</th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach ($b->getServices() as $service) {
              echo '<tr>';
              echo '<td>' . $service->type . ' ' . $service->name . '</td>';
              echo '<td>' . $service->checkin . '</td>';
              echo '<td>' . $service->checkout . '</td>';
              echo '<td>' . $service->price . ' ' . $service->currency . '</td>';
              echo '</tr>';
            }
            ?>
          </tbody>
        </table> 
      </div>

      <div class="booking-resume col-md-12 text-right">
        <strong>Precio Total:</strong> <?php echo $b->price; ?>
      </div>

      <div class="booking-actions col-md-12 text-right">
        <span><button>Bonos y Nota de debito</button></span>
        <span><button>Cancelar</button></span>
        <span><button>Modificar</button></span>
      </div>
    </div>
    <?php
  }
  ?>
</div>
