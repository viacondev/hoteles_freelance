
<form id="add_book_form" method="get" enctype="multipart/form-data">
  <div class="form-group col-md-3">
    <label for="holder">Pasajero principal:</label>
    <input type="text" class="form-control" id="name" name="name" placeholder="Nombre"><br/>
    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Apellido">
  </div>

  <div class="form-group col-md-3">
    <label>Agencia:</label>
    <select class="form-control" name="agency_id" id="agency_id" onchange="loadAgencyUsers(value);">
      <option value="0"></option>
      <?php foreach ($agencies as $agency) {
        echo '<option value="' . $agency->id . '">'. $agency->name . '</option>';
      } ?>
    </select>
    <!-- <input type="checkbox" name="manual_agency" id="manual_agency" value="1" onclick="toggleManualAgency()" tabindex="-1"> Introducir agencia manualmente
    <input type="text" name="agency" id="agency" class="form-control" style="display: none;" placeholder="Nombre de agencia..."> -->
  </div>

  <div class="form-group col-md-3">
    <label>Agente:</label>
    <select class="form-control" name="user_id" id="user_id"></select>
    <!-- <input type="checkbox" name="manual_agent" id="manual_agent" value="1" onclick="toggleManualAgent()" tabindex="-1"> Introducir agente manualmente
    <input type="text" name="agent" id="agent" class="form-control" style="display: none;" placeholder="Nombre de agente..."> -->
  </div>

  <div class="form-group col-md-3">
    <label>Estado Reserva:</label>
    <input type="hidden" id="estado_id" name="estado_id" value="">
    <select class="form-control" name="estado" id="estado">
    </select>
  </div>

  <div class="clearfix"></div>

  <div class="form-group col-md-3">
    <label>Vista :</label>
    <select class="form-control" name="view_mode" id="view_mode" onchange="javascript: if(this.value==2) { $('#package_container').show();}else{ $('#package_container').hide();}">
      <option value="0">Desplegado</option>
      <option value="1">Desplegado sin Precio</option>
      <option value="2">Paquete</option>
    </select>
  </div>

  <div id="services" class="col-md-12"></div>

  <div id="package_container" class="collapse">
    <div class="form-group col-md-12">
      <label>Descripci&oacute;n de Paquete</label>
      <textarea class="form-control" name="package_description" id="package_description" rows="3" ></textarea>
    </div>

    <div class="form-group col-md-3" style="margin-top: 2.5%;">
      <button class="btn btn-info" name="generate_package" id="generate_package" onclick="return false;">Generar Paquete</button>
    </div>
  </div>

  <div class="clearfix"></div>

  <div id="remarks_container" class="collapse">
    <div class="form-group col-md-6">
      <label for="remarks">Comentarios:</label>
      <textarea id="remarks" name="remarks" class="form-control"></textarea>
    </div>

    <div class="form-group col-md-6">
      <label for="operator_remarks">Comentarios Operadora:</label> <span class="text-muted small">(Para control interno)</span>
      <textarea id="operator_remarks" name="operator_remarks" class="form-control" ></textarea>
    </div>
  </div>

  <div class="col-md-12" style="margin: 15px 0;">
    <ul class="add_services small">
      <li class="title">Agregar Servicio:</li>
      <li onclick="addService('hotel');"><i class="fa fa-plus"></i>&nbsp; Hotel</li>
      <li onclick="addService('ticket');"><i class="fa fa-plus"></i>&nbsp; Ticket</li>
      <li onclick="addService('transfer');"><i class="fa fa-plus"></i>&nbsp; Transfer</li>
      <!-- <li><i class="fa fa-plus"></i>&nbsp; Rent Car</li> -->
      <li onclick="addService('flight');"><i class="fa fa-plus"></i>&nbsp; Aereo</li>
      <li onclick="addService('other');"><i class="fa fa-plus"></i>&nbsp; Otros</li>
      <li onclick="$('#remarks_container').fadeToggle(); $('.remarks_icon').toggle();">
        <i class="fa fa-plus remarks_icon"></i>
        <i class="fa fa-minus remarks_icon" style="display: none;"></i>&nbsp; Comentarios
      </li>
    </ul>
  </div>
  <div class="form-group col-md-12" style="margin-bottom: 15px;">
    <input type="hidden" name="services_count" id="services_count" value="0" />
    <input type="hidden" name="request_action" value="create_update" />
    <input type="hidden" name="controller" value="booking" />
    <input type="hidden" name="action" value="create" />
    <?php  if (isset($book) && isset($book->id) && $book->id && $book->debit_note != '') { ?>
          <input type="hidden" name="book_id" id="book_d" value="<?php echo $book->id; ?>" />
          <div class="pull-right">
            <button type="submit" class="btn btn-info" id="btn_clone" name="submit" value="clone">
              Generar <?php echo $book->getCloneDebitNoteNumber(); ?>
            </button>
          </div>
    <?php  }
        if(isset($book) && isset($book->id) && $book->id){ ?>
          <input type="hidden" name="book_id" id="book_d" value="<?php echo $book->id; ?>" />
          <button type="submit" id="btn_update" class="btn btn-info-css" name="submit" value="update">Actualizar</button>
    <?php  }
        else { ?>
          <button type="submit" id="btn_save" class="btn btn-info-css" name="submit" value="save">Guardar</button>
    <?php  } ?>
  </div>
</form>
<div class="clear" style="clear: both;height: 15px;"></div>
<script>
  $(function() {

    $('#generate_package').on("click",function(e){
      e.preventDefault();
      cad = '';
      $('#package_content').val(' ');
      count_servic = $('#services_count').val();
      for (i = 1; i <= count_servic; i++) {
        type = $('#type_' + i).val();
        if( type != undefined){
            switch (type){
              case "1":
                cad = load_Hotel(i, cad);
              break;
              case "2":
                cad = load_Ticket(i, cad);
              break;
              case "3":
                cad = load_Transfer(i, cad);
              break;
              case "5":
                cad = load_Flight(i, cad);
              break;
              case "6":
                cad = load_Other(i, cad);
              break;
            }
        }
      }
      rows1 = cad.split('\n');
      $('#package_description').attr("rows", rows1.length);
      $('#package_description').val(cad);
    });

    $('#estado').on("change", function(e) {
      $('#estado_id').val($('#estado option:selected').val());
    });

    $("#add_book_form :input[type=text]").on("keypress", function(e) {
        return e.keyCode != 13;
    });

    <?php if (isset($book) && ($book->remarks != '' || $book->operator_remarks != '')) { ?>
      rmk = <?php echo json_encode($book->remarks); ?>;
      op_rmk = <?php echo json_encode($book->operator_remarks); ?>;
      $('#remarks').val(rmk);
      $('#operator_remarks').val(op_rmk);
      $('#remarks_container').toggle();
      $('.remarks_icon').toggle();
    <?php } ?>

    $('#btn_clone').click(function() {
      showProgress();
      var form = $('#add_book_form').serialize() + "&submit=clone";
      $.ajax({
          url         : 'rest_from_js.php',
          type        : 'GET',
          data        : form,
          success     : function(data) {
            book = JSON.parse(data);
            window.location.href = getLinkGlobal() + "/zion/index.php?controller=booking&action=show&id=" + book.bookId;
          },
          error       : function(errors) {
            alert('Error, hubo un problema.');
          }
      });
      return false;
    });
    $('#btn_update').click(function() {
      showProgress();
      setTimeout(function() {  }, 1700);
      var cantFiles  = $('input:file')
      var data    = new FormData();
      var count   = 0;
      var arr_id  = new Array()
      var service_count = $('#services_count').val()
      for (var i = 1; i <= service_count; i++) {
        if ($('.service_id_' + i).length == 0) {
          continue;
        }
        var archivos    = document.getElementById("file_" + i);
        var archivo     = archivos.files;
        if (archivo.length != 0) {
          count++;
          var id = $('.service_id_' + i).val();
          data.append('archivo_' + i, archivo[0]);
          data.append('id_' + i, i);
          data.append('service_id_' + i,  id);
        }
      }
      if (count > 0) {
        data.append('service_count', service_count);
        data.append('action', 'upload_image');
        data.append('controller', 'booking');
        $.ajax({
            async:          false,
            url:            'rest_from_js.php',
            type:           'POST',
            contentType:    false,
            data:           data,
            processData:    false,
            cache:          false
        }).done(function(msg)
        {
            console.log(msg)
        });
      }

      var form = $('#add_book_form').serialize() + "&submit=update";
      $.ajax({
          url         : 'rest_from_js.php',
          type        : 'POST',
          data        : form,
          success     : function(data) {
            book = JSON.parse(data);
            window.location.href = getLinkGlobal() + "/zion/index.php?controller=booking&action=show&id=" + book.bookId;
          },
          error       : function(errors) {
            alert('Error, hubo un problema.');
          }
      });
      return false;
    });
    $('#btn_save').click(function() {
      showProgress();
      var form = $('#add_book_form').serialize() + "&submit=save";
      $.ajax({
          url         : 'rest_from_js.php',
          type        : 'GET',
          data        : form,
          success     : function(data) {
            book = JSON.parse(data);
            window.location.href = getLinkGlobal() + "/zion/index.php?controller=booking&action=show&id=" + book.bookId;
          },
          error       : function(errors) {
            alert('Error, hubo un problema.');
          }
      });
      return false;
    });
  });
</script>
