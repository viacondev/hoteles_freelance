<?php
$transfer = ServiceTransfer::find(array('service_id' => $service->id));
$transferType_str = $transfer->type == 'OUT' ? 'Salida' : 'Llegada';
?>
<div style="padding: 5px 10px; background-color: #fff; border-top: 1px solid #999;">
  <span style="color: #3399f3; font-weight: bold;"><i class="fa fa-suitcase"></i> <?php echo $service->name ?></span>
  <?php if ($service->destination_code != '') {
    echo '<span style="padding: 0 20px;"><strong>Destino:</strong> ' . $service->destination_code . '</span>';
  } ?>
  <?php if ($service->localizer != '') {
    echo '<strong>Localizador: </strong>' . $service->localizer;
  } ?>
  <div><?php echo $transfer->origin . ' &#8594 ' . $transfer->destination; ?></div>
  <div>
    <strong>Fecha recogida: </strong><?php echo dateFormatFromDB($service->checkin); ?>. &nbsp; 
    <strong>Hora de <?php echo $transferType_str; ?> (vuelo, bus, tren): </strong><?php echo $transfer->time_in; ?>. &nbsp; 
    <strong>Nro. vuelo: </strong><?php echo $transfer->flight_in; ?> &nbsp; 
  </div>

  <?php if ($transfer->type == 'IN_OUT') { ?>
    <div style="margin-top: 3px;"><?php echo $transfer->destination . ' &#8594 ' . $transfer->origin; ?></div>
    <div>
      <strong>Fecha recogida: </strong><?php echo dateFormatFromDB($service->checkout); ?>. &nbsp; 
      <strong>Hora de Salida (vuelo, bus, tren): </strong><?php echo $transfer->time_out ?>. &nbsp; 
      <strong>Nro. vuelo: </strong><?php echo $transfer->flight_out; ?> &nbsp; 
    </div>
  <?php } ?>

  <div style="padding: 0 10px;">
    <table style="width: 100%; font-size: 12px">
      <tr>
        <td>
          <?php
          echo $service->adult_count . ' adultos';
          if ($service->child_count != 0) {
            echo ', ' . $service->child_count . ' ni&ntilde;os';
            if (isset($service->child_ages) && $service->child_ages != '') {
              echo ' (' . $service->child_ages . ')';
            }
          } ?>
        </td>
        <td style="font-weight: bold; text-align: right;">
          <?php if ($book->view_mode != 1) echo number_format($service->price, 2) . ' ' . $service->currency; ?>
        </td>
      </tr>
    </table>
  </div>
  <?php if ($service->includes != '') {
          echo  '<div>'.
                '<strong>Observaciones </strong> / Remarks '.
                '<pre style="background-color:white;border:none;font-family:inherit;padding: 0">' .  
                $service->includes . 
                '</pre></div>';
      } 
  ?>
</div>