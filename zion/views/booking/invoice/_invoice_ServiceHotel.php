<?php
// $rooms = $service->getRooms();
// $discounts = $service->getDiscounts();
$rooms = $service->rooms;
$discounts = array();
?>

<div style="padding: 5px 10px; background-color: #fff; border-top: 1px solid #999;">
  <span style="color: #3399f3; font-weight: bold;"><i class="fa fa-home"></i> <?php echo $service->name; ?></span> &nbsp;
  <span><?php showCategory($service->category); ?></span>
  <?php if ($service->destination_code != '') {
    echo '<span style="padding: 0 20px;"><strong>Destino:</strong> ' . $service->destination_code . '</span>';
  } ?>
  <?php if ($service->localizer != '') {
    echo '<strong>Localizador: </strong>' . $service->localizer;
  } ?>
  <div>
    Entrada: <strong><?php echo dateFormatFromDB($service->checkin); ?></strong> -
    Salida: <strong><?php echo dateFormatFromDB($service->checkout); ?></strong>
    <?php $from = new DateTime($service->checkin);$to = new DateTime($service->checkout); echo "(" . $to->diff($from)->days . " noches)"; ?>
  </div>
  <div style="padding: 0 10px;">
    <table style="width: 100%; font-size: 12px">
      <thead>
        <tr>
          <th style="text-align: left;">Tipo de habitaci&oacute;n</th>
          <th style="text-align: left;">R&eacute;gimen</th>
          <th style="text-align: left;">Ocupaci&oacute;n</th>
          <th style="text-align: right;"></th>
        </tr>
      </thead>
      <tbody>
        <?php
        foreach ($rooms as $room) {
          ?>
          <tr>
            <td><?php echo $room->type; ?></td>
            <td><?php echo $room->board; ?></td>
            <td>
              <?php
              $adult_str = $room->adult_count > 1 ? ' adultos' : ' adulto';
              if ($room->adult_count > 0) { echo $room->adult_count . $adult_str; }
              if ($room->child_count > 0) {
                $child_str = $room->child_count > 1 ? ' ni&ntilde;os' : ' ni&ntilde;o';
                echo ' ' . $room->child_count . $child_str;
              }
              ?>
            </td>
            <td style="font-weight: bold; text-align: right;">
              <?php if ($book->view_mode != 1) echo number_format($room->price, 2) . ' ' . $service->currency; ?>
            </td>
          </tr>
          <?php
        }
        if (count($discounts) > 0) {
          ?>
          <tr>
            <th colspan="3">Descuentos</th>
            <th></th>
          </tr>
          <?php
          foreach ($discounts as $discount) {
            ?>
            <tr>
              <td colspan="3"><?php echo $discount->concept; ?></td>
              <td style="font-weight: bold; text-align: right;">
                - <?php echo $discount->amount . ' ' . $service->currency; ?>
              </td>
            </tr>
            <?php
          }
        }
        ?>
      </tbody>
    </table>
  </div>
  <?php if ($service->includes != '') {
          echo  '<div>'.
                '<strong>Observaciones </strong> / Remarks '.
                '<pre style="background-color:white;border:none;font-family:inherit;padding: 0">' .
                $service->includes .
                '</pre></div>';
        }
  ?>
</div>
