<div style="padding: 5px 10px; background-color: #fff; border-top: 1px solid #999;">
  <span style="color: #3399f3; font-weight: bold;"><i class="fa fa-plane"></i> <?php echo $service->name ?></span>
  <?php if ($service->destination_code != '') {
    echo '<span style="padding: 0 20px;"><strong>Destino:</strong> ' . $service->destination_code . '</span>';
  } ?>
  <?php if ($service->localizer != '') {
    echo '<strong>PNR: </strong>' . $service->localizer;
  } ?>
  <div>
    Salida: <strong><?php echo dateFormatFromDB($service->checkin); ?></strong>
  </div>

  <div style="padding: 0 10px;">
    <table style="width: 100%; font-size: 12px">
      <tbody>
        <tr>
          <td>
            <?php
            echo $service->adult_count . ' adultos';
            if ($service->child_count != 0) {
              echo ', ' . $service->child_count . ' ni&ntilde;os';
            }
            ?>
          </td>
          <td style="font-weight: bold; text-align: right;">
            <?php if ($book->view_mode != 1) echo number_format($service->price, 2) . ' ' . $service->currency; ?>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
  <?php if ($service->includes != '') {
          echo  '<div>'.
                '<strong>Observaciones </strong> / Remarks '.
                '<pre style="background-color:white;border:none;font-family:inherit;padding: 0">' .  
                $service->includes . 
                '</pre></div>';
        } 
  ?>
</div>