<div style="padding: 5px 10px; background-color: #fff; border-top: 1px solid #999;">
  <span style="color: #3399f3; font-weight: bold;"><i class="fa fa-book"></i> <?php echo $service->name ?></span>
  <? if ($service->destination_code != '') {
   echo '<span style="padding: 0 20px;"><strong>Destino:</strong> ' . $service->destination_code . '</span>';
  } ?>
  <? if ($service->localizer != '') {
   echo '<strong>Localizador: </strong>' . $service->localizer;
  } ?>

  <div style="padding: 0 10px;">
    <table style="width: 100%; font-size: 12px">
      <tbody>
        <tr>
          <td>
            <? if ($service->includes != '') {
                  echo  '<div>'.
                        '<strong>Observaciones </strong> / Remarks '.
                        '<pre style="background-color:white;border:none;font-family:inherit;padding: 0">' .  
                        $service->includes . 
                        '</pre></div>';
                } 
            ?>
          </td>
          <td style="font-weight: bold; text-align: right;">
            <? if ($book->view_mode != 1) echo number_format($service->price, 2) . ' ' . $service->currency; ?>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>