<?php global $provider ?>

<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-3"><?php include('views/admin/_menu.php'); ?></div>

      <div class="col-md-9 wbox" style="width:73.7%">
      <h2>Datos Proveedor</h2>
      <div class="col-md-12 text-right">
        <a href="?controller=provider&amp;action=edit&amp;id=<?php echo $provider->id; ?>"><i class="fa fa-pencil"></i> Editar Proveedor</a>
      </div>
      <table class="table table-hover">
        <tbody>
          <tr>
            <td><strong>Nombre:</strong></td>
            <td><?php echo $provider->name; ?></td>
          </tr>
          <tr>
            <td><strong>Tipo:</strong></td>
            <td><? echo ($provider->type == "R") ? "Receptivo" : "Emisivo"; ?></td>
          </tr>
          <tr>
            <td><strong>Tel&eacute;fono:</strong></td>
            <td><? echo $provider->phone; ?></td>
          </tr>
          <tr>
            <td><strong>Emails:</strong></td>
            <td><? echo $provider->mail; ?></td>
          </tr>
          <tr>
            <td><strong>Direcci&oacute;n</strong></td>
            <td><? echo $provider->address; ?></td>
          </tr>
          <tr>
            <td><strong>Comentarios</strong></td>
            <td>
              <ul class="lista_descripcion" style="list-style:none;padding: 0;">
                <?php
                  $observaciones = explode("\n", $provider->coments);
                  for($i=0; $i<count($observaciones); $i++)
                  {
                    if($observaciones[$i] != "")
                    {
                  ?>
                      <li>
                        <span><?php echo $observaciones[$i]; ?></span>
                      </li>
                  <?php
                    }
                  }
                ?>
              </ul>
            </td>
          </tr>
        </tbody>
      </table>

    </div>
    </div>
  </div>
</div>

<form method="post" id="add_user_form" action="?controller=user&amp;action=add" class="hidden invisible">
  <input type="hidden" name="agency_id" id="agency_id" />
</form>

<script type="text/javascript">
  $(function() {
    $('#provider_home').addClass('active');
  });
</script>
