<?php global $provider; ?>

<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-3"><?php include('views/admin/_menu.php'); ?></div>

      <div class="col-md-9 wbox" style="width:73.7%">
      <h2>Editar Proveedor</h2>

      <form method="post" action="?controller=provider&amp;action=update" enctype="multipart/form-data">
        <div class="col-md-7">
          <div class="form-group col-md-12">
            <label>Nombre:</label>
            <input type="text" class="form-control" id="name" name="name" value="<?php echo $provider->name;?>">
          </div>

          <div class="form-group col-md-12">
            <label>Tel&eacute;fono:</label>
            <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $provider->phone;?>">
          </div>

          <div class="form-group col-md-12">
            <label>Email:</label>
            <input type="text" class="form-control" id="mail" name="mail" value="<?php echo $provider->mail;?>">
          </div>

          <div class="form-group col-md-12">
            <label>Direcci&oacute;n:</label>
            <input type="text" class="form-control" id="address" name="address" value="<?php echo $provider->address;?>">
          </div>

          <div class="form-group col-md-12">
            <label>Tipo:</label>
            <select class="form-control" id="type" name="type">
              <option value="E" <? if($provider->type == "E"){ echo "selected"; } ?> >Emisivo</option>
              <option value="R" <? if($provider->type == "R"){ echo"selected"; } ?> >Receptivo</option>
            </select>
          </div>

          <div class="form-group col-md-12">
            <label>Comentarios:</label>
            <textarea class="form-control" id="coments" name="coments" class="form-control" rows="3" placeholder="Mensaje" ><?php echo $provider->coments; ?></textarea>
          </div>

          <div class="form-group col-md-12">
            <input type="hidden" name="id" value="<?php echo $provider->id; ?>" />
            <button class="btn btn-info-css">Guardar</button>
          </div>
        </div>
      </form>
    </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(function() {
    $('#provider_home').addClass('active');
  });
</script>
