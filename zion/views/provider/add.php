<div class="content">
  <div class="container">
    <div class="col-md-3"><?php include('views/admin/_menu.php'); ?></div>

    <div class="col-md-9 wbox">
      <h2>Agregar nuevo proveedor</h2>

      <form method="post" action="?controller=provider&amp;action=create" enctype="multipart/form-data">
        <div class="col-md-7">
          <div class="form-group col-md-12">
            <label>Nombre:</label>
            <input type="text" class="form-control" id="name" name="name">
          </div>

          <div class="form-group col-md-12">
            <label>Tel&eacute;fono:</label>
            <input type="text" class="form-control" id="phone" name="phone" value="">
          </div>

          <div class="form-group col-md-12">
            <label>Email:</label>
            <input type="text" class="form-control" id="mail" name="mail" value="">
          </div>

          <div class="form-group col-md-12">
            <label>Direcci&oacute;n:</label>
            <input type="text" class="form-control" id="address" name="address" value="">
          </div>

          <div class="form-group col-md-12">
            <label>Tipo:</label>
            <select class="form-control" id="type" name="type">
              <option value="E" >Emisivo</option>
              <option value="R" >Receptivo</option>
            </select>
          </div>

          <div class="form-group col-md-12">
            <label>Comentarios:</label>
            <textarea class="form-control" id="coments" name="coments" class="form-control" rows="3" placeholder="Mensaje" ></textarea>
          </div>

          <div class="form-group col-md-12">
            <button class="btn btn-info-css">Guardar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(function() {
    $('#provider_home').addClass('active');
  });
</script>
