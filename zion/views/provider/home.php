<?php global $provider; ?>

<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-3"><?php include('views/admin/_menu.php'); ?></div>

      <div class="col-md-9 wbox" style="width:73.7%">

      <h2>Lista de Proveedores</h2>

      <div class="col-md-12 text-right">
        <a href="?controller=provider&amp;action=add"><i class="fa fa-plus"></i> Agregar Proveedor</a>
      </div>
      <table class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Tipo</th>
            <th>Tel&eacute;fono</th>
            <th>Mail</th>
            <th>&nbsp;&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach ($provider as $provid) {
            ?>
            <tr>
              <td><?php echo $provid->name; ?></td>
              <td><?php echo ($provid->type == "R") ? "Receptivo" : "Emisivo"; ?></td>
              <td><?php echo $provid->phone; ?></td>
              <td><?php echo $provid->mail; ?></td>
              <td>
                <a href="?controller=provider&amp;action=show&amp;id=<?php echo $provid->id; ?>">Ver</a>
              </td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(function() {
    $('#provider_home').addClass('active');
  });
</script>
