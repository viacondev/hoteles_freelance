<?php

class HotelRoom extends Model {
  public $type;
  public $board;
  public $adult_count;
  public $child_count;
  public $childs_ages;
  public $price;
  public $net_price;
  public $service_id;

  public function build($type, $board, $adult_count, $child_count, $childs_ages, $price, $net_price, $service_id) {
    $this->type = $type;
    $this->board = $board;
    $this->adult_count = $adult_count;
    $this->child_count = $child_count;
    $this->childs_ages = $childs_ages;
    $this->price = $price;
    $this->net_price = $net_price;
    $this->service_id = $service_id;
  }
}

?>