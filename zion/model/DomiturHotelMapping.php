<?php

class DomiturHotelMapping extends Model {
  public $code;
  public $zion_code;

  public static function getMappedCode($code) {
    $map = DomiturHotelMapping::find(array('code' => $code));
    if ($map != NULL) {
      return $map->zion_code;
    }
    return NULL;
  }
}

?>