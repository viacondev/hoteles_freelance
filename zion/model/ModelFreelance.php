<?php

abstract class ModelFreelance {
  public $id;

  public static function getTableName() {
    return get_called_class();
  }

  /**
   * List of attrs that does not match the DB model
   */
  public static function getNonDBAttrs() {
    return array();
  }

  /**
   * TODO: Add LIMIT and PAGE to avoid large data
   */
  public static function findAll($params = array(), $orderBy = '') {
    $table = self::getTableName();
    $where = '';
    $conditions = array();
    $condition_values = array();
    if (count($params) > 0) {
      foreach ($params as $key => $value) {
        $conditions[] = "$key = :$key";
        $condition_values[":$key"] = $value;
      }
      $where = 'WHERE ' . implode(' AND ', $conditions);
    }
    $orderBy = $orderBy != '' ? "ORDER BY $orderBy" : '';

    $query = DBFreelance::conn()->prepare("SELECT * FROM $table $where $orderBy");
    $query->setFetchMode(PDO::FETCH_CLASS, get_called_class());
    $query->execute($condition_values);

    return $query->fetchAll();
  }

  public static function find($params) {
    $result = self::findAll($params);
    return count($result) > 0 ? $result[0] : NULL;
  }

  public static function findById($id) {
    return self::find(array('id' => $id));
  }

  public function save() {
    if (isset($this->id) && $this->id != 0) {
      return $this->update();
    }

    $table = self::getTableName();

    $columns = array();
    $values_prepare = array();
    $values = array();
    $vars = get_object_vars($this);
    foreach ($vars as $key => $value) {
      if (in_array($key, $this->getNonDBAttrs())) {
        continue;
      }

      $columns[] = $key;
      $values_prepare[] = ":$key";
      $values[":$key"] = $value;
    }
    $columns_str = implode(', ', $columns);
    $values_prepare_str = implode(', ', $values_prepare);
    $sql = "INSERT INTO $table ($columns_str) VALUES ($values_prepare_str)";

    $query = DB::conn()->prepare($sql);
    $query->execute($values);
    //__logarr($query->errorInfo());
    $this->id = DBFreelance::conn()->lastInsertId();
  }

  public function update() {
    if (!isset($this->id) || $this->id == 0) {
      return $this->save();
    }

    $table = self::getTableName();

    $columns = array();
    $values = array();
    $vars = get_object_vars($this);
    foreach ($vars as $key => $value) {
      if (in_array($key, $this->getNonDBAttrs())) {
        continue;
      }

      $columns[] = "$key=:$key";
      $values[":$key"] = $value;
    }
    $columns_str = implode(', ', $columns);
    $sql = "UPDATE $table SET $columns_str WHERE id=" . $this->id;

    $query = DBFreelance::conn()->prepare($sql);
    $query->execute($values);
    //__logarr($query->errorInfo());
  }

  public function updateColumn($column, $value) {
    if (!isset($this->id)) {
      return;
    }

    $table = self::getTableName();
    $sql = "UPDATE $table SET $column=:$column WHERE id=" . $this->id;

    $query = DBFreelance::conn()->prepare($sql);
    $query->execute(array(":$column" => $value));
    //__logarr($query->errorInfo());
  }

  public function delete() {
    $table = self::getTableName();
    $id = $this->id;
    DBFreelance::conn()->exec("DELETE FROM $table WHERE id = '$id'");
  }

  public static function findByQuery($sqlQuery) {
    $query = DBFreelance::conn()->prepare($sqlQuery);
    $query->setFetchMode(PDO::FETCH_CLASS, get_called_class());
    $query->execute();

    return $query->fetchAll();
  }

  public static function execQuery($sqlQuery) {
    $query = DBFreelance::conn()->prepare($sqlQuery);
    $query->execute();

    return $query->fetchAll();
  }
}

?>