<?php

class Provider extends Model {
  public $name;
  public $type;
  public $phone;
  public $mail;
  public $address;
  public $coments;
  public $id_proveedor_contable;
  public $status;

  // Consts that match the XML providers
  const HOTELBEDS   = 1;
  const DOMITUR     = 23;
  const METHABOOK   = 75;
  const METHABOOK2  = 150;
  const TOURICO     = 80;
  const DOMITUR2    = 159;
  const RESTEL      = 3;
  const TAILORBEDS  = 136;
  const NEMO        = 187;
  const SPECIALTOURS  = 114;
  const TRESBTOUR     = 236;
  const RATEHAWK      = 248;

  public function build($name, $type, $phone, $mail, $address, $coments, $id_proveedor_contable = 0, $status = 1) {
    $this->name   = $name;
    $this->type   = $type;
    $this->phone  = $phone;
    $this->mail   = $mail;
    $this->address = $address;
    $this->coments = $coments;
    $this->id_proveedor_contable = $id_proveedor_contable;
    $this->status = $status;
  }

  public static function findAll($params = array(), $orderBy = '') {
    return parent::findAll($params, 'name ASC');
  }

  public static function getProviderName($id) {
    if ($id == 1) return 'HOTELBEDS';
    if ($id == 23) return 'DOMITUR';
    if ($id == 75) return 'METHABOOK';
    if ($id == 150) return 'METHABOOK2';
    if ($id == 80) return 'TOURICO';
    if ($id == 159) return 'DOMITUR2';
    if ($id == 3)    return 'RESTEL';
    if ($id == 136) return 'TAILORBEDS';
    if ($id == 187) return 'NEMO';
    if ($id == 114) return 'SPECIALTOURS';
    if ($id == 236) return '3BTOURS';
    if ($id == 248) return 'RATEHAWK';
  }

  public static function showNameIfAdmin($id, $includeHTML = false) {
    if ($_SESSION['admin_role'] && !empty($id)) {
      $name = Provider::findById($id)->name;
      if ($includeHTML) {
        return '<span class="small text-muted"> - ' . $name . '</span>';
      }
      return $name;
    }
    return '';
  }
}

?>
