<?php

class Booking extends Model {
  public $localizer;
  public $status;
  public $holder;
  public $origen;
  public $book_date;
  public $confirm_date;
  public $cancel_date;
  public $user_id;
  public $agency_id;
  public $agent;
  public $agency;
  public $operator_id;
  public $remarks;
  public $operator_remarks;
  public $debit_note;
  public $view_mode;
  public $package_description;
  public $booking_code;
  public $holder_phone;
  public $holder_mail;
  public $user_confirm_id;
  public $debit_number_conta;

  public function build($localizer, $status, $holder, $book_date, $user_id, $agency_id, $agent, $agency, $operator_id, $remarks, 
                        $operator_remarks, $view_mode, $package_description, $booking_code = 0, $holder_phone = '', $holder_mail='', 
                        $confirm_date = NULL, $cancel_date = '0000-00-00 00:00:00', $user_confirm_id = 0, $name = '', $last_name = '') {
    $this->localizer        = $localizer;
    $this->status           = $status;
    $this->holder           = $holder;
    $this->name             = $name;
    $this->last_name        = $last_name;
    $this->book_date        = $book_date;
    $this->user_id          = $user_id;
    $this->agency_id        = $agency_id;
    $this->agent            = $agent;
    $this->agency           = $agency;
    $this->operator_id      = $operator_id;
    $this->remarks          = $remarks;
    $this->operator_remarks = $operator_remarks;
    $this->debit_note       = $status != ServiceStatus::PENDING ? $this->getNextDebitNoteNumber() : '';
    $this->view_mode        = $view_mode;
    $this->package_description = $package_description;
    $this->booking_code     = $this->getNextBookingCodeNumber();
    $this->holder_phone     = $holder_phone;
    $this->holder_mail      = $holder_mail;
    $this->confirm_date     = $confirm_date;
    $this->cancel_date      = $cancel_date;
    $this->origen           = 'YoCounter';
    $this->user_confirm_id  = $user_confirm_id;
    $this->debit_number_conta = 0;
  }

  public function getServices() {
    return Service::findAll(array('booking_id' => $this->id));
  }

  public function getServiceWithNearstCancellation() {
    $sql = 'SELECT * FROM Service
              WHERE booking_id = ' . $this->id . ' AND cancellation_date !=  "0000-00-00"
              ORDER BY cancellation_date LIMIT 1';
    $service = Service::findByQuery($sql);
    if (isset($service[0]) && $service[0] != NULL) {
      return $service[0];
    }
    return NULL;
  }

  public function Reporte_Books($filters) {
    $select = 'SELECT DISTINCT b.* FROM Booking b, Service s, Agency a';
    $orderBy = 'ORDER BY b.booking_code ASC';
    $conditions = array();
    // $conditions[] = 'b.agency_id = a.id';
    $conditions[] = 'a.ciudad_id = 30';
    if (array_key_exists('agency_id', $filters) && $filters['agency_id'] != '') {
      //$conditions[] = 'b.user_id = u.id';
      //$conditions[] = 'u.agency-id = a.id';
      $conditions[] = 'b.agency_id = ' . $filters['agency_id'];
    }
    if(array_key_exists('user_id', $filters) && $filters['user_id'] != '') {
      $conditions[] = 'b.user_id = ' . $filters['user_id'];
    }
    if (array_key_exists('status', $filters) && $filters['status'] != '') {
      if ($filters['status'] == 'NO_CANCELLED') {
        $status = "b.status != " . ServiceStatus::CANCELLED;
      }
      else {
        $status  = 'b.status = "' . $filters['status'] . '"';
        if ($filters['status'] == ServiceStatus::CONFIRMED) {
          $status = "($status OR b.status = " . ServiceStatus::PAYED . ")";
        }
      }
      $conditions[] = $status;
    }
    if (array_key_exists('note_number', $filters) && $filters['note_number'] != '') {
      $note_number = $filters['note_number'];
      $first = substr($note_number, 0, 1);
      if ($first != 'R' && $first != 'r') {
        if ($first == 'A' || $first == 'a') {
          $note_number = substr($note_number, 1);
        }
        $conditions[] = 'b.debit_note LIKE "' . $note_number . '%"';
      }
      else {
        $note_number = substr($note_number, 1);
        $conditions[] = 'b.booking_code = "' . $note_number . '"';
      }
    }
    if (array_key_exists('checkin', $filters) && $filters['checkin'] != '' &&
        array_key_exists('checkout', $filters) && $filters['checkout'] != '') {
      if ($filters['datesFilter'] == 'confirm') {
        $conditions[] = '(b.confirm_date BETWEEN " 00:00:00' . $checkin . '" AND "' . $checkout . ' 23:59:59")';
        //$orderBy = 'ORDER BY b.confirm_date DESC';
      }
    }
    if (array_key_exists('from',$filters) && array_key_exists('to',$filters)) {
      $from_arr = explode('/', $filters['from']);
      $from = $from_arr[2] . '-' . $from_arr[1] . '-' . $from_arr[0];
      $to_arr = explode('/', $filters['to']);
      $to = $to_arr[2] . '-' . $to_arr[1] . '-' . $to_arr[0];
      $conditions[] = '(b.confirm_date BETWEEN "' . $from. ' 00:00:00" AND "' . $to . ' 23:59:59")';
    }
    if (array_key_exists('provider_id', $filters) && $filters['provider_id'] != '') {
      $conditions[] = 's.booking_id = b.id';
      $conditions[] = 's.provider_id = 1';
      $conditions[] = 's.provider_id = ' . $filters['provider_id'];
    }
    if (!isset($filters['all_books'])) {
      $conditions[] = 's.booking_id = b.id';
      $conditions[] = 'b.status != ' . ServiceStatus::ANNULLED;
    }

    $where = count($conditions) > 0 ? ' WHERE ' . implode(' AND ', $conditions) : '';

    return Booking::findByQuery("$select $where $orderBy");
  }
  
  public function filterByAgencyManager($filters) {
    $select = 'SELECT DISTINCT b.* FROM Booking b, Service s, Agency a';
    $orderBy = 'ORDER BY b.booking_code DESC';
    $conditions = array();
    $conditions[] = 'a.ciudad_id = ' . $filters['ciudad_id'];

    if (array_key_exists('agency_id', $filters) && $filters['agency_id'] != '') {
      //$conditions[] = 'b.user_id = u.id';
      //$conditions[] = 'u.agency-id = a.id';
      $conditions[] = 'b.agency_id = ' . $filters['agency_id'];
    }
    if(array_key_exists('user_id', $filters) && $filters['user_id'] != '') {
      $conditions[] = 'b.user_id = ' . $filters['user_id'];
    }
    if (array_key_exists('status', $filters) && $filters['status'] != '') {
      if ($filters['status'] == 'NO_CANCELLED') {
        $status = "b.status != " . ServiceStatus::CANCELLED;
      }
      else {
        $status  = 'b.status = "' . $filters['status'] . '"';
        if ($filters['status'] == ServiceStatus::CONFIRMED) {
          $status = "($status OR b.status = " . ServiceStatus::PAYED . ")";
        }
      }
      $conditions[] = $status;
    }
    if (array_key_exists('note_number', $filters) && $filters['note_number'] != '') {
      $note_number = $filters['note_number'];
      $first = substr($note_number, 0, 1);
      if ($first != 'R' && $first != 'r') {
        if ($first == 'A' || $first == 'a') {
          $note_number = substr($note_number, 1);
        }
        $conditions[] = 'b.debit_note LIKE "' . $note_number . '%"';
      }
      else {
        $note_number = substr($note_number, 1);
        $conditions[] = 'b.booking_code = "' . $note_number . '"';
      }
    }
    if (array_key_exists('destination_code', $filters) && $filters['destination_code'] != '') {
      $conditions[] = 's.destination_code = "' . $filters['destination_code'] . '"';
    }
    if (array_key_exists('checkin', $filters) && $filters['checkin'] != '' &&
        array_key_exists('checkout', $filters) && $filters['checkout'] != '') {
      $checkin_arr = explode('/', $filters['checkin']);
      $checkin = $checkin_arr[2] . '-' . $checkin_arr[1] . '-' . $checkin_arr[0];
      $checkout_arr = explode('/', $filters['checkout']);
      $checkout = $checkout_arr[2] . '-' . $checkout_arr[1] . '-' . $checkout_arr[0];
      if ($filters['datesFilter'] == 'checkin') {
        $conditions[] = '(s.checkin BETWEEN "' . $checkin . '" AND "' . $checkout . '")';
        //$orderBy = 'ORDER BY s.checkin ASC';
      }
      else if ($filters['datesFilter'] == 'confirm') {
        $conditions[] = '(b.book_date BETWEEN "' . $checkin . '" AND "' . $checkout . '")';
        //$orderBy = 'ORDER BY b.confirm_date DESC';
      }
      else if ($filters['datesFilter'] == 'cancellation') {
        $conditions[] = '(s.cancellation_date BETWEEN "' . $checkin . '" AND "' . $checkout . '")';
        //$orderBy = 'ORDER BY s.cancellation_date ASC';
      }
    }
    if (array_key_exists('from',$filters) && array_key_exists('to',$filters)) {
      $from_arr = explode('/', $filters['from']);
      $from     = $from_arr[2] . '-' . $from_arr[1] . '-' . $from_arr[0];
      $to_arr   = explode('/', $filters['to']);
      $to       = $to_arr[2] . '-' . $to_arr[1] . '-' . $to_arr[0];
      $conditions[] = '(b.book_date BETWEEN "' . $from. '" AND "' . $to . '")';
    }
    if (!isset($filters['all_books'])) {
      $conditions[] = 's.booking_id = b.id and a.id = b.agency_id';
      $conditions[] = 'b.status != ' . ServiceStatus::ANNULLED;
    }

    $where = count($conditions) > 0 ? ' WHERE ' . implode(' AND ', $conditions) : '';
    // __log("$select $where $orderBy");
    return Booking::findByQuery("$select $where $orderBy");

  }

  public function filterBooks($filters) {
    $select = 'SELECT DISTINCT b.* FROM Booking b, Service s';
    $orderBy = 'ORDER BY b.booking_code DESC';
    $conditions = array();
    if (array_key_exists('agency_id', $filters) && $filters['agency_id'] != '') {
      //$conditions[] = 'b.user_id = u.id';
      //$conditions[] = 'u.agency-id = a.id';
      $conditions[] = 'b.agency_id = ' . $filters['agency_id'];
    }
    if(array_key_exists('user_id', $filters) && $filters['user_id'] != '') {
      $conditions[] = 'b.user_id = ' . $filters['user_id'];
    }
    if (array_key_exists('status', $filters) && $filters['status'] != '') {
      if ($filters['status'] == 'NO_CANCELLED') {
        $status = "b.status != " . ServiceStatus::CANCELLED;
      }
      else {
        $status  = 'b.status = "' . $filters['status'] . '"';
        if ($filters['status'] == ServiceStatus::CONFIRMED) {
          $status = "($status OR b.status = " . ServiceStatus::PAYED . ")";
        }
      }
      $conditions[] = $status;
    }
    if (array_key_exists('note_number', $filters) && $filters['note_number'] != '') {
      $note_number = $filters['note_number'];
      $first = substr($note_number, 0, 1);
      if ($first != 'R' && $first != 'r') {
        if ($first == 'A' || $first == 'a') {
          $note_number = substr($note_number, 1);
        }
        $conditions[] = 'b.debit_note LIKE "' . $note_number . '%"';
      }
      else {
        $note_number = substr($note_number, 1);
        $conditions[] = 'b.booking_code = "' . $note_number . '"';
      }
    }
    if (array_key_exists('destination_code', $filters) && $filters['destination_code'] != '') {
      $conditions[] = 's.destination_code = "' . $filters['destination_code'] . '"';
    }
    if (array_key_exists('checkin', $filters) && $filters['checkin'] != '' &&
        array_key_exists('checkout', $filters) && $filters['checkout'] != '') {
      $checkin_arr = explode('/', $filters['checkin']);
      $checkin = $checkin_arr[2] . '-' . $checkin_arr[1] . '-' . $checkin_arr[0];
      $checkout_arr = explode('/', $filters['checkout']);
      $checkout = $checkout_arr[2] . '-' . $checkout_arr[1] . '-' . $checkout_arr[0];
      if ($filters['datesFilter'] == 'checkin') {
        $conditions[] = '(s.checkin BETWEEN "' . $checkin . '" AND "' . $checkout . '")';
        //$orderBy = 'ORDER BY s.checkin ASC';
      }
      else if ($filters['datesFilter'] == 'confirm') {
        $conditions[] = '(b.book_date BETWEEN "' . $checkin . ' 00:00:00" AND "' . $checkout . ' 23:59:59")';
        //$orderBy = 'ORDER BY b.confirm_date DESC';
      }
      else if ($filters['datesFilter'] == 'cancellation') {
        $conditions[] = '(s.cancellation_date BETWEEN "' . $checkin . '" AND "' . $checkout . '")';
        //$orderBy = 'ORDER BY s.cancellation_date ASC';
      }
    }
    if (array_key_exists('from',$filters) && array_key_exists('to',$filters)) {
      $from_arr = explode('/', $filters['from']);
      $from = $from_arr[2] . '-' . $from_arr[1] . '-' . $from_arr[0];
      $to_arr = explode('/', $filters['to']);
      $to = $to_arr[2] . '-' . $to_arr[1] . '-' . $to_arr[0];
      $conditions[] = '(b.book_date BETWEEN "' . $from. '" AND "' . $to . '")';
    }
    if (!isset($filters['all_books'])) {
      $conditions[] = 's.booking_id = b.id';
      $conditions[] = 'b.status != ' . ServiceStatus::ANNULLED;
    }

    $where = count($conditions) > 0 ? ' WHERE ' . implode(' AND ', $conditions) : '';

    return Booking::findByQuery("$select $where $orderBy");
  }

  public function getNextDebitNoteNumber() {
    $sql = "SELECT debit_note FROM Booking WHERE debit_note NOT LIKE '%-%' ORDER BY debit_note DESC LIMIT 1";
    $res = Booking::execQuery($sql);

    return intval($res[0][0]) + 1;
  }

  public function getNextBookingCodeNumber() {
    $sql = "SELECT booking_code FROM Booking WHERE booking_code NOT LIKE '%-%' ORDER BY booking_code DESC LIMIT 1";
    $res = Booking::execQuery($sql);

    return intval($res[0][0]) + 1;
  }

  public function getCloneDebitNoteNumber() {
    $arr = explode('-', $this->debit_note);
    if (count($arr) > 1) {
      $current_clone = intval($arr[1]);
      $clone = $current_clone + 1;
      $clone = str_pad($clone, 2, '0', STR_PAD_LEFT);

      return $arr[0] . '-' . $clone;
    }

    return $this->debit_note . '-01';
  }

  public function getCloneBookingCodeNumber() {
    $arr = $this->booking_code;
    if ($this->booking_code != 0) {
      return $arr;
    }
    return 0;
  }

  /**
   * Number of the previous debit note number.
   * Ej: 1400001-03 will return 1400001-03, 1400001-01 -> 1400001, etc.
   */
  public function getClonePrevDebitNoteNumber() {
    $arr = explode('-', $this->debit_note);
    if (count($arr) < 2 || $arr[1] == '01') {
      return $arr[0];
    }

    $prev_clone = intval($arr[1]);
    $prev = $prev_clone - 1;
    $prev = str_pad($prev, 2, '0', STR_PAD_LEFT);

    return $arr[0] . '-' . $prev;
  }

  public function getDebitNoteNumber() {
    if (isset($this->debit_note) && $this->debit_note != '') {
      return 'A'. $this->debit_note;
    }

    return '';
  }

  public function getBookNumber() {
    return 'R' . $prev = str_pad($this->booking_code, 5, '0', STR_PAD_LEFT);
  }

  public function getBookingCode() {
    return 'R' . $prev = str_pad($this->booking_code, 5, '0', STR_PAD_LEFT);
  }

  public function getDebitOrBookNumber() {
    $debit = $this->getDebitNoteNumber();
    return $debit != '' ? $debit : $this->getBookNumber();
  }

  public function bookingToJSON() {
    $booking_arr = array();
    $vars = get_object_vars($this);
    foreach ($vars as $key => $value) {
      $booking_arr[$key] = $value;
    }

    $services_arr = array();
    foreach ($this->getServices() as $service) {
      $arr = array();
      $vars = get_object_vars($service);
      foreach ($vars as $key => $value) {
        $arr[$key] = $value;
      }
      $services_arr[] = $arr;
    }

    $booking_arr['services'] = $services_arr;

    return json_encode($booking_arr);
  }
}

?>
