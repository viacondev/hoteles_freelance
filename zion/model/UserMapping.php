<?php
/**
 *
 */
class UserMapping extends Model {
  public $userbb;
  public $userbarry;
  public $nombre;
  public $id_agency_type;
  function __construct() {

  }

  public function build($userbb, $userbarry, $nombre, $id_agency_type = 0) {
      $this->userbb         = $userbb;
      $this->userbarry      = $userbarry;
      $this->nombre         = $nombre;
      $this->id_agency_type = $id_agency_type;
  }

  public function getUserMappingWithUserBB($userIdBb) {
      $user = parent::find(array('userbb' => $userIdBb, 'id_agency_type' => 2));
      return $user;
  }
}

?>
