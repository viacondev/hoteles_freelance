<?php

class User extends Model {
  public $username;
  public $password;
  public $name;
  public $lastname;
  public $phone;
  public $mail;
  public $agency_id;
  public $role;
  public $user_code;

  function __construct() {

  }

  public function build($username, $password, $name, $lastname, $phone, $mail, $agency_id, $role) {
    $this->username = $username;
    $this->password = $password;
    $this->name = $name;
    $this->lastname = $lastname;
    $this->phone = $phone;
    $this->mail = $mail;
    $this->agency_id = $agency_id;
    $this->role = $role;
    $this->user_code = '';
  }
}

?>
