<?php

class FeeGeneral extends Model {
  public $agency_comission;
  public $incentive_counter;
  public $factor_comission;
  public $operator_comission;
  public $agencia;
  public $origen;
  public $destino;

  public static  $fee_yc = array('origen' => 'yc', 'mark_up' => 0.79, 'agency_comission' => 12, 'incentive_counter' => 0);

    function __construct()
    {
    # code...
    }

    public function build($agency_comission, $incentive_counter, $factor_comission, $agencia , $origen, $destino, $proveedor = 0) {
        $this->agency_comission     = $agency_comission;
        $this->incentive_counter    = $incentive_counter;
        $this->factor_comission     = $factor_comission;
        $this->agencia              = $agencia;
        $this->origen               = $origen;
        $this->destino              = $destino;
        $this->proveedor            = $proveedor;
    }
    
    public static function getFactorFee($current_user, $destCode = '', $proveedor = 0) {
    
        $fee = self::$fee_yc['mark_up'];
        if(isset($current_user['agency_id'])) {
            $feeUser = FeeGeneral::findAll(array('agencia' => $current_user['agency_id']));
            if (isset($feeUser[0])) {
                $fee = $feeUser[0]->factor_comission;  
            }
        }
        $agencies_not_applicate = array();
        $proveedor = FeeGeneral::find(array('proveedor' => $proveedor));
        if (isset($proveedor->factor_comission) && !in_array($current_user['agency_id'], $agencies_not_applicate)) {
            $fee = $proveedor->factor_comission;
        }
        
        return $fee;
    }

    public static function getMontoFee($current_user) {
        $fee_monto = self::$fee_yc['agency_monto'];
        if(isset($current_user['agency_id'])) {
            $feeUser = FeeGeneral::findAll(array('agencia' => $current_user['agency_id']));
            if (isset($feeUser[0])) {
                $fee_monto = $feeUser[0]->agency_monto;
            }
        }
        return $fee_monto;
    }

    public static function getComission($current_user) {
        // COMISION AGENCIA
        $fee = self::$fee_yc['agency_comission'];
        if(isset($current_user['agency_id'])) {
            $feeUser = FeeGeneral::findAll(array('agencia' => $current_user['agency_id']));
            if (isset($feeUser[0])) {
                $fee = $feeUser[0]->agency_comission;
            }
        }
        return $fee;
    }

    public static function getIncentiveUser($current_user) {
        // INCENTIVO COUNTER
        $fee = self::$fee_yc['incentive_counter'];
        if(isset($current_user['agency_id'])) {
            $feeUser = FeeGeneral::findAll(array('agencia' => $current_user['agency_id']));
            if (isset($feeUser[0])) {
                $fee = $feeUser[0]->incentive_counter;
            }
        }
        return $fee;
    }

    public static function getPriceWithFee($net_price, $current_user, $destCode = "", $proveedor = 0) {
        $factorFee  = self::getFactorFee($current_user, $destCode, $proveedor);
        $price      = $net_price / $factorFee;

        return round($price, 2);
    }

    public static function getFactorDefault($origen) {
        $query          = "SELECT * FROM FeeGeneral WHERE origen = '$origen' AND agencia = 0 AND proveedor = 0";
        $factor         = self::findByQuery($query);
        if (isset($factor[0]->id)) {
            $factor                 = $factor[0];
            $comisionCliente        = $factor->agency_comission + $factor->incentive_counter;
            $comisionOperator       = floatval($factor->operator_comission);
            $factor->totalMarckup   = (100 - ($comisionCliente + $comisionOperator))/100;
            return $factor;
        }
        $fact = (object)['agency_comission' => 12, 'incentive_counter' => 0,'totalMarckup'=>0.79];
        return $fact;
    }

    public static function getFactorFeeAgency($agencyId = "", $userId = "") {

        $arrMarckUpAgency   = [];
        if(isset($agencyId) && $agencyId != "") {
            $feeAgency = FeeGeneral::findAll(array('agencia' => $agencyId));
            if (count($feeAgency) > 0) {
                foreach ($feeAgency as $key => $ag) {
                    $agencyMarckUp = (object)[];
                    $agencyMarckUp->agency_comission    = $ag->agency_comission;
                    $agencyMarckUp->incentive_counter   = $ag->incentive_counter;
                    $costo_transferencia                = 0;
                    $proveedor                          = 0;
                    if ($ag->proveedor != 0 && $ag->proveedor != null) {
                        $proveedor              = $ag->proveedor;
                        $proveedorFound         = Provider::findById($ag->proveedor);
                        $costo_transferencia    = $proveedorFound->costo_transferencia;
                    }
                    $comisionOperator   = floatval($costo_transferencia) + floatval($ag->operator_comission);
                    if ($comisionOperator == 0) {
                        $comisionOperator = 10;
                    }
                    $comisionCliente    = $ag->agency_comission + $ag->incentive_counter; 
                    if ($comisionCliente == 0) {
                        $comisionCliente = 12;
                    }
                    $fee = (100 - ($comisionCliente + $comisionOperator))/100;
                    $agencyMarckUp->totalMarckUp    = $fee;
                    $arrMarckUpAgency[$proveedor]   = $agencyMarckUp; 
                }
            }
        }
        return $arrMarckUpAgency;
    }

    public static function getFactorFeeProvider($origen) {
        $query          = "SELECT * FROM FeeGeneral WHERE origen = '$origen' AND proveedor != 0";
        $feeProvider    = self::findByQuery($query);
        foreach ($feeProvider as $key => $value) {
            $proveedor      = Provider::findById($value->proveedor);
            $feeProvider[$key]->costo_transferencia  = $proveedor->costo_transferencia;
            $comisionOperator   = floatval($proveedor->costo_transferencia) + floatval($value->operator_comission);
            $comisionCliente    = $value->agency_comission + $value->incentive_counter; 
            $totalMarckUp       = (100 - ($comisionCliente + $comisionOperator))/100;
            $feeProvider[$key]->totalMarckup = $totalMarckUp;
        }
        return $feeProvider;
    }
}

?>
