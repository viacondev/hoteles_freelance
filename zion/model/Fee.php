<?php

class Fee extends Model {
  public $destination_code;
  public $fee;

  public static function setDestinationFee($destination_code) {
    $fee = 1;
    $destFee = Fee::find(array('destination_code' => $destination_code));
    if ($destFee) {
      $fee = $destFee->fee;
    }
    else {
      $fee = Config::findById(1)->fee;
    }

    $_SESSION['fee'] = $fee;
  }

  public static function getFee($destination_code = '') {
    $fee = 1;
    $destFee = Fee::find(array('destination_code' => $destination_code));
    if ($destFee) {
      $fee = $destFee->fee;
    }
    else {
      $fee = Config::findById(1)->fee;
    }

    return $fee;
  }
}

?>
