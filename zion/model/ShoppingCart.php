<?php

class ShoppingCart {

  public static function addItem($data) {
    $items = ShoppingCart::getItems();
    $items[] = $data;
    $_SESSION['sc_items'] = $items;

    return $items;
  }

  public static function getItems() {
    if (isset($_SESSION['sc_items'])) {
      return $_SESSION['sc_items'];
    }
    return array();
  }

  public static function getItem($id) {
    $items = ShoppingCart::getItems();
    return $items[$id];
  }

  public static function removeItem($id) {
    $items = ShoppingCart::getItems();
    unset($items[$id]);
    $_SESSION['sc_items'] = $items;
  }

  public static function getLifetime() {

  }

  public static function getItemsCount() {
    return count(ShoppingCart::getItems());
  }

  public static function emptyCart() {
    $_SESSION['sc_items'] = array();
  }
}

?>