<?php

class Discount extends Model {
  public $concept;
  public $amount;
  public $service_id;

  public function build($concept, $amount, $service_id) {
    $this->concept = $concept;
    $this->amount = $amount;
    $this->service_id = $service_id;
  }
}