<?php

class Service extends Model {
  public $type;
  public $hotel_code;
  public $name;
  public $category;
  public $localizer;
  public $reservation_id;
  public $status;
  public $destination_code;
  public $cancellation_date;
  public $cancellation_fee;
  public $checkin;
  public $checkout;
  public $time;
  public $adult_count;
  public $child_count;
  public $child_ages;
  public $price;
  public $booking_id;
  public $currency;
  public $provider_id;
  public $net_price;
  public $commission;
  public $agency_fee;
  public $incentive;
  public $includes;
  public $is_manual;
  public $show_voucher;
  public $enable_commission_suc;
  public $commission_suc;
  public $fee_monto;

  public function build($type, $hotel_code = '', $name, $category, $localizer, $reservation_id, $status, $destination_code, $cancellation_date,                    
                        $cancellation_fee, $checkin, $checkout, $time, $adult_count, $child_count, $child_ages, $price, $currency, $booking_id,
                        $provider_id, $net_price, $commission, $agency_fee, $incentive, $includes, $is_manual, $show_voucher, $enable_commission_suc = 0, 
                        $commission_suc = 0, $fee_monto = 0) {
    $this->type                   = $type;
    $this->hotel_code             = $hotel_code;
    $this->name                   = $name;
    $this->category               = $category;
    $this->localizer              = $localizer;
    $this->reservation_id         = $reservation_id;
    $this->status                 = $status;
    $this->destination_code       = $destination_code;
    $this->cancellation_date      = $cancellation_date;
    $this->cancellation_fee       = $cancellation_fee;
    $this->checkin                = $checkin;
    $this->checkout               = $checkout;
    $this->time                   = $time;
    $this->adult_count            = $adult_count;
    $this->child_count            = $child_count;
    $this->child_ages             = $child_ages;
    $this->price                  = $price;
    $this->currency               = $currency;
    $this->booking_id             = $booking_id;
    $this->provider_id            = $provider_id;
    $this->net_price              = $net_price;
    $this->commission             = $commission;
    $this->agency_fee             = $agency_fee;
    $this->incentive              = $incentive;
    $this->includes               = $includes;
    $this->is_manual              = $is_manual;
    $this->show_voucher           = $show_voucher;
    $this->enable_commission_suc  = $enable_commission_suc;
    $this->commission_suc         = $commission_suc;
    $this->fee_monto              = $fee_monto;
  }

  public function getRooms() {
    return HotelRoom::findAll(array('service_id' => $this->id));
  }

  public function getDiscounts() {
    return Discount::findAll(array('service_id' => $this->id));
  }
}

?>
