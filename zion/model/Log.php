<?php

class Log extends Model {
  public $date;
  public $level;
  public $msg;
  public $user_id;

  public static function debug($msg, $action = '') {
    Log::__log('DEBUG', $action, $msg);
  }

  public static function info($msg, $action = '') {
    Log::__log('INFO', $action, $msg);
  }

  public static function warning($msg, $action = '') {
    Log::__log('WARNING', $action, $msg);
  }

  public static function error($msg, $action = '') {
    Log::__log('ERROR', $action, $msg);
  }

  public static function __log($level, $action, $msg) {
    $log = new Log();
    $log->date = date("Y-m-d H:i:s");
    $log->level = $level;
    $log->msg = $msg;
    $log->action = $action;
    $log->user_id = isset($_SESSION['current_user']) ? $_SESSION['current_user']->id : 0;
    $log->save();
  }
}

?>