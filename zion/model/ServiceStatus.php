<?php

class ServiceStatus extends Model {
  public $status;

  // Consts that match the ServiceStatus table
  const CONFIRMED = 1;
  const PENDING = 2;
  const PAYED = 3;
  const CANCELLED = 4;
  const ANNULLED = 5;
}

?>