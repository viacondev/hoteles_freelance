<?php

class HotelMappingProvider extends Model {
  public $destination_code;
  public $zion_code;
  public $code;
  public $zone_code;
  public $provider;

  public static function getMappedCode($code) {
    $map = HotelMappingProvider::find(array('code' => $code['hotel_code'], 'provider' => $code['provider']));
    if ($map != NULL) {
      return $map->zion_code;
    }
    return NULL;
  }

  public static function getMappedCodeId($code) {
    $map = HotelMappingProvider::find(array('code' => $code['hotel_code'], 'provider' => $code['provider']));
    if ($map != NULL) {
      return $map->id;
    }
    return NULL;
  }

  public static function getHotelByZoneCode($filter)  {
    $hotels =  HotelMappingProvider::findAll(array('destination_code' => $filter['destCode'], 'zone_code' => $filter['zone_code'], 'provider' => $filter['provider']));
    if($hotels != NULL) {
      return $hotels;
    }
    return array();
  }

  public static function getHotelByZionCode($filter) {
    $map = HotelMappingProvider::find(array('zion_code' => $filter['hotel_code'], 'provider' => $filter['provider']));
    if ($map != NULL) {
      return $map->code;
    }
    return $filter['hotel_code'];
  }

  public static function getHotelMappingList($hotels) {
    $cadQuery = 'SELECT h.zion_code, h.code, h.provider FROM HotelMappingProvider h WHERE ' . $hotels;
    // __log($cadQuery);
    $map = HotelMappingProvider::findByQuery($cadQuery);
    return $map;
  }

}

?>