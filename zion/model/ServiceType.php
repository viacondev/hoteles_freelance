<?php

class ServiceType extends Model {
  public $type;

  // Consts that match the ServiceType table
  const HOTEL = 1;
  const TICKET = 2;
  const TRANSFER = 3;
  const CAR_RENT = 4;
  const FLIGHT = 5;
  const OTHER = 6;
  const ASSISTANCE = 7;
  const RENTCARS = 8;

  public static function getTypeFromHotelBedType($t) {
    if ($t == 'ServiceHotel') return ServiceType::HOTEL;
    if ($t == 'ServiceTicket') return ServiceType::TICKET;
    if ($t == 'ServiceTransfer') return ServiceType::TRANSFER;
    if ($t == 'ServiceFlight') return ServiceType::FLIGHT;
    if ($t == 'ServiceOther') return ServiceType::OTHER;
    if ($t == 'ServiceAssistance') return ServiceType::ASSISTANCE;
    if ($t == 'ServiceRentCars') return ServiceType::RENTCARS;
  }

  public static function getTypeName($id) {
    switch ($id) {
      case 1:
        return 'ServiceHotel';
      case 2:
        return 'ServiceTicket';
      case 3:
        return 'ServiceTransfer';
      case 5:
        return 'ServiceFlight';
      case 6:
        return 'ServiceOther';
      case 7:
        return 'ServiceAssistance';
      case 8:
        return 'ServiceRentCars';
      default:
        return 'Service';
    }
  }

  public static function getName($id) {
    switch ($id) {
      case 1:
        return 'Hotel';
      case 2:
        return 'Ticket';
      case 3:
        return 'Transfer';
      case 5:
        return 'Vuelo';
      case 6:
        return 'Paquete'; // 'ServiceOther' may be package most of the time
      case 7:
        return 'Asistencia al Viajero';
      case 8:
        return 'Rent Cars';
      default:
        return 'Otros';
    }
  }
}

?>