<?php

class Agency extends Model {
  public $name;
  public $address;
  public $phone;
  public $mail;
  public $booking; /* BOOK; EMIT */
  public $status; /* 1: Active; 0: Inactive */
  public $commission;
  public $extra_fee;
  public $extra_fee_type;
  public $ciudad_id;
  public $id_agency_type;

  public function build($name, $address, $phone, $mail, $booking, $status, $commission, $ciudad_id, $id_agency_type = 0) {
    $this->name     = $name;
    $this->address  = $address;
    $this->phone    = $phone;
    $this->mail     = $mail;
    $this->booking  = $booking;
    $this->status   = $status;
    $this->commission = $commission;
    $this->ciudad_id  = $ciudad_id;
    $this->id_agency_type = $id_agency_type;
  }

  public static function findAll($params = array(), $orderBy = '') {
    return parent::findAll($params, 'name ASC');
  }

  public static function findAllNameOnly() {
    return parent::findByQuery('SELECT id, name FROM Agency WHERE id_agency_type = 2 AND status = 1 ORDER BY name ASC');
  }

  public static function findByCity($ciudad_id) {
    return parent::findByQuery('SELECT id, name FROM Agency WHERE ciudad_id = ' . $ciudad_id);
  }

  public function updateLogo($logo) {
    if ($logo) {
      $this->updateColumn('logo', file_get_contents($logo));

      // Copy the file to our filesystem
      $upload_dir = 'assets/images/resources/agency_logos/';
      move_uploaded_file($logo, $upload_dir . $this->id);
    }
  }

  public function showLogo() {
    echo '<img src="http://barrybolivia.com/zion/assets/images/resources/agency_logos/' . $this->id . '"
            onerror="if (this.src != \'assets/images/agency_no_logo.png\') this.src = \'assets/images/agency_no_logo.png\';" />';
  }

  public function get_url_logo() {
    $url = "http://barrybolivia.com/zion/assets/images/resources/agency_logos/" . $this->id;
    return $url;
  }
  /**
   * Get the price that will be displayed to the user.
   */
  public static function getUserPrice($price, $include_extra_fee = true) {
    $agency = Agency::getCurrent();
    if (!isset($_SESSION['fee']) || $_SESSION['fee'] == 0) {
      $_SESSION['fee'] = Config::findById(1)->fee;
    }
    $price = $price / $_SESSION['fee'];

    $extra_fee = 0;
    if ($include_extra_fee && $agency->extra_fee_type == 'PERC' && isset($agency->extra_fee) && $agency->extra_fee != 0) {
      $extra_fee = $price * ($agency->extra_fee / 100);
    }

    $price += $extra_fee;
    return round($price, 2);
  }

  public static function getUserPriceRest($price, $destination_code = '', $agencyId) {
    $agency = Agency::getCurrentRest($agencyId);
    $fee = Fee::getFee($destination_code);
    $price = $price / $fee;
    $extra_fee = 0;
    if ($agency->extra_fee_type == 'PERC' && isset($agency->extra_fee) && $agency->extra_fee != 0) {
      $extra_fee = $price * ($agency->extra_fee / 100);
    }

    $price += $extra_fee;
    return round($price, 2);
  }

  public static function getAgencyFee($price) {
    $agency = Agency::getCurrent();
    if ($agency->extra_fee_type != 'PERC' || !isset($agency->extra_fee) || $agency->extra_fee == 0) {
      return 0;
    }

    return $price * ($agency->extra_fee / 100);
  }

  public function getAgencyFeeRest($price, $agencyId) {
    $agency = Agency::getCurrentRest($agencyId);
    if ($agency->extra_fee_type != 'PERC' || !isset($agency->extra_fee) || $agency->extra_fee == 0) {
      return 0;
    }

    return $price * ($agency->extra_fee / 100);
  }

  public static function getCurrent() {
    return Agency::findById($_SESSION['current_user']->agency_id);
  }

  public static function getCurrentRest($userId) {
    return Agency::findById($userId);
  }

  public static function getAgencyWithFee() {
    return parent::findByQuery('SELECT a.id, a.name, a.status, a.booking, f.agency_comission as commission FROM Agency a,FeeGeneral f WHERE a.id = f.agencia ORDER BY a.name ASC');
  }

  public static function isAgencySucursal($agency) {
    // RETURN TRUE IF AGENCY IS SUCURSAL CBB O LPB EXCLUYE VIACON CBB AND LPB
    $agencyId   = $agency->id;
    $manager    =  AgencyManager::getAgencyManager($agencyId);
    if (count($manager) > 0 && $agency->ciudad_id == 28 && 
        $agency->id != $manager[0]->id || count($manager) > 0 && 
        $agency->ciudad_id == 30 && $agency->id != $manager[0]->id) {
        return true;
    }
    return false;
  }

    public static function getUserPriceWithFee($price, $fees) {
        $price       = round($price / 0.89, 2);
        $total_prc   = 0;
        $total_monto = 0;
        if (property_exists($fees, 'fee_prc') && $fees->fee_prc != 0) {
            $fee_porcentaje = floatval($fees->fee_prc);
            $total_prc      = $price * ($fee_porcentaje / 100);
        }
        if (property_exists($fees, 'fee_monto') && $fees->fee_monto != 0) {
            $total_monto = floatval($fees->fee_monto);
        }
        return round($price + $total_monto + $total_prc, 2);
    }

}
