<?php

include_once(__DIR__ . '/Model.php');

class Profiling extends Model {
  public $init;
  public $end;
  public $time; // $end - $init (miliseconds)
  public $action;
  public $origin;
  public $size;
  public $userId;

  public function __construct($action = '', $origin = 'Server') {
    if (!isset($this->action)) {
      $this->action = $action;
    }
    if (!isset($this->origin)) {
      $this->origin = $origin;
    }
    if (!isset($this->userId) && isset($_SESSION['current_user'])) {
      $this->userId = $_SESSION['current_user']->id;
    }
    else { // TODO: Temp: assign user
      $this->userId = '17';
    }
  }

  public function init() {
    $this->init = date("Y-m-d H:i:s");
  }

  public function end($size = NULL) {
    $this->end = date("Y-m-d H:i:s");
    $this->size = $size;

    $this->profile();
  }

  public function profile() {
    $this->time = strtotime($this->end) - strtotime($this->init);
    $this->save();
  }
}

?>
