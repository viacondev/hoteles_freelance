<?php

class ServiceTransfer extends Model {
  public $origin;
  public $destination;
  public $type;
  public $time_in;
  public $time_out;
  public $flight_in;
  public $flight_out;
  public $service_id;

  public function build($origin, $destination, $type, $time_in, $time_out, $flight_in, $flight_out, $service_id) {
    $this->origin = $origin;
    $this->destination = $destination;
    $this->type = $type;
    $this->time_in = $time_in;
    $this->time_out = $time_out;
    $this->flight_in = $flight_in;
    $this->flight_out = $flight_out;
    $this->service_id = $service_id;
  }
}

?>