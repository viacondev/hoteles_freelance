<?php

class MotorConexion extends Model
{
    // const url = 'http://localhost/hotel-ws-v2/public/'; // Local
    const url = 'https://agente.barrybolivia.com/hotel-ws-v2/public/'; //production
    private $method;
    private $params;

    public function build($method = '', $params = [])
    {
        $this->method = $method;
        $this->params = $params;
    }

    public function consultar()
    {
        $ch = curl_init();
        curl_setopt_array(
            $ch,
            array(
                CURLOPT_URL => self::url . $this->method,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($this->params),
                CURLOPT_HTTPHEADER => array('Content-Type:application/json'),
                CURLOPT_USERAGENT => $_SERVER['HTTP_USER_AGENT']
            )
        );
        $xml = curl_exec($ch);
        return json_decode($xml);
    }
}