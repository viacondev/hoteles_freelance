<?php

class BackHotelsAvail extends Model {
  public $update_date;
  public $result;
  public $token_avail;

  public function build($creationdate, $updatedate, $result) {
    $this->update_date   = new DateTime('now');
    $this->result        = "";
    $this->token_avail   = "";
  }

  public function getResults($token) {
    $res = BackHotelsAvail::find(array('token_avail' => $token));
    if (!empty($res->result)) {
      $resDecode = gzinflate($res->result);
      $resObj    = json_decode($resDecode);
      return $resObj;
    }
    return "";
  }

  public function generateToken($longitud = 8) {
    if ($longitud < 4) {
        $longitud = 4;
    }
    $token = bin2hex(openssl_random_pseudo_bytes(($longitud - ($longitud % 2)) / 2));
    return $token;
  }

}

?>
