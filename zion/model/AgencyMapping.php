<?php
/**
 *
 */
class AgencyMapping extends Model
{
  public $agencybb;
  public $agencybarry;
  public $agencyname;
  public $id_agency_type;

  function __construct()
  {
    # code...
  }

  public function build($agency_bb, $agency_barry, $agency_name, $id_agency_type = 0) {
    $this->agencybb       = $agency_bb;
    $this->agencybarry    = $agency_barry;
    $this->agencyname     = $agency_name;
    $this->id_agency_type = $id_agency_type;
  }
}

?>
