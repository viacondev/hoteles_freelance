<?php

class Ciudad extends Model {
  public $codigo_iata;
  public $nombre;
  public $user_id;

  public function build($codigo_iata, $nombre, $user_id) {
    $this->codigo_iata = $codigo_iata;
    $this->nombre      = $nombre;
    $this->user_id     = $user_id;
  }

  public static function findAll($params = array(), $orderBy = '') {
    return parent::findAll($params, 'name ASC');
  }

  public static function findAllNameOnly() {
    return parent::findByQuery('SELECT id, name FROM Agency WHERE status = 1 ORDER BY name ASC');
  }

}
