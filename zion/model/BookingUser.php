<?php

class BookingUser extends Model {
  public $id_cliente;
  public $user_id;
  public $booking_id;
  public $nombre_apellido;

  public static function buid($id_cliente, $booking_id, $nombre_apellido, $user_id = '') {
    $this->id_cliente      = $id_cliente;
    $this->booking_id      = $booking_id;
    $this->nombre_apellido = $nombre_apellido;
    $this->user_id         = $user_id;
  }
}

?>