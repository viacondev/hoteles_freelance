<?php

class HotelTopTen extends Model {
  public $zion_code;
  public $reserved;

  public static function getMappedCode($code) {
    $map = HotelMappingProvider::find(array('code' => $code['hotel_code'], 'provider' => $code['provider']));
    if ($map != NULL) {
      return $map->zion_code;
    }
    return NULL;
  }
}

?>