<?php

class AgencyManager extends Model {
  public $agency_id;
  public $state;

  public function build($agency_id, $state) {
    $this->agency_id   = $agency_id;
    $this->state  = $state;
  }

  public static function getAgencyManager($agency_id) {
    $agency = Agency::findById($agency_id);
    $agency_manager = parent::findByQuery("SELECT a.id, a.name FROM Agency a, Ciudad c, AgencyManager am WHERE a.ciudad_id = $agency->ciudad_id AND am.agency_id = a.id AND a.ciudad_id = c.id");

    return $agency_manager;
  }

}
