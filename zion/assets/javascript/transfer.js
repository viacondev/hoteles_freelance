function initTransferDestinationAutocomplete() {
  $("#destination").autocomplete({
    minLength: 3,
    source: dests,
    select: function(event, ui) {
        $('#destination').val(ui.item.label);
        $('#destination_code').val(ui.item.c);
        loadTerminals(ui.item.c);
        loadHotels(ui.item.c);
        return false;
      }
  }).data("ui-autocomplete")._renderItem = function(ul, item) {
      var search = $('#destination').val();
      var exp = new RegExp('(' + search + ')', 'ig');
      var label = item.label.replace(exp, '<b>$1</b>');
      var icon = '<i class="fa fa-map-marker text-muted"></i> &nbsp; ';

      return $("<li></li>")
        .append("<a>" + icon + label + "</a>")
        .appendTo(ul);
    };
}

function initCalendarsTransfer() {
  $('#dateIn').datepicker({
    numberOfMonths: 2,
    minDate: 0,
    onClose: function(selectedDate) {
      $("#dateOut").datepicker("option", "minDate", selectedDate);
    }
  });

  $('#dateOut').datepicker({
    numberOfMonths: 2
  });

  $("#dateIn").datepicker("option", "dateFormat", 'dd/mm/yy');
  $("#dateOut").datepicker("option", "dateFormat", 'dd/mm/yy');
}

function loadTerminals(destination_code, setDefaultValue) {
  $.getJSON(
    "helpers/ajax/load_terminals.php",
    { destination_code: destination_code },
    function(result) {
      var tIn = $("#terminalCodeIn");
      var tOut = $("#terminalCodeOut");
      tIn.html('');
      tOut.html('');
      tIn.append('<option value="" class="text-muted" default>Seleccione una terminal</option>')
      tOut.append('<option value="" class="text-muted" default>Seleccione una terminal</option>');
      $.each(result, function() {
        tIn.append($("<option />").val(this.code).text(this.name + ' (' + this.code + ')'));
        tOut.append($("<option />").val(this.code).text(this.name + ' (' + this.code + ')'));
      });
      if (setDefaultValue) {
        $('#terminalCodeIn').val(terminalCodeIn);
        $('#terminalCodeOut').val(terminalCodeOut);
      }
    });
}

function loadHotels(destination_code, setDefaultValue) {
  hotels = [];
  $.getJSON(
    "helpers/ajax/load_hotels.php",
    { destination_code: destination_code },
    function(result) {
      hotels = result;
      var hIn = $("#hotelCodeIn");
      var hOut = $("#hotelCodeOut");
      hIn.html('');
      hOut.html('');
      hIn.append('<option value="" class="text-muted" default>Seleccione un hotel</option>')
      hOut.append('<option value="" class="text-muted" default>Seleccione un hotel</option>');
      $.each(hotels, function() {
        hIn.append($("<option />").val(this.code + '-' + this.zone_code).text(this.name));
        hOut.append($("<option />").val(this.code + '-' + this.zone_code).text(this.name));
      });
      if (setDefaultValue) {
        $('#hotelCodeIn').val(hotelCodeIn);
        $('#hotelCodeOut').val(hotelCodeOut);
      }
    });
}

function changeTypeIn() {
  if ($('#typeIn').val() == 'T') {
    $('#terminalIn').show();
    $('#hotelIn').hide();
    $('#typeOut').html('<option value="T">Terminal</option><option value="H">Hotel</option>');
    $('#dateTimeIn').show();
    $('#roudTripDiv').show();
  }
  else {
    $('#terminalIn').hide();
    $('#hotelIn').show();
    $('#typeOut').html('<option value="T">Terminal</option>');
    $('#dateTimeIn').hide();
    $('#roudTripDiv').hide();
  }
  changeTypeOut();
  changeRoundTrip();
}

function changeTypeOut() {
  if ($('#typeOut').val() == 'T') {
    $('#terminalOut').show();
    $('#hotelOut').hide();
  }
  else {
    $('#terminalOut').hide();
    $('#hotelOut').show();
  }
  changeRoundTrip();
}

function changeRoundTrip() {
  var round = $('#roundTrip').is(':checked');
  var inType = $('#typeIn').val();
  var outType = $('#typeOut').val();

  if (round) {
    if (inType == 'T') {
      $('#dateTimeIn').show();
      $('#dateTimeOut').show();
    }
  }
  else {
    if (inType == 'T' && outType == 'T') {
      $('#dateTimeIn').hide();
      $('#dateTimeOut').show();
    }
    else if (inType == 'T' && outType == 'H') {
      $('#dateTimeIn').show();
      $('#dateTimeOut').hide();
    }
    else if (inType == 'H') {
      $('#dateTimeIn').hide();
      $('#dateTimeOut').show();
    }
  }
}

function showChildAgesConfig() {
  var html = '<label>Edad de los ni&ntilde;os:</label><br />';
  childCount = parseInt($('#childCount').val());

  for (var i = 1; i <= childCount; i++) {
    var lastValue = $('#childAge' + i).val() ? $('#childAge' + i).val() : '';
    html += '<input type="text" size="2" maxlength="2" name="childAge' + i + '" id="childAge' + i + '" value="' + lastValue + '" />';
  }
  html = childCount == 0 ? '' : html;

  $('#child_ages').html(html);
}

/**
 * The following vars need to be set before calling this method:
 *  - typeIn, typeOut
 */
function restoreTransferSearchValues() {
  $('#typeIn').val(typeIn);
  changeTypeIn();
  $('#typeOut').val(typeOut);
  changeTypeOut();
  $('#roundTrip').prop('checked', roundTrip);
  changeRoundTrip();
  $('#adultCount').val(adultCount);
  $('#childCount').val(childCount);
  $('#dateIn').val(dateIn);
  $('#dateOut').val(dateOut);
  $('#hourIn').val(hourIn);
  $('#hourOut').val(hourOut);
  $('#minsIn').val(minsIn);
  $('#minsOut').val(minsOut);

  showChildAgesConfig();
  for (var i = 1; i < childAges.length; i++) {
    $('#childAge' + i).val(childAges[i]);
  }

  loadTerminals(destination_code, true);
  loadHotels(destination_code, true);
}
