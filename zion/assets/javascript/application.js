function initDestinationAutocomplete() {
  $("#destination").autocomplete({
    minLength: 3,
    source: dests,
    select: function(event, ui) {
      $('#destination').val(ui.item.label);
      $('#destination_code').val(ui.item.c);
      return false;
    },
    change: function() {
      if ($('#destination').val() == '') {
        $('#destination_code').val('');
      }
    }
  }).data("ui-autocomplete")._renderItem = function(ul, item) {
      var search = $('#destination').val();
      var exp = new RegExp('(' + search + ')', 'ig');
      var label = item.label.replace(exp, '<b>$1</b>');
      var icon = '<i class="fa fa-map-marker text-muted"></i> &nbsp; ';

      return $("<li></li>")
        .append("<a>" + icon + label + "</a>")
        .appendTo(ul);
    };
}

function initCalendars() {
  $.datepicker.regional['es'] =
  	{
	  	closeText: 'Cerrar',
	  	prevText: 'Previo',
	  	nextText: 'Próximo',
	  	monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
	  	monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
	  	monthStatus: 'Ver otro mes', yearStatus: 'Ver otro año',
	  	dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
	  	dayNamesShort: ['Dom','Lun','Mar','Mie','Jue','Vie','Sáb'],
	  	dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sa'],
	  	dateFormat: 'dd/mm/yy', firstDay: 0,
	  	initStatus: 'Selecciona la fecha', isRTL: false
	 };
	 $.datepicker.setDefaults($.datepicker.regional['es']);
  $('#checkin').datepicker({
    numberOfMonths: 2,
    minDate: 0,
    onClose: function(selectedDate) {
      $("#checkout").datepicker("option", "minDate", selectedDate);
      $("#checkout").datepicker('show');
    }
  });

  $('#checkout').datepicker({
    numberOfMonths: 2,
    onClose: function(selectedDate) {
      setNumberOfNights();
    }
  });

  $("#checkin").datepicker("option", "dateFormat", 'dd/mm/yy');
  $("#checkout").datepicker("option", "dateFormat", 'dd/mm/yy');
}

/**
 * Set number of nights based on the checkout date (checkout - checkin)
 */
function setNumberOfNights() {
  var checkin = $('#checkin').datepicker("getDate");
  var checkout = $('#checkout').datepicker("getDate");
  var days = (checkout - checkin) / (1000 * 60 * 60 * 24);
  $("#nights_count").html("(" + parseInt(days, 10) + " noches)");
}

function toggleFilter(e) {
  $(e).children('.colapse-down').toggle();
  $(e).children('.colapse-right').toggle();
  $(e).next('.items').slideToggle();
}

function showSendMail(subject, div) {
  $('#mailing-subject').val(subject);
  $('#mailing-div').val(div);
  $('#mailing-modal').modal('show');
}

function printDiv(div) {
  // printThis has beed modified to load the content for 1333 ms
  $('#' + div).printThis();
  showProgress();
  setTimeout(function() { hideProgress(); }, 1333);
}

var spinnerVisible = false;
function showProgress() {
  if (!spinnerVisible) {
    grayoutPage();
    $("div#spinner").fadeIn("fast");
    spinnerVisible = true;
  }
}
function hideProgress() {
  if (spinnerVisible) {
    removeGrayoutPage();
    var spinner = $("div#spinner");
    spinner.stop();
    spinner.fadeOut("fast");
    spinnerVisible = false;
  }
}

function grayoutPage() {
    $('body').append('<div id="screen_black" style="position: absolute; left: 0; top: 0; background: #000; z-index: 1500"></div>');
    $('#screen_black').css({ opacity: 0.7, 'width':$(document).width(),'height':$(document).height()});
    $('body').css({'overflow':'hidden'});
}

function removeGrayoutPage() {
    $('#screen_black').remove();
    $('body').css({'overflow':'visible'});
}

function initLoadingMsg() {
  //$('.loadingAction').on('click', function() { showProgress(); });
}

function getLinkGlobal() {
  return 'https://yocounter.com/hoteles_freelance'
  // return 'http://localhost/hoteles_freelance'
  // return 'http://localhost:8080/agente_independiente/hoteles_freelance'
}

// Set required (<input ... required>) message
document.addEventListener("DOMContentLoaded", function() {
  var elements = document.getElementsByTagName("INPUT");
  for (var i = 0; i < elements.length; i++) {
    elements[i].oninvalid = function(e) {
      e.target.setCustomValidity("");
      if (!e.target.validity.valid) {
        e.target.setCustomValidity("No puedes dejar este campo en blanco");
      }
    };
    elements[i].oninput = function(e) {
      e.target.setCustomValidity("");
    };
  }
})

$(function() {
    
    var docElem = document.documentElement,
    didScroll = false,
    changeHeaderOn = 100;
    document.querySelector( 'header' );
        
    function init() {
        window.addEventListener( 'scroll', function() {
          if( !didScroll ) {
            didScroll = true;
            setTimeout( scrollPage, 250 );
          }
        }, false );
    }
      
    function scrollPage() {
        var sy = scrollY();
        if ( sy >= changeHeaderOn ) {
          if(!$($('.top-bar')[0]).attr('id'))
          {
            $('.top-bar').slideUp(300);
          }
          $("header").addClass("fixed-header");
          $(".navbar").removeClass("navbar-on-top")
          $(".img-logo-full").removeClass("hidden")
          $(".img-logo-full-white").addClass("hidden")
          $('.navbar-brand').css({ 'padding-top' : 5 + "px", 'padding-bottom' : 5 + "px" });
          
          if (/iPhone|iPod|BlackBerry/i.test(navigator.userAgent) || $(window).width() < 479 ){
            $('.navbar-default .navbar-nav > li > a').css({ 'padding-top' : 0 + "px", 'padding-bottom' : 0 + "px" })
          }else{
            $('.navbar-default .navbar-nav > li > a').css({ 'padding-top' : 23 + "px", 'padding-bottom' : 23 + "px" })
            $('.search-side').css({ 'margin-top' : -7 + "px" });
          };
          
        }
        else {
          if(!$($('.top-bar')[0]).attr('id'))
          {
            $('.top-bar').slideDown(300);
          }
          $("header").removeClass("fixed-header");
          $(".navbar").addClass("navbar-on-top")
          $(".img-logo-full").addClass("hidden")
          $(".img-logo-full-white").removeClass("hidden")
          //$('.navbar-brand').css({ 'padding-top' : 7 + "px", 'padding-bottom' : 7 + "px" });
          
          if (/iPhone|iPod|BlackBerry/i.test(navigator.userAgent) || $(window).width() < 479 ){
            $('.navbar-default .navbar-nav > li > a').css({ 'padding-top' : 0 + "px", 'padding-bottom' : 0 + "px" });
          }else{
            $('.navbar-default .navbar-nav > li > a').css({ 'padding-top' : 22 + "px", 'padding-bottom' : 22 + "px" });
            $('.navbar-default .navbar-right > li > a').css({ 'padding-top' : 12 + "px", 'padding-bottom' : 12 + "px" });
            $('.search-side').css({ 'margin-top' : 0  + "px" });
          };
          
        }
        didScroll = false;
    }
      
    function scrollY() {
        return window.pageYOffset || docElem.scrollTop;
    }
      
    init();
        
});
