
function copyFirstCustomerData(services_count, pax_count) {
  for (var i = 2; i <= services_count; i++) {
    for (var j = 1; j <= pax_count; j++) {
      var name = $('input[name="customerName_1_' + j + '"]').val();
      var lastname = $('input[name="customerLastName_1_' + j + '"]').val();
      $('input[name="customerName_' + i + '_' + j + '"]').val(name);
      $('input[name="customerLastName_' + i + '_' + j + '"]').val(lastname);
    }
  }
}

function load_googlemap(id, latitude, longitude, dir) {
  var pos = new google.maps.LatLng(latitude, longitude);
  var mapOptions = {
    center: pos,
    zoom: 15,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(document.getElementById(id), mapOptions);
  var marker = new google.maps.Marker({ position: pos, title: dir });
  marker.setMap(map);
}

function checkAgreement(event) {
  if (!$('#agreement_check').is(':checked')) {
    alert('Debes aceptar las condiciones generales para confirmar la reserva');
    return false;
  }
  showProgress();
  return true;
}

function initCalendarsFilter() {
  $('#checkin').datepicker({
    numberOfMonths: 2,
    onClose: function(selectedDate) {
      $("#checkout").datepicker("option", "minDate", selectedDate);
      $("#checkout").datepicker('show');
    }
  });

  $('#checkout').datepicker({
    numberOfMonths: 2
  });

  $("#checkin").datepicker("option", "dateFormat", 'dd/mm/yy');
  $("#checkout").datepicker("option", "dateFormat", 'dd/mm/yy');
}

/**
 * Shortcuts for more common searchs
 */
function filterBooks(status, datesFilter, checkin, checkout) {
  $('#status').val(status);
  $('#datesFilter').val(datesFilter);
  $('#checkin').val(checkin);
  $('#checkout').val(checkout);
  $('#search_books').click();
  //$('#search_books_form').submit();
}


/**
 * Methods for Booking add & edit
 */
function initServiceDestAutocomplete(id) {
  if (!$("#destination_" + id).length) return;

  $("#destination_" + id).autocomplete({
    minLength: 3,
    source: dests,
    select: function(event, ui) {
      $('#destination_' + id).val(ui.item.label);
      $('#destination_code_' + id).val(ui.item.c);
      return false;
    },
    change: function() {
      if ($('#destination_' + id).val() == '') {
        $('#destination_code_' + id).val('');
      }
    }
  }).data("ui-autocomplete")._renderItem = function(ul, item) {
      var search = $('#destination_' + id).val();
      var exp = new RegExp('(' + search + ')', 'ig');
      var label = item.label.replace(exp, '<b>$1</b>');
      var icon = '<i class="fa fa-map-marker text-muted"></i> &nbsp; ';

      return $("<li></li>")
        .append("<a>" + icon + label + "</a>")
        .appendTo(ul);
    };
}

function initServiceCalendars(id) {
  var checkin = $('#checkin_' + id).val();
  var checkout = $('#checkout_' + id).val();
  var cancellation_date = $('#cancellation_date_' + id).val();

  $('#checkin_' + id).datepicker({
    numberOfMonths: 2,
    onClose: function(selectedDate) {
      $('#checkout_' + id).datepicker("option", "minDate", selectedDate);
      $('#checkout_' + id).datepicker('show');
    }
  });

  $('#checkout_' + id).datepicker({
    numberOfMonths: 2
  });

  $('#cancellation_date_' + id).datepicker({
    numberOfMonths: 2
  });

  $('#checkin_' + id).datepicker("option", "dateFormat", 'dd/mm/yy');
  $('#checkout_' + id).datepicker("option", "dateFormat", 'dd/mm/yy');
  $('#cancellation_date_' + id).datepicker("option", "dateFormat", 'dd/mm/yy');

  $('#checkin_' + id).val(checkin);
  $('#checkout_' + id).val(checkout);
  $('#cancellation_date_' + id).val(cancellation_date);
}

function loadAgencyUsers(agency_id, user_id) {
  $.getJSON(
    "helpers/ajax/load_agency_users.php",
    { agency_id: agency_id },
    function(result) {
      var options = $("#user_id");
      options.html('');
      options.append('<option default value="-1"></option>')
      $.each(result, function() {
        options.append($("<option />").val(this.id).text(this.name + ' ' + this.lastname));
      });
      if (user_id) {
        $('#user_id').val(user_id);
      }
    });
}

function loadAgencyUsers_books(agency_id, user_id) {
  $.getJSON(
    "helpers/ajax/load_agency_users.php",
    { agency_id: agency_id },
    function(result) {
      var options = $("#user_id");
      options.html('');
      options.append('<option value="">Todos</option>')
      $.each(result, function() {
        options.append($("<option />").val(this.id).text(this.name + ' ' + this.lastname));
      });
      if (user_id) {
        $('#user_id').val(user_id);
      }
    });
}

function toggleManualAgency() {
  if ($('#manual_agency').is(':checked')) {
    $('#agency').show();
    $('#manual_agent').prop('checked', true);
    $('#agent').show();
  }
  else {
    $('#agency').hide();
    $('#manual_agent').prop('checked', false);
    $('#agent').hide();
  }
}

function toggleManualAgent() {
  if ($('#manual_agent').is(':checked')) {
    $('#agent').show();
  }
  else if ($('#manual_agency').is(':checked')) {
    $('#manual_agent').prop('checked', true);
  }
  else {
    $('#agent').hide();
  }
}

function addService(service, id) {
  var s = parseInt($('#services_count').val()) + 1;
  var url = 'views/booking/add_service/_add_' + service + '.php';
  $.ajax({
      url: url,
      data: {
        s: s,
        id: id
      }
    })
    .done(function(res) {
      $('#services').append(res);
      initServiceDestAutocomplete(s);
      initServiceCalendars(s);

      $("#add_book_form :input[type=text]").on("keypress", function(e) {
        return e.keyCode != 13;
      });
    });

  $('#services_count').val(s);
}

function addRoom(service, id) {
  var r = parseInt($('#room_count_' + service).val()) + 1;
  $.ajax({
      url: "views/booking/add_service/_add_room.php",
      data: {
        service_number: service,
        room_number: r,
        id: id
      }
    })
    .done(function(res) {
      $('#rooms_' + service + ' tr:last').after(res);
    });

  $('#room_count_' + service).val(r);
}

function updateRoomPrice(service, room) {
  var fee = parseFloat($('#fee_' + service).val());
  var net = parseFloat($('#net_price_' + service + '_' + room).val());
  if ($('#net_price_' + service + '_' + room).val() != '') {
    var price = net / fee;
    price = Math.round(price * 100) / 100;
    $('#price_' + service + '_' + room).val(price);
    updatePriceFromRooms(service);
  }
  updateNetPriceFromRooms(service);
}

function updateRoomNetPrice(service, room) {
  var fee = parseFloat($('#fee_' + service).val());
  var price = parseFloat($('#price_' + service + '_' + room).val());
  if ($('#price_' + service + '_' + room).val() != '' && $('#net_price_' + service + '_' + room).val() == '') {
    net = price * fee;
    net = Math.round(net * 100) / 100;
    $('#net_price_' + service + '_' + room).val(net);
    updateNetPriceFromRooms(service);
  }
  updatePriceFromRooms(service);
}

function updateServicePrice(service) {
  var fee = parseFloat($('#fee_' + service).val());
  var net = parseFloat($('#net_price_' + service).val());
  if ($('#net_price_' + service).val() != '') {
    var price = net / fee;
    price = Math.round(price * 100) / 100;
    $('#price_' + service).val(price);
  }
}

function updateNetPriceFromRooms(service) {
  var r = parseInt($('#room_count_' + service).val());
  var total = 0;
  for (var i = 1; i <= r; i++) {
    if (!$('#room_' + service + '_' + i).length || $('#net_price_' + service + '_' + i).val() == '') {
      continue;
    }
    total += parseFloat($('#net_price_' + service + '_' + i).val());
  }
  $('#net_price_' + service).val(total);
}

function updatePriceFromRooms(service) {
  var r = parseInt($('#room_count_' + service).val());
  var total = 0;
  for (var i = 1; i <= r; i++) {
    if (!$('#room_' + service + '_' + i).length || $('#price_' + service + '_' + i).val() == '') {
      continue;
    }
    total += parseFloat($('#price_' + service + '_' + i).val());
  }
  $('#price_' + service).val(total);
}

function changeInOut(service) {
  if ($('#transfer_type_' + service).val() == 'IN_OUT') {
    $('#transfer_out_' + service).show();
  }
  else {
    $('#transfer_out_' + service).hide();
  }
}

function load_Hotel(i, cad){
  cad += "Hotel: \n\t" + $('#name_' + i).val() + ' (' + $('#category_' + i).val() + ") - Desde: " + $('#checkin_' + i).val() + " - Hasta: " + $('#checkout_' + i).val() + " ( Localizer: " + $('#localizer_' + i).val() + " )\n";
  room_cn = $('#room_count_' + i).val();
  if(room_cn != 0){
    regimen=0, adultcon=0, childcon=0;
    for(j = 1;j <= room_cn; j++){
      cad += "\tTipo: " + $('#type_' + i + '_' + j).val();
      cad += " - Régimen: " + $('#board_' + i +'_' + j).val();
      adultcon = parseInt($('#adult_count_' + i +'_' + j).val());
      childcon = parseInt($('#child_count_' + i +'_' + j).val());
      if(adultcon == 1){ cad += " - " + adultcon +" Adulto";}else{ cad += " - " + adultcon + " Adultos"; }
      if(childcon == 1){ cad += ", " + childcon + " niño";}else if(childcon != 0) { cad += ", " + childcon + " niños"; }
      cad += "\n";
    }
  } 
  if($('#includes_' +i).val() != ''){
    cad += "\tObservaciones: \n";
    var lines = $('#includes_'+i).val().split('\n');
    for(var i = 0;i < lines.length;i++){
      cad += "\t" + lines[i] + "\n";
    }
  }
  return cad;
}

function load_Ticket(i, cad){
  cad += "Ticket: \n\t" +  $('#name_' + i).val() + " - Destino: " + $('#destination_' +i).val() + " ( Localizer: " + $('#localizer_' + i).val() + " )" + "\n";
  cad += "\tFecha: " + $('#checkin_' + i).val() + " - " + $('#adult_count_' + i).val() + " Adultos"
  if ($('#child_count_'+ i).val() != 0) { cad += ", " + $('#child_count_'+ i).val() + "niños" }
  cad += "\n"; 
  if($('#includes_' +i).val() != ''){
    cad += "\tObservaciones: \n";
    var lines = $('#includes_'+i).val().split('\n');
    for(var i = 0;i < lines.length;i++){
      cad += "\t" + lines[i] + "\n";
    }
  }
  return cad;
}

function load_Transfer(i, cad){
  cad += "Transfer: \n\t" + $('#name_' +i).val() +  " - Destino: " + $('#destination_' +i).val() + " ( Localizer: " + $('#localizer_' + i).val() + " )" +  "\n";
  cad += "\tDesde: "+ $('#from_' + i).val() + " - Hasta: " + $('#to_' + i).val() + " ( "+ "Llegada: " + $('#checkin_' +i).val() + ", Nro. Vuelo: " + $('#flight_in_' +i).val() + ", Hora de Llegada: " + $('#hourIn_'+i).val() + ":" + $('#minsIn_'+i).val() +" ) \n";
  if ($("#transfer_type_"+i).val() == "IN_OUT") {
    cad += "\tDesde: "+ $('#to_' + i).val() + " - Hasta: " + $('#from_' + i).val() + " ( "+ "Salida: " + $('#checkout_' +i).val() + ", Nro. Vuelo: " + $('#flight_out_' +i).val() + ", Hora de Salida: " + $('#hourOut_'+i).val() + ":" + $('#minsOut_'+i).val() +" ) \n";
  }
  cad += "\t" + $('#adult_count_' +i).val() +" Adultos";
  if ($('#child_count_'+ i).val() != 0) { cad += ", " + $('#child_count_'+ i).val() + " niños" }
  cad += "\n";
  if($('#includes_' +i).val() != ''){
    cad += "\tObservaciones: \n";
    var lines = $('#includes_'+i).val().split('\n');
    for(var i = 0;i < lines.length;i++){
      cad += "\t" + lines[i] + "\n";
    }
  }
  return cad;
}

function load_Flight(i, cad){
  cad += "Aereo: \n\t" + $('#name_' + i).val() + " - Linea Aerea:" +$('#category_' + i).val() + " ( PNR: "+ $('#localizer_' + i).val() +" ) \n";
  cad += "\tFecha Salida: " + $('#checkin_' +i).val() + " - " + $('#adult_count_' +i).val() +" Adultos";
  if ($('#child_count_'+ i).val() != 0) { cad += ", " + $('#child_count_'+ i).val() + " niños\n" }
  else{ cad += "\n"; } 
  if($('#includes_' +i).val() != ''){
    cad += "\tObservaciones: \n";
    var lines = $('#includes_'+i).val().split('\n');
    for(var i = 0;i < lines.length;i++){
      cad += "\t" + lines[i] + "\n";
    }
  }
  return cad;
}

function load_Other(i, cad){
  cad += $('#name_' + i).val() + " ( Localizer: "+ $('#localizer_' + i).val() +" ) \n";
  if($('#includes_' +i).val() != ''){
    cad += "\tObservaciones: \n";
    //cad += "\t"+ $('#includes_' +i).val() + "\n";
    var lines = $('#includes_'+i).val().split('\n');
    for(var i = 0;i < lines.length;i++){
      cad += "\t" + lines[i] + "\n";
    }
  }
  return cad;
}