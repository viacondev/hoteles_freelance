function initCalendarsTicket() {
  $('#dateFrom').datepicker({
    numberOfMonths: 2,
    minDate: 0,
    onClose: function(selectedDate) {
      $("#dateTo").datepicker("option", "minDate", selectedDate);
      $("#dateTo").datepicker('show');
    }
  });

  $('#dateTo').datepicker({
    numberOfMonths: 2,
    onClose: function(selectedDate) {
      setNumberOfNightsTicket();
    }
  });

  $("#dateFrom").datepicker("option", "dateFormat", 'dd/mm/yy');
  $("#dateTo").datepicker("option", "dateFormat", 'dd/mm/yy');
}

function updateDuration(element, ticket_avail_id) {
  var days = $(element).find(':selected').data('maxduration');
  $('#days_' + ticket_avail_id).val(days);
  $('#days_txt_' + ticket_avail_id).html(days);
}

/**
 * Set number of nights based on the checkout date (checkout - checkin)
 */
function setNumberOfNightsTicket() {
  var checkin_arr = $('#dateFrom').val().split('/');
  var checkin_date = new Date();
  checkin_date.setYear(parseInt(checkin_arr[2]));
  checkin_date.setMonth(parseInt(checkin_arr[1]) - 1);
  checkin_date.setDate(parseInt(checkin_arr[0]));

  var checkout_arr = $('#dateTo').val().split('/');
  var checkout_date = new Date();
  checkout_date.setYear(parseInt(checkout_arr[2]));
  checkout_date.setMonth(parseInt(checkout_arr[1]) - 1);
  checkout_date.setDate(parseInt(checkout_arr[0]));

  var diff = checkout_date - checkin_date;
  var days = parseInt(diff / 1000 / 60 / 60 / 24);
  $('#nights_count').html('(' + days + ' noches)');
}

function showChildAgesConfig() {
  var html = '<label>Edad de los ni&ntilde;os:</label><br />';
  childCount = parseInt($('#childCount').val());

  for (var i = 1; i <= childCount; i++) {
    var lastValue = $('#childAge' + i).val() ? $('#childAge' + i).val() : '';
    html += '<input type="text" size="2" maxlength="2" name="childAge' + i + '" id="childAge' + i + '" value="' + lastValue + '" />';
  }
  html = childCount == 0 ? '' : html;

  $('#child_ages').html(html);
}

/**
 * The following vars need to be set before calling this method:
 *  - adultCount, childCount, dateFrom, dateTo
 */
function restoreTicketSearchValues() {
  $('#adultCount').val(adultCount);
  $('#childCount').val(childCount);
  $('#dateFrom').val(dateFrom);
  $('#dateTo').val(dateTo);
  showChildAgesConfig();
  for (var i = 1; i < childAges.length; i++) {
    $('#childAge' + i).val(childAges[i]);
  }

  $('#orderPriceCombo').val(orderPrice);
}
