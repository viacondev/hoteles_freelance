/**
 * @deprecated. There is no longer an option to select nights
 * Set checkout date based on number of nights
 */
function setCheckoutDate() {
  var nights = parseInt($('#nights').val());
  var selectedDate_arr = $('#checkin').val().split('/');
  var checkout_date = new Date();
  checkout_date.setYear(parseInt(selectedDate_arr[2]));
  checkout_date.setMonth(parseInt(selectedDate_arr[1]) - 1);
  checkout_date.setDate(parseInt(selectedDate_arr[0]) + nights);
  $('#checkout').datepicker('setDate', checkout_date);
}

function updateRoomsConfigPanel() {
  var conf_html = $('#rooms_config_header_raw').html();
  var conf_html_raw = $('#rooms_config_raw').html();
  var num_rooms = parseInt($('#habitaciones').val());
  var lastValuesAdult = [];
  var lastValuesChild = [];
  for (var i = 1; i <= num_rooms; i++) {
    conf_html += conf_html_raw.replace(/__X/g, i);

    lastValuesAdult[i] = $('#adultos' + i).val() ? $('#adultos' + i).val() : 2;
    lastValuesChild[i] = $('#ninos' + i).val() ? $('#ninos' + i).val() : 0;
  }

  $('#rooms_config').hide();
  $('#rooms_config').html(conf_html);
  $('#rooms_config').fadeIn();

  // Restore values
  for (var i = 1; i <= num_rooms; i++) {
    $('#adultos' + i).val(lastValuesAdult[i]);
    $('#ninos' + i).val(lastValuesChild[i]);
  }
}

function showChildAgesConfig() {
  var html = '<label>Edad de los ni&ntilde;os:</label><br />';
  var roomsCount = parseInt($('#habitaciones').val());
  var childCount = 0;
  for (var i = 1; i <= roomsCount; i++) {
    childCount += parseInt($('#ninos' + i).val())
  }
  for (var i = 1; i <= childCount; i++) {
    var lastValue = $('#childAge' + i).val() ? $('#childAge' + i).val() : '';
    html += '<input type="text" size="2" maxlength="2" name="childAge' + i + '" id="childAge' + i + '" value="' + lastValue + '"  required />';
  }
  html = childCount == 0 ? '' : html;

  $('#child_ages').html(html);
}

function loadZones(destination_code) {
  $.getJSON(
    "helpers/ajax/load_zones.php",
    { destination_code: destination_code },
    function(result) {
      var options = $("#zones");
      options.html('');
      options.append('<option default></option>')
      $.each(result, function() {
        options.append($("<option />").val(this.zone_code).text(this.zone_name));
      });
    });
}

/**
 * The following vars need to be set before calling this method:
 *  - habitaciones, rooms(array of array), childAge(array), checkin, checkout, orderPrice
 */
function restoreAccommodationSearchValues() {
  $('#habitaciones').val(habitaciones);
  updateRoomsConfigPanel();
  for (var i = 1; i <= habitaciones; i++) {
    $('#adultos' + i).val(rooms[i][0]);
    $('#ninos' + i).val(rooms[i][1]);
  }
  showChildAgesConfig();
  for (var i = 1; i < childAges.length; i++) {
    $('#childAge' + i).val(childAges[i]);
  }
  $('#checkin').val(checkin);
  $('#checkout').val(checkout);

  $("#checkin").datepicker("option", "minDate", 0);
  $("#checkout").datepicker("option", "minDate", checkin);
  if (orderPrice) {
    $('#orderPriceCombo').val(orderPrice);
  }
}

function toggleComparePreview(element) {
  // var items = $('input:checkbox[class=compare-box]:checked');
  var unselected = $('input:checkbox[class=compare-box]:not(:checked)');
  var items = JSON.parse(localStorage.getItem('compare'))
  arrHotelsCompare = items == null ? new Array() : items;
  var items = element;
  var hotel     = 0;
  var provider  = 0;
  var type      = 0;
  var board     = "";
  var price     = 0;
  var currency  = 0;
  var hotelcode = 0;
  var img       = "";
  hotel     = element
  provider  = $('#provider-' + hotel).val()
  type      = hotel != '' ? $('#type-' + hotel).val() : '';
  board     = hotel != '' ? $('#board-' + hotel).val() : '';
  price     = hotel != '' ? $('#price-' + hotel).val() : '';
  currency  = hotel != '' ? $('#currency-' + hotel).val() : '';
  hotelname = hotel != '' ? $('#hotel-link-' + hotel).html() : '';
  hotelcode = $('#' + element).data('hotelcode')
  img       = $('#hotel-image-' + $('#' + hotel).val()).attr('src');
  if ($('#' + element).prop('checked')) {
    // Insertando
    existItem = false
    for (var i = 0; i < arrHotelsCompare.length; i++) {
      hot = arrHotelsCompare[i]
      if (hot.hotelcode == hotelcode) {
        existItem = true
      }
    }
    if (!existItem) {
      arrHotelsCompare.push({hotel:hotel, provider:provider, board:board, price:price, 
                            type:type,currency:currency, hotelcode:hotelcode, img:img,
                            hotelname:hotelname})
      indice = arrHotelsCompare.length
    }
  }
  else if (!$('#' + element).prop('checked')) {
    //Eliminando Elemento
    arr = arrHotelsCompare
    for (var i = 0; i < arr.length; i++) {
      hcode = arr[i].hotelcode
      if(hotelcode == hcode) {
        arr.splice(i, 1);
      }
    }
  }
  for (var i = 1; i <= 4; i++) {
    $('#compare-item-' + i + ' img').attr('title', '');
    $('#compare-item-' + i + ' img').attr('src', 'assets/images/no-item.png');
    $('#del_' + i).hide();
  }
  var con = 1;
  for (var i = 0; i < arrHotelsCompare.length; i++) {
    img = arrHotelsCompare[i]
    $('#compare-item-' + (con) + ' img').attr('src', img.img);
    $('#compare-item-' + (con) + ' img').attr('title', img.hotelname);
    $('#del_' + con).attr('data-index_compare', con);
    $('#del_' + con).show();
    con++;
  }
  
  localStorage.setItem('compare', JSON.stringify(arrHotelsCompare))

  // Only allow 4 items
  if (arrHotelsCompare.length == 4) {
    unselected.attr("disabled", true);
  }
  else {
    unselected.removeAttr("disabled");
  }

  // Toggle bottom's compare preview
  if (arrHotelsCompare.length > 0) {
    $('#compare-preview').show('drop', {direction: 'right'});
  }
  else {
    $('#compare-preview').hide('drop', {direction: 'right'});
  }
}

function verifySelectCompare() {
  var arrHotelsCompare = JSON.parse(localStorage.getItem('compare'))
  if (arrHotelsCompare != null) {
    for (var i = 0; i < arrHotelsCompare.length; i++) {
      item = arrHotelsCompare[i]
      if ($('#' + item.hotel).length > 0) {
        $('#'+arrHotelsCompare[i].hotel).prop('checked', true);
      }
    }

    // Toggle bottom's compare preview
    if (arrHotelsCompare.length > 0) {
      $('#compare-preview').show('drop', {direction: 'right'});
    }
    else {
      $('#compare-preview').hide('drop', {direction: 'right'});
    }
    var con = 1;
    for (var i = 0; i < arrHotelsCompare.length; i++) {
      img = arrHotelsCompare[i]
      $('#compare-item-' + (con) + ' img').attr('title', img.hotelname);
      $('#compare-item-' + (con) + ' img').attr('src', img.img);
      $('#del_' + con).attr('data-index_compare', con);
      $('#del_' + con).show();
      con++;
    }
  }
}

function deleteItemCompare(indexItem) {
  var items = JSON.parse(localStorage.getItem('compare'))
  arrHotelsCompare = items == null ? new Array() : items;
  for (var i = 0; i < arrHotelsCompare.length; i++) {
    if (i == (indexItem -1)) {
      $('#'+arrHotelsCompare[i].hotel).prop('checked',false);
      arrHotelsCompare.splice(i, 1);
    }
  }
  for (var i = 1; i <= 4; i++) {
    $('#compare-item-' + i + ' img').attr('title', '');
    $('#compare-item-' + i + ' img').attr('src', 'assets/images/no-item.png');
    $('#del_' + i).hide();
  }
  var con = 1;
  for (var i = 0; i < arrHotelsCompare.length; i++) {
    img = arrHotelsCompare[i]
    $('#compare-item-' + (con) + ' img').attr('title', img.hotelname);
    $('#compare-item-' + (con) + ' img').attr('src', img.img);
    $('#del_' + con).attr('data-index_compare', con);
    $('#del_' + con).show();
    con++;
  }
  localStorage.setItem('compare', JSON.stringify(arrHotelsCompare))
  if (arrHotelsCompare.length > 0) {
    $('#compare-preview').show('drop', {direction: 'right'});
  }
  else {
    $('#compare-preview').hide('drop', {direction: 'right'});
  }
}

function showComparationV2() {
  var items = $('input:checkbox[class=compare-box]:checked');
  var hotel1 = items[0] ? $(items[0]).val() : '';
  var hotel2 = items[1] ? $(items[1]).val() : '';
  var hotel3 = items[2] ? $(items[2]).val() : '';
  var hotel4 = items[3] ? $(items[3]).val() : '';
  var provider1 = hotel1 != '' ? $('#provider-' + hotel1).val() : '';
  var provider2 = hotel2 != '' ? $('#provider-' + hotel2).val() : '';
  var provider3 = hotel3 != '' ? $('#provider-' + hotel3).val() : '';
  var provider4 = hotel4 != '' ? $('#provider-' + hotel4).val() : '';
  var type1 = hotel1 != '' ? $('#type-' + hotel1).val() : '';
  var type2 = hotel2 != '' ? $('#type-' + hotel2).val() : '';
  var type3 = hotel3 != '' ? $('#type-' + hotel3).val() : '';
  var type4 = hotel4 != '' ? $('#type-' + hotel4).val() : '';
  var board1 = hotel1 != '' ? $('#board-' + hotel1).val() : '';
  var board2 = hotel2 != '' ? $('#board-' + hotel2).val() : '';
  var board3 = hotel3 != '' ? $('#board-' + hotel3).val() : '';
  var board4 = hotel4 != '' ? $('#board-' + hotel4).val() : '';
  var price1 = hotel1 != '' ? $('#price-' + hotel1).val() : '';
  var price2 = hotel2 != '' ? $('#price-' + hotel2).val() : '';
  var price3 = hotel3 != '' ? $('#price-' + hotel3).val() : '';
  var price4 = hotel4 != '' ? $('#price-' + hotel4).val() : '';
  var currency1 = hotel1 != '' ? $('#currency-' + hotel1).val() : '';
  var currency2 = hotel2 != '' ? $('#currency-' + hotel2).val() : '';
  var currency3 = hotel3 != '' ? $('#currency-' + hotel3).val() : '';
  var currency4 = hotel4 != '' ? $('#currency-' + hotel4).val() : '';
  var hotelcode1 = $(items[0]).data('hotelcode')
  var hotelcode2 = $(items[1]).data('hotelcode')
  var hotelcode3 = $(items[2]).data('hotelcode')
  var hotelcode4 = $(items[3]).data('hotelcode')
  var destination = $('#destination').val();
  var checkin = $('#checkin').val();
  var checkout = $('#checkout').val();
  var form = $('#refine-form').serialize();
  $('#hotels-comparation').html('<div class="text-center"><img src="assets/images/ajax-loader.gif" /></div>');
  $('#compare-modal').modal();
  $.ajax({
    url: "helpers/ajax/compare_hotels.php",
    data: {
      hotel1: hotelcode1, hotel2: hotelcode2, hotel3: hotelcode3, hotel4: hotelcode4,
      provider1: provider1, provider2: provider2, provider3: provider3, provider4: provider4,
      type1: type1, type2: type2, type3: type3, type4: type4,
      board1: board1, board2: board2, board3: board3, board4: board4,
      price1: price1, price2: price2, price3: price3, price4: price4,
      currency1: currency1, currency2: currency2, currency3: currency3, currency4: currency4,
      destination: destination, checkin: checkin, checkout: checkout,
      form
    }
  })
    .done(function(r) {
      $('#hotels-comparation').html(r);
    });
}

function showComparation() {
  var items = JSON.parse(localStorage.getItem('compare'))
  var destination = $('#destination').val();
  var checkin     = $('#checkin').val();
  var checkout    = $('#checkout').val();
  var agencyId    = $('#id_agencia').val();
  var form        = $('#refine-form').serialize();
  $('#hotels-comparation').html('<div class="text-center"><img src="assets/images/ajax-loader.gif" /></div>');
  $('#compare-modal').modal();
  $.ajax({
    url: "helpers/ajax/compare_hotels.php",
    data: {
      items:items,agencyId:agencyId,
      destination: destination, checkin: checkin, checkout: checkout,
      form
    }
  })
  .done(function(r) {
    $('#hotels-comparation').html(r);
  });
}
