$(function() {

  initCalendars();
  // restoreAccommodationSearchValues();

  $('#hotel-images').carousel({
      interval: 4000
  });

  // handles the carousel thumbnails
  $('[id^=carousel-selector-]').click( function(){
    var id_selector = $(this).attr("id");
    var id = id_selector.substr(id_selector.length -1);
    id = parseInt(id);
    $('#hotel-images').carousel(id);
    $('[id^=carousel-selector-]').removeClass('selected');
    $(this).addClass('selected');
  });

  // when the carousel slides, auto update
  $('#hotel-images').on('slid', function (e) {
    var id = $('.item.active').data('slide-number');
    id = parseInt(id);
    $('[id^=carousel-selector-]').removeClass('selected');
    $('[id^=carousel-selector-'+id+']').addClass('selected');
  });

  $('[data-toggle="tooltip"]').tooltip({trigger: 'hover','placement': 'top'});
});

function load_googlemap(latitude, longitude, dir) {
  var pos = new google.maps.LatLng(latitude, longitude);
  var mapOptions = {
    center: pos,
    zoom: 17,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(document.getElementById("googlemap"), mapOptions);
  var marker = new google.maps.Marker({ position: pos, title: dir });
  marker.setMap(map);
}

/**
 * Cargar el mapa de Google
 */
function addtag(point, address) {
  var marker = new GMarker(point);
  GEvent.addListener(marker, "click", function() { marker.openInfoWindowHtml(address); } );
  return marker;
}
