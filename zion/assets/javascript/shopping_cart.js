function checkAgreement() {
  if (!$('#agreement_check').is(':checked')) {
    alert('Debes aceptar las politicas de cancelaciones para confirmar la reserva');
    return false;
  }
  return true;
}

function checkValidForm() {
  var fields = $('.required_pax');
  var validForm = true;
  for (var i = 0; i < fields.length; i++) {
    if (!fields[i].checkValidity()) {
      validForm = false;
      break;
    }
  }
  return validForm;
}

function copyFirstCustomerData(services_count, pax_count) {
  for (var i = 2; i <= services_count; i++) {
    for (var j = 1; j <= pax_count; j++) {
      var name = $('input[name="customerName_1_' + j + '"]').val();
      var lastname = $('input[name="customerLastName_1_' + j + '"]').val();
      $('input[name="customerName_' + i + '_' + j + '"]').val(name);
      $('input[name="customerLastName_' + i + '_' + j + '"]').val(lastname);
    }
  }
}

function removeService(scartId, SPUI) {
  if (confirm('Estas seguro que deseas eliminar este producto del Carrito de Compras?')) {
    $('#remove_scartId').val(scartId);
    $('#remove_SPUI').val(SPUI);
    $('#service_remove_form').submit();
    showProgress();
  }
}
