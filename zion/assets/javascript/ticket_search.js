$(function() {

  initDestinationAutocomplete();
  initCalendarsTicket();
  restoreTicketSearchValues();
  initPagination();

});

function initPagination() {
  if (totalPages && currentPage) {
    $('#pagination').bootpag({
      total: totalPages,
      page: currentPage,
      maxVisible: 6
    }).on('page', function(event, num){
      $('#page').val(num);
      $('#refine-form').submit();
    });
  }
}