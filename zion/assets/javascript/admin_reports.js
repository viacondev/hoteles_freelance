$(function() {
Ext.onReady(function () {
    var createLayout = function() {
        viewport = Ext.create('widget.panel', {
            layout: 'border',
            width: 1140,
            height: 500,
            title: 'Reportes',
            renderTo: 'reports',
            items: [{
                region: 'west',
                collapsible: true,
                width: 200,
                xtype: 'panel',
                layout: {
                        type:'vbox',
                        align:'center'
                },
                items: [ usuariosVentasTree, tipoVentasTree, reporteVentasTree ]
            }, {
                region: 'center',
                xtype: 'tabpanel',
                id: 'tabs',
                activeTab: 0,
                items: [{
                    id: 'tabTortas',
                    title: 'Tortas',
                    layout: 'border',
                    items: chart,
                }, {
                    id: 'tabBarras',
                    title: 'Barras',
                    layout: 'border',
                    items: bars
                }, {
                    id: 'tabTablas',
                    title: 'Tablas',
                    layout: 'fit',
                    items: grid
                }, {
                    id: 'tabTendencias',
                    title: 'Tendencias',
                    layout: 'fit',
                    items: lines
                }],
                listeners: {
                    tabchange: actualizarReporte
                }
            }, {
                region: 'east',
                collapsible: true,
                collapsed: false,
                width: 190,
                autoScroll: true,
                items: [{
                    xtype: 'panel',
                    title: 'Desde - Hasta',
                    items: [{
                        id: 'fromDate',
                        xtype: 'datepicker',
                        title: 'Desde:',
                        value: new Date((new Date).getFullYear(), (new Date).getMonth(), 1), // Primer dia del mes actual
                        maxDate: new Date(),
                        startDay: 1,
                        showToday: false
                    }, {
                        id: 'toDate',
                        xtype: 'datepicker',
                        title: 'Hasta:',
                        startDay: 1,
                        showToday: false,
                        maxDate: new Date()
                    }]
                }, {
                    xtype: 'button',
                    text: 'Actualizar',
                    textAlign: 'right',
                    margin: '5',
                    handler: function() {
                        usuarios = _usuarios;
                        _usuarios = '';
                        mostrarReporte(usuarios, _ventas, _reporte);
                    }
                }, {
                    xtype: 'button',
                    text: 'Guardar Reporte',
                    textAlign: 'right',
                    margin: '5',
                    handler: function() {
                        if (tabActual == 'tabTortas') chart.save({ type: 'image/png' });
                        if (tabActual == 'tabBarras') bars.save({ type: 'image/png' });
                        if (tabActual == 'tabTablas') grid.save({ type: 'image/png' });
                        if (tabActual == 'tabTendencias') lines.save({ type: 'image/png' });
                    }
                }]
            }]
        });
    };

    usuariosVentasStore = Ext.create('Ext.data.TreeStore', {
        proxy: {
            type: 'ajax',
            url: 'views/admin/_report_ajax_users.php'
        },
        root: {
            id: 'usuariosroot',
            expanded: true,
            children: [
                { text: "Agencias", id: "AG00", leaf: false, icon: 'assets/images/users16.ico' },
                { text: "Proveedores", id: "PR00", leaf: false, icon: 'assets/images/users16.ico' }
            ]
        }
    });

    usuariosVentasTree = Ext.create('Ext.tree.Panel', {
        title: 'Usuarios',
        xtype: 'treepanel',
        id: 'usuariosTree',
        store: usuariosVentasStore,
        rootVisible: false,
        multiSelect: true,
        flex: 4,
        listeners: {
            itemclick : function(view, rec, item, index, eventObj) {
                idusuarios = '';
                usuariosSelecc = view.getSelectionModel()
                for (var i = 0; i < usuariosSelecc.getCount(); i++) {
                    if (i != 0) {
                        idusuarios += ',';
                    }
                    idusuarios += usuariosSelecc.getSelection()[i].getId();
                }
                mostrarReporte(idusuarios, _ventas, _reporte);
            }
        }
    });

    tipoVentasStore = Ext.create('Ext.data.TreeStore', {
        root: {
            id: "root",
            expanded: true,
            children: [
                { text: "Ventas", id: "ventas", leaf: true, icon: 'assets/images/report.png' },
                { text: "Tipo Ventas", id: "tipo_ventas", leaf: true, icon: 'assets/images/report.png' },
                { text: "Destinos", id: "destinos", leaf: true, icon: 'assets/images/report.png' },
            ]
        }
    });

    tipoVentasTree = Ext.create('Ext.tree.Panel', {
        title: 'Tipo Ventas',
        xtype: 'treepanel',
        store: tipoVentasStore,
        rootVisible: false,
        flex: 3,
        listeners: {
            itemclick : function(view, rec, item, index, eventObj) {
                mostrarReporte(_usuarios, rec.get('id'), _reporte);
            }
        }
    });

    reporteVentasStore = Ext.create('Ext.data.TreeStore', {
        root: {
            id: "root",
            expanded: true,
            children: [
                { text: "Total", id: "total", leaf: true, icon: 'assets/images/graph.png' },
                { text: "Neto", id: "neto", leaf: true, icon: 'assets/images/graph.png' },
                { text: "Comision", id: "comision", leaf: true, icon: 'assets/images/graph.png' },
                { text: "Incentivo", id: "insentivo", leaf: true, icon: 'assets/images/graph.png' },
                { text: "Ganancia", id: "ganancia", leaf: true, icon: 'assets/images/graph.png' },
                { text: "Cantidad", id: "cantidades", leaf: true, icon: 'assets/images/graph.png' },
            ]
        }
    });

    reporteVentasTree = Ext.create('Ext.tree.Panel', {
        title: 'Tipo Reporte',
        xtype: 'treepanel',
        store: reporteVentasStore,
        rootVisible: false,
        flex: 5,
        listeners: {
            itemclick : function(view, rec, item, index, eventObj) {
                mostrarReporte(_usuarios, _ventas, rec.get('id'));
            }
        }
    });

    reporteStore = new Ext.data.JsonStore({
        storeId: 'reporteStore',
        proxy: {
            type: 'ajax',
            url: 'views/admin/_report_ajax_result.php',
            reader: {
                type: 'json',
                root: 'data'
            }
        },

        fields: [
            'id',
            { name: 'total', type: 'float' },
            { name: 'neto', type: 'float' },
            { name: 'comision', type: 'float' },
            { name: 'insentivo', type: 'float' },
            { name: 'ganancia', type: 'float' },
            { name: 'cantidades', type: 'float' },
        ]
    });

    tendenciaStore = new Ext.data.JsonStore({
        storeId: 'reporteStore',
        proxy: {
            type: 'ajax',
            url: 'views/admin/_report_ajax_result.php',
            reader: {
                type: 'json',
                root: 'data'
            }
        },

        fields: [
            'id',
            { name: 'total', type: 'float' },
            { name: 'neto', type: 'float' },
            { name: 'comision', type: 'float' },
            { name: 'insentivo', type: 'float' },
            { name: 'ganancia', type: 'float' },
            { name: 'cantidades', type: 'float' },
        ]
    });

    xdata = 'total';
    createChart = function() {
        chart = Ext.create('Ext.chart.Chart', {
            xtype: 'chart',
            id: 'chartCmp',
            style: 'background:#fff',
            animate: true,
            store: reporteStore,
            shadow: true,
            legend: {
                position: 'right'
            },
            theme: 'Base:gradients',
            series: [{
                type: 'pie',
                field: xdata,
                showInLegend: true,
                donut: false,
                tips: {
                  trackMouse: true,
                  width: 150,
                  height: 35,
                  renderer: function(storeItem, item) {
                    //calculate percentage.
                    var total = 0;
                    reporteStore.each(function(rec) {
                        total += rec.get(xdata) * 1;
                    });
                    if (storeItem.get(xdata) > 0) {
                        percentage = Math.round(storeItem.get(xdata) / total * 100);
                        this.setTitle(storeItem.get('id') + ' (' + percentage + '%)');
                    }
                    else {
                        this.setTitle('');
                    }
                  }
                },
                highlight: {
                    segment: {
                        margin: 20
                    }
                },
                label: {
                    field: 'id',
                    display: 'rotate',
                    contrast: true,
                    font: '17px Arial',
                    stopEvent: true,
                    renderer: function (label) {
                        var monto = reporteStore.getById(label).getData()[xdata];
                        monto = Math.round(monto);
                        if (monto > 0) {
                            return label;
                        }
                        return '';
                    }
                },
            }]
        });
    }

    createBars = function() {
        bars = Ext.create('Ext.chart.Chart', {
            id: 'barCmp',
            xtype: 'chart',
            style: 'background:#fff',
            animate: true,
            shadow: true,
            store: reporteStore,
            axes: [{
                type: 'Numeric',
                position: 'left',
                fields: xdata,
                label: {
                    renderer: Ext.util.Format.numberRenderer('0,0')
                },
                title: 'Monto',
                grid: true,
                minimum: 0
            }, {
                type: 'Category',
                position: 'bottom',
                fields: ['id'],
                title: 'Usuario'
            }],
            series: [{
                type: 'column',
                axis: 'left',
                highlight: true,
                tips: {
                    trackMouse: true,
                    width: 140,
                    height: 35,
                    renderer: function(storeItem, item) {
                        //calculate percentage.
                        var total = 0;
                        reporteStore.each(function(rec) {
                            total += rec.get(xdata) * 1;
                        });
                        if (storeItem.get(xdata) > 0) {
                            percentage = Math.round(storeItem.get(xdata) / total * 100);
                            var monto = Math.round(storeItem.get(xdata));
                            var tipomonto = xdata.indexOf('_cantidades') == -1 ? ' USD' : '';
                            this.setTitle(storeItem.get('id') + ': ' + monto + tipomonto + ' (' + percentage + '%)');
                        }
                        else {
                            this.setTitle('');
                        }
                    }
                },
                label: {
                    display: 'insideEnd',
                    'text-anchor': 'middle',
                    field: 'id',
                    renderer: function (label) {
                        return Math.round(reporteStore.getById(label).getData()[xdata]);
                    },
                    orientation: 'vertical',
                    color: '#333'
                },
                xField: 'id',
                yField: [{name: xdata, type: 'int'}]
            }]
        });
    }

    createLines = function() {
        lines = Ext.create('Ext.chart.Chart', {
            id: 'linesCmp',
            xtype: 'chart',
            style: 'background:#fff',
            animate: true,
            shadow: true,
            store: tendenciaStore,
            axes: [{
                type: 'Numeric',
                position: 'left',
                fields: xdata,
                label: {
                    renderer: Ext.util.Format.numberRenderer('0,0')
                },
                title: 'Monto',
                grid: true,
                minimum: 0
            }, {
                type: 'Category',
                position: 'bottom',
                fields: ['id'],
                title: 'Fecha'
            }],
            series: [{
                type: 'line',
                axis: 'left',
                highlight: true,
                tips: {
                    trackMouse: true,
                    width: 140,
                    height: 35,
                    renderer: function(storeItem, item) {
                        if (storeItem.get(xdata) > 0) {
                            var monto = Math.round(storeItem.get(xdata));
                            var tipomonto = xdata.indexOf('_cantidades') == -1 ? ' USD' : '';
                            this.setTitle(storeItem.get('id') + ': ' + monto + tipomonto);
                        }
                        else {
                            this.setTitle('');
                        }
                    }
                },
                label: {
                    display: 'rotate',
                    'text-anchor': 'middle',
                    field: 'id',
                    renderer: function (label) {
                        return Math.round(tendenciaStore.getById(label).getData()[xdata]);
                    },
                    orientation: 'vertical',
                    color: '#333'
                },
                xField: 'id',
                yField: xdata
            }]
        });
    }

    // Logica para saber que graficos mostrar
    _usuarios = '';
    _ventas = '';
    _reporte = '';
    _sort = false;
    mostrarReporte = function (usuarios, ventas, reporte) {
        _reporte = reporte;
        if (usuarios != '' && ventas != '' && (usuarios != _usuarios || ventas != _ventas)) {
            _usuarios = usuarios;
            _ventas = ventas;
            chart.hide();
            bars.hide();
            grid.hide();
            lines.hide();

            tipoUsuarios = usuarios.substr(0, 2);
            var cargando = new Ext.LoadMask(Ext.getBody(), { msg: "Cargando..." });
            cargando.show();

            store = reporteStore;
            if (tabActual == 'tabTendencias') {
                store = tendenciaStore;
            }

            store.getProxy().extraParams.usuarios = usuarios;
            store.getProxy().extraParams.ventas = ventas;
            store.getProxy().extraParams.tendencias = tabActual == 'tabTendencias';
            fromDate = Ext.getCmp('fromDate').getValue();
            toDate = Ext.getCmp('toDate').getValue();
            store.getProxy().extraParams.fromDate = fromDate.getFullYear() + "-" + (fromDate.getMonth() + 1) + "-" + fromDate.getDate();
            store.getProxy().extraParams.toDate = toDate.getFullYear() + "-" + (toDate.getMonth() + 1) + "-" + toDate.getDate();

            store.load();
            store.on('load', function() { cargando.destroy(); mostrarReporte(_usuarios, _ventas, reporte); }, this);
        }
        else if (_usuarios != '' && _ventas != '' && (_reporte != '' || tabActual == 'tabTablas')) {
            if (tabActual == 'tabTortas') chart.show();
            if (tabActual == 'tabBarras') bars.show();
            if (tabActual == 'tabTablas') grid.show();
            if (tabActual == 'tabTendencias') lines.show();

            xdata = reporte;
            reporteStore.sort(xdata, 'DESC');

            actualizarTortas();
            actualizarBarras();
            actualizarTablas();
            actualizarTendencias();

            actualizarLegends();

            // Mostrar total
            if (tabActual != 'tabTablas' && tabActual != 'tabTendencias') {
                var total = 0; 
                reporteStore.each(function (rec) { total += rec.get(xdata); });
                var tipomonto = xdata.indexOf('_cantidades') == -1 ? ' USD' : '';
                totalWindow.update('Total: ' + Math.round(total) + tipomonto);
                totalWindow.showAt(0, 0);
                totalWindow.alignTo(Ext.getDom('reports'), "tl", [750, 50]);
            }
            else {
                totalWindow.hide();
            }
        }
        _usuarios = usuarios;
        _ventas = ventas;
    }

    actualizarReporte = function() {
        tabActual = Ext.getCmp('tabs').getActiveTab().getId();

        var nuevoReporte = 'normal';
        if (tabActual == 'tabTendencias') {
            nuevoReporte = 'tendencia';
        }

        usuarios = _usuarios;
        if (nuevoReporte != tipoReporte) {
            _usuarios = ''; // Forzar que vuelva a cargar los datos del usuario
        }
        tipoReporte = nuevoReporte;

        mostrarReporte(usuarios, _ventas, _reporte);
    }

    function actualizarLegends() {
        size = reporteStore.getCount();
        for (var i = 0; i < size; i++) {
            var r = reporteStore.getAt(i);
            var legend = r.getId().toString();

            // En el caso de las tortas agregamos el monto y la moneda
            if (tabActual == 'tabTortas') {
                var monto = Math.round(r.getData()[xdata]);
                var tipomonto = xdata.indexOf('_cantidades') == -1 ? ' USD' : '';
                legend = legend.split(':')[0];
                legend += ": " + monto + tipomonto;
                r.setId(legend);
            }

            // En el caso de las barras cortamos los nombres largos en el caso de los counters
            if (tabActual == 'tabBarras') {
                legend = legend.split(':')[0];                
                r.setId(legend.substr(0, 8));
            }

            // En el caso de las tablas cortamos el monto que le pudo ser asignado por las tortas
            if (tabActual == 'tabTablas') {
                legend = legend.split(':')[0];
                r.setId(legend);
            }
        }
    }

    gridColumns = [ 'id', 'cantidades', 'comision', 'insentivo', 'ganancia', 'neto', 'total' ];
    createGrid = function() {
        grid = Ext.create('Ext.grid.Panel', {
            store: reporteStore,
            columns: [
                {
                    text     : 'Usuario',
                    dataIndex: 'id',
                    flex     : 3,
                    sortable : true
                },
                {
                    text     : 'Cantidad',
                    flex     : 1,
                    sortable : true,
                    align    : 'right'
                },
                {
                    text     : 'Comision',
                    flex     : 1,
                    sortable : true,
                    align    : 'right',
                    renderer : money
                },
                {
                    text     : 'Insentivo',
                    flex     : 1,
                    sortable : true,
                    align    : 'right',
                    renderer : money
                },
                {
                    text     : 'Ganancia',
                    flex     : 1,
                    sortable : true,
                    align    : 'right',
                    renderer : money
                },
                {
                    text     : 'Neto',
                    flex     : 1,
                    sortable : true,
                    align    : 'right',
                    renderer : money
                },
                {
                    text     : 'Total',
                    flex     : 1,
                    sortable : true,
                    align    : 'right',
                    renderer : money
                },
            ],
            viewConfig: {
                stripeRows: true,
                enableTextSelection: true,
                markDirty: false
            }
        });
    }

    function money(val) {
        return Math.round(val) + ' USD';
    }

    totalWindow = Ext.create ('Ext.window.Window', {
        id: 'totalWindow',
        width: 200,
        height: 60,
        //header: false,
        closable: false,
        border: false,
        bodyStyle: 'background: #FFF; font-size: 16px; padding: 5px;'
    });

    tabActual = 'tabTortas';
    tipoReporte = 'normal'; // normal; tendencia
    filtroTendencia = 'mes',
    createChart();
    createBars();
    createGrid();
    createLines();
    createLayout();

    Ext.override(Ext.data.proxy.Ajax, { timeout: 120000 });
});
});

function actualizarTortas() {
    if (tabActual == 'tabTortas') {
        chart.series.first().field = xdata;
        chart.refresh();
    }
}

function actualizarBarras() {
    if (tabActual == 'tabBarras') {
        bars.series.first().yField = xdata;
        // Actualizar la medicion del monto segun el dato mas grande
        var mayor = 0;
        reporteStore.each(function(rec) {
            if (rec.get(xdata) * 1 > mayor) { 
                mayor = rec.get(xdata) * 1;
            }
        });
        bars.axes.getAt(0).maximum = Math.round(mayor * 1.2);
        bars.refresh();
    }
}

function actualizarTablas() {
    if (tabActual == 'tabTablas') {
        for (var i = 1; i < grid.columns.length; i++) {
            grid.columns[i].dataIndex = gridColumns[i];
        }
        grid.getView().refresh();
    }
}

function actualizarTendencias() {
    if (tabActual == 'tabTendencias') {
        lines.series.first().yField = xdata;
        // Actualizar la medicion del monto segun el dato mas grande
        var mayor = 0;
        tendenciaStore.each(function(rec) {
            if (rec.get(xdata) * 1 > mayor) { 
                mayor = rec.get(xdata) * 1;
            }
        });
        lines.axes.getAt(0).maximum = Math.round(mayor * 1.2);
        lines.refresh();
    }
}