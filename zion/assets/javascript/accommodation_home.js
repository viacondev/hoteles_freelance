$(function() {

  initDestinationAutocomplete();
  initCalendars();

});

function toggleAdvanced() {
  if ($('#advanced_options').is(':visible')) {
    $('#advanced_options').slideUp();
    $('#advanced_options_link').html('<i class="fa fa-caret-right"></i> Mostrar Opciones de b&uacute;squeda avanzada');
  }
  else {
    $('#advanced_options').slideDown();
    $('#advanced_options_link').html('<i class="fa fa-caret-down"></i> Ocultar Opciones de b&uacute;squeda avanzada');
  }
}