$(function() {

  // initDestinationAutocomplete();

  initCalendars();

  $("#checkin").datepicker("option", "dateFormat", 'dd/mm/yy');
  $("#checkout").datepicker("option", "dateFormat", 'dd/mm/yy');

  // restoreAccommodationSearchValues();
  restoreFilterValues();

  $('[data-toggle="tooltip"]').tooltip({trigger: 'hover', 'placement': 'top'});

  loadZonesForFilter();

});

function loadZonesForFilter() {
  destination_code = $('#destination_code').val();
  $.getJSON(
    "helpers/ajax/load_zones.php",
    { destination_code: destination_code },
    function(result) {
      str = '<select id="zoneCodeCombo" name="zoneCode" class="form-control" onchange="$(\'#zone_name\').val($(\'#zoneCodeCombo option:selected\').text()); $(\'#zoneCode\').val(value); $(\'#page\').val(1); $(\'#refine-form\').submit();">';
      str += '<option value="">Todas las zonas</option>';
      $.each(result, function() {
        str += '<option value="' + this.zone_code + '">' + this.zone_name + '</option>';
      });
      str += '</select>';
      //str = '<input type="radio" id="allZonesRadio" name="zoneCode" value="" checked onchange="$(\'#page\').val(1); $(\'#refine-form\').submit();" /> Todas las zonas';
      //$.each(result, function() {
      //  str += '<br /><input type="radio" name="zoneCode" onchange="$(\'#refine-form\').submit();" value="' + this.zone_code + '" /> ' + this.zone_name;
      //});
      $('#zones').html(str);
      if (typeof variable !== 'undefined' && zoneCode.length > 0 && zoneCode !== '') {
        //$('input:radio[name=zoneCode][value=' + zoneCode + ']').prop('checked', true);;
        $('#zoneCodeCombo').val(zoneCode);
      }
    });
}

function restoreFilterValues() {
}

function initPagination() {
  if (typeof totalPages !== 'undefined' &&  typeof currentPage !== 'undefined') {
    $('#pagination').bootpag({
      total: totalPages,
      page: currentPage,
      maxVisible: 6
    }).on('page', function(event, num){
      $('#page').val(num);
      $('#refine-form').submit();
    });
    $('#pagination').show();
  }
}
