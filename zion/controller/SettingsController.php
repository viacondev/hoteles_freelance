<?php
include_once('model/HotelMappingProvider.php');
include_once('model/HotelTopTen.php');

class SettingsController extends ApplicationController {

  public static function hotelTop() {
    global $res;
    $destCode = isset($_GET['destCode']) ? $_GET['destCode'] : "";
    if ($destCode != "") {
      $query = "SELECT DISTINCT hp.destination_code, hp.zion_code , h.hotel_name 
                FROM HotelMappingProvider hp, Hotel h 
                WHERE hp.zion_code = h.hotel_code AND hp.destination_code = '" . $destCode . "'";
      $res = HotelMappingProvider::findByQuery($query);
      $rs = array();
      foreach ($res as $value) {
        $exist = false;
        $e  = HotelTopTen::findByQuery("SELECT ID as codigo, ZION_CODE FROM HotelTopTen WHERE zion_code = '$value->zion_code'");
        $id = "";
        if ($e) {
          $exist = true;
          $id    = $e[0]->codigo;
        }
        $rs[] = array('id'  => $id , 'zion_code' => $value->zion_code, 'hotel_name'  => $value->hotel_name, 'destination_code' => $_GET['destCode'], 'exist' => $exist);
      }
      $res = $rs;
    }
  
  }

}

?>
