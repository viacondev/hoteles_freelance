<?php

include_once('model/Fee.php');

class FeeController extends ApplicationController {

  public $admin_only = array('create', 'remove');

  public static function create() {
    $fee = new Fee();
    $fee->destination_code = $_POST['destination_code'];
    $fee->fee = $_POST['fee'];
    $fee->save();

    header('Location: ./?controller=admin&action=edit');
  }

  public static function remove() {
    $fee = Fee::findById($_GET['id']);
    $fee->delete();

    header('Location: ./?controller=admin&action=edit');
  }
}

?>