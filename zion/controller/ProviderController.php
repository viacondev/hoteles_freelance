<?php

include_once('model/Provider.php');

class ProviderController extends ApplicationController {

  public $admin_only = array('home', 'add', 'create', 'edit', 'update');

  public static function home() {
    global $provider;

    $provider = Provider::findAll();
  }

  public static function add() {
  }

  public static function create() {
    $provider = new Provider();
    $provider->build($_POST['name'], $_POST['type'], $_POST['phone'], $_POST['mail'], $_POST['address'],$_POST['coments']);
    $provider->save();

    header('Location: ./?controller=provider&action=show&id=' . $provider->id);
  }

  public static function show() {
    global $provider;

    $provider = Provider::findById($_GET['id']);
  }

  public static function edit() {
    global $provider;

    $provider = Provider::findById($_GET['id']);
  }

  public static function update() {
    $provider = new Provider();
    $provider->build($_POST['name'], $_POST['type'], $_POST['phone'], $_POST['mail'], $_POST['address'],$_POST['coments']);
    $provider->id = $_POST['id'];
    $provider->save();

    header('Location: ./?controller=provider&action=show&id=' . $provider->id);
  }

}

?>