<?php

include_once('../booking/BookingEngine.php');
include_once('model/ShoppingCart.php');
include_once('controller/HotelbedsController.php');
include_once('model/Fee.php');
include_once('model/FeeGeneral.php');
include_once('model/Provider.php');
include_once('model/MotorConexion.php');
include_once('model/ServiceType.php');

class ShoppingCartController extends ApplicationController {

  public static function service_add() {
    global $services;

    if (isNewRequest()) {
      $params = (array) json_decode($_POST['service_add_data']);

      if ($params['provider'] == BookingProvider::HOTELBEDS) {
        // Check if HB items already exists
        $hb_exists = false;
        $items = ShoppingCart::getItems();
        $purchaseToken = '';
        foreach ($items as $item) {
          if ($item['provider'] == BookingProvider::HOTELBEDS) {
            $hb_exists = true;
            $purchaseToken = $item['purchaseToken'];
            break;
          }
        }
        $rs = HotelbedsController::service_add($params, $purchaseToken);

        // Only save one item for HotelBeds in the Shopping Cart
        if (!$hb_exists) {
          $params_hb = array(
            'requestType'   => BookingRequestTypes::PurchaseDetail,
            'purchaseToken' => $rs->getPurchaseToken(),
            'provider'      => BookingProvider::HOTELBEDS
          );
          $items = ShoppingCart::addItem($params_hb);
        }
      }
      else if ($params['provider'] == BookingProvider::DOMITUR) {
        $items = ShoppingCart::addItem($params);
      }
      else if ($params['provider'] == BookingProvider::METHABOOK) {
        $items = ShoppingCart::addItem($params);
      }
      else if ($params['provider'] == BookingProvider::TOURICO) {
        $items = ShoppingCart::addItem($params);
      }
      else if ($params['provider'] == BookingProvider::DOMITUR2) {
        $items = ShoppingCart::addItem($params);
      }
    }
    else {
      header('Location: ?controller=shopping_cart&action=show');
    }

    ShoppingCartController::show();
    include('views/shopping_cart/show.php');
  }

  public static function show($p) {
    $serviceData = json_decode($p['service_add_data']);
    if ($serviceData->requestType == BookingRequestTypes::AddHotelService) {
      $p['origen']      = 'yc';
      $p['agency_id']   = $p['current_user']['agency_id'];
      $p['user_id']     = $p['current_user']['id'];
      $conexion = new MotorConexion();
      $conexion->build('policyCancellation', $p);
      $resp     = $conexion->consultar();
      return array('services' => $resp->services, 'book_params' => $resp->book_params);
      exit;
    } else {
      $p['origen']      = 'bb';
      $p['agency_id']   = $p['current_user']['agency_id'];
      $p['user_id']     = $p['current_user']['id'];
      
      $conexion = new MotorConexion();
      $conexion->build('policyTicket', $p);
      $resp     = $conexion->consultar();
      return array('services' => $resp->services, 'book_params' => $resp->book_params);
      exit;
    }
    exit;

    if (!isset($p['service_add_data'])) {
      return array('services' => array(), 'book_params' => array());
    }
    if (isNewRequest() || true) {
      $factorFee = FeeGeneral::getFactorFee($p['current_user']);
      $params = (array) json_decode($p['service_add_data']);
      $params['moreParams'] = $p;
      $params['factorFee'] = $factorFee;
      if ($params['provider'] == BookingProvider::HOTELBEDS) {
        // Check if HB items already exists
        $hb_exists = false;
        $items = ShoppingCart::getItems();
        $purchaseToken = '';
        $rs = HotelbedsController::service_add($params, $purchaseToken);
        $verify_errors = $rs->getBookingServices();
        if (count($verify_errors[0]->getErrors()) != 0) {
          return array('services' => array(), 'book_params' => array());
        }
        // Only save one item for HotelBeds in the Shopping Cart
        if (!$hb_exists) {

          $factorFee = FeeGeneral::getFactorFee($p['current_user'], '', 1);

          $request = array(
            'requestType'   => BookingRequestTypes::PurchaseDetail,
            'purchaseToken' => $rs->getPurchaseToken(),
            'provider'      => BookingProvider::HOTELBEDS,
            'factorFee'     => $factorFee
          );
          // $items = ShoppingCart::addItem($params_hb);
        }
      }
      /*if ($params['provider'] == BookingProvider::HOTELBEDS) {
        $request = $params;
        $request['factorFee'] = $factorFee;
      }*/ else if ($params['provider'] == BookingProvider::METHABOOK || $params['provider'] == BookingProvider::DOMITUR) {
        $request = $params;
        $request['factorFee'] = $factorFee;
      } else if ($params['provider'] == BookingProvider::METHABOOK2) {
        $request = $params;
        $request['factorFee'] = $factorFee;
      } else if ($params['provider'] == BookingProvider::TOURICO) {
        $request = $params;
        $request['factorFee'] = $factorFee;
      } else if ($params['provider'] == BookingProvider::DOMITUR2) {
        $request = $params;
        $request['factorFee'] = $factorFee;
      } else if ($params['provider'] == BookingProvider::RESTEL) {
        $request = $params;
        $request['factorFee'] = $factorFee;
      } else if ($params['provider'] == BookingProvider::TAILORBEDS) {
        $request = $params;
        // $request['factorFee'] = $factorFee;
        $request['factorFee'] = 0.79; //NORMALMARKUP
        // $request['factorFee'] = 0.86;// HOT DAY MARKUP
      } else if ($params['provider'] == BookingProvider::NEMO) {
        $request = $params;
        $factorFee = FeeGeneral::getFactorFee($p['current_user'], '', 187);
        $request['factorFee'] = $factorFee;
      }
    }
    $booking  = new BookingEngine();
    $services = array();
    $rs       = $booking->execRequest($request);
    $ss       = $rs->getBookingServices();
    foreach ($ss as $s) {
      $s->scartId = 0;
    }
    $services = array_merge($services, $ss);
    return array('services' => $services, 'book_params' => $request);
  }

  public static function emptyCart() {
    ShoppingCart::emptyCart();

    header('Location: ?controller=shopping_cart&action=show');
  }

  public static function remove() {
    $id = _post('scartId');
    $item = ShoppingCart::getItem($id);

    if ($item['provider'] == BookingProvider::HOTELBEDS) {
      $rs = HotelbedsController::service_remove($item['purchaseToken'], _post('SPUI'));
      // __logarr($rs);
      if (count($rs->getBookingServices()) == 0) {
        // echo "[[[EMPTY NOW]]]]";
        ShoppingCart::removeItem($id);
      }
    }
    else if ($item['provider'] == BookingProvider::DOMITUR) {
      ShoppingCart::removeItem($id);
    }
    else if ($item['provider'] == BookingProvider::METHABOOK) {
      ShoppingCart::removeItem($id);
    }

    // __logarr(ShoppingCart::getItems());

    header('Location: ?controller=shopping_cart&action=show');
  }
}

function execTicketValuation($params) {
  $diff_days = intval(_post('days', $params['moreParams'])) - 1;
  $dateFrom = _post('dateFrom', $params['moreParams']);
  $dateTo = date('Ymd', strtotime("$dateFrom + $diff_days days"));
  $params['dateFrom'] = $dateFrom;
  $params['dateTo'] = $dateTo;

  $booking = new BookingEngine();
  $rs = $booking->execRequest($params);

  $availModality = $rs->getAvailableModalityList();
  $availModality = $availModality[0];

  $p = array(
    'requestType'       => BookingRequestTypes::AddTicketService,
    'availToken'        => $params['availToken'],
    'contractName'      => $rs->getContractName(),
    'contractIncomingOfficeCode' => $rs->getContractIncomingOfficeCode(),
    'dateFrom'          => $rs->getDateFrom(),
    'dateTo'            => $rs->getDateTo(),
    'code'              => $rs->getCode(),
    'destCode'          => $rs->getDestinationCode(),
    'destType'          => $rs->getDestinationType(),
    'availCode'         => $availModality->getCode(),
    'availName'         => $availModality->getName(),
    'availContractName' => $availModality->getContractName(),
    'availContractIncomingOfficeCode' => $availModality->getContractIncomingOfficeCode(),
    'adultCount'        => $rs->getAdultCount(),
    'childCount'        => $rs->getChildCount(),
    'childAges'         => $params['childAges']
  );

  return $p;
}

?>
