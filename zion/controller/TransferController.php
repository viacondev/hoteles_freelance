<?php

include_once('../booking/BookingEngine.php');
include_once('model/Fee.php');

include_once('database/DBFreelance.php');
include_once('model/ModelFreelance.php');
include_once('model/Air_Config.php');

class TransferController extends ApplicationController {

  public static function home() {
  }

  public static function search($p) {
    global $rs, $childAges, $fee_agency;
    $factorFee    = FeeGeneral::getFactorFee($p['current_user'], $p['destination_code']);
    $dateIn_arr   = explode('/', $p['dateIn']);
    $dateIn       = $dateIn_arr[2] . $dateIn_arr[1] . $dateIn_arr[0];
    $dateOut_arr  = explode('/', $p['dateOut']);
    $dateOut      = $dateOut_arr[2] . $dateOut_arr[1] . $dateOut_arr[0];
    $timeIn       = $p['hourIn'] . $p['minsIn'];
    $timeOut      = $p['hourOut'] . $p['minsOut'];
    if ($p['typeIn'] == 'T') {
      $codeIn             = $p['terminalCodeIn'];
      $transferZoneCodeIn = '';
    }
    else {
      $codeIn_arr = explode('-', $p['hotelCodeIn']);
      $codeIn = $codeIn_arr[0];
      $transferZoneCodeIn = $codeIn_arr[1];
    }
    if ($p['typeOut'] == 'T') {
      $codeOut = $p['terminalCodeOut'];
      $transferZoneCodeOut = '';
    }
    else {
      $codeOut_arr = explode('-', $p['hotelCodeOut']);
      $codeOut = $codeOut_arr[0];
      $transferZoneCodeOut = $codeOut_arr[1];
    }
    $roundTrip = isset($p['roundTrip']);

    $params = array(
      'requestType'         => BookingRequestTypes::SearchTransferAvail,
      'dateIn'              => $dateIn,
      'timeIn'              => $timeIn,
      'typeIn'              => $p['typeIn'],
      'codeIn'              => $codeIn,
      'transferZoneCodeIn'  => $transferZoneCodeIn,
      'dateOut'             => $dateOut,
      'timeOut'             => $timeOut,
      'typeOut'             => $p['typeOut'],
      'codeOut'             => $codeOut,
      'transferZoneCodeOut' => $transferZoneCodeOut,
      'adultCount'          => $p['adultCount'],
      'childCount'          => $p['childCount'],
      'roundTrip'           => $roundTrip,
      'factorFee'           => $factorFee
    );

    $childAges = array();
    for ($i = 1; $i <= intval($_GET['childCount']) ; $i++) {
      $params['childAge_' . $i] = $_GET['childAge' . $i];
      $childAges[] = $_GET['childAge' . $i];
    }

    // Fee::setDestinationFee($_GET['destination_code']);

    $booking = new BookingEngine( );
    $rs = $booking->execRequest($params);

    $_SESSION['destination_code'] = $_GET['destination_code'];
    $_SESSION['destination'] = $_GET['destination'];
    $_SESSION['checkin'] = $_GET['dateIn'];
    $_SESSION['checkout'] = $_GET['dateOut'];
    $agencyMp   = AgencyMapping::find(array('agencybarry' => $_SESSION['current_agency']->id));
    $hotel_fee  = air_config::find(array('idcliente' => $agencyMp->agencybb));
    $fee_agency = (object)array('fee_prc' => $hotel_fee->hotel_fee_porcentaje, 'fee_monto' => $hotel_fee->hotel_fee_monto);
  }

}

?>
