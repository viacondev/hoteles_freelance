<?php

include_once('../booking/BookingEngine.php');
include_once('model/Fee.php');
include_once('model/Provider.php');
include_once('model/DomiturHotelMapping.php');
include_once('model/FeeGeneral.php');
include_once('model/HotelMappingProvider.php');
include_once('model/MotorConexion.php');


include_once('database/DBFreelance.php');
include_once('model/ModelFreelance.php');
include_once('model/Air_Config.php');

class AccommodationController extends ApplicationController {

  public static function home() {
  }

  public static function search($p) {

    // Query params en current_user vendran los la informacion del usuario
    $factorFee = FeeGeneral::getFactorFee($p['current_user'], $p['destination_code']);
    $agency = Agency::getCurrentRest($p['current_user']['agency_id']);
    $extraFee = 0;
    $destination = _get('destination_code', $p);
    $checkin_arr = explode('/', _get('checkin', $p));
    $checkin = $checkin_arr[2] . $checkin_arr[1] . $checkin_arr[0];
    $checkout_arr = explode('/', _get('checkout', $p));
    $checkout = $checkout_arr[2] . $checkout_arr[1] . $checkout_arr[0];
    $roomCount = _get('habitaciones', $p);
    $hotelName = _get('hotelName', $p);
    $hotelCode = _get('hotelCode', $p);
    $zoneCode = _get('zoneCode', $p);
    $orderPrice = _get('orderPrice', $p);
    $providers = _get('providers', $p);
    $stars = _get('star', $p);
    $regimen = _get('regimen', $p);
    $zone_name = (_getHas('zone_name', $p) && _get('zone_name', $p) != 'Todas las zonas') ? _get('zone_name', $p) : '';
    $hotel_destination = _get('hotel_destination');

    $params = array(
        'requestType'   => BookingRequestTypes::SearchHotelAvail,
        'destCode'      => $destination,
        'destType'      => 'SIMPLE',
        'checkin'       => $checkin,
        'checkout'      => $checkout,
        'roomCount'     => $roomCount,
        'page'          => _get('page', $p),
        'hotelName'     => $hotelName,
        'hotelCode'     => $hotelCode,
        'zoneCode'      => $zoneCode,
        'orderPrice'    => $orderPrice,
        'providers'     => $providers,
        'stars'         => $stars,
        'regimen'       => $regimen,
        'zone_name'     => $zone_name,
        'hotel_destination' => $hotel_destination,
        'last-version'  => _get('last-version', $p),
        'factorFee' => $factorFee,
        'extraFee' => $extraFee);
    
    $totalChildCount = 0;
    for ($i = 1; $i <= $roomCount; $i++) {
      $params['adultCount_' . $i] = _get('adultos' . $i, $p);
      $params['childCount_' . $i] = _get('ninos' . $i, $p);
      $totalChildCount += intval(_get('ninos' . $i, $p));
    }

    $childAges = array();
    $params['totalChildCount'] = $totalChildCount;
    for ($i = 1; $i <= $totalChildCount; $i++) {
      $params['childAge_' . $i] = _get('childAge' . $i, $p);
      $childAges[] = _get('childAge' . $i, $p);
    }

    // Fee::setDestinationFee($destination);
    
    $booking = new BookingEngine();
    $rs = $booking->execRequest($params);

    $_SESSION['destination_code'] = _get('destination_code', $p);
    $_SESSION['checkin'] = _get('checkin', $p);
    $_SESSION['checkout'] = _get('checkout', $p);
    if (_getHas('destination', $p)) {
      $_SESSION['destination'] = _get('destination', $p);
    }
    
    $agencyMp   = AgencyMapping::find(array('agencybarry' => $p['current_user']['agency_id']));
    $hotel_fee  = air_config::find(array('idcliente' => $agencyMp->agencybb));
    
    return array('rs' => $rs, 'params' => $params, 'childAges' => $childAges);
  }

  public static function search_ajax($p) {
  }

  public static function show($p) {
    // global $rs, $detail, $childAges, $params;
    $p['origen'] = 'yc';
    $p['agency_id']  = $p['current_user']['agency_id'];
    // $_GET['agency_id']  = 9;
    $p['user_id']    = $p['current_user']['id'];
    // $_GET['user_id']    = 177;

    $motor  =  new MotorConexion();
    $motor->build('hotelDetails', $p);
    $res    = $motor->consultar();
    // __logarr($res);
    return array('rs' => $res->rs, 'detail' => $res->detail, 'childAges' => $res->childAges);
    exit;

    $params = array(
        'requestType'   => BookingRequestTypes::HotelDetail,
        'hotelCode'     => _get('hotelCode', $p),
        'provider'     => _get('provider', $p));

    $booking = new BookingEngine();
    $detail = $booking->execRequest($params);

    if ($detail != NULL) {
        $rs = AccommodationController::search($p);
        $rs['detail'] = $detail;
        return $rs;
    }
  }

}

?>
