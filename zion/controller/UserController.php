<?php

global $current_user, $current_agency;
// include_once('../booking/BookingSerializable.php');
include_once(__DIR__ . '/ApplicationController.php');
include_once(__DIR__ . '/../../config/Config.php');
include_once(__DIR__ . '/../database/DB.php');
include_once(__DIR__ . '/../model/Model.php');
include_once(__DIR__ . '/../model/Admin.php');
include_once(__DIR__ . '/../model/Agency.php');
include_once(__DIR__ . '/../model/Config.php');
include_once(__DIR__ . '/../model/User.php');
include_once(__DIR__ . '/../model/UserMapping.php');
include_once(__DIR__ . '/../model/AgencyMapping.php');
include_once(__DIR__ . '/../model/FeeGeneral.php');
class UserController extends ApplicationController {

  public static function show() {
    global $user;

    $user = User::findById($_GET['id']);
  }

  public static function profile() {
    global $user, $current_user;

    $user = User::findById($current_user->id);
  }

  public static function edit() {
    global $agency, $user, $current_user;

    $user = User::findById($_GET['id']);
    $agency_id = $user->agency_id;

    $agency = Agency::findById($agency_id);

    // Security: Check if the user can be edited by the current user
    if (!$_SESSION['admin_role'] && $agency_id != $current_user->agency_id) {
      header('Location: ?controller=user&action=edit_profile');
    }
  }

  public static function edit_profile() {
    global $agency, $user, $current_user;

    $user = User::findById($current_user->id);
    $agency_id = $current_user->agency_id;

    $agency = Agency::findById($agency_id);
  }

  public static function add() {
    global $agency;

    if (isset($_GET['agency_id'])) {
      $agency_id = $_GET['agency_id'];
    }
    else if (isset($_POST['agency_id'])) {
      $agency_id = $_POST['agency_id'];
    }
    else {
      global $current_user;
      $agency_id = $current_user->agency_id;
    }
    $agency = Agency::findById($agency_id);
  }

  public static function create() {

    if (User::find(array("username" => $_POST['username'])) == NULL && !empty($_POST['username'])) {
      $user = new User();
      $user->build($_POST['username'], md5($_POST['password']), $_POST['name'], $_POST['lastname'],
                  $_POST['phone'], $_POST['mail'], $_POST['agency_id'], $_POST['role']);
      $user->save();

      header('Location: ./?controller=user&action=show&id=' . $user->id);
    }
    else {
      header('Location: ./?controller=user&action=add&error=error&agency_id=' . $_POST['agency_id']);
    }

  }

  public static function createbb($agency_id_bb, $user_id_bb, $user_name, $password, $name, $lastname, $phone, $mail, $rol) {

      $roluser  = $rol == 1 ? 'ADMIN' : 'COUNTER';
      $agency   = AgencyMapping::find(array('agencybb' => $agency_id_bb,'id_agency_type' => 2));
      
      $user     = new User();
      $user->build($user_name, md5($password), $name, $lastname,
                  $phone, $mail, $agency->agencybarry, $roluser);
      $user->save();

      $usermapping = new UserMapping();
      $usermapping->build($user_id_bb, $user->id, $name, 2);
      $usermapping->save();

  }

  public static function createAgency($agency_bb, $name, $address, $phone, $mail, $booking, $commission, $ciudad_id = 34) {
    // se creo por motivo de no realizar otra inclusion en boliviabooking del controlador Agency
    $agency = new Agency();
    $agency->build($name, $address, $phone, $mail, $booking, 1, $commission, $ciudad_id, 2);
    $agency->save();

    $agencymapping = new AgencyMapping();
    $agencymapping->build($agency_bb, $agency->id, $name, 2);
    $agencymapping->save();

    $commission = 12;
    $incentivo  = 0;
    $factor_fee = 0.79;
    $agencia    = $agency->id;
    $origen     = 'yc';
    $destino    = '';
    $fee        = new FeeGeneral();
    $fee->build($commission, $incentivo, $factor_fee, $agencia, $origen, $destino);
    $fee->save();
    return $agency->id;
  }

  public static function update() {
    global $current_user;
    $user = new User();
    $user->build($_POST['username'], md5($_POST['password']), $_POST['name'], $_POST['lastname'],
                  $_POST['phone'], $_POST['mail'], $_POST['agency_id'], $_POST['role']);
    $user->id = $_POST['id'];

    // Do not change pwd if it is null
    if (!isset($_POST['password']) || $_POST['password'] == '') {
      $user->password = User::findById($user->id)->password;
    }
    $user->save();

    if ($current_user->id != $user->id) {
      header('Location: ./?controller=user&action=show&id=' . $user->id);
    }
    else {
      header('Location: ./?controller=user&action=profile');
    }
  }

  /**
   * Session methods
   */
  public static function validateCredentials($username, $password) {
    $cryptedPwd = md5($password);

    return User::find(array('username' => $username, 'password' => $cryptedPwd));
  }

  public static function startSession($username, $password, $userCode = null) {
    $user = null;
    $sessionbarry = false;
    if (isset($username) && isset($password)) {
      $sessionbarry = true;
      $user = self::validateCredentials($username, $password);
    }
    else if ($userCode) {
      $user = User::find(array('user_code' => $userCode));
    }
    if ($user) {
      $_SESSION['current_user'] = $user;
      #$_SESSION['fee'] = Config::findById(1)->fee;
      $agency = Agency::findById($user->agency_id);
      $agency->logo = '';
      $_SESSION['current_agency'] = $agency;
      $_SESSION['agency_id'] = $agency->id;
      $_SESSION['agency_name'] = $agency->name;
      $_SESSION['agency_mail'] = $agency->mail;
      $_SESSION['agency_booking'] = $agency->booking;

      $admin = Admin::find(array('user_id' => $user->id));
      $_SESSION['admin_role'] = $admin ? $admin->role : NULL;
      $_SESSION['rol'] = isset($_GET['rol']) ? $_GET['rol'] : NULL ;
      $_SESSION['codigo_acceso_cliente'] = isset($_GET['codigo_acceso_cliente']) ? $_GET['codigo_acceso_cliente'] : NULL;
      $GLOBALS['current_user'] = $user;
      Log::info('Logged in', 'SESSION');
      if ($sessionbarry) {
        $url = isset($_POST['return']) ? $_POST['return'] : './';
        header('location: ' . $url);
      }
      return true;
    }
    else {
      // header('location: http://boliviabooking.com/boliviabooking/public/login');
    }
  }

  public static function startSessionBB($userId) {
    $usermapping = UserMapping::find(array('userbb' => $userId, 'id_agency_type' => 2));
    if($usermapping) {
      $user = User::findById($usermapping->userbarry);
      if ($user) {
          $_SESSION['current_user'] = $user;
          #$_SESSION['fee'] = Config::findById(1)->fee;
          $agency = Agency::findById($user->agency_id);
          $_SESSION['current_agency'] = $agency;
          $_SESSION['agency_id'] = $agency->id;
          $_SESSION['agency_name'] = $agency->name;
          $_SESSION['agency_mail'] = $agency->mail;
          $_SESSION['agency_booking'] = $agency->booking;

          $admin = Admin::find(array('user_id' => $user->id));
          $_SESSION['admin_role'] = $admin ? $admin->role : NULL;

          $GLOBALS['current_user'] = $user;
          //codigo que se genera para el user_code de inicio de sesion
          $rand_part = str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789".uniqid());
          $user->user_code = $rand_part;
          $user->save();
          // Log::info('Logged in', 'SESSION');

          // $url = isset($_POST['return']) ? $_POST['return'] : './';
          // header('location: ' . $url);
          // return true;
        }
    }
      // return false;
  }

  public static function getValuesForSessionBB($userId, $userCode) {
    $usermapping = UserMapping::find(array('userbb' => $userId, 'id_agency_type' => 2));
    if($usermapping) {
      $user = User::find(array('user_code' => $userCode));
      if ($user) {
        $admin = Admin::find(array('user_id' => $user->id));
        $rol = $admin ? $admin->role : NULL;
        return array('admin_role' => $rol, 'usercode' => $user->user_code);
      }
    }
    return array();
  }

  public static function verifySession() {
    $agency_blocked = array(196,662);
    $action_bloqued = array('search_ajax', 'search');
    // ESTE ARRAY $agency_blocked BLOQUEA A LAS AGENCIA PARA QUE NO PERMITAN BUSCAR DISPONIBILIDAD
    if (isset($_GET['action']) && in_array($_GET['action'], $action_bloqued) && isset($_SESSION['current_agency']) && in_array($_SESSION['current_agency']->id, $agency_blocked)) {
      header('location: http://yocounter.com/freelance/public/aereo/vuelos');
      return false;
    }
    else if (isset($_SESSION['current_user'])) {
      $GLOBALS['current_user'] = $_SESSION['current_user'];
      $GLOBALS['current_agency'] = $_SESSION['current_agency'];
    }
    else if(isset($_GET['current_user_code'])) {
      if (!isset($_SESSION['current_user'])) {
        $current_user_code = $_GET['current_user_code'];
        UserController::startSession(null, null, $current_user_code);
      }
    }
    else {
      header('location: http://yocounter.com/freelance/public/login');
      return false;
    }
  }

  public static function getCurrentUserFullName() {
    return $GLOBALS['current_user']->name . " " . $GLOBALS['current_user']->lastname;
  }

  public static function getCurrentUser() {
    if (isset($GLOBALS['current_user']->username)) {
      return $GLOBALS['current_user']->username;
    }
    return '';
  }

  function selfURL() {
    $s = (empty($_SERVER["HTTPS"]) ? "" : ($_SERVER["HTTPS"] == "on")) ? "s" : "";
    $s1 = strtolower($_SERVER["SERVER_PROTOCOL"]);
    $protocol = substr($s1, 0, strpos($s1, "/")) . $s;
    $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":" . $_SERVER["SERVER_PORT"]);
    return $protocol . "://" . $_SERVER['SERVER_NAME'] . $port . $_SERVER['REQUEST_URI'];
  }

}

?>
