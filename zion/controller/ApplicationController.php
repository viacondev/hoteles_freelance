<?php

include_once(__DIR__ . '/../model/Model.php');
//include_once(__DIR__ . '/../model/Log.php');
include_once(__DIR__ . '/../model/AgencyManager.php');
class ApplicationController {

  public static function loadAction($controller_name, $action_name) {

    ApplicationController::loadHelpers($controller_name, $action_name);

    $controller_class_name = ApplicationController::loadController($controller_name, $action_name);

    if ($controller_class_name != NULL) {
      $controller = new ReflectionClass($controller_class_name);
      $manager  = AgencyManager::find(array('agency_id' => $_SESSION['current_agency']->id));
      // Security for admin-only actions
      if (!$_SESSION['admin_role'] && $controller->hasProperty('admin_only') && count($manager) == 0) {
        $prop = $controller->getProperty('admin_only');
        if (in_array($action_name, $prop->getValue(new $prop->class))) {
          header('Location: ./');
        }
      }

      $action = $controller->getMethod($action_name);
      $_GET['current_user'] = (array)$_SESSION['current_user'];
      $res = $action->invoke($controller, $_GET);
      ApplicationController::setGlobalVars($res);
    }

    ApplicationController::loadView($controller_name, $action_name);
  }

  public static function loadController($controller_name, $action_name) {
    $controller_class_name = str_replace('_', ' ', $controller_name);
    $controller_class_name = ucwords($controller_class_name);
    $controller_class_name = str_replace(' ', '', $controller_class_name);
    $controller_class_name = $controller_class_name . 'Controller';
    $controller_dir = 'controller/' . $controller_class_name . '.php';
    if (!file_exists($controller_dir)) {
      return '';
    }
    include_once($controller_dir);
    return $controller_class_name;
  }

  public static function loadHelpers($controller_name, $action_name) {
    // Load PHP helpers
    $helper_dir = 'helpers/' . $controller_name . '_helper.php';
    if (file_exists($helper_dir)) {
      include($helper_dir);
    }
  }

  public static function loadView($controller_name, $action_name) {
    // Load view
    $view_dir = 'views/' . $controller_name . '/' . $action_name . '.php';
    if (file_exists($view_dir)) {
      include($view_dir);
    }
  }

  public static function loadActionJS($controller_name, $action_name) {
    // Load javascript
    $js_dir = 'assets/javascript/' . $controller_name . '.js';
    if (file_exists($js_dir)) {
      echo '<script src="' . $js_dir . '?202007070602"></script>';
    }
    $js_action_dir = 'assets/javascript/' . $controller_name . '_' . $action_name . '.js';
    if (file_exists($js_action_dir)) {
      echo '<script src="' . $js_action_dir . '?202007070605"></script>';
    }
  }

  public static function loadActionCSS($controller_name, $action_name) {
    // Load CSS
    $css_dir = 'assets/css/' . $controller_name . '.css';
    if (file_exists($css_dir)) {
      echo ' <link href="' . $css_dir . '?202007070602" rel="stylesheet" media="all">';
    }
    $css_action_dir = 'assets/css/' . $controller_name . '_' . $action_name . '.css';
    if (file_exists($css_action_dir)) {
      echo ' <link href="' . $css_action_dir . '?202007070605" rel="stylesheet" media="all">';
    }
  }

  public static function setGlobalVars($vars) {
    if (isset($vars) && is_array($vars)) {
      foreach ($vars as $key => $value) {
        $GLOBALS[$key] = $value;
      }
    }
  }

}

?>
