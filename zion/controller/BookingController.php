<?php

include_once('../booking/BookingEngine.php');
include_once('model/Booking.php');
include_once('model/Service.php');
include_once('model/ServiceStatus.php');
include_once('model/ServiceType.php');
include_once('model/Provider.php');
include_once('model/HotelRoom.php');
include_once('model/ServiceTransfer.php');
include_once('model/Discount.php');
include_once('model/Fee.php');
include_once('model/DomiturHotelMapping.php');
include_once('../lib/swiftmailer/swift_required.php');
include_once('model/FeeGeneral.php');
include_once('model/AgencyManager.php');
include_once('model/AgencyType.php');
include_once('model/MotorConexion.php');

class BookingController extends ApplicationController {

  public $admin_only = array('add', 'edit', 'create', 'books', 'pay', 'update_agency', 'syncHB');

  public static function add() {
    global $agencies;
    $filters   = array('status' => '1', 'id_agency_type' => AgencyType::YOCOUNTER);
    $manager  = AgencyManager::find(array('agency_id' => $_SESSION['current_agency']->id));
    if (count($manager) > 0) {
      $ciudad_id = $_SESSION['current_agency']->ciudad_id;
      $filters['ciudad_id'] = $ciudad_id;
    }
    $agencies = Agency::findAll($filters);
  }

  public static function edit() {
    global $book, $agencies;

    $book = Booking::findById(_get('id'));
    $agencies = Agency::findAll();
  }

  public static function create($p) {
    $book = saveManualBooking($p);
    // header('Location: ./?controller=booking&action=show&id=' . $book->id);
    return array('bookId' => $book->id);
  }

  public static function upload_image($p) {
    uploadImg();
  }

  public static function delete_file($p) {
    $result = Options_file($p['service_id'], "delete");
    return array('bookId' => $p['bookingId']);
  }

  public static function index() {
    global $bookings;
    $bookings = Booking::findAll();
  }

  public static function agency_books($p) {
    // global $bookings, $agenci_user;
    $agency_users = User::findAll(array('agency_id' => $p['current_user']['agency_id']));
    $manager      = AgencyManager::find(array('agency_id' => $_SESSION['current_agency']->id));
    $agency_id    = $p['current_user']['agency_id'];
    $agencies = array();
    if (count($manager) > 0) {
      $agencies     = Agency::findByCity($_SESSION['current_agency']->ciudad_id);
      $agency_id    = _get('agency_id', $p);
      $agency_users = array();
    }
    if (!isset($p['search_books'])) {
      $bookings = array();
      return array('bookings' => array(), 'agencies' => $agencies, 'agency_users' => $agency_users);
    }
    $filters = array();
    $filters['agency_id']   = $agency_id;
    $filters['user_id']     = _get('user_id', $p);
    $filters['status']      = _get('status', $p);
    $filters['note_number'] = _get('note_number', $p);
    $filters['destination_code'] = _get('destination_code', $p);
    $filters['datesFilter']      = _get('datesFilter', $p);
    $filters['checkin']          = _get('checkin', $p);
    $filters['checkout']         = _get('checkout', $p);
    $filters['ciudad_id']        =  $_SESSION['current_agency']->ciudad_id;

    $bookings = Booking::filterBooks($filters);

    if (count($manager)) {
      $bookings = Booking::filterByAgencyManager($filters);
    }
    foreach ($bookings as $book) {
      $book->cancellation_date  = $book->getServiceWithNearstCancellation();
      $book->booking_code       = $book->getBookingCode();
      $book->debit_note         = $book->getDebitNoteNumber();
      $book->services           = $book->getServices();
    }
    return array('bookings' => $bookings, 'agency_users' => $agency_users, 'agencies' => $agencies);
  }

  public static function books($p) {
    // if (!$_SESSION['admin_role']) { return; }

    // global $bookings, $agencies, $agency_users;
    $agencies       = Agency::findAllNameOnly();
    $agency_users   = User::findAll(array('agency_id' => _get('agency_id', $p)));
    if (!isset($p['search_books'])) {
      $bookings = array();
      return array('bookings' => array(), 'agencies' => $agencies, 'agency_users' => array());
    }
    $filters = array();
    $filters['agency_id']           = _get('agency_id', $p);
    $filters['user_id']             = _get('user_id', $p);
    $filters['status']              = _get('status', $p);
    $filters['note_number']         = _get('note_number', $p);
    $filters['destination_code']    = _get('destination_code', $p);
    $filters['datesFilter']         = _get('datesFilter', $p);
    $filters['checkin']             = _get('checkin', $p);
    $filters['checkout']            = _get('checkout', $p);

    $bookings = Booking::filterBooks($filters);
    foreach ($bookings as $book) {
      $book->cancellation_date  = $book->getServiceWithNearstCancellation();
      $book->booking_code       = $book->getBookingCode();
      $book->debit_note         = $book->getDebitNoteNumber();
      $book->services           = $book->getServices();
    }
    return array('bookings' => $bookings, 'agencies' => $agencies, 'agency_users' => $agency_users);
  }

  public static function pay() {
    $b = Booking::findById($_GET['id']);
    $b->status = ServiceStatus::PAYED;
    $b->save();
    header('Location: ./?controller=booking&action=show&id=' . $b->localizer);
  }

  public static function show($p) {
    // global $HBServices, $MBServices, $DTServices, $details, $book, $agency, $current_user;
    $book = array();
    if (isset($p['id'])) {
      $book = Booking::findById($p['id']);  
    }
    if (isset($p['debit_note'])) {
      $note_number = substr($p['debit_note'], 1);
      $book = Booking::find(array('debit_note' => $note_number));
    }
    if (count($book) == 0) {
      return array('status' => 'error la reserva no existe');
    }
    $book->booking_code = $book->getBookingCode();
    $services = $book->getServices();
    foreach ($services as $service) {
      if($service->type == ServiceType::HOTEL) {
        $service->rooms = $service->getRooms();
      }
    }
    $book->services = $services;
    $user = User::findById($book->user_id);
    $current_user = $user;
    $agency_id = $book->agency_id ? $book->agency_id : 1; // If no agency then use Barry
    $agency = Agency::findById($agency_id);
    $agency->logo = $agency->get_url_logo();
    // Security: Check if the user can see the booking
    //if (!$_SESSION['admin_role'] && $agency_id != $current_user->agency_id) {
      // header('Location: ?controller=booking&action=agency_books');
    //}

    $providerJuniper = array(159);
    $booking      = new BookingEngine();
    $localizers   = array();
    $HBServices   = array();
    $MBServices   = array();
    $MBBServices  = array();
    $DTServices   = array();
    $THServices   = array();
    $JPServices   = array();
    $RTServices   = array();
    $TBServices   = array();
    $details      = array();
    $arrServiceAllow = [ServiceType::HOTEL, ServiceType::TICKET];
    foreach ($book->getServices() as $service) {
      if (!in_array($service->type, $arrServiceAllow) && $service->provider_id == Provider::HOTELBEDS && !isset($localizers[$service->localizer]) && !$service->is_manual) {
        $ref = explode('-', $service->localizer);
        $fileNumber = $ref[1];
        $incomingOffice = $ref[0];
        $params = array(
          'requestType'     => BookingRequestTypes::PurchaseDetail,
          'fileNumber'      => $fileNumber,
          'incomingOffice'  => $incomingOffice,
          'provider'        => $service->provider_id,
          'version' 		    => 1
        );
        $HBService = $booking->execRequest($params);
        $HBServices[] = $HBService;
        foreach ($HBService->getBookingServices() as $s) {
          // Get service details
          if ($s->getServiceType() == 'ServiceHotel') {
            $serviceInfo = $s->getServiceInfo();
            $params = array(
                'requestType'   => BookingRequestTypes::HotelDetail,
                'hotelCode'     => $serviceInfo->getCode(),
                'provider'      => $service->provider_id);
            $details[] = $booking->execRequest($params);
          }
        }

        $localizers[$service->localizer] = true;
      }
      else if (in_array($service->type, $arrServiceAllow) && $service->provider_id == Provider::HOTELBEDS && !isset($localizers[$service->localizer]) && !$service->is_manual) {
        // nueva version hotelebds APITUDE
				$booking      = new BookingEngine(2);
				$params = array(
					'requestType'   => BookingRequestTypes::PurchaseDetail,
					'localizer'     => $service->localizer,
					'provider'      => $service->provider_id,
					'serviceType'		=> $service->type,
					'version'			  => 2
				);
				$HBService = $booking->execRequest($params);
				$HBServices[] = $HBService;
				foreach ($HBService->getBookingServices() as $s) {
					// Get service details
					if ($s->getServiceType() == 'ServiceHotel') {
						$serviceInfo = $s->getServiceInfo();
						$params = array(
							'requestType'   => BookingRequestTypes::HotelDetail,
							'hotelCode'     => $serviceInfo->getCode(),
							'provider'      => $service->provider_id,
							'version' 		=> 2
						);
						$details[] = $booking->execRequest($params);
					}
				}
      } else if ($service->provider_id == Provider::METHABOOK && !$service->is_manual) {
        $params = array(
          'requestType'     => BookingRequestTypes::PurchaseDetail,
          'localizer'       => $service->localizer,
          'provider'        => $service->provider_id
        );

        $MBService = $booking->execRequest($params);
        $MBServices[] = $MBService;

        $ss = $MBService->getBookingServices();
        $serviceInfo = $ss[0]->getServiceInfo();
        $params = array(
            'requestType'   => BookingRequestTypes::HotelDetail,
            'hotelCode'     => $serviceInfo->getCode(),
            'provider'      => $service->provider_id);
        $details[] = $booking->execRequest($params);
      }
      else if ($service->provider_id == Provider::METHABOOK2 && !$service->is_manual) {
        $params = array(
          'requestType'     => BookingRequestTypes::PurchaseDetail,
          'localizer'       => $service->localizer,
          'provider'        => $service->provider_id
        );

        $MBBService = $booking->execRequest($params);
        $MBBServices[] = $MBBService;

        $ss = $MBBService->getBookingServices();
        $serviceInfo = $ss[0]->getServiceInfo();
        $params = array(
            'requestType'   => BookingRequestTypes::HotelDetail,
            'hotelCode'     => $serviceInfo->getCode(),
            'provider'      => $service->provider_id);
        $details[$serviceInfo->getCode()] = $booking->execRequest($params);
      }
      else if($service->provider_id == Provider::DOMITUR && !isset($localizers[$service->localizer]) && !$service->is_manual) {
        $fileNumber = $service->localizer;
        $params = array(
          'requestType'     =>  BookingRequestTypes::PurchaseDetail,
          'localizer'      =>  $service->localizer,
          'provider'        => BookingProvider::DOMITUR
        );

        $DTService = $booking->execRequest($params);
        $DTServices[] = $DTService;

        $ss = $DTService->getBookingServices();
        $serviceInfo = $ss[0]->getServiceInfo();
        $map = DomiturHotelMapping::find(array('code' => $serviceInfo->getCode()));
        if($map != null) {
          $params = array(
              'requestType'   => BookingRequestTypes::HotelDetail,
              'hotelCode'     => $map->zion_code,
              'provider'      => BookingProvider::HOTELBEDS);
          $details[] = $booking->execRequest($params);
        }
        else
        {
          $params = array(
              'requestType'   => BookingRequestTypes::HotelDetail,
              'hotelCode'     => $serviceInfo->getCode(),
              'provider'      => BookingProvider::DOMITUR);
          $details[$serviceInfo->getCode()] = $booking->execRequest($params);
        }
      }
      else if($service->provider_id == Provider::TOURICO && !isset($localizers[$service->localizer]) && !$service->is_manual) {
        $params = array(
          'requestType'     => BookingRequestTypes::PurchaseDetail,
          'localizer'       => $service->localizer,
          'provider'        => $service->provider_id
        );

        $THService = $booking->execRequest($params);
        $THServices[] = $THService;

        $ss = $THService->getBookingServices();
        $serviceInfo = $ss[0]->getServiceInfo();//__logarr($serviceInfo);
        $params = array(
            'requestType'   => BookingRequestTypes::HotelDetail,
            'hotelCode'     => $serviceInfo->getCode(),
            'provider'      => $service->provider_id);
        $details[] = $booking->execRequest($params);
      }
      else if ($service->provider_id == Provider::RESTEL && !$service->is_manual) {
        $params = array(
          'requestType'     => BookingRequestTypes::PurchaseDetail,
          'localizer'       => $service->localizer,
          'provider'        => $service->provider_id
        );

        $RTService = $booking->execRequest($params);
        $RTServices[] = $RTService;

        $ss = $RTService->getBookingServices();
        $serviceInfo = $ss[0]->getServiceInfo();
        $params = array(
            'requestType'   => BookingRequestTypes::HotelDetail,
            'hotelCode'     => $serviceInfo->getCode(),
            'provider'      => $service->provider_id);
        $details[$serviceInfo->getCode()] = $booking->execRequest($params);
      }
      else if ($service->provider_id == Provider::TAILORBEDS && !$service->is_manual) {
        $params = array(
          'requestType'     => BookingRequestTypes::PurchaseDetail,
          'localizer'       => $service->localizer,
          'provider'        => $service->provider_id
        );

        $TBService = $booking->execRequest($params);
        $TBServices[] = $TBService;

        $ss = $TBService->getBookingServices();
        $serviceInfo = $ss[0]->getServiceInfo();
        $params = array(
            'requestType'   => BookingRequestTypes::HotelDetail,
            'hotelCode'     => $service->hotel_code,
            'provider'      => $service->provider_id);
        $hotelDetail            = $booking->execRequest($params);
        $hotelDetail->hotelCode = $serviceInfo->getCode();
        $details[$serviceInfo->getCode()] = $hotelDetail;
      }
      else if ($service->provider_id == Provider::NEMO && !$service->is_manual) {
        $params = array(
          'requestType'     => BookingRequestTypes::PurchaseDetail,
          'localizer'       => $service->localizer,
          'provider'        => $service->provider_id
        );

        $NMService = $booking->execRequest($params);
        $NMServices[] = $NMService;

        $ss = $NMService->getBookingServices();
        $serviceInfo = $ss[0]->getServiceInfo();
        $params = array(
            'requestType'   => BookingRequestTypes::HotelDetail,
            'hotelCode'     => $service->hotel_code,
            'provider'      => $service->provider_id);
        $hotelDetail            = $booking->execRequest($params);
        $hotelDetail->hotelCode = $serviceInfo->getCode();
        $details[$serviceInfo->getCode()] = $hotelDetail;
      }
      else if (in_array($service->provider_id, $providerJuniper) && !$service->is_manual) {
        $params = array(
          'requestType'     => BookingRequestTypes::PurchaseDetail,
          'localizer'       => $service->reservation_id,
          'provider'        => $service->provider_id
        );
        $JPService = $booking->execRequest($params);
        $JPServices[] = $JPService;

        $ss           = $JPService->getBookingServices();
        $serviceInfo  = $ss[0]->getServiceInfo();
        $params = array(
            'requestType'   => BookingRequestTypes::HotelDetail,
            'hotelCode'     => $serviceInfo->getCode(),
            'provider'      => $service->provider_id);
        $details[$serviceInfo->getCode()] = $booking->execRequest($params);
      }
    }
    // UserController::logOut();

    return array('HBServices' => $HBServices, 'MBServices' => $MBServices, 'MBBServices'  => $MBBServices,
                 'DTServices' => $DTServices, 'THServices' => $THServices, 'RTServices'   => $RTServices,
                 'TBServices' => $TBServices, 'NMServices' => $NMServices, 'JPServices' => $JPServices,   'details'    => $details,
                  'book'       => $book,      'current_user' => $current_user, 'agency'   => $agency);
  }

  public static function service_remove() {
    global $rs;

    // Avoid resending an ServiceRemove on reloading a page
    if (!isset($_POST['requestCode']) || (isset($_SESSION['requestCode']) && $_SESSION['requestCode'] == $_POST['requestCode'])) {
      if (isset($_SESSION['purchaseToken'])) {
        $params = array(
          'requestType'   => BookingRequestTypes::PurchaseDetail,
          'purchaseToken' => $_SESSION['purchaseToken']
        );
        $booking = new BookingEngine();
        $rs = $booking->execRequest($params);
      }
    }
    else {
      $params = array(
        'requestType'   => BookingRequestTypes::RemoveService,
        'purchaseToken' => $_POST['purchaseToken'],
        'SPUI'          => $_POST['SPUI']);

      $_SESSION['requestCode'] = $_POST['requestCode'];
      $_SESSION['shopping_items'] = intval($_SESSION['shopping_items']) - 1;
      if ($_SESSION['shopping_items'] == 0) {
        clearBookingSession();
      }
      else {
        $booking = new BookingEngine();
        $rs = $booking->execRequest($params);
      }
    }
  }

  public static function cancel_purchase() {
    //esta funcion no es utilizada
    global $HBServices, $details, $book, $agency;

    $book = Booking::findById($_GET['id']);
    $user = User::findById($book->user_id);
    $agency = Agency::findById($user->agency_id);

    // Security: Check if the user can cancel the booking
    if (!$_SESSION['admin_role'] && $agency_id != $current_user->agency_id) {
      header('Location: ?controller=booking&action=agency_books');
    }

    $localizers = array();
    $HBServices = array();
    foreach ($book->getServices() as $service) {
      if ($service->provider_id == Provider::HOTELBEDS && !isset($localizers[$service->localizer])) {
        $ref = explode('-', $service->localizer);
        $fileNumber = $ref[1];
        $incomingOffice = $ref[0];
        $params = array(
          'requestType'     => BookingRequestTypes::CancelPurchase,
          'fileNumber'      => $fileNumber,
          'incomingOffice'  => $incomingOffice
        );

        $booking = new BookingEngine();
        $rs = $booking->execRequest($params);
        $HBServices[] = $rs;
        $localizers[$service->localizer] = true;

        // Update Booking in DB
        $localizer = $rs->getReferenceIncomingOffice() . '-' . $rs->getReferenceFileNumber();
        $b = Booking::find(array('localizer' => $localizer));
        $b->status = ServiceStatus::CANCELLED;
        $b->price = $rs->getTotalPrice();
        $b->save();
        $services = Service::findAll(array('booking_id' => $b->id));
        foreach ($services as $service) {
          $service->status = ServiceStatus::CANCELLED;
          $service->price = 0;
          $service->net_price = 0;
          $service->commission = 0;
          $service->agency_fee = 0;
          $service->save();
        }
      }
    }

    // Send notification mails
    $msg = 'CANCELACI&Oacute;N DE LA RESERVA<br /><br />';
    $msg .= 'La reserva <strong>' . $localizer . '</strong>';
    $msg .= ' a nombre de ' . $rs->getHolder()->getName() . ' ' . $rs->getHolder()->getLastName() . ' fue cancelada.';
    $msg .= '<br /><br />Puedes ver la reserva desde:<br />';
    $msg .= '<a href="http://boliviabooking.com/hotelesboliviabooking/zion/?controller=booking&action=show&id=' . $b->id . '">';
    $msg .= 'http://boliviabooking.com/hotelesboliviabooking/zion/?controller=booking&action=show&id=' . $b->id . '</a>';
    $msg .= '<br /><br /><div style="font-size:0.9em; color:#999;">Por favor no respondas a este correo porque es un correo automatizado sólo para notificaciones.</div>';
    sendNotificationMail('Reserva ' . $localizer . ' cancelada', $msg);
  }

  public static function modify_purchase() {
    //esta funcion no es utilizada
    global $rs;

    $ref = explode('-', $_GET['id']);
    $fileNumber = $ref[1];
    $incomingOffice = $ref[0];
    $params = array(
      'requestType'     => BookingRequestTypes::PurchaseDetail,
      'fileNumber'      => $fileNumber,
      'incomingOffice'  => $incomingOffice
    );

    $booking = new BookingEngine();
    $rs = $booking->execRequest($params);

    $_SESSION['purchaseToken'] = $rs->getPurchaseToken();
    $params = array(
      'requestType'   => BookingRequestTypes::PurchaseDetail,
      'purchaseToken' => $_SESSION['purchaseToken']
    );

    $rs = $booking->execRequest($params);
  }

  public static function update_agency() {
    $book = Booking::findById($_POST['id']);
    $book->updateColumn('agency_id', $_POST['agency_id']);

    header('Location: ./?controller=booking&action=show&id=' . $book->localizer);
  }

  public static function syncHB() {
    global $HBServices, $details, $book, $agency;

    $HBServices = array();

    $ref = explode('-', $_GET['localizer']);
    $fileNumber = $ref[1];
    $incomingOffice = $ref[0];
    $params = array(
      'requestType'     => BookingRequestTypes::PurchaseDetail,
      'fileNumber'      => $fileNumber,
      'incomingOffice'  => $incomingOffice
    );

    $booking = new BookingEngine();
    $rs = $booking->execRequest($params);
    $HBServices[] = $rs;

    // Save the booking to the DB
    if ($rs->getStatus() == "BOOKING") {
      $localizer = $rs->getReferenceIncomingOffice() . '-' . $rs->getReferenceFileNumber();
      $status = $_SESSION['agency_booking'] == 'EMIT' ? ServiceStatus::CONFIRMED : ServiceStatus::PENDING;
      $holder = $rs->getHolder()->getName() . ' ' . $rs->getHolder()->getLastName();
      $confirm_date = date('Y-m-d H:i:s');
      $user_id = $GLOBALS['current_user']->id;
      $agency_id = $GLOBALS['current_user']->agency_id;
      $agent = '';
      $agency = '';
      $operator_id = '';
      $remarks = '';
      $operator_remarks = '';
      $view_mode = 0;
      $package_description = '';

      $book = new Booking();
      $book->build($localizer, $status, $holder, $confirm_date, $user_id, $agency_id, $agent, $agency, $operator_id, $remarks, $operator_remarks, $view_mode, $package_description);
      $book->save();
      $user = User::findById($user_id);
      $agency = Agency::findById($user->agency_id);

      $details = array();
      // Save Services
      $total_services = count($rs->getBookingServices());
      foreach ($rs->getBookingServices() as $service) {
        $serviceInfo = $service->getServiceInfo();
        Fee::setDestinationFee($serviceInfo->getDestinationCode());

        $type = ServiceType::getTypeFromHotelBedType($service->getServiceType());
        $name = $serviceInfo->getName();
        $category = '';
        $status = $book->status;
        $destination_code = $serviceInfo->getDestinationCode();
        $cancellation_date = '';
        $cancellation_fee = 0;
        $checkin = $service->getDateFrom();
        $checkout = $service->getDateTo();
        $time = NULL;
        $adult_count = 0;
        $child_count = 0;
        $child_ages = '';
        $net_price = $service->getTotalAmount();
        $price = Agency::getUserPrice($net_price, false);
        $commission = round($price * (Agency::getCurrent()->commission / 100), 2);
        $incentive = 2;
        $agency_fee = Agency::getAgencyFee($price);
        $currency = $service->getCurrency();
        $booking_id = $book->id;
        $provider_id = Provider::HOTELBEDS;
        $includes = '';
        $is_manual = false;
        $show_voucher = true;

        // Set cancellation date
        if ($type == ServiceType::HOTEL) {
          $category = $serviceInfo->getCategoryCode();
          $_rooms = $serviceInfo->getRoomsAvail();
          $_policies = $_rooms[0]->getCancellationPolicies();
        }
        else {
          $_policies = $serviceInfo->getCancellationPolicies();
          $adult_count = $serviceInfo->getAdultCount();
          $child_count = $serviceInfo->getChildCount();
          $child_ages = ''; // TODO: set child ages
          if ($type == ServiceType::TRANSFER) {
            $category = $serviceInfo->getTransferType();
            $time = $serviceInfo->getTimeFrom();
          }
        }
        $_policy = count($_policies) > 0 ? $_policies[0] : NULL;
        $c_date = $_policy ? $_policy->getDateFrom() : '00/00/0000';
        $c_date_arr = explode('/', $c_date);
        $cancellation_date = $c_date_arr[2] . '-' . $c_date_arr[1] . '-' . $c_date_arr[0];

        $serviceDB = new Service();
        $serviceDB->build($type, $name, $category, $localizer, $status, $destination_code, $cancellation_date, $cancellation_fee,
                        $checkin, $checkout, $time, $adult_count, $child_count, $child_ages, $price, $currency, $booking_id,
                        $provider_id, $net_price, $commission, $agency_fee, $incentive, $includes, $is_manual, $show_voucher);
        $serviceDB->save();

        if ($type == ServiceType::HOTEL) {
          foreach ($_rooms as $room) {
            $roomDB = new HotelRoom();
            $childAges = array();
            foreach ($room->getGuestList() as $guest) {
              if ($guest->getType() == 'CH') {
                $childAges[] = $guest->getAge();
              }
            }
            $childAges_str = implode(', ', $childAges);

            $roomDB->build($room->getRoomType() . ' x ' . $room->getRoomCount(), $room->getBoard(), $room->getAdultCount(), $room->getChildCount(), $childAges_str,
                  Agency::getUserPrice($room->getPrice()), $room->getPrice(), $serviceDB->id);
            $roomDB->save();
          }

          // Load hotel details
          $params = array(
              'requestType'   => BookingRequestTypes::HotelDetail,
              'hotelCode'     => $serviceInfo->getCode());
          $details[$serviceInfo->getCode()] = $booking->execRequest($params);
        }
        else if ($type == ServiceType::TRANSFER) {
          $origin = $serviceInfo->getPickupLocationName();
          $destination = $serviceInfo->getDestLocationName();
          $transferType = $serviceInfo->getTransferType();
          $time_in = $serviceInfo->getTimeFrom();
          $flight_in = $serviceInfo->getTravelNumber();

          $transferDB = new ServiceTransfer();
          $transferDB->build($origin, $destination, $transferType, $time_in, NULL, $flight_in, NULL, $serviceDB->id);
          $transferDB->save();
        }
      }
    }
  }

  public static function purchase($p) {
    // UserController::startSession($p['userId']);
    // $agency = Agency:getCurrentRest($p['current_user']['agency_id']);
    $p['origen'] 		  = 'yc';
		$p['agency_id']  	= $p['current_user']['agency_id'];
		$p['user_id']    	= $p['current_user']['id'];
		$motor  			=  new MotorConexion();
		$motor->build('purchase', $p);
		$resp    = $motor->consultar();
		return $resp;
		exit;

    $factorDefault      = FeeGeneral::getFactorDefault('yc');
    $factorAgency       = FeeGeneral::getFactorFeeAgency($p['current_user']['agency_id'], 'yc');
    $factorProvider     = FeeGeneral::getFactorFeeProvider('yc');
		$arrFactorProvider  = [];
    foreach ($factorProvider as $key => $pr) {
      if (!isset($arrFactorProvider[$pr->proveedor])) {
        $arrFactorProvider[$pr->proveedor] = (object)['agency_comission'=> $pr->agency_comission,'incentive_counter'=>$pr->incentive_counter,'totalMarckUp'=>$pr->totalMarckup];
      }
    }

    $agency           = Agency::findById($p['current_user']['agency_id']);
    $userName         = $p['current_user']['name'] . ' ' . $p['current_user']['lastname'];
    $agencyReference  = substr($p['current_user']['name'] . '/' . $agency->name,0,20);

    $params = array(
      'requestType'     => BookingRequestTypes::ConfirmPurchase,
      'holderType'      => $p['customerType_1_1'],
      'holderName'      => $p['customerName_1_1'],
      'holderLastName'  => $p['customerLastName_1_1'],
      'agencyReference' => $agencyReference,
      'servicesCount'   => $p['servicesCount'],
      'holderPhone'     => _post('holderPhone', $p),
      'holderAddress'   => _post('holderAddress', $p),
      'holderPostalCode'=> _post('holderPostalCode', $p),
      'holderCity'      => _post('holderCity', $p),
      'holderCountry'   => _post('holderCountry', $p),
      'holderCountryCode' => _post('holderCountryCode', $p),
      'book_params' => _post('book_params', $p)
    );

    $s_count = intval($p['servicesCount']);
    for ($i = 1; $i <= $s_count; $i++) {
      $c_count = $p['customerCount_' . $i];
      for ($j = 1; $j <= $c_count; $j++) {
        $params['customerType_' . $i . '_' . $j]     = $p['customerType_' . $i . '_' . $j];
        $params['customerName_' . $i . '_' . $j]     = $p['customerName_' . $i . '_' . $j];
        $params['customerLastName_' . $i . '_' . $j] = $p['customerLastName_' . $i . '_' . $j];
        $params['customerAge_' . $i . '_' . $j]      = $p['customerAge_' . $i . '_' . $j];
        $params['CustomerId_' . $i . '_' . $j]       = $p['CustomerId_' . $i . '_' . $j];
        if (isset($p['customerBirthDate_' . $i . '_' . $j])) {
          $params['customerBirthDate_' . $i . '_' . $j] = $p['customerBirthDate_' . $i . '_' . $j];
        }
      }
      $params['customerCount_' . $i] = $c_count;
      $params['SPUI_' . $i] = $p['SPUI_' . $i];
      $params['scartId_' . $i] = _post("scartId_$i", $p);
      $params['dataRequired_' . $i] = _post("dataRequired_$i");

      // Details for the ticktes (Used by hotelbeds)
      if (isset($p['serviceDetailCount_' . $i])) {
        $d_count = $p['serviceDetailCount_' . $i];
        for ($j = 1; $j <= $d_count; $j++) {
          $params['serviceDetailName_' . $i . '_' . $j] = $p['serviceDetailName_' . $i . '_' . $j];
          $params['serviceDetailCode_' . $i . '_' . $j] = $p['serviceDetailCode_' . $i . '_' . $j];
          $params['serviceDetail_' . $i . '_' . $j] = $p['serviceDetail_' . $i . '_' . $j];
        }
        $params['serviceDetailCount_' . $i] = $p['serviceDetailCount_' . $i];
      }

      $params['serviceType_' . $i] = $p['serviceType_' . $i];

      // Service comments
      if (_post("serviceType_$i", $p) == 'ServiceHotel') {
        $comments = '';
        if(!empty($p["room_attr_comment_$i"])) {
          foreach($p["room_attr_comment_$i"] as $rcomment) {
            $comments .= $rcomment;
          }
        }
        $comments .= _post("client_comments_$i", $p);
        $params["comments_$i"] = $comments;
        $params["version"] = 2;
      }
    }

    $booking    = new BookingEngine();
    $rs         = $booking->execRequest($params);
    $services   = $rs->getBookingServices();

    $book_params  = (array)json_decode($p['book_params']);

    if(count($services[0]->getErrors()) > 0) {
      return array('errors' => $services[0]->getErrors());
    }
    // Save the booking to the DB
    if (isset($services) && count($services) > 0) {
      $localizer  = '';
      $status       = ServiceStatus::PENDING;
      $holder       = "";
      $name         = _post('customerName_1_1', $p);
      $last_name    = _post('customerLastName_1_1', $p);
      $confirm_date = date('Y-m-d H:i:s');
      $user_id      = $p['current_user']['id'];
      $agency_id    = $p['current_user']['agency_id'];
      $agent        = '';
      $agency       = '';
      $operator_id  = '';
      $remarks      = '';
      $operator_remarks = '';
      $view_mode    = 0;
      $package_description = '';
      $booking_code = 0;

      $book = new Booking();
      $book->build($localizer, $status, $holder, $confirm_date, $user_id, $agency_id, $agent, $agency, $operator_id, $remarks, $operator_remarks, $view_mode, $package_description,$booking_code, '', '', NULL, '', 0, $name, $last_name);
      $book->save();
      $user = User::findById($user_id);
      $agency = Agency::findById($p['current_user']['agency_id']);
      // Save Services
      $total_services = count($services);
      foreach ($services as $service) {
        $serviceInfo  = $service->getServiceInfo();
        $hotel_code = $serviceInfo->getCode();
        $type         = ServiceType::getTypeFromHotelBedType($service->getServiceType());
        $name         = $serviceInfo->getName();
        $category     = '';
        $localizer    = $service->getLocalizer();
        $rs_id        = $service->getSPUI();
        $status       = $book->status;
        $destination_code   = $serviceInfo->getDestinationCode();
        $cancellation_date  = '';
        $cancellation_fee   = 0;
        $checkin  = $service->getDateFrom();
        $checkout = $service->getDateTo();
        $time     = NULL;
        $adult_count  = 0;
        $child_count  = 0;
        $child_ages   = '';
        $net_price    = $service->getTotalAmount();

        $provider     	= $serviceInfo->getProvider();
				if ($service->getIsConector()) {
					$provider = $service->getSubProvider();
				}

        // Convierte el neto en total segun el marckup 
					$marckUp = $factorDefault;
					if (count($factorAgency) > 0 && isset($factorAgency[$provider])) {
						// si una agencia en especidigo tiene un marckup asignado
						$marckUp = $factorAgency[$provider];
					} else if (isset($arrFactorProvider[$provider])) {
						// si un proveedor en especifico tiene un marckup asignado
						$marckUp = $arrFactorProvider[$provider];
					}
					$price = round($net_price / $marckUp->totalMarckUp, 2);
        // Fin Conversion
        //NORMAL
        $commission            = round($price * ($marckUp->agency_comission / 100), 2);
        $enable_commission_suc = 0;
        $commission_suc        = 0;
        //END NORMAL

        //PARA SACAR EL % PARA LA SUCURSAL
        if (Agency::isAgencySucursal($agency)) {
          $enable_commission_suc  = 1;
          $porcentaje_comis = 4;
          $commission_suc   = ($porcentaje_comis * $price) / 100;
        }
        // $incentivo_counter  = FeeGeneral::getIncentiveUser($p['current_user']);
        $incentive    = ($price * $marckUp->incentive_counter) / 100;
        // $arrComission = array(9,128,147);
        // if (in_array($_SESSION['current_agency']->id, $arrComission)) {
        //   $commission = round($price * ($agency->commission / 100), 2);
        //   $incentive  = ($price * 2) / 100;
        // }
        // END LOW COST

        // $agency_fee = Agency::getAgencyFee($price);
        $agency_fee     = $agency->getAgencyFeeRest($price,$agency->id);
        $currency       = $service->getCurrency();
        $booking_id     = $book->id;
        $provider_id    = $serviceInfo->getProvider();
        $includes       = '';
        $is_manual      = false;
        $show_voucher   = true;
        $SupplementList = $service->getSupplementList();

        // Set cancellation date
        if ($type == ServiceType::HOTEL) {
            $category   = $serviceInfo->getCategoryCode();
            $_rooms     = $serviceInfo->getRoomsAvail();
            $_policies  = $_rooms[0]->getCancellationPolicies();
        }
        else {
            $_policies    = $serviceInfo->getCancellationPolicies();
            $adult_count  = $serviceInfo->getAdultCount();
            $child_count  = $serviceInfo->getChildCount();
            $child_ages   = ''; // TODO: set child ages
            if ($type == ServiceType::TRANSFER) {
                $category = $serviceInfo->getTransferType();
                $time     = $serviceInfo->getTimeFrom();
            }
        }
        $_policy = NULL;
        if(count($_policies) > 0) {
            $policy_menor = '';
            foreach ($_policies as $policy) {
                if($policy_menor == '') {
                    $policy_menor = $policy->getDateFrom();
                    $_policy = $policy;
                    continue;
                }
                $result = date_diff(new DateTime(dateFormatForDB($policy_menor)), new DateTime(dateFormatForDB($policy->getDateFrom()))); 
                // date_diff(fecha_comparada, fecha menor encontrada);
                $diferencia = (int) $result->format('%R%a');
                if($diferencia < 0) { //hace la diferencia es con numero negativo significa que la fecha es menor
                    $policy_menor = $policy->getDateFrom();
                    $_policy = $policy;
                }
            }
        }
        // $_policy = count($_policies) > 0 ? $_policies[0] : NULL;
        $c_date = $_policy ? $_policy->getDateFrom() : '00/00/0000';
        $c_date_arr = explode('/', $c_date);
        $cancellation_date = $c_date_arr[2] . '-' . $c_date_arr[1] . '-' . $c_date_arr[0];
        $policy_price = $_policy ? $_policy->getAmount() : 0;
        // $cancellation_fee = FeeGeneral::getPriceWithFee($policy_price, $p['current_user']);
        $cancellation_fee 	= round($policy_price / $marckUp->totalMarckUp, 2);
        $serviceDB = new Service();
        $serviceDB->build($type, $hotel_code , $name, $category, $localizer, $rs_id, $status, $destination_code, $cancellation_date,
                          $cancellation_fee, $checkin, $checkout, $time, $adult_count, $child_count, $child_ages, $price, $currency, $booking_id,
                          $provider_id, $net_price, $commission, $agency_fee, $incentive, $includes, $is_manual, $show_voucher,
                          $enable_commission_suc, $commission_suc);

        $serviceDB->save();

        if ($type == ServiceType::HOTEL) {
          foreach ($_rooms as $room) {
            $roomDB = new HotelRoom();
            $childAges = array();
            foreach ($room->getGuestList() as $guest) {
              if ($guest->getType() == 'CH') {
                $childAges[] = $guest->getAge();
              }
            }
            $childAges_str  = implode(', ', $childAges);
            // $roomPrice      = FeeGeneral::getPriceWithFee($room->getPrice(), $p['current_user']);
            $roomPrice      = round($room->getPrice() / $marckUp->totalMarckUp, 2);
            // if (isset($provider) && $provider == Provider::TAILORBEDS) {
            //   $price = round($net_price / 0.79, 2);//MARKUPNORMAL
              // $price = round($net_price/0.86, 2);//MARKUP HOT DAY
            // }
            $roomDB->build($room->getRoomType() . ' x ' . $room->getRoomCount(), $room->getBoard(), $room->getAdultCount(), $room->getChildCount(), $childAges_str,
                $roomPrice  , $room->getPrice(), $serviceDB->id);
            $roomDB->save();
          }

          // Save Suplement
          if (!empty($SupplementList)) {
              foreach ($SupplementList as $value) {
                $suppPrice = round($value->getAmount() / $marckUp->totalMarckUp, 2);
                $roomDB = new HotelRoom();
                $roomDB->build("Suplemento: " . $value->getDescription(), " ", 0, "", 0, $suppPrice, $p['current_user'], 0, $serviceDB->id);
                $roomDB->save();
              }
          }
          // Save Discount
          $Discount = $serviceInfo->getDiscountList();
          if (!empty($Discount)) {
               foreach ($Discount as $value) {
                $discPrice = round($value->getAmount() / $marckUp->totalMarckUp, 2);
                $roomDB = new HotelRoom();
                $roomDB->build("Descuentos: " . $value->getDescription(), " ", "", "", 0,  $discPrice, 0, $serviceDB->id);
                $roomDB->save();
              }
          }

          // Save the discounts
          // foreach ($serviceInfo->getDiscountList() as $discount) {
          //   $d = new Discount();
          //   // $d_amount = getPriceWithFee($discount->getAmount());
          //   $d_amount = FeeGeneral::getPriceWithFee($discount->getAmount(), $p['current_user']);
          //   $d->build($discount->getDescription(), $d_amount, $serviceDB->id);
          // }

          // Load hotel details
          // $params = array(
          //     'requestType'   => BookingRequestTypes::HotelDetail,
          //     'hotelCode'     => $serviceInfo->getCode());
          // $details[$serviceInfo->getCode()] = $booking->execRequest($params);
        }
        else if ($type == ServiceType::TRANSFER) {
          $origin = $serviceInfo->getPickupLocationName();
          $destination = $serviceInfo->getDestLocationName();
          $transferType = $serviceInfo->getTransferType();
          $time_in = $serviceInfo->getTimeFrom();
          if ($transferType == 'OUT') {
            $time_in = $serviceInfo->getTravelInfoDepartTime();
          }
          $flight_in = $serviceInfo->getTravelNumber();

          $transferDB = new ServiceTransfer();
          $transferDB->build($origin, $destination, $transferType, $time_in, NULL, $flight_in, NULL, $serviceDB->id);
          $transferDB->save();
        }
      }

      // ShoppingCart::emptyCart();
      if (isset($_POST['requestCode'])) {
        // $_SESSION['requestCode'] = $_POST['requestCode'];
      }
      try { 
        include_once(__DIR__ . '/../helpers/template_mail/mail_book.php');
        sendNotificationMail('Reserva ' . $book->getBookNumber() . ' realizada', $msg, $p['current_user']);
      } catch(Exception $e) {
        \Log::error($e->getMessage(), 'Error al Enviar Correo ' . $book->id);
      }
      // header('Location: ?controller=booking&action=show&id=' . $book->id);
      return array('bookId' => $book->id);
    }
  }

  public static function cancel_book($p) {
    //cuando cancelan la reservas online
    // global $current_user;
    $book = Booking::findById($p['id']);
    $user = User::findById($book->user_id);
    $agency = agency::findById($p['current_user']['agency_id']);
    // Security: Check if the user can cancel the booking
    // if (!$_SESSION['admin_role'] && $book->agency_id != $current_user->agency_id) {
      // header('Location: ?controller=booking&action=agency_books');
    // }

    $cancellation_fee = 0;
    $cancellations    = false;
    $localizers       = array();
    $juniperProvider  = array(159);
    $arrServiceAllow = [ServiceType::HOTEL, ServiceType::TICKET];
    foreach ($book->getServices() as $service) {
      // Cancel Hotelbeds services
      if (!in_array($service->type, $arrServiceAllow) && $service->provider_id == Provider::HOTELBEDS && !isset($localizers[$service->localizer]) && !$service->is_manual) {

        $ref = explode('-', $service->localizer);
        $fileNumber = $ref[1];
        $incomingOffice = $ref[0];
        $params = array(
          'requestType'     => BookingRequestTypes::CancelPurchase,
          'fileNumber'      => $fileNumber,
          'incomingOffice'  => $incomingOffice,
          'provider'        => Provider::HOTELBEDS
        );

        $booking = new BookingEngine();
        $rs = $booking->execRequest($params);
        $localizers[$service->localizer] = true;

        // Update Services in DB
        $ss = Service::findAll(array('localizer' => $service->localizer));
        foreach ($ss as $s) {
          if ($book->status == ServiceStatus::CONFIRMED) {
            $s->status = ServiceStatus::ANNULLED;
          }
          else {
            $s->status = ServiceStatus::CANCELLED;
          }
          $s->save();
        }

        $cancellation_fee += $rs->getTotalPrice();
        $cancellations = true;
      } else if (in_array($service->type, $arrServiceAllow) && $service->provider_id == Provider::HOTELBEDS && !isset($localizers[$service->localizer]) && !$service->is_manual) {
        // nueva version APITUDE
				$ref = explode('-', $service->localizer);
				$params = array(
					'requestType' 	=> BookingRequestTypes::CancelPurchase,
					'localizer'   	=> $service->localizer,
					'provider'      => Provider::HOTELBEDS,
					'serviceType' 	=> $service->type,
					'version' 		=> 2
				);
				$booking 			= new BookingEngine();
				$rs 				= $booking->execRequest($params);
				$localizers[$service->localizer] = true;
				// Update Services in DB
				$ss = Service::findAll(array('localizer' => $service->localizer));
				foreach ($ss as $s) {
					if ($book->status == ServiceStatus::CONFIRMED) {
						$s->status = ServiceStatus::ANNULLED;
					} else {
						$s->status = ServiceStatus::CANCELLED;
					}
					$s->save();
				}
				$cancellation_fee 	+= $rs->getTotalPrice();
				$cancellations 		= true;
      } else if (($service->provider_id == Provider::DOMITUR || $service->provider_id == Provider::METHABOOK || $service->provider_id == Provider::METHABOOK2) && !$service->is_manual) {
        $params = array(
          'requestType'     => BookingRequestTypes::CancelPurchase,
          'localizer'       => $service->localizer,
          'provider'        => $service->provider_id
        );

        $booking = new BookingEngine();
        $rs = $booking->execRequest($params);

        if ($rs != NULL) {
          // Update Service in DB
          if ($book->status == ServiceStatus::CONFIRMED) {
            $service->status = ServiceStatus::ANNULLED;
          }
          else {
            $service->status = ServiceStatus::CANCELLED;
          }
          //$service->status = ServiceStatus::ANNULLED;
          $service->save();

          $cancellation_fee += $rs->getTotalPrice();
          $cancellations = true;
        }
      }
      else if ($service->provider_id == Provider::TOURICO) {
        $params = array(
          'requestType'     => BookingRequestTypes::CancelPurchase,
          'localizer'       => $service->localizer,
          'provider'        => $service->provider_id,
          'rs_id'           => $service->reservation_id
        );
        $booking = new BookingEngine();
        $rs = $booking->execRequest($params);

        if ($rs != NULL) {
          // Update Service in DB
          if ($book->status == ServiceStatus::CONFIRMED) {
            $service->status = ServiceStatus::ANNULLED;
          }
          else {
            $service->status = ServiceStatus::CANCELLED;
          }
          //$service->status = ServiceStatus::ANNULLED;
          $service->save();

          $cancellation_fee += $rs->getTotalPrice();
          $cancellations = true;
        }
      }
      else if ($service->provider_id == Provider::RESTEL) {
        $params = array(
          'requestType'     => BookingRequestTypes::CancelPurchase,
          'localizer'       => $service->localizer,
          'provider'        => $service->provider_id,
          'rs_id'           => $service->reservation_id
        );
        $booking = new BookingEngine();
        $rs = $booking->execRequest($params);

        if ($rs != NULL) {
          // Update Service in DB
          if ($book->status == ServiceStatus::CONFIRMED) {
            $service->status = ServiceStatus::ANNULLED;
          }
          else {
            $service->status = ServiceStatus::CANCELLED;
          }
          //$service->status = ServiceStatus::ANNULLED;
          $service->save();

          $cancellation_fee += $rs->getTotalPrice();
          $cancellations = true;
        }
      }
      else if ($service->provider_id == Provider::TAILORBEDS) {
        $params = array(
          'requestType'     => BookingRequestTypes::CancelPurchase,
          'localizer'       => $service->localizer,
          'provider'        => $service->provider_id
        );
        $booking  = new BookingEngine();
        $rs       = $booking->execRequest($params);

        if ($rs != NULL) {
          // Update Service in DB
          if ($book->status == ServiceStatus::CONFIRMED) {
            $service->status = ServiceStatus::ANNULLED;
          }
          else {
            $service->status = ServiceStatus::CANCELLED;
          }
          //$service->status = ServiceStatus::ANNULLED;
          $service->save();

          $cancellation_fee += $rs->getTotalPrice();
          $cancellations = true;
        }
      }
      else if (in_array($service->provider_id, $juniperProvider)) {
        $params = array(
          'requestType'     => BookingRequestTypes::CancelPurchase,
          'localizer'       => $service->reservation_id,
          'provider'        => $service->provider_id
        );

        $booking = new BookingEngine();
        $rs = $booking->execRequest($params);

        if ($rs != NULL) {
          // Update Service in DB
          if ($book->status == ServiceStatus::CONFIRMED) {
            $service->status = ServiceStatus::ANNULLED;
          }
          else {
            $service->status = ServiceStatus::CANCELLED;
          }
          //$service->status = ServiceStatus::ANNULLED;
          $service->save();

          $cancellation_fee += $rs->getTotalPrice();
          $cancellations = true;
        }
      }
    }
    $send_messagge = false;
    if($book->status == ServiceStatus::PENDING && $cancellations) {
      $allCancelled = true;
      foreach ($book->getServices() as $s) {
        if ($s->status != ServiceStatus::CANCELLED && $s->status != ServiceStatus::ANNULLED) {
          $allCancelled = false;
          break;
        }
      }
      if ($allCancelled) {
        $book->status = ServiceStatus::CANCELLED;
      }

      $book->cancel_date = date('Y-m-d H:i:s');
      $book->save();
      if ($cancellation_fee != 0) {
          $net_price = $cancellation_fee;
          $price = $net_price / Config::findById(1)->fee;
          $serviceDB = new Service();
          $serviceDB->build(ServiceType::OTHER, 'Gastos de cancelaci&oacute;n', '', '', '', ServiceStatus::CONFIRMED, '', '', 0,
                  '', '', '', 0, 0, '', $price, $book->currency, $book->id, 0, $net_price, 0, 0, 0, '', 1, 0, 0);
          $serviceDB->save();
      }
      else {
        foreach ($book->getServices() as $s) {
          if(!$s->is_manual) {
              $s->status = ServiceStatus::CANCELLED;
              $s->price           = 0;
              $s->net_price       = 0;
              $s->commission      = 0;
              $s->agency_fee      = 0;
              $s->incentive       = 0;
              $s->commission_suc  = 0;
              $s->fee_monto       = 0;
              $s->save();

            if ($s->type == ServiceType::HOTEL) {
              $rooms = HotelRoom::findAll(array('service_id' => $s->id));
              foreach ($rooms as $room) {
                  $room->price = 0;
                  $room->net_price = 0;
                  $room->save();
              }
            }
          }
        }
      }
      $send_messagge = true;

    }
    else if ($cancellations) {
      $oldBook = Booking::findById($book->id);
      $oldBook->status = ServiceStatus::ANNULLED;

      $oldBook->save();

      $book->id = NULL;
      $book->debit_note = $oldBook->debit_note != '' ? $oldBook->getCloneDebitNoteNumber() : '';
      //$book->booking_code = $oldBook->booking_code != 0 ? $oldBook->getCloneBookingCodeNumber() : 0;
      // Check if all services are cancelled before cancelling the whole Book
      $allCancelled = true;
      foreach ($oldBook->getServices() as $s) {
        if ($s->status != ServiceStatus::CANCELLED && $s->status != ServiceStatus::ANNULLED) {
          $allCancelled = false;
          break;
        }
      }
      if ($allCancelled) {
        $book->status = ServiceStatus::CANCELLED;
      }
      $book->confirm_date = $oldBook->confirm_date;
      $book->cancel_date = date('Y-m-d H:i:s');
      $book->save();

      $services = Service::findAll(array('booking_id' => $oldBook->id));
      foreach ($services as $ss) {
        $oldssid = $ss->id;
        if ($ss->status == ServiceStatus::ANNULLED) {
          $ss->status = ServiceStatus::CANCELLED;
          $ss->price          = 0;
          $ss->net_price      = 0;
          $ss->commission     = 0;
          $ss->agency_fee     = 0;
          $ss->incentive      = 0;
          $ss->commission_suc = 0;
          $ss->fee_monto      = 0;
        }
        $ss->id = NULL;
        $ss->booking_id = $book->id;
        $ss->save();

        if ($ss->type == ServiceType::HOTEL) {
          $rooms = HotelRoom::findAll(array('service_id' => $oldssid));
          foreach ($rooms as $room) {
            if ($ss->status == ServiceStatus::CANCELLED) {
              $room->price = 0;
              $room->net_price = 0;
            }
            $room->id = NULL;
            $room->service_id = $ss->id;
            $room->save();
          }
        }
        else if ($ss->type == ServiceType::TRANSFER) {
          $tfr = ServiceTransfer::find(array('service_id' => $oldssid));
          $trf->id = NULL;
          $trf->save();
        }

        // Check if there are cancellation fees
        if ($cancellation_fee != 0) {
          $net_price = $cancellation_fee;
          $price = $net_price / Config::findById(1)->fee;
          $serviceDB = new Service();
          $serviceDB->build(ServiceType::OTHER, 'Gastos de cancelaci&oacute;n', '', '', '', ServiceStatus::CONFIRMED, '', '', 0,
                  '', '', '', 0, 0, '', $price, $book->currency, $book->id, 0, $net_price, 0, 0, 0, '', 1, 0, 0);
          $serviceDB->save();
        }
      }
      $send_messagge = true;
    }
    if ($send_messagge) {
      try {
        include_once(__DIR__ . '/../helpers/template_mail/mail_book.php');
        sendNotificationMail('Reserva ' . $book->getBookingCode() . ' cancelada', $msg, $p['current_user']);
      } catch(Exception $e) {
        \Log::error($e->getMessage(), 'Error al Enviar Correo ' . $id);
      }
    }
    return array('bookId' => $book->id);
  }

  public static function confirm_book($p) {
    $id = _post('id', $p);
    try {
      $book                   = Booking::findById($id);
      $currenAgency           = Agency::findById($p['current_user']['agency_id']);
      
      if ($currenAgency->booking == 'EMIT') {
        $book->user_confirm_id  = $p['current_user']['id'];
        $book->status           = ServiceStatus::CONFIRMED;
        $book->confirm_date     = date('Y-m-d H:i:s');
        $book->debit_note = $book->getNextDebitNoteNumber();

        $book->save();

        $services = $book->getServices();
        foreach ($services as $service) {
          $service->status = ServiceStatus::CONFIRMED;
          $service->save();
        }
        include_once(__DIR__ . '/../helpers/template_mail/mail_book.php');
        sendNotificationMail('Confirmacion Reserva ' . $book->getDebitNoteNumber() . ' (' . $book->getBookNumber() . ')', $msg, $p['current_user']);
      }
      return array('bookId' => $id);
    } catch(Exception $e) {
      \Log::error($e->getMessage(), 'Error al Confirmar Reserva ' . $id);
      return array('bookId' => $id);
    }
    

    // $msg = "La reserva " . $book->getDebitNoteNumber() . " fue confirmada. <br /><br />";
    // $msg .= "Nro Reserva:" . $book->getBookNumber() . "<br />";
    // $msg .= "Nro Confirmacion:" . $book->getDebitNoteNumber() . "<br />";
    // $msg .= "Agente: " . $p['current_user']['name'] . '/' . $currenAgency->name . "<br />";
    // $msg .= "Nombre pasajero: " . $book->holder . "<br />";
    //
    // $msg .= '</ul><br />Puedes ver la reserva desde:<br />';
    // $msg .= '<a href="http://boliviabooking.com/hotelesboliviabooking/zion/?controller=booking&action=show&id=' . $book->id . '">';
    // $msg .= 'http://boliviabooking.com/hotelesboliviabooking/zion/?controller=booking&action=show&id=' . $book->id . '</a>';
    // $msg .= '<br /><br /><div style="font-size:0.9em; color:#999;">Por favor no respondas a este correo porque es un correo automatizado sólo para notificaciones.</div>';

    
  }

  public static function create_debit_note($p) {

    $book     = Booking::findById($p['id']);
    $is_comision_sucursal = false;
    // TIPO DE SERVICIO EN BARRYBOLIVIA Y CONTABLEBARRY
    // HOTEL, TRANSFER, VUELOS, OTROS
    $service_type = array(1 => 2, 3 => 12, 5 => 11, 6 => 9);
    $services     = $book->getServices();
    $agency       = Agency::findById($book->agency_id);
    $manager      = AgencyManager::getAgencyManager($book->agency_id);
    $id_sucursal  = 0;
    $glosa_asiento = '';
    //VERIFICAMOS SI LA AGENCIA ESTA MAPEADA
    if ($agency->idcliente == 0) {
      return array('succcess' => false, 'message' => 'La Agencia no se encuentra mapeada');;
    }
    if (Agency::isAgencySucursal($agency)) {
    //VERIFICAMOS SI LA AGENCIA PERTENECE A CBB O LPB
      $is_comision_sucursal = true;
      // OBTENIENDO EL ID DE LA SUCURSAL
      $sucursal             = Agency::findById($manager[0]->id);
      $id_sucursal          = $sucursal->idcliente;
    }
    // ID DEL CONTABLE BARRY
    $cliente_contable  = User::findById($book->user_id);
    // USUARIO QUE CONFIRMO LA NOTA
    $user_contable     = User::findById($book->operator_id);
    if ($user_contable->id_user_contable == 0 || $cliente_contable->id_cliente_contable == 0) {
      return array('succcess' => false, 'message' => 'El usuario no se encuentra mapeado');
    }

    $debit_note = $book->getDebitNoteNumber();
    $datos      = array();
    $datos['idclientes']            = $agency->idcliente;
    $datos['moneda']                = $services[0]->currency;
    $datos['idcliente_solicitante'] = $cliente_contable->id_cliente_contable;

    $datos['idusuario']             = $user_contable->id_user_contable;
    $datos['idusuario_cliente']     = $user_contable->id_cliente_contable;
    $name       = $book->holder;
    $last_name  = '';
    if (!empty($book->name))
      $name       = $book->name;
      $last_name  = $book->last_name;
    $con = 1;
    $total_array = array();
    $total_ar    = array();

    foreach ($services as $service) {
      $provider_id = $service->id;
      $tipo_s     = isset($service_type[$service->type]) ? $service_type[$service->type] : 9;
      if (!isset($total_ar[$provider_id])) {
        $total_ar[$provider_id] = $service->net_price;
        $total_array[$provider_id . '_price']             = floatval($service->price);
        $total_array[$provider_id . '_commission']        = floatval($service->commission);
        $total_array[$provider_id . '_incentive']         = floatval($service->incentive);
        $total_array[$provider_id . '_debit_note']        = $debit_note;
        $total_array[$provider_id . '_net_price']         = floatval($service->net_price);
        $total_array[$provider_id . '_tipo_servicio']     = $tipo_s;
        $total_array[$provider_id . '_descripcion']       = BookingController::get_description_service($service);
        $total_array[$provider_id . '_provider']          = $service->provider_id;
        $total_array[$provider_id . '_commission_suc']    = $service->commission_suc;
        continue;
      }
      $total_ar[$provider_id] += $service->net_price;
      $total_array[$provider_id . '_price']             += floatval($service->price);
      $total_array[$provider_id . '_commission']        += floatval($service->commission);
      $total_array[$provider_id . '_incentive']         += floatval($service->incentive);
      $total_array[$provider_id . '_debit_note']        = $debit_note;
      $total_array[$provider_id . '_net_price']         += floatval($service->net_price);
      $total_array[$provider_id . '_tipo_servicio']     = $tipo_s;
      $total_array[$provider_id . '_provider']          = $service->provider_id;
      $total_array[$provider_id . '_commission_suc']    += $service->commission_suc;
    }
    $datos['nro_servicio']          = count($total_ar);
    foreach ($total_ar as $key => $value) {
      $proveedor  = Provider::findById($total_array[$key . '_provider']);
      if ($proveedor->id_proveedor_contable == 0) {
        return array('succcess' => false, 'message' => 'El proveedor no se encuentra mapeado');
      }

      $comision_operadora = $total_array[$key.'_price'] - $total_array[$key.'_commission'] - $total_array[$key.'_net_price'] - $total_array[$key.'_incentive'];

      $datos['proveedor'.$con]          = $proveedor->id_proveedor_contable;
      $datos['nropax'.$con]             = 1;
      $datos['s'.$con.'_nombre1']       = $name;
      $datos['s'.$con.'_apellido1']     = $last_name;
      $datos['tipo_servicio'.$con]      = $total_array[$key.'_tipo_servicio'];
      $datos['precio'.$con]             = $total_array[$key.'_net_price'];
      $datos['comision'.$con]           = $comision_operadora;
      $datos['codigo_proveedor'.$con]   = $debit_note;
      $datos['incentivo_counter'.$con]  = $total_array[$key.'_incentive'];
      $datos['incentivo'.$con]          = 0;
      $datos['comision_cliente'.$con]   = $total_array[$key.'_commission'];
      $datos['descripcion'.$con]        = $total_array[$key.'_descripcion'];
      $datos['service_fee'.$con]        = 0;

      if ($is_comision_sucursal) {
        $comision_sucursal  = $total_array[$key.'_commission_suc'];
        $comision_operadora = $total_array[$key.'_price'] - $total_array[$key.'_commission'] - $total_array[$key.'_net_price'] - $total_array[$key.'_incentive'] - $comision_sucursal;

        $datos['comision'.$con]           = round($comision_operadora, 2);
        $datos['comision_sucursal'.$con]  = round($total_array[$key.'_commission_suc'], 2);
        $datos['cb_sucursal']             = $id_sucursal;
      }
      $glosa_asiento .= $glosa_asiento != '' ? '<--> ' : '';
      $glosa_asiento .= $name . ' ' . $last_name . ' - ' . $total_array[$key.'_descripcion'];

      $con++;
    }
    $datos['glosa_asiento'] = $glosa_asiento;
    $datos['save_debit_note_bb'] = 'save';

    $totalNeto      = $total_array[$key.'_net_price'];
    $totalComission = $total_array[$key.'_commission'];
    $totalIncentive = $total_array[$key.'_incentive'];

    if ($totalNeto < 0 || $totalComission < 0 || $totalIncentive < 0 || $comision_operadora < 0) {
      return array('succcess' => false, 'message' => 'No se pudo crear la NOTA de Debito, hay montos negativos.');
    }
    $respuesta = sendToContable($datos);

    Log::warning(json_encode($respuesta), 'BOOK DEBIT NOTE ID: ' . $book->id);
    if ($respuesta->success) {
      $id_nota_debito           = $respuesta->data->idnota_debito;
      $book->debit_number_conta = $id_nota_debito;
      $book->save();
    }
    return $respuesta;
  }

  public static function get_description_service($service) {
    $name       = trim($service->name) . ' ';
    $localizer  = 'LOCALIZADOR: ' . trim($service->localizer);
    $descrip    = $name . ' ' . $localizer.' ';
    if ($service->type == ServiceType::HOTEL || $service->type == ServiceType::TRANSFER || $service->type == ServiceType::TICKET) {
      $destCode = $service->destination_code != '' ? 'DESTINO: ' . $service->destination_code.' ' : '';
      $checkin  = $service->checkin != '' ? 'ENTRADA: '.dateFormatFromDB($service->checkin).' ' : '';
      $descrip .= $destCode;
      $descrip .= $checkin;
    }
    else if ($service->type == ServiceType::FLIGHT) {
      $checkin  = $service->category != '' ? $service->category : '';
      $descrip .= $checkin;
    }
    else if ($service->type == ServiceType::OTHER) {
      $destCode = $service->destination_code != '' ? 'DESTINO: '.$service->destination_code.' ' : '';
      $descrip .= $destCode;
    }
    return $descrip;
  }

  public static function purchase_detail($p) {
    $HBService = array();
    $booking = new BookingEngine();
    if ($p['provider'] == Provider::HOTELBEDS) {
      $params = array(
        'requestType'     => _get('requestType', $p),
        'fileNumber'      => _get('fileNumber', $p),
        'incomingOffice'  => _get('incomingOffice', $p),
        'provider'        => _get('provider', $p)
      );
      $HBService = $booking->execRequest($params);
    }
    return array("HBService" => $HBService);
  }

  public static function hotel_detail($p) {
    $params = array(
        'requestType'   => BookingRequestTypes::HotelDetail,
        'hotelCode'     => _get('hotelCode', $p),
        'provider'     => _get('provider', $p));

    $booking = new BookingEngine();
    $detail = $booking->execRequest($params);
    return array('detail' => $detail);
  }

  public static function process($p) {
    $id = _post('id', $p);

    $book = Booking::findById($id);
    $book->operator_id = $p['current_user']['id'];;
    $book->save();

    return array('bookId' => $id);
  }

  public static function show_books_table() {
    if (!$_SESSION['admin_role']) { return; }

    global $bookings, $agencies, $agenci_user;
    $agencies = Agency::findAll();
    $agenci_user = User::findAll(array('agency_id' => _get('agency_id')));
    if (!isset($_GET['search_books'])) {
      $bookings = array();
      return;
    }
    $filters = array();
    $filters['agency_id'] = _get('agency_id');
    $filters['user_id'] = _get('user_id');
    $filters['status'] = _get('status');
    $filters['note_number'] = _get('note_number');
    $filters['destination_code'] = _get('destination_code');
    $filters['datesFilter'] = _get('datesFilter');
    $filters['checkin'] = _get('checkin');
    $filters['checkout'] = _get('checkout');

    $bookings = Booking::filterBooks($filters);
  }

  public static function purchase_detail_list() {
    global $rs;
    if (isset($_GET['from'])) {
      $from_ar = explode("/", _get('from'));
      $to_ar = explode("/", _get('to'));
      $from  = $from_ar[2] . $from_ar[1] . $from_ar[0];
      $to = $to_ar[2] . $to_ar[1] . $to_ar[0];
      if($_GET['provider_id'] == Provider::METHABOOK) {
        $requestType = BookingRequestTypes::PurchaseDetailList;
      }
      else if($_GET['provider_id'] == Provider::HOTELBEDS){
        $requestType = BookingRequestTypes::PurchaseList;
      }
      else if($_GET['provider_id'] == Provider::METHABOOK2) {
        $requestType = BookingRequestTypes::PurchaseDetailList;
      }
      if($_GET['provider_id'] == Provider::DOMITUR2) {
        $requestType = BookingRequestTypes::PurchaseDetailList;
      }
      else if($_GET['provider_id'] == Provider::TOURICO){
        $requestType = BookingRequestTypes::PurchaseDetailList;
      }
      if($_GET['provider_id'] == Provider::RESTEL) {
        $requestType = BookingRequestTypes::PurchaseDetailList;
      }
      $params = array('requestType' => $requestType,
                      'provider'    => _get('provider_id'),
                      'dateFrom'        => $from,
                      'dateTo'          => $to,
                      'page'        => '1',
                      'includeCancelled' => 'N',
                      'filterType' => 'C');

      $booking = new BookingEngine();
      $rs = $booking->execRequest($params);
    }

    return $rs;
  }

  public static function show_by_debit_note() {
    // global $HBServices, $MBServices, $DTServices, $details, $book, $agency, $current_user;
    $debit_note = substr(trim($_GET['debit_note']), 1);
    $book = Booking::find(array('debit_note' => $debit_note));

    if (!isset($book)) {
      return array('res' => "NO");
    }
    $book->booking_code = $book->getBookingCode();
    $services = $book->getServices();
    foreach ($services as $service) {
      if($service->type == ServiceType::HOTEL) {
        $service->rooms = $service->getRooms();
      }
    }
    $book->services = $services;
    $user = User::findById($book->user_id);
    $current_user = $user;
    $agency_id = $book->agency_id ? $book->agency_id : 1; // If no agency then use Barry
    $agency = Agency::findById($agency_id);
    $agency->logo = $agency->get_url_logo();
    // Security: Check if the user can see the booking
    //if (!$_SESSION['admin_role'] && $agency_id != $current_user->agency_id) {
      // header('Location: ?controller=booking&action=agency_books');
    //}

    $booking = new BookingEngine();
    $localizers = array();
    $HBServices = array();
    $MBServices = array();
    $MBBServices = array();
    $DTServices = array();
    $THServices = array();
    $details = array();
    foreach ($book->getServices() as $service) {
      if ($service->provider_id == Provider::HOTELBEDS && !isset($localizers[$service->localizer])) {
        $ref = explode('-', $service->localizer);
        $fileNumber = $ref[1];
        $incomingOffice = $ref[0];
        $params = array(
          'requestType'     => BookingRequestTypes::PurchaseDetail,
          'fileNumber'      => $fileNumber,
          'incomingOffice'  => $incomingOffice,
          'provider'        => $service->provider_id
        );
        $HBService = $booking->execRequest($params);
        $HBServices[] = $HBService;
        foreach ($HBService->getBookingServices() as $s) {
          // Get service details
          if ($s->getServiceType() == 'ServiceHotel') {
            $serviceInfo = $s->getServiceInfo();
            $params = array(
                'requestType'   => BookingRequestTypes::HotelDetail,
                'hotelCode'     => $serviceInfo->getCode(),
                'provider'      => $service->provider_id);
            $details[] = $booking->execRequest($params);
          }
        }

        $localizers[$service->localizer] = true;
      }
      else if ($service->provider_id == Provider::METHABOOK && !$service->is_manual) {
        $params = array(
          'requestType'     => BookingRequestTypes::PurchaseDetail,
          'localizer'       => $service->localizer,
          'provider'        => $service->provider_id
        );

        $MBService = $booking->execRequest($params);
        $MBServices[] = $MBService;

        $ss = $MBService->getBookingServices();
        $serviceInfo = $ss[0]->getServiceInfo();
        $params = array(
            'requestType'   => BookingRequestTypes::HotelDetail,
            'hotelCode'     => $serviceInfo->getCode(),
            'provider'      => $service->provider_id);
        $details[] = $booking->execRequest($params);
      }
      else if ($service->provider_id == Provider::METHABOOK2 && !$service->is_manual) {
        $params = array(
          'requestType'     => BookingRequestTypes::PurchaseDetail,
          'localizer'       => $service->localizer,
          'provider'        => $service->provider_id
        );

        $MBBService = $booking->execRequest($params);
        $MBBServices[] = $MBBService;

        $ss = $MBBService->getBookingServices();
        $serviceInfo = $ss[0]->getServiceInfo();
        $params = array(
            'requestType'   => BookingRequestTypes::HotelDetail,
            'hotelCode'     => $serviceInfo->getCode(),
            'provider'      => $service->provider_id);
        $details[$serviceInfo->getCode()] = $booking->execRequest($params);
      }
      else if($service->provider_id == Provider::DOMITUR && !isset($localizers[$service->localizer]) && !$service->is_manual) {
        $fileNumber = $service->localizer;
        $params = array(
          'requestType'     =>  BookingRequestTypes::PurchaseDetail,
          'localizer'      =>  $service->localizer,
          'provider'        => BookingProvider::DOMITUR
        );

        $DTService = $booking->execRequest($params);
        $DTServices[] = $DTService;

        $ss = $DTService->getBookingServices();
        $serviceInfo = $ss[0]->getServiceInfo();
        $map = DomiturHotelMapping::find(array('code' => $serviceInfo->getCode()));
        if($map != null) {
          $params = array(
              'requestType'   => BookingRequestTypes::HotelDetail,
              'hotelCode'     => $map->zion_code,
              'provider'      => BookingProvider::HOTELBEDS);
          $details[] = $booking->execRequest($params);
        }
        else
        {
          $params = array(
              'requestType'   => BookingRequestTypes::HotelDetail,
              'hotelCode'     => $serviceInfo->getCode(),
              'provider'      => BookingProvider::DOMITUR);
          $details[$serviceInfo->getCode()] = $booking->execRequest($params);
        }
      }
      else if($service->provider_id == Provider::TOURICO && !isset($localizers[$service->localizer]) && !$service->is_manual) {
        $params = array(
          'requestType'     => BookingRequestTypes::PurchaseDetail,
          'localizer'       => $service->localizer,
          'provider'        => $service->provider_id
        );

        $THService = $booking->execRequest($params);
        $THServices[] = $THService;

        $ss = $THService->getBookingServices();
        $serviceInfo = $ss[0]->getServiceInfo();//__logarr($serviceInfo);
        $params = array(
            'requestType'   => BookingRequestTypes::HotelDetail,
            'hotelCode'     => $serviceInfo->getCode(),
            'provider'      => $service->provider_id);
        $details[] = $booking->execRequest($params);
      }
    }
    // UserController::logOut();
    return array('HBServices' => $HBServices, 'MBServices' => $MBServices, 'MBBServices' => $MBBServices,
                 'DTServices' => $DTServices, 'THServices' => $THServices, 'details'=> $details, 'book' => $book,
                 'current_user' => $current_user, 'agency' => $agency);
  }

  public static function test_mail() {
    $msg = '<h2>Prueba de Correo</h2>';
    $usuario = 'Omar Flores';
    sendNotificationMail('Reserva Prueba cancelada', $msg, $usuario);
  }
}

?>
