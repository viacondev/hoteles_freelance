<?php
  
class RestController {
  public static function convert_from_latin1_to_utf8_recursively($dat) {
      if (is_string($dat)) {
         return utf8_encode($dat);
      } elseif (is_array($dat)) {
         $ret = [];
         foreach ($dat as $i => $d) $ret[ $i ] = self::convert_from_latin1_to_utf8_recursively($d);

         return $ret;
      } elseif (is_object($dat)) {
         foreach ($dat as $i => $d) $dat->$i = self::convert_from_latin1_to_utf8_recursively($d);

         return $dat;
      } else {
         return $dat;
      }
  }

  public static function loadAction($params) {
    if (!isset($params['controller']) && !isset($params['action'])) {
      return NULL;
    }
    $controller_name = $params['controller'];
    $action_name = $params['action'];
    // Load PHP helpers
    $helper_dir = 'helpers/' . $controller_name . '_helper.php';
    if (file_exists($helper_dir)) {
      include($helper_dir);
    }

    // Load controller
    $controller_class_name = str_replace('_', ' ', $controller_name);
    $controller_class_name = ucwords($controller_class_name);
    $controller_class_name = str_replace(' ', '', $controller_class_name);
    $controller_class_name = $controller_class_name . 'Controller';
    $controller_dir = 'controller/' . $controller_class_name . '.php';
    if (file_exists($controller_dir)) {
      include_once($controller_dir);
      $controller = new ReflectionClass($controller_class_name);

      // Security for admin-only actions
      // if (!$_SESSION['admin_role'] && $controller->hasProperty('admin_only')) {
      //   $prop = $controller->getProperty('admin_only');
      //   if (in_array($action_name, $prop->getValue(new $prop->class))) {
      //     header('Location: ./');
      //   }
      // }

      $action = $controller->getMethod($action_name);
      $res    = $action->invoke($controller, $params);

      $bookSerialize = new BookingSerializable();
      if (is_array($res)) { // Always should be true
        $res_buffer = array();
        foreach ($res as $key => $value) {
          if ($bookSerialize->objIsBookingSerializable($value)) {
            $value = $value->serialize();
          }
          else if (is_array($value)) {
            $value = $bookSerialize->serialize($value);
          }

          $res_buffer[$key] = $value;
        }
        $res = $res_buffer;
      }

      if ($params['controller'] != 'accommodation' && $params['action'] != 'search_ajax') {
        return json_encode($res);
      }
      
      $res = RestController::convert_from_latin1_to_utf8_recursively($res);

      $gzip_txt = gzdeflate(json_encode($res)); 

      $user     = $_SESSION['current_user'];
      $name     = $user->username;
      $userId   = $user->id;
      $agencyId = $user->agency_id;
      $date     = date('Y-m-d H:i:s'); 
      $token    = $date . '_' . $agencyId . '_' . $name . '_' . $userId;

      $back = new \BackHotelsAvail();
      $back->update_date    = date('Y-m-d H:i:s');
      $back->result         = $gzip_txt;
      
      $back->token_avail     = $token;
      $field = $back->find(array('token_avail' => $token));
      if ($field) {
        $back->id = $field->id;
      }
      $back->save();

      /* INICIO DE LA PAGINACION */
      $result         = new BackHotelsAvail();
      $result         = $result->getResults($token);
      $result->rs->totalPages = 1;
      $orderedHotels  = $result->rs->hotelsAvail;
      if (isset($params['regimen']) && $params['regimen'] != '') {
        $arrResultReg = array();
        $arrResultReg = RestController::searchByBoardCode($orderedHotels, $params['regimen']);
        $orderedHotels = array();
        $orderedHotels = array_merge($orderedHotels, $arrResultReg);
        $result->rs->hotelsAvail = $orderedHotels;
      }
      if (isset($params['hotelName']) && $params['hotelName'] != '' || isset($params['star']) && $params['star'] != '') {
        $arrResult = array();
        $hotelName = isset($params['hotelName']) ? $params['hotelName'] : '';
        $star      = isset($params['star']) ? $params['star'] : '';
        $arrResult = RestController::searchByName($orderedHotels, $hotelName, $star);
        $orderedHotels = array();
        $orderedHotels = array_merge($orderedHotels, $arrResult);
        $result->rs->hotelsAvail = $orderedHotels;
      }

      
      $cantidadHotels = count($orderedHotels);

      $limit        = 15;
      $page         = 1;
      $inicio       = ($page - 1) * $limit;
      $fin          = ($page * $limit) - 1;
      $totalPages   = ceil(count($orderedHotels) / $limit);

      usort($orderedHotels, array('RestController','orderPriceAsc'));

      if (!isset($params['last-version'])) {
        $hotelsPaginated = array();
        for ($i = $inicio; $i <= $fin; $i++) { 
          if (isset($orderedHotels[$i])) {
            $hotelsPaginated[] = $orderedHotels[$i];
          }
        }

        $result->rs->hotelsAvail = $hotelsPaginated;
        $result->rs->totalPages  = $totalPages;
      }
      
      $result->rs->tokenAvail  = $token;

      // FIN DE LA PAGINACION
      // $res['rs']['tokenAvail']  = $token;
      return json_encode($result);
    }
  }

  public static function orderPriceAsc($a, $b) {
    $priceA = $a->groupedRooms;
    $priceB = $b->groupedRooms;
    $roomsA  = $priceA[0];
    $roomsB  = $priceB[0];
    $a = 0;
    $b = 0;
    foreach ($roomsA as $room) {
      $a += floatval($room->priceWithFee);
    }
    foreach ($roomsB as $room) {
      $b += floatval($room->priceWithFee);
    }
    if ($a == $b) return 0;
    return ($a > $b) ? 1 : -1;
  }

  public static function searchByName($orderedHotels, $filterhotelName, $filterCategory) {
    //search hotel by name
    $hotelName  = '';
    $star       = ($filterCategory == '') ? '' : explode(',', implode(',', $filterCategory));
    $filterName = $filterhotelName;
    $hotel      = str_replace(" ", ',', $filterName);
    $hotel      = explode(',', $hotel);
    foreach ($hotel as $value) {
      if (strlen($value) > 3) {
        $hotelName .= trim($value) . "|";  
      }
    }
    $filterName = substr($hotelName, 0, -1);
    $arrResult  = array();
    foreach ($orderedHotels as $hotel) {
      $isFilter     = true;
      $hotelName    = strtolower($hotel->name);
      $categoryCode = strval($hotel->categoryCode);
      if ($hotelName != '' && !preg_match("/$filterName/i",$hotelName)) {
        $isFilter = false; 
      }
      if ($star != '' && !in_array("'" . $categoryCode . "'", $star)) {
        $isFilter = false;
      }
      if ($isFilter == true) {
        $arrResult[] = $hotel;
      }
    }

    return $arrResult;
  }

  public static function searchByBoardCode($orderedHotels, $regimen) {
    $regimen    = explode(',', implode(',', $regimen));
    $contador   = 0;
    $arrBoards  = array(
      'AI'  => 'TI', //Todo Incluido Codigo MBB=>GENERAL
      'BB'  => 'AD',  //Alojamiento y desayuno
      'OR'  => 'SA', //Solo Alojamiento
      'RO'  => 'SA',
      'NO'  => 'SA',
      'PC'  => 'FB', //Pension Completa
      'FB'  => 'FB',
      'MP'  => 'HB', //Media Pension
      'HB'  => 'HB',
      'RO' => 'RO','SA' => 'RO','OB' => 'RO','FI' => 'RO',/* Solo Alojamiento */
      'BB' => 'BB','AD' => 'BB',/* Alojamiento y Desayuno */
      'HB' => 'HB','MP' => 'HB',/* Media Pension */
      'FB' => 'FB','PC' => 'FB',/*Pension Completa*/
      'AI' => 'AI','TI' => 'AI'/*Todo Incluido*/
    );
    foreach ($orderedHotels as $hotel) {
      $h                = $hotel->groupedRooms;
      $cantidad         = count($h);
      $newGroupedRooms  = array();
      for ($i = 0; $i < $cantidad; $i++) {
        $reg = $h[$i][0]->boardCode;
        $reg = isset($arrBoards[$reg]) ? $arrBoards[$reg] : substr($reg, 0, 2); 
        if (in_array($reg, $regimen)) {
          $newGroupedRooms[] = $h[$i];
        }
      }
      $count = count($newGroupedRooms);
      if ($count > 0) {
        $hotel->groupedRooms = $newGroupedRooms;
      }
      else {
        unset($orderedHotels[$contador]);
      }
      $contador++;
    }
    return $orderedHotels;
  }

}

?>
