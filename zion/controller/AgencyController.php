<?php
include_once('../booking/BookingSerializable.php');
include_once('model/Agency.php');
include_once('model/Ciudad.php');
include_once('model/AgencyManager.php');

class AgencyController extends ApplicationController {

  public $admin_only = array('home', 'add', 'create', 'edit', 'update');

  public static function home() {
    global $agencies;
    $status = array('status' => '1');
    $manager = AgencyManager::find(array('agency_id' => $_SESSION['current_agency']->id));
    if(isset($_GET['status'])) {
      $status['status'] = $_GET['status'];
    }
    if(isset($_GET['city']) && count($manager) == 0) {
      $status['ciudad_id'] = $_GET['city'];
    }
    if (count($manager) > 0) {
      $ciudad_id = Agency::findById($_SESSION['current_agency']->id);
      $status['ciudad_id'] = $ciudad_id->ciudad_id; 
    }
    $agencies = Agency::findAll($status);
  }

  public static function add() {
  }

  public static function create() {
    $agency = new Agency();
    $agency->build($_POST['name'], $_POST['address'], $_POST['phone'], $_POST['mail'], $_POST['booking'], 1, $_POST['commission'], $_POST['city']);
    $agency->save();

    $agency->updateLogo($_FILES['logo']['tmp_name']);

    header('Location: ./?controller=agency&action=show&id=' . $agency->id);
  }

  public static function show() {
    global $agency, $users;

    $agency = Agency::findById($_GET['id']);
    $users = User::findAll(array('agency_id' => $_GET['id']));
  }

  public static function edit() {
    global $agency;

    $agency = Agency::findById($_GET['id']);
  }

  public static function update() {
    $agency = new Agency();
    $agency->build($_POST['name'], $_POST['address'], $_POST['phone'], $_POST['mail'], $_POST['booking'], $_POST['status'], $_POST['commission'], $_POST['city']);
    $agency->id = $_POST['id'];
    $agency->save();

    $agency->updateLogo($_FILES["logo"]['tmp_name']);

    header('Location: ./?controller=agency&action=show&id=' . $agency->id);
  }

  public static function users() {
    global $current_user, $agency, $users;

    $agency = Agency::findById($current_user->agency_id);
    $users = User::findAll(array('agency_id' => $agency->id));
  }

  public static function config() {
    global $current_user, $agency;

    $agency = Agency::findById($current_user->agency_id);
  }

  public static function update_config() {
    global $current_user;

    $agency = Agency::findById($current_user->agency_id);
    $agency->extra_fee = $_POST['extra_fee'];
    $agency->extra_fee_type = $_POST['extra_fee_type'];
    $agency->save();

    header('Location: ./?controller=agency&action=config');
  }

  public static function agency_mapping() {
    global $agency, $agencynomapping;
    $agency = Agency::findAll(array('status' => 1));
    $agencynomapping = Agency::findByQuery("SELECT a.id, a.name FROM Agency a WHERE NOT EXISTS (SELECT id FROM FeeGeneral f WHERE a.id = f.agencia)");
  }

}

?>
