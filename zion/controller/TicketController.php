<?php

include_once('../booking/BookingEngine.php');
include_once('model/Fee.php');

include_once('database/DBFreelance.php');
include_once('model/ModelFreelance.php');
include_once('model/Air_Config.php');
include_once('model/MotorConexion.php');

class TicketController extends ApplicationController {
  
  public static function home() {
  }

  public static function search() {
    global $rs, $childAges, $fee_agency;
      $p = $_GET;
      $p['origen']    = 'bb';
      $p['agency_id'] = $p['current_user']['agency_id'];
      $p['user_id']   = $p['current_user']['id'];
      // __log(json_encode($p));
      $motor  =  new MotorConexion();
      $motor->build('availTickets', $p);
      $res        = $motor->consultar();
      $rs         = $res;
      $childAges  = isset($res->childAges)?$res->childAges:[];
      $childAges  = array();
      for ($i = 1; $i <= intval($_GET['childCount']) ; $i++) { 
        $childAges[] = $_GET['childAge' . $i];
      }
      return $res;
      exit;
    /* end production */
    // Query params
    // $dateFrom_arr = explode('/', $_GET['dateFrom']);
    // $dateFrom = $dateFrom_arr[2] . $dateFrom_arr[1] . $dateFrom_arr[0];
    // $dateTo_arr = explode('/', $_GET['dateTo']);
    // $dateTo = $dateTo_arr[2] . $dateTo_arr[1] . $dateTo_arr[0];
    // $orderPrice = isset($_GET['orderPrice']) ? $_GET['orderPrice'] : '';

    // $params = array(
    //     'requestType'   => BookingRequestTypes::SearchTicketAvail,
    //     'destCode'      => $_GET['destination_code'],
    //     'destType'      => 'SIMPLE',
    //     'dateFrom'      => $dateFrom,
    //     'dateTo'        => $dateTo,
    //     'adultCount'    => $_GET['adultCount'],
    //     'childCount'    => $_GET['childCount'],
    //     'orderPrice'    => $orderPrice,
    //     'page'          => $_GET['page']);

    // $childAges = array();
    // for ($i = 1; $i <= intval($_GET['childCount']) ; $i++) { 
    //   $params['childAge_' . $i] = $_GET['childAge' . $i];
    //   $childAges[] = $_GET['childAge' . $i];
    // }

    // Fee::setDestinationFee($_GET['destination_code']);

    // $booking = new BookingEngine();
    // $rs = $booking->execRequest($params);

    // $_SESSION['destination_code'] = $_GET['destination_code'];
    // $_SESSION['destination'] = $_GET['destination'];
    // $_SESSION['checkin'] = $_GET['dateFrom'];
    // $_SESSION['checkout'] = $_GET['dateTo'];

    // $agencyMp   = AgencyMapping::find(array('agencybarry' => $_SESSION['current_agency']->id));
    // $hotel_fee  = air_config::find(array('idcliente' => $agencyMp->agencybb));
    // $valores_0  = true;

    // $hotelFeePorcentaje = $hotel_fee->hotel_fee_porcentaje;
    // $hotelFeeMonto = $hotel_fee->hotel_fee_monto;
    // if ($hotel_fee->hotel_fee_porcentaje == 0) {
    //   $valores_0 = false;
    // }
    // if ($hotel_fee->hotel_fee_monto == 0) {
    //   $valores_0 = false;
    // }
    // if($valores_0 == false) {
    //   $hotelFeePorcentaje = 12;
    // }

    // $fee_agency = (object)array('fee_prc' => $hotelFeePorcentaje, 'fee_monto' => $hotelFeeMonto);
  }

}

?>