<?php

class HotelbedsController {

  public static function service_add($params, $purchaseToken) {
    // Tickets need to be Valued before adding service
    if ($params['requestType'] == BookingRequestTypes::TicketValuation) {
      $params = execTicketValuation($params);
    }

    // Transfer may have two services (IN and OUT) to be added
    if ($params['requestType'] == BookingRequestTypes::AddTransferService) {
      $moreParams = $params['moreParams'];
      if ($params['transferType'] == 'IN') {
        $params = (array) $params['data_in'];
        $params['travel_number'] = _post('travel_number_in', $moreParams);
      }
      else if ($params['transferType'] == 'OUT') {
        $params = (array) $params['data_out'];
        $params['travel_number'] = _post('travel_number_out', $moreParams);
      }
      // Add IN Service first and the continue with the OUT
      else if ($params['transferType'] == 'IN_OUT') {
        $params_in = (array) $params['data_in'];
        $params_in['travel_number'] = _post('travel_number_in', $moreParams);
        $params_in['purchaseToken'] = $purchaseToken;
        $params = (array) $params['data_out'];
        $params['travel_number'] = _post('travel_number_out', $moreParams);
        $booking = new BookingEngine();
        $rs = $booking->execRequest($params_in);
        $purchaseToken = $rs->getPurchaseToken();

        // Add an extra item because IN_OUT results on two different services
        if (!isset($_SESSION['shopping_items'])) {
          $_SESSION['shopping_items'] = 0;
        }
        $_SESSION['shopping_items'] = intval($_SESSION['shopping_items']) + 1;
      }
    }

    $params['purchaseToken'] = $purchaseToken;

    if (!isset($_SESSION['shopping_items'])) {
      $_SESSION['shopping_items'] = 0;
    }
    $_SESSION['shopping_items'] = intval($_SESSION['shopping_items']) + 1;


    $booking = new BookingEngine();
    $rs = $booking->execRequest($params);

    return $rs;
  }

  public static function service_remove($purchaseToken, $SPUI) {
    $params = array(
      'requestType'   => BookingRequestTypes::RemoveService,
      'purchaseToken' => $purchaseToken,
      'SPUI'          => $SPUI);

    $booking = new BookingEngine();
    $rs = $booking->execRequest($params);

    return $rs;
  }
}
?>
