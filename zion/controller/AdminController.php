<?php

include_once('model/Config.php');
include_once('model/Fee.php');
include_once('model/Hotel.php');
class AdminController extends ApplicationController {

  public $admin_only = array('edit', 'update_fee', 'update_mail', 'users', 'reports');

  public static function edit() {
    global $config, $fees;

    $config = Config::findById(1);
    $fees = Fee::findAll();
  }

  public static function update_fee() {
    $config = Config::findById(1);
    $config->fee = $_POST['fee'];
    $config->save();

    header('Location: ./?controller=admin&action=edit');
  }

  public static function update_mail() {
    $config = Config::findById(1);
    $config->mail = $_POST['mail'];
    $config->mail2 = $_POST['mail2'];
    $config->save();

    header('Location: ./?controller=admin&action=edit');
  }

  public static function users() {
  }

  public static function reports() {
  }

  public static function showDestinations() {
    global $country;
    $query = "SELECT DISTINCT destination_code, destination_name, country_code, country_name FROM Hotel ORDER BY country_code";
    $country = Hotel::findByQuery($query);
  }
}

?>
