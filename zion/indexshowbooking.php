<?php
ob_start();
include_once('includes.php');
include_once('model/Agency.php');
session_start();
date_default_timezone_set('America/La_Paz');

$controller_name = isset($_GET['controller']) ? $_GET['controller'] : 'accommodation';
$action_name = isset($_GET['action']) ? $_GET['action'] : 'home';
?>
<!DOCTYPE html>
<html>
<head>
  <title>BoliviaBooking</title>
  <meta charset="utf-8" />
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="shortcut icon" href="assets/images/favicon1.png">
  <!-- css -->
  <link href="../lib/jquery-ui/css/custom-theme/jquery-ui-1.10.0.custom.css" media="screen" rel="stylesheet">
  <link href="../lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
  <link href="assets/css/layout.css" rel="stylesheet" media="screen">
  <link href="assets/css/print.css" rel="stylesheet" media="print">
  <link href="assets/css/stylesbb.css?201711030629" rel="stylesheet" media="screen">
  <?php ApplicationController::loadActionCSS($controller_name, $action_name); ?>

  <!-- js -->
  <script src="../lib/jquery/jquery.min.js"></script>
  <script src="../lib/jquery-ui/assets/js/jquery-ui-1.10.0.custom.min.js"></script>
  <script src="../lib/jquery/jquery.printElement.min.js"></script>
  <script src="assets/javascript/application.js?201708140934"></script>

  <!-- Overwrite jqueryui widgets to avoid collision with Bootstrap -->
  <script type="text/javascript">
    $.widget.bridge('uibutton', $.ui.button);
    $.widget.bridge('uitooltip', $.ui.tooltip);
  </script>
  <script src="../lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="../lib/bootstrap/js/jquery.bootpag.min.js"></script> <!-- Pagination -->

  <?php ApplicationController::loadActionJS($controller_name, $action_name); ?>
  <script src="assets/javascript/destinations_list.js"></script>

</head>
<body>
  <div id="content" >
    <?php ApplicationController::loadAction($controller_name, $action_name); ?>
  </div>
</body>
</html>
