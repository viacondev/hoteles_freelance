<?php
session_start();
include_once('includes.php');

$login_err_msg = '';
$login_show_style = '';

if (isset($_POST['login'])) {
  if (!UserController::startSession($_POST['username'], $_POST['password'])) {
    // If the login was from the home, the go back there
    if (isset($_POST['home'])) {
      header('location: ../?login_error=true');
    }

    $login_error = true;
    $login_err_msg = 'Usuario/contrase&ntilde;a incorrectos. Vuelva a intentar.';
    $login_show_style = 'shake';
  }
}

?>
<!DOCTYPE html>
<html>
<head>
  <title><?php echo TITLE; ?></title>
  <meta charset="utf-8" />
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- css -->
  <link href="../lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
  <style type="text/css">
    #login { border: 1px solid #CCC; border-radius: 5px; padding: 10px 20px 30px 20px; margin-top: 150px; }
  </style>
</head>

<body>
  <!-- Login box -->   
  <div id="intro">
    <div class="intro-text">
      <div class="container">
        <div class="row">
          <div id="login" class="col-md-offset-4 col-md-4 wow <?php echo $login_show_style ?>">
            <div id="login-header">
              <h1 class="text-center">
                <img src="../images/logo.png" />
                <span class="booking">Barry</span><span class="bolivia">Bolivia</span>
              </h1>
              <span class="text-danger"><?php echo $login_err_msg; ?></span>
            </div>
            <div id="login-body">
              <form class="form" method="post" action="login.php">
                <div class="form-group">
                  <input type="text" name="username" class="form-control input-lg" placeholder="Usuario" tabindex="1">
                </div>
                <div class="form-group">
                  <input type="password" name="password" class="form-control input-lg" placeholder="Contrase&ntilde;a" tabindex="1">
                </div>
                <div class="form-group">
                  <input type="hidden" name="return" value="<?php echo isset($_GET['return']) ? $_GET['return'] : './'; ?>">
                  <button class="btn btn-primary btn-lg btn-block" name="login" tabindex="1">Ingresar</button>
                </div>
              </form>
            </div>
            <span class="text-info">Tu sesion ha expirado. Por favor vuelve a ingresar.</span>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- js -->
  <script src="../lib/jquery/jquery.min.js"></script>
  <script src="../lib/bootstrap/js/bootstrap.min.js"></script>
</html>