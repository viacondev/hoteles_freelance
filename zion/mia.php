<?php
  include_once('includes.php');
  session_start();
  UserController::verifySession();
  $controller_name = isset($_GET['controller']) ? $_GET['controller'] : 'accommodation';
  $action_name = isset($_GET['action']) ? $_GET['action'] : 'home';
?>
<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

    <!-- Basic -->
    <title>YoCounter</title>

    <!-- Define Charset -->
    <meta charset="utf-8">

    <!-- Favicon -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="shortcut icon" href="assets/images/favicon2.png">
    <!-- css -->
    <link href="../lib/jquery-ui/css/custom-theme/jquery-ui-1.10.0.custom.css" media="screen" rel="stylesheet" />
    <link href="../lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
    <link href="assets/css/layout.css?202007100934" rel="stylesheet" media="screen" />
    <link href="assets/css/print.css" rel="stylesheet" media="print" />
    <link href="assets/css/stylesbb.css?202008113408" rel="stylesheet" media="screen" />
    <!-- js -->
    <script src="../lib/jquery/jquery.min.js"></script>
    <script src="../lib/jquery-ui/assets/js/jquery-ui-1.10.0.custom.min.js"></script>
    <script src="../lib/jquery/jquery.printElement.min.js"></script>
    <script src="assets/javascript/application.js?202007111222"></script>

  <!-- Overwrite jqueryui widgets to avoid collision with Bootstrap -->
  <script type="text/javascript">
    $.widget.bridge('uibutton', $.ui.button);
    $.widget.bridge('uitooltip', $.ui.tooltip);
  </script>

  <?php  if ($action_name == 'search_ajax') { ?>
          <link href="assets/css/accommodation.css?202007070455" rel="stylesheet" media="screen">
          <script type="text/javascript" src="assets/javascript/accommodation_search.js?202007151156">
          </script>
          <script type="text/javascript" src="assets/javascript/accommodation.js?201901171133">
          </script>
          <script type="text/javascript" src="views/Pqt_Integracion/integration_functions.js?201901110455">
          </script>

  <?php  } ?>

  <script src="../lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="../lib/bootstrap/js/jquery.bootpag.min.js"></script> <!-- Pagination -->
</head>
<body>

    <div class="hidden-header" style="height: 68px;"></div>
        <header class="clearfix">
            <!-- Start  Logo & Naviagtion  -->
            <?php $url = "http://" . $_SERVER["HTTP_HOST"] . "/"; ?>
            <?php $url_abs = $_SERVER["PHP_SELF"]; ?>
            <div class="navbar navbar-default navbar-top navbar-on-top" />
                <div class="container">
                    <div class="navbar-header">
                        <!-- Stat Toggle Nav Link For Mobiles -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                          <i class="fa fa-bars"></i>
                        </button>
                        <!-- End Toggle Nav Link For Mobiles -->
                        <a class="navbar-brand" href="<?php echo $url; ?>freelance/public/aereo">
                            <img src="<?php echo $url_abs; ?>/../assets/images/yocounter-logo.png" style="margin-right: 20px;height: 60px !important;" class="img-logo-full hidden" />
                            <img src="<?php echo $url_abs; ?>/../assets/images/yocounter-logo-white.png" style="margin-right: 20px;height: 60px !important;" class="img-logo-full-white" />
                        </a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <!-- Start Navigation List -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <a href="<?php echo $url_abs; ?>/../../../freelance/public/aereo">Inicio</a>
                            </li>
                            <li class="drop">
                                <a href="#" data-toggle="dropdown">Promociones</a>
                                <div class="arrow-up"></div>
                                <ul class="dropdown">
                                    <li>
                                        <a href="<?php echo $url_abs; ?>/../../../freelance/public/paquetes">Paquetes</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo $url_abs; ?>/../../../freelance/public/traslados_tours">Traslados y Tours</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="<?php echo $url_abs; ?>/../../../freelance/public/circulares" >Comunicados</a>
                            </li>
                            <li class="drop">
                                <a href="#" >Administraci&oacute;n</a>
                                <ul class="dropdown">
                                    <li>
                                        <a class="">Mis Reservas</a>
                                        <div class="arrow-left"></div>
                                        <ul class="sup-dropdown">
                                            <li>
                                                <a href="<?php echo $url_abs; ?>/../../../freelance/public/mis_reservas">Aereas</a>
                                            </li>
                                            <li>
                                                <a href="./?controller=booking&action=agency_books">Hoteles</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo $url_abs; ?>/../../../freelance/public/rentcar_buscar">Autos</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo $url_abs; ?>/../../../freelance/public/listar_cotizaciones">Cotizaciones</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">Reportes</a>
                                        <div class="arrow-left"></div>
                                        <ul class="sup-dropdown">
                                            <li>
                                                <a href="<?php echo $url_abs; ?>/../../../freelance/public/depositos_saldos">Dep&oacute;sitos y Saldos</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo $url_abs; ?>/../../../freelance/public/estado_cuentas">Estados de Cuentas</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo $url_abs; ?>/../../../freelance/public/reporte_general">Reporte General</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">Comisiones</a>
                                        <div class="arrow-left"></div>
                                        <ul class="sup-dropdown">
                                            <li>
                                                <a href="<?php echo $url_abs; ?>/../../../freelance/public/comisiones">Comisiones Percibidas</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo $url_abs; ?>/../../../freelance/public/lista_comisiones">Tabla de comisiones</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">Mis Pasajeros</a>
                                        <div class="arrow-left"></div>
                                        <ul class="sup-dropdown">
                                            <li>
                                                <a href="<?php echo $url_abs; ?>/../../../freelance/public/mis_pasajeros">Buscar Pasajero</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo $url_abs; ?>/../../../freelance/public/nuevo_pasajero">Nuevo Pasajero</a>
                                            </li>
                                        </ul>
                                    </li>
                              <?php if(isset($_SESSION['rol']) && $_SESSION['rol'] == 0) { ?>
                              <li class="drop"><a href="#">Clientes</a>
                                <div class="arrow-left"></div>
                                <ul class="sup-dropdown">
                                    <li><a href="<?php echo $url_abs; ?>/../../../freelance/public/mostrarClientes">Buscar Clientes</a></li>
                                    <li><a href="<?php echo $url_abs; ?>/../../../freelance/public/clientes">Nuevo Cliente</a></li>
                                </ul>
                              </li>
                              <?php } ?>
                              <li>
                                  <a href="<?php echo $url_abs; ?>/../../../freelance/public/contacto">Contacto</a>
                              </li>
                            </ul>
                          </li>
                        </ul>
                        <!-- End Navigation List -->
                        <ul class="nav navbar-nav navbar-right">
                            <li class="drop">
                                <a href="#" data-toggle="dropdown"><i class="fa fa-user"></i><?php echo UserController::getCurrentUser(); ?></a>
                                <ul class="dropdown">
                                  <li><a href="<?php echo $url_abs; ?>/../../../freelance/public/mi_cuenta">Mi Cuenta</a></li>
                                  <li><a href="<?php echo $url_abs; ?>/../../../freelance/public/logout">Cerrar Sessi&oacute;n</a></li>
                                </ul>
                              </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>



  <div id="content" >
    <!-- Loader -->
    <style type="text/css">
      .loader { margin: 300px auto 150px auto; border: 16px solid #ccc; border-top: 16px solid #3498db; border-radius: 50%; width: 120px; height: 120px; animation: spin 2s linear infinite; }
      @keyframes spin { 0% { transform: rotate(0deg); } 100% { transform: rotate(360deg); } }

    </style>

    <div class="loader"></div>
  </div>
    <div>
        <footer>
            <div class="container">

                <div class="row footer-widgets">

                    <!-- Start Subscribe & Social Links Widget -->
                    <div class="col-md-3">
                        <div class="footer-widget social-widget">
                           
                        </div>
                    </div><!-- .col-md-3 -->
                    <!-- End Subscribe & Social Links Widget -->


                    <!-- Start Twitter Widget -->
                    <div class="col-md-3">
                        <div class="footer-widget twitter-widget">
                            <h4>Vuelos</h4>
                            <ul>
                                <li>
                                    <a href="<?php echo $url_abs; ?>/../../../freelance/public/aereo">Reserva ahora</a>
                                </li>
                                <li>
                                    <a href="<?php echo $url_abs; ?>/../../../freelance/public/mis_reservas">Ver mis reservas</a>
                                </li>
                            </ul>
                        </div>
                        <div class="footer-widget twitter-widget">
                            <h4>Mi cuenta</h4>
                            <ul>
                                <li>
                                    <a href="<?php echo $url_abs; ?>/../../../freelance/public/mis_pasajeros">Pasajeros</a>
                                </li>
                                <li>
                                    <a href="<?php echo $url_abs; ?>/../../../freelance/public/mi_cuenta">Informaci&oacute;n de cuenta</a>
                                </li>
                            </ul>
                        </div>
                    </div><!-- .col-md-3 -->
                    <!-- End Twitter Widget -->


                    <!-- Start Flickr Widget -->
                    <div class="col-md-3">

                    </div><!-- .col-md-3 -->
                    <!-- End Flickr Widget -->


                    <!-- Start Contact Widget -->
                    <div class="col-md-3">
                        <div class="footer-widget contact-widget">
                            <h4 style="text-align: center;">
                                <img src="<?php echo $url_abs; ?>/../assets/images/yocounter-logo-white.png" style="height: 60px !important;" />
                            </h4>
                            <p>No dudes en contactarnos a trav&eacute;s de los siguientes medios:</p>
                            <ul>
                                <li><span>Tel&eacute;fono:</span> +591 3 33 3332</li>
                                <li><span>Email:</span> info@yocounter.com</li>
                                <li><span>Website:</span> www.yocounter.com</li>
                            </ul>
                        </div>
                    </div><!-- .col-md-3 -->
                    <!-- End Contact Widget -->


                </div><!-- .row -->

                <!-- Start Copyright -->
                <div class="copyright-section">
                    <div class="row">
                        <div class="col-md-6">
                            <p>&copy; 2020 Viajes - Asesor Independiente -  Todos los Derechos Reservados</p>
                        </div><!-- .col-md-6 -->
                        <div class="col-md-6">
                        </div><!-- .col-md-6 -->
                    </div><!-- .row -->
                </div>
                <!-- End Copyright -->

            </div>
        </footer>
    </div>


  <script type="text/javascript">
    $(function() {
      $.ajax({
        url: 'mia-restContent.php' + location.search,
        success: function(res) {
          $('#content').html(res);
          if (typeof verificar_parametros === 'function') {
            verificar_parametros(data)
          }
          if (typeof update_count_cotizacion === 'function') {
            update_count_cotizacion()
          }
          initCalendars();
          if (typeof restoreAccommodationSearchValues === 'function') {
            restoreAccommodationSearchValues();
          }
          if (typeof loadZonesForFilter === 'function') {
            loadZonesForFilter();
          }
          if (typeof initComponentsCotizacion === 'function') {
            initComponentsCotizacion();  
          }
        },
        error: function(err) {
          alert('Lo sentimos, la pagina no pudo ser cargada correctamente, favor intente nuevamente o ponganse en contacto con nosotros.');
        }
      });
    });

    // Update shopping cart status
    $(function() {

      $('#shopping_cart_status').html('<?php echo getShoppingCartStatus() ?>');
      initLoadingMsg();
      <?php
      if (ShoppingCart::getItemsCount() == 0) {
        echo '$("#show-chart-link").hide();';
      }
      ?>
      
    });

    // header stick
    stickHeaderOnScroll = function() {
      var scroll = $(window).scrollTop();
      if (scroll >= 80) {
        $("#header").addClass('smaller');
      } else {
        $("#header").removeClass("smaller");
      }
    }

    $(window).scroll(stickHeaderOnScroll);
  </script>

  <!-- Chat -->
  <script type='text/javascript'>
    // (function(){ var widget_id = 'YmWkg3t3Th';
    // var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();
  </script>

  <?php include('views/tools/_mailing.php'); ?>
  <div id="spinner" class="hidden-print">Cargando...</div>

  <script src="assets/javascript/destinations_list.js"></script>

</body>
</html>
