<?php

function getAddServiceTransferDataJSON($transferType, $transferIn, $transferOut, $childAges, $fees) {
  $data = array();

  if ($transferType == 'IN' || $transferType == 'IN_OUT') {
    $data['data_in'] = TransferToArray($transferIn, $childAges);
    $data['provider'] = $transferIn->getProvider();
  }
  if ($transferType == 'OUT' || $transferType == 'IN_OUT') {
    $data['data_out'] = TransferToArray($transferOut, $childAges);
    $data['provider'] = $transferOut->getProvider();
  }
  $data['requestType']  = BookingRequestTypes::AddTransferService;
  $data['transferType'] = $transferType;
  $data['fees']         = $fees;
  return json_encode($data);
}

function TransferToArray($transfer, $childAges) {
  $data = array(
    'requestType' => BookingRequestTypes::AddTransferService,
    'availToken' => $transfer->getAvailToken(),
    'transferType' => $transfer->getTransferType(),
    'contractName' => $transfer->getContractName(),
    'contractIncomingOfficeCode' => $transfer->getContractIncomingOffice(),
    'dateFrom' => $transfer->getDateFrom(),
    'timeFrom' => $transfer->getTimeFrom(),
    'transferCode' => $transfer->getCode(),
    'transferTypeCode' => $transfer->getTransferInfoTypeCode(),
    'vehicleTypeCode' => $transfer->getTransferInfoVehicleTypeCode(),
    'adultCount' => $transfer->getAdultCount(),
    'childCount' => $transfer->getChildCount(),
    'pickupType' => $transfer->getPickupLocationType(),
    'pickupCode' => $transfer->getPickupLocationCode(),
    'destinationType' => $transfer->getDestLocationType(),
    'destinationCode' => $transfer->getDestLocationCode(),
    'departDate' => $transfer->getTravelInfoDepartDate(),
    'departTime' => $transfer->getTravelInfoDepartTime(),
    'departType' => $transfer->getTravelInfoDepartType(),
    'departCode' => $transfer->getTravelInfoDepartCode(),
    'arrivalDate' => $transfer->getTravelInfoArrivalDate(),
    'arrivalTime' => $transfer->getTravelInfoArrivalTime(),
    'arrivalType' => $transfer->getTravelInfoArrivalType(),
    'arrivalCode' => $transfer->getTravelInfoArrivalCode(),
    'childAges'   => $childAges
  );

  return $data;
}