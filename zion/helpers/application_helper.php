<?php

/**
 * Return a value from an array ($_GET array if no array is passed) or empty if it does not exists
 */
function _get($val, $arr = NULL) {
  $arr = isset($arr) ? $arr : $_GET;
  return isset($arr[$val]) ? $arr[$val] : '';
}

/**
 * Checks if an array ($_GET array if no array is passed) has an attribute and it is different to '' (empty)
 */
function _getHas($val, $arr = NULL) {
  return _get($val, $arr) != '';
}

/**
 * Return an array ($_POST array if no array is passed) value or empty if it does not exists
 */
function _post($val, $arr = NULL) {
  $arr = isset($arr) ? $arr : $_POST;
  return isset($arr[$val]) ? $arr[$val] : '';
}

/**
 * Load images from 'assets/images' folder
 */
function loadImage($img) {
  echo "<img src='http://barrybolivia.com/zion/assets/images/$img' />";
}

function showCategory($category) {
  $stars = array(
    '1EST' => 1,
    '1LL' => 1,
    '2EST' => 2,
    '2LL' => 2,
    '3EST' => 3,
    '3LL' => 3,
    '4EST' => 4,
    '4LL' => 4,
    '4LUX' => 4,
    '5EST' => 5,
    '5LL' => 5,
    '5LUX' => 5,
    'AG' => 0,
    'ALBER' => 0,
    'APTH' => 1,
    'APTH2' => 2,
    'APTH3' => 3,
    'APTH4' => 4,
    'APTH5' => 5,
    'AT1' => 0,
    'AT2' => 0,
    'AT3' => 0,
    'BB' => 0,
    'BB3' => 3,
    'BB4' => 4,
    'BB5' => 5,
    'BOU' => 0,
    'CAMP1' => 0,
    'CAMP2' => 0,
    'CHUES' => 0,
    'H1_5' => 1.5,
    'H2S' => 2,
    'H2_5' => 2.5,
    'H3S' => 3,
    'H3_5' => 3.5,
    'H4_5' => 4.5,
    'H5_5' => 5.5,
    'HIST' => 0,
    'HR' => 1,
    'HR2' => 2,
    'HR3' => 3,
    'HR4' => 4,
    'HR5' => 5,
    'HRS' => 5,
    'HS' => 1,
    'HS2' => 2,
    'HS3' => 3,
    'HS4' => 4,
    'HS5' => 5,
    'HSR1' => 1,
    'HSR2' => 2,
    'LODGE' => 0,
    'MINI' => 0,
    'PENDI' => 0,
    'PENSI' => 0,
    'POUSA' => 0,
    'RESID' => 0,
    'RSORT' => 0,
    'SPC' => 0,
    'STD' => 0,
    'SUP' => 4,
    'VILLA' => 0,
    'VTV' => 0);
  $num_stars = array_key_exists($category . '', $stars) ? $stars[$category . ''] : 0;
  showStars($num_stars);
}

function category_number($category) {
  $stars = array(
    '1EST' => 1,
    '1LL' => 1,
    '2EST' => 2,
    '2LL' => 2,
    '3EST' => 3,
    '3LL' => 3,
    '4EST' => 4,
    '4LL' => 4,
    '4LUX' => 4,
    '5EST' => 5,
    '5LL' => 5,
    '5LUX' => 5,
    'AG' => 0,
    'ALBER' => 0,
    'APTH' => 1,
    'APTH2' => 2,
    'APTH3' => 3,
    'APTH4' => 4,
    'APTH5' => 5,
    'AT1' => 0,
    'AT2' => 0,
    'AT3' => 0,
    'BB' => 0,
    'BB3' => 3,
    'BB4' => 4,
    'BB5' => 5,
    'BOU' => 0,
    'CAMP1' => 0,
    'CAMP2' => 0,
    'CHUES' => 0,
    'H1_5' => 1.5,
    'H2S' => 2,
    'H2_5' => 2.5,
    'H3S' => 3,
    'H3_5' => 3.5,
    'H4_5' => 4.5,
    'H5_5' => 5.5,
    'HIST' => 0,
    'HR' => 1,
    'HR2' => 2,
    'HR3' => 3,
    'HR4' => 4,
    'HR5' => 5,
    'HRS' => 5,
    'HS' => 1,
    'HS2' => 2,
    'HS3' => 3,
    'HS4' => 4,
    'HS5' => 5,
    'HSR1' => 1,
    'HSR2' => 2,
    'LODGE' => 0,
    'MINI' => 0,
    'PENDI' => 0,
    'PENSI' => 0,
    'POUSA' => 0,
    'RESID' => 0,
    'RSORT' => 0,
    'SPC' => 0,
    'STD' => 0,
    'SUP' => 4,
    'VILLA' => 0,
    'VTV' => 0);
  $num_stars = array_key_exists($category . '', $stars) ? $stars[$category . ''] : 0;
  return $num_stars;
}

function showStars($stars) {
  echo '<span style="white-space: nowrap;">';
  for ($i = 0; $i < floor($stars); $i++) {
    echo '<i class="fa fa-star" /></i> ';
  }

  // If it has a half star
  if (floor($stars) != $stars) {
    echo '<i class="fa fa-star-o" /></i> ';
  }

  // Missing stars as empty stars
  for ($i = ceil($stars); $i < 5; $i++) {
    echo '<i class="fa fa-star" style="opacity: 0.3;" /></i> ';
  }

  // If no star
  if ($stars == 0) {
    echo '<img src="http://barrybolivia.com/zion/assets/images/home.png" />';
  }
  echo '</span>';
}

function getShoppingCartStatus() {
  $count = ShoppingCart::getItemsCount();
  if ($count == 0) {
    return 'Carrito vac&iacute;o';
  }
  else if ($count == 1) {
    return '1 Producto';
  }
  else {
    return $count . ' Productos';
  }
}

/**
 * Converts from YYYYMMDD to DD/MM/YYYY
 */
function dateToDDMMYY($date) {
  return substr($date, 6, 2) . '/' . substr($date, 4, 2) . '/' . substr($date, 0, 4);
}

/**
 * Date from the DB format to DD/MM/YYYY
 */
function dateFormatFromDB($date) {
  if ($date == '0000-00-00' || $date == '0000-00-00 00:00:00') {
    return '';
  }
  return date_format(new DateTime($date), 'd/m/Y');
}

/**
 * Converts user-friendly date (DD/MM/YYYY) to DB format (YYYY-MM-DD)
 */
function dateFormatForDB($date) {
  $date_arr = explode('/', $date);
  if (count($date_arr) >= 3) {
    return $date_arr[2] . '-' . $date_arr[1] . '-' . $date_arr[0];
  }
  else return '0000-00-00';
}

/**
 * Converts from YYYYMMDD to YYYY-MM-DD
 */
function dateFromYmdToDB($date) {
  return substr($date, 0, 4) . '-' . substr($date, 4, 2) . '-' . substr($date, 6, 4);
}

function clearBookingSession() {
  ShoppingCart::emptyCart();
}

/**
 * Send mail to the agency, the counter and the operator.
 * '../lib/swiftmailer/swift_required.php' should be included by the method caller
 */
function sendNotificationMail($subject, $msg, $currentUser = array()) {
  $currentAgency = Agency::findById($currentUser['agency_id']);
  $to = array();
  if (isset($currentAgency)) {
    $to[] = trim($currentAgency->mail);
  }
  if ($currentUser['mail'] != '' && !in_array($currentUser['mail'], $to)) {
    $to[] = trim($currentUser['mail']);
  }
  $config = Config::findById(1);
  if ($config->mail != '' && !in_array($config->mail, $to)) {
    $to[] = trim($config->mail);
  }
  if ($config->mail2 != '' && !in_array($config->mail2, $to)) {
    $to[] = trim($config->mail2);
  }

  $arrUserAdmin = getAllUsersAdmin($currentUser['agency_id']);
  if (count($arrUserAdmin) > 0) {
    $to = array_merge($arrUserAdmin, $to);
  }
  //obtiene los user admin d la agencia administradora y envia correo 
  $agency_manager   =  AgencyManager::getAgencyManager($currentAgency->id);
  if (count($agency_manager) > 0 && $currentAgency->id != 128 && $currentAgency->id != 147) {
    $arrManagerAdmin  = getAllUsersAdmin($agency_manager->agency_id);
    if (count($arrManagerAdmin) > 0) {
      $to = array_merge($arrManagerAdmin, $to);   
    }
  }
  
  $to[] = 'notificaciones@barrybolivia.com';
  
  $to = array_filter($to);
  $html = "<html><body>$msg</body></html>";

  // $transport = Swift_SmtpTransport::newInstance('mail.boliviabooking.com', 25)
  //         ->setUsername('mailing@boliviabooking.com')
  //         ->setPassword('mailing!@#')
  //         ;
  $transport = Swift_SmtpTransport::newInstance('mail.viacontours.com', 465, 'ssl')
          ->setUsername('info@yocounter.com')
          ->setPassword('KVefuuB4Q[d,');

  $mailer = Swift_Mailer::newInstance($transport);

  $message = Swift_Message::newInstance($subject)
    ->setFrom(array('reservas@yocounter.com' => 'Reservas YoCounter'))
    ->setTo($to)
    ->setBody($html, 'text/html');

  $result = $mailer->send($message);
}

function getAllUsersAdmin($agencyId) {
  $query = "SELECT * FROM User WHERE agency_id = '$agencyId' AND role = 'ADMIN'";
  $users = User::findByQuery($query);
  $dontSendMail = array(10 => 'Reny Severiche', 26 => 'Dirza Pantoja' , 177 => 'Omar Flores', 328 => 'Darwin Robles',
                        590 => 'Gabor Moncada', 309 => 'Angie Cepeda', 905 => 'Miguel Condori', 894 => 'Carlos Rodriguez');
  $arr_user = array();
  foreach ($users as $value) {
    if (!array_key_exists($value->id, $dontSendMail)) {
      $arr_user[] = $value->mail;
    }
  }
  return $arr_user;
}

function getAgencyManager($bookAgency) {
  $agency_manager =  Ciudad::find(array('id' => $bookAgency->ciudad_id));
}

function formatTime($time) {
  if (strlen($time) == 4) {
    $time = substr($time, 0, 2) . ':' . substr($time, 2, 2);
  }
  return $time;
}

/**
 * Different from getUserPrice because it does not involve the Agency extra fee.
 * Useful for cancellation fee or discounts.
 */
function getPriceWithFee($price) {
  if (!isset($_SESSION['fee']) || $_SESSION['fee'] == 0) {
    $_SESSION['fee'] = Config::findById(1)->fee;
  }
  return round($price / $_SESSION['fee'], 2);
}

function isNewRequest() {
  $isOld = !isset($_POST['requestCode']) || (isset($_SESSION['requestCode']) && $_SESSION['requestCode'] == $_POST['requestCode']);

  if (isset($_POST['requestCode'])) {
    $_SESSION['requestCode'] = $_POST['requestCode'];
  }

  return !$isOld;
}

/**
 * Parse stdClass object to a given class
 */
function stdToObject($instance, $className) {
  if (get_class($instance) != $className) {
    return unserialize(sprintf(
        'O:%d:"%s"%s',
        strlen($className),
        $className,
        strstr(strstr(serialize($instance), '"'), ':')
    ));
  }
  return $instance;
}
function getHotelCodesMarkUp()
{
  // codigo de hotel aplicando un fee distinto
  // Codigohotel_CodigoProveedor
  // $hotelCodesMarkUp = array('751703_VLF-HIM8-31T_187'   => 0.78, '3592546_O4R-2DRO6-31T_187'  => 0.78, '5068922_SYY-3D7VC-PU1_187'  => 0.78, 
  //                         '30070447_Q1O-K1CAM-ID1_187'  => 0.78, '31217094_SYY-KS3BY-PVW_187' => 0.78, '28900738_Q1O-J92FD-ID1_187' => 0.78, 
  //                         '3108535_VLF-22HKA-31T_187'   => 0.78, '30919162_SYY-KL54M-PVW_187' => 0.78);
  $hotelCodesMarkUp = [];
  return $hotelCodesMarkUp;
}

function sendToContable($params) {

    // $url     = 'http://localhost:8080/contablebarry/api_rest/generar_nota_debito_otros_servicios.php';
    $url     = 'https://agente.barrybolivia.com/contable/api_rest/generar_nota_debito_otros_servicios.php';
    
    // Get cURL resource
    $curl = curl_init();
    // Set some options - we are passing in a useragent too here 
    $a_options = [
        CURLOPT_RETURNTRANSFER  => 1, 
        CURLOPT_SSL_VERIFYHOST  => false, 
        CURLOPT_SSL_VERIFYPEER  => false, 
        CURLOPT_USERAGENT       => 'Contable Request'
    ];

    $a_options[CURLOPT_URL]         = $url;
    $a_options[CURLOPT_POST]        = 1;
    $a_options[CURLOPT_POSTFIELDS]  = array('params'=>json_encode($params));

    curl_setopt_array($curl, $a_options);
    // Send the request & save response to $resp
    $error  = '';
    $resp   = curl_exec($curl);

    if($resp === false)
        $error = 'Curl error: ' . curl_error($curl);
    // Close request to clear up some resources
    curl_close($curl);
    if($error != '')
        return json_decode(array('result' => 'error', 'detalle' => $error));
    $resultado = json_decode($resp);
    return $resultado;    
}

function get_price_only_fees($price, $fees, $onlyMontos = false) {

    $total_prc   = 0;
    $total_monto = 0;
    if (property_exists($fees, 'fee_prc') && $fees->fee_prc != 0) {
        $fee_porcentaje = floatval($fees->fee_prc);
        $total_prc      = $price * ($fee_porcentaje / 100);
    }
    if (property_exists($fees, 'fee_monto') && $fees->fee_monto != 0) {
        $total_monto = floatval($fees->fee_monto);
    }
    if ($onlyMontos) {
      return round($total_monto + $total_prc, 2);
    }
    return round($price + $total_monto + $total_prc, 2);
}

?>
