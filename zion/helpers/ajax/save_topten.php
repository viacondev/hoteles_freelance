<?php

  include_once('../../database/DB.php');
  include_once('../../../config/Config.php');
  include_once('../../model/Model.php');
  include_once('../../model/HotelTopTen.php');

  if ($_POST['action'] == 'save') {
    print_r($_POST);
    $conexion = new PDO("mysql:host=localhost;dbname=barrybol_zion_engine;charset=utf8", 'barrybol_zion', 'zionshopdbpwd');
    $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conexion->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

    $hotelList  = $_POST['params'];
    print_r($hotelList);
    $zion_code  = $hotelList['zion_code'];
    $hotel_name = $hotelList['hotel_name'];
    $destination_code = $hotelList['destination_code'];
    $reserved = 0;
    $sentencia = $conexion->prepare("INSERT INTO HotelTopTen(DESTINATION_CODE, ZION_CODE, HOTEL_NAME, RESERVED) 
                                    VALUES (:DESTINATION_CODE, :ZION_CODE, :HOTEL_NAME, :RESERVED)");
    $sentencia->bindParam(':DESTINATION_CODE', $destination_code);
    $sentencia->bindParam(':ZION_CODE', $zion_code);
    $sentencia->bindParam(':HOTEL_NAME', $hotel_name);
    $sentencia->bindParam(':RESERVED', $reserved);
    $sentencia->execute();
    $respuesta = 'Se Inserto';
  }

  if ($_POST['action'] == 'delete') {
    print_r($_POST);
    $params       = $_POST['params'];
    $registro     = new HotelTopTen();
    $registro->id = $params['id'];
    $registro->delete();
  }
  

?>
