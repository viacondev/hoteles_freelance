<?php
include('../../../config/Config.php');
include('../../database/DB.php');
include('../../model/Model.php');
include('../../model/BookingUser.php');

  if(isset($_POST['selector']) && $_POST['selector'] == 'guardar_clientes') {
    $user = new BookingUser();
    $user->id_cliente      = $_POST['id_cliente'];
    $user->nombre_apellido = $_POST['nombre_cliente'];
    $user->booking_id      = $_POST['booking_id'];
    $user->user_id         = '';
    $user->save();
    echo json_encode(array("si","correcto"));
  }

  if(isset($_POST['selector']) && $_POST['selector'] == 'eliminar_cliente') {
    $user = new BookingUser();
    $id   = BookingUser::findAll(array('booking_id' => $_POST['booking_id'],'id_cliente' => $_POST['id_cliente']));
    if($id != NULL) {
      $user->id = $id[0]->id;
      $user->delete();
    }
    echo json_encode(array("no","elemento no eliminado"));
  }

  if(isset($_POST['selector']) && $_POST['selector'] == 'actualizar_cliente'){
    $user = new BookingUser();
    $id   = BookingUser::findAll(array('booking_id' => $_GET['booking_id']));
    if($id != NULL) {
      $user->id = $id[0]->id;
    }
    $user->id_cliente = $_POST['user_id'];
    $user->nombre_apellido = $_POST['nombre_cliente'];
    $user->booking_id = $_POST['booking_id'];
    $user->save();
  }
  
?>