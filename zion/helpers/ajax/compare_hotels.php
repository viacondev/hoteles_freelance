<?php

include_once('../../database/DB.php');
include_once('../../../config/Config.php');
include_once('../../helpers/logger.php');
include_once('../../helpers/application_helper.php');
include_once('../../../booking/BookingEngine.php');
include_once('../../model/Model.php');
include_once('../../model/User.php');
include_once('../../model/Agency.php');
include_once('../../model/ServiceType.php');

session_start();

$items    = $_GET['items'];
$agencyId = $_GET['agencyId'];
$agency   = Agency::findById($agencyId);
$booking  = new BookingEngine();
$details  = array();
$ar       = array();
parse_str($_GET['form'], $ar);

for ($i = 0; $i <= 3; $i++) {
  if (isset($items[$i])) {
    $item = $items[$i];
    $details[] = $booking->execRequest(array(
                  'requestType' => BookingRequestTypes::HotelDetail,
                  'hotelCode' => $item['hotelcode'],
                  'provider' => $item['provider'],
                  'version' => 2,
                  'params' => $ar));
  }
}

$internet = array();
$restaurant = array();
$parking = array();
$television = array();
$gym = array();
$air = array();
$pool = array();
$heat = array();

foreach ($details as $detail) {
  $roomFacilities = $detail->getRoomFacilities();
  $cateringFacilities = $detail->getCateringFacilities();
  $servicesFacilities = $detail->getServicesFacilities();
  $entertainmentFacilities = $detail->getEntertainmentFacilities();

  $internet[] = array_key_exists('100', $roomFacilities) || array_key_exists('250', $servicesFacilities);;
  $restaurant[] = count($cateringFacilities) > 0;
  $television[] = array_key_exists('55', $roomFacilities);
  $gym[] = array_key_exists('470', $servicesFacilities);
  $air[] = array_key_exists('170', $roomFacilities) || array_key_exists('180', $roomFacilities);
  $pool[] = array_key_exists('360', $entertainmentFacilities) ||
            array_key_exists('361', $entertainmentFacilities) ||
            array_key_exists('362', $entertainmentFacilities) ||
            array_key_exists('363', $entertainmentFacilities) ||
            array_key_exists('364', $entertainmentFacilities) ||
            array_key_exists('365', $entertainmentFacilities) ||
            array_key_exists('385', $entertainmentFacilities);
  $heat[] = array_key_exists('190', $roomFacilities) || array_key_exists('195', $roomFacilities);
  $parking[] = array_key_exists('320', $servicesFacilities) ||
                array_key_exists('500', $servicesFacilities) ||
                array_key_exists('996', $servicesFacilities);
}

function showService($name, $service) {
  echo '<tr>';
  echo '<td style="font-weight: bold;">' . $name . '</td>';
  foreach ($service as $s) {
    echo $s ? '<td style="text-align: center;"><img src="https://app.boliviabooking.com/hotelesboliviabooking/zion/assets/images/check-circle.png" /></td>' : '<td>&nbsp;</td>';
  }

  for ($i = 0; $i < 4 - count($service); $i++) {
    echo '<td>&nbsp;</td>';
  }
  echo '</tr>';
}

?>

<style type="text/css">
  .compare-table {
    border: 1px solid #ddd;
  }
  .compare-table td {
    padding: 5px;
    border-left: 1px solid #ddd;
    margin: 0;
  }
  .compare-table > tr > td {
    border-bottom-width: 2px;
  }
  .compare-table > tbody > tr:nth-child(odd) > td,
  .compare-table > tbody > tr:nth-child(odd) > th {
    background-color: #f9f9f9;
  }
</style>

<style media="print">
  .compare-table { font-size: 13px; }
</style>
<table width="840">
  <tr>
    <td>
      <table width="840">
        <tr>
          <td>
            <div>
              <?php echo $agency->address; ?><br>
              <?php echo $agency->phone; ?><br>
              <?php echo $agency->mail; ?>
            </div>
          </td>
          <td align="right">
            <div><img src="<?php echo $agency->get_url_logo(); ?>"></div>
          </td>
          <tr>
            <td colspan="2">
              <div style='background: #323131; font-size: 1px; line-height: 2px;'>&nbsp;</div>
              <h4>Hoteles en <?php echo $_GET['destination'] . '. Del ' . $_GET['checkin'] . ' al ' . $_GET['checkout'] ?>.</h4>
              <br/>
              <h4 style="color:red;font-size: 15px;font-weight: bold;">PRECIOS POR HABITACIÓN  POR LAS NOCHES INDICADAS</h4>
            </td>
          </tr>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <table class="compare-table" style="border: 1px solid #ddd;">
        <tr>
          <td width="">&nbsp;</td>
          <?php
          for ($i = 0; $i < 4; $i++) {
            if (!isset($details[$i])) {
              echo '<td width="20%"></td>';
              continue;
            }
            $detail     = $details[$i];
            $images     = $detail->getImageList();
            $image      = count($images) > 0 ? $images[0] : $items[$i]['img'];
            $hotelName  = $detail->getName() != '' ? $detail->getName() : $items[$i]['hotelname']; 
            ?>
            <td style="width: 155px; vertical-align: top;">
              <div><img src="<?php echo $image; ?>" width='155' height='104' onerror='this.onerror=null;this.src=\"http://barrybolivia.com/zion/assets/images/no-photo.jpg\";' /></div>
              <div style="color: #3399f3; font-size: 16px;"><?php echo $hotelName; ?></div>
              <div><?php showCategory($detail->getCategoryCode()); ?></div>

              <div style="font-size: 0.9em; color: #999;">
                <img src="https://app.boliviabooking.com/hotelesboliviabooking/zion/assets/images/location-arrow.png" />
                <?php echo $detail->getAddressStreetName(); ?>
              </div>
              <div style="font-size: 0.9em; color: #999;">
                <img src="https://app.boliviabooking.com/hotelesboliviabooking/zion/assets/images/map-marker.png" />
                <?php echo $detail->getDestinationZone() . ', ' . $detail->getDestinationName(); ?>
              </div>
            </td>
            <?php
          }
          ?>
        </tr>

        <tr>
          <th colspan="5" style="text-align: left; background-color: #D3D3D3; border: 1px solid #ddd; padding: 5px; font-size: 1.1em;">Condiciones del contrato</th>
        </tr>
        <tr>
          <td style="font-weight: bold;">Precio desde*</td>
          <?php
          for ($i = 0; $i <= 3; $i++) {
            $item = $items[$i];
            echo '<td style="text-align: center; font-weight: bold; font-size: 1.2em;">';
            echo isset($item['price']) ? $item['price'] : '';
            echo isset($item['currency']) ? ' ' . $item['currency'] : '';
            echo '</td>';
          }
          ?>
        </tr>
        <tr>
          <td style="font-weight: bold;">R&eacute;gimen</td>
          <?php
          for ($i = 0; $i <= 3; $i++) {
            $item = $items[$i];
            echo '<td  style="text-align: center;">';
            echo isset($item['board']) ? $item['board'] : '';
            echo '</td>';
          }
          ?>
        </tr>
        <tr>
          <td style="font-weight: bold;">Tipo habitaci&oacute;n</td>
          <?php
          for ($i = 0; $i <= 3; $i++) {
            $item = $items[$i];
            echo '<td style="text-align: center;">';
            echo isset($item['type']) ? $item['type'] : '';
            echo '</td>';
          }
          ?>
        </tr>

        <tr>
          <th colspan="5" style="text-align: left; background-color: #D3D3D3; border: 1px solid #ddd; padding: 5px; font-size: 1.1em;">Servicios</th>
        </tr>
        <?php
        showService('Acceso a internet', $internet);
        showService('Restaurante', $restaurant);
        showService('Parqueo', $parking);
        showService('Televisi&oacute;n', $television);
        showService('Gimnasio', $gym);
        showService('Aire Acondicionado', $air);
        showService('Piscina', $pool);
        showService('Calefacci&oacute;n', $heat);
        ?>
      </table>
    </td>
  </tr>
</table>




<small>* Los precios son solo indicativos; pueden estar sujetos a modificaciones</small>
