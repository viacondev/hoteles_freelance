<?php

  include_once('../../../../viaconapp-rest/HotelBook.php');
 //   include('../../../config/Config.php');
  // include('../../helpers/logger.php');
  include_once('../../helpers/application_helper.php');
  include_once('../../helpers/logger.php');
  include_once('../../../booking/BookingEngine.php');
  include_once('../../../config/Config.php');
  include_once('../../database/DB.php');
  include_once('../../model/Model.php');
  include_once('../../model/User.php');
  include_once('../../model/Booking.php');
  include_once('../../model/Service.php');
  include_once('../../model/Provider.php');
  include_once('../../model/ServiceStatus.php');
  include_once('../../model/ServiceType.php');
  include_once('../../model/HotelRoom.php');
  include_once('../../model/DomiturHotelMapping.php');

  session_start();

  $booking    = new BookingEngine();
  $book       = Booking::findById($_POST['id']);
  $users      = json_decode($_POST['users']);
  $localizers = array();
  $HBServices = array();
  $MBServices = array();
  $DTServices = array();
  $details    = array();
  // BUSCAR INFORMACION

  foreach ($book->getServices() as $service) {
    if ($service->provider_id == Provider::HOTELBEDS && !isset($localizers[$service->localizer]) && $service->type == ServiceType::HOTEL) {
        $ref = explode('-', $service->localizer);
        $fileNumber = $ref[1];
        $incomingOffice = $ref[0];
        $params = array(
          'requestType'     => BookingRequestTypes::PurchaseDetail,
          'fileNumber'      => $fileNumber,
          'incomingOffice'  => $incomingOffice,
          'provider'        => $service->provider_id
        );
        $HBService = $booking->execRequest($params);
        $HBServices[] = $HBService;
        foreach ($HBService->getBookingServices() as $s) {
        //   // Get service details
          if ($s->getServiceType() == 'ServiceHotel') {
            $serviceInfo = $s->getServiceInfo();
            $params = array(
                'requestType'   => BookingRequestTypes::HotelDetail,
                'hotelCode'     => $serviceInfo->getCode(),
                'provider'      => $service->provider_id);
            $details[$serviceInfo->getCode()] = $booking->execRequest($params);
          }
        }

        $localizers[$service->localizer] = true;
    }
    else if($service->provider_id == Provider::DOMITUR && !isset($localizers[$service->localizer])) {
        $fileNumber = $service->localizer;
        $params = array(
          'requestType'     =>  BookingRequestTypes::PurchaseDetail,
          'localizer'      =>  $service->localizer,
          'provider'        => BookingProvider::DOMITUR
        );

        $DTService = $booking->execRequest($params);
        $DTServices[] = $DTService;

        $ss = $DTService->getBookingServices();
        $serviceInfo = $ss[0]->getServiceInfo();
        $map = DomiturHotelMapping::find(array('code' => $serviceInfo->getCode()));
        if($map != null) {
          $params = array(
              'requestType'   => BookingRequestTypes::HotelDetail,
              'hotelCode'     => $serviceInfo->getCode(),
              'provider'      => BookingProvider::HOTELBEDS);
          $details[$serviceInfo->getCode()] = $booking->execRequest($params);
        }
        else
        {
          //$DTServices = array();
        }
    }
    else if ($service->provider_id == Provider::METHABOOK && $service->type == ServiceType::HOTEL) {
        $params = array(
          'requestType'     => BookingRequestTypes::PurchaseDetail,
          'localizer'       => $service->localizer,
          'provider'        => $service->provider_id
        );

        $MBService = $booking->execRequest($params);
        $MBServices[] = $MBService;

        $ss = $MBService->getBookingServices();
        $serviceInfo = $ss[0]->getServiceInfo();
        $params = array(
            'requestType'   => BookingRequestTypes::HotelDetail,
            'hotelCode'     => $serviceInfo->getCode(),
            'provider'      => $service->provider_id);
        $details[$serviceInfo->getCode()] = $booking->execRequest($params);
    }
    else if ($service->provider_id == Provider::METHABOOK2 && $service->type == ServiceType::HOTEL) {
        $params = array(
          'requestType'     => BookingRequestTypes::PurchaseDetail,
          'localizer'       => $service->localizer,
          'provider'        => $service->provider_id
        );

        $MBBService = $booking->execRequest($params);
        $MBBServices[] = $MBBService;

        $ss = $MBBService->getBookingServices();
        $serviceInfo = $ss[0]->getServiceInfo();
        $params = array(
            'requestType'   => BookingRequestTypes::HotelDetail,
            'hotelCode'     => $serviceInfo->getCode(),
            'provider'      => $service->provider_id);
        $details[$serviceInfo->getCode()] = $booking->execRequest($params);
    }
    else if ($service->provider_id == Provider::TOURICO && $service->type == ServiceType::HOTEL) {
        $params = array(
          'requestType'     => BookingRequestTypes::PurchaseDetail,
          'localizer'       => $service->localizer,
          'provider'        => $service->provider_id
        );

        $THService = $booking->execRequest($params);
        $THServices[] = $THService;

        $ss = $THService->getBookingServices();
        $serviceInfo = $ss[0]->getServiceInfo();
        $params = array(
            'requestType'   => BookingRequestTypes::HotelDetail,
            'hotelCode'     => $serviceInfo->getCode(),
            'provider'      => $service->provider_id);
        $details[$serviceInfo->getCode()] = $booking->execRequest($params);
    }
  }

  // LEER INFORMACION
  $book_information = array();
  $localizers = array();
  foreach ($book->getServices() as $service) {
    $book_data    = array();
    $hotel_data   = array();
    $_rooms_array = array();
    $_status = 'not found';
    if($book->status == ServiceStatus::CONFIRMED)
      $_status = 'C';
    else if($book->status == ServiceStatus::PENDING)
      $_status = 'P';
    else if($book->status == ServiceStatus::CANCELLED)
      $_status = 'X';

    $book_data['status']        = $_status;
    $book_data['holder']        = $book->holder;
    $book_data['localizer']     = $service->localizer;
    $book_data['checkin']       = $service->checkin.'T13:00:00';
    $book_data['checkout']      = $service->checkout.'T13:00:00';
    $book_data['price']         = $service->price;
    $book_data['currency']      = $service->currency;
    $book_data['remarks']       = $book->remarks;
    $book_data['clientRemarks'] = '';
    $book_data['payableBy']     = 'Con el respaldo de Barry Top Services S.R.L.';
    $hotel_data['code']         = '';
    $hotel_data['providerId']   = $service->provider_id;
    $hotel_data['name']         = $service->name;
    $hotel_data['category']     = '';
    $hotel_data['destination']  = $service->destination_code;
    $hotel_data['lat']          = '';
    $hotel_data['long']         = '';
    $hotel_data['address']      = '';
    $hotel_data['mails']        = array();
    $hotel_data['phones']       = array();
    $hotel_data['web']          = '';
    $stado_send                 = '';
    if(count($HBServices) > 0 &&  $service->provider_id == Provider::HOTELBEDS) {
      $rooms                  = $service->getRooms();
      foreach ($rooms as $room) {
        if($room->adult_count == 0 && $room->child_count == 0)
          continue;
        $_room['type']      = $room->type;
        $_room['board']     = $room->board;
        $_room['adults']    = $room->adult_count;
        $_room['childs']    = $room->child_count;
        $_room['childAges'] = $room->childs_ages;
        $_rooms_array[]     = $_room;
      }
      foreach ($HBServices as $HBService) {
        foreach ($HBService->getBookingServices() as $serviced) {
          $serviceInfo   = $serviced->getServiceInfo();
          if(preg_match("/".$service->name."/i", strval($serviceInfo->getName()))) {
            $detail        = $details[$serviceInfo->getCode()];
            $localizer_hb  = $HBService->getReferenceIncomingOffice() . '-' . $HBService->getReferenceFileNumber();
            // if($service->localizer == $localizer_hb) {

              // $book_data['status']        = $_status;
              // $book_data['holder']        = $book->holder;
              // $book_data['localizer']     = $service->localizer;
              // $book_data['checkin']       = dateFromYmdToDB($serviceInfo->getDateFrom()).'T13:00:00';
              // $book_data['checkout']      = dateFromYmdToDB($serviceInfo->getDateTo()).'T13:00:00';
              // $book_data['price']         = $service->price;
              // $book_data['currency']      = $service->currency;
              $hotel_data['providerId']   = $service->provider_id;
              $hotel_data['name']         = $serviceInfo->getName();
              $hotel_data['destination']  = $service->destination_code;
              $hotel_data['address']      = '';
              $hotel_data['web']          = '';
              // $book_data['clientRemarks'] = $serviceInfo->getClientComment();
              $book_data['remarks']       = $serviceInfo->getContractComment();
              $book_data['payableBy']     = $serviceInfo->getSupplier() . ' ' . $serviceInfo->getSupplierVatNumber();
              // $book_data['checkin']       = dateFromYmdToDB($serviced->getDateFrom());
              // $book_data['checkout']      = dateFromYmdToDB($serviced->getDateTo());
              $hotel_data['code']         = $serviceInfo->getCode();
              $hotel_data['category']     = category_number($detail->getCategoryCode());
              $hotel_data['lat']          = $detail->getLatitude();
              $hotel_data['long']         = $detail->getLongitude();
              $hotel_data['address']      = $detail->getAddressStreetName() . ', ' . $detail->getAddressNumber(). ' - ' . $detail->getAddressPostalCode();
              $hotel_data['mails']        = $detail->getEmailList();
              $hotel_data['phones']       = $detail->getPhoneList();

              /*foreach ($serviceInfo->getRoomsAvail() as $room) {
                $_room['type']   = $room->getRoomType(). ' x ' . $room->getRoomCount();
                $_room['board']  = $room->getBoard();
                $_room['adults'] = $room->getAdultCount();
                $_room['childs'] = $room->getChildCount();
                $_child_ages = '';
                foreach ($room->getGuestList() as $guest) {
                  if ($guest->getType() == 'CH') {
                    $_child_ages .= ($_child_ages == '') ? $guest->getAge() : ', ' . $guest->getAge();
                  }
                }
                $_room['childAges'] = $_child_ages;
                $_rooms_array[] = $_room;
              }*/
              // $books['room'] = $_rooms_array;
              // break;
              // if($serviced->getServiceStatus() == "CONFIRMED") {
                $book_information[] = array('book_data'=>$book_data, 'hotel_data' => $hotel_data, 'rooms' => $_rooms_array);
                // break;
              // }
          }
        }
      }
    }
    else if(count($MBServices) > 0 && $service->provider_id == Provider::METHABOOK) {
      foreach ($MBServices as $MBService) {
            foreach ($MBService->getBookingServices() as $serviced) {
               $serviceInfo    = $serviced->getServiceInfo();
               $detail         = $details[$serviceInfo->getCode()];
               $localizer_mb   = $serviced->getLocalizer();
               if($book_data['localizer'] == $localizer_mb) {
                $sear           = array("<strong>",'</strong>');
                $hotel_data['category']     = category_number($detail->getCategoryCode());
                $book_data['payableBy']     = str_replace($sear, ' ', $serviceInfo->getSupplier()) . ' ' . $serviceInfo->getSupplierVatNumber();
                $book_data['clientRemarks'] = $serviceInfo->getClientComment();
                $book_data['remarks']       = $serviceInfo->getContractComment();
                $hotel_data['code']         = $serviceInfo->getCode();
                $hotel_data['lat']          = $detail->getLatitude();
                $hotel_data['long']         = $detail->getLongitude();
                $hotel_data['address']      = $detail->getAddressStreetName() . ', ' . $detail->getAddressNumber(). ' - ' . $detail->getAddressPostalCode();
                $hotel_data['mails']        = $detail->getEmailList();
                $_phones  = array();
                foreach ($detail->getPhoneList() as $item) {
                  $_phones[] = $item.'';
                }
                $hotel_data['phones'] = $_phones;
                foreach ($serviceInfo->getRoomsAvail() as $room) {
                  $_room['type']   = $room->getRoomType(). ' x ' . $room->getRoomCount();
                  $_room['board']  = $room->getBoard().'';
                  $_room['adults'] = $room->getAdultCount();
                  $_room['childs'] = $room->getChildCount();
                  $_child_ages     = '';
                  foreach ($room->getGuestList() as $guest) {
                    if ($guest->getType() == 'CH') {
                      $_child_ages .= ($_child_ages == '') ? $guest->getAge() : ', ' . $guest->getAge();
                    }
                  }
                  $_room['childAges'] = $_child_ages;
                  $_rooms_array[] = $_room;
                }
                break;
              }
            }
        }
    }
    else if(count($MBBServices) > 0 && $service->provider_id == Provider::METHABOOK2) {
      foreach ($MBBServices as $MBBService) {
            foreach ($MBBService->getBookingServices() as $serviced) {
               $serviceInfo    = $serviced->getServiceInfo();
               $detail         = $details[$serviceInfo->getCode()];
               $localizer_mb   = $serviced->getLocalizer();
               if($book_data['localizer'] == $localizer_mb) {
                $sear           = array("<strong>",'</strong>');
                $hotel_data['category']     = category_number($detail->getCategoryCode());
                $book_data['payableBy']     = str_replace($sear, ' ', $serviceInfo->getSupplier()) . ' ' . $serviceInfo->getSupplierVatNumber();
                $book_data['clientRemarks'] = $serviceInfo->getClientComment();
                $book_data['remarks']       = $serviceInfo->getContractComment();
                $hotel_data['code']         = $serviceInfo->getCode();
                $hotel_data['lat']          = $detail->getLatitude();
                $hotel_data['long']         = $detail->getLongitude();
                $hotel_data['address']      = $detail->getAddressStreetName() . ', ' . $detail->getAddressNumber(). ' - ' . $detail->getAddressPostalCode();
                $hotel_data['mails']        = $detail->getEmailList();
                $_phones  = array();
                foreach ($detail->getPhoneList() as $item) {
                  $_phones[] = $item.'';
                }
                $hotel_data['phones'] = $_phones;
                foreach ($serviceInfo->getRoomsAvail() as $room) {
                  $_room['type']   = $room->getRoomType(). ' x ' . $room->getRoomCount();
                  $_room['board']  = $room->getBoard().'';
                  $_room['adults'] = $room->getAdultCount();
                  $_room['childs'] = $room->getChildCount();
                  $_child_ages     = '';
                  foreach ($room->getGuestList() as $guest) {
                    if ($guest->getType() == 'CH') {
                      $_child_ages .= ($_child_ages == '') ? $guest->getAge() : ', ' . $guest->getAge();
                    }
                  }
                  $_room['childAges'] = $_child_ages;
                  $_rooms_array[] = $_room;
                }
                break;
              }
            }
        }
    }
    else if($service->provider_id == Provider::DOMITUR && $service->type == ServiceType::HOTEL) {
      foreach ($DTServices as $DTService) {
        foreach ($DTService->getBookingServices() as $serviced) {

          $serviceInfo   = $serviced->getServiceInfo();
          $detail        = (array_key_exists($serviceInfo->getCode(), $details)) ? $details[$serviceInfo->getCode()] : array();
          $localizer_dt  = $serviced->getLocalizer();
          if($book_data['localizer'] == $localizer_dt) {
            $_clientRemarks = $serviceInfo->getClientComment();
            $_payableBy    = 'DOMITUR PRODUCT,S.L.U B3887776';
            // $_clientRemarks = $serviceInfo->getClientComment();
            $hotel_data['code']     = $serviceInfo->getCode();
            $hotel_data['category'] = category_number('4EST');
            if($detail != null) {
              $_lat           = $detail->getLatitude();
              $_lon           = $detail->getLongitude();
              $_address       = $detail->getAddressStreetName() . ', ' . $detail->getAddressNumber(). ' - ' . $detail->getAddressPostalCode();
              $_mails         = $detail->getEmailList();
              $_phones        = $detail->getPhoneList();
            }
            $rooms          = $service->getRooms();
            foreach ($rooms as $room) {
              $_room['type']      = $room->type;
              $_room['board']     = $room->board;
              $_room['adults']     = $room->adult_count;
              $_room['childs']     = $room->child_count;
              $_room['childAges'] = $room->childs_ages;
              $_rooms_array[]     = $_room;
            }
            break;
          }
        }
      }
    }
    else if($service->provider_id == Provider::TOURICO && $service->type == ServiceType::HOTEL) {
        foreach ($THServices as $THService) {
            foreach ($THService->getBookingServices() as $serviced) {
               $serviceInfo    = $serviced->getServiceInfo();
               $detail         = $details[$serviceInfo->getCode()];
               $localizer_mb   = $serviced->getLocalizer();
               if($book_data['localizer'] == $localizer_mb) {
                $sear           = array("<strong>",'</strong>');
                $hotel_data['category']     = category_number($detail->getCategoryCode());
                $book_data['payableBy']     = str_replace($sear, ' ', $serviceInfo->getSupplier()) . ' ' . $serviceInfo->getSupplierVatNumber();
                $book_data['clientRemarks'] = $serviceInfo->getClientComment();
                $book_data['remarks']       = $serviceInfo->getContractComment();
                $hotel_data['code']         = $serviceInfo->getCode();
                $hotel_data['lat']          = $detail->getLatitude();
                $hotel_data['long']         = $detail->getLongitude();
                $hotel_data['address']      = $detail->getAddressStreetName() . ', ' . $detail->getAddressNumber(). ' - ' . $detail->getAddressPostalCode();
                $hotel_data['mails']        = $detail->getEmailList();
                $_phones  = array();
                foreach ($detail->getPhoneList() as $item) {
                  $_phones[] = $item.'';
                }
                $hotel_data['phones'] = $_phones;
                foreach ($serviceInfo->getRoomsAvail() as $room) {
                  $_room['type']   = $room->getRoomType(). ' x ' . $room->getRoomCount();
                  $_room['board']  = $room->getBoard().'';
                  $_room['adults'] = strval($room->getAdultCount());
                  $_room['childs'] = strval($room->getChildCount());
                  $_child_ages     = '';
                  foreach ($room->getGuestList() as $guest) {
                    if ($guest->getType() == 'CH') {
                      $_child_ages .= ($_child_ages == '') ? $guest->getAge() : ', ' . $guest->getAge();
                    }
                  }
                  $_room['childAges'] = $_child_ages;
                  $_rooms_array[] = $_room;
                }
                break;
              }
            }
        }
    }
    else if($service->type == ServiceType::HOTEL) {
      $hotel_data['category'] = category_number($service->category);
      $book_data['payableBy'] = 'Con el respaldo de Barry Top Services S.R.L.';
      $rooms                  = $service->getRooms();
      foreach ($rooms as $room) {
        $_room['type']      = $room->type;
        $_room['board']     = $room->board;
        $_room['adults']    = $room->adult_count;
        $_room['childs']    = $room->child_count;
        $_room['childAges'] = $room->childs_ages;
        $_rooms_array[]     = $_room;
      }
    }
    if($service->type == ServiceType::HOTEL && $service->provider_id != Provider::HOTELBEDS) {
      $book_information[] = array('book_data'=>$book_data, 'hotel_data' => $hotel_data, 'rooms' => $_rooms_array);
    }
  }


  $user_result = array();
  foreach ($users as $user) {
    $cliente = array();
    if($user[0] == 'user_id') {
        $cliente = array(
          // 'idclientes' => 500 // Id en el CRM. Opcional
          'userId'  => $user[1] // Id del usuario en ViaconApp. Opcional
        );
    }
    else {
      $cliente = array(
        'idclientes' => $user[1] // Id en el CRM. Opcional
        // 'userId'  => 'kGwYoeiuzRDmBdECg' // Id del usuario en ViaconApp. Opcional
      );
    }
    $si = '';$no = '';
    foreach ($book_information as $value) {
      $hotelBook = new Rest\HotelBook();
      $hotelBook->setBookData($value['book_data']);
      $hotelBook->setHotelData($value['hotel_data']);
      // $room_ = json_decode($value->addroom);
      foreach ($value['rooms'] as $room) {
        $hotelBook->addRoom($room);
      }

      if ($hotelBook->send($cliente))
        $si .= $value['hotel_data']['name'] . ', ';
      else
        $no .= $value['hotel_data']['name'] . ', ';
    }
    $user_result[] = array('si' => $si, 'no' => $no, 'id_control' => $user[2]);
  }
  echo json_encode($user_result);

?>
