<?php
include('../../../config/Config.php');
include('../../database/DB.php');
include('../../model/Model.php');
include('../../model/User.php');

$users = User::findAll(array('agency_id' => $_GET['agency_id'], 'status' => 1));

echo json_encode($users);

?>