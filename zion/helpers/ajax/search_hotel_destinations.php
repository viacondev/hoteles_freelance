<?php
// $db = new \PDO("mysql:host=localhost;dbname=barrybol_hotelbeds;charset=utf8", 'barrybol_zion', 'zionshopdbpwd');
$dbz = new \PDO("mysql:host=23.91.64.139;dbname=barrybol_zion_engine;charset=utf8", 'barrybol_zion', 'zionshopdbpwd');

$q = addslashes($_GET['q']);
$q = str_replace(' ', '%', $q);

// Lookup for hotels
$hotels_query =
    "SELECT * FROM Hotel
      WHERE hotel_name LIKE '%$q%' OR zone_name LIKE '%$q%' OR 
        destination_name LIKE '$q%' OR country_name LIKE '$q%'
      LIMIT 9";
$hotels_res = $dbz->query($hotels_query);

// Lookup for zones
$zones_query =
    "SELECT DISTINCT zone_code, zone_name, destination_code, destination_name, country_name FROM Hotel
      WHERE zone_name LIKE '$q%' OR destination_name LIKE '$q%' OR
        country_name LIKE '$q%'
      LIMIT 4";
$zones_res = $dbz->query($zones_query);

// Lookup for destination
$dests_query =
    "SELECT DISTINCT destination_code, destination_name, country_name FROM Hotel
      WHERE destination_name LIKE '$q%' OR destination_code = '$q' OR country_name LIKE '$q%'
      LIMIT 3";
$dests_res = $dbz->query($dests_query);

$res = array();
$res['dest'] = $dests_res->fetchAll();
$res['zones'] = $zones_res->fetchAll();
$res['hotels'] = $hotels_res->fetchAll();
echo json_encode($res);
?>