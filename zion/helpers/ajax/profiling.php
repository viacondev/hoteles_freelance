<?php

include('../../../config/Config.php');
include('../../helpers/logger.php');
include('../../database/DB.php');
include('../../model/Model.php');
include('../../model/User.php');
include('../../model/Profiling.php');

session_start();

$init = $_GET['init'];
$end = $_GET['end'];
$action = $_GET['action'];
$origin = $_GET['origin'];
$size = $_GET['size'];

$profile = new Profiling($action, $origin);
$profile->init = $init;
$profile->end = $end;
$profile->size = $size;

$profile->profile();

?>
