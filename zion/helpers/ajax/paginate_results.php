<?php

ini_set('memory_limit', -1);

include('../../model/Model.php');
include('../../model/MotorConexion.php');

$_GET['origen'] = 'yc';
$conexion = new MotorConexion();
$conexion->build('paginate', $_GET);
$resp  = $conexion->consultar();
echo json_encode($resp);


/*
include_once('../../database/DB.php');
include_once('../../../config/Config.php');
include_once('../../helpers/logger.php');
include_once('../../helpers/application_helper.php');
include_once('../../model/Model.php');
include_once('../../model/User.php');
include_once('../../model/BackHotelsAvail.php');
include_once('../../model/HotelTopTen.php');
include_once('../../model/Provider.php');

$token 			= $_GET['tokenAvail'];
$result 		= new BackHotelsAvail();
$result 		= $result->getResults($token);

if (!isset($result->rs)) {
	$ar = array('rs' => 'false');
	echo json_encode($ar);
}
else {
	$orderedHotels 	= $result->rs->hotelsAvail;
	$cantidadHotels = count($orderedHotels);

  if ($_GET['nombre'] != "" || isset($_GET['star']) && $_GET['star'] != '') {
    $hotelName     = $_GET['nombre'];
    $star          = isset($_GET['star']) ? $_GET['star'] : '';
		$arrResult     = searchByName($orderedHotels, $hotelName, $star);
		$orderedHotels = array();
		$orderedHotels = array_merge($orderedHotels, $arrResult);
	}
  if (isset($_GET['regimen']) && $_GET['regimen'] != '') {
    $regimen = $_GET['regimen'];
    $arrResultReg   = array();
    $arrResultReg   = searchByBoardCode($orderedHotels, $regimen);
    $orderedHotels  = array();
    $orderedHotels  = array_merge($orderedHotels, $arrResultReg);
  }

	if (isset($_GET['orderPrice']) && $_GET['orderPrice'] == 'DESC') {
		// ORDENACION DESCENDENTEMENTE
		usort($orderedHotels, 'orderPriceDesc');
	}
	if (isset($_GET['orderPrice']) && $_GET['orderPrice'] == 'ASC') {
		// ORDENACION ASCENDENTEMENTE
		usort($orderedHotels, 'orderPriceAsc');
	}
	if (isset($_GET['orderPrice']) && $_GET['orderPrice'] == 'REC') {
		// ORDENACION DE HOTELES RECOMENDADO
		$destCode 			= $result->params->destCode;
		$orderedHotels 	= orderRecomendado($orderedHotels, $destCode);
	}

	$limit      	=  15;
	$page 				= $_GET['page'];
	$inicio     	= ($page - 1) * $limit;
	$fin       		= ($page * $limit) - 1;
	$totalPages  	= ceil(count($orderedHotels) / $limit);
	$hotelsPaginated = array();
	// for ($i = $inicio; $i <= $fin; $i++) { 
		// if (isset($orderedHotels[$i])) {
			// $hotelsPaginated[] = $orderedHotels[$i];
		// }
	// }
  $hotelsPaginated = array_slice($orderedHotels, $inicio, $limit, false);
  // __log($inicio . ' -- ' . $fin);
  // __log(count($hotelsPaginated));
  if (isset($_GET['last_version']) && $_GET['last_version'] == "allResult") {
    $result->rs->hotelsAvail = $orderedHotels;
    $result->rs->currentPage = "1";
    $result->rs->totalPages  = "1";
  }
  else {
    $result->rs->hotelsAvail = $hotelsPaginated;
    $result->rs->currentPage = $page;
    $result->rs->totalPages  = $totalPages;
  }
	
	echo json_encode($result);
}
*/

function orderPriceDesc($a, $b) {
	$priceA = $a->groupedRooms;
  $priceB = $b->groupedRooms;
  $roomsA  = $priceA[0];
  $roomsB  = $priceB[0];
  $a = 0;
  $b = 0;
  foreach ($roomsA as $room) {
    $a += floatval($room->priceWithFee);
  }
  foreach ($roomsB as $room) {
    $b += floatval($room->priceWithFee);
  }
  if ($a == $b) return 0;
  return ($a > $b) ? -1 : 1;
}

function orderPriceAsc($a, $b) {
	$priceA = $a->groupedRooms;
  $priceB = $b->groupedRooms;
  $roomsA  = $priceA[0];
  $roomsB  = $priceB[0];
  $a = 0;
  $b = 0;
  foreach ($roomsA as $room) {
    $a += floatval($room->priceWithFee);
  }
  foreach ($roomsB as $room) {
    $b += floatval($room->priceWithFee);
  }
  if ($a == $b) return 0;
  return ($a > $b) ? 1 : -1;
}

function orderRecomendado($orderedHotels, $destCode) {
	$query = "SELECT DISTINCT hp.destination_code, hp.zion_code , h.hotel_name 
              FROM HotelTopTen hp, Hotel h 
              WHERE hp.zion_code = h.hotel_code AND hp.destination_code = '$destCode'";
  $hotels 				= HotelTopTen::findByQuery($query);
  $arrHotelCodes 	= array();
  foreach ($hotels as $hotel) {
    $arrHotelCodes[] = trim($hotel->zion_code);
  }
  $topOrderedHotels = array();
  $con = 0;
  foreach ($orderedHotels as $value) {
    if ($value->provider == Provider::DOMITUR2) { continue; }
    $hotelCode = $value->code;
    if(in_array($hotelCode, $arrHotelCodes)) {
      $topOrderedHotels[] = $value;
      unset($orderedHotels[$con]);
    }
    $con++;
  }
  $topOrderedHotels = array_merge($topOrderedHotels, $orderedHotels);
  return $topOrderedHotels;
}

function searchByName($orderedHotels, $filterhotelName, $filterCategory) {
  //search hotel by name
  if ($filterhotelName == '' && $filterCategory == '') {
    return $orderedHotels;
  }
  $hotelName  = '';
  $star       = $filterCategory != '' ? explode(',', implode(',', $filterCategory)) : '';
  $filterName = $filterhotelName;
  $hotel      = str_replace(" ", ',', $filterName);
  $hotel      = explode(',', $hotel);
  foreach ($hotel as $value) {
    if (strlen($value) > 3) {
      $hotelName .= trim($value) . "|";  
    }
  }
  $filterName = substr($hotelName, 0, -1);
  $arrResult  = array();
  foreach ($orderedHotels as $hotel) {
    $isFilter     = true;
    $hotelName    = strtolower($hotel->name);
    $categoryCode = strval($hotel->categoryCode);
    if ($hotelName != '' && !preg_match("/$filterName/i",$hotelName)) {
      $isFilter = false; 
    }
    if ($star != '' && !in_array("'" . $categoryCode . "'", $star)) {
      $isFilter = false;
    }
    if ($isFilter == true) {
      $arrResult[] = $hotel;
    }
  }

  return $arrResult;
}

function searchByBoardCode($orderedHotels, $regimen) {
  $regimen    = explode(',', implode(',', $regimen));
  $contador   = 0;
  $arrBoards  = array(
    'AI'  => 'TI', //Todo Incluido Codigo MBB=>GENERAL
    'BB'  => 'AD',  //Alojamiento y desayuno
    'OR'  => 'SA', //Solo Alojamiento
    'RO'  => 'SA',
    'NO'  => 'SA',
    'PC'  => 'FB', //Pension Completa
    'FB'  => 'FB',
    'MP'  => 'HB', //Media Pension
    'HB'  => 'HB',
    'RO' => 'RO','SA' => 'RO','OB' => 'RO','FI' => 'RO',/* Solo Alojamiento */
    'BB' => 'BB','AD' => 'BB',/* Alojamiento y Desayuno */
    'HB' => 'HB','MP' => 'HB',/* Media Pension */
    'FB' => 'FB','PC' => 'FB',/*Pension Completa*/
    'AI' => 'AI','TI' => 'AI'/*Todo Incluido*/
  );
  foreach ($orderedHotels as $hotel) {
    $h                = $hotel->groupedRooms;
    $cantidad         = count($h);
    $newGroupedRooms  = array();
    for ($i = 0; $i < $cantidad; $i++) {
      $reg = $h[$i][0]->boardCode;
      $reg = isset($arrBoards[$reg]) ? $arrBoards[$reg] : substr($reg, 0, 2); 
      if (in_array($reg, $regimen)) {
        $newGroupedRooms[] = $h[$i];
      }
    }
    $count = count($newGroupedRooms);
    if ($count > 0) {
      $hotel->groupedRooms = $newGroupedRooms;
    }
    else {
      unset($orderedHotels[$contador]);
    }
    $contador++;
  }
  return $orderedHotels;
}

function searchByZoneCode($orderedHotel, $zoneCode) {
  $listOrderHotels = array();
  $arrNames = array();
  foreach ($orderedHotel as $hotel) {
    $Zcode = $hotel->zoneCode;
    if ($hotel->provider == Provider::HOTELBEDS && $Zcode == $zoneCode) {
      $listOrderHotels[] = $hotel;
      $arrNames[] = $hotel->name;
    }
  }
  foreach ($orderedHotel as $hotel) {
    if ($hotel->provider != Provider::HOTELBEDS && in_array($hotel->name, $arrNames)) {
      $listOrderHotels[] = $hotel;
    }
  }
  return $listOrderHotels;
}

?>