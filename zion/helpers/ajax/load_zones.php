<?php
include('../../../config/Config.php');
include('../../database/DB.php');
include('../../model/Model.php');
include('../../model/Zone.php');

$zones = Zone::findAll(array('destination_code' => $_GET['destination_code']));

echo json_encode($zones);

?>