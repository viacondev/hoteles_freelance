<?php

include('../../../config/Config.php');
include('../../database/DB.php');
include('../../model/Model.php');
include('../../model/Log.php');
include('../../model/User.php');
include('../../model/ServiceType.php');

  $book_id = $_POST['book_number'];
  $logs = Log::findByQuery("SELECT * FROM Log WHERE action LIKE 'BOOK UPDATE $book_id'");
  $book_num = 1;
?>
<div class="col-md-12 ">
  <div class="col-md-12 box-header">Historial de Modificaciones</div>
  <div class="col-md-12">
    <?php foreach ($logs as $log) {
          $json = (array) json_decode($log->msg);
          $prev = (array) $json['prev'];
          $next = (array) $json['next'];
          $style_head = "background-color: #474949;color:white;padding: 10px;opacity: 0.8;text-align:center;";
          echo "<div class='col-md-4' style='$style_head'><strong>Fecha : </strong>" . date("d/m/Y", strtotime($log->date)) . "</div>
                <div class='col-md-4' style='$style_head'><strong>N# Reserva : </strong>". str_replace('BOOK UPDATE',' ', $log->action) ."</div>
                <div class='col-md-4' style='$style_head'><strong>Usuario : </strong>" . User::findById($log->user_id)->username . "</div>";
          echo "<div class='col-md-12' style='border: 1px solid #ddd;padding: 5px;'>
                  <div class='row' style='margin-top: 10px;'>";
          $cad = "<div class='col-md-12'>";
          foreach ((array)$prev as $key => $value) {
            if ($key == 'services') continue;
            $nextValue = isset($next[$key]) ? $next[$key] : '0';
            $style = $value != $nextValue ? 'style="background-color: #f89d0e;"' : '';
            if ($value != '') {
                $before_update = $value;
                $value_update = $nextValue;
                if ($key == 'user_id') {
                  $before_update = User::findById($before_update)->username;
                  $value_update  = User::findById($value_update)->username;
                }
                $cad .= "<div class='col-md-4'>
                          <strong>$key</strong> :</div>
                         <div class='col-md-4' $style> $before_update</div>
                         <div class='col-md-4' $style>$value_update</div>";
            }
          }
          $cad .= "</div>";
          echo $cad;
          $prevServices = (array) $prev['services'];
          $nextServices = (array) $next['services'];
          $reachedServices = get_last_update($prevServices,$nextServices, $book_num);
          get_after_update($prevServices,$nextServices,$reachedServices);
          echo "  </div>
                </div>";
          echo "<div class='clearfix'></div><br><hr/>";
          $book_num++;
       }
      ?>
  </div>
</div>
<?php
  function get_last_update($prevServices,$nextServices, $book_num) {
    $reachedServices = array();
    $con = 1;
    foreach ($prevServices as $prevService) {
      $prevService = (array) $prevService;
      $reachedServices[] = $prevService['id'];

      $nextService = NULL;
      foreach ($nextServices as $service) {
        $service = (array) $service;
        if ($service['id'] == $prevService['id']) {
          $nextService = $service;
          break;
        }
      }
      $type_service = "";
      if ($prevService['type'] == ServiceType::HOTEL){ $type_service = "fa fa-home"; }
      if ($prevService['type'] == ServiceType::FLIGHT){ $type_service = "fa fa-plane"; }
      if ($prevService['type'] == ServiceType::TICKET){ $type_service = "fa fa-ticket"; }
      if ($prevService['type'] == ServiceType::TRANSFER){ $type_service = "fa fa-suitcase"; }

      $cad = "<div id = 'service_" . $book_num . "_" . $con . "' class='col-md-12' style='display:none;padding: 10px 16px;'>";
      $can_dif = 0 ;
      foreach ((array) $prevService as $key => $value) {
        $nextValue = isset($nextService) && isset($nextService[$key]) ? $nextService[$key] : 'xxx';
        $style = '';
        if ($value != '' && $nextValue != '' && $value != $nextValue) {
          $style = 'style="background-color: #f89d0e;"';
          $can_dif++;
        }
        if ($value != '' && $nextValue != '') {
            $cad .= "<div class='col-md-4'><strong>$key</strong> :</div>
                     <div class='col-md-4' $style> $value</div>
                     <div class='col-md-4' $style> $nextValue</div>";
            $cad .= "<div class='clearfix'></div>";
        }
      }

      $cad .= "</div>";
      $head_cad =  "<div class='col-md-12'>
              <div class='' style='margin-top: 10px;'>
              <div style='background-color: #4b9ead;border: 1px solid #ddd;padding: 10px;border-radius: 5px;color:white;opacity: 0.8;padding: 10px;cursor:pointer;' onclick='$(\"#service_" . $book_num . "_" . $con . "\").fadeToggle();'>
                  <i class='" . $type_service . "'></i> " . $prevService['name'] . " (" . $can_dif .  " modificaciones)<span class='fa fa-caret-down pull-right'></span>
              </div>";
      echo $head_cad;
      echo $cad;

      echo "  </div>
            </div>";
      $con++;
    }
    return $reachedServices;
  }

  function get_after_update($prevServices, $nextServices, $reachedServices) {

    foreach ($nextServices as $service) {
      $next = (array) $service;
      if (in_array($next['id'], $reachedServices)) {
        continue;
      }
      echo '<table border="1" style="width: 100%">';
      foreach ((array)$prevServices as $key => $value) {
        $value = (array) $value;
        if ($value['id'] != $next['id']) {
          continue;
        }
        $nextValue = isset($next) && isset($next[$key]) ? $next[$key] : 'xxx';
        echo "<tr>";
        $style = $value != $nextValue ? 'style="background-color: yellow;"' : '';
        echo "<td $style><strong>$key</strong></td>";
        echo "<td $style>$value</td>";
        echo "<td $style>" . $nextValue . "</td>";
        echo "</tr>";
      }
      echo '</table>';
    }
  }
?>
