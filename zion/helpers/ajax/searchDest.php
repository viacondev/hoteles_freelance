<?php

include('../../../config/Config.php');
include('../../database/DB.php');
include('../../model/Model.php');
include_once('../../model/Config.php');
include_once('../../model/Fee.php');
include_once('../../model/Hotel.php');
$search = $_GET['extraParams'];
if ($_GET['term'] == 'country') {

  $query = "SELECT DISTINCT country_code, country_name FROM Hotel WHERE country_name LIKE '%" . $search . "%'";

  $country = Hotel::findByQuery($query);
  $countries = "[";
  $count = 1;
  foreach ($country as $value) {
    $countries .= "{\"label\":\"$value->country_name\", \"id\":\"$value->country_code\"},";
    $count++;
  }
  $countries = substr($countries, 0, -1);
  $countries .= "]";
  echo $countries;
}
if ($_GET['term'] == 'Dest') {

  $query = "SELECT DISTINCT destination_code, destination_name, country_code FROM Hotel WHERE country_code ='" . $search . "'";
  // echo $query;
  $country = Hotel::findByQuery($query);
  $countries = "[";
  $count = 1;
  foreach ($country as $value) {
    $countries .= "{\"destname\":\"$value->destination_name\", \"destid\":\"$value->destination_code\", \"countryid\":\"$value->country_code\"},";
    $count++;
  }
  $countries = substr($countries, 0, -1);
  $countries .= "]";
  echo $countries;
}

?>
