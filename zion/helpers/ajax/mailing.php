<?php
include('../../../lib/swiftmailer/swift_required.php');

$subject = $_POST['subject'];
$from = $_POST['from'];
$fromName = $_POST['fromName'];
$to = explode(',', $_POST['to']);
$msg = isset($_POST['msg']) ? $_POST['msg'] : '';
$body = isset($_POST['body']) ? $_POST['body'] : '';
$copy = $_POST['copy'];

$html = "<html><body>";
$html .= isset($msg) && $msg != '' ? $msg . '<br /><br />' : '';
$html .= isset($body) && $body != '' ? $body  : '';
$html .= "</body></html>";

$transport = Swift_SmtpTransport::newInstance('mail.boliviabooking.com', 25)
        ->setUsername('mailing@boliviabooking.com')
        ->setPassword('mailing!@#')
        ;

$mailer = Swift_Mailer::newInstance($transport);

$message = Swift_Message::newInstance($subject)
  ->setFrom(array($from => $fromName))
  ->setTo($to)
  ->setBody($html, 'text/html');
if ($copy == '1') {
  $message->setCc(array($from));
}

$result = $mailer->send($message);
?>