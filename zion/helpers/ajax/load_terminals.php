<?php
include('../../../config/Config.php');
include('../../database/DB.php');
include('../../model/Model.php');
include('../../model/Terminal.php');

$terminals = Terminal::findAll(array('destination_code' => $_GET['destination_code']));

echo json_encode($terminals);

?>