<?php
/**
 * TODO: We should create a Hotel model under Zion Engine, but we would face
 * sync issues, for now we query the HotelBeds DB (which should be updated frecuently)
 */

$dest_code = $_GET['destination_code'];
$db = new \PDO("mysql:host=localhost;dbname=barrybol_hotelbeds;charset=utf8", 'barrybol_zion', 'zionshopdbpwd');

$hot_query = "SELECT HOTELCODE as code, NAME as name, ZONECODE as zone_code
                FROM HOTELS WHERE DESTINATIONCODE = '$dest_code' ORDER BY NAME ASC";
$hot_res = $db->query($hot_query);

$hotels = array();
foreach ($hot_res as $row) {
  $hotel = array();
  $hotel['code'] = $row['code'];
  $hotel['name'] = $row['name'];
  $hotel['zone_code'] = $row['zone_code'];
  $hotels[] = $hotel;
}

echo json_encode($hotels);

?>