<?php
include('../../../config/Config.php');
include('../../database/DB.php');
include('../../model/Model.php');
include('../../model/User.php');
include('../../model/UserMapping.php');
include('../../model/AgencyMapping.php');
include('../../model/FeeGeneral.php');
include('../../model/Log.php');

date_default_timezone_set('America/La_Paz');
$db = new \PDO("mysql:host=localhost;dbname=boliviab_aereo;charset=utf8", 'boliviab_aereo', 'B0l1v1@B00k1ng');
$db_barry = new \PDO("mysql:host=localhost;dbname=barrybol_zion_engine;charset=utf8", 'barrybol_zion', 'zionshopdbpwd');

if (isset($_GET['buscar'])) {
  $users = User::findAll(array('agency_id' => $_GET['agency_id']));
  $array_users =  array();
  foreach ($users as $key => $value) {
    $user_mapping = UserMapping::find(array('userbarry'=>$value->id));
    $ismapping = false;
    if($user_mapping) {
       $ismapping = true;
    }
    $array_users[] = array('id' => $value->id,'username' => $value->username, 'name' => $value->name . ' ' . $value->lastname, 'ismapping' => $ismapping);
  }
  echo json_encode($array_users);
}

if (isset($_GET['save'])) {
  $user = new UserMapping();
  $user->userbb = $_GET['user_bb'];
  $user->userbarry = $_GET['user_barry'];
  $user->nombre = $_GET['name'];
  $user->save();
  echo "Guardado con Exito";
}

if (isset($_GET['save_agency'])) {
  $agency = new AgencyMapping();
  $agency->agencybb = $_GET['agency_bb'];
  $agency->agencybarry = $_GET['agency_barry'];
  $agency->agencyname = $_GET['agency_name'];
  $agency->save();
  echo "Guardado con Exito";
}

if (isset($_GET['save_fee'])) {
  $fee = new FeeGeneral();
  $fee->agency_comission = 10;
  $fee->incentive_counter = 2;
  $fee->factor_comission = 0.79;
  $fee->agencia = $_GET['id'];
  $fee->origen = 0;
  $fee->destino = '';
  $fee->save();
  echo "Guardado con Exito";
}
if(isset($_GET['get_agency_not_mapping'])) {
  if ($_GET['sistem'] == 'br') {
    $query = "SELECT id, name, address, phone, mail  FROM Agency ORDER BY name";
    $result = $db_barry->query($query);
    $arr_agenciasbr = array();
    foreach ($result as $key => $value) {
      $user_mapping = AgencyMapping::find(array('agencybarry'=>$value['id']));
      $ismapping = false;
      if($user_mapping) {
         $ismapping = true;
      }
      $arr_agenciasbr[] = array('id' => $value['id'], 'nombre' => $value['name'], 'mails' => $value['mail'],
                                'direccion' => $value['address'], 'telefonos' => $value['phone'], 'ismapping' => $ismapping);
    }
    echo json_encode($arr_agenciasbr);
  }
  if ($_GET['sistem'] == 'bb') {
    $hot_query = "SELECT * FROM bb_cliente ORDER BY NOMBRE ASC";
    $hot_res = $db->query($hot_query);
    $arr_agenciasbb = array();
    foreach ($hot_res as $key => $value) {
      $db_query = "SELECT * FROM AgencyMapping WHERE agencybb = " . $value['id'];
      $res = $db_barry->query($db_query);
      $datos = array( 'id'=>$value['id'], 'nombre' => $value['nombre'],
                      'mails' => $value['mails'], 'direccion' => $value['direccion'],
                      'telefonos' => $value['telefonos']);
      $datos['ismapping'] = (count($res->fetchAll()) > 0) ? true : false;
      $arr_agenciasbb[] = $datos;
    }
    echo json_encode($arr_agenciasbb);
  }

}
if (isset($_GET['get_user_agency'])) {
  if ($_GET['sistem'] == 'br') {
    $query = "SELECT id, username, name, lastname, agency_id, phone, mail FROM User WHERE agency_id=" .  $_GET['ID_AGENCY'];

    $res = $db_barry->query($query);
    $arr_user = array();
    foreach ($res as $key => $value) {
      $user_mapping = UserMapping::find(array('userbarry' => $value['id']));
      $ismapping = false;
      if($user_mapping) {
         $ismapping = true;
      }
      $arr_user[] = array('id' => $value['id'], 'username' => $value['username'], 'nombre' => $value['name'] . " " .$value['lastname'],
                          'mails' => $value['mail'], 'telefonos' => $value['phone'], 'agency_id' => $value['agency_id'], 'ismapping' => $ismapping);
    }
    echo json_encode($arr_user);
  }
  if ($_GET['sistem'] == 'bb') {
    $query = "SELECT * FROM bb_usuario WHERE idcliente =" . $_GET['ID_AGENCY'];
    $res = $db->query($query);
    $arr_user = array();
    foreach ($res as $key => $value) {
      $user_mapping = UserMapping::find(array('userbb'=>$value['id']));
      $ismapping = false;
      if($user_mapping) {
         $ismapping = true;
      }
      $arr_user[] = array('id' => $value['id'], 'nombre' => $value['nombre_completo'],
                          'mails' => $value['mails'], 'telefonos' => $value['telefonos'], 'ismapping' => $ismapping);
    }
    echo json_encode($arr_user);
  }
}

if (isset($_GET['save_agency_Mapping'])) {
  if($_GET['sistem'] == 'br') {

    $data = (array)json_decode($_GET['agency']);
    $autorizacion = 0;
    $idcliente = 0;
    $acceso = '';

    $date = new \DateTime('now');
    $date = $date->format('Y-m-d H:i:s');
    // $date = '2017-09-28 06:14:10';
    $idciudad = 34;
    $sentencia = $db->prepare("INSERT INTO bb_cliente(NOMBRE,MAILS,DIRECCION,TELEFONOS,AUTORIZADO_EMISION_AEREO,IDCLIENTE,CODIGO_ACCESO_CLIENTE,CREATED_AT,UPDATED_AT,IDCIUDAD)
                              VALUES (:NOMBRE,:MAILS,:DIRECCION,:TELEFONOS,:AUTORIZADO_EMISION_AEREO,:IDCLIENTE,:CODIGO_ACCESO_CLIENTE,:CREATED_AT,:UPDATED_AT,:IDCIUDAD)");
    $sentencia->bindParam(':NOMBRE', $data['nombre']);
    $sentencia->bindParam(':MAILS', $data['mails']);
    $sentencia->bindParam(':DIRECCION', $data['direccion']);
    $sentencia->bindParam(':TELEFONOS', $data['telefonos']);
    $sentencia->bindParam(':AUTORIZADO_EMISION_AEREO', $autorizacion);
    $sentencia->bindParam(':IDCLIENTE', $idcliente);
    $sentencia->bindParam(':CODIGO_ACCESO_CLIENTE', $acceso);
    $sentencia->bindParam(':CREATED_AT', $date);
    $sentencia->bindParam(':UPDATED_AT', $date);
    $sentencia->bindParam(':IDCIUDAD', $idciudad);
    if($sentencia->execute()) {
      // print_r($sentencia->errorInfo());
      $id_bb = $db->lastInsertId();
      $agency = new AgencyMapping();
      $agency->agencybb = $id_bb;
      $agency->agencybarry = $data['id'];
      $agency->agencyname = $data['nombre'];
      $agency->save();
      echo "Se Guardo Exitosamente";
    }
    else {
      echo "Error Al Insertar";
    }

  }

}

if (isset($_GET['save_user_Mapping'])) {
  if($_GET['sistem'] == 'br') {
    $agency = (array)json_decode($_GET['agency']);
    $date = new \DateTime('now');
    $date = $date->format('Y-m-d H:i:s');
    $remenber_token = '';
    $user_code = '';
    $password = '$2y$10$fh5yyONzOSrBcZQSm4u7YuWS0FIn0H2764Ch8Arul4I3VnI86xIj2';
    $rol = 2;
    $agencymapping = AgencyMapping::find(array('agencybarry' => $agency['agency_id']));
    if($agencymapping) {
      $sentencia = $db->prepare("INSERT INTO bb_usuario(NOMBRE,PASSWORD,NOMBRE_COMPLETO,MAILS,TELEFONOS,ROL,REMEMBER_TOKEN,CREATED_AT,UPDATED_AT,IDCLIENTE,USER_CODE)
                                VALUES (:NOMBRE,:PASSWORD,:NOMBRE_COMPLETO,:MAILS,:TELEFONOS,:ROL,:REMEMBER_TOKEN,:CREATED_AT,:UPDATED_AT,:IDCLIENTE,:USER_CODE)");
      $sentencia->bindParam(':NOMBRE', $agency['username']);
      $sentencia->bindParam(':PASSWORD', $password);
      $sentencia->bindParam(':NOMBRE_COMPLETO', $agency['nombre']);
      $sentencia->bindParam(':MAILS', $agency['mails']);
      $sentencia->bindParam(':TELEFONOS', $agency['telefonos']);
      $sentencia->bindParam(':ROL', $rol);
      $sentencia->bindParam(':REMEMBER_TOKEN', $remenber_token);
      $sentencia->bindParam(':CREATED_AT', $date);
      $sentencia->bindParam(':UPDATED_AT', $date);
      $sentencia->bindParam(':IDCLIENTE', $agencymapping->agencybb);
      $sentencia->bindParam(':USER_CODE', $user_code);
      if($sentencia->execute()) {
        echo "Se Guardo Exitosamente";
      }
      else {
        echo "Error al Guardar";
      }
    }
    else {
      echo "Error al Guardar No Existe Agencia Mapeada";
    }

  }
}

?>
