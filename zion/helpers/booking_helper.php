<?php

function saveManualBooking($p) {
  $localizer  = '';
  $holder     = _post('holder', $p);
  $name       = _post('name', $p);
  $last_name  = _post('last_name', $p);
  $status     = _post('estado_id', $p);
  $book_date  = date('Y-m-d H:i:s');

  if (!isset($p['manual_agency'])) {
    $agency_id = _post('agency_id', $p);
    $agency = '';
  }
  else {
    $agency_id = 0;
    $agency = _post('agency', $p);
  }
  if (!isset($p['manual_agent'])) {
    $user_id = _post('user_id', $p);
    $agent = '';
  }
  else {
    $user_id = 0;
    $agent = _post('agent', $p);
  }

  $operator_id = $p['current_user']['id'];
  $remarks = _post('remarks', $p);
  $operator_remarks = _post('operator_remarks', $p);
  $view_mode = _post('view_mode', $p);
  $package_description = _post('package_description', $p);
  $book = new Booking();
  $book->build($localizer, $status, $holder, $book_date, $user_id, $agency_id, $agent, $agency, $operator_id, $remarks, $operator_remarks, $view_mode, $package_description, $booking_code = 0, '', '', NULL, '', 0, $name, $last_name);
  if(_post('submit', $p) == 'save' && $book->status != ServiceStatus::PENDING && $book->status != ServiceStatus::CANCELLED) {
    $book->confirm_date     = date('Y-m-d H:i:s');
    $book->user_confirm_id  = $p['current_user']['id'];
  }
  else if (_post('submit', $p) == 'update') {
    $book->id = _post('book_id', $p);
    $oldBook = Booking::findById(_post('book_id', $p));
    $book->debit_note   = $oldBook->debit_note;
    $book->book_date    = $oldBook->book_date;
    $book->confirm_date = $oldBook->confirm_date;
    $book->cancel_date  = $oldBook->cancel_date;
    $book->user_confirm_id = $oldBook->user_confirm_id;
    $book->debit_number_conta = $oldBook->debit_number_conta;

    if ($book->debit_note == '' && $book->status != ServiceStatus::PENDING && $book->status != ServiceStatus::CANCELLED) {
      $book->debit_note       = $book->getNextDebitNoteNumber();
      $book->confirm_date     = date('Y-m-d H:i:s');
      $book->user_confirm_id  = $p['current_user']['id'];
    }
    if ($book->cancel_date == "0000-00-00 00:00:00" && $book->status == ServiceStatus::CANCELLED) {
      $book->cancel_date = date('Y-m-d H:i:s');
    }
    //para que la reserva no se procese autmaticamente
    if ($oldBook->operator_id == 0){
      $book->operator_id = 0;
    }
    else if ($oldBook->operator_id != 0) { //para que al actualizar no se cambio el usuario que proceso la reserva
      $book->operator_id = $oldBook->operator_id;
    }
    $book->booking_code = $oldBook->booking_code;
    $book->holder_phone = $oldBook->holder_phone;
    $book->holder_mail  = $oldBook->holder_mail;
    // JSON for Loggin
    $oldBookJSON = $oldBook->bookingToJSON();
  }
  else if (_post('submit', $p) == 'clone') {
    $oldBook = Booking::findById(_post('book_id', $p));
    $oldBook->status = ServiceStatus::ANNULLED;
    $oldBook->save();
    $book->book_date = date('Y-m-d H:i:s');
    $book->debit_note = $oldBook->getCloneDebitNoteNumber();
    $book->confirm_date = date('Y-m-d H:i:s');
    $book->booking_code = $oldBook->getCloneBookingCodeNumber();
    $book->debit_number_conta = $oldBook->debit_number_conta;
  }
  $book->save();

  $services_count = _post('services_count', $p);
  $services_ids = array();
  for ($i = 1; $i <= $services_count; $i++) {
    // Ignore deleted services
    if (!isset($p['type_' . $i])) continue;
    $service = saveManualService($i, $book->id, $p);
    $services_ids[] = $service->id;
  }

  if (_post('submit', $p) == 'update') {
    // Delete the removed services
    foreach ($book->getServices() as $service) {
      if (!in_array($service->id, $services_ids)) {
        $service->delete();
      }
    }

    // Log update
    $bookJSON = $book->bookingToJSON();
    Log::warning('{"prev": ' . $oldBookJSON . ', "next": ' . $bookJSON . '}', 'BOOK UPDATE ' . $book->booking_code);
  }
  return $book;
}

function save_file($booking_id,$file) {
  move_uploaded_file($_FILES['file']['tmp_name'], 'file_voucher/'.$booking_id);
}

function saveManualService($i, $booking_id, $p) {
  $type         = _post('type_' . $i, $p);
  $name         = _post('name_' . $i, $p);
  $category     = _post('category_' . $i, $p);
  $localizer    = _post('localizer_' . $i, $p);
  $rs_id        = '';
  $hotel_code   = '';
  $status       = _post('status_' . $i, $p);
  $destination_code   = _post('destination_code_' . $i, $p);
  $cancellation_date  = dateFormatForDB(_post('cancellation_date_' . $i, $p));
  $cancellation_fee   = 0;
  $checkin      = dateFormatForDB(_post('checkin_' . $i, $p));
  $checkout     = dateFormatForDB(_post('checkout_' . $i, $p));
  $time         = _post("hour_$i") . ':' . _post("mins_$i", $p) . ':00';
  $adult_count  = _post('adult_count_' . $i, $p);
  $child_count  = _post('child_count_' . $i, $p);
  $child_ages   = _post('child_ages_' . $i, $p);
  $net_price    = floatval(_post('net_price_' . $i, $p));
  $price        = floatval(_post('price_' . $i, $p));
  $commission   = floatval(_post("commission_$i", $p));
  if (_post("commission_type_$i", $p) == 'perc') {
    $commission = $price * ($commission / 100);
  }
  $incentive = floatval(_post("incentive_$i", $p));
  if (_post("incentive_type_$i", $p) == 'perc') {
    $incentive = $price * ($incentive / 100);
  }
  $agency_fee   = 0;
  $currency     = _post('currency_' . $i, $p);
  $provider_id  = _post('provider_' . $i, $p);
  $includes     = _post('includes_' . $i, $p);
  $is_manual    = _post('is_manual_' . $i, $p);
  $show_voucher = 0;
  if (_post('show_voucher_' . $i, $p) != '' || _post('type_' . $i, $p) == ServiceType::FLIGHT) {
    $show_voucher = 1;
  }
  $enable_commission_suc  = _post('show_comis_suc_' . $i, $p);
  $commission_suc         = 0;
  if ($enable_commission_suc == 1) {
    $factor_com     = floatval(_post('comis_suc_' . $i, $p));
    $commission_suc = ($factor_com * $price) / 100;
  }
  $serviceDB = new Service();
  $serviceDB->build($type, $hotel_code, $name, $category, $localizer, $rs_id, $status, $destination_code, $cancellation_date, $cancellation_fee,
                  $checkin, $checkout, $time, $adult_count, $child_count, $child_ages, $price, $currency, $booking_id,
                  $provider_id, $net_price, $commission, $agency_fee, $incentive, $includes, $is_manual, $show_voucher,
                  $enable_commission_suc, $commission_suc);

  if (_post('submit', $p) == 'update') {
    if (_post("service_id_$i", $p) != '') {
      //se agrego estas lineas para recuperar los datos anteriores ya que al actualizar eran modificados
      $oldService = Service::findById(_post("service_id_$i", $p));
      if(_post('status_' . $i) == '') {
        $serviceDB->status = $oldService->status;
        $serviceDB->cancellation_fee = $oldService->cancellation_fee;
        // $serviceDB->commission = $oldService->commission;
      }
      $serviceDB->reservation_id  = $oldService->reservation_id;
      $serviceDB->hotel_code      = $oldService->hotel_code;
      $serviceDB->id              = _post("service_id_$i", $p);
    }
  }
  else if (_post('submit', $p) == 'clone') {
    $oldService = Service::findById(_post("service_id_$i", $p));
    if ($oldService) {
      $oldService->status = ServiceStatus::ANNULLED;
      $oldService->save();
      $serviceDB->cancellation_fee = $oldService->cancellation_fee;
      $serviceDB->reservation_id = $oldService->reservation_id;
      $serviceDB->hotel_code = $oldService->hotel_code;
    }
  }

  $serviceDB->save();
  if(_post('submit', $p) == 'clone') {
    if(Options_file(_post("service_id_$i", $p), "search")) {
       copy(Options_file(_post("service_id_$i", $p), "link"), "file_voucher/".$serviceDB->id.".".Options_file(_post("service_id_$i"), 'typefile'));
    }
  }

  if ($serviceDB->type == ServiceType::HOTEL) {
    $rooms_count = _post('room_count_' . $i, $p);
    for ($j = 1; $j <= $rooms_count; $j++) {
      // Ingore deleted rooms
      if (!isset($p['type_' . $i . '_' . $j])) { 
        $roomDB = new HotelRoom();
        $roomDB->id = _post('room_id_' . $i . '_' . $j, $p);
        $roomDB->delete();
        continue;
      }
      saveManualRoom($i, $j, $serviceDB->id, $p);
    }
  }
  else if ($serviceDB->type == ServiceType::TRANSFER) {
    saveManualTransferService($i, $serviceDB->id, $p);
  }

  // if ($_FILES['file_' . $i]['tmp_name'] != "") {
  //   Options_file($serviceDB->id, "delete");
  //   $type = explode('.', $_FILES['file_' . $i]['name']);
  //   move_uploaded_file($_FILES['file_' . $i]['tmp_name'], 'file_voucher/' . $serviceDB->id . "." . $type[1]);
  // }

  return $serviceDB;
}

// function Options_file($id, $options) {
//     $directorio = opendir("http://barrybolivia.com/zion/file_voucher/");__log("entraste1");
//     while ($archivos = readdir($directorio)) { __log("entraste");
//       if (!is_dir($archivos)) {
//         $name = explode('.', $archivos);
//         if($name[0] == 10969){
//           if ($options == 'delete') { unlink("file_voucher/" . $archivos);}
//           if ($options == 'search') { return true; closedir($directorio);}
//           if ($options == 'link') { return "file_voucher/". $archivos; closedir($directorio);}
//           if ($options == 'typefile') { return $name[1]; closedir($directorio);}
//         }
//       }
//     }
//     closedir($directorio);
//     return false;
// }

function Options_file($id, $action) {
  $request = curl_init('http://barrybolivia.com/zion/file_voucher/ReceptionFile.php?action=' . $action . '&service_id=' . $id);
  // $request = curl_init('localhost:8080/barry/zion/file_voucher/ReceptionFile.php?action=' . $action . '&service_id=' . $id);
  curl_setopt($request, CURLOPT_POST, true);
  curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
  $result =  curl_exec($request);
  curl_close($request);
  return $result;
}

function uploadImg() {
  $con = 1;
  $service_count = $_POST['service_count'];
  for ($i = 1; $i <= $service_count; $i++) {
    if (!isset($_POST['service_id_' . $i])) {
      continue;
    }
    $nombre     = explode('.', $_FILES['archivo_' . $i]['name']);
    $extension  = end($nombre);
    $temporal   = $_FILES['archivo_' . $i]['tmp_name'];
    // $request = curl_init('http://localhost:8080/barry/zion/file_voucher/ReceptionFile.php');
    $request = curl_init('http://barrybolivia.com/zion/file_voucher/ReceptionFile.php');
    curl_setopt($request, CURLOPT_POST, true);
    curl_setopt(
        $request,
        CURLOPT_POSTFIELDS,
        array(
          'id'      => $_POST['service_id_' . $i],
          'action'  => 'upload_img',
          'file'    => new CURLFile($_FILES['archivo_' . $i]['tmp_name']),
          'extension' => $extension
        ));
    curl_exec($request);
    curl_close($request);
  }
}

function saveManualRoom($i, $j, $service_id, $p) {
  $type        = _post('type_' . $i . '_' . $j, $p);
  $board       = _post('board_' . $i . '_' . $j, $p);
  $adult_count = _post('adult_count_' . $i . '_' . $j, $p);
  $child_count = _post('child_count_' . $i . '_' . $j, $p);
  $childs_ages = _post('child_ages_' . $i . '_' . $j, $p);
  $price       = _post('price_' . $i . '_' . $j, $p);
  $net_price   = _post('net_price_' . $i . '_' . $j, $p);

  $roomDB = new HotelRoom();
  $roomDB->build($type, $board, $adult_count, $child_count, $childs_ages,
                  $price, $net_price, $service_id);

  if (_post('submit', $p) == 'update' && _post('room_id_' . $i . '_' . $j, $p) != '') {
    $roomDB->id = _post('room_id_' . $i . '_' . $j, $p);
  }
  $roomDB->save();

  return $roomDB;
}

function saveManualTransferService($i, $service_id, $p) {
  $origin       = _post("from_$i", $p);
  $destination  = _post("to_$i", $p);
  $transferType = _post("transfer_type_$i", $p);
  $time_in      = _post("hourIn_$i", $p) . ':' . _post("minsIn_$i", $p);
  $time_out     = _post("hourOut_$i", $p) . ':' . _post("minsOut_$i", $p);
  $flight_in    = _post("flight_in_$i", $p);
  $flight_out   = _post("flight_out_$i", $p);

  $transferDB = new ServiceTransfer();
  $transferDB->build($origin, $destination, $transferType, $time_in, $time_out, $flight_in, $flight_out, $service_id);
  if (_post('submit', $p) == 'update') {
    $serviceTra = ServiceTransfer::find(array('service_id' => $service_id));    
    $transferDB->id = $serviceTra->id;
  }
  $transferDB->save();
}

function Formar_Stados($estado) {
  $html = "";
  $elementos = array(1 => "Confirmado", 2 => "Pendiente", 3 => "Pagado", 4 => "Cancelado");
  $html .= "<option value=" . $estado . " >" . $elementos[$estado] . "</option>";
  foreach ($elementos as $key => $value) {
    if ($estado != $key) {
      $html .= "<option value=" . $key . " >" . $value . "</option>";
    }
  }
  return $html;
}

function getServiceWithNearstCancellation($services) {
  $mayor = '';
  $service = NULL;
  foreach ($services as $value) {
    $diff = date_diff(new DateTime('now'), new \DateTime($value->cancellation_date));
    if ($diff->format('%R%a') < $mayor || empty($mayor)) {
      $service = $value;
      $mayor = $diff->format('%R%a');
    }
  }
  return $service;
}

function getBookNumberOrDebitNumber($book) {
  if (isset($book->debit_note) && $book->debit_note != '') {
    return 'A'. $book->debit_note;
  }
  return $book->booking_code;
}

function getBookingCode($book) {
  return 'R' . $prev = str_pad($book->booking_code, 5, '0', STR_PAD_LEFT);
}

function getDebitNoteNumber($book) {
  if (isset($book->debit_note) && $book->debit_note != '') {
    return 'A'. $book->debit_note;
  }

  return '';
}

function getClonePrevDebitNoteNumber($book) {
  $arr = explode('-', $book->debit_note);
  if (count($arr) < 2 || $arr[1] == '01') {
    return $arr[0];
  }

  $prev_clone = intval($arr[1]);
  $prev = $prev_clone - 1;
  $prev = str_pad($prev, 2, '0', STR_PAD_LEFT);

  return $arr[0] . '-' . $prev;
}

function getCloneDebitNoteNumber($book) {
  $arr = explode('-', $book->debit_note);
  if (count($arr) > 1) {
    $current_clone = intval($arr[1]);
    $clone = $current_clone + 1;
    $clone = str_pad($clone, 2, '0', STR_PAD_LEFT);

    return $arr[0] . '-' . $clone;
  }

  return $book->debit_note . '-01';
}

function getHotelEmail($service) {
  $cuerpo =         "<div style='font-size:17px;color:#3396d1;font-weight:bold;margin-top:7px;'>$service->name</div>";
  $cuerpo .=        "<table border=\"0\" width='100%'>";
  $cuerpo .=          "<tr>";
  $cuerpo .=            "<td>";
  $cuerpo .=                "<table class='tb_information_black'>
                              <tr>
                                <td>
                                  <label>Category: </label>
                                </td>
                                <td style='padding-left:10px;'>
                                  $service->category
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <label>Localizador:</label>
                                </td>
                                <td style='padding-left:10px;'>
                                  $service->localizer
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <label>Destino:</label>
                                </td>
                                <td style='padding-left:10px;'>
                                  $service->destination_code
                                </td>
                              </tr>
                             </table>";
  $cuerpo .=            "</td>";
  $cuerpo .=          "</tr>";
  $cuerpo .=          "<tr>";
  $cuerpo .=            "<td>";
  $cuerpo .=              "<strong>Descripci&oacute;n</strong><br>";
  $cuerpo .=            "</td>";
  $cuerpo .=          "</tr>";
  $cuerpo .=         "</table>";

  $cuerpo .=         "<table border=\"1\" class='tb_information_rooms'>";
  $cuerpo .=            "<tr style='background:#cecece'>";
  $cuerpo .=              "<th>";
  $cuerpo .=                "<strong>CheckIn</strong>";
  $cuerpo .=              "</th>";
  $cuerpo .=              "<th>";
  $cuerpo .=                "<strong>CheckOut</strong>";
  $cuerpo .=              "</th>";
  $cuerpo .=              "<th>";
  $cuerpo .=                "<strong>Habitacion</strong>";
  $cuerpo .=              "</th>";
  $cuerpo .=              "<th>";
  $cuerpo .=                "<strong>Regimen</strong>";
  $cuerpo .=              "</th>";
  $cuerpo .=            "</tr>";

  foreach ($service->getRooms() as $room) {
      $cuerpo .=   "<tr>
                      <td>
                          <label>$service->checkin</label>
                      </td>
                      <td>
                          <label>$service->checkout</label>
                      </td>
                      <td>
                          <label>$room->type</label>
                      </td>
                      <td>
                          <label>$room->board</label>
                      </td>
                    </tr>";
  }

  $cuerpo .=    "</table>";

  return $cuerpo;
}

function getTranferEmail($service) {
  $query = ServiceTransfer::find(array('service_id' =>$service->id));
  $transfer = $query;
  $cuerpo =  "<div style='font-size:17px;color:#3396d1;font-weight:bold;margin-top:7px;'>$service->name</div>";
  $cuerpo .=  "<table border=\"0\" width='100%'>
                <tr>
                  <td>
                    <label>Localizador :</label> $service->localizer<br>
                    <label>Destino :</label> $service->destination_code<br>
                  </td>
                </tr>
                <tr>
                    <td>
                        <strong>Descripci&oacute;n</strong><br>
                    </td>
                </tr>
              </table>
              <table>
                <tr>
                  <td>
                    <strong>Desde: </strong> $transfer->origin → <strong>Hasta :</strong> $transfer->destination<br>
                    <strong>Fecha de Llegada : </strong> $service->checkin → <strong>Nro. de Vuelo : </strong> $transfer->flight_in, <strong>Hora de Salida : </strong> $transfer->time_in
                  </td>
                </tr>
              </table>";
  if ($transfer->type == 'IN_OUT') {
    $cuerpo .= "<table>
                  <tr>
                    <td>
                      <strong>Desde: </strong> $transfer->destination → <strong>Hasta :</strong> $transfer->origin<br>
                      <strong>Fecha de Llegada : </strong> $service->checkout → <strong>Nro. de Vuelo : </strong> $transfer->flight_out, <strong>Hora de Salida : </strong> $transfer->time_out
                    </td>
                  </tr>
                </table>";
  }

  return $cuerpo;
}

function getTicket($service) {
  $adt = $service->adult_count == 1 ? 'Adulto' : 'Adultos';
  $chi = $service->child_count == 1 ? ($service->child_count . 'ni&ntilde;o') : ($service->child_count . "ni&ntilde;os");
  $ch = $service->child_count > 0 ? $chi : '';
  $cuerpo = "<div style='font-size:17px;color:#3396d1;font-weight:bold;margin-top:7px;'>$service->name</div>";
  $cuerpo .= "<table>
              <tr>
                <td>
                  <label>Localizador:</label> $service->localizer<br>
                  <label>Destino:</label> $service->destination_code<br>
                </td>
              </tr>
              <tr>
                  <td>
                      <strong>Descripci&oacute;n</strong><br>
                  </td>
              </tr>
            </table>
            <table>
              <tr>
                <td>
                  $service->adult_count $adt $ch ($service->name)<br>
                </td>
              </tr>
            </table>";
  return $cuerpo;
}

?>
