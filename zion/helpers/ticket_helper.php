<?php

function getTicketValuationDataJSON($ticket, $availableModality, $childAges, $fees = array()) {
  /* antigua version */
  // $data = array(
  //   'requestType' => BookingRequestTypes::TicketValuation,
  //   'availToken'  => $ticket->getAvailToken(),
  //   'adultCount'  => $ticket->getAdultCount(),
  //   'childCount'  => $ticket->getChildCount(),
  //   'modalityCode'=> $availableModality->getCode(),
  //   'ticketCode'  => $ticket->getCode(),
  //   'childAges'   => $childAges,
  //   'provider'    => $ticket->getProvider(),
  //   'fees'        => $fees
  // );
  /* nueva version */
  $data = array(
    'requestType' => BookingRequestTypes::TicketValuation,
    'availToken'  => $ticket->availToken,
    'adultCount'  => $ticket->adultCount,
    'childCount'  => $ticket->childCount,
    'modalityCode'=> $availableModality->code,
    'modeCode'    => $availableModality->modeCode,
    'type'        => $availableModality->type,
    'ticketCode'  => $ticket->code,
    'childAges'   => $childAges,
    'provider'    => $ticket->provider,
    'checkIn'     => $ticket->dateFrom,
    'checkOut'    => $ticket->dateTo,
    'typeCode'    => $availableModality->typeCode
  );
  return json_encode($data);
}