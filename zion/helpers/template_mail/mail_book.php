<?php

// include_once('../../../config/Config.php');
// include_once('../../database/DB.php');
// include_once('../../model/Model.php');
// include_once('../../model/Booking.php');
// include_once('../../model/Service.php');
// include_once('../../model/User.php');
// include_once('../../model/ServiceType.php');
// include_once('../../model/ServiceStatus.php');
// include_once('../../model/HotelRoom.php');
// include_once('../../model/Agency.php');
// include_once('../../model/ServiceTransfer.php');
// include_once('../../helpers/booking_helper.php');
// include_once('../../helpers/application_helper.php');
// include_once('../../../lib/swiftmailer/swift_required.php');
// $book = new Booking();
// $book = $book->findById(7533);

$msg  = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">";
$msg .= "<html xmlns=\"http://www.w3.org/1999/xhtml\">";
$msg .= "<head>";
$msg .= 	"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />";
$msg .= 	"<meta name=\"viewport\" content=\"width=device-width\"/>";
$msg .= 	"<title>ALERTA - Cancelacion de Reserva</title>";
$msg .= 	"<style type=\"text/css\">";
$msg .= 		"body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}";
$msg .= 		"table{border-collapse:collapse;}";
$msg .= 		"img, a img{border:0; outline:none; text-decoration:none;}";
$msg .= 		"h1, h2, h3, h4, h5, h6{margin:0; padding:0;}";
$msg .= 		"p{margin: 1em 0;}";
$msg .= 		".ReadMsgBody{width:100%;} .ExternalClass{width:100%;}";
$msg .= 		".ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%;}";
$msg .= 		"table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;padding:10px;}";
$msg .= 		"#outlook a{padding:0;}";
$msg .= 		"img{-ms-interpolation-mode: bicubic;}";
$msg .= 		"body, table, td, p, a, li, blockquote{-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%;}";
$msg .= 		".flexibleContainerCell{padding-top:20px; padding-Right:20px; padding-Left:20px;}";
$msg .= 		".flexibleImage{height:auto;}";
$msg .= 		".bottomShim{padding-bottom:20px;}";
$msg .= 		".imageContent, .imageContentLast{padding-bottom:20px;}";
$msg .= 		".nestedContainerCell{padding-top:20px; padding-Right:20px; padding-Left:20px;}";
$msg .= 		"body, #bodyTable{background-color:#F5F5F5;}";
$msg .= 		"#bodyCell{padding-top:40px; padding-bottom:40px;}";
$msg .= 		".tb_footer{background-color:#424040;}";
$msg .= 		".tb_information tr td{color: #f7f7f7;padding: 1px 2px;}";
$msg .= 		".tb_information_rooms tr td{padding: 3px 5px;}";
$msg .= 		".tb_information_rooms tr th{padding: 3px 5px;}";
$msg .= 		".tb_information_black tr td{padding: 1px 0px;}";
$msg .=			"#emailBody{background-color:#FFFFFF; border:1px solid #DDDDDD; border-collapse:separate; border-radius:4px;}";
$msg .=			"h1, h2, h3, h4, h5, h6{color:#202020; font-family:Helvetica; font-size:20px; line-height:125%; text-align:Left;}";
$msg .=			".textContent, .textContentLast{color:#404040; font-family:Helvetica; font-size:16px; line-height:125%; text-align:Left; padding-bottom:20px;}";
$msg .=			".status {background-color:#514e4e;color:white;}";
$msg .=			".nestedContainer{background-color:#E5E5E5; border:1px solid #CCCCCC;}";
$msg .=			".nestedContainer{background-color:#E5E5E5; border:1px solid #CCCCCC;}";
$msg .=			".emailButton{background-color:#2C9AB7; border-collapse:separate; border-radius:4px;}";
$msg .=			".buttonContent{color:#FFFFFF; font-family:Helvetica; font-size:18px; font-weight:bold; line-height:100%; padding:15px; text-align:center;}";
$msg .=			".buttonContent a{color:#FFFFFF; display:block; text-decoration:none;}";
$msg .=			".emailCalendar{background-color:#FFFFFF; border:1px solid #CCCCCC;}";
$msg .=			".emailCalendarMonth{background-color:#2C9AB7; color:#FFFFFF; font-family:Helvetica, Arial, sans-serif; font-size:16px; font-weight:bold; padding-top:10px; padding-bottom:10px; text-align:center;}";
$msg .=			".emailCalendarDay{color:#2C9AB7; font-family:Helvetica, Arial, sans-serif; font-size:60px; font-weight:bold; line-height:100%; padding-top:20px; padding-bottom:20px; text-align:center;}";
$msg .=			"@media only screen and (max-width: 480px){";
$msg .=				"body{width:100% !important; min-width:100% !important;}";
$msg .=				"table[id=\"emailBody\"], table[class=\"flexibleContainer\"]{width:100% !important;}";
$msg .=				"img[class=\"flexibleImage\"]{height:auto !important; width:100% !important;}";
$msg .=				"table[class=\"emailButton\"]{width:100% !important;}";
$msg .=				"td[class=\"buttonContent\"]{padding:0 !important;}";
$msg .=				"td[class=\"buttonContent\"] a{padding:15px !important;}";
$msg .=				"td[class=\"textContentLast\"], td[class=\"imageContentLast\"]{padding-top:20px !important;}";
$msg .=				"td[id=\"bodyCell\"]{padding-top:10px !important; padding-Right:10px !important; padding-Left:10px !important;}";
$msg .=			"}";
$msg .= 	"</style>";
$msg .= "</head>";
$msg .= "<body>";
$msg .= 	"<center>";
$msg .= 		"<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" width=\"100%\" id=\"bodyTable\" >";
$msg .= 		  "<tr>";
$msg .= 			 "<td align=\"center\" valign=\"top\" id=\"bodyCell\" style='padding:10px 10px;'>";

$user           = User::findById($book->user_id);
$userFullName   = $user->name . " " . $user->lastname;
$bookNumber     = $book->getBookNumber();
$agency         = Agency::findById($book->agency_id);
$serviceStatus  = ServiceStatus::findById($book->status);
$styleStatus    = 'color:white;background-color:#514e4e;padding:0px 5px';
$user_online = isset($p['current_user']['name']) ? $p['current_user']['name'] : '';// usuario que reservo o confirmo una reserva
if ($book->status == ServiceStatus::CONFIRMED) {
  $styleStatus = 'color:white;background-color:#027e02;padding:0px 5px';
}
else if($book->status == ServiceStatus::CANCELLED) {
  $styleStatus = 'color:white;background-color:#ea1a1a;padding:0px 5px';
}
$global_holder_name = $book->holder;
if (!empty($book->name))
  $global_holder_name = $book->name . ' ' . $book->last_name;
 // <img src='http://boliviabooking.com/boliviabooking/public/images/boliviabooking1.png'>
 // <img src='http://barrybolivia.com/zion/assets/images/resources/agency_logos/1'>
$currentUser = isset($p['current_user']['name']) ? $p['current_user']['name'] : 'Agente';
$confirm = ($book->status == ServiceStatus::CONFIRMED) ? "<tr><td>Nro. Confirmacion:</td><td> " . $book->getDebitNoteNumber() . "</td></tr>" : "";
$head = "<table border=\"1\" width=\"1314\" style='width:1000px;margin-left:5.4pt;border-collapse:collapse;background-color:white;'>";
$head .=  "<tr>";
$head .=    "<td style='padding:15px 10px;'>";
$head .=       "<table border=\"0\" style='width:100%'>";
$head .=          "<tr>";
$head .=            "<td>";
$head .=              "<img src='https://yocounter.com/freelance/public/images/yocounter-logo.png'>";
$head .=            "</td>";
$head .=          "</tr>";
$head .=        "</table>";
$cancelmsg =     "<h2>Cancelaci&oacute;n de Reserva</h2>
                   <div style='background: #323131; font-size: 1px; line-height: 2px;''>&nbsp;</div>
                   <table>
                    <tr>
                      <td>
                        La reserva <strong> " . $book->getDebitNoteNumber() . " ( " . $book->getBookingCode() ." )
                        a nombre de $global_holder_name fue cancelada por " . $currentUser . " / " . $agency->name . "<br><br>";

                        if (isset($cancellation_fee) && $cancellation_fee != 0) {
                          $price = $cancellation_fee / Config::findById(1)->fee;
                          $price = number_format($price, 2);
                          $cancelmsg .= '<br /><strong style="color:red">Esta reserva tiene gastos de cancelaci&oacute;n de: ' . $price . ' ' . $currency . '.</strong>';
                        }
$cancelmsg .= 'Puedes ver la reserva desde:<br><br>';
$cancelmsg .= '<a href="http://boliviabooking.com/hotelesboliviabooking/zion/?controller=booking&action=show&id=' . $book->id . '">';
$cancelmsg .= 'http://boliviabooking.com/hotelesboliviabooking/zion/?controller=booking&action=show&id=' . $book->id . '</a><br>';
$cancelmsg .=           "<div style='font-size:0.9em; color:#999;'>Por favor no respondas a este correo porque es un correo automatizado sólo para notificaciones.</div>
                      </td>
                    </td>
                   </table><br>";
// $cuerpo .= $cancelmsg;
$cuerpo =        "<label>Estimad@:</label> " . ucwords($userFullName) . " - " . ucwords(strtolower($agency->name)) . "<br>";//Pasajero
$cuerpo .=        "<label>Gracias por reservar con <strong>YoCounter</strong></label>";

$cuerpo .=        "<h2>Reservas</h2>";
$cuerpo .=        "<div style='background: #323131; font-size: 1px; line-height: 2px;''>&nbsp;</div>";
$cuerpo .=        "<table border=\"0\">";
$cuerpo .=          "<tr>";
$cuerpo .=            "<td>";
$cuerpo .=              "<table class='tb_information_black'>
                          <tr>
                            <td>
                              Estado de Reserva:
                            </td>
                            <td>
                              <span style='$styleStatus'>$serviceStatus->status</span>
                            </td>
                          </tr>
                          $confirm
                          <tr>
                            <td>
                              Nro. Reserva:
                            </td>
                            <td>
                              $bookNumber
                            </td>
                          </tr>
                          <tr>
                            <td>
                              Fecha Creacion:
                            </td>
                            <td>
                              " . dateFormatFromDB($book->book_date) . "
                            </td>
                          </tr>
                          <tr>
                            <td>
                              Agente:
                            </td>
                            <td>
                              $user_online
                            </td>
                          </tr>
                        </table>";

$cuerpo .=            "</td>";
$cuerpo .=          "</tr>";
$cuerpo .=          "<tr>";
$cuerpo .=            "<td>";
$cuerpo .=              "<strong>Datos del Cliente</strong><br>";
$cuerpo .=              "<label>Nombre:</label> $global_holder_name<br>";
$cuerpo .=            "</td>";
$cuerpo .=          "</tr>";
$cuerpo .=         "</table>";
$cuerpo .=        "<h2>Descripci&oacute;n</h2>";
$cuerpo .=        "<div style='background: #323131; font-size: 1px; line-height: 2px;'>&nbsp;</div>";
$total_price = 0;
$total_agency_fee = 0;
$total_commission = 0;
$currency = '';
$count_service = 0;
foreach ($book->getServices() as  $service) {
  $total_price += $service->price;
  $total_agency_fee += $service->agency_fee;
  $total_commission += $service->commission;
  $currency = $service->currency;
  $count_service++;
  if($service->type == ServiceType::HOTEL) {
    $cuerpo .= getHotelEmail($service);
  } else if ($service->type == ServiceType::TICKET) {
    $cuerpo .= getTicket($service);
  }
  else if ($service->type == ServiceType::TRANSFER) {
    $cuerpo .= getTranferEmail($service);
  }

  if (count($book->getServices()) != 1 && $count_service < count($book->getServices())) {
      $cuerpo .= "<div style='background: #ddd; font-size: 1px; line-height: 2px;margin-top:10px;'>&nbsp;</div>";
  }

}
$precio_total     = number_format($total_price + $total_agency_fee, 2);
$comssion_agencia = number_format($total_commission, 2);
$total_payable    = number_format(round($total_price - $total_commission, 2), 2);
$service_policy   = $book->getServiceWithNearstCancellation();//servicio con la fecha e gastos mas cercano

$cuerpo .=          "<table border=\"0\" width='100%'>";
$cuerpo .=            "<tr>";
$cuerpo .=              "<td>";
$cuerpo .=                "<label style='font-weight:bold;'>Gastos de Cancelaci&oacute;n</label>";
$cuerpo .=                "<div style='background: #323131; font-size: 1px; line-height: 2px;'>&nbsp;</div>";
$cuerpo .=              "</td>";
$cuerpo .=            "</tr>";
$cuerpo .=            "<tr>";
$cuerpo .=              "<td>";
$cuerpo .=                "<strong style='color:#f71010'>Fecha: " . dateFormatFromDB($service_policy->cancellation_date) . "</strong>";
$cuerpo .=              "</td>";
$cuerpo .=            "</tr>";
$cuerpo .=            "<tr>";
$cuerpo .=              "<td>";
$cuerpo .=                "<label style='font-weight:bold;'>Tarifas</label>";
$cuerpo .=                "<div style='background: #323131; font-size: 1px; line-height: 2px;'>&nbsp;</div>";
$cuerpo .=              "</td>";
$cuerpo .=            "</tr>";
$cuerpo .=            "<tr>";
$cuerpo .=              "<td>";
$cuerpo .=                "<table class='tb_information_black'>
                            <tr>
                              <td>
                                <strong>Total a Pagar :</strong>
                              </td>
                              <td style='padding-left:10px'>
                                $precio_total $currency
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <strong>Comision Agencia :</strong>
                              </td>
                              <td style='padding-left:10px'>
                                $comssion_agencia $currency
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <strong>Pagar a Barry :</strong>
                              </td>
                              <td style='padding-left:10px'>
                                $total_payable $currency
                              </td>
                            </tr>
                          </table>";
$cuerpo .=              "</td>";
$cuerpo .=            "</tr>";
$cuerpo .=            "<tr>
                        <td>
                          <strong>Datos Bancarios</strong>
                          <div style='background: #323131; font-size: 1px; line-height: 2px;'>&nbsp;</div>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <strong>Informaci&oacute;n Bancaria:</strong><br>
                          <label>Santa Cruz y Resto de Bolivia: </label> <br>
                          <table style='width: 100%;'>
                            <td>
                              <table class='tb_information_black'>
                                <tr>
                                  <td colspan='2'>
                                    Barry Top Services Bolivia
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <strong>Nit : </strong>
                                  </td>
                                  <td style='padding-left:10px'>
                                    129535024
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <strong>Cuenta en Bolivianos :</strong>
                                  </td>
                                  <td style='padding-left:10px'>
                                    4010893136
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <strong>Cuenta en D&oacute;lares :</strong>
                                  </td>
                                  <td style='padding-left:10px'>
                                    4010893143
                                  </td>
                                </tr>
                              </table>
                            </td>
                            <td>
                              <img src='http://viacontours.com/air/public/images/bmsc.png' width='120' height='50' style='border:1px'>
                            </td>
                            <td>
                              <div style='font-weight: bold;font-size: 20px;'>Enviar Comprobante de pago a: <br> admincontable.bo@barrybolivia.com<br>ventas@barrybolivia.com</div>
                            </td>
                          </table>
                          <small>Nota: Para deposito de dolares a bolivianos el tipo de cambio es 6.97</small><br>
                          <small>Nota: Para deposito de bolivianos a dolares el tipo de cambio es 6.85</small>
                        </td>
                      </tr>";
$cuerpo .=          "</table>";

$cuerpo .=        "<div style='text-align:center;font-weight: bold;font-size: 20px;margin-top: 13px;'>BoliviaBooking agradece por la confianza!!!</div>";
$cuerpo .=    "</td>";
$cuerpo .=  "</tr>";
$cuerpo .=  "<tr>";
$cuerpo .=    "<td class='tb_footer'>";
$cuerpo .=      "<table>
                  <tr>
                    <td>
                      <table class='tb_information'>
                       <tr>
                         <td>
                           <strong>Direcci&oacute;n :</strong>
                         </td>
                         <td>
                           Barrio Equipetrol, Calle 9 Este Nro. 5
                         </td>
                       </tr>
                       <tr>
                         <td>
                           <strong>Email :</strong>
                         </td>
                         <td>
                           <a href='mailto:ventas@barrybolivia.com' style='color:white;'>ventas@barrybolivia.com</a>
                         </td>
                       </tr>
                     </table>
                    </td>
                    <td>
                      <table class='tb_information'>
                        <tr>
                         <td>
                           <strong>Telefono :</strong>
                         </td>
                         <td>
                           +591 (3) 367135
                         </td>
                       </tr>
                       <tr>
                         <td>
                           <strong>Tel. Emergencia :</strong>
                         </td>
                         <td>
                           +591 72122266 (Fuera de Horario de Oficina)
                         </td>
                       </tr>
                     </table>
                    </td>
                  <tr>
                 </table>";
$footer =    "</td>";
$footer .=  "</tr>";
$footer .= "</table>";
if ($book->status == ServiceStatus::CANCELLED) {
  $msg .= $head;
  $msg .= $cancelmsg;
  $msg .= $footer;
}
else {
  $msg .= $head;
  $msg .= $cuerpo;
  $msg .= $footer;
}


$msg .=         "</td>";
$msg .=      "</tr>";
$msg .=      "</table>";
$msg .=    "</center>";
$msg .=   "</body>";
// echo $msg;
// $to[] = 'ysabel.claure@barrybolivia.com';
// $to[] = 'omar.flores@barrybolivia.com';
// $to[] = 'reny@viacontours.com';
// $to[] = 'omarflores9855@gmail.com';
// $to[] = 'omarflores74@hotmail.com';
// $to[] = 'cinthya.claros@boliviabooking.com';
// $subject = 'Reserva Omar Flores';

// $transport = Swift_SmtpTransport::newInstance('mail.boliviabooking.com', 25)
//         ->setUsername('mailing@boliviabooking.com')
//         ->setPassword('mailing!@#')
//         ;
//
// $mailer = Swift_Mailer::newInstance($transport);
//
// $message = Swift_Message::newInstance($subject)
//   ->setFrom(array('reservas@boliviabooking.com' => 'Reservas Boliviaboooking'))
//   ->setTo($to)
//   ->setBody($msg, 'text/html');
// $result = $mailer->send($message);

?>
