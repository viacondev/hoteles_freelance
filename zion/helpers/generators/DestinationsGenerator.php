<?php

$db = new \PDO("mysql:host=localhost;dbname=barrybol_hotelbeds4;charset=utf8", 'root', 'root');

$dest_query = "SELECT c.NAME as cname, d.DESTINATIONCODE, di.NAME as dname
                  FROM COUNTRY_IDS as c, DESTINATIONS as d, DESTINATION_IDS as di
                  WHERE c.LANGUAGECODE = 'CAS' AND c.COUNTRYCODE = d.COUNTRYCODE AND d.DESTINATIONCODE = di.DESTINATIONCODE AND di.LANGUAGECODE = 'CAS'";
$dest_res = $db->query($dest_query);

/*
 * Zones will not be included in the meanwhile but with an extra combobox in the search form
 *
$zones_query = "SELECT z.ZONENAME, d.NAME as dname, c.NAME as cname
                  FROM ZONES as z, DESTINATION_IDS as d, COUNTRY_IDS as c, DESTINATIONS as dd
                  WHERE z.DESTINATIONCODE = d.DESTINATIONCODE AND d.LANGUAGECODE = 'CAS'
                  AND d.DESTINATIONCODE = dd.DESTINATIONCODE AND dd.COUNTRYCODE = c.COUNTRYCODE
                  AND c.LANGUAGECODE = 'CAS' AND (c.COUNTRYCODE = 'US') AND z.DESTINATIONCODE = 'MIA'";
$zones_res = $db->query($zones_query);
*/

$json = "dests=[";
foreach ($dest_res as $row) {
  $json .= '{"c":"' . $row['DESTINATIONCODE'] . '","label":"' . $row['dname'] . ' (' . $row['DESTINATIONCODE'] . ')' . ', ' . ucwords(strtolower($row['cname'])) . '"},';
}

// Remove the last ','
$json = substr($json, 0, -1);

$json .= "];";

$fp = fopen('destinations_list.js', 'w');
fwrite($fp, $json);
fclose($fp);

?>