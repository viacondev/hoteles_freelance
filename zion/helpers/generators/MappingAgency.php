<?
    ini_set('display_errors', 1);
    $db = new \PDO("mysql:host=23.91.64.139;dbname=barrybol_zion_engine", 'barrybol_zion', 'zionshopdbpwd');
    $db2 = new \PDO("mysql:host=23.91.64.139;dbname=barrybol_barry", 'barrybol_zion', 'zionshopdbpwd');

    // $query = "SELECT a.id, a.name, a.address, a.phone, a.mail, a.logo, c.id as ciudad_id  
    //             FROM Agency a, Ciudad c WHERE a.ciudad_id = c.id AND a.idcliente = 0 AND a.status = 1 
    //             AND a.id IN (566)";
    
    $agencys = $db->query($query);
    foreach ($agencys as $key => $agency) {
        // 28 CBB, 30 LPB, 34 SRZ
        $arrCiudad      = array(28 => 28,30 => 28,34 => 32);
        $id             = $agency['id'];
        $trato_cliente  = 'Ag.';
        $name           = $agency['name'];
        $address        = $agency['address'];
        $phone          = $agency['phone'];
        $mail           = $agency['mail'];
        $tipo_cliente   = 3;
        $idcentro_costo = 3;
        $c_id           = $agency['ciudad_id'];
        $ciudad_id      = $arrCiudad[$c_id];
        $logo           = $agency['logo'];

        

        $zion_query = $db2->prepare("INSERT INTO clientes (TRATO_CLIENTE, NOMBRE_CLIENTE, DIRECCION, CIUDAD_IDCIUDAD, TIPO_CLIENTE, IDCENTRO_COSTO) 
                                        VALUES (:TRATO_CLIENTE, :NOMBRE_CLIENTE, :DIRECCION, :CIUDAD_IDCIUDAD, :TIPO_CLIENTE, :IDCENTRO_COSTO)");
        $zion_query->bindParam(':TRATO_CLIENTE', $trato_cliente);
        $zion_query->bindParam(':NOMBRE_CLIENTE', $name);
        $zion_query->bindParam(':DIRECCION', $address);
        $zion_query->bindParam(':CIUDAD_IDCIUDAD', $ciudad_id);
        $zion_query->bindParam(':TIPO_CLIENTE', $tipo_cliente);
        $zion_query->bindParam(':IDCENTRO_COSTO', $idcentro_costo);

    if($zion_query->execute()) {
        echo "Se Inserto " . $name . " en Clientes<br/>";
        $id_cliente = $db2->lastInsertId();

        $users = saveUsersAgency($id, $db, $db2, $id_cliente, $ciudad_id);
        
        $query = "INSERT INTO clientes_agencias (IDCLIENTES, TELEFONO, CORREO, FOTO_LOGO)
                            VALUES (:IDCLIENTES, :TELEFONO, :CORREO, :FOTO_LOGO)";

        $z_query = $db2->prepare($query);
        $z_query->bindParam(':IDCLIENTES', $id_cliente);
        $z_query->bindParam(':TELEFONO', $phone);
        $z_query->bindParam(':CORREO', $mail);
        $z_query->bindParam(':FOTO_LOGO', $logo);
        
        if($z_query->execute()) {
            $idcliente = $id_cliente;
            echo "Se Inserto " . $name . " en Clientes Agencia<br/>";
            $queryUpdate = "UPDATE agency SET IDCLIENTE=? WHERE ID=?";
            $q = $db->prepare($queryUpdate);
        $q->execute([$idcliente, $id]);
        echo "Se Actualizo " . $name . "<br/>";
        }
        else {
            echo "ERROR AL INSERTAR CLIENTES " . $name . " Agencia</br>";
            print_r($z_query->errorInfo());
            echo "ERROR AL INSERTAR Clientes Agencia<br>";
        }
    }
    else {
        echo "ERROR AL INSERTAR CLIENTES " . $name . " Agencia</br>";
        print_r($zion_query->errorInfo());
        echo "ERROR AL INSERTAR<br>";
    }
        // print_r($agency);
        echo "<br/><br/>";
    }

    function saveUsersAgency($id, $db, $db2, $idCliente , $ciudad_id) {
        //$id =  idAgencia Barry
        // $idcliente = idAgecia Contable
        $query = "SELECT * FROM User WHERE agency_id = $id";
        
        $res = $db->query($query);
        foreach ($res as $user) {
            $idUser     = $user['id'];
            $name       = $user['name'];
            $lastname   = $user['lastname'];
            $phone      = $user['phone'];
            $email      = $user['mail'];
            $trato_cliente   = 'Lic.';
            $tipo_cliente    = 1;
            $ciudad_idciudad = $ciudad_id;
            $idcentro_costo  = 1;
            $empleado        = 1;
            $cargo_empleado  = $user['role'];
            
            $queryCliente               = "INSERT INTO clientes (TRATO_CLIENTE, NOMBRE_CLIENTE, APELLIDO_CLIENTE, CIUDAD_IDCIUDAD, TIPO_CLIENTE, IDCENTRO_COSTO) 
                                                            VALUES (:TRATO_CLIENTE, :NOMBRE_CLIENTE, :APELLIDO_CLIENTE, :CIUDAD_IDCIUDAD, :TIPO_CLIENTE, :IDCENTRO_COSTO)";
            $zion_query = $db2->prepare($queryCliente);
            $zion_query->bindParam(':TRATO_CLIENTE', $trato_cliente);
            $zion_query->bindParam(':NOMBRE_CLIENTE', $name);
            $zion_query->bindParam(':APELLIDO_CLIENTE', $lastname);
            $zion_query->bindParam(':CIUDAD_IDCIUDAD', $ciudad_idciudad);
            $zion_query->bindParam(':TIPO_CLIENTE', $tipo_cliente);
            $zion_query->bindParam(':IDCENTRO_COSTO', $idcentro_costo);
            if ($zion_query->execute()) {
                echo "Se Guardo el Cliente " . $name . "<br>";
                $id_cliente_user = $db2->lastInsertId();
                $queryClienteNatural    = "INSERT INTO clientes_naturales (IDCLIENTES, TRATO_CLIENTE, APELLIDO_CLIENTE, EMPLEADO, CARGO_EMPLEADO) 
                                                                VALUES (:IDCLIENTES, :TRATO_CLIENTE, :APELLIDO_CLIENTE, :EMPLEADO, :CARGO_EMPLEADO)";
                $zi_query = $db2->prepare($queryClienteNatural);
                $zi_query->bindParam(':IDCLIENTES', $id_cliente_user);
                $zi_query->bindParam(':TRATO_CLIENTE', $trato_cliente);
                $zi_query->bindParam(':APELLIDO_CLIENTE', $lastname);
                $zi_query->bindParam(':EMPLEADO', $empleado);
                $zi_query->bindParam(':CARGO_EMPLEADO', $cargo_empleado);
                if ($zi_query->execute()) {
                    echo "se Guardo el Clinete Natural " . $name . "<br>";
                    $cargo_idcargo  = 3;
                    $action         = 1;

                    $queryClienteContacto = "INSERT INTO clientes_has_clientes_contacto (IDCLIENTES, IDCLIENTES_RELACION, ACCION_CLIENTE, CARGO_IDCARGO)                                                            VALUES (:IDCLIENTES, :IDCLIENTES_RELACION, :ACCION_CLIENTE, :CARGO_IDCARGO)";
                    $zio_query = $db2->prepare($queryClienteContacto);
                    $zio_query->bindParam(':IDCLIENTES', $idCliente);
                    $zio_query->bindParam(':IDCLIENTES_RELACION', $id_cliente_user);
                    $zio_query->bindParam(':ACCION_CLIENTE', $action);
                    $zio_query->bindParam(':CARGO_IDCARGO', $cargo_idcargo);
                    if ($zio_query->execute()) {
                        echo "se Guardo el CLIENTE HAS CONTACTO " . $name . "<br>";
                        $id_cliente_contable = $id_cliente_user;

                        $queryUpdateCliente = "UPDATE User SET id_cliente_contable = " . $id_cliente_contable . " WHERE id = " . $idUser;
                        $q = $db->prepare($queryUpdateCliente);
                        $q->execute();
                        // print_r($q->errorInfo());
                        echo "<br>";
                    }
                    else {
                        echo "ERROR al Guardar CLIENTE HAS CONTACTO " . $name . "<br>";
                        print_r($zio_query->errorInfo());
                        echo "<br>";
                        
                    }
                }
                else {
                    echo "ERROR al Guardar el Cliente Natural " . $name . "<br>";
                    print_r($zi_query->errorInfo());
                    echo "<br>";
                }

        }
        else {
            echo "ERROR Al Guardar el Cliente " . $name . "<br>";
            print_r($zion_query->errorInfo());
                echo "<br>";
        }
            
            $queryClienteContacto   = "";
            // print_r($user);
            echo "<br>";
        }
    }

?>