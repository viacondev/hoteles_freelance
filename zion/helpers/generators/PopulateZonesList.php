<?php
/**
 * Populates the Zones table on the zion_engine DB, based on the hotelbeds DB.
 */

$hb_db = new \PDO("mysql:host=localhost;dbname=hotelbeds", 'root', 'root');
$zion_db = new \PDO("mysql:host=localhost;dbname=zion_engine", 'root', 'root');

$hb_query = "SELECT * FROM ZONES";
$hb_res = $hb_db->query($hb_query);

foreach ($hb_res as $row) {
  $zion_query = $zion_db->prepare("INSERT INTO Zone (zone_code, destination_code, zone_name) VALUES (:zcode, :dcode, :name)");
  $zion_query->bindParam(':zcode', $row['ZONECODE']);
  $zion_query->bindParam(':dcode', $row['DESTINATIONCODE']);
  $zion_query->bindParam(':name', $row['ZONENAME']);
  $zion_query->execute();
}

?>
