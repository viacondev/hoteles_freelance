<?php
/**
 * Populates the Hotels table on the zion_engine DB, based on the hotelbeds DB.
 */

$hb_db = new PDO("mysql:host=localhost;dbname=barrybol_hotelbeds;charset=utf8", 'barrybol_zion', 'zionshopdbpwd');
$zion_db = new PDO("mysql:host=localhost;dbname=barrybol_zion_engine;charset=utf8", 'barrybol_zion', 'zionshopdbpwd');

$start = 0;
$counter = 0;
while (true) {
  $query =
    "SELECT
        h.HOTELCODE as hotel_code, h.NAME as hotel_name,
        z.ZONECODE as zone_code, z.ZONENAME as zone_name,
        di.DESTINATIONCODE as destination_code, di.NAME as destination_name,
        c.COUNTRYCODE as country_code, c.NAME as country_name
    FROM HOTELS as h, ZONES as z, DESTINATION_IDS as di, DESTINATIONS as d, COUNTRY_IDS as c
    WHERE
    di.LANGUAGECODE = 'CAS' AND c.LANGUAGECODE = 'CAS' AND 
    h.ZONECODE = z.ZONECODE AND h.DESTINATIONCODE = di.DESTINATIONCODE AND z.DESTINATIONCODE = di.DESTINATIONCODE AND
    di.DESTINATIONCODE = d.DESTINATIONCODE AND d.COUNTRYCODE = c.COUNTRYCODE
    LIMIT $start, 1000";

  $res = $hb_db->query($query);

  foreach ($res as $row) {
    $zion_query = $zion_db->prepare("INSERT INTO Hotel (hotel_code, hotel_name, zone_code, zone_name, destination_code, destination_name, country_code, country_name) VALUES (:hotel_code, :hotel_name, :zone_code, :zone_name, :destination_code, :destination_name, :country_code, :country_name)");
    $zion_query->bindParam(':hotel_code', $row['hotel_code']);
    $zion_query->bindParam(':hotel_name', $row['hotel_name']);
    $zion_query->bindParam(':zone_code', $row['zone_code']);
    $zion_query->bindParam(':zone_name', $row['zone_name']);
    $zion_query->bindParam(':destination_code', $row['destination_code']);
    $zion_query->bindParam(':destination_name', $row['destination_name']);
    $zion_query->bindParam(':country_code', $row['country_code']);
    $zion_query->bindParam(':country_name', $row['country_name']);
    $zion_query->execute();
  }

  echo $start . ' (' . $res->rowCount() . ') <br />';
  $start += 1000;
  $counter++;
  if ($res->rowCount() == 0 || $counter > 150) {
    break;
  }
}

?>