<?php

$db = new \PDO("mysql:host=localhost;dbname=barrybol_hotelbeds", 'barrybol_zion', 'zionshopdbpwd');

$dest_query = "SELECT COUNTRYCODE, NAME FROM COUNTRY_IDS WHERE LANGUAGECODE = 'CAS'";
$dest_res = $db->query($dest_query);

$json = "countries_list=[";
foreach ($dest_res as $row) {
  $json .= '{"c":"' . $row['COUNTRYCODE'] . '","n":"' . $row['NAME'] . '"},';
}

// Remove the last ','
$json = substr($json, 0, -1);
$json .= "];";

$fp = fopen('countries_list.js', 'w');
fwrite($fp, $json);
fclose($fp);

?>