<?
	ini_set('display_errors', 1);
	$db 	= new \PDO("mysql:host=localhost;dbname=barrybol_zion_engine", 'barrybol_zion', 'zionshopdbpwd');
	$db2 	= new \PDO("mysql:host=localhost;dbname=barrybol_barry", 'barrybol_zion', 'zionshopdbpwd');
	$query = "SELECT * FROM Provider";
	$res   = $db->query($query);
	
	foreach ($res as $provider) {
		$id 		= $provider['id'];
		$nombre_proveedor 	= $provider['name'];
		$estado 			= 1;
		$forma_pago	 		= 1;
		$idplan_cuentas 	= 0;
		$zion_query = $db2->prepare("INSERT INTO proveedor (nombre_proveedor, estado, forma_pago, idplan_cuentas) 
										VALUES (:nombre_proveedor, :estado, :forma_pago, :idplan_cuentas)");
      	$zion_query->bindParam(':nombre_proveedor', $nombre_proveedor);
      	$zion_query->bindParam(':estado', $estado);
      	$zion_query->bindParam(':forma_pago', $forma_pago);
      	$zion_query->bindParam(':idplan_cuentas', $idplan_cuentas);

      	if($zion_query->execute()) {
      		echo "Se Inserto Correctamente<br>";
      		$id_proveedor_contable = $db2->lastInsertId();
      		$queryUpdate = "UPDATE Provider SET id_proveedor_contable=? WHERE id=?";

      		$q = $db->prepare($queryUpdate);
      		$q->execute([$id_proveedor_contable, $id]);
      		// print_r($q->errorInfo());
      		echo $id_proveedor_contable . "<br/>";
      	}
      	else {
      		print_r($zion_query->errorInfo());
      		echo "<br>";
      		echo "Error al Insertar " . $provider['name'] . "<br/>";
      	}
		// print_r($provider);
		echo "<br>";
	}
?>