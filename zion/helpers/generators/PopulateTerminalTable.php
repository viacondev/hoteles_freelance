<?php
/**
 * Populates the Terminal table on the zion_engine DB, based on the hotelbeds DB.
 */

$hb_db = new \PDO("mysql:host=localhost;dbname=barrybol_hotelbeds", 'barrybol_zion', 'zionshopdbpwd');
$zion_db = new \PDO("mysql:host=localhost;dbname=barrybol_zion_engine", 'barrybol_zion', 'zionshopdbpwd');

$hb_query = "SELECT distinct tt.TERMINALCODE, tz.NAME, tt.DESTINATIONCODE
              FROM TRANSFER_ZONE_DESCRIPTION tz, TERMINAL_TRANSFER_ZONE tt 
              WHERE tz.LANGUAGECODE = 'CAS' AND tt.ZONETRANSFERTERMINAL = tz.ZONETRANSFER";
$hb_res = $hb_db->query($hb_query);

$i = 0;
foreach ($hb_res as $row) {
  $zion_query = $zion_db->prepare("INSERT INTO Terminal (code, name, destination_code) VALUES (:code, :name, :dest)");
  $zion_query->bindParam(':code', $row['TERMINALCODE']);
  $zion_query->bindParam(':name', $row['NAME']);
  $zion_query->bindParam(':dest', $row['DESTINATIONCODE']);
  $zion_query->execute();
}

?>