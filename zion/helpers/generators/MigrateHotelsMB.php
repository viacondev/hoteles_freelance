<?
  ini_set('MAX_EXECUTION_TIME', -1);
  set_time_limit(1000000);
  $zion_db = new PDO("mysql:host=50.23.240.133;dbname=barrybol_zion_engine;charset=utf8", 'barrybol_zion', 'zionshopdbpwd');

  $fp = fopen("AccommodationExtendedInfo.json", "r");
  $inicio = $_GET['inicio'];
  $fin    = $_GET['fin'];
  $con = 0;
  // while(!feof($fp) && $inicio < $fin) {
  while(!feof($fp)) {
    $con++;
    $linea          = fgets($fp);
    // if ($con <= $inicio) {
    //   continue;
    // }

    $hotel          = json_decode($linea);
    if (isset($hotel->Code)) {
      $hotel_code = $hotel->Code;
    }
    else {
      // print_r($linea);
    }
    // $hotel_code     = isset($hotel->Code) ? $hotel->Code: ;
    $hotel_name     = isset($hotel->Name) ? $hotel->Name : '';
    $zone_code      = isset($hotel->Location->Zone->Code) ? $hotel->Location->Zone->Code : '';
    $zone_name      = isset($hotel->Location->Zone->Name) ? $hotel->Location->Zone->Name : '';
    $destination_code = isset($hotel->Location->Town) ? $hotel->Location->Town : '';
    $destination_name = '';
    $country_code     = isset($hotel->Location->CountryIsoCode) ? $hotel->Location->CountryIsoCode : '';
    $country_name     = '';
    $longitude        = isset($hotel->Location->Coordinates->Longitude) ? $hotel->Location->Coordinates->Longitude : '';
    $latitude         = isset($hotel->Location->Coordinates->Latitude) ? $hotel->Location->Coordinates->Latitude : '';
    $address          = isset($hotel->Location->Address) ? $hotel->Location->Address : '';
    $categoryCode     = isset($hotel->Category->Code) ? $hotel->Category->Code : '';
    $hotelDescription = isset($hotel->ExtendedInformation->Descriptions[0]->Text) ? $hotel->ExtendedInformation->Descriptions[0]->Text : '';
    $images           = isset($hotel->ExtendedInformation->Images) ? json_encode($hotel->ExtendedInformation->Images) : "";
    $sql = "SELECT * FROM HotelMb WHERE hotel_code = '$hotel_code'";
    $result = $zion_db->query($sql);
    if (count($result->fetchAll()) > 0) {
      // echo "<pre>";
      // echo $hotel_code . " " . $hotel_name . " Existe el hotel <br>";
      // echo "posicion : " . $con;
      // echo "</pre>";
    }
    else {
      $zion_query = $zion_db->prepare("INSERT INTO HotelMb (hotel_code, hotel_name, zone_code, zone_name, destination_code, destination_name, country_code, country_name, longitude, latitude, address, category_code, hotel_description, images) VALUES (:hotel_code, :hotel_name, :zone_code, :zone_name, :destination_code, :destination_name, :country_code, :country_name, :longitude, :latitude, :address, :category_code, :hotel_description, :images)");
      $zion_query->bindParam(':hotel_code', $hotel_code);
      $zion_query->bindParam(':hotel_name', $hotel_name);
      $zion_query->bindParam(':zone_code', $zone_code);
      $zion_query->bindParam(':zone_name', $zone_name);
      $zion_query->bindParam(':destination_code', $destination_code);
      $zion_query->bindParam(':destination_name', $destination_name);
      $zion_query->bindParam(':country_code', $country_code);
      $zion_query->bindParam(':country_name', $country_name);
      $zion_query->bindParam(':longitude', $longitude);
      $zion_query->bindParam(':latitude', $latitude);
      $zion_query->bindParam(':address', $address);
      $zion_query->bindParam(':category_code', $categoryCode);
      $zion_query->bindParam(':hotel_description', $hotelDescription);
      $zion_query->bindParam(':images', $images);
      if($zion_query->execute()) {
        echo "<pre>";
        echo $hotel_code . " " . $hotel_name . " Guardado con Exito <br>";
        echo "posicion : " . $con;
        echo "</pre>";
      }
      else {
        echo "<pre>";
        echo $hotel_code . " " . $hotel_name . " Error al guardar hotel ";
        print_r($zion_query->errorInfo());
        echo "</pre>";
      }
    }



    // $arrhotels = array('MTH0041424', 'MTH0039248', 'MTH0186764', 'MTH0175645', 'MTH0156131');
    // if (in_array($hotel_code, $arrhotels)) {
    //   echo "<pre>";
    //   echo $hotel_code . " " . $hotel_name . " Encontrado <br>";
    //   echo "posicion : " . $con;
    //   echo "</pre>";
    //   if($zion_query->execute()) {
    //     echo "<pre>";
    //   //   // print_r($hotel);
    //     echo $hotel_code . " " . $hotel_name . " Guardado con Exito <br>";
    //     echo "posicion : " . $con;
    //     echo "</pre>";
    //   }
    //   else {
    //     echo "<pre>";
    //     echo $hotel_code . " " . $hotel_name . " Error al guardar hotel ";
    //     print_r($zion_query->errorInfo());
    //     echo "</pre>";
    //   }
    // }
    // echo "<pre>";print_r(json_decode($linea)); echo "</pre><br />";
    $inicio++;
  }
  fclose($fp);
?>
