<?php

include_once('../../../config/Config.php');
include_once('../../database/DB.php');
include_once('../../model/Model.php');
include_once('../../model/User.php');
include_once('../../model/Log.php');

$logs = Log::findByQuery("SELECT * FROM Log WHERE action LIKE 'BOOK UPDATE%'");

foreach ($logs as $log) {
  $json = (array) json_decode($log->msg);
  $prev = (array) $json['prev'];
  $next = (array) $json['next'];

  echo '<table border="1" style="width: 100%"><tr>';

  echo '<td  style="width: 40%; vertical-align: top;"><table border="1" style="width: 100%">';
  echo '<th>' . $log->date . '</th>';
  echo '<th>' . $log->action . '</th>';
  echo '<th>' . User::findById($log->user_id)->username . '</th>';
  foreach ((array) $prev as $key => $value) {
    if ($key == 'services') continue;

    $nextValue = isset($next[$key]) ? $next[$key] : '';
    echo "<tr>";
    $style = $value != $nextValue ? 'style="background-color: yellow;"' : '';
    echo "<td $style><strong>$key</strong></td>";
    echo "<td $style>$value</td>";
    echo "<td $style>" . $nextValue . "</td>";
    echo "</tr>";
  }
  echo '</table></td>';

  echo '<td style="width: 60%">';
  $prevServices = (array) $prev['services'];
  $nextServices = (array) $next['services'];

  $reachedServices = array();
  foreach ($prevServices as $prevService) {
    $prevService = (array) $prevService;
    $reachedServices[] = $prevService['id'];

    $nextService = NULL;
    foreach ($nextServices as $service) {
      $service = (array) $service;
      if ($service['id'] == $prevService['id']) {
        $nextService = $service;
        break;
      }
    }

    servicesTable($prevService, $nextService);
  }

  foreach ($nextServices as $service) {
    $service = (array) $service;
    if (in_array($service['id'], $reachedServices)) {
      continue;
    }
    servicesTableNoPrev($service);
  }

  echo '</td></tr>';
  echo '<tr><td><hr /><hr /></td><td><hr /><hr /></td></tr>';
}
echo '</table>';

function servicesTable($prev, $next) {
  echo '<table border="1" style="width: 100%">';
  foreach ((array) $prev as $key => $value) {
    $nextValue = isset($next) && isset($next[$key]) ? $next[$key] : 'xxx';
    echo "<tr>";
    $style = $value != $nextValue ? 'style="background-color: yellow;"' : '';
    echo "<td $style><strong>$key</strong></td>";
    echo "<td $style>$value</td>";
    echo "<td $style>" . $nextValue . "</td>";
    echo "</tr>";
  }
  echo '</table>';
}

function servicesTableNoPrev($next) {
  echo '<table border="1" style="width: 100%">';
  foreach ((array) $prev as $key => $value) {
    $nextValue = isset($next) && isset($next[$key]) ? $next[$key] : 'xxx';
    echo "<tr>";
    $style = $value != $nextValue ? 'style="background-color: yellow;"' : '';
    echo "<td $style><strong>$key</strong></td>";
    echo "<td $style>$value</td>";
    echo "<td $style>" . $nextValue . "</td>";
    echo "</tr>";
  }
  echo '</table>';
}

// var_dump($logs);

?>
