<?php

include_once('../../../config/Config.php');
include_once('../../database/DB.php');
include_once('../../model/Model.php');
include_once('../../model/User.php');
include_once('../../model/Agency.php');
include_once('../../model/Profiling.php');

session_start();

$search = isset($_GET['search']);
$action = isset($_GET['action']) ? $_GET['action'] : '';
$origin = isset($_GET['origin']) ? $_GET['origin'] : '';

echo '<form>';
// Action
$actions = Profiling::execQuery("SELECT DISTINCT action FROM Profiling");
echo '<select name="action">';
foreach ($actions as $log) {
  if ($action == $log['action']) {
    echo '<option selected>' . $log['action'] . '</option>';
  }
  else {
    echo '<option>' . $log['action'] . '</option>';
  }
}
echo '</select>';

// Origin
$origins = Profiling::execQuery("SELECT DISTINCT origin FROM Profiling");
echo '<select name="origin">';
echo '<option value="">--Origen--</option>';
foreach ($origins as $log) {
  if ($origin == $log['origin']) {
    echo '<option selected>' . $log['origin'] . '</option>';
  }
  else {
    echo '<option>' . $log['origin'] . '</option>';
  }
}
echo '</select>';

echo '<input type="submit" name="search" value="Search">';
echo '<input type="checkbox" name="order" value="1"> Ordenar';
echo ' &nbsp; <input type="checkbox" name="today" value="1" checked> Today';
echo '</form>';

if ($search) {
  $orderBy = '';
  if (isset($_GET['order'])) {
    $orderBy = 'ORDER BY time DESC';
  }
  $originSql = '';
  if ($origin != '') {
    $originSql = "AND origin='$origin'";
  }
  $today = '';
  if (isset($_GET['today'])) {
    $today = 'AND init > CURDATE()';
  }

  $profiles = Profiling::findByQuery("SELECT * FROM Profiling WHERE action='$action' $originSql $today $orderBy");
  $max = Profiling::execQuery("SELECT max(time) FROM Profiling WHERE action='$action' $originSql $today");
  $min = Profiling::execQuery("SELECT min(time) FROM Profiling WHERE action='$action' $originSql $today");
  $avg = Profiling::execQuery("SELECT avg(time) FROM Profiling WHERE action='$action' $originSql $today");
  $count = Profiling::execQuery("SELECT count(id) FROM Profiling WHERE action='$action' $originSql $today");

  echo '<strong>Count:</strong> ' . $count[0][0] . ' / ';
  echo '<strong>Min:</strong> ' . $min[0][0] . ' / ';
  echo '<strong>Max:</strong> ' . $max[0][0] . ' / ';
  echo '<strong>Avg:</strong> ' . round($avg[0][0]);

  echo '<table border="1" style="">';
  echo '<tr>';
  echo '<th>Action</th>';
  echo '<th>Init / End</th>';
  echo '<th> Origin </th>';
  echo '<th> Time </th>';
  echo '<th> Size </th>';
  echo '<th> User </th>';
  echo '</tr>';

  foreach ($profiles as $profile) {
    echo "<tr>";
    echo "<td>" . $profile->action . "</td>";
    echo "<td>" . $profile->init . " / " . $profile->end . "</td>";
    echo "<td>" . $profile->origin . "</td>";
    echo "<td style='text-align: right;'>" . $profile->time . "</td>";
    echo "<td style='text-align: right;'>" . $profile->size . "</td>";

    $user = User::findById($profile->userId);
    $agency = Agency::findById($user->agency_id);
    echo "<td>" . $user->username . "/" . $agency->name . "</td>";
    echo "</tr>";
  }
  echo '</table>';
}
?>
