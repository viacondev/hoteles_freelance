<?php

function getServiceAddDataJSON($roomsGroup, $childAges = array())
{
  $hotelAvail = json_decode($roomsGroup[0]->hotel);
  $service_data = HotelAvailToArray($hotelAvail);
  $rooms_arr = array();
  foreach ($roomsGroup as $room) {
    $rooms_arr[] = RoomAvailToArray($room);
  }
  $service_data['rooms'] = $rooms_arr;
  $service_data['childAges'] = $childAges;
  $service_data['requestType'] = BookingRequestTypes::AddHotelService;

  $service_data['provider'] = $hotelAvail->provider;
  return htmlspecialchars(json_encode($service_data), ENT_QUOTES, 'UTF-8');
}


function RoomAvailToArray($roomAvail)
{
  $arr = array(
    'roomCount' => $roomAvail->roomCount,
    'adultCount' => $roomAvail->adultCount,
    'childCount' => $roomAvail->childCount,
    'price' => $roomAvail->price,
    'SHRUI' => $roomAvail->sHRUI,
    'onRequest' => $roomAvail->onRequest,
    'board' => $roomAvail->board,
    'boardCode' => $roomAvail->boardCode,
    'boardType' => $roomAvail->boardType,
    'roomType' => $roomAvail->roomType,
    'roomTypeType' => $roomAvail->roomTypeType,
    'roomTypeCode' => $roomAvail->roomTypeCode,
    'roomTypeCharacteristic' => $roomAvail->roomTypeCharacteristic
  );

  return $arr;
}

function HotelAvailToArray($hotelAvail) {
  $arr = array(
    'availToken' => $hotelAvail->availToken,
    'dateFrom' => $hotelAvail->dateFrom,
    'dateTo' => $hotelAvail->dateTo,
    'contractName' => $hotelAvail->contractName,
    'contractIncomingOfficeCode' => $hotelAvail->contractIncomingOfficeCode,
    'hotelCode' => $hotelAvail->code,
    'destCode' => $hotelAvail->destinationCode,
    'destType' => $hotelAvail->destinationType,
    'hotelName' => $hotelAvail->name,
    'category' => $hotelAvail->categoryCode
  );

  return $arr;
}

function getHotelLink($hotelAvail) {
  global $params;
  $p = $params;

  $hotelCode = $hotelAvail->getCode();

  $providers = "&amp;providers%5B%5D=" . BookingProvider::HOTELBEDS;
  $providers .= "&amp;providers%5B%5D=" . BookingProvider::DOMITUR;

  if ($hotelAvail->getProvider() == BookingProvider::DOMITUR) {
    $map = DomiturHotelMapping::find(array('code' => $hotelCode));
    if ($map != NULL) {
      $hotelCode = $map->zion_code;
    }
    else {
      $providers = "&amp;providers%5B%5D=" . BookingProvider::DOMITUR;
    }
  }
  else if ($hotelAvail->getProvider() == BookingProvider::METHABOOK) {
    $providers = "&amp;providers%5B%5D=" . BookingProvider::METHABOOK;
  }

  $hotelLink = '?controller=accommodation&amp;action=show&amp;hotelCode=' . $hotelCode;
  $hotelLink .= '&amp;search_avail=1&amp;destination_code=' . $p['destCode'] . '&amp;page=1';
  $hotelLink .= '&amp;checkin=' . dateToDDMMYY($p['checkin']) . '&amp;checkout=' . dateToDDMMYY($p['checkout']);
  $hotelLink .= '&amp;habitaciones=' . $p['roomCount'];
  $totalChildCount = 0;
  for ($i = 1; $i <= intval($p['roomCount']); $i++) {
    $hotelLink .= "&amp;adultos$i=" . $p["adultCount_$i"];
    $hotelLink .= "&amp;ninos$i=" . $p["childCount_$i"];
  }
  for ($i = 1; $i <= $p['totalChildCount']; $i++) {
    $hotelLink .= "&amp;childAge$i=" . $p["childAge_$i"];
  }
  $hotelLink .= "&amp;provider=" . $hotelAvail->getProvider();
  $hotelLink .= $providers;

  return $hotelLink;
}

?>
