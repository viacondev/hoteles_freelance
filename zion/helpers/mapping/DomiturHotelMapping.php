<?php
include_once('../../../config/Config.php');
include_once('../../database/DB.php');
include_once('../../model/Model.php');
include_once('../../model/User.php');
include_once('../../model/Hotel.php');
include_once('../../model/DomiturHotelMapping.php');
include_once('../../helpers/logger.php');
include_once('../../../booking/domitur/DomiturRQ.php');
include_once('../../../booking/domitur/RQOperation.php');
include_once('../../../booking/domitur/HotelListRQ_dt.php');

$rq = new Domitur\DomiturRQ();
$hotel_rq = new Domitur\HotelListRQ('REPDOM');

$xml = $rq->execRequest($hotel_rq);
$xml = str_replace('&', '&amp;', $xml);
$dt_list = simplexml_load_string(utf8_encode($xml));

?>
<!DOCTYPE html>
<html>
<head><meta charset="utf-8" /></head>
<body>
<script src="../../../lib/jquery/jquery.min.js"></script>

<table id="maps" style="width:100%;">
  <tr>
    <th colspan="2">Domitur</th>
    <th colspan="2">Zion</th>
  </tr>
  <?
  foreach ($dt_list->parameters->hotels->hotel as $hotel) {
    // __logarr($hotel);
    $zionCode = DomiturHotelMapping::getMappedCode($hotel->code);
    $zionHotel = Hotel::find(array('hotel_code' => $zionCode));
    $hotelName_arr = explode('-', $hotel->name);
    $hotelName = trim($hotelName_arr[0]);

    $style = 'style="padding: 8px; border-top: 1px solid #ccc; border-right: 1px solid #ccc;';
    $style .= $zionCode != NULL ? ' background-color: yellow"' : '"';
    $mapBtn = $zionCode == NULL ? ' <button onclick="findMap('. $hotel->code . ', \'' . $hotelName . '\');">Mapear</button>' : '';
    $code = $hotel->code;
    $zoneStr = $hotel->address . ' - ' . $hotel->localization . ' - ' . $hotel->country;
    $zone = "<div id='zone$code' style='display: none; color: #777;'>$zoneStr</div>";
    $showZone = " <span style='color: blue; cursor: pointer;' onclick='$(\"#zone$code\").show()'>+</span> ";
    $trClass = $zionCode == NULL ? 'notMapped' : '';
    echo "<tr id='tr$code' class='$trClass'>";
    echo "<td $style id='hc$code'><strong>" . $hotel->code . "</strong></td>";
    echo "<td $style id='hn$code'>" . $hotel->name . $mapBtn . $showZone . $zone . "</td>";

    $zionHotelName = isset($zionHotel) ? $zionHotel->hotel_name : '-';
    echo "<td $style id='zc$code'>$zionCode</td>";
    echo "<td $style id='zn$code'>$zionHotelName</td>";
    echo "</tr>";
  }

  ?>
</table>

<div style="width: 100%; height: 250px; position: fixed; bottom: 0; left: 0; z-index: 10; border-top: 3px solid #999; overflow: auto; background-color: #eaeaea; text-align: center;">
  <input type="hidden" id="search_code">
  <input type="text" id="search_name" style="width: 300px;">
  <button onclick="search()">Search</button>
  <table id="mapOptions" border="1"></table>
</div>

<script type="text/javascript">
  function findMap(code, name) {
    $('#search_code').val(code);
    $('#search_name').val(name);
    search();

    $('.notMapped td').css('background-color', '#fff');
    $('#hc' + code).css('background-color', '#ccc');
    $('#hn' + code).css('background-color', '#ccc');
    $('#zc' + code).css('background-color', '#ccc');
    $('#zn' + code).css('background-color', '#ccc');
  }

  function search() {
    $.ajax({
      url: "DomiturHotelMapping_ajax.php",
      data: {
        code: $('#search_code').val(),
        search: $('#search_name').val(),
        action: 'search',
      }
    })
      .done(function(r) {
        $('#mapOptions').html(r);
      });
  }

  function map(code, zionCode) {
    $.ajax({
      url: "DomiturHotelMapping_ajax.php",
      data: {
        code: code,
        zionCode: zionCode,
        action: 'map',
      }
    })
      .done(function(r) {
        $('#zc' + code).html(zionCode);
        $('#zn' + code).html(r);
        $('#tr' + code).removeClass('notMapped');
        $('#hc' + code).css('background-color', 'yellow');
        $('#hn' + code).css('background-color', 'yellow');
        $('#zc' + code).css('background-color', 'yellow');
        $('#zn' + code).css('background-color', 'yellow');
      });
  }
</script>
</body>
</html>