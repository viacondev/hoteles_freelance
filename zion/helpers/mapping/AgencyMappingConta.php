
<? ini_set('display_errors', 1); ?>
<link href="../../../lib/jquery-ui/css/custom-theme/jquery-ui-1.10.0.custom.css" media="screen" rel="stylesheet" />
<link href="../../../lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="../../../lib/jquery/jquery.min.js"></script>
<script src="../../../lib/jquery-ui/assets/js/jquery-ui-1.10.0.custom.min.js"></script>
<script src="../../../lib/bootstrap/js/bootstrap.min.js"></script>
<?

$db = new PDO("mysql:host=23.91.64.139;dbname=barrybol_zion_engine;charset=utf8", 'barrybol_zion', 'zionshopdbpwd');
$query = "SELECT * FROM Agency WHERE status = 1";
$agency = $db->query($query);
$result = $agency->fetchAll();
// echo '<pre>';
// print_r($result);
// echo '</pre>';
?>
<style type="text/css">
    table tr td{
        border: 1px solid #ddd;
        padding: 5px;
    }
    table tr th{
        border: 1px solid #ddd;
        padding: 5px;
    }
</style>

<div id="msg_result_modal" class="modal fade" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Notificaci&oacute;n!</h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button class="btn btn-danger" onclick="$('#msg_result_modal').modal('hide')">Aceptar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<table>
    <tr>
        <td>CODIGO</td>
        <td>NOMBRE</td>
        <td>ACCION</td>
    </tr>
<?  for($i = 0;$i < count($result);$i++) {  
        $hotel = $result[$i];
?>
        <tr>
            <td><? echo $hotel['id']; ?></td>
            <td><? echo $hotel['name']; ?></td>
            <td>
                <? if($hotel['idcliente'] == 0) { ?>
                <input type="button" name="btn_buscar" value="Buscar" onclick="searchAgencyContable('<? echo $hotel['id']; ?>','<? echo $hotel['name']; ?>')">
                <? } ?>
            </td>
        </tr>

<?  } ?>
</table>

<script type="text/javascript">
    function searchAgencyContable(id,name) {
        $.ajax({
            url : '../ajax/search_agency_contable.php',
            type : 'POST',
            data : {'name':name, 'action':'search'},
            success : function(data) {
                res  =  JSON.parse(data)
                html =  '<table>'+
                            '<tr>'+
                                '<th>Codigo</th>'+
                                '<th>Nombre</th>'+
                                '<th>Accion</th>'+
                            '</tr>'
                            for (var i =  0; i < res.length; i++) {
                                hotel = res[i]
                                html += '<tr>'+
                                            '<td>' + hotel.idclientes + '</td>'+
                                            '<td>' + hotel.nombre_cliente + '</td>'+
                                            '<td><input type="button" value="Mapear" onclick="mappingAgency('+hotel.idclientes+','+id+')" /></td>'+
                                        '</tr>' 
                            }
                html += '</table>'

              $('.modal-body').html(html);
            }
        });
        $('#msg_result_modal').modal()
    }

    function mappingAgency(id_contable, id_agency) {
        $.ajax({
            url : '../ajax/search_agency_contable.php',
            type : 'POST',
            data : {'id_contable':id_contable,'id_agency':id_agency,'action':'mapping'},
            success : function(data) {
                res  =  JSON.parse(data)
                console.log(res)
            }
        });
    }
</script>