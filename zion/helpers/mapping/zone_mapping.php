<style type="text/css">
  #table_content, #main_zone {
    display: inline-table;
  }
  #table_content {
    width: 35%;
  }

  #main_zone{
    width: 400px;
    position: fixed;
    z-index: 999;
  }

  #main_zone tr:hover{
    background-color: #ddd; 
  }

  #googlemapview {
    position: absolute;
    right: 50px;
    width: 30%;
    height: 100%;
  }
     

</style>
<select id="destCodeSelect">
  <option value="3">Restel</option>
  <option value="159">Domitur2</option>
  <option value="136">TailorBeds</option>
</select>
<input type="text" name="destination_code" id="destination_code" value="">
<input type="button" name="search" value="Buscar" onclick="search_zones()">
<input type="button" name="Save" value="Guardar" onclick="save_zonas()">
<div id="loading" style="display:none;">Buscando Resultados.....</div>
<div id="result_content" style="margin-top: 21px;">
  <table id="table_content" border="1"></table>
  <table id="main_zone" border="1"></table>
</div>
<div id="googlemapview" class="map"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
  var hotel_provider = ""
  var marcadores = [];
  function search_zones() {
    $('#loading').show()
    $('#table_content').html("")
    destCode = $('#destination_code').val()
    provider = $('#destCodeSelect').val()
    $.ajax({
      url  : 'zone_mapping_ajax.php',
      dataType : "html",
      type : "POST", 
      data : {destCode:destCode, provider:provider, action:"search_zones"},
      success : function(res) {
         res  = JSON.parse(res)
         $('#loading').hide()
         html = "<tr><th>N#</th><th>Hotel Code</th><th>Holte Name</th><th>*</th></tr>"
         r = res['provider_zone'];
         for (var i =  0; i < r.length; i++) {
          var td  = "";
          if (!r[i].exist) {
            td = "<input type='button' class='check-main' id='" + r[i].hotelCode + "_" + r[i].provider + "' data-main='" + JSON.stringify(r[i]) + "' value='Mapear' onclick='findMap(\"" + r[i].hotelCode + "_" + r[i].provider + "\")' />";
          }
          else {
            td = "<input type='button' class='check-main' id='" + r[i].hotelCode + "_" + r[i].provider + "' data-main='" + JSON.stringify(r[i]) + "' value='Eliminar' onclick='deleteMapping(\"" + r[i].hotelCode + "_" + r[i].provider + "\")' />"; 
          }
          html +=   "<tr class='rc_" + r[i].hotelCode + "_" + r[i].provider + " not_mapped'>" +
                      "<td>" + (i+1) + "</td>" +
                      "<td>" + r[i].hotelCode + "</td>" +
                      "<td>" + r[i].hotelName + "</td>" +
                      "<td>" + td + "</td>" +
                    "</tr>";
         }
         $('#table_content').html(html);
      }
    });
  }

  function delete_zone(input) {
    if (confirm("Estas seguro de Eliminar??")) {
      codigo = $('#' + input).data('val')
       $.ajax({
            url  : 'zone_mapping_ajax.php',
            dataType : "html",
            type : "POST", 
            data : {id:codigo , action:"delete_zones"},
            success : function(res) {
              console.log(res)
            }
          });
    }
  }

  function deleteMapping(input) {
    if (confirm("Estas seguro de Eliminar??")) {
      codigo = $('#' + input).data('main')
      codigo = codigo.idMapping
       $.ajax({
            url  : 'zone_mapping_ajax.php',
            dataType : "html",
            type : "POST", 
            data : {idMapping:codigo , action:"delete_hotel_mapping"},
            success : function(res) {
              console.log(res)
              location.reload()
            }
          });
    }
  }

  function adicionar_marcadores(hotel, sw) {
    var color = 'http://chart.apis.google.com/chart?chst=d_map_spin&chld=.6|0|54ABD5|10|_|' + hotel.pos
    if(sw) {
      color = 'http://chart.apis.google.com/chart?chst=d_map_spin&chld=.6|0|009688|10|_|' + hotel.pos
    }
    contentString = hotel.name

   
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(hotel.lat, hotel.lon),
      map: map,
      color: 'azul',
      title: hotel.name,
      icon: color
    });
    if(sw) {
      var infowindow = new google.maps.InfoWindow({
        content: contentString
      });
      infowindow.open(map,marker);
    }
     
    marcadores[hotel.pos] = marker;

  }

  function setMapOnAll(map) {
    //hace ciclo sobre los marcadores que hemos guardado en la variable markers
    for (var i = 1; i < marcadores.length; i++) {
      marcadores[i].setMap(map);
    }
  }

  function checkenable() {
    var ch    = $('input:checkbox[class=check-zone-provider]:not(:checked)');
    var allch = $('.check-zone-provider')
    if(ch.length != allch.length) {
      $(ch).attr("disabled", true);
    }
    else {
      $(allch).removeAttr("disabled");
    }
  }
  posHotel = 1
  function findMap(id) {
    posHotel = 1
    if(marcadores.length > 0) {
      setMapOnAll(null);
      marcadores = [];
    }
    
    hotel        = $('#' + id).data('main');
    // var position = $('#' + id).position()
    var top      = $(window).scrollTop()
    $('#googlemapview').css("top", top);
    hotel_provider = hotel
    $('.not_mapped').css('background-color', 'white')
    $('.rc_' + id).css('background-color', '#ccc')
    hotel = {
        pos: posHotel,
        hotelCode : hotel_provider.hotelCode,
        name      : hotel_provider.hotelName,
        lat       : hotel_provider.latitude,
        lon       : hotel_provider.longitude
      }
    posHotel++
    adicionar_marcadores(hotel, true)
    var hotelName = hotel_provider.hotelName
    var destCode  = hotel_provider.destCode
    $('#main_zone').html("<tr><td colspan='4'>Buscando Coincidencias...</td></tr>")
    $.ajax({
            url  : 'zone_mapping_ajax.php',
            dataType : "html",
            type : "POST", 
            data : {hotelName:hotelName, destCode:destCode, action:"search_hotelbeds"},
            success : function(res) {
              console.log(res)
              var html = "<tr><th>Numero</th><th>Codigo</th><th>Nombre Hotel</th><th>*</th></tr>"
              var res = JSON.parse(res)
              for (var i = 0; i < res.length; i++) {
                hotel = {
                          pos: posHotel,
                          hotelCode : res[i].hotel_code,
                          name      : res[i].hotel_name,
                          lat       : res[i].latitude,
                          lon       : res[i].longitude
                        }
                
                adicionar_marcadores(hotel, false)
                html += "<tr>" +
                          "<td>" + posHotel + "</td>" +
                          "<td>" + res[i].hotel_code + "</td>" +
                          "<td>" + res[i].hotel_name + "</td>" +
                          "<td><input type='button' id='hb_" + res[i].hotel_code  + "_1' value='Guardar' data-hb='" + JSON.stringify(res[i]) + "' onclick='Mapi(\"hb_" + res[i].hotel_code + "_1\")'/></td>" +
                        "</tr>"
                posHotel++
              }
              $('#main_zone').html(html)
            }
          });
    var center = new google.maps.LatLng(hotel_provider.latitude, hotel_provider.longitude);
    google.maps.event.trigger(map, "resize");
    map.setCenter(center);
  }

  function Mapi(id) {
    mainHotel = $('#' + id).data('hb')
    console.log(mainHotel)
    $.ajax({
            url  : 'zone_mapping_ajax.php',
            dataType : "html",
            type : "POST", 
            data : {main_hotel:mainHotel, hotel_provider:hotel_provider, action:"save_hotel_mapping"},
            success : function(res) {
              console.log(res)
            }
          });
  }

  function save_zonas() {
    var main_zones    = $('input:checkbox[class=check-zone-provider]:checked')
    var provider_zone = $('input:checkbox[class=check-main]:checked')
    var destCode      = $('#destination_code').val()
    if (main_zones.length == 0 || provider_zone.length == 0) {
      alert("Tienes que seleccionar al menos 1 zona")
    }
    zonas = new Array()
    for (var i = 0; i < provider_zone.length; i++) {
      zone = $(provider_zone[i]).data('main')
      zonas.push(zone)
    }
    provider_z = $(main_zones[0]).data('info')
    $.ajax({
            url  : 'zone_mapping_ajax.php',
            dataType : "html",
            type : "POST", 
            data : {destCode:destCode, main_zones:provider_z, provider_zone:zonas, action:"save_zones"},
            success : function(res) {
              console.log(res)
            }
          });
  }
  centerLat = 0;
  centerLon = 0;
  var map;
  function initialize() {
    var mapOptions = {
      zoom: 11,
      center: new google.maps.LatLng(centerLat, centerLon),
      gestureHandling: 'greedy',
      center: new google.maps.LatLng(-34.61454722222219970718, -58.40306111111109999001)
    };
    map = new google.maps.Map(document.getElementById('googlemapview'), mapOptions);
    radiusMarker = new google.maps.Marker({
      map       : map,
      position  : new google.maps.LatLng(centerLat, centerLon),
      draggable : true,
      title     : 'Puedes arrastrar para buscar hoteles en una nueva area',
      //icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'
      icon      : 'http://chart.apis.google.com/chart?chst=d_map_spin&chld=.7|0|1|10|_|R',
      zIndex   : 1
    });
  }

  var ready = function() {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyD56vxIspKesbI0tRFvXe9Txrb3pWaBwps&v=3.exp&libraries=places&callback=initialize';
    document.body.appendChild(script);
  };
  $(document).ready(ready);
</script>