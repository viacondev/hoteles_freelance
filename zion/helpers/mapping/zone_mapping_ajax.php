<? 

include_once('../../../config/Config.php');
include_once('../../database/DB.php');
include_once('../../model/Model.php');
include_once('../../model/Zone.php');
include_once('../../model/User.php');
include_once('../../model/Agency.php');
include_once('../../model/Hotel.php');
include_once('../../model/HotelMappingProvider.php');
include_once('../../model/DomiturHotelMapping.php');
include_once('../../../booking/BookingEngine.php');
include_once('../../helpers/application_helper.php');
include_once('../../helpers/accommodation_helper.php');
include_once('../../helpers/logger.php');

 $db = new PDO("mysql:host=localhost;dbname=barrybol_zion_engine;charset=utf8", 'barrybol_zion', 'zionshopdbpwd');
 $db2 = new PDO("mysql:host=localhost;dbname=barrybol_hotelbeds;charset=utf8", 'barrybol_zion', 'zionshopdbpwd');
  if($_POST['action'] == "search_zones") {
    $params = array('provider' => $_POST['provider'], 'requestType' => 16, 'destCode' => $_POST['destCode']);
    $zone   = Zone::findAll(array('destination_code' => $_POST['destCode']));
    $zone_p = array();
    foreach ($zone as $value) {
      $zone_p[] = array('zone_code' => $value->zone_code, 'zone_name' => $value->zone_name);
    }
    $book   = new BookingEngine();
    $rs     = $book->execRequest($params);
    $con = 0;
    echo json_encode(array('main_zone' => $zone_p, 'provider_zone' => $rs));
  }

  if($_POST['action'] == "save_zones") {
   
    $destination_code = $_POST['destCode'];
    $main_zone        = $_POST['main_zones'];
    $zion_zone_code   = $main_zone['zone_code'];
    $zion_zone        = $main_zone['zone_name'];
    $zones_provider   = $_POST['provider_zone'];
    foreach ($zones_provider as $zone) {
      // __logarr($zone);
      $zone_code_provider = isset($zone->zone_code) ? $zone->zone_code : 0;
      $zone_name          = $zone['zone'];
      $provider           = $zone['provider'];
      $sentencia = $db->prepare("INSERT INTO ZoneMapping(DESTINATION_CODE, ZION_ZONE_CODE, ZION_ZONE, ZONE_CODE , ZONE_NAME, PROVIDER) VALUES 
                                                      (:DESTINATION_CODE,:ZION_ZONE_CODE,:ZION_ZONE,:ZONE_CODE,:ZONE_NAME,:PROVIDER)");
      $sentencia->bindParam(':DESTINATION_CODE', $destination_code);
      $sentencia->bindParam(':ZION_ZONE_CODE', $zion_zone_code);
      $sentencia->bindParam(':ZION_ZONE', $zion_zone);
      $sentencia->bindParam(':ZONE_CODE', $zone_code_provider);
      $sentencia->bindParam(':ZONE_NAME', $zone_name);
      $sentencia->bindParam(':PROVIDER', $provider);
      $sentencia->execute();
      print_r($sentencia->errorInfo());
    }
  }

  if($_POST['action'] == "delete_zones") {
    $id     = $_POST['id'];
    $sql    = "DELETE FROM ZoneMapping WHERE id=?;";
    $query  = $db->prepare($sql);
    $query->execute(array($id['codigo']));
    print_r($query->errorInfo());
    
  }

  if($_POST['action'] == "search_hotelbeds") {
    // $search = $_POST['hotelName'];
    $r       = array();
    $filter  = explode(' ', $_POST['hotelName']);
    $arr     = array();
    $keys    = array();
    foreach ($filter as $value) {
      $cadQue = "SELECT DISTINCT * FROM HOTELS WHERE destinationcode = '" . $_POST['destCode'] . "' AND name LIKE '%$value%' LIMIT 10";
      $query  = $db2->query($cadQue);
      $r      = $query->fetchAll();
      foreach ($r as $hotel) {
        $value = (object) $hotel;
        if (!array_key_exists($value->HOTELCODE, $keys)) {
          $keys[$value->HOTELCODE]  = $value->NAME; 
          $arr[] = array( 'hotel_code' => $value->HOTELCODE, 'hotel_name' => $value->NAME, 'zone_code' => $value->ZONECODE,
                        'zone_name' =>  "", 'latitude' => $value->LATITUDE,'longitude' => $value->LONGITUDE);
        }
      }
    }
    // __logarr($hotels);
    //$hotels = Hotel::findByQuery("SELECT DISTINCT * FROM Hotel WHERE destination_code = '" . $_POST['destCode'] . "' AND hotel_name LIKE '%$search%'");
    // __log("SELECT DISTINCT * FROM HOTELS WHERE destinationcode = '" . $_POST['destCode'] . "' AND name LIKE '%$search%'");
    
    
    echo json_encode($arr);
  }
   
  if($_POST['action'] == "save_hotel_mapping") {
    $hotelMapping   = new HotelMappingProvider();
    $hotel_main     = $_POST['main_hotel'];
    $hotel_provider = $_POST['hotel_provider']; 
    
    $hotelMapping->destination_code = $hotel_provider['destCode'];
    $hotelMapping->zion_code        = $hotel_main['hotel_code'];
    $hotelMapping->code             = $hotel_provider['hotelCode'];
    $hotelMapping->zone_code        = $hotel_main['zone_code'];
    $hotelMapping->provider         = $hotel_provider['provider'];
    $hotelMapping->save();
    
  }

  if($_POST['action'] == "delete_hotel_mapping") {
    $hotelMapping     = new HotelMappingProvider();
    $hotelMapping->id = $_POST['idMapping'];
    $hotelMapping->delete();
  }
?>