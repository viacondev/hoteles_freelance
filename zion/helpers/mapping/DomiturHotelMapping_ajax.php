<?
include_once('../../../config/Config.php');
include_once('../../database/DB.php');
include_once('../../model/Model.php');
include_once('../../model/User.php');
include_once('../../model/Hotel.php');
include_once('../../model/DomiturHotelMapping.php');
include_once('../../helpers/logger.php');

$action = $_GET['action'];
$code = $_GET['code'];

if ($action == 'search') {
  $search = $_GET['search'];
  $hotels = Hotel::findByQuery("SELECT * FROM Hotel WHERE hotel_name LIKE '%$search%'");

  foreach ($hotels as $hotel) {
    $hotel_name = $hotel->hotel_name;
    $strpos = stripos($hotel_name, $search);
    $name = substr($hotel_name, 0, $strpos);
    $name .= '<span style="color: red;">' . substr($hotel_name, $strpos, strlen($search)) . '</span>';
    $name .= substr($hotel_name, $strpos + strlen($search));

    echo '<tr>';
    echo '<td><a href="../../?controller=accommodation&action=show&hotelCode=' . $hotel->hotel_code . '" target="_blank">' . $hotel->hotel_code . '</td>';
    echo '<td><strong>' . $name . '</strong></td>';
    echo '<td>' . $hotel->zone_name . ', ' . $hotel->destination_name . ' - ' . $hotel->country_code . '</td>';
    echo '<td><button onclick="map(' . $code . ', \'' . $hotel->hotel_code . '\')">Map</button></td>';
  }

  if (count($hotels) == 0) {
    echo '<tr><td style="font-weight: bold; color: red;">No hay resultados</td></tr>';
  }
}
else if ($action == 'map') {
  $zionCode = $_GET['zionCode'];

  $map = new DomiturHotelMapping();
  $map->code = $code;
  $map->zion_code = $zionCode;
  $map->save();

  echo Hotel::find(array('hotel_code' => $zionCode))->hotel_name;
}
?>