<?php

function get_diff($date_policy) {
  if(gettype($date_policy) == "string") {
    $date_policy = new DateTime(implode('-', array_reverse(explode('/', $date_policy))));
  }

  $interval = date_diff($date_policy, new DateTime('now'));
  return (int) $interval->format('%R%a');
}
function get_options($diff, $Options) {
  if ($diff == 0)  {
    if ($Options != 'expired_date') {$Options = 'expired_date';} // 'now_expired'
  }
  if ($diff >= 1 && $diff != 0) { $Options = 'expired_date'; }
  return $Options;
}
function get_message_show($diff, $time, $datefrom, $amount, $hotel_getCurrency) {
  $message="";
  if ($diff == 0) {
    //$message .= 'Si cancelas despu&eacute;s de las ' . $time . 'hrs del ' . $datefrom . ' se aplicar&aacute;n gastos de: ' . $amount . ' ' . $hotel_getCurrency;
    //$message .= '. Si la reserva no es confirmada hasta dicha fecha &eacute;sta se cancelar&aacute; autom&aacute;nticamente.';
    $message .= 'Atenci&oacute;n! esta reserva tiene gastos de ' . $amount . ' ' . $hotel_getCurrency . ' por cancelaci&oacute;n desde el momento de la reserva';
  }
  if ($diff < 1 && $diff != 0) {
    $message .= 'Si cancelas despu&eacute;s de las ' . $time . 'hrs del ' . $datefrom . ' se aplicar&aacute;n gastos de: ' . $amount . ' ' . $hotel_getCurrency;
    $message .= '. Si la reserva no es confirmada hasta dicha fecha &eacute;sta se cancelar&aacute; autom&aacute;ticamente.';
  }
  else if ($diff >= 1 && $diff != 0) {
    $message .= 'Atenci&oacute;n! esta reserva tienes gastos de '. $amount . ' ' . $hotel_getCurrency . ' por cancelaci&oacute;n desde el momento de la reserva';
  }
  return $message;
}

?>
