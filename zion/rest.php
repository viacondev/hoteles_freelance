<?php
date_default_timezone_set('America/La_Paz');

include_once("../lib/rest/rest-api/Rest.inc.php");

include_once('includes.php');
include_once('controller/RestController.php');

class ZionRest extends REST {

  public function __construct() {
    parent::__construct();
  }

  public function process() {
    $func = strtolower(trim(str_replace("/", "", $_REQUEST['rquest'])));
    if ((int) method_exists($this, $func) > 0) {
      $this->$func();
    }
    else {
      $this->response('', 404);
    }
  }

  public function execQuery() {
    if (isset($this->_request['controller']) && isset($this->_request['action'])) {
      $res = RestController::loadAction($this->_request);
      $this->response($res, 200);
    }
  }
}

$search = new ZionRest();
$search->process();
?>
