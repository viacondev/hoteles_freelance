<?
  include('config/Config.php');
  include('zion/database/DB.php');
  include('zion/model/Model.php');
  include_once('zion/model/Config.php');
  include_once('zion/model/Fee.php');
  include_once('zion/model/Hotel.php');


  $query = "SELECT DISTINCT zone_code, zone_name, destination_code, destination_name, country_code, country_name FROM Hotel ORDER BY country_code, destination_code";
  $country = Hotel::findByQuery($query);
  $dest = array();
  foreach ($country as $value) {
    $dest[] = array('code' => $value->zone_code, 'th_code' => $value->destination_code, 'city' => $value->zone_name,
                    'state' => '', 'country_code' => $value->country_code, 'lat' =>'' , 'long' => '');
  }
  // echo "<pre>";
  // print_r($dest);
  // echo "</pre>";
  $fp = fopen('countries_list.json', 'w');
  fwrite($fp, json_encode($dest));
  fclose($fp);


  header("Content-Disposition: attachment; filename=\"" . basename("countries_list.json") . "\"");
  header("Content-Type: application/force-download");
  header("Content-Length: " . filesize("countries_list.json"));
  header("Connection: close");
  readfile("countries_list.json");
?>
