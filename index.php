<?php
session_start();
include_once('zion/config/constants.php');

if (isset($_SESSION['current_user'])) {
  header('Location: zion/');
}

$login_err_msg = '';
$login_show_style = 'flipInX';
if (isset($_GET['login_error'])) {
  $login_err_msg = 'Usuario/contrase&ntilde;a incorrectos. Vuelva a intentar.';
  $login_show_style = 'shake';
}
?>
<!DOCTYPE html>
<html>
<head>
  <title><?php echo TITLE; ?></title>
  <meta charset="utf-8" />
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- css -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
  <link href="css/login.style.css" rel="stylesheet" media="screen">
  <link href="css/login.color.css" rel="stylesheet" media="screen">
  <script src="lib/js/modernizr.custom.js"></script>
</head>

<body>
  <div class="menu-area" tabindex="2">
    <div id="dl-menu" class="dl-menuwrapper">
      <button class="dl-trigger">Abrir Menu</button>
      <ul class="dl-menu">
        <li>
          <a href="#intro">Inicio</a>
        </li>
        <li><a href="#about">Servicios</a></li>
        <li><a href="#services">Ventajas</a></li>
        <li><a href="#works">Hoteles</a></li>
        <li><a href="#contact">Contacto</a></li>
      </ul>
    </div>
  </div>  

  <!-- Login box -->   
  <div id="intro">
    <div class="intro-text">
      <div class="container">
        <div class="row">
          <div id="login" class="col-md-offset-4 col-md-4 wow <?php echo $login_show_style ?>">
            <div id="login-header">
              <h1 class="text-center">
                <img src="images/logo.png" />
                <span class="booking">Barry</span><span class="bolivia">Bolivia</span>
              </h1>
              <span class="text-danger"><?php echo $login_err_msg; ?></span>
            </div>
            <div id="login-body">
              <form class="form" method="post" action="zion/login.php">
                <div class="form-group">
                  <input type="text" name="username" class="form-control input-lg" placeholder="Usuario" tabindex="1">
                </div>
                <div class="form-group">
                  <input type="password" name="password" class="form-control input-lg" placeholder="Contrase&ntilde;a" tabindex="1">
                </div>
                <div class="form-group">
                  <input type="hidden" name="home" value="1" />
                  <button class="btn btn-primary btn-lg btn-block" name="login" tabindex="1">Ingresar</button>
                  <span class="pull-left"><ul class="dl-menu"><li><a href="#contact" tabindex="1">Necesitas ayuda?</a></li></ul></span>
                  <span class="pull-right"><ul class="dl-menu"><li><a href="#contact" tabindex="1">Registrarse</a></li></ul></span>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- About -->
  <section id="about" class="home-section bg-white">
    <div class="container">
      <div class="row">
        <div class="col-md-offset-2 col-md-8">
          <div class="section-heading">
            <h2>Servicios</h2>
            <p>Contamos con los mejores servicios para que puedas planificar tus viajes desde un solo lugar.</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <div class="box-team wow bounceInDown" data-wow-delay="0.1s">
            <img src="images/login/hotels.jpg" alt="" class="img-responsive" />
            <h4>Hoteles</h4>
            <p></p>
          </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" data-wow-delay="0.3s">
          <div class="box-team wow bounceInDown">
            <img src="images/login/transfer.jpg" alt="" class="img-responsive" />
            <h4>Traslados</h4>
            <p></p>
          </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" data-wow-delay="0.7s">
          <div class="box-team wow bounceInDown">
            <img src="images/login/tickets.jpg" alt="" class="img-responsive" />
            <h4>Excursiones</h4>
            <p></p>
          </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" data-wow-delay="0.5s">
          <div class="box-team wow bounceInDown">
            <img src="images/login/package.jpg" alt="" class="img-responsive" />
            <h4>Paquetes</h4>
            <p></p>
          </div>
        </div>
      </div>        
    </div>    
  </section>
    
  <!-- spacer -->   
  <section id="spacer1" class="home-section spacer">  
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="color-light">
            <h2 class="wow bounceInDown" data-wow-delay="0.5s">Descubr&iacute; el placer de tener el control</h2>
            <p class="lead wow bounceInUp" data-wow-delay="1s">No volver&aacute;s a perder el rastro de ninguna reserva</p> 
          </div>
        </div>        
      </div>
    </div>
  </section>
    
  <!-- Ventajas -->
  <section id="services" class="home-section bg-white">
    <div class="container">
      <div class="row">
        <div class="col-md-offset-2 col-md-8">
          <div class="section-heading">
            <h2>Ventajas</h2>
            <p>Queremos que tu agencia crezca. Por eso te brindamos las mayores comodidades y ventajas</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <div class="service-box wow bounceInDown" data-wow-delay="0.1s">
            <i class="fa fa-heart fa-4x"></i>
            <h4>F&aacute;cil y Sencillo</h4>
            <p>Te ofrecemos un sistema sencillo e intuitivo desde donde podr&aacute;s hacer tus b&uacute;squedas y reservas.</p>
            <a class="btn btn-primary">Ver m&aacute;s...</a>
          </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" data-wow-delay="0.3s">
          <div class="service-box wow bounceInDown" data-wow-delay="0.1s">
            <i class="fa fa-mobile fa-4x"></i>
            <h4>Siempre Contigo</h4>
            <p>Podr&aacute; realizar y administrar tus reservas desde cualquier dispositivo conectado a internet.</p>
            <a class="btn btn-primary">Ver m&aacute;s...</a>
          </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" data-wow-delay="0.5s">
          <div class="service-box wow bounceInDown" data-wow-delay="0.1s">
            <i class="fa fa-bell fa-4x"></i>
            <h4>Notificaciones</h4>
            <p>Tu y tus clientes recibir&aacute;n alertas sobre sus reservas. No volver&aacute;s a perder el control!</p>
            <a class="btn btn-primary">Ver m&aacute;s...</a>
          </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" data-wow-delay="0.7s">
          <div class="service-box wow bounceInDown" data-wow-delay="0.1s">
            <i class="fa fa-bar-chart-o fa-4x"></i>
            <h4>Estad&iacute;sticas</h4>
            <p>Total control sobre tus counters y sus ventas. Te ofrecemos estad&iacute;sticas precisas de tus movimientos.</p>
            <a class="btn btn-primary">Ver m&aacute;s...</a>
          </div>
        </div>
      </div>  
    </div>
  </section>
  
  <!-- Works -->
  <section id="works" class="home-section bg-gray">
    <div class="container">
      <div class="row">
        <div class="col-md-offset-2 col-md-8">
        <div class="section-heading">
         <h2>Hoteles</h2>
         <p>Te brindamos acceso a m&aacute;s de 50.000 hoteles alrededor de 147 pa&iacute;ses.</p>
        </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-offset-2 col-md-8">
          <ul class="lb-album">
            <li class="wow bounceInLeft" data-wow-delay="0.3s"><img src="images/login/h1.jpg" alt=""></li>
            <li class="wow bounceInLeft"><img src="images/login/h2.jpg" alt=""></li>
            <li class="wow bounceInRight"><img src="images/login/h3.jpg" alt=""></li>
            <li class="wow bounceInRight" data-wow-delay="0.3s"><img src="images/login/h4.jpg" alt=""></li>
            <li class="wow bounceInLeft" data-wow-delay="0.3s"><img src="images/login/h5.jpg" alt=""></li>
            <li class="wow bounceInLeft"><img src="images/login/h6.jpg" alt=""></li>
            <li class="wow bounceInRight"><img src="images/login/h7.jpg" alt=""></li>
            <li class="wow bounceInRight" data-wow-delay="0.3s"><img src="images/login/h8.jpg" alt=""></li>
          </ul>
        </div>
      </div>
    </div>
  </section>    
  
  <!-- spacer 2 -->   
  <section id="spacer2" class="home-section spacer">  
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="color-light">
          <h2 class="wow bounceInDown" data-wow-delay="0.5s">Ya no importa donde te encuentras</h2>
          <p class="lead wow bounceInUp" data-wow-delay="1s">Ahora tu oficina viaja siempre contigo</p>  
          </div>
        </div>        
      </div>
    </div>
  </section>
    
  <!-- Contact -->
  <section id="contact" class="home-section bg-white">
    <div class="container">
      <div class="row">
        <div class="col-md-offset-2 col-md-8">
          <div class="section-heading">
            <h2>Cont&aacute;ctenos</h2>
            <p>Para cualquier duda o solicitud de acceso al sistema cont&aacute;ctenos a trav&eacute;s 
              de este formulario y nos pondremos en contacto con usted en las pr&oacute;ximas 24 horas.</p>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-offset-1 col-md-10">
          <form class="form-horizontal" role="form">
            <div class="form-group">
              <div class="col-md-offset-2 col-md-8">
                <input type="text" class="form-control" id="name" name="name" placeholder="Nombre">
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-offset-2 col-md-8">
                <input type="email" class="form-control" id="email" name="email" placeholder="Email">
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-offset-2 col-md-8">
                <input type="text" class="form-control" id="subject" name="subject" placeholder="Asunto">
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-offset-2 col-md-8">
                <textarea name="message" class="form-control" rows="3" placeholder="Mensaje"></textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-offset-2 col-md-8">
               <button type="button" class="btn btn-theme btn-lg btn-block">Enviar Mensaje</button>
              </div>
            </div>
          </form>
        </div>
      </div>

      <div class="row mar-top30">
        <div class="col-md-offset-2 col-md-8">
          <h5>Estamos en las redes sociales!</h5>
          <ul class="social-network">
            <li class="wow bounceInLeft">
              <a href="#">
                <span class="fa-stack fa-2x wow bounceInLeft">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li >
              <a href="#" >
                <span class="fa-stack fa-2x wow bounceInRight">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
          </ul>
        </div>       
      </div>
    </div>
  </section>

  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <p>&copy;2014 BarryBolivia. Todos los derechos reservados. Con el respaldo de <b>Barry Top Services S.R.L.</b></p>
        </div>
      </div>
    </div>
  </footer>
   
  <!-- js -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/jquery/jquery.smooth-scroll.min.js"></script>
  <script src="lib/jquery/jquery.dlmenu.js"></script>
  <script src="lib/js/wow.min.js"></script>
  <script src="js/login.js"></script>
</body>
</html>