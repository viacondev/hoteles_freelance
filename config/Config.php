<?php

class ZionConfig {
  public static $dbname = 'barrybol_zion_engine';
}

class HBConfig {
  // Test:
  // public static $XMLLocation = 'http://testapi.interface-xml.com/appservices/ws/FrontendService';
  // public static $WSDLLocation = 'http://testapi.interface-xml.com/appservices/ws/FrontendService?wsdl';

  // Live:
  public static $XMLLocation = 'http://api.interface-xml.com./appservices/ws/FrontendService';
  public static $WSDLLocation = 'http://api.interface-xml.com./appservices/ws/FrontendService?wsdl';

  // True if the server has the IP enabled for XML request. False to go throw BarryBolivia's wrapper
  public static $DirectXMLAccess = false;
}

class DTConfig {
  // Test:
   // public static $agencyName = 'BARRYXML';
   // public static $agencyCode = '20209';
   // public static $url = 'http://domitur.com/xml3/peticionlistatest.php';

  // Live:
  public static $agencyName = 'BARRYXML';
  public static $agencyCode = '20498';
  public static $url = 'http://domitur.com/xml3/peticionlista.php';
}

class MBConfig {
  // Test:
   // public static $agencyName = 'notificaciones@barrybolivia.com';
   // public static $agencyCode = 'DeT8bh3k';
   // public static $url = 'http://xml2.bookingengine.es/webservice/';

  // Live:
  // public static $agencyName = 'XMLbarry';
  // public static $agencyCode = 'H53Ne5SP';
  // public static $url = 'http://xmlusd.methabook.com/webservice/';

  //DOMITUR2
  public static $agencyName = 'xmlDOW_Barry';
  public static $agencyCode = 'Bdow!87370';
  public static $url = 'http://dow.juniperhub.com/webservice/';

  // True if the server has the IP enabled for XML request. False to go throw BarryBolivia's wrapper
  public static $DirectXMLAccess = false;
}

class MBBConfig {
  // Test:
  // public static $agencyName = 'CRTBarryBolivia';
  // public static $agencyCode = '0wkW9TWAX3vv';
  // public static $url = 'http://api-crt.iboosy.com/';

  // Live:
  public static $agencyName = 'PRODBarryBolivia';
  public static $agencyCode = 'jcLBAcB1GeVW';
  public static $url = 'http://api.iboosy.com/';

  // True if the server has the IP enabled for XML request. False to go throw BarryBolivia's wrapper
  public static $DirectXMLAccess = false;
}

class THConfig {
  // Test:
  // public static $agencyName = 'Bar107';
  // public static $agencyCode = '111111';
  // public static $locationHotelService = 'http://demo-hotelws.touricoholidays.com/HotelFlow.svc/bas/';
  // public static $wsdlHotelService = 'http://tourico.com/webservices/hotelv3/IHotelFlow/';
  //
  // public static $locationReservationService = 'http://demo-wsnew.touricoholidays.com/ReservationsService.asmx';
  // public static $wsdlReservationService = 'http://tourico.com/webservices/';

  // Live:
  public static $agencyName = 'Bar1072';
  public static $agencyCode = 'B@rr!123';

  public static $locationHotelService = 'http://thfwsv3.touricoholidays.com/ABHotelFlow.svc/bas';
  public static $wsdlHotelService     = 'http://tourico.com/webservices/hotelv3/IHotelFlow/';

  public static $locationReservationService = 'http://newws.touricoholidays.com/ApiBridgeReservationsService.asmx';
  public static $wsdlReservationService     = 'http://tourico.com/webservices/';

  public static $locationActivity = 'http://destservices.touricoholidays.com/DestinationsService.svc';
  public static $wsdlActivity     = 'http://touricoholidays.com/WSDestinations/2008/08/Contracts/IDestinationContracts/';

  public static $locationReservationBooks = 'http://reservationws.touricoholidays.com/ApiBridgeReservationService.svc/bas';
  public static $wsdlReservationBooks     = 'http://tourico.com/webservices/reservation/IReservationServices/';
  
  // True if the server has the IP enabled for XML request. False to go throw BarryBolivia's wrapper
  public static $DirectXMLAccess = false;
}

class RTConfig {
//credito
  // public static $codigousu  = 'BARRY';
  // public static $secacc     = '9994';
  // public static $clausu     = '320027';
  // public static $afiliacio  = 'HA';

  public static $codigousu  = 'LYHO';
  public static $secacc     = '102192';
  public static $clausu     = 'xml463874';
  public static $afiliacio  = 'RS';

  public static $url = 'http://xml.hotelresb2b.com/xml/listen_xml.jsp';
  
  public static $DirectXMLAccess = false; 
}

class TBConfig {
  // Demo:
  public static $agencyName = 'barrybolivia.WS.PRODUCCION';
  public static $agencyCode = 'wotki0-dovzem-jozbeM';
  public static $token      = 'b583ecb1bb90b3c6de0f7089eeac15fc';

  // public static $url = 'http://service.psurfer.net/pricesurfer/';
  public static $url = 'http://service.psurfer.net/pricesurfer/';
  public static $DirectXMLAccess = true; 
}

?>
