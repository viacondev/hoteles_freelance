<?php

class HotelAvailListRS extends HotelAvailList {

  public $serviceHotels;
  public $currentPage;
  public $totalPages;

  public function __construct($services, $currentPage = 1, $totalPages = 1) {
    $this->serviceHotels = $services;
    $this->currentPage   = $currentPage;
    $this->totalPages    = $totalPages;
  }

  public function getHotelsAvail() {
    return $this->serviceHotels;
  }

  public function getCurrentPage() {
    return $this->currentPage;
  }

  public function getTotalPages() {
    return $this->totalPages;
  }
}

?>