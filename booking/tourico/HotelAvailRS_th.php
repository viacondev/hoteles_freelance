<?php
namespace Tourico;

include_once('RoomAvail_th.php');

class HotelAvailRS extends \HotelAvail {

  private $element;
  private $destCode;
  private $rooms;
  public $dateFrom;
  public $dateTo;
  public $roomsAvail;
  public $hotelZionCode;

  public function __construct($element, $destCode, $dateFrom, $dateTo, $roomsList, $regimen = "", $factorFee = 1, $extraFee = 0) {
    $this->element  = $element;
    $this->roomsAvail   = array();
    $this->dateFrom = str_replace('-', '', $dateFrom);
    $this->dateTo   = str_replace('-', '', $dateTo);
    $this->destCode = $destCode;
    $roomCount = count($roomsList);
    // $adultCount = 0;
    // $childCount = 0;
    // foreach ($roomsList as $room) {
    //   $adultCount += $room->getAdultCount();
    //   $childCount += $room->getChildCount();
    // }

    foreach ($this->element->RoomTypes->RoomType as $value) {
      $groupedRooms = array();
      $seqNumber    = array();
      $r            = $roomsList[0];
      $attr         = $value->attributes();
      foreach ($value->Occupancies->Occupancy as $occupancy) {
        $seq = $occupancy->Rooms->Room->attributes();
        if (!in_array(strval($seq['seqNum']), $seqNumber)) {
          // los room se guian por un seqNumber a veces hay repeticiones de 1 hab por eso se maneja de numero relacionado a esa habitacion
          foreach ($occupancy->Rooms->Room as $room) {
            $childAge = array();
            $adultCount = strval($room->AdultNum);
            $childCount = strval($room->ChildNum);
            if ($room->ChildAges->ChildAge) {
              foreach ($room->ChildAges->ChildAge as $ChildAge) {
                $attrAge = $ChildAge->attributes();
                $childAge[] = strval($attrAge['age']);
              }
            }
            $roomgr = new RoomAvail($value, $occupancy, $this, $r, $adultCount, $childCount, $childAge, $roomCount, $factorFee, $extraFee);
            if ($regimen != '') {
              if (in_array($roomgr->getBoardCode(), $regimen)) {
                $groupedRooms[] = $roomgr;
                $this->roomsAvail[] = $roomgr;
              }
            }
            else {
              $groupedRooms[] = $roomgr;
              $this->roomsAvail[] = $roomgr;
            }
          }
          $seqNumber[] = strval($seq['seqNum']);
        }
      }

      if (count($groupedRooms) > 0) {
        $this->insertGroupedRoom($groupedRooms);
      }
      // $room = new RoomAvail($value, $this, $r, $adultCount, $childCount, $roomCount, $factorFee, $extraFee);
    }
  }

  public function getProvider() {
    return \BookingProvider::TOURICO;
  }

  public function getProviderName() {
    return 'TOURICO';
  }

  public function getAvailToken() {
    return '';
  }

  public function getCode() {
    $attrs = $this->element->attributes();
    return strval($attrs['hotelId']);
  }

  public function getHotelZionCode() {
    $attrs = $this->element->attributes();
    $this->hotelZionCode = strval($attrs['hotelId']);
    return $this->hotelZionCode;
  }

  public function getName() {
    $attrs =$this->element->attributes();
    return strval($attrs['name']);
  }

  public function getDateFrom() {
    return $this->dateFrom;
  }

  public function getDateTo() {
    return $this->dateTo;
  }

  public function getDestinationCode() {
    return $this->destCode;
  }

  public function getDestinationType() {
    return '';
  }

  public function getMainImage() {
    $res = $this->element->attributes();
    if (isset($res['thumb'])) {
      $img = strval($res['thumb']);
      return $img;
    }

    return 'assets/images/no-photo.jpg';
  }

  public function getDescription() {
    $res = $this->element->attributes();
    if(isset($res['desc'])) {
      return strval($res['desc']);
    }
    return '';
  }

  public function getLatitude() {
    $res = $this->element->Location->attributes();
    return strval($res['latitude']);
  }

  public function getLongitude() {
    $res = $this->element->Location->attributes();
    return strval($res['longitude']);
  }

  public function getZone() {
    return strval('');
  }

  public function getLocation() {
    $location = $this->element->Location->attributes();
    return strval($location['location']);
  }

  public function getCategory() {
    $star = array('1.5' => 'H1_5',
                  '2.5' =>'H2_5',
                  '3.5' => 'H3_5',
                  '3.7' => 'H3_5',
                  '4.5' => 'H4_5',
                  '5.5' => 'H5_5');
    $category = $this->element->attributes();
    $starval = strval($category['starsLevel']);
    if (strpos($starval, '.')) {
      return $star[$starval];
    }
    return strval($category['starsLevel']);
  }

  public function getCategoryType() {
    return '';
  }

  public function getCategoryCode() {
    $star = array('1.5' => 'H1_5',
                  '2.5' => 'H2_5',
                  '3.5' => 'H3_5',
                  '3.7' => 'H3_5',
                  '4.5' => 'H4_5',
                  '5.5' => 'H5_5');
    $category = $this->element->attributes();
    $starval = strval($category['starsLevel']);
    if (strpos($starval, '.')) {
      return $star[$starval];
    }
    return strval($category['starsLevel']) . 'EST';
  }

  public function getRoomsAvail() {
    return $this->roomsAvail;
  }

  public function getCurrency() {
    $currency = $this->element->attributes();
    return strval($currency['currency']);
  }

  public function getContractName() {
    return '';
  }

  public function getContractIncomingOfficeCode() {
    return '';
  }

  public function getContractComment() {
    return '';
  }

  public function getClientComment() {
    return '';
  }

  public function getSupplier() {
    return '';
  }

  public function getSupplierVatNumber() {
    return '';
  }

  public function getDiscountList() {
    return array();
  }

  public function getAllPaxRequired() {
    return false;
  }

  public function getXtraDataRequired() {
    return false;
  }

  public function getIsWithPromotion() {
    $attrs = $this->element->attributes();
    $ispromotion = "negative";
    if (strval($attrs['bestValue']) == "true") {
      return "confirm";
    }
    return $ispromotion;
  }
}

?>
