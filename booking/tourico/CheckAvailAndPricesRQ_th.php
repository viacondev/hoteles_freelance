<?php
namespace Tourico;

class CheckAvailAndPricesRQ {

  public $destCode;
  public $checkin;
  public $checkout;
  public $rooms;
  public $hotelId;
  public $childAges;
  public function __construct($destCode, $checkin, $checkout, $hotelId = '', $rooms, $childAges) {
    $this->destCode = $destCode;
    $this->checkin  = $checkin;
    $this->checkout = $checkout;
    $this->rooms    = $rooms;
    $this->hotelId  = $hotelId;
    $this->childAges = $childAges;
  }

  public function getType() {
    return 30;
  }

  public function getMethodName() {
    return 'CheckAvailabilityAndPrices';
  }

  public function getParams() {
    $room_cad = '<hot1:RoomsInformation>';

    foreach ($this->rooms as $room) {
      $childstr = '';
      $childIndex = 0;
      for($i = 0; $i < $room->childCount; $i++) {
        $childstr .= '<hot1:ChildAge age="' . $room->childAges[$childIndex] . '"/>';
        $childIndex++;
      }
      $room_cad .= '<hot1:RoomInfo>
                      <hot1:AdultNum>' . $room->adultCount . '</hot1:AdultNum>
                      <hot1:ChildNum>' . $room->childCount . '</hot1:ChildNum>
                      <hot1:ChildAges>
                        ' . $childstr . '
                      </hot1:ChildAges>
                    </hot1:RoomInfo>';
    }
    $room_cad .= '</hot1:RoomsInformation>';
    if ($this->hotelId != '') {
      $conca = '<hot1:HotelIdsInfo>
                  <hot1:HotelIdInfo id="' . $this->hotelId .'"/>
                 </hot1:HotelIdsInfo>';
      $type = 'CheckAvailabilityAndPrices';
    }
    $xml = '
      <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:aut="http://schemas.tourico.com/webservices/authentication" xmlns:hot="http://tourico.com/webservices/hotelv3" xmlns:hot1="http://schemas.tourico.com/webservices/hotelv3">
        <soapenv:Header>
          __POS__
        </soapenv:Header>
        <soapenv:Body>
          <hot:' . $type . '>
           <hot:request>
              ' . $conca . '
              <hot1:CheckIn>' . $this->checkin . '</hot1:CheckIn>
              <hot1:CheckOut>' . $this->checkout . '</hot1:CheckOut>
              ' . $room_cad . '
              <hot1:MaxPrice>0</hot1:MaxPrice>
              <hot1:StarLevel>0</hot1:StarLevel>
              <hot1:AvailableOnly>true</hot1:AvailableOnly>
             </hot:request>
           </hot:' . $type . '>
        </soapenv:Body>
      </soapenv:Envelope>';

    return $xml;
  }
}

?>
