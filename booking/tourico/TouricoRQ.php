<?php
namespace Tourico;
// ini_set('display_errors', 1);
class TouricoRQ {

  private $location;
  private $client;
  private $user;
  private $pass;
  private $wsdl;
  public function __construct() {
    ini_set('soap.wsdl_cache_enabled', '0');
    ini_set('soap.wsdl_cache_ttl', '0');

    // $this->location  = \THConfig::$location;
    // $this->user = \THConfig::$agencyName;
    // $this->pass = \THConfig::$agencyCode;
    // $this->wsdl = \THConfig::$wsdl;

  }

  public function getRequestXML($RQElement) {
    $autentication = '
                  <m:AuthenticationHeader xmlns:m="http://schemas.tourico.com/webservices/authentication">
                    <m:LoginName>' . \THConfig::$agencyName . '</m:LoginName>
                    <m:Password>' . \THConfig::$agencyCode . '</m:Password>
                    <m:Culture>en_US</m:Culture>
                    <m:Version>8</m:Version>
                  </m:AuthenticationHeader>';
    if ($RQElement->getType() == 'reservationService') {
      $autentication = '
                      <web:LoginHeader>
                        <trav:username>' . \THConfig::$agencyName . '</trav:username>
                        <trav:password>' . \THConfig::$agencyCode . '</trav:password>
                        <trav:culture>en_US</trav:culture>
                        <trav:version>7.123</trav:version>
                      </web:LoginHeader>';
    }
    if ($RQElement->getType() == 'reservationBooks') {
      $autentication = '
                  <res:LoginHeader>
                    <res:UserName>' . \THConfig::$agencyName . '</res:UserName>
                    <res:Password>' . \THConfig::$agencyCode . '</res:Password>
                    <res:Culture>en_US</res:Culture>
                    <res:Version>8</res:Version>
                  </res:LoginHeader>';
    }

    $res = $RQElement->getParams();
    $xml = str_replace('__POS__', $autentication, $res);
    return $xml;
  }

  public function execRequest($RQElement, $solicitud = "", $type = "") {
    // $request = $this->getRequestXML($RQElement->getParams());
    $url = \THConfig::$locationHotelService;
    $wsdl = \THConfig::$wsdlHotelService;
    if ($RQElement->getType() == 'reservationService') {
      $url = \THConfig::$locationReservationService;
      $wsdl = \THConfig::$wsdlReservationService;
    }
    else if ($RQElement->getType() == 'reservationBooks') {
      $url = \THConfig::$locationReservationBooks;
      $wsdl = \THConfig::$wsdlReservationBooks;
    }
    $header = array();
    $header[] = 'Cache-Control: no-cache';
    $header[] = 'Content-Type: text/xml';
    $header[] = 'SOAPAction: ' . $wsdl . $RQElement->getMethodName();
    if (isset(\THConfig::$DirectXMLAccess) && \THConfig::$DirectXMLAccess) { //falta incluir el archivo rest para que funcione

      $ch = curl_init();
      curl_setopt_array($ch, array(
                                CURLOPT_URL => $url,
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => "",
                                /*CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 30,*/
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "POST",
                                CURLOPT_POSTFIELDS => $this->getRequestXML($RQElement),
                                CURLOPT_HTTPHEADER => $header,
                                CURLOPT_USERAGENT => $_SERVER['HTTP_USER_AGENT'])
                      );
      $xml = curl_exec($ch);
      curl_close($ch);
    }
    else {
      $rq = 'http://test.barrybolivia.com/booking/tourico/RemoteRequestWrapper_th.php';
      $rq .= '?url=' . urlencode($url);
      $rq .= '&header=' . urlencode(json_encode($header));
      $rq .= '&request=' . urlencode($this->getRequestXML($RQElement));

      // __logtxt($this->getRequestXML($RQElement));
      $context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));
      $xml = file_get_contents($rq, false, $context);
    }
    if($solicitud == 'XML_RQ') {
      $descrip = $type;
      $params  = $this->getRequestXML($RQElement);
      \Log::info($params, $descrip);
    }
    // __logtxt($this->getRequestXML($RQElement));
    // __logtxt($xml); // RS
    return $xml;
  }

  public function allOptions($RQElement)
  {
    $url = \THConfig::$locationHotelService;
    $wsdl = \THConfig::$wsdlHotelService;
    if ($RQElement->getType() == 'reservationService') {
      $url = \THConfig::$locationReservationService;
      $wsdl = \THConfig::$wsdlReservationService;
    }
    else if ($RQElement->getType() == 'reservationBooks') {
      $url = \THConfig::$locationReservationBooks;
      $wsdl = \THConfig::$wsdlReservationBooks;
    }
    $header = array();
    $header[] = 'Cache-Control: no-cache';
    $header[] = 'Content-Type: text/xml';
    $header[] = 'SOAPAction: ' . $wsdl . $RQElement->getMethodName();
    if (isset(\THConfig::$DirectXMLAccess) && \THConfig::$DirectXMLAccess) {
      $options  = array(CURLOPT_URL => $url,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => $this->getRequestXML($RQElement),
                        CURLOPT_HTTPHEADER => $header,
                        CURLOPT_USERAGENT => $_SERVER['HTTP_USER_AGENT']);
    }
    else {
      $rq = 'http://test.barrybolivia.com/booking/tourico/RemoteRequestWrapper_th.php';
      $rq .= '?url=' . urlencode($url);
      $rq .= '&header=' . urlencode(json_encode($header));
      $rq .= '&request=' . urlencode($this->getRequestXML($RQElement));
      $options  = array(CURLOPT_AUTOREFERER => true, 
                        CURLOPT_HEADER => 0, 
                        CURLOPT_RETURNTRANSFER => 1, 
                        CURLOPT_URL             => $rq);
    }
    return $options;
  }

}

?>
