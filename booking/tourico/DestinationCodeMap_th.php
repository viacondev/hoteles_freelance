<?php
namespace Tourico;

class DestinationCodeMap {

  function __construct() {
  }

  public static function getDestCode($destCode) {
    $destinos = array('CUN' => 'CUN',
                      'MIA' => 'MIA',
                      'PUJ' => 'PUJ',
                      'RIO' => 'RIO',
                      'MCO' => 'MCO',
                      'BUE' => 'BUE',
                      'BCN' => 'BCN',
                      'BOG' => 'BOG',
                      'CTG' => 'CTG'
                      );
    if (isset($destinos[$destCode])) {
      return $destinos[$destCode];
    }
    return NULL;
  }
}

?>
