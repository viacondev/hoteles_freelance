<?php
namespace Tourico;


class ConfirmHotelAvail extends \HotelAvail {

  public $data;
  public $rules;
  public $roomsAvail;
  public $item;

  public function __construct($data, $item, $paxes, $totalAmount) {

    $this->item   = $item;
    $this->data   = $data->ResGroup;
    $this->rules  = $data->cancellationPolicy;
    $res  = $this->data->Reservations->Reservation->attributes();
    $from = explode('T', $res['fromDate']);

    $r = $this->data->Reservations->Reservation->attributes();
    $to               = explode('T', $r['toDate']);
    $child_position   = 0;
    $this->roomsAvail = array();
    $roomCode         = $this->data->Reservations->Reservation->ProductInfo->RoomExtraInfo->attributes();
    // $hotelInfo = $data->ResGroup->Reservations->Reservation->ProductInfo->attributes();
    $params =  array('hotelId'    => $this->getCode(),
                     'hotelRoomId' => strval($roomCode['hotelRoomTypeId']),
                     'dateFrom'   => $from[0],
                     'dateTo'     => $to[0]);
    $res  = new HotelBookingRuleRQ($params);
    $rq   = new TouricoRQ();
    $xml  = $rq->execRequest($res, 'XML_RQ', 'BB_TH_RQ_POLICY2');
    \Log::info($xml, 'BB_TH_RS_POLICY2');
    $res  = new TouricoRS($xml);
    $this->rules = $res->getResponse();
    foreach ($data->ResGroup->Reservations->Reservation as $key => $room) {
      $res        = $room->attributes();
      $totalRoom  = floatval($res['price']);
      $arr_child  = array();
      for ($i = 0; $i < $room->childCount; $i++) {
        $arr_child[] = $this->item->childAges[$child_position];
        $child_position++;
      }
      $this->roomsAvail[] = new ConfirmRoomAvail($room, $this, $from[0], $to[0], $arr_child, $this->rules, $paxes, $totalRoom);
    }

  }

  public function getProvider() {
    return \BookingProvider::TOURICO;
  }

  public function getAvailToken() {
    return '';
  }

  public function getCode() {
    $hotelCode = $this->data->Reservations->Reservation->ProductInfo->attributes();
    return strval($hotelCode['hotelId']);
  }

  public function getName() {
    $hotelName = $this->data->Reservations->Reservation->ProductInfo->attributes();
    return strval($hotelName['name']);
  }

  public function getDateFrom() {
    $res = $this->data->Reservations->Reservation->attributes();
    $res = explode('T', $res['fromDate']);
    // $checkin = $res;
    $checkin = explode('-', $res[0]);
    $checkin = $checkin[0] . $checkin[1] . $checkin[2];
    return $checkin;
  }

  public function getDateTo() {
    $res = $this->data->Reservations->Reservation->attributes();
    $res = explode('T', $res['toDate']);
    // $checkOut = $res;
    $checkOut = explode('-', $res[0]);
    $checkOut = $checkOut[0] . $checkOut[1] . $checkOut[2];
    return $checkOut;
  }

  public function getDestinationCode() {
    return $this->item->destCode;
  }

  public function getDestinationType() {
    return '';
  }

  public function getMainImage() {
    return '';
  }

  public function getDescription() {
    return '';
  }

  public function getLatitude() {
    return '';
  }

  public function getLongitude() {
    return '';
  }

  public function getZone() {
    return '';
  }

  public function getLocation() {
    return '';
  }

  public function getCategory() {
    $star = array('1.5' => 'H1_5',
                  '2.5' =>'H2_5',
                  '3.5' => 'H3_5',
                  '4.5' => 'H4_5',
                  '5.5' => 'H5_5');
    $category = $this->data->Reservations->Reservation->ProductInfo->attributes();
    $starval = strval($category['numOfStars']);
    if (strpos($starval, '.')) {
      return $star[$starval];
    }
    return strval($category['numOfStars']);
  }

  public function getCategoryType() {
    $categoryType = $this->data->Reservations->Reservation->ProductInfo->attributes();
    return strval($categoryType['roomTypeCategory']);
  }

  public function getCategoryCode() {
    $star = array('1.5' => 'H1_5',
                  '2.5' =>'H2_5',
                  '3.5' => 'H3_5',
                  '4.5' => 'H4_5',
                  '5.5' => 'H5_5');
    $category = $this->data->Reservations->Reservation->ProductInfo->attributes();
    $starval = strval($category['numOfStars']);
    if (strpos($starval, '.')) {
      return $star[$starval];
    }
    return strval($category['numOfStars']) . 'EST';
  }

  public function getRoomsAvail() {
    return $this->roomsAvail;
  }

  public function getGroupedRooms() {
    return $this->roomsAvail;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getContractName() {
    return '';
  }

  public function getContractIncomingOfficeCode() {
    return '';
  }

  public function getContractComment() {
    // return $this->rules->getBookingRulesDesc();
    //revisar los comentario
    return '';
  }

  public function getClientComment() {
    return '';
  }

  public function getSupplier() {
    return '';
  }

  public function getSupplierVatNumber() {
    return '';
  }

    public function getDiscountList() {
    return array();
  }

  public function areAllPaxRequired() {
    return true;
  }

  public function isExtraDataRequired() {
    return false;
  }

  public function isDataRequired() {
    return false;
  }

  public function getIsWithPromotion() {
    return array();
  }
}

?>
