<?php
namespace Tourico;

class CancellationFeeRQ {

  private $localizer;

  public function __construct($localizer) {
    $this->localizer = $localizer;
  }

  public function getType() {
    return 'reservationService';
  }

  public function getMethodName() {
    return 'GetCancellationFee';
  }

  public function getParams() {
    $dateNow = date('Y-m-d');
    $xml = '
          <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://tourico.com/webservices/" xmlns:trav="http://tourico.com/travelservices/">
            <soapenv:Header>
              __POS__
            </soapenv:Header>
            <soapenv:Body>
              <web:GetCancellationFee>
                  <web:nResID>' . $this->localizer . '</web:nResID>
                  <web:clxDate>' . $dateNow . '</web:clxDate>
              </web:GetCancellationFee>
            </soapenv:Body>
          </soapenv:Envelope>';

    return $xml;
  }
}
?>
