<?php
namespace Tourico;

/**
 * Class used for confirmed Services
 */
class PurchasedServiceRS extends \BookingService {

  private $elements;

  public function __construct($rs) {
    $this->elements = $rs->Body->GetRGInfoResponse->GetRGInfoResult;
  }

  public function getLocalizer() {
    $localizer = $this->elements->ResGroup->attributes();
    return strval($localizer['rgId']);
  }

  public function getSPUI() {
    $rsId = '';
    foreach ($this->elements->ResGroup->Reservations->Reservation as $reservation) {
      $attr = $reservation->attributes();
      $rsId .= strval($attr['reservationId']) . ' /';
      //reservationId se necesita para cancelar lareserva
    }
    $rsId = substr($rsId, 0, -1);
    return $rsId;
  }

  public function getServiceType() {
    return 'ServiceHotel';
  }

  public function getServiceStatus() {
    return '';
  }

  public function getDirectPayment() {
    return '';
  }

  public function getDateFrom() {
    $res = $this->elements->ResGroup->Reservations->Reservation->attributes();
    $res = explode('T', $res['fromDate']);
    $checkin = explode('-', $res[0]);
    $checkin = $checkin[0] . $checkin[1] . $checkin[2];
    return $checkin;
  }

  public function getDateTo() {
    $res = $this->elements->ResGroup->Reservations->Reservation->attributes();
    $res = explode('T', $res['toDate']);
    $checkOut = explode('-', $res[0]);
    $checkOut = $checkOut[0] . $checkOut[1] . $checkOut[2];
    return $checkOut;
  }

  public function getCurrency() {
    $currency = $this->elements->ResGroup->attributes();
    return strval($currency['currency']);
  }

  public function getTotalAmount() {
    $totalAmount = $this->elements->ResGroup->attributes();
    return floatval($totalAmount['totalPrice']);
  }

  public function getAdditionalCosts() {
    return array();
  }

  public function getServiceInfo() {
    $accommodations = $this->elements;
    return new PurchasedHotelRS($accommodations);
  }

  public function getSupplementList() {
    return array();
  }

  public function getStatus() {
    $status = $this->elements->status;

    if ($status == 'Confirmed') {
      return \ServiceStatus::CONFIRMED;
    }
  }

  public function getErrors() {
    return $this->errors;
  }

}

?>
