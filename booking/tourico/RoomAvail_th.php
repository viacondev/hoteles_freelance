<?php
namespace Tourico;

class RoomAvail extends \RoomAvail {

  private $element;
  private $occupancy;
  public $hotel;
  public $priceWithFee;
  public $adultCount;
  public $childCount;
  public $roomCount;
  public $price;
  public $childAges;

  public function __construct($element, $occupancy, $hotel, $r, $adultCount, $childCount, $childAges, $roomCount, $factorFee = 0.79, $extraFee = 0) {
    $this->element    = $element;
    $this->occupancy  = $occupancy;
    $this->hotel      = $hotel;
    $this->hotelCode  = $hotel->getCode();
    $this->hotel      = json_encode($this->buildObjecArray());
    $attrsSuplement   = $this->occupancy->SelctedSupplements->Supplement[0];//Price Suplement
    $attrs            = $this->occupancy->BoardBases->Boardbase[0];//Price Boardbase
    $attrPrice        = $this->occupancy->attributes();//Price Ocupancy
    $this->price      = floatval($attrPrice['occupPrice']) + floatval($attrs['bbPrice']);
    if (strval($attrsSuplement['suppChargeType']) == 'Addition') {
      $this->price += floatval($attrsSuplement['price']);
    }

    $priceWithFee       = floatval($this->price) / $factorFee;
    $this->priceWithFee = $priceWithFee;
    $this->adultCount   = $adultCount;
    $this->childCount   = $childCount;
    $this->ChildAges    = $childAges;
    // $this->roomCount  = count($this->occupancy->Rooms->Room);
  }

  public function getProvider() {
    return \BookingProvider::TOURICO;
  }

  public function getHotel() {
    return $this->hotel;
  }

  public function getProviderName() {
    return 'TOURICO';
  }

  public function getHotelCode() {
    return strval($this->hotelCode);
  }

  public function getRoomCount() {
    // return $this->roomCount;
    return 1;
  }

  public function getAdultCount() {
    return $this->adultCount;
  }

  public function getChildCount() {
    return $this->childCount;
  }

  public function getChildAges() {
    return $this->ChildAges;
  }

  public function getPrice() {
    return round($this->price, 2);
  }

  public function getPriceWithFee() {
    return round($this->priceWithFee, 2);
  }

  public function getSHRUI() {
    $roomId = $this->element->attributes();
    return floatval($roomId['hotelRoomTypeId']);
  }

  public function getOnRequest() {
    return '';
  }

  public function getBoard() {
    $board    = $this->element->Occupancies->Occupancy->BoardBases;
    $roomBoard = isset($board->Boardbase) ? $board->Boardbase->attributes() : '';
    $room     = $this->element->attributes();
    $roomType = strval($room['name']);
    $regimen = "";
    if (isset($roomBoard['bbName']) && strval($roomBoard['bbName']) == 'All Inclusive') {
      $regimen = 'Todo Incluido';
    }
    else if ($roomBoard != '') {
      $regimen = strval($roomBoard['bbName']);
    }
    if ($regimen == "" && preg_match('/All Inclusive|Breakfast|Continental Breakfast/i', $roomType)) {
      $regimen = $roomType;
    }
    if ($regimen == "") {
      $regimen = "Solo Habitaci&oacute;n";
    }

    return $regimen;
  }

  public function getBoardCode() {
    $arrCode = array('9'        => 'TI', //Todo ICLUIDO
                     '1'        => 'AD', //Desayuno
                     '1000117'  => 'AD',
                     '4'        => 'AD',
                     '1000360'  => 'AD',
                     '1004877' => 'AD',
                     '1004893'  => 'AD',
                     '1006652'  => 'AD',
                     '14'       => 'HB'//MEDIA PENSION
                     );
    $board       = $this->element->Occupancies->Occupancy->BoardBases;
    $roomBoard   = isset($board->Boardbase) ? $board->Boardbase->attributes() : '';
    $regimenCad  = isset($roomBoard['bbId']) && strval($roomBoard['bbId']) ? strval($roomBoard['bbId']) : '';

    if (isset($arrCode[$regimenCad])) {
      return $arrCode[$regimenCad];
    }
    $room     = $this->element->attributes();
    $roomType = strval($room['name']);
    if (preg_match('/All Inclusive|Breakfast|Continental Breakfast/i', $roomType)) {
      $arrRegimen = array('all inclusive' => 'TI', 'breakfast' => 'AD', 'continental breakfast' => 'AD');
      $found      = array();
      $value      = preg_match('/All Inclusive|Breakfast|Continental Breakfast/i', $roomType, $found);
      $arrvalue   = strtolower($found[0]);
      return $arrRegimen[$arrvalue];
    }
    return 'SA';
  }

  public function getBoardType() {
    return '';
  }

  public function getRoomType() {
    $room = $this->element->attributes();
    return strval($room['name']);
  }

  public function getRoomTypeType() {
    $room = $this->element->attributes();
    return strval($room['roomTypeCategory']);
  }

  public function getRoomTypeCode() {
    $room = $this->element->attributes();
    return strval($room['hotelRoomTypeId']);
  }

  public function getRoomTypeCharacteristic() {
    return '';
  }

  public function getCancellationPolicies() {
    return array();
  }

  public function getGuestList() {
    return array();
  }

  public function buildObjecArray() {
    $con = 0;
    $hotel = $this->hotel;
    $methods = get_class_methods($hotel);
    $objArray = array();
    foreach ($methods as $method) {
      if (substr($method, 0, 2) == '__') continue;
      if (substr($method, 0, 3) === 'get' && $method != 'getGroupedRooms' && $method != 'getRoomsAvail') {
        $key = lcfirst(substr($method, 3));
        $objArray[$key] = $hotel->$method();
        $con++;
      }
    }
    return $objArray;
  }
}

?>
