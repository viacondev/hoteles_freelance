<?php
namespace Tourico;

include_once('SCartHotelAvailRS_th.php');

class SCartServiceRS extends \BookingService {

  private $data;
  private $element;
  public  $hotelDetail;
  // private $localizer;

  public function __construct($data, $localizer = '', $total_price = 0) {
    $this->data = json_decode(json_encode($data), FALSE);
    $this->localizer    = $localizer;
    $this->$total_price = $total_price;
    $from               = dateFromYmdToDB($this->getDateFrom());
    $to                 = dateFromYmdToDB($this->getDateTo());

    $request            = new CheckAvailAndPricesRQ($this->data->destCode, $from, $to, $this->data->hotelCode, $this->data->rooms, $this->data->childAges);
    $rq                 = new TouricoRQ();
    $r                  = $rq->execRequest($request);
    $res                = new TouricoRS($r);
    $resSuplpement      = $res->getResponse();
    $this->element      = $resSuplpement->Body->CheckAvailabilityAndPricesResponse->CheckAvailabilityAndPricesResult->HotelList->Hotel;

    $hotelDetailRQ      = new HotelDetailRQ($this->data->hotelCode);
    $rqHotelDetail      = new TouricoRQ();
    $xml                = $rq->execRequest($hotelDetailRQ);
    $resHotelDetail     = new TouricoRS($xml);
    $resHotelDetailRS   = $resHotelDetail->getResponse();
    $this->hotelDetail  = $resHotelDetailRS->Body->GetHotelDetailsV3Response->GetHotelDetailsV3Result->TWS_HotelDetailsV3->Hotel;

  }

  public function getLocalizer() {
    return $this->localizer;
  }

  public function getSPUI() {
    return '';
  }

  public function getServiceType() {
    return 'ServiceHotel';
  }

  public function getServiceStatus() {
    return '';
  }

  public function getDirectPayment() {
    return '';
  }

  public function getDateFrom() {
    return $this->data->dateFrom;
  }

  public function getDateTo() {
    return $this->data->dateTo;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getTotalAmount() {
    if (isset($this->total_price) && $this->total_price != 0) {
      return $this->total_price;
    }

    $price = 0;
    foreach ($this->data->rooms as $room) {
      $price += $room->price;
    }
    return $price;
  }

  public function getTotalAmountWithFee() {
    if (isset($this->total_price) && $this->total_price != 0) {
      return round($this->total_price / $this->data->factorFee, 2);
    }

    $price = 0;
    foreach ($this->data->rooms as $room) {
      $price += $room->price;
    }
    return round($price / $this->data->factorFee, 2);
  }

  public function getAdditionalCosts() {
    return array();
  }

  public function getServiceInfo() {
    return new SCartHotelAvailRS($this->data, $this->hotelDetail, $this->element, $this->getTotalAmount());
  }

  public function getSupplementList() {
    $roomType     = '';
    $roomId       = $this->data->rooms[0]->SHRUI;
    $arrSuplement = array();
    $seqNumber    = array();
    if (isset($this->element->RoomTypes)) {
      foreach ($this->element->RoomTypes->RoomType as $key => $value) {
        $attrRoom = $value->attributes();
        if (strval($attrRoom['hotelRoomTypeId']) == $roomId) {
          $roomType = $value;
          break;
        }
      }

      // $suplementList = $this->element->RoomTypes->RoomType->Occupancies;
      foreach ($roomType->Occupancies->Occupancy as $value) {
        $seq = $value->Rooms->Room->attributes();
        if (in_array(strval($seq['seqNum']), $seqNumber)) {
          continue;
        }
        if (isset($value->SelctedSupplements->Supplement)) {
          $suplement = $value->SelctedSupplements->Supplement[0];
          $rs = $suplement->attributes();
          $msg = ' <strong>Incluido en el Precio</strong>';
          if (strval($rs['suppChargeType']) == 'AtProperty') {
            $msg = ' <strong>a pagarse en el Hotel</strong>';
          }
          $price = floatval($rs['price']);
          $descrip = strval($rs['suppName']) . $msg;
          $arrSuplement[] = new \BookingPrice($price, $descrip, $this->data->factorFee);
        }
      }
    }
    return $arrSuplement;
  }

  public function getStatus() {
    return \ServiceStatus::PENDING;
  }

}

?>
