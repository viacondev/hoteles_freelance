<?php
namespace Tourico;

class CancellBookRQ {

  private $localizer;

  public function __construct($localizer) {
    $this->localizer = $localizer;
  }

  public function getType() {
    return 'reservationService';
  }

  public function getMethodName() {
    return 'CancelReservation';
  }

  public function getParams() {
    $xml = '
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://tourico.com/webservices/" xmlns:trav="http://tourico.com/travelservices/">
          <soapenv:Header>
            __POS__
          </soapenv:Header>
          <soapenv:Body>
            <web:CancelReservation>
               <web:nResID>' . $this->localizer . '</web:nResID>
            </web:CancelReservation>
          </soapenv:Body>
        </soapenv:Envelope>';

    return $xml;
  }
}
?>
