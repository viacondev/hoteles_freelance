<?php
namespace Tourico;

class BookDetailRQ {

  private $checkin;
  private $checkout;

  public function __construct($checkin, $checkout) {
    $this->checkin = $checkin;
    $this->checkout = $checkout;
  }

  function getType() {
    return "reservationBooks";
  }

  function getMethodName() {
    return 'GetPreviousReservations';
  }

  public function getParams() {
    $xml = '<?xml version="1.0" encoding="UTF-8"?>
              <soapenv:Envelope
                  xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                  xmlns:res="http://tourico.com/webservices/reservation"
                  xmlns:res1="http://schemas.tourico.com/webservices/reservation">
                  <soapenv:Header>
                    __POS__
                  </soapenv:Header>
              <soapenv:Body>
                <res:GetPreviousReservations>
                    <!--Optional:-->
                    <res:PreviousReservationsRequest>
                        <res1:ResDateFrom>' . $this->checkin . '</res1:ResDateFrom>
                        <res1:ResDateTo>' . $this->checkout . '</res1:ResDateTo>
                        <res1:ServiceType>Hotel</res1:ServiceType>
                        <!--Optional:-->
                        <res1:ResStatus>
                            <!--Zero or more repetitions:-->
                            <res1:Status>Confirm</res1:Status>
                        </res1:ResStatus>
                    </res:PreviousReservationsRequest>
                </res:GetPreviousReservations>
              </soapenv:Body>
            </soapenv:Envelope>';
    return $xml;
  }
}
?>
