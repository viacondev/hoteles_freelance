<?php
namespace Tourico;

class ReadServiceRQ {

  private $bookId;
  public function __construct($bookId) {
    $this->bookId = $bookId;
  }

  public function getType() {
    return 'hotelService';
  }

  function getMethodName() {
    return 'GetRGInfo';
  }

  function getParams() {
    $xml = '
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:aut="http://schemas.tourico.com/webservices/authentication" xmlns:hot="http://tourico.com/webservices/hotelv3" xmlns:hot1="http://schemas.tourico.com/webservices/hotelv3">
          <soapenv:Header>
            __POS__
          </soapenv:Header>
          <soapenv:Body>
            <hot:GetRGInfo>
               <hot:request>
                  <hot1:RGId>' . $this->bookId . '</hot1:RGId>
               </hot:request>
            </hot:GetRGInfo>
          </soapenv:Body>
        </soapenv:Envelope>';
    return $xml;
  }

}
?>
