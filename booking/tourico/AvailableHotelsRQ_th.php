<?php
namespace Tourico;

class AvailableHotelsRQ {

  public $destCode;
  public $checkin;
  public $checkout;
  public $rooms;
  public $hotelId;
  public function __construct($destCode, $checkin, $checkout, $hotelId = '', $rooms) {
    $this->destCode = $destCode;
    $this->checkin  = $checkin;
    $this->checkout = $checkout;
    $this->rooms    = $rooms;
    $this->hotelId  = $hotelId;
  }

  public function getType() {
    return 'hotelService';
  }

  public function getMethodName() {
    if (count($this->hotelId) > 0) {
      return 'SearchHotelsById';//si solo es busqueda de 1 solo hotel en especifico
    }
    return 'SearchHotels';
  }

  public function getParams() {
    $room_cad = '<hot1:RoomsInformation>';
    foreach ($this->rooms as $room) {
      $room_cad .= '<hot1:RoomInfo>';
      $room_cad .= $room->getParams();
      $room_cad .= '</hot1:RoomInfo>';
    }
    $room_cad .= '</hot1:RoomsInformation>';
    $type = 'SearchHotels';
    $conca = '<hot1:Destination>' . $this->destCode . '</hot1:Destination>
              <hot1:HotelLocationName></hot1:HotelLocationName>
              <hot1:HotelName></hot1:HotelName>';
    if (count($this->hotelId) > 0) {
      $cad_hotel = '';
      foreach ($this->hotelId as $value) {
        $cad_hotel .= '<hot1:HotelIdInfo id="' . $value .'"/>';
      }
      $conca = '<hot1:HotelIdsInfo>
                  ' . $cad_hotel . '
                 </hot1:HotelIdsInfo>';
      $type = 'SearchHotelsById';
    }
    $xml = '
      <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:aut="http://schemas.tourico.com/webservices/authentication" xmlns:hot="http://tourico.com/webservices/hotelv3" xmlns:hot1="http://schemas.tourico.com/webservices/hotelv3">
        <soapenv:Header>
          __POS__
        </soapenv:Header>
        <soapenv:Body>
          <hot:' . $type . '>
           <hot:request>
              ' . $conca . '
              <hot1:CheckIn>' . $this->checkin . '</hot1:CheckIn>
              <hot1:CheckOut>' . $this->checkout . '</hot1:CheckOut>
              ' . $room_cad . '
              <hot1:MaxPrice>0</hot1:MaxPrice>
              <hot1:StarLevel>0</hot1:StarLevel>
              <hot1:AvailableOnly>true</hot1:AvailableOnly>
              <hot1:PropertyType>NotSet</hot1:PropertyType>
              <hot1:ExactDestination>true</hot1:ExactDestination>
             </hot:request>
           </hot:' . $type . '>
        </soapenv:Body>
      </soapenv:Envelope>';

    return $xml;
  }
}

?>
