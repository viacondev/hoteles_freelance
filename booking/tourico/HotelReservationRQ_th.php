<?php
namespace Tourico;

class HotelReservationRQ {

  private $params;
  private $item;
  private $serviceIndex;
  private $availPrice;

  public function __construct($params, $item, $serviceIndex, $availPrice) {
    $this->params = $params;
    $this->item = $item;
    $this->serviceIndex = $serviceIndex;
    $this->availPrice = $availPrice->Body->CheckAvailabilityAndPricesResponse->CheckAvailabilityAndPricesResult->HotelList->Hotel;
  }

  public function getType() {
    return 'hotelService';
  }

  public function getMethodName() {
    return 'BookHotelV3';
  }

  public function getParams() {
    $p = $this->params;
    $i = $this->serviceIndex;
    $item = $this->item;
    // __logarr($item);
    // __logarr($p);
    $price = 0;
    $roomsXml = '';
    $numberOfRooms = count($item['rooms']);
    $currentPax = 1;

    $ocupationXml = '';
    $ocupindex = 1;
    $totalPrice = 0;
    $roomId = $item['rooms'][0]->SHRUI;
    $roomType = '';
    $seqNumber = array();
    foreach ($this->availPrice->RoomTypes->RoomType as $key => $value) {
      $attrRoom = $value->attributes();
      if (strval($attrRoom['hotelRoomTypeId']) == $roomId) {
        $roomType = $value;
        break;
      }
    }
    foreach ($roomType->Occupancies->Occupancy as $value) {
       $seq = $value->Rooms->Room->attributes();
       if (in_array(strval($seq['seqNum']), $seqNumber)) {
         continue;
       }
       $seqNumber[] = strval($seq['seqNum']);
       $attr = $value->attributes();
       foreach ($value->Rooms->Room as $room) {
         $totalPrice += floatval($attr['occupPrice']);
         $ocupationXml .= '<m0:RoomReserveInfo>';
         $ocupationXml .= '<m0:RoomId>' . $ocupindex . '</m0:RoomId>';
         $adultCount = intval($room->AdultNum);
         $childCount = intval($room->ChildNum);
         for ($r = 0; $r < $adultCount; $r++) {
            if ($p['customerName_1_' . $currentPax] == "") {
               $currentPax++;
               continue;
            }
            if ($currentPax == 1) {
                $ocupationXml .= '<m0:ContactPassenger>
                                     <m0:FirstName>' . $p['customerName_1_' . $currentPax] . '</m0:FirstName>
                                     <m0:LastName>' . $p['customerLastName_1_' . $currentPax] . '</m0:LastName>
                                     <m0:HomePhone>(+591) 72122266</m0:HomePhone>
                                     <m0:MobilePhone>(+591) 72122266</m0:MobilePhone>
                                  </m0:ContactPassenger>';
                $boardBase = $value->BoardBases;
                if (isset($boardBase->Boardbase)) {
                  $attrBoard = $boardBase->Boardbase[0]->attributes();
                  $totalPrice += floatval($attrBoard['bbPrice']);
                  $ocupationXml .= '<m0:SelectedBoardBase>
                                       <m0:Id>' . strval($attrBoard['bbId']) . '</m0:Id>
                                       <m0:Price>' . strval($attrBoard['bbPrice']) . '</m0:Price>
                                    </m0:SelectedBoardBase>';
                }
                $suplement = $value->SelctedSupplements;
                if (isset($suplement->Supplement)) {
                  $attrs = $suplement->Supplement[0]->attributes();
                  if (strval($attrs['suppChargeType']) == 'Addition') {
                    $totalPrice += floatval($attrs['price']);
                  }
                  $ocupationXml .= '<m0:SelectedSupplements>
                                       <m0:SupplementInfo suppId="' . strval($attrs['suppId']) . '" supTotalPrice="' . strval($attrs['price']) . '" suppType="' . strval($attrs['supptType']) . '">';
                  if (isset($suplement->SuppAgeGroup)) {
                    $attrsAge      = $suplement->SuppAgeGroup->SupplementAge;
                    $suplementAge  = $attrsAge->attributes();
                    $ocupationXml .= '<m0:SupAgeGroup>
                                         <m0:SuppAges suppFrom="' . strval($suplementAge['suppFrom']) . '" suppTo="' . strval($suplementAge['suppTo']) . '" suppQuantity="' . strval($suplementAge['suppQuantity']) . '" suppPrice="' . strval($suplementAge['suppPrice']) . '"/>
                                      </m0:SupAgeGroup>';
                  }
                 $ocupationXml  .=     '</m0:SupplementInfo>
                                    </m0:SelectedSupplements>';
                }
                $ocupationXml .= '<m0:AdultNum>' . $adultCount . '</m0:AdultNum>';
                $ocupationXml .= '<m0:ChildNum>' . $childCount . '</m0:ChildNum>';
            }
            $currentPax++;
          }
          $ocupationXml .= '<m0:ChildAges>';
          if (isset($room->ChildAges->ChildAge)) {
            foreach ($room->ChildAges->ChildAge as $childAge) {
              $attrAge = $childAge->attributes();
              $ocupationXml .= '<m0:ChildAge age="' . $attrAge['age'] . '"/>';
              $currentPax++;
            }
          }
          $ocupationXml .= '</m0:ChildAges>';
          $ocupationXml .= '</m0:RoomReserveInfo>';
          $ocupindex++;
       }
     }
     $roomsXml .= $ocupationXml;
     $checkIn   = dateFromYmdToDB($item['dateFrom']);
     $checkOut  = dateFromYmdToDB($item['dateTo']);
     $xml = '
            <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:m0="http://schemas.tourico.com/webservices/hotelv3">
              <SOAP-ENV:Header>
                __POS__
              </SOAP-ENV:Header>
            <SOAP-ENV:Body>
              <m:BookHotelV3 xmlns:m="http://tourico.com/webservices/hotelv3">
                <m:request>
                  <m0:RecordLocatorId>0</m0:RecordLocatorId>
                  <m0:HotelId>' . $item['hotelCode'] . '</m0:HotelId>
                  <m0:HotelRoomTypeId>' . $item['rooms'][0]->SHRUI . '</m0:HotelRoomTypeId>
                  <m0:CheckIn>' . $checkIn . '</m0:CheckIn>
                  <m0:CheckOut>' . $checkOut . '</m0:CheckOut>
                  <m0:RoomsInfo>
                      ' . $ocupationXml . '
                  </m0:RoomsInfo>
                  <m0:PaymentType>Obligo</m0:PaymentType>
                  <m0:AgentRefNumber></m0:AgentRefNumber>
                  <m0:ContactInfo>(+591) 72122266</m0:ContactInfo>
                  <m0:RequestedPrice>' . $totalPrice . '</m0:RequestedPrice>
                  <m0:DeltaPrice>2</m0:DeltaPrice>
                  <m0:Currency>USD</m0:Currency>
                  <m0:IsOnlyAvailable>true</m0:IsOnlyAvailable>
                </m:request>
              </m:BookHotelV3>
            </SOAP-ENV:Body>
          </SOAP-ENV:Envelope>';
    // __logtxt($xml);
    return $xml;
  }
}
?>
