<?php
namespace Tourico;

include_once('HotelAvailRS_th.php');

class HotelAvailListRS extends \HotelAvailList {

  private $elements;
  public $serviceHotels;

  public function __construct($rs, $destCode, $dateFrom, $dateTo, $roomsList, $hotelName, $regimen, $factorFee) {
    $this->serviceHotels = array();
    foreach ($rs->HotelList->Hotel as $value) {
      $hotelAvail = new HotelAvailRS($value, $destCode, $dateFrom, $dateTo, $roomsList, $regimen, $factorFee);
      $count      = count($hotelAvail->getRoomsAvail());
      if ($hotelName != '' && $count > 0) {
        $name = $hotelAvail->getName();
        if (preg_match("/$hotelName/i", $name)) {
          $this->serviceHotels[] = $hotelAvail;
        }
      }
      else {
        if ($count > 0) {
          $this->serviceHotels[] = $hotelAvail;
        }
      }
      // $this->serviceHotels[] = new HotelAvailRS($value, $destCode, $dateFrom, $dateTo, $roomsList, $factorFee);
    }
  }

  public function getHotelsAvail() {
    return $this->serviceHotels;
  }

  public function getCurrentPage() {
    return 1;
  }

  public function getTotalPages() {
    return 1;
  }
}

?>
