<?php
namespace Tourico;
// error_reporting(0);
ini_set('memory_limit', -1);
include_once('TouricoRQ.php');
include_once('Room_th.php');
include_once('AvailableHotelsRQ_th.php');
include_once('TouricoRS.php');
include_once('HotelAvailListRS_th.php');
include_once('PurchaseRS_th.php');
include_once('SCartServiceRS_th.php');
include_once('HotelBookingRuleRQ_th.php');
include_once('HotelReservationRQ_th.php');
include_once('ServiceRS_th.php');
include_once('ConfirmHotelAvail_th.php');
include_once('ConfirmRoomAvail_th.php');
include_once('HotelDetailRS_th.php');
include_once('HotelDetailRQ_th.php');
include_once('ReadServiceRQ_th.php');
include_once('PurchasedServiceRS_th.php');
include_once('PurchasedHotelRS_th.php');
include_once('CheckAvailAndPricesRQ_th.php');
include_once('CancellBookRQ_th.php');
include_once('BookDetailRQ_th.php');
include_once('CancellationFeeRQ_th.php');
include_once('DestinationCodeMap_th.php');

class TouricoBooking {
  private $request;

  public function __construct() {
    $this->request = new TouricoRQ();
  }

  public function execRequest($params) {
    switch ($params['requestType']) {
      case \BookingRequestTypes::SearchHotelAvail:
        return $this->execSearchHotelAvailRQ($params);
        break;

      case \BookingRequestTypes::AddHotelService:
        return $this->execHotelServiceAddRQ($params);
        break;

      case \BookingRequestTypes::ConfirmPurchase:
        return $this->execPurchaseConfirmRQ($params);
        break;

      case \BookingRequestTypes::PurchaseDetail:
        return $this->execPurchaseDetailRQ($params);
        break;

      case \BookingRequestTypes::HotelDetail:
        return $this->execHotelDetailRQ($params);
        break;

      case \BookingRequestTypes::CancelPurchase:
        return $this->execPurchaseCancelRQ($params);
        break;

      case \BookingRequestTypes::PurchaseDetailList:
        return $this->execPurchaseDetailList($params);
        break;

      default:
        return NULL;
        break;
    }
  }

  public function execSearchHotelAvailRQ($p) {
    // if ($p['hotelCode'] != '' && substr($p['hotelCode'], 0, 2) != 'JP') return NULL;

    // Only search for page=1 since there is no pagination on MB
    // if ($p['page'] != 1) return NULL;
    $roomCount      = intval($p['roomCount']);
    $rooms          = array();
    $rooms_numbers  = array();
    $childIndex = 1;
    for ($i = 1;$i <= $roomCount; $i++) {
      $adultCount = intval($p["adultCount_$i"]);
      $childCount = intval($p["childCount_$i"]);
      $occupancy  = array();
      $childAges  = array();
      for  ($d = 1; $d <= intval($p["adultCount_$i"]); $d++) {
        $occupancy[] = 30;
      }

      for ($c = 1; $c <= intval($p["childCount_$i"]); $c++) {
        $occupancy[] = $p["childAge_$childIndex"];
        $childAges[] = $p["childAge_$childIndex"];
        $childIndex++;
      }
      $rooms_numbers[]  = new Room($adultCount, $childCount, $childAges);
      $rooms[]          = $occupancy;
    }
    $r = \Hotel_Mapping::getHotelByLimit($p['destCode'], \BookingProvider::TOURICO);

    $destCode   = $p['destCode'];
    $arr_hotels = array();
    if (!empty($p['hotelCode'])) {
      $zion         = \HotelMappingProvider::getHotelByZionCode(array('hotel_code' => $p['hotelCode'], 'provider' => \BookingProvider::TOURICO));
      $arr_hotels[] = $zion;
    }
    else if (!empty($p['zoneCode'])) {
      $res = \HotelMappingProvider::getHotelByZoneCode(array('destCode' => $destCode, 'zone_code' => $p['zoneCode'], 'provider' => \BookingProvider::TOURICO));
      foreach ($res as $hotel) {
        $arr_hotels[] = $hotel->code;
      }
      if (count($arr_hotels) == 0) {
        return array();
      }
    }
    else if (count($r) > 2 && empty($p['zoneCode']) && empty($p['hotelName'])) {
      foreach ($r as $value) {
        $arr_hotels[] = $value->hotel_code;
      }
    }

    $checkin  = substr($p['checkin'], 0, 4) . '-' . substr($p['checkin'], 4, 2) . '-' . substr($p['checkin'], 6, 4);
    $checkout = substr($p['checkout'], 0, 4) . '-' . substr($p['checkout'], 4, 2) . '-' . substr($p['checkout'], 6, 4);
    $request  = new AvailableHotelsRQ($destCode, $checkin, $checkout, $arr_hotels, $rooms_numbers);

    $profiling = new \Profiling('hotelAvail query', 'TouricoBooking');
    $profiling->init();
    
    $xml = $this->request->execRequest($request);
    $profiling->end(strlen($xml));

    $rs       = new TouricoRS($xml);
    $res      = $rs->getResponse();
    $regimen  = '';

    if(isset($p['regimen']) && $p['regimen'] != '') {
      $regimen = explode(',', implode(',', $p['regimen']));
    }
    if ($p['hotelCode'] != '' || count($r) > 2 && empty($p['hotelName'])) {
      $rs = $res->Body->SearchHotelsByIdResponse->SearchHotelsByIdResult;
    }
    else {
      $rs = $res->Body->SearchHotelsResponse->SearchHotelsResult;
    }
    $hotelName = '';
    if (isset($p['hotelName']) && $p['hotelName'] != '') { 
      $hotel = str_replace(" ", ',', $p['hotelName']);
      $hotel = explode(',', $hotel);
      foreach ($hotel as $value) {
        $hotelName .= trim($value) . "|";
      }
      $hotelName = substr($hotelName, 0, -1);
    }
    $result = new HotelAvailListRS($rs, $destCode, $checkin, $checkout, $rooms_numbers, $hotelName, $regimen, $p['factorFee']);

    if (isset($p['stars']) && $p['stars'] != '') {
      $star      = explode(',', implode(',', $p['stars']));
      $hotel     = $result->getHotelsAvail();
      $can_hotel = count($hotel);
      for ($i = 0; $i < $can_hotel; $i++) {
          $service = $hotel[$i];
          if (!in_array("'" . $service->getCategoryCode() . "'", $star)) {
            unset($result->serviceHotels[$i]);
          }
      }
    }
    
    return $result;
  }

  public function prepararConsulta($p) {
    $dest = DestinationCodeMap::getDestCode($p['destCode']);
    if ($dest == NULL) return array();
    $roomCount      = intval($p['roomCount']);
    $rooms          = array();
    $rooms_numbers  = array();
    $childIndex = 1;
    for ($i = 1;$i <= $roomCount; $i++) {
      $adultCount = intval($p["adultCount_$i"]);
      $childCount = intval($p["childCount_$i"]);
      $occupancy  = array();
      $childAges  = array();
      for  ($d = 1; $d <= intval($p["adultCount_$i"]); $d++) {
        $occupancy[] = 30;
      }

      for ($c = 1; $c <= intval($p["childCount_$i"]); $c++) {
        $occupancy[] = $p["childAge_$childIndex"];
        $childAges[] = $p["childAge_$childIndex"];
        $childIndex++;
      }
      $rooms_numbers[]  = new Room($adultCount, $childCount, $childAges);
      $rooms[]          = $occupancy;
    }
    // $r = \Hotel_Mapping::getHotelByLimit($p['destCode'], \BookingProvider::TOURICO);
    $r = array();
    $destCode   = DestinationCodeMap::getDestCode($p['destCode']);

    $arr_hotels = array();
    if (!empty($p['hotelCode'])) {
      $zion         = \HotelMappingProvider::getHotelByZionCode(array('hotel_code' => $p['hotelCode'], 'provider' => \BookingProvider::TOURICO));
      $arr_hotels[] = $zion;
    }
    else if (!empty($p['zoneCode'])) {
      $res = \HotelMappingProvider::getHotelByZoneCode(array('destCode' => $destCode, 'zone_code' => $p['zoneCode'], 'provider' => \BookingProvider::TOURICO));
      foreach ($res as $hotel) {
        $arr_hotels[] = $hotel->code;
      }
      if (count($arr_hotels) == 0) {
        return array();
      }
    }
    else if (count($r) > 2 && empty($p['zoneCode']) && empty($p['hotelName'])) {
      foreach ($r as $value) {
        $arr_hotels[] = $value->hotel_code;
      }
    }

    $checkin  = substr($p['checkin'], 0, 4) . '-' . substr($p['checkin'], 4, 2) . '-' . substr($p['checkin'], 6, 4);
    $checkout = substr($p['checkout'], 0, 4) . '-' . substr($p['checkout'], 4, 2) . '-' . substr($p['checkout'], 6, 4);
    $request  = new AvailableHotelsRQ($destCode, $checkin, $checkout, $arr_hotels, $rooms_numbers);

    return $this->request->allOptions($request);    
  }

  public function obtenerResultado($xml, $p) {

    $roomCount      = intval($p['roomCount']);
    $rooms          = array();
    $rooms_numbers  = array();
    $childIndex = 1;
    for ($i = 1;$i <= $roomCount; $i++) {
      $adultCount = intval($p["adultCount_$i"]);
      $childCount = intval($p["childCount_$i"]);
      $occupancy  = array();
      $childAges  = array();
      for  ($d = 1; $d <= intval($p["adultCount_$i"]); $d++) {
        $occupancy[] = 30;
      }

      for ($c = 1; $c <= intval($p["childCount_$i"]); $c++) {
        $occupancy[] = $p["childAge_$childIndex"];
        $childAges[] = $p["childAge_$childIndex"];
        $childIndex++;
      }
      $rooms_numbers[]  = new Room($adultCount, $childCount, $childAges);
      $rooms[]          = $occupancy;
    }
    // $r = \Hotel_Mapping::getHotelByLimit($p['destCode'], \BookingProvider::TOURICO);
    $r = array();
    $destCode   = $p['destCode'];
    $checkin  = substr($p['checkin'], 0, 4) . '-' . substr($p['checkin'], 4, 2) . '-' . substr($p['checkin'], 6, 4);
    $checkout = substr($p['checkout'], 0, 4) . '-' . substr($p['checkout'], 4, 2) . '-' . substr($p['checkout'], 6, 4);

    $rs       = new TouricoRS($xml);
    $res      = $rs->getResponse();
    $regimen  = '';

    if ($p['hotelCode'] != '' || count($r) > 2 && empty($p['hotelName']) || !empty($p['zoneCode'])) {
      $rs = $res->Body->SearchHotelsByIdResponse->SearchHotelsByIdResult;
    }
    else {
      $rs = $res->Body->SearchHotelsResponse->SearchHotelsResult;
    }
    $hotelName = '';
    $result = new HotelAvailListRS($rs, $destCode, $checkin, $checkout, $rooms_numbers, $hotelName, $regimen, $p['factorFee']);
    
    return $result;
  }

  public function execHotelServiceAddRQ($p) {
    $service = new SCartServiceRS($p);

    return new PurchaseRS(array($service));
  }

  public function execPurchaseConfirmRQ($p) {
    $services_count = intval($p['servicesCount']);
    $services = array();
    $errors = array();
    for ($i = 1; $i <= $services_count; $i++) {
      $book_params  = (array)json_decode($p['book_params']);
      $item         = $book_params;
      if ($item['provider'] != \BookingProvider::TOURICO) { continue; }

      $destCode  = $item["destCode"];
      $from      = dateFromYmdToDB($item["dateFrom"]);
      $to        = dateFromYmdToDB($item["dateTo"]);
      $hotelCode = $item["hotelCode"];
      $requestAvailPrice = new CheckAvailAndPricesRQ($destCode, $from, $to, $hotelCode,
                                                     $item["rooms"], $item["childAges"]);
      $resAvailPrice      = $this->request->execRequest($requestAvailPrice, 'XML_RQ', 'BB_TH_RQ_CHECKPRICE');
      \Log::info($resAvailPrice, 'BB_TH_RS_CHECKPRICE');
      $resultAvailPrice   = new TouricoRS($resAvailPrice);
      $HotelReservationRQ = new HotelReservationRQ($p, $item, $i , $resultAvailPrice->getResponse());

      $xml = $this->request->execRequest($HotelReservationRQ, 'XML_RQ', 'BB_TH_RQ_PURCHASE');
      \Log::info($xml, 'BB_TH_RS_PURCHASE');
      // __logtxt($xml);
      $result = new TouricoRS($xml);
      $result = $result->getResponse();
      if (isset($result->Body->Fault)) {
        $error      = $result->Body->Fault->detail->WSFault;
        $code       = strval($error->WSErrorCode);
        $msg        = strval($error->Description);
        $provider   = \BookingProvider::TOURICO;
        $errors[]   = array('code' => $code, 'msg' => $msg,'provider' => $provider);
        $services[] = new ServiceRS(array(), array(), $errors);
      }
      if (count($errors) == 0) {
        $services[] = new ServiceRS($result, $item, $errors);
      }

    }

    return new PurchaseRS($services);
  }

  function execPurchaseCancelRQ($p) {
    $localizers      = explode('|', $p['rs_id']);// reservation_id se crea 1 por habitacion
    $totalCancellationFee = 0;
    foreach ($localizers as $localizer) {
      $cancelFeeRQ    = new CancellationFeeRQ($localizer);
      $rsCancellFeeRQ = $this->request->execRequest($cancelFeeRQ, 'XML_RQ', 'BB_TH_RQ_CANCELFEE');
      $rsCancellFeeRS = new TouricoRS($rsCancellFeeRQ);
      $CancellFeeRS = $rsCancellFeeRS->getResponse();
      \Log::info($rsCancellFeeRQ, 'BB_TH_RS_CANCELFEE');

      $CancellBookRQ  = new CancellBookRQ($localizer);
      $rs = $this->request->execRequest($CancellBookRQ, 'XML_RQ', 'BB_TH_RQ_CANCELBOOK');
      \Log::info($rs, 'BB_TH_RS_CANCELBOOK');
      $cancellBookRS  = new TouricoRS($rs);

      $totalCancellationFee += floatval($CancellFeeRS->Body->GetCancellationFeeResponse->GetCancellationFeeResult->CancellationFeeValue);
    }

    return new PurchaseRS(array(), $totalCancellationFee);
  }

  public function execPurchaseDetailRQ($p) {
    $localizer      = explode('-', $p['localizer']);//separamos por que el primero es el rgId y el segundo el rsId
    $ReadServiceRQ  = new ReadServiceRQ($localizer[0]);

    $rs = $this->request->execRequest($ReadServiceRQ);
    // \Log::info(json_encode($rs), 'MB_RS_INFOBOOK');
    $serviceRS  = new TouricoRS($rs);
    $service    = new PurchasedServiceRS($serviceRS->getResponse());
    return new PurchaseRS(array($service));
  }

  public function execHotelDetailRQ($p) {
    $request = new HotelDetailRQ($p['hotelCode']);
    $rs      = $this->request->execRequest($request);
    $rs      = new TouricoRS($rs);
    return new HotelDetailRS($rs->getResponse());
  }

  public function execPurchaseDetailList($p) {
    $from       = substr($p['dateFrom'], 0, 4) . '-' . substr($p['dateFrom'], 4, 2) . '-' . substr($p['dateFrom'], 6, 4);
    $to         = substr($p['dateTo'], 0, 4) . '-' . substr($p['dateTo'], 4, 2) . '-' . substr($p['dateTo'], 6, 4);
    $list_books = array();
    $result     = new BookDetailRQ($from, $to);

    $rs         = $this->request->execRequest($result);
    $res        = new TouricoRS($rs);
    $res        = $res->getResponse();
    $bookings   = $res->Body->PreviousReservationsResponse->PreviousReservationsResults->ResGroups;
    foreach ($bookings->ResGroup as $book) {
      $hotel  = $book->Reservations->Reservation;
      $attrs  = $hotel->attributes();
      $b      = $book->attributes();
      $localizer = strval($b['rgId']);
      $locSub    = strval($attrs['resId']);
      $status    = 'Confirmed';
      if($status == 'Confirmed') {
        $status = \ServiceStatus::CONFIRMED;
      }
      else if($status == 'CancelledByCustomer') {
        $status = \ServiceStatus::CANCELLED;
      }
      $paxs_arr   = array();
      $passenger  = $hotel->Passengers->Passenger->attributes();
      $holder     = strval($passenger->firstName) . ' ' . strval($passenger->lastName);
      $paxs_arr[] = strval($holder);
      $list_books[] = array('localizer' => $localizer, 'locSub' => $locSub, 'holder' => $paxs_arr, 'status_provider' => $status);
    }

    return $list_books;
  }
  
}
?>
