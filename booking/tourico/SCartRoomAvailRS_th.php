<?php
namespace Tourico;

class SCartRoomAvailRS extends \RoomAvail {

  private $data;
  public $hotel;
  private $date_in;
  private $date_out;
  public $childAges;
  private $rules;
  private $factorFee;
  private $totalAmount;
  private $indexChild;

  public function __construct($data, $hotel, $date_in, $date_out, $childAges, $totalAmount, $rules, $indexChild, $factorFee = 0.79) {
    $this->data       = $data;
    $this->hotel      = $hotel;
    $this->date_in    = $date_in;
    $this->date_out   = $date_out;
    $this->childAges  = $childAges;
    $this->rules      = $rules->getResponse();
    $this->factorFee  = $factorFee;
    $this->totalAmount = $totalAmount;
    $this->indexChild  = $indexChild;
  }

  public function getProvider() {
    return \BookingProvider::METHABOOK;
  }

  public function getHotel() {
    return $this->hotel;
  }

  public function getProviderName() {
    return 'TURICO';
  }

  public function getHotelCode() {
    return '';
  }

  public function getRoomCount() {
    return $this->data->roomCount;
  }

  public function getAdultCount() {
    return $this->data->adultCount;
  }

  public function getChildCount() {
    return $this->data->childCount;
  }

  public function getPrice() {
    return $this->data->price;
  }

  public function getPriceWithFee() {
    return round($this->data->price / $this->factorFee, 2);
  }

  public function getSHRUI() {
    return $this->data->SHRUI;
  }

  public function getOnRequest() {
    return '';
  }

  public function getBoard() {
    return $this->data->board;
  }

  public function getBoardCode() {
    return $this->data->boardCode;
  }

  public function getBoardType() {
    return $this->data->boardType;
  }

  public function getRoomType() {
    return $this->data->roomType;
  }

  public function getRoomTypeType() {
    return $this->data->roomTypeType;
  }

  public function getRoomTypeCode() {
    return $this->data->roomTypeCode;
  }

  public function getRoomTypeCharacteristic() {
    return $this->data->roomTypeCharacteristic;
  }

  public function getCancellationPolicies() {
    $policies = array();
    $totalAmount = $this->totalAmount;
    $rules = $this->rules->Body->GetCancellationPoliciesResponse->HotelPolicy->RoomTypePolicy->CancelPolicy;
    foreach ($rules->CancelPenalty as $value) {
      $valueAmount  = $value->AmountPercent->attributes();
      $attrDate     = $value->Deadline->attributes();
      if (strval($attrDate['OffsetTimeUnit']) == 'Hour' && strval($attrDate['OffsetDropTime']) == 'BeforeArrival') {
        $days = intval($attrDate['OffsetUnitMultiplier']) / 24;
        if($days <= 0) {
          $dateFrom = new \DateTime($this->date_in);
        }
        else {
          $days = intval($attrDate['OffsetUnitMultiplier']) / 24;
          $date = new \DateTime($this->date_in);
          $dateFrom = date_sub($date, new \DateInterval('P' . $days .'D'));
        }
      }
      if (strval($attrDate['OffsetTimeUnit']) == 'Hour' && strval($attrDate['OffsetDropTime']) == 'AfterBooking') {
        $days = intval($attrDate['OffsetUnitMultiplier']) / 24;
        if($days <= 0) {
          $dateFrom = new \DateTime("now");
        }
        else {
          $days = intval($attrDate['OffsetUnitMultiplier']) / 24;
          $date = new \DateTime($this->date_in);
          $dateFrom = date_sub($date, new \DateInterval('P' . $days .'D'));
        }

      }
      if (strval($valueAmount['BasisType']) == 'Nights') {
        $night = intval($valueAmount['NmbrOfNights']);
        $amount = ($totalAmount / $this->hotel->calculateNights()) * $night;
      }
      if (strval($valueAmount['BasisType']) == 'Percent') {
        $night = strval($valueAmount['NmbrOfNights']);
      }
      if (strval($valueAmount['BasisType']) == 'Amount') {
        $night = strval($valueAmount['NmbrOfNights']);
      }
      if (strval($valueAmount['BasisType']) == 'FullStay') {//en FullStay viene 2 typo por porcentaje y monto
        if (isset($valueAmount['Percent'])) {
          $porcentaje = floatval($valueAmount['Percent']);
          $amount = ($totalAmount / 100) * $porcentaje;
        }
        else if (isset($valueAmount['Amount'])) {
          $amount = floatval($valueAmount['Amount']);
        }
      }
      $cancellation = new \BookingCancellationPolicy();
      $cancellation->build($amount, $dateFrom, '00:00', true, $this->factorFee);
      $policies[] = $cancellation;
    }

    return $policies;
  }

  public function getGuestList() {
    $customer_counter = 0;
    for ($i = 0; $i < $this->getAdultCount(); $i++) {
      $customer_counter++;
      $guests[] = new \BookingCustomer('AD', $customer_counter, '30', '', '');
    }
    $indexChild = $this->indexChild;
    $childIndex = 0;
    for ($i = 0; $i < $this->getChildCount(); $i++) {
      $customer_counter++;
      $guests[] = new \BookingCustomer('CH', $customer_counter, $this->data->childAges[$childIndex], '', '');
      $indexChild++;
      $childIndex++;
    }

    return $guests;
  }

  public function buildObjecArray() {
    return array();
  }
}

?>
