<?php
namespace Tourico;

class PurchasedRoomRS extends \RoomAvail {

  private $elements;
  private $guests;

  public function __construct($elements, $hotel) {
    $this->elements = $elements;
    // __logarr($elements);
    // $this->board = $board;
    $this->adultCount = 0;
    $this->childCount = 0;
    $this->guests = array();
    $room = $this->elements->ProductInfo->RoomExtraInfo->RoomInfo;
    $i = 0;
    $pax = $this->elements->ProductInfo->Passenger->attributes();
    $i++;
    $this->guests[] = new \BookingCustomer('AD', $i, 30, strval($pax['firstName']), strval($pax['lastName']));
    foreach ($room->ChildAges->ChildAge as $key => $value) {
      $age = $value->attributes();
      $i++;
      $type = 'CH';
      $this->guests[] = new \BookingCustomer($type, $i, strval($age['age']), '', '');
    }
    // foreach ($this->elements->RoomExtraInfo->RoomInfo->Guest as $guest) {
    //   $i++;
    //   $attrs = $guest->attributes();
    //   $type = $attrs['Age'] > 18 ? 'AD' : 'CH';
    //   $this->guests[] = new \BookingCustomer($type, $i, $attrs['Age'], $attrs['Name'], $attrs['Surname']);
    //   if ($type == 'AD') {
    //     $this->adultCount++;
    //   }
    //   else if ($type == 'CH') {
    //     $this->childCount++;
    //   }
    // }
  }

  public function getProvider() {
    return \BookingProvider::TOURICO;
  }

  public function getRoomCount() {
    return 1;
  }

  public function getAdultCount() {
    return $this->elements->ProductInfo->RoomExtraInfo->RoomInfo->AdultNum;
  }

  public function getChildCount() {
    return $this->elements->ProductInfo->RoomExtraInfo->RoomInfo->ChildNum;
  }

  public function getPrice() {
    // TODO: return price
    return 0;
  }

  public function getSHRUI() {
    return '';
  }

  public function getOnRequest() {
    return '';
  }

  public function getBoard() {
    $board      = $this->elements->ProductInfo->RoomExtraInfo;
    $roomBoard  = isset($board->BoardBase) ? $board->BoardBase->attributes() : '';
    $attr       = $this->elements->ProductInfo->attributes();
    $roomType   = strval($attr['name']);
    if (strval($roomBoard['bbName']) == 'All Inclusive') {
      return 'Todo Incluido';
    }
    return strval(($roomBoard != '') ? $roomBoard['bbName'] : $roomType);
  }

  public function getBoardCode() {
    return '';
  }

  public function getBoardType() {
    return '';
  }

  public function getRoomType() {
    $attr = $this->elements->ProductInfo->RoomExtraInfo->attributes();
    return strval($attr['name']);
  }

  public function getRoomTypeType() {
    return '';
  }

  public function getRoomTypeCode() {
    return '';
  }

  public function getRoomTypeCharacteristic() {
    return '';
  }

  public function getCancellationPolicies() {
    return array();
  }

  public function getGuestList() {
    return $this->guests;
  }
}

?>
