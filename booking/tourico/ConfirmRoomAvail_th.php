<?php
namespace Tourico;

class ConfirmRoomAvail extends \RoomAvail {

  public $data;
  public $hotel;
  public $date_in;
  public $date_out;
  public $childAges;
  public $rules;
  public $totalAmountService;
  private $factorFee;
  public function __construct($data, $hotel, $date_in, $date_out, $childAges, $rules, $paxes, $totalAmount) {
    // __logarr($data->Reservation);
    $this->data = $data;
    $this->hotel = $hotel;
    $checkin = explode('-', $date_in);
    $checkin = $checkin[0] . $checkin[1] . $checkin[2];
    $this->date_in = $checkin;
    $checkout = explode('-', $date_out);
    $checkout = $checkout[0] . $checkout[1] . $checkout[2];
    $this->date_out = $checkout;
    $this->childAges = $childAges;
    $this->rules = $rules;
    $this->totalAmountService = $totalAmount;
  }

  public function getProvider() {
    return \BookingProvider::TOURICO;
  }

  public function getHotel() {
    return $this->hotel;
  }

  public function getRoomCount() {
    return 1;
  }

  public function getAdultCount() {
    // __logarr($this->data->ProductInfo->RoomExtraInfo->RoomInfo);
    return $this->data->ProductInfo->RoomExtraInfo->RoomInfo->AdultNum;
  }

  public function getChildCount() {
    return $this->data->ProductInfo->RoomExtraInfo->RoomInfo->ChildNum;
  }

  public function getPrice() {
    $attr = $this->data->attributes();
    return floatval($attr['price']);
  }

  public function getSHRUI() {
    return '';
  }

  public function getOnRequest() {
    return '';
  }

  public function getBoard() {
    $board    = $this->data->ProductInfo->RoomExtraInfo;
    $roomBoard = isset($board->BoardBase) ? $board->BoardBase->attributes() : '';
    $attr     = $this->data->ProductInfo->RoomExtraInfo->attributes();
    $roomType = strval($attr['name']);
    if (strval($roomBoard['bbName']) == 'All Inclusive') {
      return 'Todo Incluido';
    }
    return strval(($roomBoard != '') ? $roomBoard['bbName'] : $roomType);
  }

  public function getBoardCode() {
    return '';
  }

  public function getBoardType() {
    return '';
  }

  public function getRoomType() {
    $attr = $this->data->ProductInfo->RoomExtraInfo->attributes();
    return strval($attr['name']);
  }

  public function getRoomTypeType() {
    return '';
  }

  public function getRoomTypeCode() {
    return '';
  }

  public function getRoomTypeCharacteristic() {
    return '';
  }

  public function getCancellationPolicies() {
    $policies = array();
    $totalAmount = $this->totalAmountService;
    $rules = $this->rules->Body->GetCancellationPoliciesResponse->HotelPolicy->RoomTypePolicy->CancelPolicy;
    foreach ($rules->CancelPenalty as $value) {
      // __logarr($value);
      $valueAmount  = $value->AmountPercent->attributes();
      $attrDate     = $value->Deadline->attributes();
      if (strval($attrDate['OffsetTimeUnit']) == 'Hour' && strval($attrDate['OffsetDropTime']) == 'BeforeArrival') {
        $days = intval($attrDate['OffsetUnitMultiplier']) / 24;
        if($days <= 0) {
          $dateFrom = new \DateTime($this->date_in);
        }
        else {
          $days = intval($attrDate['OffsetUnitMultiplier']) / 24;
          $date = new \DateTime($this->date_in);
          $dateFrom = date_sub($date, new \DateInterval('P' . $days .'D'));
        }
      }
      if (strval($attrDate['OffsetTimeUnit']) == 'Hour' && strval($attrDate['OffsetDropTime']) == 'AfterBooking') {
        $days = intval($attrDate['OffsetUnitMultiplier']) / 24;
        if($days <= 0) {
          $dateFrom = new \DateTime("now");
        }
        else {
          $days = intval($attrDate['OffsetUnitMultiplier']) / 24;
          $date = new \DateTime($this->date_in);
          $dateFrom = date_sub($date, new \DateInterval('P' . $days .'D'));
        }

      }
      if (strval($valueAmount['BasisType']) == 'Nights') {
        $night = intval($valueAmount['NmbrOfNights']);
        $amount = ($totalAmount / $this->hotel->calculateNights()) * $night;
      }
      if (strval($valueAmount['BasisType']) == 'Percent') {
        $night = strval($valueAmount['NmbrOfNights']);
      }
      if (strval($valueAmount['BasisType']) == 'Amount') {
        $night = strval($valueAmount['NmbrOfNights']);
      }
      if (strval($valueAmount['BasisType']) == 'FullStay') {//en FullStay viene 2 typo por porcentaje y monto
        if (isset($valueAmount['Percent'])) {
          $porcentaje = floatval($valueAmount['Percent']);
          $amount = ($totalAmount / 100) * $porcentaje;
        }
        else if (isset($valueAmount['Amount'])) {
          $amount = floatval($valueAmount['Amount']);
        }
      }
      $cancellation = new \BookingCancellationPolicy();
      $cancellation->build($amount, $dateFrom, '00:00', true, $this->factorFee);
      $policies[] = $cancellation;
    }

    return $policies;
  }

  public function getGuestList() {
    $guests = array();
    $customer_counter = 0;
    // for ($i = 0; $i < $this->getAdultCount(); $i++) {
    //   $customer_counter++;
    //   $guests[] = new \BookingCustomer('AD', $customer_counter, '30', '', '');
    // }

    for ($i = 0; $i < $this->getChildCount(); $i++) {
      $customer_counter++;
      $attr = $this->data->ProductInfo->RoomExtraInfo->RoomInfo->ChildAges->ChildAge[$i];
      $guests[] = new \BookingCustomer('CH', $customer_counter, strval($attr['age']), '', '');
    }
    return $guests;
  }
}

?>
