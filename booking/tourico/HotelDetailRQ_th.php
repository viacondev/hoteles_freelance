<?php
namespace Tourico;

class HotelDetailRQ {

  private $code;

  public function __construct($code) {
    $this->code = $code;
  }

  public function getType() {
    return 'hotelService';
  }

  public function getMethodName() {
    return 'GetHotelDetailsV3';
  }

  public function getParams() {
    $xml = '
    <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
      <SOAP-ENV:Header>
        __POS__
      </SOAP-ENV:Header>
      <SOAP-ENV:Body>
    		<m:GetHotelDetailsV3 xmlns:m="http://tourico.com/webservices/hotelv3">
    			<m:HotelIds>
    				<m:HotelID id="' . $this->code . '"/>
    			</m:HotelIds>
    		</m:GetHotelDetailsV3>
    	</SOAP-ENV:Body>
    </SOAP-ENV:Envelope>';

    return $xml;
  }
}
?>
