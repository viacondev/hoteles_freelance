<?php
namespace Tourico;

include_once('PurchasedRoomRS_th.php');

class PurchasedHotelRS extends \HotelAvail {

  private $element;
  private $rooms;
  private $hotelDetail;
  public function __construct($element) {
    $this->element = $element->ResGroup;
    $this->rooms = array();
    $room = $this->element->Reservations->Reservation->ProductInfo;
    // $this->rooms[] = new PurchasedRoomRS($room, $this);
    foreach ($element->ResGroup->Reservations->Reservation as $key => $room) {

      $this->rooms[] = new PurchasedRoomRS($room, $this);
    }
    $rs = $element->ResGroup->Reservations->Reservation->ProductInfo->attributes();
    $hotelDetailRQ = new HotelDetailRQ($rs['hotelId']);
    $rq = new TouricoRQ();
    $rqHotelDetail = $rq->execRequest($hotelDetailRQ);
    $rs = new TouricoRS($rqHotelDetail);
    $rsDetail = $rs->getResponse();
    $this->hotelDetail = $rsDetail->Body->GetHotelDetailsV3Response->GetHotelDetailsV3Result->TWS_HotelDetailsV3->Hotel;
    // foreach ($this->element->Reservations->Reservation->ProductInfo->RoomExtraInfo as $room) {
    //   $attrs = $this->element->RoomStays->RoomStay->RatePlans->RatePlan->attributes();
    //   $this->rooms[] = new PurchasedRoomRS($room, $this, $attrs['RatePlanName']);
    // }
  }

  public function getProvider() {
    return \BookingProvider::TOURICO;
  }

  public function getProviderName() {
    return 'TOURICO';
  }

  public function getAvailToken() {
    return '';
  }

  public function getCode() {
    $attrs = $this->element->Reservations->Reservation->ProductInfo->attributes();
    return strval($attrs['hotelId']);
  }

  public function getName() {
    $attrs = $this->element->Reservations->Reservation->ProductInfo->attributes();
    return strval($attrs['name']);
  }

  public function getDateFrom() {
    $attrs = $this->element->Reservations->Reservation->attributes();
    $from = explode('T', $attrs['fromDate']);
    return $from[0];
  }

  public function getDateTo() {
    $attrs = $this->element->Reservations->Reservation->attributes();
    $toDate = explode('T', $attrs['toDate']);
    return $toDate[0];
  }

  public function getDestinationCode() {
    // TODO
    return '';
  }

  public function getDestinationType() {
    return '';
  }

  public function getMainImage() {
    return '';
  }

  public function getDescription() {
    return '';
  }

  public function getLatitude() {
    return '';
  }

  public function getLongitude() {
    return '';
  }

  public function getZone() {
    return '';
  }

  public function getLocation() {
    return '';
  }

  public function getCategory() {
    return '';
  }

  public function getCategoryType() {
    return '';
  }

  public function getCategoryCode() {
    $attrs = $this->element->Reservations->Reservation->ProductInfo->attributes();
    return strval($attrs['numOfStars']).'EST';
  }

  public function getRoomsAvail() {
    return $this->rooms;
  }

  public function getGroupedRooms() {
    return $this->rooms;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getContractName() {
    return '';
  }

  public function getContractIncomingOfficeCode() {
    return '';
  }

  public function getContractComment() {
    return strval($this->hotelDetail->Descriptions->VoucherRemark);
  }

  public function getClientComment() {
    return '';
  }

  public function getSupplier() {
    return 'Tourico';
  }

  public function getSupplierVatNumber() {
    return '';
  }

  public function getDiscountList() {
    return array();
  }

  public function getAllPaxRequired() {
    return true;
  }

  public function getXtraDataRequired() {
    return true;
  }
}

?>
