<?php
namespace Tourico;

// ini_set('display_errors', 0);
class TouricoRS {

  private $response;

  public function __construct($xml, $solicitud = "", $type = "") {
    $xml = stripslashes($xml);
    $xml = str_replace(array('<soap:','<s:'), "<", $xml);
    $xml = str_replace(array('</soap:','</s:'), "</", $xml);
    $this->response = simplexml_load_string($xml);
    if($solicitud == 'XML_RS') {
      $descrip = $solicitud . $type;
      \Log::__log('INFO', $descrip, $xml);
    }
    //\Log::debug($xml, 'XML RS');
    // __logarr($this->response);
  }

  public function getResponse() {
    return $this->response;
  }


}

?>
