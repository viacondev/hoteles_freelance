<?php
namespace Tourico;

include_once('SCartRoomAvailRS_th.php');

class SCartHotelAvailRS extends \HotelAvail {

  private $data;
  private $rules;
  public  $hotelDetail;
  public  $hotelDiscount;
  public function __construct($data, $hotelDetail, $hotelDiscount, $totalAmount) {
    $this->data = $data;
    $this->hotelDetail    = $hotelDetail;

     // Build Booking Rules
    $from = $this->getDateFrom();
    $to   = $this->getDateTo();
    $from = substr($from, 0, 4) . '-' . substr($from, 4, 2) . '-' . substr($from, 6, 4);
    $to   = substr($to, 0, 4) . '-' . substr($to, 4, 2) . '-' . substr($to, 6, 4);
    $roomTypeId = $this->data->rooms[0]->SHRUI;
    $params = array(
      'hotelId'     => $this->getCode(),
      'hotelRoomId' => $roomTypeId,
      'dateFrom'    => $from,
      'dateTo'      => $to
      );

    $rules  = new HotelBookingRuleRQ($params);
    $rq     = new TouricoRQ();
    $xml    = $rq->execRequest($rules, 'XML_RQ', 'BB_TH_RQ_POLICY');
    \Log::info($xml, 'BB_TH_RS_POLICY');
    $this->rules = new TouricoRS($xml);
    $this->roomsAvail = array();
    $indexChild = 0;
    foreach ($this->data->rooms as $room) {
      $this->roomsAvail[] = new SCartRoomAvailRS($room, $this, $this->getDateFrom(), $this->getDateTo(), $this->data->childAges, $totalAmount, $this->rules, $indexChild, $this->data->factorFee);
      $indexChild += $room->childCount;
    }

    $roomType = '';
    $roomId = $this->data->rooms[0]->SHRUI;
    foreach ($hotelDiscount->RoomTypes->RoomType as $key => $value) {
      $attrRoom = $value->attributes();
      if (strval($attrRoom['hotelRoomTypeId']) == $roomId) {
        $roomType = $value;
        break;
      }
    }
    $this->hotelDiscount  = $roomType;
  }

  public function getProvider() {
    return \BookingProvider::TOURICO;
  }

  public function getProviderName() {
    return 'TOURICO';
  }

  public function getAvailToken() {
    return $this->data->availToken;
  }

  public function getCode() {
    return $this->data->hotelCode;
  }

  public function getName() {
    return $this->data->hotelName;
  }

  public function getDateFrom() {
    return $this->data->dateFrom;
  }

  public function getDateTo() {
    return $this->data->dateTo;
  }

  public function getDestinationCode() {
    return $this->data->destCode;
  }

  public function getDestinationType() {
    return '';
  }

  public function getMainImage() {
    return '';
  }

  public function getDescription() {
    return '';
  }

  public function getLatitude() {
    return '';
  }

  public function getLongitude() {
    return '';
  }

  public function getZone() {
    return '';
  }

  public function getLocation() {
    return '';
  }

  public function getCategory() {
    return '';
  }

  public function getCategoryType() {
    return '';
  }

  public function getCategoryCode() {
    return strval($this->data->category);
  }

  public function getRoomsAvail() {
    return $this->roomsAvail;
  }

  public function getGroupedRooms() {
    return $this->roomsAvail;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getContractName() {
    return '';
  }

  public function getContractIncomingOfficeCode() {
    return '';
  }

  public function getContractComment() {
    if($this->hotelDetail->Descriptions->VoucherRemark != '') {
      return $this->hotelDetail->Descriptions->VoucherRemark;
    }
    return "";
  }

  public function getClientComment() {
    return '';
  }

  public function getSupplier() {
    return '';
  }

  public function getSupplierVatNumber() {
    return '';
  }

  public function getDiscountList() {
    $discout = array();
    // if (isset($this->hotelDiscount->Discount)) {
    //   $xsiType = strval($this->hotelDiscount->Discount->attributes('xsi', TRUE)->type);
    //   $attrsDiscount = $this->hotelDiscount->Discount->attributes();
    //
    //   if ($xsiType == 'ProgressivePromotion') {
    //     $res = array();
    //     $price = intval($attrsDiscount['value']);
    //     $discout[] =  strval($attrsDiscount['name']) . ' Descuento del ' . $price . ' %' . ',<strong>Incluido en el precio</strong>';
    //     // $discout[] = new \BookingPrice($price, strval($attrsDiscount['name']), $this->data->factorFee);
    //   }
    //   else if($xsiType == 'PayStayPromotion') {
    //     $pay   = strval($attrsDiscount['pay']);
    //     $stay  = strval($attrsDiscount['stay']);
    //     $discout[] = 'Paga ' . $pay . ' Noches y ocupa ' . $stay . ',<strong> Incluido en el precio</strong>';
    //     // $res[] = new \BookingPrice($price, strval($attrsDiscount['name']), $this->data->factorFee);
    //   }
    // }
    return $discout;
  }

  public function getAllPaxRequired() {
    return false;
  }

  public function getXtraDataRequired() {
    return true;
  }

  public function isDataRequired() {
   return false;
  }

  public function calculateNights() {
    $from = new \DateTime($this->getDateFrom());
    $to = new \DateTime($this->getDateTo());
    return $to->diff($from)->days;
  }
}

?>
