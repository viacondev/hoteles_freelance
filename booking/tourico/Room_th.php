<?php
namespace Tourico;

class Room
{
  private $adultCount;
  private $childCount;
  private $childAges;
  function __construct($adultCount, $childCount, $childAges) {
    $this->adultCount = $adultCount;
    $this->childCount = $childCount;
    $this->childAges = $childAges;
  }

  function getAdultCount() {
    return $this->adultCount;
  }

  function getChildCount() {
    return $this->childCount;
  }

  function getChilddAges() {
    return $this->childAges;
  }

  function getParams() {
    $xml = '
            <hot1:AdultNum>' . $this->adultCount . '</hot1:AdultNum>
            <hot1:ChildNum>' . $this->childCount . '</hot1:ChildNum>';
    $xml .= '<hot1:ChildAges>';
    foreach ($this->childAges as $value) {
      $xml .= '<hot1:ChildAge age="' . $value . '"/>';
    }
    $xml .= '</hot1:ChildAges>';
    return $xml;
  }

}

?>
