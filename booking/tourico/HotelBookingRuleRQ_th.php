<?php
namespace Tourico;

class HotelBookingRuleRQ {

  private $params;

  public function __construct($params = array()) {
    $this->params = $params;
  }

  public function getType() {
    return 'hotelService';
  }

  public function getMethodName() {
    return 'GetCancellationPolicies';
  }

  public function getParams() {
    $p = $this->params;

    $xml = '
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:aut="http://schemas.tourico.com/webservices/authentication" xmlns:hot="http://tourico.com/webservices/hotelv3" xmlns:hot1="http://schemas.tourico.com/webservices/hotelv3">
          <soapenv:Header>
            __POS__
          </soapenv:Header>
          <soapenv:Body>
            <hot:GetCancellationPolicies>
               <hot:hotelId>' . $p['hotelId'] . '</hot:hotelId>
               <hot:hotelRoomTypeId>' . $p['hotelRoomId'] . '</hot:hotelRoomTypeId>
               <hot:dtCheckIn> ' . $p['dateFrom'] .  '</hot:dtCheckIn>
               <hot:dtCheckOut>' . $p['dateTo'] . '</hot:dtCheckOut>
            </hot:GetCancellationPolicies>
          </soapenv:Body>
        </soapenv:Envelope>';

    return $xml;
  }
}
?>
