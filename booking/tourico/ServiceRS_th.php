<?php
namespace Tourico;

/**
 * Class used for confirmed Services
 */
class ServiceRS extends \BookingService {

  private $elements;
  private $item;
  private $errors;

  public function __construct($rs, $scItem, $errors = array()) {
    $this->elements = (count($rs) > 0) ? $rs->Body->BookHotelV3Response->BookHotelV3Result : array();
    $this->errors = $errors;
    $this->item = json_decode(json_encode($scItem), FALSE);
  }

  public function getLocalizer() {
    $localizer = $this->elements->ResGroup->attributes();
    $rgId = strval($localizer['rgId']); // es el localizer se necesita para obtener info de la reserva
    return $rgId;
  }

  public function getSPUI() {
    // $rsId = $this->elements->ResGroup->Reservations->Reservation->attributes();
    $rsId = '';
    foreach ($this->elements->ResGroup->Reservations->Reservation as $reservation) {
      $attr = $reservation->attributes();
      $rsId .= strval($attr['reservationId']) . '|';//reservationId se necesita para cancelar lareserva
    }
    $rsId = substr($rsId, 0, -1);
    // $rsId = strval($rsId[]);
    return $rsId;
  }

  public function getServiceType() {
    return 'ServiceHotel';
  }

  public function getServiceStatus() {
    return '';
  }

  public function getDirectPayment() {
    return '';
  }

  public function getDateFrom() {
    $res = $this->elements->ResGroup->Reservations->Reservation->attributes();
    $res = explode('T', $res['fromDate']);
    // $checkin = $res[0];
    $checkin = explode('-', $res[0]);
    $checkin = $checkin[0] . $checkin[1] . $checkin[2];
    return $checkin;
    // return ''
  }

  public function getDateTo() {
    $res = $this->elements->ResGroup->Reservations->Reservation->attributes();
    $res = explode('T', $res['toDate']);
    // $checkOut = $res;
    $checkOut = explode('-', $res[0]);
    $checkOut = $checkOut[0] . $checkOut[1] . $checkOut[2];
    return $checkOut;
  }

  public function getCurrency() {
    $currency = $this->elements->ResGroup->attributes();
    return strval($currency['currency']);
  }

  public function getTotalAmount() {
    $totalAmount = $this->elements->ResGroup->attributes();
    return floatval($totalAmount['totalPrice']);
  }

  public function getAdditionalCosts() {
    return array();
  }

  public function getServiceInfo() {
    $accommodations = $this->elements;
    $paxes          = $this->elements->paxes;
    $totalAmount    = $this->getTotalAmount();
    return new ConfirmHotelAvail($accommodations, $this->item, $paxes, $totalAmount);
  }

  public function getSupplementList() {
    return array();
  }

  public function getStatus() {
    $status = $this->elements->ResGroup->Reservations->Reservation->attributes();

    if (strval($status['status']) == 'Confirm') {
      return \ServiceStatus::CONFIRMED;
    }
    return \ServiceStatus::CANCELLED;
  }

  public function getErrors() {
    return $this->errors;
  }

}

?>
