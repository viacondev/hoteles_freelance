<?php
namespace Tourico;

class HotelDetailRS extends \BookingHotelDetail {

  public $elements;
  private $hotel;
  private $desc;
  private $images;
  private $contact;
  public $servicesFacilities;
  public $roomFacilities;
  public $cateringFacilities;

  public function __construct($rs) {
    $this->elements = $rs->Body->GetHotelDetailsV3Response->GetHotelDetailsV3Result->TWS_HotelDetailsV3->Hotel;
    // __logarr($this->elements);
    // $this->hotel = $this->elements->HotelDescriptiveContents->HotelDescriptiveContent->HotelInfo;
    // $this->contact = $this->elements->HotelDescriptiveContents->HotelDescriptiveContent->ContactInfos->ContactInfo;
    //
    $this->build();
  }

  private function build() {

    if (isset($this->elements->Media->Images)) {
      foreach ($this->elements->Media->Images->Image as $item) {
        $img = $item->attributes();
        $this->images[] = $img['path'];
      }
    }

    // Facilities
    $this->roomFacilities = array();
    $this->servicesFacilities = array();
    $this->cateringFacilities = array();

    if (isset($this->elements->Amenities)) {
      foreach ($this->elements->Amenities->Amenity as $value) {
        $val = $value->attributes();
        $this->roomFacilities[] = $val['name'];
      }
    }

    // foreach ($this->hotel->Services->Service as $service) {
    //   $attrs = $service->attributes();
    //   $txt = strval($service->DescriptiveText);
    //   if ($attrs['Sort'] == 'Hotel') {
    //     $this->servicesFacilities[] = $txt;
    //   }
    //   else if ($attrs['Sort'] == 'Room') {
    //     $this->roomFacilities[] = $txt;
    //   }
    //   else if ($attrs['Sort'] == 'Catering') {
    //     $this->cateringFacilities[] = $txt;
    //   }
    // }
  }

  public function getCode() {
    $attrs = $this->elements->attributes();
    return strval($attrs['hotelID']);
  }

  public function getName() {
    $attrs = $this->elements->attributes();
    return strval($attrs['name']);
  }

  public function getDescription() {
    return $this->elements->LongDescription->FreeTextLongDescription;
  }

  public function getImageList() {
    return $this->images;
  }

  public function getAddressStreetName() {
    $attr = $this->elements->Location->attributes();
    return strval($attr['address']);
  }
  public function getAddressNumber() {
    return '';
  }

  public function getAddressPostalCode() {
    return '';
  }

  public function getEmailList() {
    $mails = array();
    return $mails;
  }

  public function getPhoneList() {
    $phones = array();
    $attrs = $this->elements->attributes();
    $phones[] = strval($attrs['hotelPhone']);
    return $phones;
  }

  public function getFaxList() {
    $faxList = array();
    $attrs = $this->elements->attributes();
    $faxList[] = strval($attrs['hotelFax']);
    return $faxList;
  }

  public function getWebList() {
    return array();
  }

  public function getLocation() {
    return '';
  }

  public function getLatitude() {
    $attrs = $this->elements->Location->attributes();
    return strval($attrs['latitude']);
  }

  public function getLongitude() {
    $attrs = $this->elements->Location->attributes();
    return strval($attrs['longitude']);
  }

  public function getDestinationCode() {
    // TODO: return dest code
    return '';
  }

  public function getDestinationName() {
    // Commented because direction already inludes de city & country
    // return strval($this->contact->Addresses->Address->CityName);
    return '';
  }

  public function getDestinationZone() {
    return '';
  }

  public function getCategory() {
    $star = array('1.5' => 'H1_5',
                  '2.5' => 'H2_5',
                  '3.5' => 'H3_5',
                  '4.5' => 'H4_5',
                  '5.5' => 'H5_5');
    $category = $this->elements->attributes();
    $starval = strval($category['starLevel']);
    if (strpos($starval, '.')) {
      return $star[$starval];
    }
    return $star[$starval];
  }

  public function getCategoryCode() {
    $star = array('1.5' => 'H1_5',
                  '2.5' => 'H2_5',
                  '3.5' => 'H3_5',
                  '4.5' => 'H4_5',
                  '5.5' => 'H5_5',
                  '1'   =>  'HS',
                  '2'   =>  'HS2',
                  '3'   =>  'HS3',
                  '4'   =>  'HS4',
                  '5'   =>  'HS5');
    $category = $this->elements->attributes();
    $starval = strval($category['starLevel']);
    if (strpos($starval, '.')) {
      return $star[$starval];
    }
    return $star[$starval];
  }

  public function getBuildingFacilities() {
    // return $this->buildingFacilities;
    return array();
  }

  public function getHotelTypeFacilities() {
    // return $this->hotelTypeFacility;
    return array();
  }

  public function getCredCardsFacilities() {
    // return $this->credCardsFacilities;
    return array();
  }

  public function getRoomFacilities() {
    return $this->roomFacilities;
  }

  public function getServicesFacilities() {
    return $this->servicesFacilities;
  }

  public function getCateringFacilities() {
    return $this->cateringFacilities;
  }

  public function getBusinessFacilities() {
    // return $this->businessFacilities;
    return array();
  }

  public function getHealthBeautyFacilities() {
    // return $this->healthBeautyFacilities;
    return array();
  }

  public function getEntertainmentFacilities() {
    // return $this->entertainmentFacilities;
    return array();
  }

  public function getDistancesFacilities() {
    $arr = array();

    return $arr;
  }

  public function getHighLightFacilities() {
    // return $this->highLightFacilities;
    return array();
  }

}

?>
