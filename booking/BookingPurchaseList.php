<?php

class BookingPurchaseList extends BookingSerializable {

  public $currentPage;
  public $totalPages;
  public $totalItems;
  public $purchaseList;

  public function getCurrentPage() {
    return $this->currentPage;
  }

  public function getTotalPages() {
    return $this->totalPages;
  }

  public function getTotalItems() {
    return $this->totalItems;
  }

  public function getPurchaseList() {
    return $this->purchaseList;
  }

}

?>
