<?php
namespace Restel;

class HotelDetailRS extends \BookingHotelDetail {

  public $elements;
  private $hotel;
  private $desc;
  private $images;
  private $contact;
  public $servicesFacilities;
  public $roomFacilities;
  public $cateringFacilities;

  public function __construct($rs) {
    $this->elements = $rs->parametros->hotel;
    $this->build();
  }

  private function build() {

    if (isset($this->elements->fotos)) {
      foreach ($this->elements->fotos->foto as $item) {
        $img = $item;
        $this->images[] = $img;
      }
    }

    // Facilities
    $this->roomFacilities = array();
    $this->servicesFacilities = array();
    $this->cateringFacilities = array();

    if (isset($this->elements->Amenities)) {
      foreach ($this->elements->Amenities->Amenity as $value) {
        $val = $value->attributes();
        $this->roomFacilities[] = $val['name'];
      }
    }

  }

  public function getCode() {
    $attrs = $this->elements->codigo_hotel;
    return strval($attrs);
  }

  public function getName() {
    $attrs = $this->elements->nombre_h;
    return strval($attrs);
  }

  public function getDescription() {
    return $this->elements->desc_hotel;
  }

  public function getImageList() {
    return $this->images;
  }

  public function getAddressStreetName() {
    $dir = $this->elements->direccion;
    return strval($dir);
  }
  public function getAddressNumber() {
    return '';
  }

  public function getAddressPostalCode() {
    return '';
  }

  public function getEmailList() {
    $mails = array();
    $mails[] = $this->elements->mail;
    return $mails;
  }

  public function getPhoneList() {
    $phones = array();
    $phones[] = strval($this->elements->telefono);
    return $phones;
  }

  public function getFaxList() {
    $faxList = array();
    return $faxList;
  }

  public function getWebList() {
    return array();
  }

  public function getLocation() {
    return '';
  }

  public function getLatitude() {
    $attrs = $this->elements->latitud;
    return strval($attrs);
  }

  public function getLongitude() {
    $attrs = $this->elements->longitud;
    return strval($attrs);
  }

  public function getDestinationCode() {
    return '';
  }

  public function getDestinationName() {
    return '';
  }

  public function getDestinationZone() {
    return '';
  }

  public function getCategory() {
    $category = $this->elements->categoria;
    return $category . 'EST';
  }

  public function getCategoryCode() {
    $category = $this->elements->categoria;
    return $category . 'EST';
  }

  public function getBuildingFacilities() {
    // return $this->buildingFacilities;
    return array();
  }

  public function getHotelTypeFacilities() {
    // return $this->hotelTypeFacility;
    return array();
  }

  public function getCredCardsFacilities() {
    // return $this->credCardsFacilities;
    return array();
  }

  public function getRoomFacilities() {
    return $this->roomFacilities;
  }

  public function getServicesFacilities() {
    return $this->servicesFacilities;
  }

  public function getCateringFacilities() {
    return $this->cateringFacilities;
  }

  public function getBusinessFacilities() {
    // return $this->businessFacilities;
    return array();
  }

  public function getHealthBeautyFacilities() {
    // return $this->healthBeautyFacilities;
    return array();
  }

  public function getEntertainmentFacilities() {
    // return $this->entertainmentFacilities;
    return array();
  }

  public function getDistancesFacilities() {
    $arr = array();

    return $arr;
  }

  public function getHighLightFacilities() {
    // return $this->highLightFacilities;
    return array();
  }

}

?>
