<?php
namespace Restel;

class DestinationCodeMap {

  public static function getCode($c) {
    $map = array(
      'CUN' => array('provincia' => 'MECUN', 'pais' => 'ME'),
      'PUJ' => array('provincia' => 'RDPUJ', 'pais' => 'RD'),
      'BUE' => array('provincia' => 'AGBUE', 'pais' => 'AG'),
      'VRA' => array('provincia' => 'CUVRO', 'pais' => 'CU'),
      'HAV' => array('provincia' => 'CUHAV', 'pais' => 'CU'),
      'MCO' => array('provincia' => 'USMCO', 'pais' => 'US'),
      'MIA' => array('provincia' => 'USMIA', 'pais' => 'US'),
      'RIO' => array('provincia' => 'BRRIO', 'pais' => 'BR'),
      'SAO' => array('provincia' => 'BRSAO', 'pais' => 'BR'),
      'GYE' => array('provincia' => 'ECGUA', 'pais' => 'EC'),
      'UIO' => array('provincia' => 'ECQUI', 'pais' => 'EC'),
      'BOG' => array('provincia' => 'COBOG', 'pais' => 'CO'),
      'NYC' => array('provincia' => 'USNEW', 'pais' => 'US'),
      'LIM' => array('provincia' => 'PELIM', 'pais' => 'PE'),
      'PTY' => array('provincia' => 'PNPAN', 'pais' => 'PN'),
      'UIO' => array('provincia' => 'ECQUI', 'pais' => 'EC'),
      'IGU' => array('provincia' => 'AGIGR', 'pais' => 'AG'),
      'FDI' => array('provincia' => 'BRCUR', 'pais' => 'BR'),
      // 'SMA' => array('provincia' => 'CUJAR', 'pais' => 'CU'),//SE QUITO POR QUE APARECEN OTROS HOTELES DE OTROS DESTINOS
      'BLN' => array('provincia' => 'BRFLO', 'pais' => 'BR'),
      'PCM' => array('provincia' => 'MERIV', 'pais' => 'ME'),
      'MDF' => array('provincia' => 'MEMEX', 'pais' => 'ME'),
      'CAB' => array('provincia' => 'MEBCS', 'pais' => 'ME'),// LOS CABOS
      'CTG' => array('provincia' => 'COCTG', 'pais' => 'CO'),//CARTAGENA
      'PAR' => array('provincia' => 'FRPAR', 'pais' => 'FR'),
      'MVD' => array('provincia' => 'UYMVD', 'pais' => 'UY'),//MONTEVIDEO
      'MDE' => array('provincia' => 'COMED', 'pais' => 'CO'),//MEDELLIN
      'ADZ' => array('provincia' => 'COSAI', 'pais' => 'CO'),//SAN ANDRES
      'AUA' => array('provincia' => 'AWAUA', 'pais' => 'AW')
      );

    if (isset($map[$c])) {
      return $map[$c];
    }

    return NULL;
  }

}

?>
