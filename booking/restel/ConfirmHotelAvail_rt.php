<?php
namespace Restel;


class ConfirmHotelAvail extends \HotelAvail {

  public $data;
  public $rules;
  public $roomsAvail;
  public $item;
  public $paxs;

  public function __construct($item, $localizer, $paxs, $totalAmount) {
    $this->item = $item;

    $type = 'Rule_BookConfirm';
    $res  = new HotelBookingRuleRQ(0, array(), $localizer, $type);
    $rq   = new RestelRQ();
    $xml  = $rq->execRequest($res, 'XML_RQ', 'BR_RT_RQ_POLICY_2');

    $res  = new RestelRS($xml, 'XML_RS', 'BR_RT_RS_POLICY_2');
    $this->rules = $res->getResponse();
    $rooms    = $this->item->rooms;
    $checkin  = $this->item->dateFrom;
    $checkOut = $this->item->dateTo;
    foreach ($rooms as $room) {
       $this->roomsAvail[] = new ConfirmRoomAvail($room, $this, $checkin, $checkOut, $this->rules, $paxs, $totalAmount);
    }
  }

  public function getProvider() {
    return \BookingProvider::RESTEL;
  }

  public function getProviderName() {
    return \BookingProvider::getProviderName(\BookingProvider::RESTEL);
  }
  
  public function getAvailToken() {
    return '';
  }

  public function getCode() {
    $hotelCode = $this->item->hotelCode;
    return $hotelCode;
  }

  public function getName() {
    $hotelName = $this->item->hotelName;
    return $hotelName;
  }

  public function getDateFrom() {
    $checkin = $this->item->dateFrom;
    return $checkin;
  }

  public function getDateTo() {
    $checkOut = $this->item->dateTo;
    return $checkOut;
  }

  public function getDestinationCode() {
    return $this->item->destCode;
  }

  public function getDestinationType() {
    return '';
  }

  public function getMainImage() {
    return '';
  }

  public function getDescription() {
    return '';
  }

  public function getLatitude() {
    return '';
  }

  public function getLongitude() {
    return '';
  }

  public function getZone() {
    return '';
  }

  public function getLocation() {
    return '';
  }

  public function getCategory() {
    $category = $this->item->category;
    return strval($category);
  }

  public function getCategoryType() {
    return "";
  }

  public function getCategoryCode() {
    $category = $this->item->category;
    return strval($category);
  }

  public function getRoomsAvail() {
    return $this->roomsAvail;
  }

  public function getGroupedRooms() {
    return $this->roomsAvail;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getContractName() {
    return '';
  }

  public function getContractIncomingOfficeCode() {
    return '';
  }

  public function getContractComment() {
    // return $this->rules->getBookingRulesDesc();
    //revisar los comentario
    return '';
  }

  public function getClientComment() {
    return '';
  }

  public function getSupplier() {
    return '';
  }

  public function getSupplierVatNumber() {
    return '';
  }

  public function getDiscountList() {
    return array();
  }

  public function areAllPaxRequired() {
    return true;
  }

  public function isExtraDataRequired() {
    return true;
  }

  public function isDataRequired() {
    return false;
  }

  public function getIsWithPromotion() {
    return array();
  }
}

?>
