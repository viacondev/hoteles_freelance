<?php
namespace Restel;

class SCartRoomAvailRS extends \RoomAvail {

  private $data;
  public $hotel;
  private $date_in;
  private $date_out;
  public $childAges;
  private $rules;
  private $totalAmount;
  private $indexChild;

  public function __construct($data, $hotel, $date_in, $date_out, $childAges, $totalAmount, $rules, $indexChild, $factorFee = 0.79) {
    $this->data       = $data;
    $this->hotel      = $hotel;
    $this->date_in    = $date_in;
    $this->date_out   = $date_out;
    $this->childAges  = $childAges;
    $this->rules      = $rules->getResponse();
    $this->totalAmount = $totalAmount;
    $this->indexChild  = $indexChild;
    $this->factorFee  = $factorFee;
  }

  public function getProvider() {
    return \BookingProvider::RESTEL;
  }

  public function getHotel() {
    return $this->hotel;
  }

  public function getProviderName() {
    return 'RESTEL';
  }

  public function getHotelCode() {
    return '';
  }

  public function getRoomCount() {
    return $this->data->roomCount;
  }

  public function getAdultCount() {
    return $this->data->adultCount;
  }

  public function getChildCount() {
    return $this->data->childCount;
  }

  public function getPrice() {
    return $this->data->price;
  }

  public function getPriceWithFee() {
    return round($this->data->price / $this->factorFee, 2);
  }

  public function getSHRUI() {
    return $this->data->SHRUI;
  }

  public function getOnRequest() {
    return '';
  }

  public function getBoard() {
    return $this->data->board;
  }

  public function getBoardCode() {
    return $this->data->boardCode;
  }

  public function getBoardType() {
    return $this->data->boardType;
  }

  public function getRoomType() {
    return $this->data->roomType;
  }

  public function getRoomTypeType() {
    return $this->data->roomTypeType;
  }

  public function getRoomTypeCode() {
    return $this->data->roomTypeCode;
  }

  public function getRoomTypeCharacteristic() {
    return $this->data->roomTypeCharacteristic;
  }

  public function getCancellationPolicies() {
    $policies     = array();
    $totalAmount  = $this->totalAmount;
    $rules        = $this->rules->parametros->politicaCanc;
    $amount       = 0;
    $dateFrom     = new \DateTime("now");
    foreach ($rules as $value) {
      $estCom_gasto   = floatval($value->estCom_gasto);
      $nights         = floatval($value->noches_gasto);
      $diasAntelacion = $value->dias_antelacion;
      $attrsDate      = $value->attributes();
      $checkIn        = strval($attrsDate['fecha']);
      $checkIn        =  new \DateTime(dateFormatForDB($checkIn));
      if($nights != "") {
        $dateFrom = date_sub($checkIn, new \DateInterval('P' . $diasAntelacion .'D'));
        $amount  = ($totalAmount / $this->hotel->calculateNights()) * $nights;
      }
      else if ($estCom_gasto != 0 && $diasAntelacion != 999) {
        $dateFrom = date_sub($checkIn, new \DateInterval('P' . $diasAntelacion .'D'));
        $amount = ($totalAmount * $estCom_gasto) / 100;
      }
      else if ($diasAntelacion == 999) { 
        //si dias de antelacion es = a 999 significa que la reserva tiene gastos
        // y no podra ser cancelada sola 
        $amount = $totalAmount;
        $dateFrom = new \DateTime("now");
      }
      $cancellation = new \BookingCancellationPolicy();
      $cancellation->build($amount, $dateFrom, '00:00', true, $this->factorFee);
      $policies[] = $cancellation;
    }

    return $policies;
  }

  public function getGuestList() {
    $customer_counter = 0;
    for ($i = 0; $i < $this->getAdultCount(); $i++) {
      $customer_counter++;
      $guests[] = new \BookingCustomer('AD', $customer_counter, '30', '', '');
    }
    $indexChild = $this->indexChild;
    $childIndex = 0;
    for ($i = 0; $i < $this->getChildCount(); $i++) {
      $customer_counter++;
      $guests[] = new \BookingCustomer('CH', $customer_counter, $this->data->childAges[$childIndex], '', '');
      $indexChild++;
      $childIndex++;
    }

    return $guests;
  }

  public function buildObjecArray() {
    return array();
  }
}

?>
