<?php
namespace RESTEL;

class ReadServiceRQ {

  private $localizer;
  public function __construct($localizer) {
    $this->localizer = $localizer;
  }

  public function getType() {
    $type = 11;
    return $type;
  }

  function getMethodName() {
    return 'GetRGInfo';
  }

  function getParams() {
    $xml = '
          <parametros>
            <Localizador>' . $this->localizer . '</Localizador>
            <Afiliacion>HA</Afiliacion>
          </parametros>';

    return $xml;
  }

}
?>
