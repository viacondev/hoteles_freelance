<?php
namespace Restel;

include_once('SCartRoomAvailRS_rt.php');

class SCartHotelAvailRS extends \HotelAvail {

  private $data;
  private $rules;
  public  $hotelDetail;
  public  $hotelDiscount;
  public  $observation;
  public function __construct($data, $hotelDetail, $hotelDiscount, $totalAmount) {
    $this->data = $data;
    $hotelCode = $this->data->hotelCode;

    $rules  = new HotelBookingRuleRQ($hotelCode, $this->data->rooms);
    $rq     = new RestelRQ();
    $xml    = $rq->execRequest($rules, 'XML_RQ', 'BR_RT_RQ_POLICY');
    \Log::info($xml, 'BR_RT_RS_POLICY');
    $this->rules    = new RestelRS($xml);

    $hotelCode      = $this->data->hotelCode;
    $checkIn        = dateToDDMMYY($this->data->dateFrom);
    $checkOut       = dateToDDMMYY($this->data->dateTo);
    $observationRQ  =  new ObservationRQ($hotelCode, $checkIn, $checkOut);
    $resOb          = $rq->execRequest($observationRQ, 'XML_RQ', 'BR_RT_RQ_OBSERVATION');
    $observationRS  = new RestelRS($resOb, 'XML_RS', 'BR_RT_RS_OBSERVATION');
    $this->observation = $observationRS->getResponse();
    $this->roomsAvail = array();
    $indexChild = 0;
    foreach ($this->data->rooms as $room) {
      $this->roomsAvail[] = new SCartRoomAvailRS($room, $this, $this->getDateFrom(), $this->getDateTo(), $this->data->childAges, $totalAmount, $this->rules, $indexChild, $this->data->factorFee);
      $indexChild += $room->childCount;
    }

  }

  public function getProvider() {
    return \BookingProvider::RESTEL;
  }

  public function getProviderName() {
    return 'RESTEL';
  }

  public function getAvailToken() {
    return $this->data->availToken;
  }

  public function getCode() {
    return $this->data->hotelCode;
  }

  public function getName() {
    return $this->data->hotelName;
  }

  public function getDateFrom() {
    return $this->data->dateFrom;
  }

  public function getDateTo() {
    return $this->data->dateTo;
  }

  public function getDestinationCode() {
    return $this->data->destCode;
  }

  public function getDestinationType() {
    return '';
  }

  public function getMainImage() {
    return '';
  }

  public function getDescription() {
    return '';
  }

  public function getLatitude() {
    return '';
  }

  public function getLongitude() {
    return '';
  }

  public function getZone() {
    return '';
  }

  public function getLocation() {
    return '';
  }

  public function getCategory() {
    return '';
  }

  public function getCategoryType() {
    return '';
  }

  public function getCategoryCode() {
    return strval($this->data->category);
  }

  public function getRoomsAvail() {
    return $this->roomsAvail;
  }

  public function getGroupedRooms() {
    return $this->roomsAvail;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getContractName() {
    return '';
  }

  public function getContractIncomingOfficeCode() {
    return '';
  }

  public function getContractComment() {
    $observation = '';
    if (isset($this->observation->parametros->hotel->observaciones->observacion)) {
      $observation = $this->observation->parametros->hotel->observaciones->observacion->obs_texto;  
      return strval($observation);
    }
    return strval($observation);
  }

  public function getClientComment() {
    return '';
  }

  public function getSupplier() {
    return '';
  }

  public function getSupplierVatNumber() {
    return '';
  }

  public function getDiscountList() {
    $discout = array();
    
    return $discout;
  }

  public function areAllPaxRequired() {
    return false;
  }

  public function isExtraDataRequired() {
    return false;
  }

  public function isDataRequired() {
    return false;
  }

  public function getIsWithPromotion() {
    return array();
  }

  public function calculateNights() {
    $from = new \DateTime($this->getDateFrom());
    $to   = new \DateTime($this->getDateTo());
    return $to->diff($from)->days;
  }
}

?>
