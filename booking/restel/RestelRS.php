<?php
namespace Restel;

// ini_set('display_errors', 0);
class RestelRS {

  private $response;

  public function __construct($xml, $solicitud = "", $type = "") {
    $xml = stripslashes($xml);
    $xml = str_replace(array('<soap:','<s:'), "<", $xml);
    $xml = str_replace(array('</soap:','</s:'), "</", $xml);

    $this->response = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
    if($solicitud == 'XML_RS') {
      $descrip = $type;
      \Log::__log('INFO', $descrip, $xml);
    }
    //\Log::debug($xml, 'XML RS');
    // __logarr($this->response);
  }

  public function getResponse() {
    return $this->response;
  }


}

?>
