<?php
namespace Restel;

/**
 * Class used for confirmed Services
 */
class PurchasedServiceRS extends \BookingService {

  private $elements;

  public function __construct($rs) {
    $this->elements = $rs->RESERVA;
  }

  public function getLocalizer() {
    $localizer = $this->elements->Localizador;
    return strval($localizer);
  }

  public function getSPUI() {
    return '';
  }

  public function getServiceType() {
    return 'ServiceHotel';
  }

  public function getServiceStatus() {
    return '';
  }

  public function getDirectPayment() {
    return '';
  }

  public function getDateFrom() {
    $checkin = $this->elements->Fecha_Entrada;
    return $checkin;
  }

  public function getDateTo() {
    $checkOut = $this->elements->Fecha_Salida;
    return $checkOut;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getTotalAmount() {
    $totalAmount = $this->elements->PVA;
    return floatval($totalAmount);
  }

  public function getAdditionalCosts() {
    return array();
  }

  public function getServiceInfo() {
    $accommodations = $this->elements;
    return new PurchasedHotelRS($accommodations);
  }

  public function getSupplementList() {
    return array();
  }

  public function getStatus() {
    $status = $this->elements->Estado;
    if (strval($status) == 'C' || strval($status) == 'F') {
      return \ServiceStatus::CONFIRMED;
    }
  }

  public function getErrors() {
    return $this->errors;
  }

}

?>
