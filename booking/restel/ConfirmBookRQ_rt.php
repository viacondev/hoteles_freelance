<?php
namespace Restel;

class ConfirmBookRQ {

  private $localizer;
 
  public function __construct($localizer) {
    $this->localizer = $localizer;

  }

  public function getType() {
    $type = 3;
    return $type;
  }

  public function getMethodName() {
    return 'BookHotelV3';
  }

  public function getParams() {

    $xml = '<parametros>
              <localizador>' . $this->localizer . '</localizador>
              <accion>AE</accion>
            </parametros>';

    return $xml;
  }
}
?>
