<?php
namespace Restel;

class HotelDetailRQ {

  private $code;

  public function __construct($code) {
    $this->code = $code;
  }

  public function getType() {
    $type = 15;
    return $type;
  }

  public function getMethodName() {
    return 'GetHotelDetailsV3';
  }

  public function getParams() {
    $xml = '
      <parametros>
        <codigo>' . $this->code . '</codigo>
        <idioma>1</idioma>
      </parametros>';

    return $xml;
  }
}
?>
