<?php
namespace Restel;

include_once('RoomAvail_rt.php');

class HotelAvailRS extends \HotelAvail {

  private $element;
  private $destCode;
  private $rooms;
  public $dateFrom;
  public $dateTo;
  public $roomsAvail;
  public $roomsGroup;

  public function __construct($element, $destCode, $dateFrom, $dateTo, $roomsList, $regimen = "", $factorFee) {
    $this->element    = $element;
    $this->roomsAvail = array();
    $this->dateFrom   = $dateFrom;
    $this->dateTo     = $dateTo;
    $this->destCode   = $destCode;
    $roomComparation  = array();

    foreach ($roomsList as $value) {
      $cad = $value->getAdultCount() . '-' . $value->getChildCount();
      $roomComparation[] = $cad;
    }

    $rooms = $element->res->pax;
    $con   = 0;
    $indexRoom = 1;
    $arrRegimen = array('RO' => 'RO','SA' => 'RO','OB' => 'RO','FI' => 'RO',/* Solo Alojamiento */
                        'BB' => 'BB','AD' => 'BB',/* Alojamiento y Desayuno */
                        'HB' => 'HB','MP' => 'HB',/* Media Pension */
                        'FB' => 'FB','PC' => 'FB',/*Pension Completa*/
                        'AI' => 'AI','TI' => 'AI'/*Todo Incluido*/
                      );

    $acuRoomsGroup  = array();
    $roomsGroup     = array();
    // __logarr($roomComparation);
    foreach ($rooms as $roomPax) {
      $attrsRoomsPax = $roomPax->attributes();
      $ocupation     = explode('-', strval($attrsRoomsPax['cod']));
      foreach ($roomPax as $roomHab) { // <Hab>
        foreach ($roomHab as $roomReg) { // <Reg>
          $comparation = strval($attrsRoomsPax['cod']);
          if (in_array($comparation, $roomComparation)) {
            $this->roomsAvail[] = new RoomAvail($roomHab, $roomReg, $this, $ocupation, $factorFee);
          }
        }
      }
    }
    // __logarr($this->roomsAvail);
    // exit();
    $con        = count($this->roomsAvail);
    $roomIndex  = array();
    $groupsRoom = array();
    for ($i=0; $i < $con; $i++) {
      $rooms = $this->roomsAvail[$i];
      if (count($roomComparation) == 1) {
        if ($regimen != '') {
          if (in_array($rooms->getBoardCode(), $regimen)) {
            $groups       = array();
            $groups[]     = $rooms;
            $groupsRoom[] = $groups;
          }
        }
        else {
          $groups       = array();
          $groups[]     = $rooms;
          $groupsRoom[] = $groups;
        }
      }
      else {
        for ($d=0; $d < $con; $d++) {
          $groups     = array();
          $cadIndex   = $i . '_' . $d;
          $casReIndex = $d . '_' . $i;
          if ($d == $i || in_array($casReIndex, $roomIndex) || in_array($cadIndex, $roomIndex)){
            continue;
          }
          $roomNext   = $this->roomsAvail[$d];
          $boardRoom1 = $arrRegimen[$rooms->getBoardCode()];
          $boardRoom2 = $arrRegimen[$roomNext->getBoardCode()];
          $room1 = $rooms->getAdultCount() . '-' . $rooms->getChildCount();
          $room2 = $roomNext->getAdultCount() . '-' . $roomNext->getChildCount();
          $res =  $this->verificarRooms($room1, $room2, $roomComparation);
          // $res = true;
          if ($boardRoom1 == $boardRoom2 && $res == 'true') {
            if ($regimen != '') {
              if (in_array($rooms->getBoardCode(), $regimen)) {
                $roomIndex[]  = $cadIndex;
                $groups[]     = $rooms;
                $groups[]     = $roomNext;
                $groupsRoom[] = $groups;
              }
            }
            else {
              $roomIndex[]  = $cadIndex;
              $groups[]     = $rooms;
              $groups[]     = $roomNext;
              $groupsRoom[] = $groups;
            }
          }
        }
      }
      // if ($i == 3) {
      //   exit();  
      // }
    }
    // __logarr($groupsRoom);
    // exit();
    foreach ($groupsRoom as $key => $value) {
      $this->insertGroupedRoom($value);
      $this->roomsAvail[] = $value;
    }
    
    // $this->createGroupedRooms();
  }

  public function getProvider() {
    return \BookingProvider::RESTEL;
  }

  public function getProviderName() {
    return 'RESTEL';
  }

  public function getAvailToken() {
    return '';
  }

  public function getCode() {
    return strval(trim($this->element->cod));
  }

  public function getHotelZionCode() {
    return trim($this->element->cod);
  }

  public function getName() {
    return ucwords(strtolower(trim($this->element->nom)));
  }

  public function getDateFrom() {
    return $this->dateFrom;
  }

  public function getDateTo() {
    return $this->dateTo;
  }

  public function getDestinationCode() {
    return $this->destCode;
  }

  public function getDestinationType() {
    return '';
  }

  public function getMainImage() {
    $res = strval($this->element->thumbnail);
    if ($res != "") {
      return $res;
    }
    return 'assets/images/no-photo.jpg';
  }

  public function getDescription() {
    $res = $this->element->desc;
    if($res != "") {
      return strval($res);
    }
    return 'Sin Informacion';
  }

  public function getLatitude() {
    $res = $this->element->lat;
    return trim($res);
  }

  public function getLongitude() {
    $res = $this->element->lon;
    return trim($res);
  }

  public function getZone() {
    return strval("");
  }

  public function getLocation() {
    return "";
  }

  public function getCategory() {
    return strval($this->element->cat) . 'EST';
  }

  public function getCategoryType() {
    return '';
  }

  public function getCategoryCode() {
    return trim($this->element->cat) . 'EST';
  }

  public function getRoomsAvail() {
    return $this->roomsAvail;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getContractName() {
    return '';
  }

  public function getContractIncomingOfficeCode() {
    return '';
  }

  public function getContractComment() {
    return '';
  }

  public function getClientComment() {
    return '';
  }

  public function getSupplier() {
    return '';
  }

  public function getSupplierVatNumber() {
    return '';
  }

  public function getDiscountList() {
    return array();
  }

  public function getAllPaxRequired() {
    return false;
  }

  public function getXtraDataRequired() {
    return false;
  }

  public function isDataRequired() {
    return false;
  }

  public function getIsWithPromotion() {
    return "negative";
  }

  public function verificarRooms($room1, $room2, $rooms) {
    if ($room1 == $rooms[0] && $room2 == $rooms[1]) {
      return 'true';
    }
    if ($room2 == $rooms[0] && $room1 == $rooms[1]) {
      return 'true';
    }
    return 'false';
  }

}

?>
