<?php
namespace Restel;

class HotelAvailDestRQ {

  private $code;

  public function __construct($code) {
    $this->code = $code;
  }

  public function getType() {
    $type = 7;
    return $type;
  }

  public function getMethodName() {
    return 'Hotel Directory request';
  }

  public function getParams() {
    $xml = '
      <parametros>
        <pais>' . $this->code['provincia'] . '</pais>
        <provincia>' . $this->code['pais'] . '</provincia>
        <categoria>0</categoria>
        <afiliacion>RS</afiliacion>
        <idioma>1</idioma>
        <radio>9</radio>
      </parametros>';

    return $xml;
  }
}
?>
