<?php
namespace Restel;

class HotelReservationRQ {

  private $params;
  private $item;
  private $serviceIndex;

  public function __construct($params, $item, $serviceIndex) {
    $this->params = $params;
    $this->item = $item;
    $this->serviceIndex = $serviceIndex;
  }

  public function getType() {
    $type = 202;
    return $type;
  }

  public function getMethodName() {
    return 'BookHotelV3';
  }

  public function getParams() {
    $p    = $this->params;
    $i    = $this->serviceIndex;
    $item = $this->item;

    $price          = 0;
    $roomsXml       = '';
    $numberOfRooms  = count($item['rooms']);
    $currentPax     = 1;

    $hotelCode    = $item['hotelCode'];
    $cadLin       = '';
    foreach ($item['rooms'] as $key => $room) {
      $lin = json_decode($room->SHRUI);
      foreach ($lin as $key => $value) {
        $cadLin .= "<lin> " . $value. "</lin>";  
      }
      // $count = count($lin) - 1;
      // $cadLin .= "<lin>" . $lin[$count] . "</lin>"; 
    }
    $holderName = $p['holderName'] . ' ' . $p['holderLastName'];
    // FORMA DE PAGO 25 AL CREDITO
    // FORMA DE PAGO 44 PRE-PAGO
    $xml = '
            <parametros>
              <codigo_hotel>' . $hotelCode . '</codigo_hotel>
              <nombre_cliente>' . $holderName . '</nombre_cliente>
              <afiliacion>RS</afiliacion>
              <observaciones>Vistas al mar</observaciones>
              <num_mensaje />
              <num_expediente>AB198</num_expediente>
              <forma_pago>44</forma_pago>
              <res>
                ' . $cadLin . '
              </res>
            </parametros>';

    return $xml;
  }
}
?>
