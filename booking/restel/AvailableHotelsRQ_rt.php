<?php
namespace Restel;

class AvailableHotelsRQ {

  public $hotelCode;
  public $destCode;
  public $checkin;
  public $checkout;
  public $rooms;
  public $hotelId;
  public $pais;
  public $provincia;
  public function __construct($hotelCode, $destCode, $checkin, $checkout, $hotelId = array(), $rooms) {
    $this->hotelCode  = $hotelCode;
    $this->pais       = $destCode['pais'];
    $this->provincia  = $destCode['provincia'];
    $this->checkin    = $checkin;
    $this->checkout   = $checkout;
    $this->rooms      = $rooms;
    $this->hotelId    = $hotelId;
  }

  public function getType() {
    $type = 110;
    return $type;
  }

  public function getMethodName() {
    if (count($this->hotelId) > 0) {
      return 'SearchHotelsById';//si solo es busqueda de 1 solo hotel en especifico
    }
    return 'SearchHotels';
  }

  public function getParams() {
    $room_cad = '';
    $con = 1;
    $count = count($this->rooms);
    $hotelCode = $this->hotelCode;
    // $room_cad .= '<numhab1>' . $count . '</numhab1>';
    foreach ($this->rooms as $room) {
      $room_cad .= '<numhab' . $con .'>1';
      $room_cad .= '</numhab' . $con .'>';
      $room_cad .= '<paxes' . $con .'>';
      $room_cad .= $room->getAdultCount();
      $childCount = '-0';
      if ($room->getChildCount() != 0) {
        // $room_cad .= '-' . $room->getChildCount();
        $childCount = '-' . $room->getChildCount();
      }
      $room_cad .= $childCount;
      $room_cad .= '</paxes' . $con .'>';
      if ($room->getChildCount() != 0) {
        $childsAges = $room->getChilddAges();
        $room_cad .= '<edades' . $con .'>';
        $childAge = '';
        foreach ($childsAges as $key => $value) {
          $childAge .= $value . ',';
        }
        $childAge = substr($childAge, 0, -1);
        $room_cad .= $childAge;
        $room_cad .= '</edades' . $con .'>';
      }
      $con++;
    }
    $cadHotel = '';
    if (!empty($hotelCode)) {
      $cadHotel = '<hotel>' . $hotelCode . '#</hotel>';
    }
    $xml = '
            <parametros>
              ' . $cadHotel . '
              <pais>' . $this->pais . '</pais>
              <radio>9</radio>
              <provincia>' . $this->provincia . '</provincia>
              <categoria>0</categoria>
              <fechaentrada>' . $this->checkin . '</fechaentrada>
              <fechasalida>' . $this->checkout . '</fechasalida>
              <marca></marca>
              <afiliacion>RS</afiliacion>
              <usuario>D43744</usuario>
              ' . $room_cad . '
              <idioma>1</idioma>
              <duplicidad>1</duplicidad>
              <comprimido>2</comprimido>
              <informacion_hotel>1</informacion_hotel>
            </parametros>';

    return $xml;
  }
}

?>
