<?php
namespace Restel;

class PurchasedRoomRS extends \RoomAvail {

  private $elements;
  private $guests;

  public function __construct($elements, $hotel) {
    $this->elements = $elements;
    // $this->board = $board;
    $this->adultCount = 0;
    $this->childCount = 0;
    $this->guests = array();
  }

  public function getProvider() {
    return \BookingProvider::RESTEL;
  }

  public function getHotel() {
    return NULL;
  }

  public function getRoomCount() {
    return 1;
  }

  public function getAdultCount() {
    return 0;
  }

  public function getChildCount() {
    return 0;
  }

  public function getPrice() {
    // TODO: return price
    return 0;
  }

  public function getSHRUI() {
    return '';
  }

  public function getOnRequest() {
    return '';
  }

  public function getBoard() {
    return "";
  }

  public function getBoardCode() {
    return '';
  }

  public function getBoardType() {
    return '';
  }

  public function getRoomType() {
    return "";
  }

  public function getRoomTypeType() {
    return '';
  }

  public function getRoomTypeCode() {
    return '';
  }

  public function getRoomTypeCharacteristic() {
    return '';
  }

  public function getCancellationPolicies() {
    return array();
  }

  public function getGuestList() {
    return $this->guests;
  }
}

?>
