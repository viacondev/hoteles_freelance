<?php
namespace Restel;

include_once('HotelAvailRS_rt.php');

class HotelAvailListRS extends \HotelAvailList {

  private $elements;
  public $serviceHotels;

  public function __construct($rs, $destCode, $dateFrom, $dateTo, $roomsList, $hotelName, $regimen, $factorFee) {
    $this->serviceHotels = array();
    $hotelsList = $rs->param->hotls->hot;
    foreach ($hotelsList as $value) {
      $hotelAvail = new HotelAvailRS($value, $destCode, $dateFrom, $dateTo, $roomsList, $regimen, $factorFee);
      $count = count($hotelAvail->getGroupedRooms());
      if ($hotelName != '' && $count > 0) {
        $name = $hotelAvail->getName();
        if (preg_match("/$hotelName/i", $name)) {
          $this->serviceHotels[] = $hotelAvail;
        }
      }
      else {
        if ($count > 0) {
          $this->serviceHotels[] = $hotelAvail;
        }
      }
      
    }
    
  }

  public function getHotelsAvail() {
    return $this->serviceHotels;
  }

  public function getCurrentPage() {
    return 1;
  }

  public function getTotalPages() {
    return 1;
  }
}

?>
