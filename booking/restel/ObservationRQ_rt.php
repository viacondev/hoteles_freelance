<?php
namespace Restel;

class ObservationRQ {

  private $hotelCode;
  private $checkIn;
  private $checkOut;

  public function __construct($hotelCode, $checkIn, $checkOut) {
    $this->hotelCode  = $hotelCode;
    $this->checkIn    = $checkIn;
    $this->checkOut   = $checkOut;
  }

  public function getType() {
    $type = 24;
    return $type;
  }

  public function getMethodName() {
    return 'CancelReservation';
  }

  public function getParams() {
    $xml = '
        <parametros>
          <codigo>' . $this->hotelCode . '</codigo>
          <entrada>' . $this->checkIn . '</entrada>
          <salida>' . $this->checkOut . '</salida>
        </parametros>';

    return $xml;
  }
}
?>
