<?php
namespace Restel;

class RoomAvail extends \RoomAvail {

  private $element;
  private $roomReg;
  private $occupancy;
  public $hotel;
  public $adultCount;
  public $childCount;
  public $roomCount;
  public $price;
  public $childAges;
  public $board;
  public $boardCode;
  public $roomType;
  public $lin;
  public $hotelCode;
  public $priceWithFee;

  public function __construct($element, $roomReg, $hotel, $ocupation, $factorFee) {
    $this->element    = $element;
    $this->roomReg    = $roomReg;

    $arrLin = array();
    foreach ($roomReg->lin as $value) {
      $arrLin[] = strval($value);
    }
    $this->hotel      = $hotel;
    $this->lin        = json_encode($arrLin);
    $this->hotelCode  = $hotel->getCode();
    $this->hotel      = json_encode($this->buildObjecArray());
    $attrs            = $this->roomReg->attributes();
    $arrRegimen = array('RO' => 'Solo Alojamiento',       'OB' => 'Solo Alojamiento', 'SA' => 'Solo Alojamiento',
                        'BB' => 'Alojamiento y Desayuno', 'AD' => 'Alojamiento y Desayuno',
                        'HB' => 'Media Pension',          'MP' => 'Media Pension',
                        'FB' => 'Pension Completa',       'PC' => 'Pension Completa',
                        'AI' => 'Todo Incluido',          'TI' => 'Todo Incluido');
    $codRegimen = strval($attrs['cod']);
    $this->board      = isset($arrRegimen[$codRegimen]) ? $arrRegimen[$codRegimen] : $codRegimen;
    $this->boardCode  = strval($attrs['cod']);
    $attrPrice        = $roomReg->attributes();//Price Ocupancy
    $this->price      = floatval($attrPrice['prr']);
    $attrsElement     = $this->element->attributes();
    $this->priceWithFee = round($this->price / $factorFee, 2); 
    $this->roomType   = strval($attrsElement['desc']);
    $this->adultCount = $ocupation[0];
    $this->childCount = $ocupation[1];
    $this->ChildAges  = array();
    $this->element    = NULL;
    $this->roomReg    = NULL;
  }

  public function getProvider() {
    return \BookingProvider::RESTEL;
  }

  public function getHotel() {
    return $this->hotel;
  }

  public function getProviderName() {
    return 'RESTEL';
  }

  public function getHotelCode() {
    return strval(trim($this->hotelCode));
  }

  public function getRoomCount() {
    // return $this->roomCount;
    return 1;
  }

  public function getAdultCount() {
    return $this->adultCount;
  }

  public function getChildCount() {
    return $this->childCount;
  }

  public function getChildAges() {
    return $this->ChildAges;
  }

  public function getPrice() {
    return round($this->price, 2);
  }

  public function getPriceWithFee() {
    return $this->priceWithFee;
  }

  public function getSHRUI() {
    return $this->lin;
  }

  public function getOnRequest() {
    return '';
  }

  public function getBoard() {
    return $this->board;
  }

  public function getBoardCode() {
    $arrCode = array('PC' => 'FB',//MEDIA PENSION
                     'OB' => 'SA',
                     'PC' => 'FB');
    if (isset($arrCode[$this->boardCode])) {
      return $arrCode[$this->boardCode];
    }
    return $this->boardCode;
  }

  public function getBoardType() {
    return '';
  }

  public function getRoomType() {
    return $this->roomType;
  }

  public function getRoomTypeType() {
    
    return "";
  }

  public function getRoomTypeCode() {
    return "";
  }

  public function getRoomTypeCharacteristic() {
    return '';
  }

  public function getCancellationPolicies() {
    return array();
  }

  public function getGuestList() {
    return array();
  }

  public function buildObjecArray() {
    $con = 0;
    $hotel = $this->hotel;
    $methods = get_class_methods($hotel);
    $objArray = array();
    foreach ($methods as $method) {
      if (substr($method, 0, 2) == '__') continue;
      if (substr($method, 0, 3) === 'get' && $method != 'getGroupedRooms' && $method != 'getRoomsAvail') {
        $key = lcfirst(substr($method, 3));
        $objArray[$key] = $hotel->$method();
        $con++;
      }
    }
    return $objArray;
  }

}

?>
