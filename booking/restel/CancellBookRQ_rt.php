<?php
namespace Restel;

class CancellBookRQ {

  private $localizer;
  private $localizer_corto;

  public function __construct($localizer, $localizer_corto) {
    $this->localizer        = $localizer;
    $this->localizer_corto  = $localizer_corto;
  }

  public function getType() {
    $type = 401;
    return $type;
  }

  public function getMethodName() {
    return 'CancelReservation';
  }

  public function getParams() {
    $xml = '
        <parametros>
          <localizador_largo>' . $this->localizer . '</localizador_largo>
          <localizador_corto>' . $this->localizer_corto . '</localizador_corto>
        </parametros>';

    return $xml;
  }
}
?>
