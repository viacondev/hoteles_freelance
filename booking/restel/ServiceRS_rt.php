<?php
namespace Restel;

/**
 * Class used for confirmed Services
 */
class ServiceRS extends \BookingService {

  private $elements;
  private $item;
  public $errors;
  public $totalAmount;
  private $paxs;
  public function __construct($rs, $scItem, $paxs, $totalAmount, $errors = array()) {
    $this->elements     = $rs->parametros;
    $this->errors       = $errors;
    $this->item         = json_decode(json_encode($scItem), FALSE);
    $this->totalAmount  = $totalAmount;
    $this->paxs         = $paxs;
  }

  public function getLocalizer() {
    $localizer = $this->elements->localizador;
    return strval($localizer);
  }

  public function getSPUI() {
    $localizer_corto = $this->elements->localizador_corto;
    return strval($localizer_corto);
  }

  public function getServiceType() {
    return 'ServiceHotel';
  }

  public function getServiceStatus() {
    return '';
  }

  public function getDirectPayment() {
    return '';
  }

  public function getDateFrom() {
    $checkin = $this->item->dateFrom;
    return $checkin;
  }

  public function getDateTo() {
    $checkOut = $this->item->dateTo;
    return $checkOut;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getTotalAmount() {
    $totalAmount = $this->totalAmount;
    return $totalAmount;
  }

  public function getAdditionalCosts() {
    return array();
  }

  public function getServiceInfo() {
    $item         = $this->item;
    $localizer    = strval($this->elements->localizador);
    $totalAmount  = $this->totalAmount;
    $paxs         = $this->paxs;
    return new ConfirmHotelAvail($item, $localizer, $paxs, $totalAmount);
  }

  public function getSupplementList() {
    return array();
  }

  public function getStatus() {
    // $status = $this->elements->ResGroup->Reservations->Reservation->attributes();

    // if (strval($status['status']) == 'Confirm') {
      return \ServiceStatus::CONFIRMED; //Falta Verificar la parte de Confirmacion
    // }
    // return \ServiceStatus::CANCELLED;
  }

  public function getErrors() {
    return $this->errors;
  }

}

?>
