<?php
namespace Restel;

class ConfirmRoomAvail extends \RoomAvail {

  public $data;
  public $hotel;
  public $date_in;
  public $date_out;
  public $childAges;
  public $rules;
  private $paxs;
  private $totalAmount;

  public function __construct($data, $hotel, $date_in, $date_out, $rules, $paxs, $totalAmountService) {
    // __logarr($data->Reservation);
    $this->data     = $data;
    $this->hotel    = $hotel;
    $this->paxs     = $paxs;
    $this->date_in  = $date_in;
    $this->date_out = $date_out;
    $this->rules    = $rules;
    $this->totalAmount = $totalAmountService;
  }

  public function getProvider() {
    return \BookingProvider::RESTEL;
  }

  public function getHotel() {
    return $this->hotel;
  }

  public function getRoomCount() {
    return 1;
  }

  public function getAdultCount() {
    return $this->data->adultCount;
  }

  public function getChildCount() {
    return $this->data->childCount;
  }

  public function getPrice() {
    return $this->data->price;
  }

  public function getSHRUI() {
    return '';
  }

  public function getOnRequest() {
    return '';
  }

  public function getBoard() {
    return $this->data->board;
  }

  public function getBoardCode() {
    return $this->data->boardCode;
  }

  public function getBoardType() {
    return '';
  }

  public function getRoomType() {
    return $this->data->roomType;
  }

  public function getRoomTypeType() {
    return '';
  }

  public function getRoomTypeCode() {
    return '';
  }

  public function getRoomTypeCharacteristic() {
    return '';
  }

  public function getCancellationPolicies() {
    $policies     = array();
    $totalAmount  = $this->totalAmount;
    $rules        = $this->rules->parametros->politicaCanc;
    $amount       = 0;
    $dateFrom     = new \DateTime("now");
    foreach ($rules as $value) {
      $estCom_gasto   = floatval($value->estCom_gasto);
      //Porcentaje del cual se cargara los gastos de cancelacion
      $nights         = floatval($value->noches_gasto);
      $diasAntelacion = intval($value->dias_antelacion);
      $attrsDate      = $value->attributes();
      $checkIn        = dateFromYmdToDB($this->date_in);
      $checkIn        =  new \DateTime($checkIn);
      if($nights != "") {
        $dateFrom = date_sub($checkIn, new \DateInterval('P' . $diasAntelacion .'D'));
        $amount   = ($totalAmount / $this->hotel->calculateNights()) * $nights;
      }
      else if ($estCom_gasto != 0 && $diasAntelacion != 999) {
        $dateFrom = date_sub($checkIn, new \DateInterval('P' . $diasAntelacion .'D'));
        $amount   = ($totalAmount * $estCom_gasto) / 100;
      }
      else if ($diasAntelacion == 999) {
        $amount   = $totalAmount;
        $dateFrom = new \DateTime("now");
      }

      $cancellation = new \BookingCancellationPolicy();
      $cancellation->build($amount, $dateFrom, '00:00', true);
      $policies[] = $cancellation;
    }

    return $policies;
  }

  public function getGuestList() {
    $guests = array();
    $customer_counter = 0;
    for ($i = 0; $i < $this->getChildCount(); $i++) {
      $customer_counter++;
      $attr = $this->data->ProductInfo->RoomExtraInfo->RoomInfo->ChildAges->ChildAge[$i];
      $guests[] = new \BookingCustomer('CH', $customer_counter, strval($attr['age']), '', '');
    }
    return $guests;
  }
}

?>
