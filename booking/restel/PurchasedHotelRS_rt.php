<?php
namespace Restel;

include_once('PurchasedRoomRS_rt.php');

class PurchasedHotelRS extends \HotelAvail {

  private $element;
  private $rooms;

  public function __construct($element) {
    $this->element = $element;
    $this->rooms = array();

    foreach ($this->element->Linea as $room) {
      $this->rooms[] = new PurchasedRoomRS($room, $this);
    }
  }

  public function getProvider() {
    return \BookingProvider::RESTEL;
  }

  public function getProviderName() {
    return 'RESTEL';
  }

  public function getAvailToken() {
    return '';
  }

  public function getCode() {
    $attrs = strval($this->element->Linea->Hotel);
    return $attrs;
  }

  public function getName() {
    $attrs = strval($this->element->Linea->Nombre_Hotel);
    return $attrs;
  }

  public function getDateFrom() {
    $attrs = $this->element->Fecha_Salida;
    return $attrs;
  }

  public function getDateTo() {
    $attrs = $this->element->Fecha_Salida;
    return $attrs;
  }

  public function getDestinationCode() {
    // TODO
    return '';
  }

  public function getDestinationType() {
    return '';
  }

  public function getMainImage() {
    return '';
  }

  public function getDescription() {
    return '';
  }

  public function getLatitude() {
    return '';
  }

  public function getLongitude() {
    return '';
  }

  public function getZone() {
    return '';
  }

  public function getLocation() {
    return '';
  }

  public function getCategory() {
    return '';
  }

  public function getCategoryType() {
    return '';
  }

  public function getCategoryCode() {
    // TODO
    return '';
  }

  public function getRoomsAvail() {
    return $this->rooms;
  }

  public function getGroupedRooms() {
    return $this->rooms;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getContractName() {
    return '';
  }

  public function getContractIncomingOfficeCode() {
    return '';
  }

  public function getContractComment() {
    return strval("");
  }

  public function getClientComment() {
    return '';
  }

  public function getSupplier() {
    return 'Restel';
  }

  public function getSupplierVatNumber() {
    return '';
  }

  public function getDiscountList() {
    return array();
  }

  public function areAllPaxRequired() {
    return true;
  }

  public function isExtraDataRequired() {
    return true;
  }

  public function isDataRequired() {
    return false;
  }

  public function getIsWithPromotion() {
    return array();
  }
}

?>
