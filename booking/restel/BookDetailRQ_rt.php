<?php
namespace Restel;

class BookDetailRQ {

  private $dia;
  private $mes;
  private $anio;

  public function __construct($dia, $mes, $anio) {
    $this->dia = $dia;
    $this->mes = $mes;
    $this->anio = $anio;
  }

  function getType() {
    $type = 8;
    return $type;
  }

  function getMethodName() {
    return 'GetPreviousReservations';
  }

  public function getParams() {
    $xml = '<parametros>
              <selector>4</selector>
              <dia>' . $this->dia . '</dia>
              <mes>' . $this->mes . '</mes>
              <ano>' . $this->anio . '</ano>
              <usuario>D43744</usuario>
            </parametros>';

    return $xml;
  }
}
?>
