<?php
namespace Restel;

include_once('RestelRQ.php');
include_once('RestelRS.php');
include_once('HotelAvailListRS_rt.php');
include_once('Room_rt.php');
include_once('AvailableHotelsRQ_rt.php');
include_once('PurchaseRS_rt.php');
include_once('SCartServiceRS_rt.php');
include_once('HotelBookingRuleRQ_rt.php');
include_once('HotelReservationRQ_rt.php');
include_once('ConfirmBookRQ_rt.php');
include_once('ServiceRS_rt.php');
include_once('ConfirmHotelAvail_rt.php');
include_once('ConfirmRoomAvail_rt.php');
include_once('ReadServiceRQ_rt.php');
include_once('PurchasedServiceRS_rt.php');
include_once('PurchasedHotelRS_rt.php');
include_once('HotelDetailRQ_rt.php');
include_once('HotelDetailRS_rt.php');
include_once('CancellBookRQ_rt.php');
include_once('ObservationRQ_rt.php');
include_once('DestinationCodeMap_rt.php');
include_once('BookDetailRQ_rt.php');
include_once('HotelAvailDestRQ_rt.php');

class RestelBooking {

  private $request;

  public function __construct() {
    $this->request = new RestelRQ();
  }

  public function execRequest($params) {
    switch ($params['requestType']) {
      case \BookingRequestTypes::SearchHotelAvail:
        return $this->execSearchHotelAvailRQ($params);
        break;

      case \BookingRequestTypes::AddHotelService:
        return $this->execHotelServiceAddRQ($params);
        break;

      case \BookingRequestTypes::ConfirmPurchase:
        return $this->execPurchaseConfirmRQ($params);
        break;

      case \BookingRequestTypes::CancelPurchase:
        return $this->execPurchaseCancelRQ($params);
        break;

       case \BookingRequestTypes::HotelDetail:
        return $this->execHotelDetailRQ($params);
        break;

      case \BookingRequestTypes::PurchaseDetail:
        return $this->execPurchaseDetailRQ($params);
        break;

      case \BookingRequestTypes::SearchHotelsByDest:
        return $this->execSearchHotelsByDestRQ($params);
        break;

      case \BookingRequestTypes::PurchaseDetailList:
        return $this->execPurchaseDetailList($params);
        break;

      case \BookingRequestTypes::SearchZones:
        return $this->execSearchZonesRQ($params);
        break;

      default:
        return NULL;
        break;
    }
  }

  public function execSearchHotelAvailRQ($p) {
    $destCode = DestinationCodeMap::getCode($p['destCode']);
    if ($destCode == NULL || intval($p['roomCount']) > 2) return array();

    $roomCount      = intval($p['roomCount']);
    $rooms          = array();
    $rooms_numbers  = array();
    $childIndex     = 1;
    for ($i = 1;$i <= $roomCount; $i++) {
      $adultCount = intval($p["adultCount_$i"]);
      $childCount = intval($p["childCount_$i"]);
      $occupancy  = array();
      $childAges  = array();
      for  ($d = 1; $d <= intval($p["adultCount_$i"]); $d++) {
        $occupancy[] = 30;
      }

      for ($c = 1; $c <= intval($p["childCount_$i"]); $c++) {
        $occupancy[] = $p["childAge_$childIndex"];
        $childAges[] = $p["childAge_$childIndex"];
        $childIndex++;
      }
      $rooms_numbers[] = new Room($adultCount, $childCount, $childAges);
      $rooms[] = $occupancy;
    }

    $checkin    = substr($p['checkin'], 4, 2) . '/' . substr($p['checkin'], 6, 4) . '/' . substr($p['checkin'], 0, 4) ;
    $checkout   = substr($p['checkout'], 4, 2) . '/' . substr($p['checkout'], 6, 4) . '/' . substr($p['checkout'], 0, 4);
    
    $arr_hotels = array();
    $hotelCode = '';
    if (!empty($p['hotelCode'])) {
      $hotelCode = $p['hotelCode'];
    }
    $regimen = '';
    if(isset($p['regimen']) && $p['regimen'] != '') {
      $regimen = explode(',', implode(',', $p['regimen']));
    }
    $request    = new AvailableHotelsRQ($hotelCode, $destCode, $checkin, $checkout, $arr_hotels, $rooms_numbers);

    $xml = $this->request->execRequest($request);
    $rs  = new RestelRS($xml);
    $res = $rs->getResponse();
    
    $hotelList = new HotelAvailListRS($res , $p['destCode'], $p['checkin'], $p['checkout'], 
                                      $rooms_numbers, $p['hotelName'], $regimen, $p['factorFee']);
    
    return $hotelList;
  }

  public function prepararConsulta($p) {
    $destCode = DestinationCodeMap::getCode($p['destCode']);
    if ($destCode == NULL || intval($p['roomCount']) > 2 || !empty($p['zoneCode'])) return array();

    $roomCount      = intval($p['roomCount']);
    $rooms          = array();
    $rooms_numbers  = array();
    $childIndex     = 1;
    for ($i = 1;$i <= $roomCount; $i++) {
      $adultCount = intval($p["adultCount_$i"]);
      $childCount = intval($p["childCount_$i"]);
      $occupancy  = array();
      $childAges  = array();
      for  ($d = 1; $d <= intval($p["adultCount_$i"]); $d++) {
        $occupancy[] = 30;
      }

      for ($c = 1; $c <= intval($p["childCount_$i"]); $c++) {
        $occupancy[] = $p["childAge_$childIndex"];
        $childAges[] = $p["childAge_$childIndex"];
        $childIndex++;
      }
      $rooms_numbers[] = new Room($adultCount, $childCount, $childAges);
      $rooms[] = $occupancy;
    }

    $checkin    = substr($p['checkin'], 4, 2) . '/' . substr($p['checkin'], 6, 4) . '/' . substr($p['checkin'], 0, 4) ;
    $checkout   = substr($p['checkout'], 4, 2) . '/' . substr($p['checkout'], 6, 4) . '/' . substr($p['checkout'], 0, 4);
    
    $arr_hotels = array();
    $hotelCode = '';
    if (!empty($p['hotelCode'])) {
      $hotelCode = $p['hotelCode'];
    }
    $regimen = '';
    if(isset($p['regimen']) && $p['regimen'] != '') {
      $regimen = explode(',', implode(',', $p['regimen']));
    }
    $request    = new AvailableHotelsRQ($hotelCode, $destCode, $checkin, $checkout, $arr_hotels, $rooms_numbers);

    return $this->request->allOptions($request);
  }

  public function obtenerResultado($xml, $p)  {

    $rs  = new RestelRS($xml);
    $res = $rs->getResponse();
    if (isset($res->parametros->error)) {
      return array();
    }
    $roomCount      = intval($p['roomCount']);
    $rooms_numbers  = array();
    $childIndex     = 1;
    for ($i = 1;$i <= $roomCount; $i++) {
      $adultCount = intval($p["adultCount_$i"]);
      $childCount = intval($p["childCount_$i"]);
      $occupancy  = array();
      $childAges  = array();
      for  ($d = 1; $d <= intval($p["adultCount_$i"]); $d++) {
        $occupancy[] = 30;
      }

      for ($c = 1; $c <= intval($p["childCount_$i"]); $c++) {
        $occupancy[] = $p["childAge_$childIndex"];
        $childAges[] = $p["childAge_$childIndex"];
        $childIndex++;
      }
      $rooms_numbers[] = new Room($adultCount, $childCount, $childAges);
    }
    
    $regimen   = '';
    $hotelName = '';
    $hotelList = new HotelAvailListRS($res, $p['destCode'], $p['checkin'], $p['checkout'], 
                                      $rooms_numbers, $hotelName, $regimen, $p['factorFee']);
    
    return $hotelList;
  }

  public function execHotelServiceAddRQ($p) {
    $service = new SCartServiceRS($p);
    return new PurchaseRS(array($service));
  }

  public function execPurchaseConfirmRQ($p) {
    $services_count = intval($p['servicesCount']);
    $services       = array();
    $errors         = array();
    for ($i = 1; $i <= $services_count; $i++) {
      $book_params  = (array)json_decode($p['book_params']);
      $item         = $book_params;
      if ($item['provider'] != \BookingProvider::RESTEL) { continue; }
      $destCode  = $item["destCode"];
      $from      = dateFromYmdToDB($item["dateFrom"]);
      $to        = dateFromYmdToDB($item["dateTo"]);
      $hotelCode = $item["hotelCode"];
      $HotelReservationRQ = new HotelReservationRQ($p, $item, $i);
      $xml  = $this->request->execRequest($HotelReservationRQ, 'XML_RQ', 'BR_RT_RQ_PRERESERVA');
      $rs   =  new RestelRS($xml, 'XML_RS', 'BR_RT_RS_PRERESERVA');
      $rs   = $rs->getResponse();
      
      $localizer     = strval($rs->parametros->n_localizador);
      $confirmBookRQ = new ConfirmBookRQ($localizer);
      $resXml        = $this->request->execRequest($confirmBookRQ, 'XML_RQ', 'BR_RT_RQ_PURCHASE');

      $result        =  new RestelRS($resXml, 'XML_RS', 'BR_RT_RS_PURCHASE');
      $result        = $result->getResponse();
      
      $totalAmount = floatval($rs->parametros->importe_total_reserva);
      $services[]  = new ServiceRS($result, $item, $p, $totalAmount, $errors);
    }
    return new PurchaseRS($services);
  }

  public function execPurchaseDetailRQ($p) {
    $localizer        = $p['localizer'];
    $ReadServiceRQ    = new ReadServiceRQ($localizer);

    $rs = $this->request->execRequest($ReadServiceRQ);
    // \Log::info(json_encode($rs), 'MB_RS_INFOBOOK');
    $serviceRS  = new RestelRS($rs);
    $service    = new PurchasedServiceRS($serviceRS->getResponse());
    return new PurchaseRS(array($service));
  }

  public function execHotelDetailRQ($p) {
    $request = new HotelDetailRQ($p['hotelCode']);
    $rs      = $this->request->execRequest($request);
    $rs      = new RestelRS($rs);

    return new HotelDetailRS($rs->getResponse());
  }

  public function execPurchaseCancelRQ($p) {
    $localizer        = $p['localizer'];
    $localizer_corto  = $p['rs_id'];
    $type             = "Rule_BookConfirm";
    $rule             = new HotelBookingRuleRQ(0, array(), $localizer, $type);
    $rs = $this->request->execRequest($rule, 'XML_RQ', 'BR_RT_RQ_CANCELRULES');
    $rulesRS          = new RestelRS($rule, 'XML_RS', 'BR_RT_RS_CANCELRULES');

    $CancellBookRQ    = new CancellBookRQ($localizer, $localizer_corto);
    $rs = $this->request->execRequest($CancellBookRQ, 'XML_RQ', 'BR_RT_RQ_CANCELBOOK');
    /*$rs = '<?xml version="1.0" encoding="ISO-8859-1"?>
            <!DOCTYPE respuesta SYSTEM "http://xml.hotelresb2b.com/xml/dtd/res_cancelacion.dtd">
            <respuesta>
              <tipo><![CDATA[401]]></tipo>
              <nombre><![CDATA[Disponibilidad Varias Habitaciones Regímenes]]></nombre>
              <agencia><![CDATA[BARRY BOLIVIA]]></agencia>
              <parametros>
                <estado>25</estado>
                <localizador>31422539</localizador>
                <localizador_baja>La reserva con los localizadores referenciados no ha podido ser anulada. # Gastos de Cancelaci?n</localizador_baja>
                <id>b747763b-70ff-4d9c-98e8-9a9313d1a973</id>
              </parametros>
            </respuesta>';*/
    // \Log::info($rs, 'BR_RT_RS_CANCELBOOK');
    $cancellBookRS  = new RestelRS($rs, 'XML_RS', 'BR_RT_RS_CANCELBOOK');
    $rs = $cancellBookRS->getResponse();

    if ($rs->parametros->estado != 00) {
      return NULL;
    }
    $totalCancellationFee = floatval(0);
    return new PurchaseRS(array(), $totalCancellationFee);
  }

  public function execPurchaseDetailList($p) {
    $dia  = substr($p['dateFrom'], 6, 4);
    $mes  = substr($p['dateFrom'], 4, 2);
    $anio = substr($p['dateFrom'], 0, 4);
    $bookRQ = new BookDetailRQ($dia, $mes, $anio);
    $xml     = $this->request->execRequest($bookRQ);
    $res        = new RestelRS($xml);
    $res        = $res->getResponse();
    $bookList   = $res->parametros->reservas->reserva;
    $list_books = array();
    $paxs_arr   = array();
    foreach ($bookList as $key => $book) {
      $paxs_arr   = array();
      $localizer  = strval($book->localizador);
      $status     =  \ServiceStatus::CONFIRMED;
      $dateCancel = strval($book->fecha_cancelacion);
      if (!empty($dateCancel)) {
        $status = \ServiceStatus::CANCELLED;
      }
      $holder     = strval($book->clienteres);
      $paxs_arr[] = $holder;
      $list_books[] = array('localizer' => $localizer, 'holder' => $paxs_arr, 'status_provider' => $status);
    }
    return $list_books;
  }

  public function execSearchZonesRQ($p) {
    $destCode = DestinationCodeMap::getCode($p['destCode']);
    if ($destCode == NULL) return array();
    $rq         = new HotelAvailDestRQ($destCode);
    $xml        = $this->request->execRequest($rq);
    $res        = new RestelRS($xml);
    $res        = $res->getResponse();
    $hoteles    = $res->parametros->hoteles->hotel;
    $hotelsList = array();
    foreach ($hoteles as $hotel) {
      $exist      = false;
      $r          = \HotelMappingProvider::getMappedCodeId(array('hotel_code' => strval($hotel->codigo_cobol), 'provider' => \BookingProvider::RESTEL));
      $idMapping  = 0;
      if(count($r)) {
        $exist      = true;
        $idMapping  = $r;
      }
      $hotelsList[] = array('destCode'  => strval($p['destCode']),                  'hotelCode'     => strval($hotel->codigo_cobol), 
                            'hotelName' => addslashes(strval($hotel->nombre_h)),  'longitude'       => strval($hotel->longitud), 
                            'latitude'  => strval($hotel->latitud),               'category_code'   => strval($hotel->categoria),
                            'dest_code' => $p['destCode'], 'provider' => 3,       'zoneName'        => '','exist' => $exist,
                            'idMapping' => $idMapping);
    }
    return $hotelsList;
  }

}

?>
