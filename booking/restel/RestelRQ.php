<?php
namespace Restel;

class RestelRQ {

  private $url;
  private $codigousu;
  private $secacc;
  private $clausu;
  private $afiliacio;

  public function __construct() {
    $this->url        = \RTConfig::$url;
    $this->codigousu  = \RTConfig::$codigousu;
    $this->secacc     = \RTConfig::$secacc;
    $this->clausu     = \RTConfig::$clausu;
    $this->afiliacio  = \RTConfig::$afiliacio;
  }

  public function getRequest($RQElement) {
    $xml = '<?xml version="1.0" encoding="ISO-8859-1" ?>
            <!DOCTYPE peticion SYSTEM "http://xml.hotelresb2b.com/xml/dtd/pet_disponibilidad_110.dtd">
            <peticion>
              <tipo>' . $RQElement->getType() . '</tipo>
              <agencia>Agencia de BarryBolivia</agencia>
              __POS__
            </peticion>';
    if ($RQElement->getType() == 7) {
      $xml = '<?xml version="1.0" encoding="ISO-8859-1" ?>
              <!DOCTYPE peticion SYSTEM "http://xml.hotelresb2b.com/xml/dtd/pet_directorio.dtd">
              <peticion>
                <tipo>' . $RQElement->getType() . '</tipo>
                <agencia>Agencia de BarryBolivia</agencia>
                __POS__
              </peticion>';
    }
    $res = $RQElement->getParams();
    $xml = str_replace('__POS__', $res, $xml);
    return $xml;
  }

  public function execRequest($operation, $solicitud = "", $type = "") {

    if($solicitud == 'XML_RQ') {
      $descrip = $type;
      \Log::info($this->getRequest($operation), $descrip);
    }

    $rq = $this->url;
    $rq .= '?codigousu=' . $this->codigousu;
    $rq .= '&secacc=' . $this->secacc;
    $rq .= '&clausu=' . $this->clausu;
    $rq .= '&afiliacio=' . $this->afiliacio;
    $rq .= '&xml=' . urlencode(utf8_encode($this->getRequest($operation)));

    //__logtxt($this->getRequest($operation));

    $context = stream_context_create(array('http' => array('header'=>'Content-type: application/x-www-form-urlencoded\r\n')));
    $xml     = file_get_contents($rq, false, $context);
    // __logtxt($xml);
    return $xml;
  }
  public function allOptions($operation) {
    $rq = $this->url;
    $rq .= '?codigousu=' . $this->codigousu;
    $rq .= '&secacc=' . $this->secacc;
    $rq .= '&clausu=' . $this->clausu;
    $rq .= '&afiliacio=' . $this->afiliacio;
    $rq .= '&xml=' . urlencode(utf8_encode($this->getRequest($operation)));

    $options  = array(CURLOPT_AUTOREFERER => true, 
                        CURLOPT_HEADER => 0, 
                        CURLOPT_RETURNTRANSFER => 1, 
                        CURLOPT_URL             => $rq);
    return $options;
  }
}


?>
