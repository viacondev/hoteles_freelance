<?php
namespace Restel;

class HotelBookingRuleRQ {

  private $hotelCode;
  private $rooms;
  private $type;
  private $localizador;

  public function __construct($hotelCode = 0, $rooms = array(), $localizador = "", $type = "") {
    $this->hotelCode = $hotelCode;
    $this->rooms     = $rooms;
    $this->type      = $type;
    $this->localizador = $localizador;
  }

  public function getType() {
    $type = ($this->type != "") ? 142 : 144;
    return $type;
  }

  public function getMethodName() {
    return 'GetCancellationPolicies';
  }

  public function getParams() {
    $cadLin = "";
    foreach ($this->rooms as $key => $value) {
      $arrLin = json_decode($value->SHRUI); 
      foreach ($arrLin as $key => $lin) {
        $cadLin .= "<lin> " . $lin. "</lin>";
      }
    }
    $xml = '
        <parametros>
          <datos_reserva>
            <hotel>' . $this->hotelCode . '</hotel>
            ' . $cadLin . '
          </datos_reserva>
        </parametros>';
    if ($this->type != "") {
      $xml = '
          <parametros>
            <usuario>CODUSU</usuario>
            <localizador>' . $this->localizador . '</localizador>
            <idioma>1</idioma>
          </parametros>';
    }

    return $xml;
  }
}
?>
