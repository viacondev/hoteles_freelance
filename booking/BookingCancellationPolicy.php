<?php

class BookingCancellationPolicy extends BookingSerializable {

  public $amount;
  public $amountWithFee;
  public $dateFrom;
  public $time;
  public $description;

  /**
   * $dateFrom is a DateTime element
   * $reduceDate, true to reduce 4 days in the cancellation date, false to keep the date
   */
  public function build($amount, $dateFrom, $time, $reduceDate = false, $desc = '', $factorFee = 0.79) {
    $this->amount = floatval($amount);
    $this->amountWithFee = round(floatval($amount) / $factorFee, 2);
    $this->dateFrom = $dateFrom;
    $this->time = $time;
    $this->description = $desc;

    if ($reduceDate) {
      if ($time == '23:59') {
        $this->dateFrom = date_add($this->dateFrom, new DateInterval('P1D'));
      }
      $this->dateFrom = date_sub($this->dateFrom, new DateInterval('P5D'));
      $this->time = '18:30';
      // Verifica si la fecha es mucho menor a la de ahora en caso
      // de que fuera asi solo le quita un dia a la fecah actual
      $diff = date_diff(new DateTime('now'), $this->dateFrom);
      if ($diff->format('%R%a') <= 2) {
        $this->dateFrom = date_sub(new DateTime('now'), new DateInterval('P3D'));
      }
    }
  }

  public function getAmount() {
    return strval($this->amount);
  }

  public function getAmountWithFee() {
    return strval($this->amountWithFee);
  }

  public function getDateFrom() {
    if (gettype($this->dateFrom) == "string") {
      return $this->dateFrom;
    }
    return $this->dateFrom->format('d/m/Y');

  }

  public function getTime() {
    return $this->time;
  }

  public function getRawDateTime() {
    return $this->dateFrom;
  }

  public function getDescription() {
    return $this->description;
  }

}

?>
