<?php

class BookingProvider {
    const ALL = 0;
    const HOTELBEDS   = 1;
    const DOMITUR     = 23;
    const METHABOOK   = 75;
    const METHABOOK2  = 150;
    const TOURICO     = 80;
    const DOMITUR2    = 159;
    const RESTEL      = 3;
    const TAILORBEDS  = 136;
    const NEMO        = 187;

    public static function getProviderName($id) {
    if ($id == 1)   return 'HOTELBEDS';
    if ($id == 23)  return 'DOMITUR';
    if ($id == 75)  return 'METHABOOK';
    if ($id == 150) return 'METHABOOK2';
    if ($id == 80)  return 'TOURICO';
    if ($id == 159) return 'DOMITUR2';
    if ($id == 3)   return 'RESTEL';
    if ($id == 136) return 'TAILORBEDS';
    if ($id == 187) return 'NEMO';
  }
}

?>
