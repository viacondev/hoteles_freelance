<?php

class BookingHotelDetail extends BookingSerializable {

  public $code;
  public $name;
  public $description;
  public $imageList;
  public $addressStreetName;
  public $addressNumber;
  public $addressPostalCode;
  public $emailList;
  public $phoneList;
  public $faxList;
  public $webList;
  public $location;
  public $latitude;
  public $longitude;
  public $destinationCode;
  public $destinationName;
  public $destinationZone;
  public $category;
  public $categoryCode;
  public $buildingFacilities;
  public $hotelTypeFacilities;
  public $credCardsFacilities;
  public $roomFacilities;
  public $servicesFacilities;
  public $cateringFacilities;
  public $businessFacilities;
  public $healthBeautyFacilities;
  public $entertainmentFacilities;
  public $distancesFacilities;
  public $highLightFacilities;

  public function getCode() {
    return $this->code;
  }

  public function getName() {
    return $this->name;
  }

  public function getDescription() {
    return $this->description;
  }

  public function getImageList() {
    return $this->imageList;
  }

  public function getAddressStreetName() {
    return $this->addressStreetName;
  }

  public function getAddressNumber() {
    return $this->addressNumber;
  }

  public function getAddressPostalCode() {
    return $this->addressPostalCode;
  }

  public function getEmailList() {
    return $this->emailList;
  }

  public function getPhoneList() {
    return $this->phoneList;
  }

  public function getFaxList() {
    return $this->faxList;
  }

  public function getWebList() {
    return $this->webList;
  }

  public function getLocation() {
    return $this->location;
  }

  public function getLatitude() {
    return $this->latitude;
  }

  public function getLongitude() {
    return $this->longitude;
  }

  public function getDestinationCode() {
    return $this->destinationCode;
  }

  public function getDestinationName() {
    return $this->destinationName;
  }

  public function getDestinationZone() {
    return $this->destinationZone;
  }

  public function getCategory() {
    return $this->category;
  }

  public function getCategoryCode() {
    return $this->categoryCode;
  }

  public function getBuildingFacilities() {
    return $this->buildingFacilities;
  }

  public function getHotelTypeFacilities() {
    return $this->hotelTypeFacilities;
  }

  public function getCredCardsFacilities() {
    return $this->credCardsFacilities;
  }

  public function getRoomFacilities() {
    return $this->roomFacilities;
  }

  public function getServicesFacilities() {
    return $this->servicesFacilities;
  }

  public function getCateringFacilities() {
    return $this->cateringFacilities;
  }

  public function getBusinessFacilities() {
    return $this->businessFacilities;
  }

  public function getHealthBeautyFacilities() {
    return $this->healthBeautyFacilities;
  }

  public function getEntertainmentFacilities() {
    return $this->entertainmentFacilities;
  }

  public function getDistancesFacilities() {
    return $this->distancesFacilities;
  }

  public function getHighLightFacilities() {
    return $this->highLightFacilities;
  }

}

?>
