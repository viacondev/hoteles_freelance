<?php

include_once('BookingAvailableModality.php');

class BookingTicketAvail extends BookingSerializable {

  public $provider;
  public $availToken;
  public $code;
  public $name;
  public $dateFrom;
  public $dateTo;
  public $description;
  public $mainImage;
  public $currency;
  public $companyCode;
  public $ticketClass;
  public $destinationCode;
  public $destinationType;
  public $classification;
  public $classificationCode;
  public $availableModalityList;
  public $adultCount;
  public $childCount;
  public $contractName;
  public $contractIncomingOfficeCode;
  public $contractComment;
  public $cancellationPolicies;
  public $guestList;
  public $serviceDetailList;
  public $supplier;
  public $supplierVatNumber;

  public function getProvider() {
    return $this->provider;
  }

  public function getAvailToken() {
    return $this->availToken;
  }

  public function getCode() {
    return $this->code;
  }

  public function getName() {
    return $this->name;
  }

  public function getDateFrom() {
    return $this->dateFrom;
  }

  public function getDateTo() {
    return $this->dateTo;
  }

  public function getDescription() {
    return $this->description;
  }

  public function getMainImage() {
    return $this->mainImage;
  }

  public function getCurrency() {
    return $this->currency;
  }

  public function getCompanyCode() {
    return $this->companyCode;
  }

  public function getTicketClass() {
    return $this->ticketClass;
  }

  public function getDestinationCode() {
    return $this->destinationCode;
  }

  public function getDestinationType() {
    return $this->destinationType;
  }

  public function getClassification() {
    return $this->classification;
  }

  public function getClassificationCode() {
    return $this->classificationCode;
  }

  public function getAvailableModalityList() {
    return $this->availableModalityList;
  }

  public function getAdultCount() {
    return $this->adultCount;
  }

  public function getChildCount() {
    return $this->childCount;
  }

  public function getContractName() {
    return $this->contractName;
  }

  public function getContractIncomingOfficeCode() {
    return $this->contractIncomingOfficeCode;
  }

  public function getContractComment() {
    return $this->contractComment;
  }

  public function getCancellationPolicies() {
    return $this->cancellationPolicies;
  }

  public function getGuestList() {
    return $this->guestList;
  }

  public function getServiceDetailList() {
    return $this->serviceDetailList;
  }

  public function getSupplier() {
    return $this->supplier;
  }

  public function getSupplierVatNumber() {
    return $this->supplierVatNumber;
  }

}

?>
