<?php

include_once('BookingService.php');

class BookingPurchase extends BookingSerializable {

  public $purchaseToken;
  public $status;
  public $holder;
  public $bookingServices;
  public $totalPrice;
  public $currency;
  public $creationDate;
  public $referenceFileNumber;
  public $referenceIncomingOffice;
  public $agencyReference;
  public $paymentDataDescription;

  public function getPurchaseToken() {
    return $this->purchaseToken;
  }

  public function getStatus() {
    return $this->status;
  }

  public function getHolder() {
    return $this->holder;
  }

  public function getBookingServices() {
    return $this->bookingServices;
  }

  public function getTotalPrice() {
    return $this->totalPrice;
  }

  public function getCurrency() {
    return $this->currency;
  }

  public function getCreationDate() {
    return $this->creationDate;
  }

  public function getReferenceFileNumber() {
    return $this->referenceFileNumber;
  }

  public function getReferenceIncomingOffice() {
    return $this->referenceIncomingOffice;
  }

  public function getAgencyReference() {
    return $this->agencyReference;
  }

  public function getPaymentDataDescription() {
    return $this->paymentDataDescription;
  }

}

?>
