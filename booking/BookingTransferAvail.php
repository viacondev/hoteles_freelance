<?php

class BookingTransferAvail extends BookingSerializable {

  public $provider;
  public $transferType;
  public $availToken;
  public $code;
  public $name;
  public $description;
  public $contractName;
  public $contractIncomingOffice;
  public $contractComment;
  public $dateFrom;
  public $timeFrom;
  public $currency;
  public $totalAmount;
  public $totalAmountWithFee;
  public $mainImage;
  public $transferInfoType;
  public $transferInfoTypeCode;
  public $transferInfoVehicleType;
  public $transferInfoVehicleTypeCode;
  public $adultCount;
  public $childCount;
  public $guestList;
  public $pickupLocationType;
  public $pickupLocationCode;
  public $pickupLocationName;
  public $pickupLocationTransferZoneCode;
  public $pickupLocationTransferZoneName;
  public $destLocationType;
  public $destinationCode;
  public $destLocationCode;
  public $destLocationName;
  public $destLocationTransferZoneCode;
  public $destLocationTransferZoneName;
  public $travelInfoDepartType;
  public $travelInfoDepartCode;
  public $travelInfoDepartDate;
  public $travelInfoDepartTime;
  public $travelInfoArrivalType;
  public $travelInfoArrivalCode;
  public $travelInfoArrivalDate;
  public $travelInfoArrivalTime;
  public $travelNumber;
  public $cancellationPolicies;
  public $supplier;
  public $supplierVatNumber;

  public function getProvider() {
    return $this->provider;
  }

  public function getTransferType() {
    return $this->transferType;
  }

  public function getAvailToken() {
    return $this->availToken;
  }

  public function getCode() {
    return $this->code;
  }

  public function getName() {
    return $this->name;
  }

  public function getDescription() {
    return $this->description;
  }

  public function getContractName() {
    return $this->contractName;
  }

  public function getContractIncomingOffice() {
    return $this->contractIncomingOffice;
  }

  public function getContractComment() {
    return $this->contractComment;
  }

  public function getDateFrom() {
    return $this->dateFrom;
  }

  public function getTimeFrom() {
    return $this->timeFrom;
  }

  public function getCurrency() {
    return $this->currency;
  }

  public function getTotalAmount() {
    return $this->totalAmount;
  }

  public function getTotalAmountWithFee() {
    return $this->totalAmountWithFee;
  }

  public function getMainImage() {
    return $this->mainImage;
  }

  public function getTransferInfoType() {
    return $this->transferInfoType;
  }

  public function getTransferInfoTypeCode() {
    return $this->transferInfoTypeCode;
  }

  public function getTransferInfoVehicleType() {
    return $this->transferInfoVehicleType;
  }

  public function getTransferInfoVehicleTypeCode() {
    return $this->transferInfoVehicleTypeCode;
  }

  public function getAdultCount() {
    return $this->adultCount;
  }

  public function getChildCount() {
    return $this->childCount;
  }

  public function getGuestList() {
    return $this->guestList;
  }

  public function getPickupLocationType() {
    return $this->pickupLocationType;
  }

  public function getPickupLocationCode() {
    return $this->pickupLocationCode;
  }

  public function getPickupLocationName() {
    return $this->pickupLocationName;
  }

  public function getPickupLocationTransferZoneCode() {
    return $this->pickupLocationTransferZoneCode;
  }

  public function getPickupLocationTransferZoneName() {
    return $this->pickupLocationTransferZoneName;
  }

  public function getDestLocationType() {
    return $this->destLocationType;
  }

  public function getDestinationCode() {
    return $this->destinationCode;
  }

  public function getDestLocationCode() {
    return $this->destLocationCode;
  }

  public function getDestLocationName() {
    return $this->destLocationName;
  }

  public function getDestLocationTransferZoneCode() {
    return $this->destLocationTransferZoneCode;
  }

  public function getDestLocationTransferZoneName() {
    return $this->destLocationTransferZoneName;
  }

  public function getTravelInfoDepartType() {
    return $this->travelInfoDepartType;
  }

  public function getTravelInfoDepartCode() {
    return $this->travelInfoDepartCode;
  }

  public function getTravelInfoDepartDate() {
    return $this->travelInfoDepartDate;
  }

  public function getTravelInfoDepartTime() {
    return $this->travelInfoDepartTime;
  }

  public function getTravelInfoArrivalType() {
    return $this->travelInfoArrivalType;
  }

  public function getTravelInfoArrivalCode() {
    return $this->travelInfoArrivalCode;
  }

  public function getTravelInfoArrivalDate() {
    return $this->travelInfoArrivalDate;
  }

  public function getTravelInfoArrivalTime() {
    return $this->travelInfoArrivalTime;
  }

  public function getTravelNumber() {
    return $this->travelNumber;
  }

  public function getCancellationPolicies() {
    return $this->cancellationPolicies;
  }

  public function getSupplier() {
    return $this->supplier;
  }

  public function getSupplierVatNumber() {
    return $this->supplierVatNumber;
  }

}
?>
