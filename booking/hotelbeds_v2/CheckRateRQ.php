<?php
namespace HotelBedsV2;

class CheckRateRQV2 {

  public $rooms;
  public function __construct($rooms) {
    $this->rooms  = $rooms;
  }

  public function getType() {
    return 'POST';
  }

  public function getRequest() {
    return 'hotel-api/1.0/checkrates';    
  }

  public function getParams() {
    $rates = [];
    foreach ($this->rooms as $key => $room) {
      $rates[] = ['rateKey' => $room->SHRUI];
    }
    $consulta = ['language'=>'ENG', 'upselling' => 'false', 'rooms' => $rates];
    return json_encode($consulta);
  }
}

?>
