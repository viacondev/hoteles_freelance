<?php
namespace HotelBedsV2;

class HotelDetailRSV2 extends \BookingHotelDetail {

  public $elements;
  private $hotel;
  private $desc;
  private $images;
  private $contact;
  public $servicesFacilities;
  public $roomFacilities;
  public $cateringFacilities;

  public function __construct($rs) {
    $this->elements = $rs->hotel;
    $this->build();
  }

  private function build() {

    if (isset($this->elements->images)) {
      foreach ($this->elements->images as $item) {
        $img = 'http://photos.hotelbeds.com/giata/bigger/' . $item->path;
        $this->images[] = $img;
      }
    }

    // Facilities
    $this->roomFacilities = array();
    $this->servicesFacilities = array();
    $this->cateringFacilities = array();
    
    $facilities = $this->elements->facilities;
    foreach ($facilities as $key => $fc) {
      $code   = $fc->facilityCode;
      $value  = $fc->description->content;
      if (isset($fc->number) && $fc->number != ""){
        $value .= ': ' . $fc->number;
      }
      if ($fc->facilityGroupCode == '10') { $this->buildingFacilities[$code] = $value; }
      if ($fc->facilityGroupCode == '20') { $this->hotelTypeFacility[$code] = $value; }
      if ($fc->facilityGroupCode == '30') { $this->credCardsFacilities[$code] = $value; }
      if ($fc->facilityGroupCode == '60') { $this->roomFacilities[$code] = $value; }
      if ($fc->facilityGroupCode == '70') { $this->servicesFacilities[$code] = $value; }
      if ($fc->facilityGroupCode == '71') { $this->cateringFacilities[$code] = $value; }
      if ($fc->facilityGroupCode == '72') { $this->businessFacilities[$code] = $value; }
      if ($fc->facilityGroupCode == '73') { $this->entertainmentFacilities[$code] = $value; }
      if ($fc->facilityGroupCode == '74') { $this->healthBeautyFacilities[$code] = $value; }
      if ($fc->facilityGroupCode == '40') {
        $description  = isset($fc->description)?$fc->description->content:'';
        $distance     = isset($fc->distance)?$fc->distance:'';
        $this->distancesFacilities[$description] = $distance . ' mts';
      }
  
      if ($fc->facilityGroupCode == '100') {
        $description  = isset($fc->description)?$fc->description:'';
        $distance     = isset($fc->distance)?$fc->distance:'';
        $this->highLightFacilities[$description] = $distance . ' mts';
      }
    }

    // if (isset($this->elements->Amenities)) {
    //   foreach ($this->elements->Amenities->Amenity as $value) {
    //     $val = $value->attributes();
    //     $this->roomFacilities[] = $val['name'];
    //   }
    // }

  }

  public function getCode() {
    $attrs = $this->elements->code;
    return strval($attrs);
  }

  public function getName() {
    $attrs = $this->elements->name->content;
    return strval($attrs);
  }

  public function getDescription() {
    return $this->elements->description->content;
  }

  public function getImageList() {
    return $this->images;
  }

  public function getAddressStreetName() {
    $dir = $this->elements->address->content;
    return strval($dir);
  }
  public function getAddressNumber() {
    return '';
  }

  public function getAddressPostalCode() {
    return $this->elements->postalCode;
  }

  public function getEmailList() {
    $mails = array();
    if (isset($this->elements->email)) {
      $mails[] = $this->elements->email;
    }
    return $mails;
  }

  public function getPhoneList() {
    $phones = array();
    foreach ($this->elements->phones as $phone) {
        $phones[] = strval($phone->phoneNumber);
    }
    return $phones;
  }

  public function getFaxList() {
    $faxList = array();
    return $faxList;
  }

  public function getWebList() {
    return isset($this->elements->web) ? array($this->elements->web) : [];
  }

  public function getLocation() {
    return '';
  }

  public function getLatitude() {
    $latitude = $this->elements->coordinates->latitude;
    return strval($latitude);
  }

  public function getLongitude() {
    $longitude = $this->elements->coordinates->longitude;
    return strval($longitude);
  }

  public function getDestinationCode() {
    return $this->elements->destination->code;
  }

  public function getDestinationName() {
    return $this->elements->destination->name->content;
  }

  public function getDestinationZone() {
    return '';
  }

  public function getCategory() {
    $category = $this->elements->category->code;
    return $category;
  }

  public function getCategoryCode() {
    $category = $this->elements->category->code;
    return $category;
  }

  public function getBuildingFacilities() {
    // return $this->buildingFacilities;
    return array();
  }

  public function getHotelTypeFacilities() {
    // return $this->hotelTypeFacility;
    return array();
  }

  public function getCredCardsFacilities() {
    // return $this->credCardsFacilities;
    return array();
  }

  public function getRoomFacilities() {
    return $this->roomFacilities;
  }

  public function getServicesFacilities() {
    return $this->servicesFacilities;
  }

  public function getCateringFacilities() {
    return $this->cateringFacilities;
  }

  public function getBusinessFacilities() {
    // return $this->businessFacilities;
    return array();
  }

  public function getHealthBeautyFacilities() {
    // return $this->healthBeautyFacilities;
    return array();
  }

  public function getEntertainmentFacilities() {
    // return $this->entertainmentFacilities;
    return array();
  }

  public function getDistancesFacilities() {
    $arr = array();

    return $arr;
  }

  public function getHighLightFacilities() {
    // return $this->highLightFacilities;
    return array();
  }

}

?>
