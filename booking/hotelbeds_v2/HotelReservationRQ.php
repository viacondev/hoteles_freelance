<?php

namespace HotelbedsV2;

class HotelReservationRQV2
{

  private $params;
  private $item;
  private $serviceIndex;

  public function __construct($params, $item, $serviceIndex)
  {
    $this->params   = $params;
    $this->item     = $item;
    $this->serviceIndex = $serviceIndex;
  }

  public function getType()
  {
    return 'POST';
  }

  public function getRequest()
  {
    return 'hotel-api/1.0/bookings';
  }

  public function getParams()
  {
    $params = $this->params;
    $request = [];
    $request['language']    = 'ENG';
    $request['holder']      = array('name' => $params['holderName'], 'surname' => $params['holderLastName']);
    $request['clientReference'] = $params['agencyReference'];
    $rooms = [];
    $indexPax = 1;
    foreach ($this->item['rooms'] as $key => $room) {
      $room_arr = [];
      $room_arr['rateKey'] = $room->SHRUI;
      $paxes = [];
      $roomId = 1;
      $adultCount = 0;
      for ($i = 1; $i <= $room->adultCount * $room->roomCount; $i++) {
        if ($adultCount == $room->adultCount) {
          $roomId++;
          $adultCount = 0;
        }
        $pax            = [];
        $pax['roomId']  = $roomId;
        $pax['type']    = 'AD';
        $pax['age']     = 30;
        $pax['name']    = $params['customerName_1_' . $indexPax];
        $pax['surname'] = $params['customerLastName_1_' . $indexPax];
        $paxes[] = $pax;
        $indexPax++;
        $adultCount++;
      }
      $indexChild = 0;
      for ($j = 1; $j <= $room->childCount * $room->roomCount; $j++) {
        $pax            = [];
        $pax['roomId']  = $roomId;
        $pax['type']    = 'CH';
        $pax['age']     = $room->childAges[$indexChild];
        $pax['name']    = $params['customerName_1_' . $indexPax];
        $pax['surname'] = $params['customerLastName_1_' . $indexPax];
        $paxes[] = $pax;
        $indexPax++;
        $indexChild++;
      }
      $room_arr['paxes'] = $paxes;
      $rooms[] = $room_arr;
    }
    $request['rooms'] = $rooms;
    return json_encode($request);
  }
}
