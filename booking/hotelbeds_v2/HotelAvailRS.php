<?php

namespace HotelBedsV2;

include_once('RoomAvail.php');

class HotelAvailRS extends \HotelAvail
{

  private $element;
  private $destCode;
  private $rooms;
  public $dateFrom;
  public $dateTo;
  public $roomsAvail;
  public $roomsGroup;
  public $roomCount;

  public function __construct($element, $dateFrom, $dateTo, $roomCount, $factorFee)
  {
    $this->element    = $element;
    $this->roomsAvail = array();
    $this->roomCount  = $roomCount;
    $this->dateFrom   = $dateFrom;
    $this->dateTo     = $dateTo;
    $currency         = $element->currency;
    // $this->destCode   = $destCode;
    // $roomComparation  = array();
    $hotelCode    = $element->code;
    $rooms        = $element->rooms;
    $groupedRooms = array();
    foreach ($rooms as $room) {
      foreach ($room->rates as $key => $rate) {
        $this->roomsAvail[] = new RoomAvail($room, $rate, $this, $currency, $factorFee);
      }
    }

    if (count($this->roomsAvail) > 0) {
      $this->createGroupedRooms();
    }
  }

  public function getProvider()
  {
    return \BookingProvider::HOTELBEDS;
  }

  public function getProviderName()
  {
    return 'HOTELBEDS';
  }

  public function getAvailToken()
  {
    return '';
  }

  public function getCode()
  {
    return strval(trim($this->element->code));
  }

  public function getHotelZionCode()
  {
    return trim($this->element->code);
  }

  public function getName()
  {
    return ucwords(strtolower(trim($this->element->name)));
  }

  public function getDateFrom()
  {
    return $this->dateFrom;
  }

  public function getDateTo()
  {
    return $this->dateTo;
  }

  public function getDestinationCode()
  {
    return $this->destCode;
  }

  public function getDestinationType()
  {
    return '';
  }

  public function getMainImage()
  {

    return 'assets/images/no-photo.jpg';
  }

  public function getDescription()
  {
    return 'Sin Informacion';
  }

  public function getLatitude()
  {
    $res = $this->element->latitude;
    return trim($res);
  }

  public function getLongitude()
  {
    $res = $this->element->longitude;
    return trim($res);
  }

  public function getZone()
  {
    return $this->element->zoneName;
  }

  public function getLocation()
  {
    return "";
  }

  public function getCategory()
  {
    return strval($this->element->categoryCode) . 'EST';
  }

  public function getCategoryType()
  {
    return '';
  }

  public function getCategoryCode()
  {
    return trim($this->element->categoryCode);
  }

  public function getRoomsAvail()
  {
    return $this->roomsAvail;
  }

  public function getCurrency()
  {
    return 'USD';
  }

  public function getContractName()
  {
    return '';
  }

  public function getContractIncomingOfficeCode()
  {
    return '';
  }

  public function getContractComment()
  {
    return '';
  }

  public function getClientComment()
  {
    return '';
  }

  public function getSupplier()
  {
    return '';
  }

  public function getSupplierVatNumber()
  {
    return '';
  }

  public function getDiscountList()
  {
    return array();
  }

  public function getAllPaxRequired()
  {
    return false;
  }

  public function getXtraDataRequired()
  {
    return false;
  }

  public function isDataRequired()
  {
    return false;
  }

  public function getIsWithPromotion()
  {
    return "negative";
  }

  private function createGroupedRooms()
  {
    $rooms = $this->getRoomsAvail();

    // Move the rooms to another array that contains the rooms according to the accommodation
    // i.e. $multiRooms['1_2_0'] contains all rooms with 1 Room, 2 adults and 0 childs.
    $multiRooms = array();
    $multiRoomsKey = array();
    foreach ($rooms as $room) {
      $key = $room->getRoomCount() . '_' . $room->getAdultCount() . '_' . $room->getChildCount();
      if (!isset($multiRooms[$key])) {
        $multiRooms[$key] = array();
        $multiRoomsKey[] = $key;
      }
      $multiRooms[$key][] = $room;
    }
    // Contain the room's groups that will be displayed to the user.
    // Groups are formed looking for the best matching across all rooms
    // If we only request for one room then all groups will have one item,
    // if we request for two rooms then all groups will have two items and so on.
    $this->groupedRooms = array();
    foreach ($multiRooms[$multiRoomsKey[0]] as $room) {
      $group = array();
      $group[] = $room;

      for ($i = 1; $i < count($multiRoomsKey); $i++) {
        // __logarr('INGRESO MULTI KEY');
        $bestMatch = NULL;
        $bestMatchPoints = -1;
        foreach ($multiRooms[$multiRoomsKey[$i]] as $nextRoom) {
          $points = 0;
          // __log($room->getBoard() . ' - ' . $nextRoom->getBoard());
          if ($room->getBoard() == $nextRoom->getBoard()) {
            $points++;
            // __log($room->getRoomTypeCode() . ' - ' . $nextRoom->getRoomTypeCode());
            if ($room->getRoomTypeCode() == $nextRoom->getRoomTypeCode()) {
              $points++;
              // if ($room->getRoomTypeCharacteristic() == $nextRoom->getRoomTypeCharacteristic()) {
              //   $points++;
              // }
            }
          }
          if ($points > $bestMatchPoints) {
            $bestMatch = $nextRoom;
            $bestMatchPoints = $points;
          }
        }
        $group[] = $bestMatch;
      }
      /* Comparamos la cantidad de habitaciones*/
      /* ya que en cada roomcount suele haber mas de 1 habitacion */
      $cantidad_hab = 0;
      foreach ($group as $gru) {
        $cantidad_hab += $gru->getRoomCount();
      }
      /* end */
      if ($this->roomCount != 0 && $this->roomCount == $cantidad_hab) {
        $this->insertGroupedRoom($group);
      }
    }
  }
}
