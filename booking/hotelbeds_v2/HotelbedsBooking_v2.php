<?php

namespace HotelBedsV2;

include_once('HotelBedsRQ.php');
include_once('HotelBedsRS.php');
include_once('CheckRateRQ.php');
include_once('AvailableHotelsRQ.php');
include_once('HotelAvailListRS.php');
include_once('SCartServiceRS.php');
include_once('PurchaseRS.php');
include_once('HotelReservationRQ.php');
include_once('ServiceRS.php');
include_once('ConfirmHotelAvail.php');
include_once('ConfirmRoomAvail.php');
include_once('ReadServiceRQ.php');
include_once('PurchasedServiceRS.php');
include_once('PurchasedHotelRS.php');
include_once('HotelDetailRQ.php');
include_once('HotelDetailRS.php');
include_once('CancelBookRQ.php');
include_once('ReadServiceTicketRQ.php');
include_once('PurchasedTicketRS.php');
include_once('PurchaseListRQ.php');
include_once('CancelTicketRQ.php');

class HotelBedsBookingV2
{

    private $request;

    public function __construct()
    {
        $this->request = new HotelBedsRQV2();
    }

    public function execRequest($params)
    {
        switch ($params['requestType']) {
            case \BookingRequestTypes::SearchHotelAvail:
                return $this->execHotelValuedAvailRQ($params);
                break;

            case \BookingRequestTypes::AddHotelService:
                return $this->execHotelServiceAddRQ($params);
                break;

            case \BookingRequestTypes::ConfirmPurchase:
                return $this->execPurchaseConfirmRQ($params);
                break;

            case \BookingRequestTypes::PurchaseDetail:
                return $this->execPurchaseDetailRQ($params);
                break;

            case \BookingRequestTypes::HotelDetail:
                return $this->execHotelDetailRQ($params);
                break;
            
            case \BookingRequestTypes::CancelPurchase:
                return $this->execPurchaseCancelRQ($params);
                break;

            default:
                return NULL;
                break;
        }
    }

    public function prepararConsulta($p)
    {

        $cant_search = array('HAV', 'VRA', 'SMA', 'CCO', 'LAR', 'CIE', 'STG');
        if (in_array($p['destCode'], $cant_search)) {
            return array();
        }

        $checkIn    = substr($p['checkin'], 0, 4) . '-' . substr($p['checkin'], 4, 2) . '-' . substr($p['checkin'], 6, 4);
        $checkOut   = substr($p['checkout'], 0, 4) . '-' . substr($p['checkout'], 4, 2) . '-' . substr($p['checkout'], 6, 4);

        $roomCount = intval($p['roomCount']);
        $occupancies = array();
        for ($i = 1; $i <= $roomCount; $i++) {
            $adultCount = $p['adultCount_' . $i];
            $childCount = $p['childCount_' . $i];
            // If there is a room with the same Occupancy then only increment the room count
            // instead of creating a new Occupancy
            $occupancyFound = false;
            foreach ($occupancies as $key => $occupancy) {
                if ($occupancy['adults'] == $adultCount && $occupancy['children'] == $childCount) {
                    $occupancies[$key]['rooms']++;
                    $occupancyFound = true;
                    break;
                }
            }
            if (!$occupancyFound) {
                $occupancies[] = array('rooms' => 1, 'adults' => $adultCount, 'children' => $childCount);
            }
        }
        // // Set child ages
        // $currentChild = 1;
        // foreach ($occupancies as $occupancy) {
        //     for ($i = 0; $i < $occupancy->Occupancy->AdultCount * $occupancy->RoomCount; $i++) {
        //         $occupancy->Occupancy->AddCustomer('AD', '', '', 30);
        //     }
        //     for ($i = 0; $i < $occupancy->Occupancy->ChildCount * $occupancy->RoomCount; $i++) {
        //         $occupancy->Occupancy->AddCustomer('CH', '', '', $p['childAge_' . $currentChild]);
        //         $currentChild++;
        //     }
        // }
        $hotels = new AvailableHotelsRQ($checkIn, $checkOut, $occupancies);
        return $this->request->allOptions($hotels);
    }

    public function obtenerResultado($response_xml, $p)
    {
        // __logwrite(__DIR__, $response_xml, 'AccomodationRS');
        $roomCount = intval($p['roomCount']);
        // include('dummy_xml/HotelValuedAvailRS1.php');
        $response_elements = new HotelBedsRS($response_xml);
        $rs = $response_elements->getResponse();
        $response = new HotelAvailListRS($rs, $p, $roomCount, $p['factorFee']);

        return $response;
    }

    public function execHotelServiceAddRQ($p)
    {

        $rooms      = $p['rooms'];
        $request    = new CheckRateRQV2($rooms);
        $xml        = $this->request->execRequest($request, 'XML_RQ', 'BB_HB_V2_RQ_CHECKRATE');
        // include('dummy_xml/CheckRateRS.php');

        $response_xml = new HotelBedsRSV2($xml, 'XML_RS', 'BB_HB_V2_RS_CHECKRATE');
        $rs         = $response_xml->getResponse();

        $service    = new SCartServiceRSV2($p, $rs);

        return new PurchaseRSV2(array($service));
    }

    public function execPurchaseConfirmRQ($p)
    {
        $services_count = intval($p['servicesCount']);
        $services       = array();
        $errors         = array();
        for ($i = 1; $i <= $services_count; $i++) {
            $book_params  = (array)json_decode($p['book_params']);
            $item         = $book_params;
            if ($item['provider'] != \BookingProvider::HOTELBEDS) {
                continue;
            }
            
            $hotelReservationRQ = new HotelReservationRQV2($p, $item, $i);
            $xml    = $this->request->execRequest($hotelReservationRQ, 'XML_RQ', 'BB_HB_RQ_PURCHASE');
            // include('dummy_xml/BookingsRS.php');
            $res          = new HotelBedsRSV2($xml, 'XML_RS', 'BB_HB_RS_PURCHASE');
            $response     = $res->getResponse();
            $services[]   = new ServiceRSV2($response, $item, $errors);
        }
        if (count($services) == 0) return NULL;
        return new PurchaseRSV2($services);
    }

    public function execPurchaseDetailRQ($p)
    {
        $localizer        = $p['localizer'];
        $serviceType      = '';
        if($p['serviceType'] == 1) {
          $serviceType      = 'ServiceHotel';
          $ReadServiceRQ    = new ReadServiceRQV2($localizer);
        } else if ($p['serviceType'] == 2) {
          $serviceType      = 'ServiceTicket';
          $ReadServiceRQ    = new ReadServiceTicketRQ($localizer);
        }

        $rs = $this->request->execRequest($ReadServiceRQ);
        // include('dummy_xml/HotelDetailRS.php');
        // \Log::info(json_encode($rs), 'MB_RS_INFOBOOK');
        $serviceRS  = new HotelBedsRSV2($rs);
        $service    = new PurchasedServiceRSV2($serviceRS->getResponse(), $serviceType);
        return new PurchaseRSV2(array($service), $serviceRS->getResponse());
    }

    public function execHotelDetailRQ($p)
    {
        $request = new HotelDetailRQV2($p['hotelCode']);
        $rs      = $this->request->execRequest($request);
        $rs      = new HotelBedsRSV2($rs);
        return new HotelDetailRSV2($rs->getResponse());
    }

    public function execPurchaseCancelRQ($p)
    {
        $localizer            = $p['localizer'];
        $serviceType          = isset($p['serviceType'])?$p['serviceType']:1;
        $totalCancellationFee = 0;
        if ($serviceType == \ServiceType::HOTEL) {
            $CancellBookRQ  = new CancellBookRQV2($localizer, $serviceType);
        } else if($serviceType == \ServiceType::TICKET) {
            $CancellBookRQ  = new CancelTicketRQV2($localizer, $serviceType);
        }
        $rs = $this->request->execRequest($CancellBookRQ, 'XML_RQ', 'BB_HB_RQ_CANCELBOOK');
        \Log::info($rs, 'BB_HB_RS_CANCELBOOK');
        
        $cancellBookRS  = new HotelBedsRSV2($rs);
        $cancellBookRS  = $cancellBookRS->getResponse();
        
        if (isset($cancellBookRS->booking->totalNet)) {
            $totalCancellationFee += floatval($cancellBookRS->booking->totalNet);
        }
        return new PurchaseRSV2(array(), '', $totalCancellationFee);
    }
}
