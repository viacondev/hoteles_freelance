<?php

namespace HotelBedsV2;

include_once('SCartRoomAvailRS.php');

class SCartHotelAvailRSV2 extends \HotelAvail
{

  private $data;
  private $rules;
  public  $hotelDetail;
  public  $hotelDiscount;
  public  $requiredBithDateChild;
  public function __construct($data, $rs, $factorFee)
  {
    $this->data = $data;
    $this->rs   = $rs;
    $from       = $data->dateFrom;
    $to         = $data->dateTo;

    $this->roomsAvail = array();
    $indexChild = 0;
    foreach ($rs->rooms as $room) {
      $room_arr = ['code' => $room->code, 'name' => $room->name];
      foreach ($room->rates as $key => $rate) {
        // $adultCount = $room->adultCount;
        // $indexChild += $room->childCount;
        $this->roomsAvail[] = new SCartRoomAvailRS($room_arr, $rate, $from, $to, $factorFee);
      }
    }
    $this->requiredBithDateChild = false;
    if ($indexChild !== 0) {
      $this->requiredBithDateChild = true;
    }
  }

  public function getProvider()
  {
    return \BookingProvider::HOTELBEDS;
  }

  public function getProviderName()
  {
    return 'HOTELBEDS';
  }

  public function getAvailToken()
  {
    return '';
  }

  public function getCode()
  {
    return $this->rs->code;
  }

  public function getName()
  {
    return $this->data->hotelName;
  }

  public function getDateFrom()
  {
    return $this->data->dateFrom;
  }

  public function getDateTo()
  {
    return $this->data->dateTo;
  }

  public function getDestinationCode()
  {
    return $this->data->destCode;
  }

  public function getDestinationType()
  {
    return '';
  }

  public function getMainImage()
  {
    return '';
  }

  public function getDescription()
  {
    return '';
  }

  public function getLatitude()
  {
    return '';
  }

  public function getLongitude()
  {
    return '';
  }

  public function getZone()
  {
    return '';
  }

  public function getLocation()
  {
    return '';
  }

  public function getCategory()
  {
    return '';
  }

  public function getCategoryType()
  {
    return '';
  }

  public function getCategoryCode()
  {
    return strval($this->data->category);
  }

  public function getRoomsAvail()
  {
    return $this->roomsAvail;
  }

  public function getGroupedRooms()
  {
    return $this->roomsAvail;
  }

  public function getCurrency()
  {
    return $this->rs->currency;
  }

  public function getContractName()
  {
    return '';
  }

  public function getContractIncomingOfficeCode()
  {
    return '';
  }

  public function getContractComment()
  {
    return "";
  }

  public function getClientComment()
  {
    return '';
  }

  public function getSupplier()
  {
    return '';
  }

  public function getSupplierVatNumber()
  {
    return '';
  }

  public function getDiscountList()
  {
    return array();
  }

  public function getAllPaxRequired()
  {
    return true;
  }

  public function getXtraDataRequired()
  {
    return false;
  }

  public function isDataRequired()
  {
    return false;
  }

  public function getRequiredBithDateChild()
  {
    return $this->requiredBithDateChild;
  }

  public function calculateNights()
  {
    $from = new \DateTime($this->getDateFrom());
    $to = new \DateTime($this->getDateTo());
    return $to->diff($from)->days;
  }
}
