<?php
namespace HotelBedsV2;

include_once('PurchasedRoomRS.php');

class PurchasedHotelRSV2 extends \HotelAvail {

  private $element;
  private $rooms;
  private $rateComments;

  public function __construct($element) {
    $this->element = $element;
    $this->rooms = array();
    $this->rateComments = '';
    foreach ($element->rooms as $room) {
      foreach($room->rates as $rate) {
        $this->rateComments = $rate->rateComments;
        $this->rooms[] = new PurchasedRoomRSV2($room, $rate);
      }
    }

  }

  public function getProvider() {
    return \BookingProvider::HOTELBEDS;
  }

  public function getProviderName() {
    return 'HOTELBEDS';
  }

  public function getAvailToken() {
    return '';
  }

  public function getCode() {
    return strval($this->element->code);
  }

  public function getName() {
    return strval($this->element->name);
  }

  public function getDateFrom() {
    $res = $this->elements->checkIn;
    $checkin = explode('-', $res);
    $checkin = $checkin[0] . $checkin[1] . $checkin[2];
    return $checkin;
  }

  public function getDateTo() {
    $res = $this->elements->checkOut;
    $checkOut = explode('-', $res);
    $checkOut = $checkOut[0] . $checkOut[1] . $checkOut[2];
    return $checkOut;
  }

  public function getDestinationCode() {
    // TODO
    return '';
  }

  public function getDestinationType() {
    return '';
  }

  public function getMainImage() {
    return '';
  }

  public function getDescription() {
    return '';
  }

  public function getLatitude() {
    return '';
  }

  public function getLongitude() {
    return '';
  }

  public function getZone() {
    return '';
  }

  public function getLocation() {
    return '';
  }

  public function getCategory() {
    return $this->element->categoryName;
  }

  public function getCategoryType() {
    return '';
  }

  public function getCategoryCode() {
    // TODO
    return $this->element->categoryCode;
  }

  public function getRoomsAvail() {
    return $this->rooms;
  }

  public function getGroupedRooms() {
    return $this->rooms;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getContractName() {
    return '';
  }

  public function getContractIncomingOfficeCode() {
    return '';
  }

  public function getContractComment() {
    return $this->rateComments;
  }

  public function getClientComment() {
    return '';
  }

  public function getSupplier() {
    return $this->element->supplier->name;
  }

  public function getSupplierVatNumber() {
    return $this->element->supplier->vatNumber;
  }

  public function getDiscountList() {
    return array();
  }

  public function areAllPaxRequired() {
    return true;
  }

  public function isExtraDataRequired() {
    return true;
  }

  public function isDataRequired() {
    return false;
  }

  public function getIsWithPromotion() {
    return array();
  }
}

?>
