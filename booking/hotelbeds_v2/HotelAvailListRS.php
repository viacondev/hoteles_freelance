<?php

namespace HotelBedsV2;

include_once('HotelAvailRS.php');

class HotelAvailListRSV2 extends \HotelAvailList
{

  private $elements;
  public $serviceHotels;

  public function __construct($rs, $parametros, $roomCount, $factorFee)
  {
    $this->serviceHotels    = array();
    $hotelsList             = $rs->hotels->hotels;
    $dateFrom               = $parametros['checkin'];
    $dateTo                 = $parametros['checkout'];
    foreach ($hotelsList as $hotel) {
      $hotelAvail = new HotelAvailRS($hotel, $dateFrom, $dateTo, $roomCount, $factorFee);
      $count = count($hotelAvail->getGroupedRooms());
      if ($count > 0) {
        $this->serviceHotels[] = $hotelAvail;
      }
    }
  }

  public function getHotelsAvail()
  {
    return $this->serviceHotels;
  }

  public function getCurrentPage()
  {
    return 1;
  }

  public function getTotalPages()
  {
    return 1;
  }
}
