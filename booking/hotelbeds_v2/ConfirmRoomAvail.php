<?php
namespace HotelBedsV2;

class ConfirmRoomAvailV2 extends \RoomAvail {

  public $data;
  public $rate;
  public $factorFee;
  public $date_out;
  public $childAges;
  public $rules;
  private $paxs;
  private $totalAmount;

  public function __construct($room, $rate, $factorFee) {
    $this->data     = $room;
    $this->rate     = $rate;
    $this->factorFee = $factorFee;
  }

  public function getProvider() {
    return \BookingProvider::HOTELBEDS;
  }

  public function getHotel() {
    return '';
  }

  public function getRoomCount() {
    return $this->rate->rooms;
  }

  public function getAdultCount() {
    return $this->rate->adults;
  }

  public function getChildCount() {
    return $this->rate->children;
  }

  public function getPrice() {
    return $this->rate->net;
  }

  public function getSHRUI() {
    return '';
  }

  public function getOnRequest() {
    return '';
  }

  public function getBoard() {
    return $this->rate->boardName;
  }

  public function getBoardCode() {
    return $this->rate->boardCode;
  }

  public function getBoardType() {
    return '';
  }

  public function getRoomType() {
    return $this->data->name;
  }

  public function getRoomTypeType() {
    return '';
  }

  public function getRoomTypeCode() {
    return $this->data->code;
  }

  public function getRoomTypeCharacteristic() {
    return '';
  }

  public function getCancellationPolicies() {
    $policies     = array();
    $totalAmount  = $this->totalAmount;
    $amount       = 0;
    $rules        = $this->rate->cancellationPolicies;
    foreach ($rules as $rule) {
      $amount       = $rule->amount;
      $timestamp    = explode('T', $rule->from);
      $dateFrom     = new \DateTime($timestamp[0]);
      $cancellation = new \BookingCancellationPolicy();
      $cancellation->build($amount, $dateFrom, '00:00', true,'' , $this->factorFee);
      $policies[]   = $cancellation;
    }
    return $policies;
  }

  public function getGuestList() {
    $guests = array();
    $customer_counter = 0;
    $arrAges = [];
    foreach ($this->data->paxes as $key => $value) {
      if($value->type == 'CH') {
        $arrAges[] = isset($value->age)?$value->age:0;
      }
    }
    for ($i = 0; $i < $this->getChildCount(); $i++) {
      $customer_counter++;
      $age = isset($arrAges[$i])?$arrAges[$i]:0;
      $guests[] = new \BookingCustomer('CH', $customer_counter, $age, '', '');
    }
    return $guests;
  }
}

?>
