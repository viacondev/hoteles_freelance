<?php
namespace HotelBedsV2;

class CancellBookRQV2 {

  private $localizer;

  public function __construct($localizer) {
    $this->localizer = $localizer;
  }

  public function getType() {
    return 'DELETE';
  }

  public function getRequest() {
    return 'hotel-api/1.0/bookings';    
  }

  public function getParams() {
    return '/'.$this->localizer;
  }
}
?>
