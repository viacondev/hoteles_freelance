<?php
namespace HotelBedsV2;

class AvailableHotelsRQV2 {

  public $checkIn;
  public $checkOut;
  public $rooms;
  public function __construct($checkIn, $checkOut, $rooms) {
    $this->checkIn  = $checkIn;
    $this->checkOut = $checkOut;
    $this->rooms    = $rooms;
  }

  public function getType() {
    return 'POST';
  }

  public function getRequest() {
    return 'hotel-api/1.0/hotels';    
  }

  public function getParams() {
    $request = '{
      "stay": {"checkIn": "' . $this->checkIn . '","checkOut": "' . $this->checkOut . '"},
      "occupancies": ' . json_encode($this->rooms) . ',
      "hotels": {"hotel": [77,168,264,265,297,311]}
      }'; 
    return $request; 
  }
}

?>
