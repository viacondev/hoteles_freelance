<?php

namespace HotelBedsV2;

include_once('SCartHotelAvailRS.php');

class SCartServiceRSV2 extends \BookingService
{

  private $availableRooms;
  private $data;
  private $element;
  private $rs;
  public  $hotelDetail;
  public  $errors;
  // private $localizer;

  public function __construct($data, $rs, $total_price = 0)
  {
    $this->data           = json_decode(json_encode($data), FALSE);
    $this->rs             = $rs->hotel;
    $this->total_price    = $total_price;
    $this->errors         = [];
  }

  public function getLocalizer()
  {
    return '';
  }

  public function getSPUI()
  {
    return '';
  }

  public function getServiceType()
  {
    return 'ServiceHotel';
  }

  public function getServiceStatus()
  {
    return '';
  }

  public function getDirectPayment()
  {
    return '';
  }

  public function getDateFrom()
  {
    return $this->data->dateFrom;
  }

  public function getDateTo()
  {
    return $this->data->dateTo;
  }

  public function getCurrency()
  {
    return $this->rs->currency;
  }

  public function getTotalAmount()
  {
    if (isset($this->rs->totalNet) && $this->rs->totalNet != 0) {
      return $this->rs->totalNet;
    }

    $price = 0;
    foreach ($this->data->rooms as $room) {
      $price += $room->price;
    }
    return $price;
  }

  public function getTotalAmountWithFee()
  {
    if (isset($this->rs->totalNet) && $this->rs->totalNet != 0) {
      return round($this->rs->totalNet / $this->data->factorFee, 2);
    }

    $price = 0;
    foreach ($this->data->rooms as $room) {
      $price += $room->price;
    }
    return round($price / $this->data->factorFee, 2);
  }

  public function getAdditionalCosts()
  {
    return array();
  }

  public function getServiceInfo()
  {
    return new SCartHotelAvailRS($this->data, $this->rs, $this->data->factorFee);
  }

  public function getSupplementList()
  {
    return array();
  }

  public function getStatus()
  {
    return \BookingServiceStatus::PENDING;
  }

  public function getErrors()
  {
    return $this->errors;
  }
}
