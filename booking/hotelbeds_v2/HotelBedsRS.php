<?php

namespace HotelBedsV2;

use App\Log;

class HotelBedsRSV2
{

  private $response;

  public function __construct($xml, $solicitud = "", $type = "")
  {
    $xml = htmlspecialchars_decode($xml);
    $this->response = json_decode($xml);

    if ($solicitud == 'XML_RS') {
      $descrip      = $type;
      $referencia   = '';
      \Log::info($xml, $descrip);
    }
    // __logtxt($xml);
  }

  public function getResponse()
  {
    return $this->response;
  }

  public function attributes()
  {
    $attributes = $this->response->xpath('//ns1:AuditData/..');
    return $attributes[0]->attributes();
  }
}
