<?php
namespace HotelBedsV2;

class CancelTicketRQV2 {

  private $localizer;
  private $serviceType;

  public function __construct($localizer, $serviceType = 1) {
    $this->localizer    = $localizer;
    $this->serviceType  = $serviceType;
  }

  public function getType() {
    return 'DELETE';
  }

  public function getRequest() {
    return 'activity-api/3.0/bookings/en';    
  }

  public function getServiceType() {
    return $this->serviceType;
  }
  
  public function getParams() {
    return '/'.$this->localizer;
  }
}
?>
