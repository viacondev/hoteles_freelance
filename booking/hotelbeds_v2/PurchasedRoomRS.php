<?php
namespace HotelBedsV2;

class PurchasedRoomRSV2 extends \RoomAvail {

  private $elements;
  private $rate;
  private $guests;
  public $roomType;

  public function __construct($elements, $rate) {
    $this->elements = $elements;
    $this->rate     = $rate;
    $this->guests     = array();
    $i = 0;
    foreach($elements->paxes as $pax) {
        $type   = $pax->type;
        $age    = $type == 'AD' ? '30':$pax->age;
        $name   = $pax->name;
        $lastName = $pax->surname; 
        $this->guests[] = new \BookingCustomer($type, $i, $age, $name, $lastName);
        $i++;
    }

  }

  public function getProvider() {
    return \BookingProvider::HOTELBEDS;
  }

  public function getHotel() {
    return NULL;
  }

  public function getRoomCount() {
    return $this->rate->rooms;
  }

  public function getAdultCount() {
    return intval($this->rate->adults);
  }

  public function getChildCount() {
    return intval($this->rate->children);
  }

  public function getPrice() {
    // TODO: return price
    return 0;
  }

  public function getSHRUI() {
    return '';
  }

  public function getOnRequest() {
    return '';
  }

  public function getBoard() {
    return $this->rate->boardName;
  }

  public function getBoardCode() {
    return $this->rate->boardCode;
  }

  public function getBoardType() {
    return '';
  }

  public function getRoomType() {
    return $this->elements->name;
  }

  public function getRoomTypeType() {
    return '';
  }

  public function getRoomTypeCode() {
    return '';
  }

  public function getRoomTypeCharacteristic() {
    return '';
  }

  public function getCancellationPolicies() {
    $policies     = array();
    $amount       = 0;
    $rules        = $this->rate->cancellationPolicies;
    foreach ($rules as $rule) {
      $amount       = $rule->amount;
      $timestamp    = explode('T', $rule->from);
      $dateFrom     = new \DateTime($timestamp[0]);
      $cancellation = new \BookingCancellationPolicy();
      $cancellation->build($amount, $dateFrom, '00:00', true, '', 0.80);
      $policies[]   = $cancellation;
    }
    return $policies;
  }

  public function getGuestList() {
    return $this->guests;
  }
}

?>