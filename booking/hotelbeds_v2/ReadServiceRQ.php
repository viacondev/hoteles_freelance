<?php
namespace HotelBedsV2;

class ReadServiceRQV2 {

  private $localizer;
  public function __construct($localizer) {
    $this->localizer = $localizer;
  }

  public function getType() {
    return 'GET';
  }

  public function getRequest() {
    return 'hotel-api/1.0/bookings';    
  }

  public function getServiceType() {
    return 1;
  }
  
  function getParams() {
    return '/'.$this->localizer;
  }

}
?>