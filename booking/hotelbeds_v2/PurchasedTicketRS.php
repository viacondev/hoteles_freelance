<?php
namespace HotelBedsV2;

// include_once('AvailableModalityRS.php');

class PurchasedTicketRS extends \BookingTicketAvail {

  private $element;
  public $availableModalityList;
  public $params;
  public $adulCount;
  public $childCount;
  public $activity;

  public function __construct($element, $factorFee = 0.1) {
    $this->element  = $element;
    $this->activity = $element->activities[0];
    $this->params   = [];
    $this->availableModalityList = array();
    // foreach ($element->AvailableModality as $availableModality) {
    //   $this->availableModalityList[] = new AvailableModalityRS($availableModality, $factorFee);
    // }
    $this->adultCount = 0;
    $this->childCount = 0;
    foreach ($this->activity->paxes as $key => $pax) {
      if ($pax->paxType == 'AD') {
        $this->adultCount++;
      }
      if ($pax->paxType == 'CH') {
        $this->childCount++;
      }
    }

  }

  public function getProvider() {
    return \BookingProvider::HOTELBEDS;
  }

  public function getAvailToken() {
    return strval($this->element->code);
  }

  public function getCode() {
    return strval($this->activity->code);
  }

  public function getName() {
    return strval($this->activity->name);
  }

  public function getDateFrom() {
    return $this->activity->dateFrom;
  }

  public function getDateTo() {
    return $this->activity->dateTo;
  }

  public function getCurrency() {
    return strval($this->element->currency);
  }

  public function getDescription() {
    return strval($this->element->content->description);
  }

  public function getMainImage() {
    if (isset($this->element->content->media)) {
        $images = $this->element->content->media->images;
        return str_replace('/small', '', strval($images[0]->urls[0]->resource));
    }
  
    return 'assets/images/no-photo.jpg';
  }

  public function getCompanyCode() {
    return strval('');
  }

  public function getTicketClass() {
    return strval($this->element->type);
  }

  public function getDestinationCode() {
    if (isset($this->activity->contactInfo->country->destinations[0]->code)) {
        return strval($this->activity->contactInfo->country->destinations[0]->code);
    }
    return '';
  }

  public function getDestinationType() {
    if (isset($this->activity->contactInfo->country->destinations[0]->name)) {
      return strval($this->activity->contactInfo->country->destinations[0]->name);
  }
  return '';
  }

  public function getClassification() {
    return  '';
  }

  public function getClassificationCode() {
    return '';
  }

  public function getAvailableModalityList() {
    return $this->availableModalityList;
  }

  public function getAdultCount() {
    return $this->adultCount;
  }

  public function getChildCount() {
    return $this->childCount;
  }

  public function getContractName() {
    return '';
  }

  public function getContractIncomingOfficeCode() {
    return '';
  }

  public function getContractComment() {
    return $this->activity->comments[0]->text;
  }

  public function getCancellationPolicies() {
    $policies_arr = array();
    foreach ($this->activity->cancellationPolicies as $policy) {
      $amount   = $policy->amount;
      $date     = explode('T', $policy->dateFrom);
      $dateFrom = new \DateTime($date[0]);
      $time     = '00:00';
      $cancellation = new \BookingCancellationPolicy();
      $cancellation->build($amount, $dateFrom, $time, true);
      $policies_arr[] = $cancellation;
    }

    return $policies_arr;
  }

  public function getGuestList() {
    $guests     = array();
    $paxes      = $this->activity->paxes;
    foreach ($paxes as $key => $pax) {
      $type       = $pax->paxType;
      $customerId = 0;
      $age        = $pax->age;
      $name       = $pax->name;
      $lastName   = $pax->surname;
      $guests[] = new \BookingCustomer($type, $customerId, $age, $name, $lastName);
    }
    return $guests;
  }

  public function getServiceDetailList() {
    if (!isset($this->element->ServiceDetailList->ServiceDetail)) {
        return array();
      }
      foreach ($this->element->ServiceDetailList->ServiceDetail as $service) {
        $attrs = $service->attributes();
        $serviceDetail = new \BookingServiceDetail();
        $serviceDetail->build($attrs['code'], $service->Name);
        $services[] = $serviceDetail;
      }
  
      return $services;
  }

  public function getSupplier() {
    return $this->activity->supplier->name;
  }

  public function getSupplierVatNumber() {
    return $this->activity->supplier->vatNumber;
  }

  public function getSupplierInformation() {
    return strval($this->activity->providerInformation->name);
  }

  public function getModalityName() {
    return $this->activity->modality->name;
  }

  public function getVouchers() {
    $arrVouchers = [];
    foreach ($this->activity->vouchers as $value) {
      $arrVouchers[] = $value->url;
    }
    return $arrVouchers;
  }
}

?>
