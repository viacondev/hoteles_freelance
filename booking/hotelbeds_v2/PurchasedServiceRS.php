<?php
namespace HotelBedsV2;

/**
 * Class used for confirmed Services
 */
class PurchasedServiceRSV2 extends \BookingService {

  private $elements;
  public $serviceType;
  public $checkIn;
  public $checkOut;
  public $service;
  public $errors;

  public function __construct($rs, $serviceType) {
    $this->elements = $rs->booking;
    $this->serviceType  = $serviceType;
    if ($this->serviceType == 'ServiceHotel') {
      $checkIn        = explode('-', $this->elements->hotel->checkIn);
      $this->checkIn  = $checkIn[0] . $checkIn[1] . $checkIn[2];
      $checkOut       = explode('-', $this->elements->hotel->checkOut);
      $this->checkOut = $checkOut[0] . $checkOut[1] . $checkOut[2];
      $this->service  = new PurchasedHotelRSV2($this->elements->hotel);
    } else if ($this->serviceType == 'ServiceTicket') {
      $checkIn        = explode('-', $this->elements->activities[0]->dateFrom);
      $this->checkIn  = $checkIn[0] . $checkIn[1] . $checkIn[2];
      $checkOut       = explode('-', $this->elements->activities[0]->dateTo);
      $this->checkOut = $checkOut[0] . $checkOut[1] . $checkOut[2];
      $this->service  = new PurchasedTicketRS($this->elements);
    }
  }

  public function getLocalizer() {
    $localizer = $this->elements->reference;
    return strval($localizer);
  }

  public function getSPUI() {
    return '';
  }

  public function getServiceType() {
    return $this->serviceType;
  }

  public function getServiceStatus() {
    return '';
  }

  public function getDirectPayment() {
    return '';
  }

  public function getDateFrom() {
    return $this->checkIn;
  }

  public function getDateTo() {
    return $this->checkOut;
  }

  public function getCurrency() {
    return $this->elements->hotel->currency;
  }

  public function getTotalAmount() {
    $totalAmount = $this->elements->totalNet;
    return floatval($totalAmount);
  }

  public function getAdditionalCosts() {
    return array();
  }

  public function getServiceInfo() {
    return $this->service;
  }

  public function getSupplementList() {
    return array();
  }

  public function getStatus() {
    $status = $this->elements->status;
    if ($status == 'CONFIRMED') {
      return \ServiceStatus::CONFIRMED;
    }
    return \ServiceStatus::CANCELLED;
  }

  public function getErrors() {
    return $this->errors;
  }

}
