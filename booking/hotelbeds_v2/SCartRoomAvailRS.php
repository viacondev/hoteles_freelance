<?php

namespace HotelBedsV2;

class SCartRoomAvailRSV2 extends \RoomAvail
{

  private $data;
  private $rate;
  public $hotel;
  private $date_in;
  private $date_out;
  public $childAges;
  private $rules;
  private $totalAmount;
  private $indexChild;
  private $factorFee;

  public function __construct($room = array(), $rate, $checkIn, $checkOut, $factorFee = 0.79)
  {
    // __logarr($rate);
    $this->data        = $room;
    $this->rate        = $rate;
    $this->hotel       = array();
    $this->date_in     = $checkIn;
    $this->date_out    = $checkOut;
    $this->factorFee   = $factorFee;
  }

  public function getProvider()
  {
    return \BookingProvider::HOTELBEDS;
  }

  public function getHotel()
  {
    return $this->hotel;
  }

  public function getProviderName()
  {
    return 'HOTELBEDS';
  }

  public function getHotelCode()
  {
    return '';
  }

  public function getRoomCount()
  {
    return $this->rate->rooms;
  }

  public function getAdultCount()
  {
    return intval($this->rate->adults);
  }

  public function getChildCount()
  {
    return $this->rate->children;
  }

  public function getPrice()
  {
    return $this->rate->net;
  }

  public function getPriceWithFee()
  {
    return round($this->rate->net / $this->factorFee, 2);
  }

  public function getSHRUI()
  {
    return $this->rate->rateKey;
  }

  public function getOnRequest()
  {
    return '';
  }

  public function getBoard()
  {
    return $this->rate->boardName;
  }

  public function getBoardCode()
  {
    return $this->rate->boardCode;
  }

  public function getBoardType()
  {
    // return $this->data->boardType;
    return '';
  }

  public function getRoomType()
  {
    return $this->data['name'];
  }

  public function getRoomTypeType()
  {
    return $this->rate->rateType;
  }

  public function getRoomTypeCode()
  {
    return $this->data['code'];
  }

  public function getRoomTypeCharacteristic()
  {
    return '';
  }

  public function getCancellationPolicies()
  {
    $policies     = array();
    $totalAmount  = $this->totalAmount;
    $amount       = 0;
    $rules        = $this->rate->cancellationPolicies;
    foreach ($rules as $rule) {
      $amount       = $rule->amount;
      $timestamp    = explode('T', $rule->from);
      $dateFrom     = new \DateTime($timestamp[0]);
      $cancellation = new \BookingCancellationPolicy();
      $cancellation->build($amount, $dateFrom, '00:00', true, '', $this->factorFee);
      $policies[]   = $cancellation;
    }

    return $policies;
  }

  public function getGuestList()
  {
    $customer_counter = 0;

    for ($i = 0; $i < $this->getAdultCount(); $i++) {
      $customer_counter++;
      $guests[] = new \BookingCustomer('AD', $customer_counter, '30', '', '');
    }
    $indexChild = $this->indexChild;
    $childIndex = 0;
    for ($i = 0; $i < $this->getChildCount(); $i++) {
      $customer_counter++;
      $childAges  = $this->childAges;
      $childAge   = $childAges[$indexChild];
      $guests[]   = new \BookingCustomer('CH', $customer_counter, $childAge, '', '');
      $indexChild++;
      $childIndex++;
    }

    return $guests;
  }

  public function buildObjecArray()
  {
    return array();
  }
}
