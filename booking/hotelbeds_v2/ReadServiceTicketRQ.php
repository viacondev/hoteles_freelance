<?php
namespace HotelBedsV2;

class ReadServiceTicketRQ {

  private $localizer;
  public function __construct($localizer) {
    $this->localizer = $localizer;
  }

  public function getType() {
    return 'GET';
  }

  public function getRequest() {
    return 'activity-api/3.0/bookings/es';    
  }

  public function getServiceType() {
    return 2;
  }

  function getParams() {
    return '/'.$this->localizer;
  }

}
?>