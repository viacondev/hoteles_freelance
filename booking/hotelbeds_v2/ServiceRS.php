<?php
namespace HotelBedsV2;

/**
 * Class used for confirmed Services
 */
class ServiceRSV2 extends \BookingService {

  private $elements;
  private $item;
  private $errors;
  public $totalAmount;

  public function __construct($rs, $scItem, $errors = array()) {
    $this->elements     = $rs->booking;
    $this->errors       = $errors;
    $this->totalAmount  = $this->elements->totalNet;
    $this->item         = json_decode(json_encode($scItem), FALSE);
  }

  public function getLocalizer() {
    $localizer = $this->elements->reference;
    return strval($localizer);
  }

  public function getSPUI() {
    return '';
  }

  public function getServiceType() {
    return 'ServiceHotel';
  }

  public function getServiceStatus() {
    return '';
  }

  public function getDirectPayment() {
    return '';
  }

  public function getDateFrom() {
    $res = $this->elements->hotel->checkIn;
    $checkin = explode('-', $res);
    $checkin = $checkin[0] . $checkin[1] . $checkin[2];
    return $checkin;
    // return ''
  }

  public function getDateTo() {
    $res = $this->elements->hotel->checkOut;
    $checkOut = explode('-', $res);
    $checkOut = $checkOut[0] . $checkOut[1] . $checkOut[2];
    return $checkOut;
  }

  public function getCurrency() {
    return strval($this->elements->currency);
  }

  public function getTotalAmount() {
    // $totalAmount = $this->elements->ResGroup->attributes();
    return floatval($this->totalAmount);
  }

  public function getAdditionalCosts() {
    return array();
  }

  public function getServiceInfo() {
    $accommodations = $this->elements;
    $totalAmount    = $this->getTotalAmount();
    return new ConfirmHotelAvailV2($accommodations, $this->item, $totalAmount);
  }

  public function getSupplementList() {
    return array();
  }

  public function getStatus() {
    $status = strval($this->elements->status);

    if ($status == 'CONFIRMED') {
      return \ServiceStatus::CONFIRMED;
    }
    return \ServiceStatus::CANCELLED;
  }

  public function getErrors() {
    return $this->errors;
  }

  public function getIsConector() {
    return false;
  }

}

?>
