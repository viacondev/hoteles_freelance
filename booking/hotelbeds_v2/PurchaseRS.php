<?php

namespace HotelBedsV2;

class PurchaseRSV2 extends \BookingPurchase
{

  public $services;
  public $element;
  private $total_price;

  /**
   * @total_price is used for cancellation fees. For a Purchase it is ok to leave it as 0.
   */
  public function __construct($services, $element = null, $total_price = 0)
  {
    $this->services     = $services;
    $this->element      = isset($element->booking) ? $element->booking : null;
    $this->total_price  = $total_price;
  }

  public function getPurchaseToken()
  {
    return '';
  }

  public function getStatus()
  {
    $status = isset($this->element->status) ? $this->element->status : 'NOT FOUND';
    if ($status == 'CONFIRMED') {
      return 'BOOKING';
    }
    return \ServiceStatus::CANCELLED;
  }

  public function getHolder()
  {
    return '';
  }

  public function getBookingServices()
  {
    return $this->services;
  }

  public function getTotalPrice()
  {
    return $this->total_price;
  }

  public function getCurrency()
  {
    return $this->element->currency;
  }

  public function getCreationDate()
  {
    $res = $this->element->creationDate;
    $creationDate = explode('-', $res);
    $creationDate = $creationDate[0] . $creationDate[1] . $creationDate[2];
    return $creationDate;
  }

  public function getReferenceFileNumber()
  {
    $file = explode('-', $this->element->reference);
    return $file[1];
  }

  public function getReferenceIncomingOffice()
  {
    $inco = explode('-', $this->element->reference);
    return $inco[0];
  }

  public function getAgencyReference()
  {
    return $this->element->clientReference;
  }

  public function getPaymentDataDescription()
  {
    return '';
  }
}
