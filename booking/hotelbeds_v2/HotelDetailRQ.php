<?php
namespace HotelBedsV2;

class HotelDetailRQV2 {

  private $code;

  public function __construct($code) {
    $this->code = $code;
  }

  public function getType() {
    return 'GET';
  }

  public function getRequest() {
    return 'hotel-content-api/1.0/hotels/';    
  }

  public function getServiceType() {
    return 1;
  }

  public function getParams() {
    $xml = $this->code.'/details';
    return $xml;
  }
}
?>
