<?php
namespace HotelBedsV2;


class ConfirmHotelAvailV2 extends \HotelAvail {

  public $data;
  public $rules;
  public $roomsAvail;
  public $item;

  public function __construct($data, $item, $totalAmount) {
    $this->item = $item;
    $this->data = $data->hotel;
    $rooms      = $data->hotel->rooms;
    $factorFee  = $item->factorFee;
    foreach ($rooms as $room) {
      foreach ($room->rates as $key => $rate) {
        $this->roomsAvail[] = new ConfirmRoomAvailV2($room, $rate, $factorFee);
      }
    }

  }

  public function getProvider() {
    return \BookingProvider::HOTELBEDS;
  }

  public function getProviderName() {
    return \BookingProvider::getProviderName(\BookingProvider::HOTELBEDS);
  }
  
  public function getAvailToken() {
    return '';
  }

  public function getCode() {
    $hotelCode = $this->data->code;
    return strval($hotelCode);
  }

  public function getName() {
    $hotelName = $this->data->name;
    return strval($hotelName);
  }

  public function getDateFrom() {
    $res = $this->data->checkIn;
    $checkin = explode('-', $res);
    $checkin = $checkin[0] . $checkin[1] . $checkin[2];
    return $checkin;
  }

  public function getDateTo() {
    $res = $this->data->checkOut;
    $checkOut = explode('-', $res);
    $checkOut = $checkOut[0] . $checkOut[1] . $checkOut[2];
    return $checkOut;
  }

  public function getDestinationCode() {
    return $this->data->destinationCode;
  }

  public function getDestinationType() {
    return '';
  }

  public function getMainImage() {
    return '';
  }

  public function getDescription() {
    return '';
  }

  public function getLatitude() {
    return '';
  }

  public function getLongitude() {
    return '';
  }

  public function getZone() {
    return '';
  }

  public function getLocation() {
    return '';
  }

  public function getCategory() {
    $category = intval($this->data->categoryName);

    return $category;
  }

  public function getCategoryType() {
    return '';
  }

  public function getCategoryCode() {
    $category = intval($this->data->categoryCode);

    return $category . 'EST';
  }

  public function getRoomsAvail() {
    return $this->roomsAvail;
  }

  public function getGroupedRooms() {
    return $this->roomsAvail;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getContractName() {
    return '';
  }

  public function getContractIncomingOfficeCode() {
    return '';
  }

  public function getContractComment() {
    return '';
  }

  public function getClientComment() {
    return '';
  }

  public function getSupplier() {
    return '';
  }

  public function getSupplierVatNumber() {
    return '';
  }

  public function getDiscountList() {
    return array();
  }

  public function areAllPaxRequired() {
    return true;
  }

  public function isExtraDataRequired() {
    return true;
  }

  public function isDataRequired() {
    return false;
  }

  public function getIsWithPromotion() {
    return array();
  }
}

?>
