<?php

namespace HotelBedsV2;

use App\Log;

class HotelBedsRQV2
{

  private $location;
  private $apikey;
  private $secret;

  public function __construct()
  {

    $this->location = \HBConfigV2::$location;
    $this->apikey   = \HBConfigV2::$apikey;
    $this->secret   = \HBConfigV2::$secret;
  }

  public function getRequestXML($RQElement)
  {
    return '<?xml version="1.0" encoding="UTF-8"?>
            <soapenv:Envelope soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
                xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
              <soapenv:Body>
                <hb:' . $RQElement->getRQElementMethodName() . ' xmlns:hb="http://axis.frontend.hydra.hotelbeds.com" xsi:type="xsd:anyType">
                  ' . $RQElement->getRQElementXML() . '
                </hb:' . $RQElement->getRQElementMethodName() . '>
              </soapenv:Body>
            </soapenv:Envelope>';
  }

  public function execRequest($RQElement, $solicitud = "", $type = "")
  {
    if (\HBConfigV2::$DirectXMLAccess) {
      $apiKey = \HBConfigV2::$apikey;
      $secret = \HBConfigV2::$secret;
      $url    = \HBConfigV2::$location;
      if ($RQElement->getServiceType() == \ServiceType::TICKET) {
        // $url    = 'https://api.test.hotelbeds.com/';
        $apiKey = \HBConfigV2::$apikeyTicket;
        $secret = \HBConfigV2::$secretTicket;
      }
      $request = $RQElement->getParams();
      if ($RQElement->getType() == 'GET') {
        $url .= $RQElement->getRequest() . $request;
      } else if($RQElement->getType() == 'DELETE'){
        $url = $this->location . $RQElement->getRequest() . $RQElement->getParams();
      } else {
        $url .= $RQElement->getRequest();
      }
      $header[] = 'X-Signature: ' . hash("sha256", $apiKey . $secret . time(), false);
      $header[] = 'Api-key: ' . $apiKey;
      $header[] = 'Accept: application/json';
      $header[] = 'Content-Type: application/json';
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
      /*curl_setopt($ch, array(
                                // CURLOPT_URL => $this->location.$RQElement->getRequest(),
                                CURLOPT_RETURNTRANSFER => true,
                                // CURLOPT_ENCODING => "",
                                // CURLOPT_CUSTOMREQUEST => "GET",
                                // CURLOPT_POSTFIELDS => $RQElement->getParams(),
                                CURLOPT_HTTPHEADER => $header,
                                CURLOPT_USERAGENT => $_SERVER['HTTP_USER_AGENT'])
                      );*/
      if ($RQElement->getType() == 'GET') {
        $url = $this->location . $RQElement->getRequest() . $RQElement->getParams();
        curl_setopt($ch, CURLOPT_URL, $url);
      } else if($RQElement->getType() == 'DELETE') {
        $url = $this->location . $RQElement->getRequest() . $RQElement->getParams();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $RQElement->getParams());
      } else {
        // $a_options[CURLOPT_URL]         = $this->location . $RQElement->getRequest();
        // $a_options[CURLOPT_POST]        = 1;
        // $a_options[CURLOPT_POSTFIELDS]  = $RQElement->getParams();
        curl_setopt($ch, CURLOPT_URL, $this->location . $RQElement->getRequest());
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $RQElement->getParams());
      }

      $xml = curl_exec($ch);
      curl_close($ch);
      if ($solicitud != '') {
        $descrip = $type;
        \Log::info($RQElement->getParams(), $descrip);
        \Log::info(json_encode($header), $descrip.'_header');
      }
      if ($xml ==  "") {
        return "";
      }
      return $xml;
    } else {
      // Call to a remote request wrapper because our current dev IP doesn't have access to HB.
      // This should be changed due security issues.
      $rq = 'http://barrybolivia.com/booking/hotelbeds/RemoteRequestWrapper.php';
      $rq .= '?location=' . urlencode($this->location);
      $rq .= '&wsdl=' . urlencode($this->wsdl);
      $rq .= '&method=' . urlencode($RQElement->getRequest());
      $rq .= '&request=' . urlencode($RQElement->getParams());

      // __logtxt($this->getRequestXML($RQElement));
      if ($solicitud == 'XML_RQ') {
        $descrip = $type;
        // \Log::__log('INFO', $descrip, $RQElement->getParams());
      }
      $context = stream_context_create(array('http' => array('header' => 'Connection: close\r\n')));
      return file_get_contents($rq, false, $context);
    }
  }

  public function allOptions($RQElement)
  {
    if (\HBConfigV2::$DirectXMLAccess) {
      $header[] = 'X-Signature: ' . hash("sha256", $this->apikey . $this->secret . time(), false);
      $header[] = 'Api-key: ' . $this->apikey;
      $header[] = 'Accept: application/json';
      $header[] = 'Content-Type: application/json';

      $options = array(
        CURLOPT_URL => $this->location . $RQElement->getRequest(),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $RQElement->getParams(),
        CURLOPT_HTTPHEADER => $header,
        CURLOPT_USERAGENT => $_SERVER['HTTP_USER_AGENT']
      );
    } else {
      $rq = 'http://barrybolivia.com/booking/hotelbeds/RemoteRequestWrapper.php';
      $rq .= '?location=' . urlencode($this->location);
      $rq .= '&wsdl=' . urlencode($this->wsdl);
      //   $rq .= '&method=' . urlencode($RQElement->getRQElementMethodName());
      //   $rq .= '&request=' . urlencode($this->getRequestXML($RQElement));
      $options  = array(
        CURLOPT_AUTOREFERER => true,
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL             => $rq
      );
    }
    return $options;
  }
}
