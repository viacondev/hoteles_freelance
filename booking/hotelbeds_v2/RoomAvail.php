<?php
namespace HotelBedsV2;

class RoomAvailV2 extends \RoomAvail {

  private $element;
  private $rate;
  public $hotel;
  public $hotelCode;
  public $adultCount;
  public $childCount;
  public $roomCount;
  public $price;
  public $childAges;
  public $board;
  public $boardCode;
  public $roomType;
  public $SHRUI;
  public $roomTypeCode;
  public $priceWithFee;

  public function __construct($element, $rate, $hotel, $currency, $factorFee = 0.79) {
    $this->element      = $element;
    $this->rate         = $rate;
    $this->hotel        = $hotel;
    $this->hotel        = json_encode($this->buildObjecArray());
    $this->priceWithFee = round(floatval($rate->net) / $factorFee, 2);
  }

  public function getProvider() {
    return \BookingProvider::HOTELBEDS;
  }

  public function getHotel() {
    return $this->hotel;
  }

  public function getProviderName() {
    return 'HOTELBEDS';
  }

  public function getHotelCode() {
    return strval($this->hotelCode);
  }

  public function getRoomCount() {
    return $this->rate->rooms;
  }

  public function getAdultCount() {
    return $this->rate->adults;
  }

  public function getChildCount() {
    return $this->rate->children;
  }

  public function getChildAges() {
    // return $this->childAges;
    return '';
  }

  public function getPrice() {
    return round($this->rate->net, 2);
  }

  public function getPriceWithFee() {
    return $this->priceWithFee;
  }

  public function getSHRUI() {
    return $this->rate->rateKey;
  }

  public function getOnRequest() {
    return $this->rate->rateClass;
  }

  public function getBoard() {
    return $this->rate->boardName;
  }

  public function getBoardCode() {
    return $this->rate->boardCode;
  }

  public function getBoardType() {
    return '';
  }

  public function getRoomType() {
    return ucwords(strtolower(htmlentities($this->element->name)));
  }

  public function getRoomTypeType() {
    return $this->rate->rateClass;
  }

  public function getRoomTypeCode() {
    return $this->element->code;
  }

  public function getRoomTypeCharacteristic() {
    return '';
  }

  public function getCancellationPolicies() {
    return array();
  }

  public function getGuestList() {
    return array();
  }

  public function buildObjecArray() {
    $con = 0;
    $hotel = $this->hotel;
    $methods = get_class_methods($hotel);
    $objArray = array();
    foreach ($methods as $method) {
      if (substr($method, 0, 2) == '__') continue;
      if (substr($method, 0, 3) === 'get' && $method != 'getGroupedRooms' && $method != 'getRoomsAvail') {
        $key = lcfirst(substr($method, 3));
        $objArray[$key] = $hotel->$method();
        $con++;
      }
    }
    return $objArray;
  }

}
