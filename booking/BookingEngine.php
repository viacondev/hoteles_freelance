<?php
ini_set('display_errors', 0);
include_once('BookingSerializable.php');
include_once('BookingProvider.php');
include_once('BookingRequestTypes.php');
include_once('HotelAvailList.php');
include_once('HotelAvailListRS.php');
include_once('BookingPurchase.php');
include_once('BookingPurchaseRS.php');
include_once('BookingPurchaseList.php');
include_once('BookingCustomer.php');
include_once('BookingCancellationPolicy.php');
include_once('BookingTicketAvailList.php');
include_once('BookingPrice.php');
include_once('BookingOperationDate.php');
include_once('BookingServiceDetail.php');
include_once('BookingTransferAvailList.php');
include_once('BookingHotelDetail.php');
include_once('hotelbeds/HotelBedsBooking.php');
include_once('domitur/DomiturBooking.php');
include_once('methabook/MethabookBooking.php');
include_once('BookingProvider.php');
include_once('MultiCUrl.php');
include_once('methabookk/MethabookkBooking_mbb.php');
include_once('restel/RestelBooking.php');
include_once('tourico/TouricoBooking.php');
include_once('tailorbeds/TailorBedsBooking.php');
include_once('nemo/NemoBooking.php');

include_once('hotelbeds_v2/HotelbedsBooking_v2.php');

include_once(__DIR__ . '/../zion/model/Profiling.php');
include_once(__DIR__ . '/../zion/model/HotelMapping.php');
include_once(__DIR__ . '/../zion/model/Model.php');//se agrego por que es necesario para comparacion de hoteles
include_once(__DIR__ . '/../zion/model/ServiceStatus.php');
include_once(__DIR__ . '/../zion/model/HotelTopTen.php');
include_once(__DIR__ . '/../zion/model/BackHotelsAvail.php');

class BookingEngine {

  private $hotelBedsBooking;
  private $domiturBooking;

  public function __construct() {
    $this->hotelBedsBooking   = new HotelBeds\HotelBedsBooking();
    $this->hotelBedsBookingV2 = new HotelBedsV2\HotelBedsBookingV2();
    $this->domiturBooking     = new Domitur\DomiturBooking();
    $this->methabookBooking   = new Methabook\MethabookBooking();
    $this->methabookkBooking  = new Methabookk\MethabookkBooking();
    $this->touricoBooking     = new Tourico\TouricoBooking();
    $this->domitur2Booking    = new Methabook\MethabookBooking(BookingProvider::DOMITUR2);
    $this->restelBooking      = new Restel\RestelBooking();
    $this->tailorbedsBooking  = new TailorBeds\TailorBedsBooking();
    $this->nemoBooking        = new Nemo\NemoBooking();
  }

  public function execRequest($params) {
    switch ($params['requestType']) {
      case \BookingRequestTypes::SearchHotelAvail:
        $res = $this->execHotelValuedAvailRQ($params);
        break;

      case \BookingRequestTypes::AddHotelService:
        $res = $this->execHotelServiceAddRQ($params);
        break;

      case \BookingRequestTypes::ConfirmPurchase:
        $res = $this->execPurchaseConfirmRQ($params);
        break;

      case \BookingRequestTypes::HotelDetail:
        $res = $this->execHotelDetailRQ($params);
        break;

      case \BookingRequestTypes::CancelPurchase:
        $res = $this->execPurchaseCancelRQ($params);
        break;

      case \BookingRequestTypes::PurchaseDetail:
        $res = $this->execPurchaseDetailRQ($params);
        break;

      case \BookingRequestTypes::PurchaseDetailList:
        $res = $this->execPurchaseDetailListRQ($params);
        break;

      case \BookingRequestTypes::SearchZones:
        $res = $this->execSearchZonesRQ($params);
        break;

      default:
        $res = $this->hotelBedsBooking->execRequest($params);
        break;
    }
    return $res;
  }

  public function execHotelValuedAvailRQ($p) {
    if (empty($p['providers']) || $p['destCode'] == 'DUB') {
      return new Domitur\HotelAvailListRS(NULL, '');
    }

    $rs_hb = NULL;
    $rs_dt = NULL;
    $rs_mb = NULL;
    $rs_mb2= NULL;
    $rs_th = NULL;
    $rs_dt2 = NULL;
    $rs_rt  = NULL;
    $rs_tb  = NULL;
    $rs_nm  = NULL;

    $objconexion = new MultiCUrl;

    if(in_array(BookingProvider::HOTELBEDS, $p['providers'])) { 
      $optionshb = $this->hotelBedsBooking->prepararConsulta($p);
      if ($optionshb != NULL) {
        $objconexion->pushData('HB', $optionshb);
      }
    }
    if(in_array(BookingProvider::DOMITUR2, $p['providers'])) { 
      $optionsdt = $this->domitur2Booking->prepararConsulta($p);
      if ($optionsdt != NULL) {
        $objconexion->pushData('DT', $optionsdt);
      }
    }
    if(in_array(BookingProvider::METHABOOK2, $p['providers'])) { 
      $optionsmb = $this->methabookkBooking->prepararConsulta($p);
      if ($optionsmb != NULL) {
        $objconexion->pushData('MB', $optionsmb); 
      } 
    }
    if(in_array(BookingProvider::TOURICO, $p['providers'])) { 
      $optionstr = $this->touricoBooking->prepararConsulta($p); 
      if ($optionstr != NULL) {
        $objconexion->pushData('TR', $optionstr);
      }
    }
    if(in_array(BookingProvider::RESTEL, $p['providers'])) { 
      $optionsrt = $this->restelBooking->prepararConsulta($p); 
      if ($optionsrt != NULL) {
        $objconexion->pushData('RT', $optionsrt);
      }
    }
    if(in_array(BookingProvider::TAILORBEDS, $p['providers'])) { 
      $optionstb = $this->tailorbedsBooking->prepararConsulta($p); 
      if ($optionstb != NULL) {
        $objconexion->pushData('TB', $optionstb);
      }
    }
    if(in_array(BookingProvider::NEMO, $p['providers'])) { 
      $optionsnm = $this->nemoBooking->prepararConsulta($p); 
      if ($optionsnm != NULL) {
        $objconexion->pushData('NM', $optionsnm);
      }
    }

    $objconexion->rolling_curl();

    $results = $objconexion->getResults();
    //echo "<pre>"; print_r($results); echo "</pre>";
    if (in_array(BookingProvider::HOTELBEDS, $p['providers']) && isset($results['HB'])) {
      $rs_hb  = $this->hotelBedsBooking->obtenerResultado($results['HB'], $p);
    }
    if(in_array(BookingProvider::DOMITUR2, $p['providers']) && isset($results['DT'])) {
      $rs_dt2 = $this->domitur2Booking->obtenerResultado($results['DT'], $p);
    }
    if(in_array(BookingProvider::METHABOOK2, $p['providers']) && isset($results['MB'])) {
      $rs_mb2 = $this->methabookkBooking->obtenerResultado($results['MB'], $p);
    }
    if(in_array(BookingProvider::TOURICO, $p['providers']) && isset($results['TR'])) {
      $rs_th  = $this->touricoBooking->obtenerResultado($results['TR'], $p);
    }
    if(in_array(BookingProvider::RESTEL, $p['providers']) && isset($results['RT'])) {
      $rs_rt  = $this->restelBooking->obtenerResultado($results['RT'], $p);
    }
    if(in_array(BookingProvider::TAILORBEDS, $p['providers']) && isset($results['TB'])) {
      $rs_tb  = $this->tailorbedsBooking->obtenerResultado($results['TB'], $p);
    }
    if(in_array(BookingProvider::NEMO, $p['providers']) && isset($results['NM'])) {
      $rs_nm  = $this->nemoBooking->obtenerResultado($results['NM'], $p);
    }

    //echo "<pre>"; print_r($rs_dt2); echo "</pre>";

    /*if (in_array(BookingProvider::HOTELBEDS, $p['providers'])) {
      $rs_hb = $this->hotelBedsBooking->execRequest($p);
    }*/
    /*if (in_array(BookingProvider::DOMITUR, $p['providers'])) {
      $rs_dt = $this->domiturBooking->execRequest($p);
    }*/
    /*if (in_array(BookingProvider::METHABOOK, $p['providers'])) {
      $rs_mb = $this->methabookBooking->execRequest($p);
    }*/
    /*if (in_array(BookingProvider::METHABOOK2, $p['providers'])) {
      $rs_mb2 = $this->methabookkBooking->execRequest($p);
    }*/
    /*if (in_array(BookingProvider::TOURICO, $p['providers'])) {
      $rs_th = $this->touricoBooking->execRequest($p);
    }*/
    /*if (in_array(BookingProvider::DOMITUR2, $p['providers'])) {
      $rs_dt2 = $this->domitur2Booking->execRequest($p);
    }*/
    /*if (in_array(BookingProvider::RESTEL, $p['providers'])) {
      $rs_rt = $this->restelBooking->execRequest($p);
    }
    if (in_array(BookingProvider::TAILORBEDS, $p['providers'])) {
      $rs_tb = $this->tailorbedsBooking->execRequest($p);
    }*/

    $rs = $this->joinHotels($rs_hb, $rs_dt, $rs_mb, $rs_mb2, $rs_th, $rs_dt2, $rs_rt, $rs_tb, $rs_nm);

    $orderedHotels        = array();
    $ordereddHotels       = array();
    $insertedHotelsKeys   = array();
    $arrHotelCodesMapping = array();
    $arrHotel = array();
    $limitQuery = 60;
    $con        = 1;
    $cadQuery   = '';
    $can = count($rs->getHotelsAvail());
    //Consulta los hoteles Mapeados por Cantidad Limiquery define la cantidad
    foreach ($rs->getHotelsAvail() as $hotel) {
      if ($hotel->getProvider() == BookingProvider::HOTELBEDS) { 
        $con++;
        continue;
      }
      $cadQuery .= ' h.CODE = "' . $hotel->getCode() . '" AND h.PROVIDER = ' . $hotel->getProvider() . ' OR';
      if ($con >= $limitQuery || $con == $can) {
        $limitQuery    *= $limitQuery;
        $result         = substr($cadQuery, 0, -2);
        $res            = HotelMappingProvider::getHotelMappingList($result);
        foreach ($res as $mappingCode) {
          $key = $mappingCode->code . '_' . $mappingCode->provider; 
          $arrHotelCodesMapping[$key] = $mappingCode;
        }
        $cadQuery   = '';
      }
      $con++;
    }
    // une los hoteles Mapeados los une mediante el codigo de HotelBeds
    //si no estuviera mapeado el codigo usa su propio codigo
    $indexMain = 0;

    foreach ($rs->getHotelsAvail() as $hotel) {
      if ($hotel->getProvider() == BookingProvider::HOTELBEDS) {
        $ordereddHotels[$hotel->getCode()] = $hotel;
        $indexMain++;
        continue;
      }
      $keyMap = $hotel->getCode() . '_' . $hotel->getProvider();
      if (isset($arrHotelCodesMapping[$keyMap])) {
        // esta es una validacion para los hoteles que si estan mapeados        
        $currentHotel = $arrHotelCodesMapping[$keyMap];
        if (isset($ordereddHotels[$currentHotel->zion_code])) {
          foreach ($ordereddHotels as $key => $value) {
            if ($key == $currentHotel->zion_code) {
              foreach ($hotel->getGroupedRooms() as $group) {
                $ordereddHotels[$currentHotel->zion_code]->insertGroupedRoom($group);
              }
            }
          }
        }
        else {
          $ordereddHotels[$currentHotel->zion_code] = $hotel;
        }
        
      }
      else {
        // opcion falso para hoteles que no estan mapeados
        $ordereddHotels[$hotel->getCode()] = $hotel;
      }
      $indexMain++;
    }

    foreach ($ordereddHotels as $hotel) {
      array_push($orderedHotels, $hotel);
    }
    // $price = $orderedHotels[0]->getGroupedRooms();
    usort($orderedHotels, array('BookingEngine','ordenarPrecio'));
    // $rs->serviceHotels = $orderedHotels;
    
    
    //Fin Seccion
    if($p['orderPrice'] == 'REC') {
      $orderedHotels = $this->getTopTen($orderedHotels, $p['destCode']);
    }
    // if (!empty($p['last-version']) || !empty($p['hotelCode'])) {
    //   $rs->serviceHotels = $orderedHotels;
    // }
    // else {
      $limit       =  5;
      $inicio      = ($p['page'] - 1) * $limit;
      $fin         = ($p['page'] * $limit) - 1;
      $totalPages  = ceil(count($orderedHotels) / $limit);

      $paginationHotels = array();
      // for ($i = $inicio; $i <= $fin; $i++) {
      //   if (isset($orderedHotels[$i])) {
      //     $paginationHotels[] = $orderedHotels[$i]; 
      //   }
      // }

      // $rs->serviceHotels = $paginationHotels;
      $rs->serviceHotels = $orderedHotels;
      $rs->totalPages    = $totalPages;
      $rs->currentPage   = $p['page'];
    // }
    
    return $rs;
  }

  public static function ordenarPrecio($a, $b) {
    $priceA = $a->getGroupedRooms();
    $priceB = $b->getGroupedRooms();
    $roomsA  = $priceA[0];
    $roomsB  = $priceB[0];
    $a = 0;
    $b = 0;
    foreach ($roomsA as $room) {
      $a += floatval($room->getPriceWithFee());
    }
    foreach ($roomsB as $room) {
      $b += floatval($room->getPriceWithFee());
    }
    // __log($a . ' -- ' . $b);
    // $a = floatval($priceA[0][0]->getPriceWithFee());
    // $b = floatval($priceB[0][0]->getPriceWithFee());
    if ($a == $b) return 0;
    return ($a > $b) ? 1 : -1;
  }

  public function getTopTen($orderedHotels, $destCode) {
    $query = "SELECT DISTINCT hp.destination_code, hp.zion_code , h.hotel_name 
              FROM HotelTopTen hp, Hotel h 
              WHERE hp.zion_code = h.hotel_code AND hp.destination_code = '$destCode'";
    $hotels = HotelTopTen::findByQuery($query);
    $arrHotelCodes = array();
    foreach ($hotels as $hotel) {
      $arrHotelCodes[] = trim($hotel->zion_code);
    }
    $topOrderedHotels = array();
    $con = 0;
    foreach ($orderedHotels as $value) {
      if ($value->getProvider() == BookingProvider::DOMITUR2) { continue; }
      
      // if ($value->getProvider == BookingProvider::HOTELBEDS) {
      //   $hotelCode = $value->getCode();
      // }
      $hotelCode = $value->getCode();
      if(in_array($hotelCode, $arrHotelCodes)) {
        $topOrderedHotels[] = $value;
        unset($orderedHotels[$con]);
      }
      $con++;
    }
    $topOrderedHotels = array_merge($topOrderedHotels, $orderedHotels);
    return $topOrderedHotels;
  }

  public function execHotelServiceAddRQ($p) {
    return $this->execByProvider($p);
  }

  public function execPurchaseConfirmRQ($p) {
    if(isset($p['version']) && $p['version'] == 2) {
      $hb = $this->hotelBedsBookingV2->execRequest($p);
    }
    else {
      $hb = $this->hotelBedsBooking->execRequest($p);
    }
    // $hb   = $this->hotelBedsBooking->execRequest($p);
    $dt   = $this->domiturBooking->execRequest($p);
    // $mb = $this->methabookBooking->execRequest($p);
    $mb   = NULL;
    $mb2  = $this->methabookkBooking->execRequest($p);
    $th   = $this->touricoBooking->execRequest($p);
    $dt2  = $this->domitur2Booking->execRequest($p);
    $rt   = $this->restelBooking->execRequest($p);
    $tb   = $this->tailorbedsBooking->execRequest($p);
    $nm   = $this->nemoBooking->execRequest($p);

    return $this->joinServices($hb, $dt, $mb, $mb2, $th, $dt2, $rt, $tb, $nm);
  }

  public function execHotelDetailRQ($p) {
    return $this->execByProvider($p);
  }

  public function execPurchaseCancelRQ($p) {
    return $this->execByProvider($p);
  }

  public function execPurchaseDetailRQ($p) {
    return $this->execByProvider($p);
  }

  public function execPurchaseDetailListRQ($p) {
    return $this->execByProvider($p);
  }

  private function joinServices($hb, $dt, $mb, $mb2, $th, $dt2, $rt, $tb, $nm) {
    $services = array();
    if ($hb != NULL) {
      $services = array_merge($services, $hb->getBookingServices());
    }
    if ($dt != NULL) {
      $services = array_merge($services, $dt->getBookingServices());
    }
    if ($mb != NULL) {
      $services = array_merge($services, $mb->getBookingServices());
    }
    if ($mb2 != NULL) {
      $services = array_merge($services, $mb2->getBookingServices());
    }
    if ($th != NULL) {
      $services = array_merge($services, $th->getBookingServices());
    }
    if ($dt2 != NULL) {
      $services = array_merge($services, $dt2->getBookingServices());
    }
    if ($rt != NULL) {
      $services = array_merge($services, $rt->getBookingServices());
    }
    if ($tb != NULL) {
      $services = array_merge($services, $tb->getBookingServices());
    }
    if ($nm != NULL) {
      $services = array_merge($services, $nm->getBookingServices());
    }

    if ($hb != NULL) {
      $hb->Services = $services;
      return $hb;
    }
    else {
      return new BookingPurchaseRS($services);
    }
  }


  private function joinHotels($hb, $dt, $mb, $mb2, $th, $dt2, $rt, $tb, $nm) {
    $services = array();
    if ($hb != NULL) {
      $services = array_merge($services, $hb->getHotelsAvail());
    }
    if ($dt != NULL && count($dt->getHotelsAvail())) {
      $services = array_merge($services, $dt->getHotelsAvail());
    }
    if ($mb != NULL) {
      $services = array_merge($services, $mb->getHotelsAvail());
    }
    if ($mb2 != NULL && count($mb2->getHotelsAvail())) {
      $services = array_merge($services, $mb2->getHotelsAvail());
    }
    if ($th != NULL && count($th->getHotelsAvail())) {
       $services = array_merge($services, $th->getHotelsAvail());
    }
    if ($dt2 != NULL && count($dt2->getHotelsAvail())) {
      $services = array_merge($services, $dt2->getHotelsAvail());
    }
    if ($rt != NULL && count($rt->getHotelsAvail())) {
      $services = array_merge($services, $rt->getHotelsAvail());
    }
    if($tb != NULL) {
      $services =  array_merge($services, $tb->getHotelsAvail());
    }
    if($nm != NULL) {
      $services =  array_merge($services, $nm->getHotelsAvail());
    }

    // if ($hb != NULL) {
    //   $hb->serviceHotels = $services;
    //   return $hb;
    // }
    // else {
    //   return new HotelAvailListRS($services);
    // }
    return new HotelAvailListRS($services);
  }

  public function execSearchZonesRQ($p) {
    return $this->execByProvider($p);
  }
  
  private function execByProvider($p) {
    try{
      if ($p['provider'] == BookingProvider::HOTELBEDS) {
        if(isset($p['version']) && $p['version'] == 2) {
          return $this->hotelBedsBookingV2->execRequest($p);
        }
        else {
          return $this->hotelBedsBooking->execRequest($p);
        }
      }
      if ($p['provider'] == BookingProvider::DOMITUR) {
        return $this->domiturBooking->execRequest($p);
      }
      if ($p['provider'] == BookingProvider::METHABOOK) {
        return $this->methabookBooking->execRequest($p);
      }
      if ($p['provider'] == BookingProvider::METHABOOK2) {
        return $this->methabookkBooking->execRequest($p);
      }
      if ($p['provider'] == BookingProvider::TOURICO) {
        return $this->touricoBooking->execRequest($p);
      }
      if ($p['provider'] == BookingProvider::DOMITUR2) {
        return $this->domitur2Booking->execRequest($p);
      }
      if ($p['provider'] == BookingProvider::RESTEL) {
        return $this->restelBooking->execRequest($p);
      }
      if ($p['provider'] == BookingProvider::TAILORBEDS) {
        return $this->tailorbedsBooking->execRequest($p);
      }
      if ($p['provider'] == BookingProvider::NEMO) {
        return $this->nemoBooking->execRequest($p);
      }
    }
    catch(Exception $e){
      \Log::error($e, 'execHotelDetailRQ1');
    }

  }
}

?>
