<?php

class BookingAvailableModality extends BookingSerializable {

  public $code;
  public $name;
  public $contractName;
  public $contractIncomingOfficeCode;
  public $type;
  public $typeCode;
  public $mode;
  public $modeCode;
  public $childAgeFrom;
  public $childAgeTo;
  public $contentSequence;
  public $priceList;
  public $operationDateList;

  public function getCode() {
    return $this->code;
  }

  public function getName() {
    return $this->name;
  }

  public function getContractName() {
    return $this->contractName;
  }

  public function getContractIncomingOfficeCode() {
    return $this->contractIncomingOfficeCode;
  }

  public function getType() {
    return $this->type;
  }

  public function getTypeCode() {
    return $this->typeCode;
  }

  public function getMode() {
    return $this->mode;
  }

  public function getModeCode() {
    return $this->modeCode;
  }

  public function getChildAgeFrom() {
    return $this->childAgeFrom;
  }

  public function getChildAgeTo() {
    return $this->childAgeTo;
  }

  public function getContentSequence() {
    return $this->contentSequence;
  }

  public function getPriceList() {
    return $this->priceList;
  }

  public function getOperationDateList() {
    return $this->operationDateList;
  }

}


?>
