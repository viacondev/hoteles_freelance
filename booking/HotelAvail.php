<?php

include_once('RoomAvail.php');

class HotelAvail extends BookingSerializable {

  public $provider;
  public $providerName;
  public $availToken;
  public $code;
  public $hotelZionCode;
  public $name;
  public $dateFrom;
  public $dateTo;
  public $nights;
  public $destinationCode;
  public $destinationType;
  public $mainImage;
  public $description;
  public $latitude;
  public $longitude;
  public $zone;
  public $location;
  public $category;
  public $categoryType;
  public $categoryCode;
  public $roomsAvail;
  public $currency;
  public $contractName;
  public $contractIncomingOfficeCode;
  public $contractComment;
  public $clientComment;
  public $supplier;
  public $supplierVatNumber;
  public $discountList;
  public $allPaxRequired;
  public $extraDataRequired;
  public $groupedRooms;
  public $xtraDataRequired;
  public $isWithPromotion;

  public function getProvider() {
    return $this->provider;
  }

  public function getProviderName() {
    return $this->providerName;
  }

  public function getAvailToken() {
    return $this->availToken;
  }

  public function getCode() {
    return $this->code;
  }

  public function getHotelZionCode(){
    return $this->hotelZionCode;
  }

  public function getName() {
    return $this->name;
  }

  public function getDateFrom() {
    return $this->dateFrom;
  }

  public function getDateTo() {
    return $this->dateTo;
  }

  public function getNights() {
    $from = new DateTime($this->getDateFrom());
    $to = new DateTime($this->getDateTo());
    return $to->diff($from)->days;
  }

  public function getDestinationCode() {
    return $this->destinationCode;
  }

  public function getDestinationType() {
    return $this->destinationType;
  }

  public function getMainImage() {
    return $this->mainImage;
  }

  public function getDescription() {
    return $this->description;
  }

  public function getLatitude() {
    return $this->latitude;
  }

  public function getLongitude() {
    return $this->longitude;
  }

  public function getZone() {
    return $this->zone;
  }

  public function getLocation() {
    return $this->location;
  }

  public function getCategory() {
    return $this->category;
  }

  public function getCategoryType() {
    return $this->categoryType;
  }

  public function getCategoryCode() {
    return $this->categoryCode;
  }

  public function getRoomsAvail() {
    return $this->roomsAvail;
  }

  public function getCurrency() {
    return $this->currency;
  }

  public function getContractName() {
    return strval($this->contractName);
  }

  public function getContractIncomingOfficeCode() {
    return $this->contractIncomingOfficeCode;
  }

  public function getContractComment() {
    return strval($this->contractComment);
  }

  public function getClientComment() {
    return $this->clientComment;
  }

  public function getSupplier() {
    return $this->supplier;
  }

  public function getSupplierVatNumber() {
    return $this->supplierVatNumber;
  }

  public function getDiscountList() {
    return $this->discountList;
  }

  public function getAllPaxRequired() {
    return $this->allPaxRequired;
  }

  public function getXtraDataRequired() {
    return $this->xtraDataRequired;
  }


  /**
   * Return groups (array) of rooms according to the number requested rooms
   * i.e. If 3 rooms where requested then an array will be returned where
   * every item of the array will be another array with 3 rooms.
   */
  public function getGroupedRooms() {
    return isset($this->groupedRooms) ? $this->groupedRooms : array();
  }

  public function getIsWithPromotion() {
    return $this->isWithPromotion;
  }

  /*
   * Allows to insert a new group of rooms. Useful to add rooms from other provider to this hotel.
   * The insertion is orderd DESC
   */
  public function insertGroupedRoom($group) {
    if (!isset($this->groupedRooms)) {
      $this->groupedRooms = array();
    }

    array_push($this->groupedRooms, $group);

    // Order the array. Move up until the correct place is found
    $count = count($this->groupedRooms) - 1;
    for ($i = $count; $i > 0; $i--) {
      $price = $this->calculateGroupedRoomPrice($this->groupedRooms[$i]);
      $pricePrev = $this->calculateGroupedRoomPrice($this->groupedRooms[$i - 1]);
      if ($price >= $pricePrev) {
        break;
      }
      $tmp = $this->groupedRooms[$i - 1];
      $this->groupedRooms[$i - 1] = $this->groupedRooms[$i];
      $this->groupedRooms[$i] = $tmp;
    }

    return $this->groupedRooms;
  }

  public function calculateGroupedRoomPrice($group) {
    $price = 0;
    foreach ($group as $room) {
      $price += $room->getPrice();
    }
    return $price;
  }

  public function calculateNights() {
    $from = new DateTime($this->getDateFrom());
    $to = new DateTime($this->getDateTo());
    return $to->diff($from)->days;
  }
}

?>
