<?php

class RoomAvail extends BookingSerializable {

  public $provider;
  public $hotel;
  public $providerName;
  public $hotelCode;
  public $roomCount;
  public $adultCount;
  public $childCount;
  public $roomChildAges;
  public $price;
  public $priceWithFee;
  public $sHRUI;
  public $onRequest;
  public $board;
  public $boardCode;
  public $boardType;
  public $roomType;
  public $roomTypeType;
  public $roomTypeCode;
  public $roomTypeCharacteristic;
  public $cancellationPolicies;
  public $guestList;

  public function getProvider() {
    return $this->provider;
  }

  public function getHotel() {
    return $this->hotel;
  }

  public function getProviderName() {
    return $this->providerName;
  }

  public function getHotelCode() {
    return $this->hotelCode;
  }

  public function getRoomCount() {
    return $this->roomCount;
  }

  public function getAdultCount() {
    return $this->adultCount;
  }

  public function getChildCount() {
    return $this->roomChildAges;
  }

  public function getChildAges() {
    return $this->childAges;
  }

  public function getPrice() {
    return $this->price;
  }

  public function getPriceWithFee() {
    return $this->priceWithFee;
  }

  public function getSHRUI() {
    return $this->sHRUI;
  }

  public function getOnRequest() {
    return $this->onRequest;
  }

  public function getBoard() {
    return $this->board;
  }

  public function getBoardCode() {
    return $this->boardCode;
  }

  public function getBoardType() {
    return $this->boardType;
  }

  public function getRoomType() {
    return $this->roomType;
  }

  public function getRoomTypeType() {
    return $this->roomTypeType;
  }

  public function getRoomTypeCode() {
    return $this->roomTypeCode;
  }

  public function getRoomTypeCharacteristic() {
    return $this->roomTypeCharacteristic;
  }

  public function getCancellationPolicies() {
    return $this->cancellationPolicies;
  }

  public function getGuestList() {
    return $this->guestList;
  }

}

?>
