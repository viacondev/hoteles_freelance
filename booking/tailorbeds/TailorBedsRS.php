<?php
namespace TailorBeds;

class TailorBedsRS {

  private $response;

  public function __construct($xml) {
    $xml = stripslashes($xml);
    $xml = str_replace(array('<soap:','<s:'), "<", $xml);
    $xml = str_replace(array('</soap:','</s:'), "</", $xml);
    $xml = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $xml);

    $this->response = simplexml_load_string($xml);
    // if($solicitud == 'XML_RS') {
    //   $descrip = $solicitud . $type;
    //   \Log::__log('INFO', $descrip, $xml);
    // }
    //\Log::debug($xml, 'XML RS');
    // __logarr($this->response);
  }

  public function getResponse() {
    return $this->response;
  }


}

?>
