<?php
namespace TailorBeds;

/**
 * Class used for confirmed Services
 */
class ServiceRS extends \BookingService {

  private $elements;
  private $item;
  private $errors;
  public $totalAmount;

  public function __construct($rs, $scItem, $errors = array()) {
    $this->elements     = $rs->Details;
    $this->errors       = $errors;
    $this->totalAmount  = $this->elements->Products->Hotels->Hotel->Rate->RatePrices->RatePrice;
    $this->item         = json_decode(json_encode($scItem), FALSE);
  }

  public function getLocalizer() {
    $localizer = $this->elements->BookingReference;
    return strval($localizer);
  }

  public function getSPUI() {
    return '';
  }

  public function getServiceType() {
    return 'ServiceHotel';
  }

  public function getServiceStatus() {
    return '';
  }

  public function getDirectPayment() {
    return '';
  }

  public function getDateFrom() {
    $res = $this->elements->Products->Hotels->Hotel->CheckIn;
    $checkin = explode('-', $res[0]);
    $checkin = $checkin[0] . $checkin[1] . $checkin[2];
    return $checkin;
    // return ''
  }

  public function getDateTo() {
    $res = $this->elements->Products->Hotels->Hotel->CheckOut;
    $checkOut = explode('-', $res[0]);
    $checkOut = $checkOut[0] . $checkOut[1] . $checkOut[2];
    return $checkOut;
  }

  public function getCurrency() {
    // $currency = $this->elements->ResGroup->attributes();
    return strval('USD');
  }

  public function getTotalAmount() {
    // $totalAmount = $this->elements->ResGroup->attributes();
    return floatval($this->totalAmount);
  }

  public function getAdditionalCosts() {
    return array();
  }

  public function getServiceInfo() {
    $accommodations = $this->elements;
    $totalAmount    = $this->getTotalAmount();
    return new ConfirmHotelAvail($accommodations, $this->item, $totalAmount);
  }

  public function getSupplementList() {
    return array();
  }

  public function getStatus() {
    $status = strval($this->elements->Details->BookingState);

    if ($status == 'NMO.GBL.BST.CNF') {
      return \ServiceStatus::CONFIRMED;
    }
    return \ServiceStatus::CANCELLED;
  }

  public function getErrors() {
    return $this->errors;
  }

}

?>
