<?php
namespace TailorBeds;

include_once('SCartRoomAvailRS_tb.php');

class SCartHotelAvailRS extends \HotelAvail {

  private $data;
  private $rules;
  public  $hotelDetail;
  public  $hotelDiscount;
  public function __construct($data, $totalAmount) {
    $this->data = $data;
     // Build Booking Rules
    $from = $this->getDateFrom();
    $to   = $this->getDateTo();
    $from = substr($from, 0, 4) . '-' . substr($from, 4, 2) . '-' . substr($from, 6, 4);
    $to   = substr($to, 0, 4) . '-' . substr($to, 4, 2) . '-' . substr($to, 6, 4);

    $tripProductId = $this->data->rooms[0]->SHRUI;

    $req            = new TailorBedsRQ();
    $validationRQ   = new AvailabilityValidationRQ($tripProductId);
    $valitadionRS   = $req->execRequest($validationRQ, 'XML_RQ', 'BB_TB_RQ_VALIDATION'); 
    \Log::info($valitadionRS, 'BB_TB_RS_VALIDATION');
    $rules  = new HotelBookingRuleRQ($tripProductId);
    $rq     = new TailorBedsRQ();
    $xml    = $rq->execRequest($rules, 'XML_RQ', 'BB_TB_RQ_POLICY');
    
    \Log::info($xml, 'BB_TB_RS_POLICY');
    $rules            = new TailorBedsRS($xml);
    $rules            = $rules->getResponse();
    $this->rules      = $rules->Details->Products->Hotels->Hotel->Rate;
    $this->roomsAvail = array();
    $indexChild = 0;
    foreach ($this->data->rooms as $room) {
      $adultCount = $room->adultCount;
      $this->roomsAvail[] = new SCartRoomAvailRS($room, $this, $this->getDateFrom(), $this->getDateTo(), $this->data->childAges, $totalAmount, $this->rules, $adultCount, $indexChild, $this->data->factorFee);
      $indexChild += $room->childCount;
    }

  }

  public function getProvider() {
    return \BookingProvider::TAILORBEDS;
  }

  public function getProviderName() {
    return 'TAILORBEDS';
  }

  public function getAvailToken() {
    return $this->data->availToken;
  }

  public function getCode() {
    return $this->data->hotelCode;
  }

  public function getName() {
    return $this->data->hotelName;
  }

  public function getDateFrom() {
    return $this->data->dateFrom;
  }

  public function getDateTo() {
    return $this->data->dateTo;
  }

  public function getDestinationCode() {
    return $this->data->destCode;
  }

  public function getDestinationType() {
    return '';
  }

  public function getMainImage() {
    return '';
  }

  public function getDescription() {
    return '';
  }

  public function getLatitude() {
    return '';
  }

  public function getLongitude() {
    return '';
  }

  public function getZone() {
    return '';
  }

  public function getLocation() {
    return '';
  }

  public function getCategory() {
    return '';
  }

  public function getCategoryType() {
    return '';
  }

  public function getCategoryCode() {
    return strval($this->data->category);
  }

  public function getRoomsAvail() {
    return $this->roomsAvail;
  }

  public function getGroupedRooms() {
    return $this->roomsAvail;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getContractName() {
    return '';
  }

  public function getContractIncomingOfficeCode() {
    return '';
  }

  public function getContractComment() {
    return "";
  }

  public function getClientComment() {
    return '';
  }

  public function getSupplier() {
    return '';
  }

  public function getSupplierVatNumber() {
    return '';
  }

  public function getDiscountList() {
    return array();
  }

  public function getAllPaxRequired() {
    return true;
  }

  public function getXtraDataRequired() {
    return false;
  }

  public function isDataRequired() {
   return false;
  }

  public function calculateNights() {
    $from = new \DateTime($this->getDateFrom());
    $to = new \DateTime($this->getDateTo());
    return $to->diff($from)->days;
  }
}

?>
