<?php
namespace TailorBeds;

class ReadServiceRQ {

  private $localizer;
  public function __construct($localizer) {
    $this->localizer = $localizer;
  }

  public function getType() {
    return 'hotelService';
  }

  function getMethodName() {
    return 'booking/detail';
  }

  function getParams() {
    $xml = '
        <BookingQueryRQ>
          <GeneralParameters>
            <PreferedLanguage LanguageCode="es"/>
            <PreferedCurrency CurrencyCode="USD"/>
          </GeneralParameters>
          <Bookings>
            <Booking BookingReference="' . $this->localizer . '"/>
          </Bookings>
        </BookingQueryRQ>';
    return $xml;
  }

}
?>
