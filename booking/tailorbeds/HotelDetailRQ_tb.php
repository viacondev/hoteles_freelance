<?php
namespace TailorBeds;

class HotelDetailRQ {

  private $code;

  public function __construct($code) {
    $this->code = $code;
  }

  public function getType() {
    return 'hotelService';
  }

  public function getMethodName() {
    return 'catalog/product/detail';
  }

  public function getParams() {
    $xml = '
          <AdditionalInfoQueryRQ>
            <GeneralParameters>
              <PreferedLanguage LanguageCode="es"/>
              <PreferedCurrency CurrencyCode="USD"/>
            </GeneralParameters>
            <Products>
              <Hotels>
                <Hotel HotelDetailId="' . $this->code . '"/>
              </Hotels>
            </Products>
          </AdditionalInfoQueryRQ>';

    return $xml;
  }
}
?>
