<?php
namespace TailorBeds;

include_once('RoomAvail_tb.php');

class HotelAvailRS extends \HotelAvail {

  private $element;
  private $destCode;
  private $rooms;
  public $dateFrom;
  public $dateTo;
  public $roomsAvail;
  public $currency;

  public function __construct($element, $destCode, $dateFrom , $dateTo, $roomCount, $rooms_numbers, $regimen, $factorFee) {
    $this->dateFrom   = $dateFrom;
    $this->dateTo     = $dateTo;
    $this->element    = $element;
    $this->roomsAvail = array();
    $this->destCode   = $destCode;
    
    $roomRates      = $this->element->Rates->Rate;
    $attrCurrency   = $roomRates[0]->RatePrices->RatePrice->attributes();
    $this->currency = strval($attrCurrency['Currency']);
    foreach ($roomRates as $roomRate) {
      $rooms = $roomRate->Rooms->Room;
      $groupedRooms     = array();
      $index = 0;
      foreach ($rooms as $room) {
        $attrsPrice = $roomRate->RatePrices->RatePrice->attributes();
        $price      = floatval($roomRate->RatePrices->RatePrice) / $roomCount;
        $attrsHotel = $element->attributes();
        $hotelCode  = strval($attrsHotel['HotelDetailId']);
        $board      = strval($roomRate->Board);
        $attrsBoard = $roomRate->Board->attributes();
        $boardCode  = strval($attrsBoard['Code']);
        $roomType   = strval($room);
        $attrRoom   = $room->attributes();
        $roomTypeCode = strval($attrRoom['Type']);
        $attrsSHRUI = $roomRate->attributes();
        $SHRUI      = strval($attrsSHRUI['TripProductID']);
        $ocupancy   = strval($attrRoom['Occupancy']);
        $arrOcupancy = explode('/', $ocupancy);
        $adultCount = $arrOcupancy[0];
        $childCount = $arrOcupancy[1] != 0 ? $arrOcupancy[1] : $arrOcupancy[2];
        $childAges  = $rooms_numbers[$index];
        $params = array('hotelCode' => $hotelCode, 'board'    => $board,
                        'boardCode' => $boardCode, 'roomType' => $roomType,
                        'SHRUI'     => $SHRUI, 'roomTypeCode' => $roomTypeCode,
                        'adultCount'=> $adultCount, 'childCount'=>$childCount,
                        'childAges' => $childAges);
        $roomAvail  = new RoomAvail($room, $price, $params, $this, $factorFee);
        
        if ($regimen != '') {
          if (in_array($roomAvail->getBoardCode(), $regimen)) {
            $this->roomsAvail[] = $roomAvail;
            $groupedRooms[]     = $roomAvail;
          }
        }
        else {
          $this->roomsAvail[] = $roomAvail;
          $groupedRooms[]     = $roomAvail;
        }
      }
      if (count($groupedRooms) > 0) {
        $this->insertGroupedRoom($groupedRooms);
      }
    }

  }

  public function getProvider() {
    return \BookingProvider::TAILORBEDS;
  }

  public function getProviderName() {
    return 'TAILORBEDS';
  }

  public function getAvailToken() {
    return '';
  }

  public function getCode() {
    $attrs = $this->element->attributes();
    return strval($attrs['Code']).'_'.strval($attrs['HotelDetailId']);
  }

  public function getHotelZionCode() {
    $attrs = $this->element->attributes();
    return strval($attrs['Code']).'_'.strval($attrs['HotelDetailId']);
  }

  public function getName() {
    $attrs  = $this->element->attributes();
    $name   = strval($attrs['Name']);
    return ucwords(strtolower($name));
  }

  public function getDateFrom() {
    return $this->dateFrom;
  }

  public function getDateTo() {
    return $this->dateTo;
  }

  public function getDestinationCode() {
    return $this->destCode;
  }

  public function getDestinationType() {
    return '';
  }

  public function getMainImage() {
    $res = $this->element->Images->Image;
    return strval($res);
    // return 'assets/images/no-photo.jpg';
  }

  public function getDescription() {
    $res = $this->element->attributes();
    if(isset($res['desc'])) {
      return strval($res['desc']);
    }
    return 'M&aacute;s informaci&oacute;n de los hoteles dentro de poco tiempo...';
  }

  public function getLatitude() {
    $res = $this->element;
    return strval($res['Latitude']);
  }

  public function getLongitude() {
    $res = $this->element;
    return strval($res['Longitude']);
  }

  public function getZone() {
    return strval('');
  }

  public function getLocation() {
    return "";
  }

  public function getCategory() {
    $star = array('1.5' =>  'H1_5',
                  '2.5' =>  'H2_5',
                  '3.5' =>  'H3_5',
                  '3.7' =>  'ALBER',
                  '4.5' =>  'H4_5',
                  '5.5' =>  'H5_5');
    $category = $this->element->Rating->Value;
    // if (strpos($category, '.')) {
    //   return $star[$category];
    // }
    return strval($category['starsLevel']);
  }

  public function getCategoryType() {
    return '';
  }

  public function getCategoryCode() {
    $star = array('1.5' => 'H1_5',
                  '2.5' => 'H2_5',
                  '3.5' => 'H3_5',
                  '3.7' => 'ALBER',
                  '4.5' => 'H4_5',
                  '5.5' => 'H5_5');
    $category = intval($this->element->Rating->Value);

    return $category . 'EST';
  }

  public function getRoomsAvail() {
    return $this->roomsAvail;
  }

  public function getCurrency() {
    return $this->currency;
  }

  public function getContractName() {
    return '';
  }

  public function getContractIncomingOfficeCode() {
    return '';
  }

  public function getContractComment() {
    return '';
  }

  public function getClientComment() {
    return '';
  }

  public function getSupplier() {
    return '';
  }

  public function getSupplierVatNumber() {
    return '';
  }

  public function getDiscountList() {
    return array();
  }

  public function areAllPaxRequired() {
    return false;
  }

  public function isExtraDataRequired() {
    return false;
  }

  public function isDataRequired() {
    return false;
  }

  public function getIsWithPromotion() {
    $ispromotion = "negative";
    return $ispromotion;
  }
}

?>
