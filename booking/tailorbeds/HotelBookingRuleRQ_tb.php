<?php
namespace TailorBeds;

class HotelBookingRuleRQ {

  private $tripProductId;

  public function __construct($tripProductId = "") {
    $this->tripProductId = $tripProductId;
  }

  public function getType() {
    return 'hotelService';
  }

  public function getMethodName() {
    return 'booking/cancellation/fees';
  }

  public function getParams() {

    $xml = '<?xml version="1.0" encoding="UTF-8"?>
            <CancellationFeesQueryRQ>
              <GeneralParameters>
                <PreferedLanguage LanguageCode="es"/>
                <PreferedCurrency CurrencyCode="ARS"/>
              </GeneralParameters>
              <Products>
                <Hotels>
                  <Hotel TripProductID="' . $this->tripProductId . '" ShowHotelInformation="true"/>
                </Hotels>
              </Products>
            </CancellationFeesQueryRQ>';

    return $xml;
  }
}
?>
