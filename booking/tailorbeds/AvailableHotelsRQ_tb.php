<?php
namespace TailorBeds;

class AvailableHotelsRQ {

  public $destCode;
  public $checkin;
  public $checkout;
  public $rooms;
  public $hotelCode;

  public function __construct($destCode, $checkin, $checkout, $rooms, $hotelCode) {
    $this->destCode = $destCode;
    $this->checkin  = $checkin;
    $this->checkout = $checkout;
    $this->rooms    = $rooms;
    $this->hotelCode = $hotelCode;
  }

  public function getType() {
    return 'hotelService';
  }

  public function getMethodName() {
    return 'catalog/products/search?hideNewInfo=false';
  }

  public function getParams() {
    $cadRooms = '';
    $paxCad     = '';

    $arrRooms = array(1 => 'NMO.HTL.RMT.SGL',
                      2 => 'NMO.HTL.RMT.DBL',
                      3 => 'NMO.HTL.RMT.TPL',
                      4 => 'NMO.HTL.RMT.QUA');
    $arrChilds = array(1 => 'NMO.GBL.AGT.INF',
                       2 => 'NMO.GBL.AGT.CHD');
    $seqNumber = 1;
    foreach ($this->rooms as $room) {
      $adultCount = $room->getAdultCount();
      $childCount = $room->getChildCount();
      $roomCod    = $arrRooms[$adultCount];
      
      $cadRooms   .= '<Room RoomType="' . $roomCod . '" RoomSequence="' . $seqNumber . '"/>';
      for($i = 0; $i < $adultCount; $i++) {
        $paxCad .= '<Passenger AgeType="NMO.GBL.AGT.ADT" RoomSequence="' . $seqNumber . '"/>'; 
      }
      $arrChildAges = $room->getChildAges();
      for ($p = 0; $p < $childCount; $p++) {
        $age = $arrChildAges[$p];
        $cadChild = $arrChilds[1];
        if ($age > 1) {
          $cadChild = $arrChilds[2];
        }
        $paxCad .= '<Passenger AgeType="' . $cadChild . '" RoomSequence="' . $seqNumber . '">
                      <Age>' . $age . '</Age>
                    </Passenger>';
      }
      $seqNumber++;
    }
    $hotelList = '';
    if ($this->hotelCode != '') {
      $hotelList = explode('_', $this->hotelCode);
      $hotelList = '<HotelCodeList>' . $hotelList[0] . '</HotelCodeList>';
    }
    $xml = '
      <AvailabilityQueryRQ TransactionId="234sdf_fsdwer45" TransactionMode="Synchronous">
        <GeneralParameters>
          <PreferedLanguage LanguageCode="es"/>
          <PreferedCurrency CurrencyCode="USD"/>
        </GeneralParameters>
        <Trips>
          <Trip>
            <Destination>' . $this->destCode . '</Destination>
            <ProductsParameters>
              <HotelsParameters>
                <Criterion>
                  <Rooms>
                    ' . $cadRooms . '
                  </Rooms>
                  <CheckIn>' . $this->checkin . '</CheckIn>
                  <CheckOut>' . $this->checkout . '</CheckOut>
                  <Availability>CNF</Availability>
                  ' . $hotelList . '
                </Criterion>
              </HotelsParameters>
            </ProductsParameters>
          </Trip>
        </Trips>
        <Passengers>
          ' . $paxCad . '
        </Passengers>
        <RequestSet>
          <FirstItem>1</FirstItem>
          <ItemsPerPage>100</ItemsPerPage>
        </RequestSet>
      </AvailabilityQueryRQ>';

    return $xml;
  }
}

?>
