<?php
namespace TailorBeds;

class HotelDetailRS extends \BookingHotelDetail {

  public $elements;
  private $hotel;
  private $desc;
  private $images;
  private $contact;
  public $hotelCode;
  public $servicesFacilities;
  public $roomFacilities;
  public $cateringFacilities;

  public function __construct($rs) {
    $this->elements = $rs->Details->Products->Hotels->Hotel;
    $this->build();
  }

  private function build() {

    // Facilities
    $this->roomFacilities = array();
    $this->servicesFacilities = array();
    $this->cateringFacilities = array();

    if (isset($this->elements->Amenities)) {
      foreach ($this->elements->Amenities->Amenity as $value) {
        $this->roomFacilities[] = strval($value->Description);
      }
    }

    // foreach ($this->hotel->Services->Service as $service) {
    //   $attrs = $service->attributes();
    //   $txt = strval($service->DescriptiveText);
    //   if ($attrs['Sort'] == 'Hotel') {
    //     $this->servicesFacilities[] = $txt;
    //   }
    //   else if ($attrs['Sort'] == 'Room') {
    //     $this->roomFacilities[] = $txt;
    //   }
    //   else if ($attrs['Sort'] == 'Catering') {
    //     $this->cateringFacilities[] = $txt;
    //   }
    // }
  }

  public function getCode() {
    // $attrs = $this->elements->attributes();
    // $hotelCode = strval($attrs['HotelDetailId']); 
    return $this->hotelCode;
  }

  public function getName() {
    $attrs = $this->elements->HotelName;
    return strval($attrs);
  }

  public function getDescription() {
    return strval($this->elements->HotelDescriptions->HotelDescription);
  }

  public function getImageList() {
    $images = $this->elements->ImageLinks->ImageLink;
    $this->images = array();
    foreach ($images as $img) {
      $this->images[] = strval($img->ThumbnailURL);
    }
    return $this->images;
  }

  public function getAddressStreetName() {
    $addres = $this->elements->Addresses->Address->AddressLines->AddressLine;
    $adr = '';
    foreach ($addres as $value) {
      $adr .= strval($value) . ' - ';
    }
    return strval($adr);
  }
  public function getAddressNumber() {
    $addres = $this->elements->Addresses->Address->AddressLines;
    $adr = '';
    foreach ($addres as $value) {
      $adr .= strval($value) . ' * ';
    }
    return strval($adr);
  }

  public function getAddressPostalCode() {
    return '';
  }

  public function getEmailList() {
    $mails = array();
    return $mails;
  }

  public function getPhoneList() {
    $phones = array();
    $attrs = $this->elements->Addresses->Address->TelephoneNumbers->TelephoneNumber->TelephoneNumberValue;
    $phones[] = strval($attrs);
    return $phones;
  }

  public function getFaxList() {
    $faxList = array();
    $attrs = $this->elements->attributes();
    $faxList[] = strval("");
    return $faxList;
  }

  public function getWebList() {
    return array();
  }

  public function getLocation() {
    return '';
  }

  public function getLatitude() {
    $attrs = $this->elements->Position->Latitude;
    return strval($attrs);
  }

  public function getLongitude() {
    $attrs = $this->elements->Position->Longitude;
    return strval($attrs);
  }

  public function getDestinationCode() {
    // TODO: return dest code
    return '';
  }

  public function getDestinationName() {
    // Commented because direction already inludes de city & country
    // return strval($this->contact->Addresses->Address->CityName);
    return '';
  }

  public function getDestinationZone() {
    return '';
  }

  public function getCategory() {
    return "";
  }

  public function getCategoryCode() {
    $star = array('1.5' => 'H1_5',
                  '2.5' => 'H2_5',
                  '3.5' => 'H3_5',
                  '4.5' => 'H4_5',
                  '5.5' => 'H5_5',
                  '1.0'   =>  'HS',
                  '2.0'   =>  'HS2',
                  '3.0'   =>  'HS3',
                  '4.0'   =>  'HS4',
                  '5.0'   =>  'HS5');
    $category = $this->elements->HotelRating->RatingValue;
    $starval = strval($category);
    if (isset($star[$starval])) {
      return $star[$starval];
    }
    return '0';
  }

  public function getBuildingFacilities() {
    // return $this->buildingFacilities;
    return array();
  }

  public function getHotelTypeFacilities() {
    // return $this->hotelTypeFacility;
    return array();
  }

  public function getCredCardsFacilities() {
    // return $this->credCardsFacilities;
    return array();
  }

  public function getRoomFacilities() {
    return $this->roomFacilities;
  }

  public function getServicesFacilities() {
    return $this->servicesFacilities;
  }

  public function getCateringFacilities() {
    return $this->cateringFacilities;
  }

  public function getBusinessFacilities() {
    // return $this->businessFacilities;
    return array();
  }

  public function getHealthBeautyFacilities() {
    // return $this->healthBeautyFacilities;
    return array();
  }

  public function getEntertainmentFacilities() {
    // return $this->entertainmentFacilities;
    return array();
  }

  public function getDistancesFacilities() {
    $arr = array();

    return $arr;
  }

  public function getHighLightFacilities() {
    // return $this->highLightFacilities;
    return array();
  }

}

?>
