<?php
namespace TailorBeds;
// ini_set('display_errors', 1);
class TailorBedsRQ {

  private $location;
  private $client;
  private $user;
  private $pass;
  private $wsdl;
  public function __construct() {
    // ini_set('soap.wsdl_cache_enabled', '0');
    // ini_set('soap.wsdl_cache_ttl', '0');

    // $this->location  = \THConfig::$location;
    // $this->user = \THConfig::$agencyName;
    // $this->pass = \THConfig::$agencyCode;
    // $this->wsdl = \THConfig::$wsdl;

  }

  public function getRequestXML($RQElement) {

    //$res = $RQElement->getParams();
    //$xml = str_replace('__POS__', $autentication, $res);
    $xml =  $RQElement->getParams();
    return $xml;
  }

  public function execRequest($RQElement, $solicitud = "", $type = "") {
    // $request = $this->getRequestXML($RQElement->getParams());
    $url = \TBConfig::$url . $RQElement->getMethodName();

    $header = array();
    // $header[] = 'Cache-Control: no-cache';
    // $header[] = 'Content-Type: text/xml';
    $header[] = 'Accept: application/xml';
    $header[] = 'Accept-Encoding: gzip,deflate';
    $header[] = 'Cache-Control: no-cache';
    $header[] = 'Connection: Keep-Alive';
    $header[] = 'Content-Type: application/xml';
    $header[] = 'User-Agent: Apache-HttpClient/4.1.1 (java 1.5)';
    $header[] = 'X-PS-AUTHTOKEN: b583ecb1bb90b3c6de0f7089eeac15fc';
    //$header[] = 'SOAPAction: ' . $wsdl . $RQElement->getMethodName();

    if (isset(\TBConfig::$DirectXMLAccess) && \TBConfig::$DirectXMLAccess) { //falta incluir el archivo rest para que funcione
      $ch = curl_init();
      curl_setopt_array($ch, array(
                                CURLOPT_URL => $url,
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => "",
                                /*CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 30,*/
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "POST",
                                CURLOPT_POSTFIELDS => $this->getRequestXML($RQElement),
                                CURLOPT_HTTPHEADER => $header,
                                CURLOPT_USERAGENT => $_SERVER['HTTP_USER_AGENT'])
                      );
      $xml = curl_exec($ch);
      curl_close($ch);
    }
    else {
      $rq = 'http://test.barrybolivia.com/booking/tourico/RemoteRequestWrapper_th.php';
      $rq .= '?url=' . urlencode($url);
      $rq .= '&header=' . urlencode(json_encode($header));
      $rq .= '&request=' . urlencode($this->getRequestXML($RQElement));

      $context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));
      $xml = file_get_contents($rq, false, $context);

    }
    if($solicitud == 'XML_RQ') {
      $descrip = $type;
      \Log::info($this->getRequestXML($RQElement), $descrip);
    }
    // __logtxt($this->getRequestXML($RQElement));
    // __logtxt($xml); // RS
    return $xml;
  }
  public function allOptions($RQElement) {
    $url = \TBConfig::$url . $RQElement->getMethodName();

    $header = array();
    // $header[] = 'Cache-Control: no-cache';
    // $header[] = 'Content-Type: text/xml';
    $header[] = 'Accept: application/xml';
    $header[] = 'Accept-Encoding: gzip,deflate';
    $header[] = 'Cache-Control: no-cache';
    $header[] = 'Connection: Keep-Alive';
    $header[] = 'Content-Type: application/xml';
    $header[] = 'User-Agent: Apache-HttpClient/4.1.1 (java 1.5)';
    $header[] = 'X-PS-AUTHTOKEN: b583ecb1bb90b3c6de0f7089eeac15fc';
    if (isset(\TBConfig::$DirectXMLAccess) && \TBConfig::$DirectXMLAccess) {
      $options  = array(CURLOPT_URL => $url,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => $this->getRequestXML($RQElement),
                        CURLOPT_HTTPHEADER => $header,
                        CURLOPT_USERAGENT => $_SERVER['HTTP_USER_AGENT']);
    }
    else {
      $rq = 'http://test.barrybolivia.com/booking/tourico/RemoteRequestWrapper_th.php';
      $rq .= '?url=' . urlencode($url);
      $rq .= '&header=' . urlencode(json_encode($header));
      $rq .= '&request=' . urlencode($this->getRequestXML($RQElement));
      $options  = array(CURLOPT_AUTOREFERER => true, 
                        CURLOPT_HEADER => 0, 
                        CURLOPT_RETURNTRANSFER => 1, 
                        CURLOPT_URL             => $rq);
    }
    return $options;
  }

}

?>
