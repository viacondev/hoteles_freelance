<?php
namespace TailorBeds;

class SCartRoomAvailRS extends \RoomAvail {

  private $data;
  public $hotel;
  private $date_in;
  private $date_out;
  public $childAges;
  private $rules;
  private $totalAmount;
  private $indexChild;
  private $factorFee;

  public function __construct($data, $hotel, $date_in, $date_out, $childAges, $totalAmount, $rules, $adultCount, $indexChild, $factorFee = 0.79) {
    $this->data        = $data;
    $this->hotel       = array();
    $this->date_in     = $date_in;
    $this->date_out    = $date_out;
    $this->childAges   = $childAges;
    $this->rules       = $rules;
    $this->factorFee   = $factorFee;
    $this->totalAmount = $totalAmount;
    $this->indexChild  = $indexChild;
  }

  public function getProvider() {
    return \BookingProvider::TAILORBEDS;
  }

  public function getHotel() {
    return $this->hotel;
  }

  public function getProviderName() {
    return 'TAILORBEDS';
  }

  public function getHotelCode() {
    return '';
  }

  public function getRoomCount() {
    return $this->data->roomCount;
  }

  public function getAdultCount() {
    return $this->data->adultCount;
  }

  public function getChildCount() {
    return $this->data->childCount;
  }

  public function getPrice() {
    return $this->data->price;
  }

  public function getPriceWithFee() {
    return round($this->data->price / $this->factorFee, 2);
  }

  public function getSHRUI() {
    return $this->data->SHRUI;
  }

  public function getOnRequest() {
    return '';
  }

  public function getBoard() {
    return $this->data->board;
  }

  public function getBoardCode() {
    return $this->data->boardCode;
  }

  public function getBoardType() {
    return $this->data->boardType;
  }

  public function getRoomType() {
    return $this->data->roomType;
  }

  public function getRoomTypeType() {
    return $this->data->roomTypeType;
  }

  public function getRoomTypeCode() {
    return $this->data->roomTypeCode;
  }

  public function getRoomTypeCharacteristic() {
    return $this->data->roomTypeCharacteristic;
  }

  public function getCancellationPolicies() {
    $policies     = array();
    $totalAmount  = $this->totalAmount;
    $amount       = 0;
    $rules        = $this->rules->ChargeConditions->ChargeCondition;
    foreach ($rules as $rule) {
      $attrRule = $rule->ChargeDescription->attributes();
      $ruleType = strval($attrRule['ChargeType']);
      $amount   = 0;
      $prices   = $rule->RatePrices->RatePrice;
      foreach ($prices as $price) {
        $amount += floatval($price);
      }
      if ($amount == 0) {
        //se encontro casos en que el <rateprice> viene con un precio 0
        // y para no mostrar ese precio se coloco el precio total
        $amount = $this->data->price;
      }
      if ($ruleType == 'NMO.HTL.CHT.NSW') {
        if (intval($rule->DateRangeWithDays->DateTo) == 0 && intval($rule->DateRangeWithDays->DateFrom) == 0) {
          $dateFrom = new \DateTime("now");
        }
        else {
          $dateFrom = new \DateTime($this->date_in);  
        }
      }
      else if ($ruleType == 'NMO.HTL.CHT.CAN') {
        if (isset($rule->DateRangeWithDays)) {
          if (!isset($rule->DateRangeWithDays->DateTo) && $rule->DateRangeWithDays->DateFrom == 0) {
            $dateFrom = new \DateTime("now");
          }
          else {
            $daysFrom = $rule->DateRangeWithDays->DateFrom;
            $daysTo   = $rule->DateRangeWithDays->DateTo;
            $days     = $daysFrom + $daysTo;
            $date     = new \DateTime($this->date_in);
            $dateFrom = date_sub($date, new \DateInterval('P' . $days .'D'));
          }
        }
        if (isset($rule->DateRangeWithDateType)) {
          $arrDate  = explode('T', $rule->DateRangeWithDateType->DateFrom);
          $dateFrom = new \DateTime($arrDate[0]);
          // $dateFrom = new \DateTime("now");
        }
      }
      
      $cancellation = new \BookingCancellationPolicy();
      $cancellation->build($amount, $dateFrom, '00:00', true,'' , $this->factorFee);
      $policies[]   = $cancellation;
    }
    return $policies;
  }

  public function getGuestList() {
    $customer_counter = 0;

    for ($i = 0; $i < $this->getAdultCount(); $i++) {
      $customer_counter++;
      $guests[] = new \BookingCustomer('AD', $customer_counter, '30', '', '');
    }
    $indexChild = $this->indexChild;
    $childIndex = 0;
    for ($i = 0; $i < $this->getChildCount(); $i++) {
      $customer_counter++;
      $guests[] = new \BookingCustomer('CH', $customer_counter, $this->data->childAges[$childIndex], '', '');
      $indexChild++;
      $childIndex++;
    }

    return $guests;
  }

  public function buildObjecArray() {
    return array();
  }
}

?>
