<?php
namespace TailorBeds;

class ConfirmRoomAvail extends \RoomAvail {

  public $data;
  public $hotel;
  public $date_in;
  public $date_out;
  public $childAges;
  public $rules;
  public $totalPrice;
  public $item;
  private $factorFee;
  private $fechaTope;
  
  public function __construct($data, $hotel, $totalPrice, $board, $item, $checkin, $rules, $fechaTope) {
    $this->data   = $data;
    $this->hotel  = $hotel;
    $this->rules  = $rules;
    $this->board  = $board;
    $this->date_in = $checkin;
    $this->totalPrice = $totalPrice;
    $this->item       = $item;
    $this->fechaTope  = $fechaTope;
  }

  public function getProvider() {
    return \BookingProvider::TAILORBEDS;
  }

  public function getHotel() {
    return $this->hotel;
  }

  public function getRoomCount() {
    return 1;
  }

  public function getAdultCount() {
    return $this->data->Occupancy->AdultsCount;
  }

  public function getChildCount() {
    $child = intval($this->data->Occupancy->ChildrenCount);
    $inf   = intval($this->data->Occupancy->InfantCount);
    return $child + $inf;
  }

  public function getPrice() {
    return $this->totalPrice;
  }

  public function getSHRUI() {
    return '';
  }

  public function getOnRequest() {
    return '';
  }

  public function getBoard() {
    return strval($this->board);
  }

  public function getBoardCode() {
    return '';
  }

  public function getBoardType() {
    return '';
  }

  public function getRoomType() {
    $roomType = $this->data->RoomDescription;
    return strval($roomType);
  }

  public function getRoomTypeType() {
    return '';
  }

  public function getRoomTypeCode() {
    return '';
  }

  public function getRoomTypeCharacteristic() {
    return '';
  }

  public function getCancellationPolicies() {
    $policies = array();
    $amount   = 0;
    $rules    = $this->rules->ChargeCondition;
    $dateFrom = '';
    foreach ($rules as $rule) {
      $attrRule = $rule->ChargeDescription->attributes();
      $ruleType = strval($attrRule['ChargeType']);
      $amount   = 0;
      $prices   = $rule->RatePrices->RatePrice;
      foreach ($prices as $price) {
        $amount += floatval($price);
      }
      if ($ruleType == 'NMO.HTL.CHT.NSW') {
        $dateFrom = new \DateTime($this->date_in);
      }
      else if ($ruleType == 'NMO.HTL.CHT.CAN') {
        if (isset($rule->DateRangeWithDays)) {
          $daysFrom = $rule->DateRangeWithDays->DateFrom;
          $daysTo   = $rule->DateRangeWithDays->DateTo;
          $days     = $daysFrom + $daysTo;
          $date     = new \DateTime($this->date_in);
          $dateFrom = date_sub($date, new \DateInterval('P' . $days .'D'));
        }else if (isset($rule->DateRangeWithDateType)) {
          $arrDate  = explode('T', $rule->DateRangeWithDateType->DateFrom);
          $dateFrom = new \DateTime($arrDate[0]);
          // $dateFrom = new \DateTime("now");
        }else if ($dateFrom != '' && isset($this->fechaTope->Deadline)) {
          $dateVal  = $this->fechaTope->Deadline->Date;
          $arrDate  = explode('T', $dateVal);
          $dateFrom = new \DateTime($arrDate[0]);
        }
      }
      else if ($ruleType == 'NMO.HTL.CHT.AMD') {
        $dateFrom = new \DateTime("now");
      }

      $cancellation = new \BookingCancellationPolicy();
      $cancellation->build($amount, $dateFrom, '00:00', true, $this->factorFee);
      $policies[]   = $cancellation;
    }

    return $policies;
  }

  public function getGuestList() {
    $guests = array();
    $customer_counter = 0;
    // for ($i = 0; $i < $this->getAdultCount(); $i++) {
    //   $customer_counter++;
    //   $guests[] = new \BookingCustomer('AD', $customer_counter, '30', '', '');
    // }
    // $arr = array();
    // $roomAge = $this->data->Occupancy->Passengers->Passenger;
    $childAges = $this->item->childAges;
    $arrAges = array();
    foreach ($childAges as $key => $room) {
      $arrAges[] = $room;
    }
    for ($i = 0; $i < $this->getChildCount(); $i++) {
      $customer_counter++;
      $attr     = $arrAges[$i];
      $guests[] = new \BookingCustomer('CH', $customer_counter, strval($attr), '', '');
    }
    return $guests;
  }
}

?>
