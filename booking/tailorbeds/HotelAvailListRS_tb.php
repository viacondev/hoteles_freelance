<?php
namespace TailorBeds;

include_once('HotelAvailRS_tb.php');

class HotelAvailListRS extends \HotelAvailList {

  private $elements;
  public $serviceHotels;

  public function __construct($rs, $destCode, $hotelName, $checkin, $checkout, $roomCount, $rooms_numbers, $regimen, $factorFee) {
    $res = $rs->getResponse();
    $this->serviceHotels = array();
    $hotels = $res->Details->Trips->Trip->HotelsAvailability->Hotels->Hotel;
    foreach ($hotels as $hotel) {
      $hotelAvail = new HotelAvailRS($hotel, $destCode, $checkin, $checkout, $roomCount, $rooms_numbers, $regimen, $factorFee);
      $count = count($hotelAvail->getRoomsAvail());
      if ($hotelName != '' && $count > 0) {
        $name = $hotelAvail->getName();
        if (preg_match("/$hotelName/i", $name)) {
          $this->serviceHotels[] = $hotelAvail;
        }
      }
      else {
        if ($count > 0) {
          $this->serviceHotels[] = $hotelAvail;
        }
      }
    }
  }

  public function getHotelsAvail() {
    return $this->serviceHotels;
  }

  public function getCurrentPage() {
    return 1;
  }

  public function getTotalPages() {
    return 1;
  }
}

?>
