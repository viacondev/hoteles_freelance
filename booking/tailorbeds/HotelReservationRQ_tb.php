<?php
namespace TailorBeds;

class HotelReservationRQ {

  private $params;
  private $item;
  private $serviceIndex;

  public function __construct($params, $item, $serviceIndex) {
    $this->params       = $params;
    $this->item         = $item;
    $this->serviceIndex = $serviceIndex;
  }

  public function getType() {
    return 'hotelService';
  }

  public function getMethodName() {
    return 'catalog/product/book';
  }

  public function getParams() {
    $p            = $this->params;
    $indexService = $this->serviceIndex;
    $item         = $this->item;

    $rooms          = $item['rooms'];
    $tripProductID  = $rooms[0]->SHRUI;
    $roomCad        = '';
    $seqCount       = 1;
    $paxCad = '';
    $indexCount = 1;
    $paxCount = 1;
    foreach ($rooms as $room) {
      $roomCad .= '<Room RoomType="' . $room->roomTypeCode . '" >
                    <Guests>';
      for ($i = 0; $i < $room->adultCount; $i++) {
        $roomCad .= '<Guest PassengerSequence="' . $paxCount . '"/>';
        $paxCad .= '
                    <Passenger AgeType="NMO.GBL.AGT.ADT" Sequence="' . $paxCount . '">
                      <PersonNames>
                        <PersonName NameType="NMO.GBL.PNT.FIR">' . $p['customerName_' . $indexService . '_' . $indexCount] . '</PersonName>
                        <PersonName NameType="NMO.GBL.PNT.LAS">' . $p['customerLastName_' . $indexService . '_' . $indexCount] . '</PersonName>
                      </PersonNames>                 
                    </Passenger>';
        $indexCount++;
        $paxCount++;
      }
      $arrChilds = array( 1 => 'NMO.GBL.AGT.INF',
                          2 => 'NMO.GBL.AGT.CHD');
      for ($f = 0; $f < $room->childCount; $f++) { 
        $age = intval($p['customerAge_' . $indexService . '_' . $indexCount]);
        $cadChild = $arrChilds[1];
        if ($age > 1) {
          $cadChild = $arrChilds[2];
        }
        $roomCad .= '<Guest PassengerSequence="' . $paxCount . '"/>';
        $paxCad .= '<Passenger AgeType="' . $cadChild . '" Sequence="' . $paxCount . '">
                      <PersonNames>
                        <PersonName NameType="NMO.GBL.PNT.FIR">' . $p['customerName_' . $indexService . '_' . $indexCount] . '</PersonName>
                        <PersonName NameType="NMO.GBL.PNT.LAS">' . $p['customerLastName_' . $indexService . '_' . $indexCount] . '</PersonName>
                      </PersonNames>
                      <BirthDate>2016-05-01</BirthDate> 
                    </Passenger>';
        $indexCount++;
        $paxCount++;
      }
      $roomCad .= ' </Guests>
                  </Room>';
      $seqCount++;
    }

    $xml = '
            <BookingProductsRQ>
              <GeneralParameters>
                  <PreferedLanguage LanguageCode="es"/>
                  <PreferedCurrency CurrencyCode="USD"/>
              </GeneralParameters>
              <Products>
                <Hotels>
                   <Hotel TripProductID="' . $tripProductID . '" >
                      <Rooms>
                        ' . $roomCad . '
                      </Rooms>
                   </Hotel>
                </Hotels>
                <Passengers>
                  ' . $paxCad . '  
                </Passengers>
              </Products>
              <BookingComments>Comentarios de la reserva</BookingComments>
            </BookingProductsRQ>';

    return $xml;
  }
}
?>
