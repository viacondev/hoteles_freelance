<?php

class BookingPrice extends BookingSerializable {

  public $amount;
  public $amountWithFee;
  public $description;

  public function __construct($amount = 0, $description = "", $factorFee = 0.1) {
    $this->amount = $amount;
    $this->amountWithFee = $amount / $factorFee;
    $this->description = $description;
  }

  public function getAmount() {
    return $this->amount;
  }

  public function getAmountWithFee() {
    return round($this->amountWithFee, 2);
  }

  public function getDescription() {
    return strval($this->description);
  }

}

?>
