<?php

include_once('BookingTicketAvail.php');

class BookingTicketAvailList extends BookingSerializable {

  public $currentPage;
  public $totalPages;
  public $totalItems;
  public $ticketsAvail;

  public function getCurrentPage() {
    return $this->currentPage;
  }

  public function getTotalPages() {
    return $this->totalPages;
  }

  public function getTotalItems() {
    return $this->totalItems;
  }

  public function getTicketsAvail() {
    return $this->ticketsAvail;
  }

}

?>
