<?php
namespace HotelBeds;

include_once('Credentials.php');

abstract class HotelBedsRQElement {

  /**
   * Common tags
   */
  protected $xmlns = 'http://www.hotelbeds.com/schemas/2005/06/messages';
  protected $xmlns__xsi = 'http://www.w3.org/2001/XMLSchema-instance';
  protected $xsi__schemaLocation = 'http://www.hotelbeds.com/schemas/2005/06/messages ServiceAddRQ.xsd';
  protected $version = '2013/12';
  protected $Language = WS_LANG;
  protected $Credentials;

  /**
   * Set to true in the classes that are a request (such HotelValuedAvailRQ, etc.)
   */
  public function isRequestClass() {
    return false;
  }

  /**
   * Override for the requests classes (such HotelValuedAvailRQ, etc.)
   */
  public function getRQElementMethodName() {
    return '';
  }

  public function getRQElementName() {
    $classname = get_class($this);

    // Remove the namespace
    if (preg_match('@\\\\([\w]+)$@', $classname, $matches)) {
        $classname = $matches[1];
    }

    return $classname;
  }
  
  public function getRQElementParams() {
    return array();
  }

  public function getRQElementChilds() {
    return array();
  }

  /**
   * When an element acts like a string but has params. Ej:
   * <Comment type="INCOMING">Comments here...</Comment>
   * This method should return 'Comments here...';
   */
  public function getRQElementValueStr() {
    return '';
  }

  protected function getRQElementParamsXML() {
    $arr = $this->getRQElementParams();
    if ($this->isRequestClass()) {
      array_unshift($arr, 'xmlns', 'xmlns__xsi', 'version');
    }

    $params = '';
    foreach ($arr as $value) {
      if (isset($this->$value) && !empty($this->$value)) {
        $key = str_replace('__', ':', $value);
        $params .= ' ' . $key . '="' . $this->$value . '"';
      }
    }

    return $params;
  }

  protected function getRQElementChildsXML() {
    $arr = $this->getRQElementChilds();
    if ($this->isRequestClass()) {
      $this->Credentials = new Credentials();
      array_unshift($arr, 'Language', 'Credentials');
    }

    $childs = '';
    foreach ($arr as $value) {
      if (isset($this->$value)) {
        if (is_subclass_of($this->$value, '\HotelBeds\HotelBedsRQElement')) {
          $childs .= $this->$value->getRQElementXML();
        }
        else if (is_array($this->$value)) {
          foreach ($this->$value as $c) {
            if (is_subclass_of($c, '\HotelBeds\HotelBedsRQElement')) {
              $childs .= $c->getRQElementXML();
            }
            else {
              $childs .= '<' . $value . '>' . $c . '</' . $value . '>';
            }
          }
        }
        else if (!empty($this->$value)) {
          $childs .= '<' . $value . '>' . $this->$value . '</' . $value . '>';
        }
      }
    }
    return $childs;
  }
  
  public function getRQElementXML() {
    return '<' . $this->getRQElementName() . $this->getRQElementParamsXML() . '>
              ' . $this->getRQElementChildsXML() . $this->getRQElementValueStr() . '
            </' . $this->getRQElementName() . '>';
  }

  public function __toString() {
    return $this->getRQElementXML();
  }
}

?>