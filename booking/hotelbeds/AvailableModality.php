<?php
namespace HotelBeds;

class AvailableModality extends HotelBedsRQElement {

  /**
   * Params
   */
  public $code;

  /**
   * Childs
   */
  public $Name;
  public $Contract;

  public function __construct($code, $Name, $Contract) {
    $this->code = $code;
    $this->Name = $Name;
    $this->Contract = $Contract;
  }

  public function getRQElementParams() {
    return array('code');
  }

  public function getRQElementChilds() {
    return array('Name', 'Contract');
  }

}

?>