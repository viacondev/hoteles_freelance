<?php
namespace HotelBeds;

class ServiceTransferRS extends \BookingTransferAvail {

  private $element;
  private $xsi_ns;
  private $typeRequest;
  public  $totalAmountWithFee;
  public function __construct($element, $factorFee = 0.1, $typeRequest = "") {
    $this->element      = $element;
    $this->typeRequest  = $typeRequest;
    $this->xsi_ns       = $this->element->getNameSpaces(true);
    $this->xsi_ns       = $this->xsi_ns['xsi'];
    $this->totalAmountWithFee = floatval($this->element->TotalAmount) / $factorFee;
  }

  public function getProvider() {
    return \BookingProvider::HOTELBEDS;
  }

  public function getTransferType() {
    $attrs = $this->element->attributes();
    return strval($attrs['transferType']);
  }

  public function getAvailToken() {
    $attrs = $this->element->attributes();
    return strval($attrs['availToken']);
  }

  public function getCode() {
    return strval($this->element->TransferInfo->Code);
  }

  public function getName() {
    return $this->getTransferInfoVehicleType() . ' (' . $this->getTransferInfoType() . ')';
  }

  public function getDescription() {
    return strval($this->element->TransferInfo->DescriptionList->Description);
  }

  public function getContractName() {
    return strval($this->element->ContractList->Contract->Name);
  }

  public function getContractIncomingOffice() {
    $attrs = $this->element->ContractList->Contract->IncomingOffice->attributes();
    return strval($attrs['code']);
  }

  public function getContractComment() {
    // if ($this->element->ContractList->Contract->CommentList) {
    //   return strval($this->element->ContractList->Contract->CommentList->Comment[0]);
    // }
     if ($this->element->TransferPickupInformation->Description) {
        $rs = "";
        $rs .= strval($this->element->TransferPickupInformation->Description);
        $rs .= "<br><br>";
        $rs .= "Informacion del Traslado";
        $rs .= "<br><br>";
        $attrs =  $this->element->attributes();
        $type  =  strval($attrs['transferType']);
        $domesticWait = "";
        $domesTime    = "";
        $supplier     = "";
        $supplierTime = "";
        if($type == "OUT") {
          $domesticWait   = $this->element->TransferInfo->TransferSpecificContent->MaximumWaitingTime;
          $domesInterWait = $domesticWait->attributes();
          $domesTime      = strval($domesInterWait['time']);

          $supplier       = $this->element->TransferInfo->TransferSpecificContent->MaximumWaitingTimeSupplierDomestic;
          $attrs          = $supplier->attributes();
          $supplierTime   = strval($attrs['time']);
        }
        
        if($type == "IN") {
          $domesticWait   = $this->element->TransferInfo->TransferSpecificContent->MaximumWaitingTimeSupplierDomestic;
          $domesInterWait = $domesticWait->attributes();
          $domesTime      = strval($domesInterWait['time']);

          $supplier       = $this->element->TransferInfo->TransferSpecificContent->MaximumWaitingTimeSupplierInternational;
          $attrs          = $supplier->attributes();
          $supplierTime   = strval($attrs['time']);
        }
        $rs .= "\t\t\t* Máximo tiempo de espera del conductor en llegadas doméstico " . $domesTime . " " . $domesticWait;
        $rs .= "<br>";
        $rs .= "\t\t\t* Máximo tiempo de espera del conductor en llegadas internacional " . $supplierTime . " " . $supplier;
        $rs .= "<br><br>";

        if (isset($this->element->TransferInfo->TransferSpecificContent->GenericTransferGuidelinesList->TransferBulletPoint)) {
          foreach($this->element->TransferInfo->TransferSpecificContent->GenericTransferGuidelinesList->TransferBulletPoint as $value) {
            $rs .= "\t\t\t* " . strval($value->DetailedDescription);
            $rs .= "<br>";
          }
        }
        
        $rs .= "<br>";
        $res =  $this->element->ProductSpecifications->TransferGeneralInfoList;
        
        foreach ($res->TransferBulletPoint as $value) {
          $rs .= "\t\t\t* " .strval($value->Description);
          $rs .= "<br>";
        }
        $rs .= "<br>";
        if (isset($this->element->ContactInfoList->ContactInfo)) {
          $contac = $this->element->ContactInfoList;
          foreach ($contac->ContactInfo as $value) {
            $rs .= $value->Type . ": " . $value->PhoneNumber . " ";
          }
        }
        
      return $rs;
    }
    else {
      return '';
    }
  }

  public function getDateFrom() {
    $attrs = $this->element->DateFrom->attributes();
    return strval($attrs['date']);
  }

  public function getTimeFrom() {
    $attrs =  $this->element->attributes();
    if ($this->typeRequest == "") {
      $type  =  strval($attrs['transferType']);
      $time  =  "";
      if ($type == "IN") {
        $attrs = $this->element->ArrivalTravelInfo->ArrivalInfo->DateTime->attributes();
        $time  = strval($attrs['time']);
      }
      return $time;
    }
    
    $attrs = $this->element->DateFrom->attributes();
    return strval($attrs['time']);
  }

  public function getCurrency() {
    $attrs = $this->element->Currency->attributes();
    return strval($attrs['code']);
  }

  public function getTotalAmount() {
    return round($this->element->TotalAmount, 2);
  }

  public function getTotalAmountWithFee() {
    return round($this->totalAmountWithFee, 2);
  }

  public function getMainImage() {
    if (isset($this->element->TransferInfo->ImageList)) {
      $images = $this->element->TransferInfo->ImageList->Image;
      // $img    = str_replace('/transfers/', '/transfers/big/', strval($images[0]->Url));
      $img    = strval($images[0]->Url);
      return $img;
    }

    return 'assets/images/no-photo.jpg';
  }

  public function getTransferInfoType() {
    $res = $this->element->ProductSpecifications->MasterProductType->attributes();
    $res1 = $this->element->ProductSpecifications->MasterServiceType->attributes();
    return strval($res1['name']) . ' ' . strval($res['name']);
    // $res = $this->element->TransferInfo->Type->attributes();
    // return strval($res['code']);
  }

  public function getTransferInfoTypeCode() {
    $attrs = $this->element->TransferInfo->Type->attributes();
    return strval($attrs['code']);
  }

  public function getTransferInfoVehicleType() {
    $res = $this->element->ProductSpecifications->MasterVehicleType->attributes();
    return strval($res['name']);
    // $res = $this->element->TransferInfo->VehicleType;
    // return strval($res['code']);
  }

  public function getTransferInfoVehicleTypeCode() {
    $attrs = $this->element->TransferInfo->VehicleType->attributes();
    return strval($attrs['code']);
  }

  public function getAdultCount() {
    return strval($this->element->Paxes->AdultCount);
  }


  public function getChildCount() {
    return strval($this->element->Paxes->ChildCount);
  }

  public function getGuestList() {
    $guests = array();
    foreach ($this->element->Paxes->GuestList->Customer as $customer) {
      $attrs = $customer->attributes();
      $type = $attrs['type'];
      $customerId = strval($customer->CustomerId);
      $age = strval($customer->Age);
      $name = isset($customer->Name) ? strval($customer->Name) : NULL;
      $lastName = isset($customer->LastName) ? strval($customer->LastName) : NULL;

      $guests[] = new \BookingCustomer($type, $customerId, $age, $name, $lastName);
    }

    return $guests;
  }

  public function getPickupLocationType() {
    $attrs = $this->element->PickupLocation->attributes($this->xsi_ns);
    return strval($attrs['type']);
  }

  public function getPickupLocationCode() {
    return strval($this->element->PickupLocation->Code);
  }

  public function getPickupLocationName() {
    return strval($this->element->PickupLocation->Name);
  }

  public function getPickupLocationTransferZoneCode() {
    return strval($this->element->PickupLocation->TransferZone->Code);
  }

  public function getPickupLocationTransferZoneName() {
    return strval($this->element->PickupLocation->TransferZone->Name);
  }

  public function getDestLocationType() {
    $attrs = $this->element->DestinationLocation->attributes($this->xsi_ns);
    return strval($attrs['type']);
  }

  public function getDestinationCode() {
    return strval($this->element->DestinationLocation->Code);
  }

  public function getDestLocationCode() {
    return strval($this->element->DestinationLocation->Code);
  }

  public function getDestLocationName() {
    return strval($this->element->DestinationLocation->Name);
  }

  public function getDestLocationTransferZoneCode() {
    return strval($this->element->DestinationLocation->TransferZone->Code);
  }

  public function getDestLocationTransferZoneName() {
    return strval($this->element->DestinationLocation->TransferZone->Name);
  }

  public function getTravelInfoDepartType() {
    if (!isset($this->element->DepartureTravelInfo->DepartInfo)) return NULL;

    $attrs = $this->element->DepartureTravelInfo->DepartInfo->attributes($this->xsi_ns);
    return strval($attrs['type']);
  }

  public function getTravelInfoDepartCode() {
    if (!isset($this->element->DepartureTravelInfo->DepartInfo)) return NULL;

    return strval($this->element->DepartureTravelInfo->DepartInfo->Code);
  }

  public function getTravelInfoDepartDate() {
    if (!isset($this->element->DepartureTravelInfo->DepartInfo)) return NULL;

    $attrs = $this->element->DepartureTravelInfo->DepartInfo->DateTime;
    return strval($attrs['date']);
  }

  public function getTravelInfoDepartTime() {
    $attrs =  $this->element->attributes();
    if ($this->typeRequest == "") {
      $type  =  strval($attrs['transferType']);
      $time  =  "";
      if($type == "OUT" && isset($this->element->DepartureTravelInfo->DepartInfo)) {
        $attrs = $this->element->DepartureTravelInfo->DepartInfo->DateTime->attributes();
        $time = strval($attrs['time']);
      }
      return $time;
    }
    
    if (!isset($this->element->TravelInfo->DepartInfo)) return NULL;

    $attrs = $this->element->TravelInfo->DepartInfo->DateTime;
    return strval($attrs['time']);
  }

  public function getTravelInfoArrivalType() {
    if (!isset($this->element->ArrivalTravelInfo->ArrivalInfo)) return NULL;

    $attrs = $this->element->ArrivalTravelInfo->ArrivalInfo->attributes($this->xsi_ns);
    return strval($attrs['type']);
  }

  public function getTravelInfoArrivalCode() {
    if (!isset($this->element->ArrivalTravelInfo->ArrivalInfo)) return NULL;

    return strval($this->element->ArrivalTravelInfo->ArrivalInfo->Code);
  }

  public function getTravelInfoArrivalDate() {
    if (!isset($this->element->ArrivalTravelInfo->ArrivalInfo)) return NULL;
    
    // $attrs = $this->element->TravelInfo->ArrivalInfo->DateTime;
    $attrs = $this->element->ArrivalTravelInfo->ArrivalInfo->DateTime->attributes();
    return strval($attrs['date']);
  }

  public function getTravelInfoArrivalTime() {
    if (!isset($this->element->ArrivalTravelInfo->ArrivalInfo)) return NULL;

    // $attrs = $this->element->TravelInfo->ArrivalInfo->DateTime;
    $attrs = $this->element->ArrivalTravelInfo->ArrivalInfo->DateTime->attributes();
    return strval($attrs['time']);
  }

  public function getTravelNumber() {
    $attrs =  $this->element->attributes();
    $type  =  strval($attrs['transferType']);
    $travelNumber  =  "NULL";
    if($type == "OUT") {
      $travelNumber = $this->element->DepartureTravelInfo->TravelNumber;
    }
    if($type == "IN") {
      $travelNumber = $this->element->ArrivalTravelInfo->TravelNumber;
    }
    return $travelNumber;
  }

  public function getCancellationPolicies() {
    $policies_arr = array();
    // if (isset($this->element->CancellationPolicy)) {
    //   $amount = $this->element->CancellationPolicy->Price->Amount;
    //   $attrs = $this->element->CancellationPolicy->Price->DateTimeFrom->attributes();
    //   $dateFrom = new \DateTime(dateFromYmdToDB($attrs['date']));
    //   $time = formatTime($attrs['time']);      
    //   $policies_arr[] = new \BookingCancellationPolicy($amount, $dateFrom, $time, true);
    // }

    if (isset($this->element->CancellationPolicies)) {
      $attrs      = $this->element->CancellationPolicies->CancellationPolicy->attributes();
      $amount     = strval($attrs['amount']);
      $dateFrom   = new \DateTime(dateFromYmdToDB(strval($attrs['dateFrom'])));
      $time       = formatTime(strval($attrs['time']));      
      $cancellation = new \BookingCancellationPolicy();
      $cancellation->build($amount, $dateFrom, $time, true);
      $policies_arr[] = $cancellation;
    }
    else {
      $dateFrom   = new \DateTime("now");
      $time       = "00:00";
      $amount     = round(floatval($this->element->TotalAmount), 2);     
      $cancellation = new \BookingCancellationPolicy($amount, $dateFrom, $time, true);
      $cancellation->build($amount, $dateFrom, $time, true);
      $policies_arr[] = $cancellation;
    }
    return $policies_arr;
  }

  public function getSupplier() {
    $attrs = $this->element->Supplier;
    return strval($attrs['name']);
  }

  public function getSupplierVatNumber() {
    $attrs = $this->element->Supplier;
    return strval($attrs['vatNumber']);
  }
}

?>
