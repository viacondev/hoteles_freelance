<?php
namespace HotelBeds;

class Database {
  private $dbname = 'barrybol_hotelbeds';
  private $dbuser = 'barrybol_zion';
  private $dbpwd = 'zionshopdbpwd';

  private $db;
  static private $instance = null;
  
  private function __construct() {
    $this->db = new \PDO("mysql:host=23.91.64.139;dbname=" . $this->dbname . ';charset=utf8', $this->dbuser, $this->dbpwd);
  }
  
  public static function getInstance() {
    if (!isset(self::$instance)) {
      self::$instance = new Database();
    }
    return self::$instance;
  }

  public function fetchClass($query, $className) {
    $result = $this->db->query($query);
    $result->setFetchMode(\PDO::FETCH_CLASS, $className);

    return $result->fetch();
  }

  public function fetchAll($query, $className) {
    $result = $this->db->query($query);
    $result->setFetchMode(\PDO::FETCH_CLASS, $className);

    return $result->fetchAll();
  }
}


?>