<?php
namespace HotelBeds;

include_once('Customer.php');
include_once('ElementsList.php');
include_once('ServiceDetail.php');
include_once('Comment.php');

class ServiceData extends HotelBedsRQElement {

  /**
   * Params
   */
  public $xsi__type;
  public $SPUI;

  /**
   * Childs
   */
  public $CustomerList;
  public $CommentList;

  public function __construct($xsi__type, $SPUI, $CustomerList, $CommentList = NULL) {
    $this->xsi__type = $xsi__type;
    $this->SPUI = $SPUI;
    $this->CustomerList = $CustomerList;
    $this->CommentList = $CommentList;
  }

  public function getRQElementParams() {
    return array('xsi__type', 'SPUI');
  }

  public function getRQElementChilds() {
    return array('CustomerList', 'CommentList');
  }

}

?>