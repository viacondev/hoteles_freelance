<?php
namespace HotelBeds;

include_once('PurchaseReference.php');

class PurchaseCancelRQ extends HotelBedsRQElement {

  private $methodName = 'purchaseCancel';

  /**
   * Params
   */
  public $type = 'C'; // C: Cancel; V: Ask for canceling cost (does not cancel)

  /**
   * Childs
   */
  public $PurchaseReference;

  public function __construct($PurchaseReference, $type = 'C') {
    $this->PurchaseReference = $PurchaseReference;
  }

  public function isRequestClass() {
    return true;
  }

  public function getRQElementMethodName() {
    return $this->methodName;
  }

  public function getRQElementParams() {
    return array('type');
  }

  public function getRQElementChilds() {
    return array('PurchaseReference');
  }
}
?>