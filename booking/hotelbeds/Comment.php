<?php
namespace HotelBeds;

class Comment extends HotelBedsRQElement {

  /**
   * Params
   */
  public $type;

  public $_value;

  public function __construct($type, $value) {
    $this->type = $type;
    $this->_value = $value;
  }

  public function getRQElementParams() {
    return array('type');
  }

  public function getRQElementValueStr() {
    return $this->_value;
  }

}

?>