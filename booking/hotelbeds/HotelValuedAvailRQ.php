<?php
namespace HotelBeds;

include_once('PaginationData.php');
include_once('Destination.php');
include_once('DateElement.php');
include_once('ElementsList.php');
include_once('HotelOccupancy.php');
include_once('Occupancy.php');
include_once('HotelCodeList.php');
include_once('ExtendedData.php');

class HotelValuedAvailRQ extends HotelBedsRQElement {

  private $methodName = 'getHotelValuedAvail';

  /**
   * Params
   */
  public $echoToken = 'DummyEchoToken';
  public $sessionId = 'DummySessionId';

  /**
   * Childs
   */
  public $PaginationData;
  public $Destination;
  public $CheckInDate;
  public $CheckOutDate;
  public $OccupancyList;
  public $HotelCodeList;

  public function __construct($Destination, $CheckinDate, $CheckOutDate, $OccupancyList, $PaginationData, $HotelCodeList, $ExtraParamList) {
    $this->Destination = $Destination;
    $this->CheckinDate = $CheckinDate;
    $this->CheckOutDate = $CheckOutDate;
    $this->OccupancyList = $OccupancyList;
    $this->PaginationData = $PaginationData;
    $this->HotelCodeList = $HotelCodeList;
    $this->ExtraParamList = $ExtraParamList;
  }

  public function isRequestClass() {
    return true;
  }

  public function getRQElementMethodName() {
    return $this->methodName;
  }

  public function getRQElementParams() {
    return array('echoToken', 'sessionId');
  }

  public function getRQElementChilds() {
    return array('PaginationData', 'CheckinDate', 'CheckOutDate', 'Destination', 'OccupancyList', 'HotelCodeList', 'ExtraParamList');
  }
}
?>