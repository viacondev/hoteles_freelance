<?php
namespace HotelBeds;

include_once('Contract.php');
include_once('DateElement.php');
include_once('HotelInfo.php');
include_once('AvailableRoom.php');

class ServiceHotel extends HotelBedsRQElement {

  /**
   * Params
   */
  public $availToken;
  public $xsi__type = 'ServiceHotel';

  /**
   * Childs
   */
  public $ContractList;
  public $DateFrom;
  public $DateTo;
  public $HotelInfo;
  public $AvailableRooms;

  public function __construct($availToken, $Contract, $dateFrom, $dateTo, $HotelInfo, $AvailableRooms) {
    $this->availToken = $availToken;
    $this->ContractList = new ElementsList('ContractList', array($Contract));
    $this->DateFrom = new DateElement($dateFrom, 'DateFrom');
    $this->DateTo = new DateElement($dateTo, 'DateTo');
    $this->HotelInfo = $HotelInfo;
    $this->AvailableRooms = $AvailableRooms;
  }

  public function getRQElementName() {
    return 'Service';
  }

  public function getRQElementParams() {
    return array('availToken', 'xsi__type');
  }

  public function getRQElementChilds() {
    return array('ContractList', 'DateFrom', 'DateTo', 'HotelInfo', 'AvailableRooms');
  }

}

?>