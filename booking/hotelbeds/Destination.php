<?php
namespace HotelBeds;

include('Zone.php');

class Destination extends HotelBedsRQElement {

  /**
   * Params
   */
  public $code;
  public $type;

  /**
   * Childs
   */
  public $ZoneList;

  public function __construct($code, $type = 'SIMPLE', $ZoneList = NULL) {
    $this->code = $code;
    $this->type = $type;
    $this->ZoneList = $ZoneList;
  }

  public function getRQElementParams() {
    return array('code', 'type');
  }

  public function getRQElementChilds() {
    return array('ZoneList');
  }

}

?>