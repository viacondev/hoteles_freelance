<?php
namespace HotelBeds;

class ElementsList extends HotelBedsRQElement {

  public $items;
  public $elementName;

  public function __construct($elementName = 'List', $items = array()) {
    $this->items = $items;
    $this->elementName = $elementName;
  }

  public function getRQElementName() {
    return $this->elementName;
  }

  public function addItem($item) {
    array_push($this->items, $item);
  }

  public function AddItems($items) {
    $this->items = $items;
  }

  protected function getRQElementChildsXML() {
    $childs = '';
    foreach ($this->items as $item) {
      $childs .= $item->getRQElementXML();
    }
    return $childs;
  }

}

?>