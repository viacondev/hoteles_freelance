<?php
namespace HotelBeds;

class Zone extends HotelBedsRQElement {

  /**
   * Params
   */
  public $code;
  public $type;

  public function __construct($code, $type = 'SIMPLE') {
    $this->code = $code;
    $this->type = $type;
  }

  public function getRQElementParams() {
    return array('code', 'type');
  }

}

?>