<?php
namespace HotelBeds;

include_once('HotelOccupancy.php');
include_once('HotelRoom.php');

class AvailableRoom extends HotelBedsRQElement {

  /**
   * Childs
   */
  public $HotelOccupancy;
  public $HotelRoom;

  public function __construct($HotelOccupancy, $HotelRoom) {
    $this->HotelOccupancy = $HotelOccupancy;
    $this->HotelRoom = $HotelRoom;
  }


  public function getRQElementChilds() {
    return array('HotelOccupancy', 'HotelRoom');
  }

}

?>