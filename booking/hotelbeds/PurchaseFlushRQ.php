<?php
namespace HotelBeds;

class PurchaseFlushRQ extends HotelBedsRQElement {

  private $methodName = 'purchaseFlush';

  /**
   * Params
   */
  public $purchaseToken;

  public function __construct($purchaseToken) {
    $this->purchaseToken = $purchaseToken;
  }

  public function isRequestClass() {
    return true;
  }

  public function getRQElementMethodName() {
    return $this->methodName;
  }

  public function getRQElementParams() {
    return array('purchaseToken');
  }
}
?>