<?php
namespace HotelBeds;

class TicketInfo extends HotelBedsRQElement {

  /**
   * Params
   */
  public $xsi__type = 'ProductTicket';

  /**
   * Childs
   */
  public $Code;
  public $Destination;

  public function __construct($code, $destCode, $destType) {
    $this->Code = $code;
    $this->Destination = new Destination($destCode, $destType);
  }

  public function getRQElementParams() {
    return array('xsi__type');
  }

  public function getRQElementChilds() {
    return array('Code', 'Destination');
  }

}

?>