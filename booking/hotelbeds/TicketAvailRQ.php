<?php
namespace HotelBeds;

include_once('PaginationData.php');
include_once('ServiceOccupancy.php');
include_once('Destination.php');
include_once('DateElement.php');
include_once('ExtendedData.php');

class TicketAvailRQ extends HotelBedsRQElement {

  private $methodName = 'getTicketAvail';

  /**
   * Params
   */
  public $echoToken = 'DummyEchoToken';
  public $sessionId = 'DummySessionId';

  /**
   * Childs
   */
  public $PaginationData;
  public $ServiceOccupancy;
  public $Destination;
  public $DateFrom;
  public $DateTo;
  public $ExtraParamList;

  public function __construct($PaginationData, $ServiceOccupancy, $Destination, $DateFrom, $DateTo, $ExtraParamList) {
    $this->PaginationData = $PaginationData;
    $this->ServiceOccupancy = $ServiceOccupancy;
    $this->Destination = $Destination;
    $this->DateFrom = $DateFrom;
    $this->DateTo = $DateTo;
    $this->ExtraParamList = $ExtraParamList;
  }

  public function isRequestClass() {
    return true;
  }

  public function getRQElementMethodName() {
    return $this->methodName;
  }

  public function getRQElementParams() {
    return array('echoToken', 'sessionId');
  }

  public function getRQElementChilds() {
    return array('PaginationData', 'ServiceOccupancy', 'Destination', 'DateFrom', 'DateTo', 'ExtraParamList');
  }
}
?>