<?php
namespace HotelBeds;

include_once('AvailData.php');

class TransferValuedAvailRQ extends HotelBedsRQElement {

  private $methodName = 'getTransferValuedAvail';

  /**
   * Params
   */
  public $echoToken = 'DummyEchoToken';
  public $sessionId = 'DummySessionId';

  /**
   * Childs
   */
  public $AvailDataIn;
  public $AvailDataOut;

  public function __construct($AvailDataIn, $AvailDataOut) {
    $this->AvailDataIn = $AvailDataIn;
    $this->AvailDataOut = $AvailDataOut;
  }

  public function isRequestClass() {
    return true;
  }

  public function getRQElementMethodName() {
    return $this->methodName;
  }

  public function getRQElementParams() {
    return array('echoToken', 'sessionId');
  }

  public function getRQElementChilds() {
    return array('AvailDataIn', 'AvailDataOut');
  }
}
?>