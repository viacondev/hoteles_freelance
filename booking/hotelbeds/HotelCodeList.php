<?php
namespace HotelBeds;

class HotelCodeList extends HotelBedsRQElement {

  /**
   * Params
   */  
  public $withinResults;

  /**
   * Childs
   */
  public $ProductCode;

  public function __construct($ProductCode = array(), $withinResults = 'Y') {
    $this->ProductCode = $ProductCode;
    $this->withinResults = $withinResults;
  }

  public function getRQElementParams() {
    return array('withinResults');
  }

  public function getRQElementChilds() {
    return array('ProductCode');
  }

}

?>