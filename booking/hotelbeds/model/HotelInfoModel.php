<?php
namespace HotelBeds;

class HotelInfoModel {

  public $HotelCode;
  public $HotelFacilities;
  public $ZoneName;
  public $DestinationName;

  public function getDescription() {
    return $this->HotelFacilities;
  }

  public function getZone() {
    return $this->ZoneName;
  }

  public function getLocation() {
    return $this->ZoneName . ', ' . $this->DestinationName;
  }

  public function getCode() {
    return $this->HotelCode;
  }

  public static function buildClass($code) {
    $query = "SELECT d.HotelFacilities, z.ZONENAME as ZoneName, dest_id.NAME as DestinationName, d.HotelCode
                FROM HOTEL_DESCRIPTIONS as d, HOTELS as h, ZONES as z, DESTINATION_IDS as dest_id
                WHERE d.HotelCode = $code AND d.LanguageCode = '" . WS_LANG . "'
                  AND h.HOTELCODE=d.HotelCode AND h.DESTINATIONCODE=z.DESTINATIONCODE
                  AND h.ZONECODE=z.ZONECODE AND dest_id.DESTINATIONCODE=z.DESTINATIONCODE
                  AND dest_id.LANGUAGECODE='" . WS_LANG . "'";

    $hotelInfo = Database::getInstance()->fetchClass($query, 'HotelBeds\HotelInfoModel');
    if (!$hotelInfo) {
      $hotelInfo = new HotelInfoModel();
      $hotelInfo->HotelFacilities = "M&aacute;s informaci&oacute;n de los hoteles dentro de poco tiempo...";
    }
    return $hotelInfo;
  }

  public static function buildClassListHotelCode($code) {
    $query = "SELECT d.HotelFacilities, z.ZONENAME as ZoneName, dest_id.NAME as DestinationName, d.HotelCode
                FROM HOTEL_DESCRIPTIONS as d, HOTELS as h, ZONES as z, DESTINATION_IDS as dest_id
                WHERE d.HotelCode IN ($code) AND d.LanguageCode = '" . WS_LANG . "'
                  AND h.HOTELCODE=d.HotelCode AND h.DESTINATIONCODE=z.DESTINATIONCODE
                  AND h.ZONECODE=z.ZONECODE AND dest_id.DESTINATIONCODE=z.DESTINATIONCODE
                  AND dest_id.LANGUAGECODE='" . WS_LANG . "'";

    $hotelInfo = Database::getInstance()->fetchAll($query, 'HotelBeds\HotelInfoModel');
    if (count($hotelInfo) == 0) {
      $hotelInfo = new HotelInfoModel();
      $hotelInfo->HotelFacilities = "M&aacute;s informaci&oacute;n de los hoteles dentro de poco tiempo...";
    }
    return $hotelInfo;
  }
  /**
   * Returns an array of HotelInfoModel that only contains the Code
   */
  public static function findHotelsByCode($name, $dest) {
    $query = "SELECT HOTELCODE AS HotelCode FROM HOTELS
                WHERE NAME LIKE '%$name%' AND DESTINATIONCODE = '$dest'";

    return Database::getInstance()->fetchAll($query, 'HotelBeds\HotelInfoModel');
  }
  public static function findHotelByStar($star, $dest, $name){
    $query = "SELECT HOTELCODE AS HotelCode FROM HOTELS
                WHERE NAME LIKE '%$name%' AND DESTINATIONCODE = '$dest' AND CATEGORYCODE IN ($star) limit 1000";
                                                                 
    return  Database::getInstance()->fetchAll($query, 'HotelBeds\HotelInfoModel');      
  }
}

?>