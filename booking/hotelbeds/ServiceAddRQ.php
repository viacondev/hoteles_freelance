<?php
namespace HotelBeds;

include_once('ServiceHotel.php');
include_once('ServiceTicket.php');
include_once('ServiceTransfer.php');
include_once('ElementsList.php');

class ServiceAddRQ extends HotelBedsRQElement {

  private $methodName = 'serviceAdd';

  /**
   * Params
   */
  public $echoToken = 'DummyEchoToken';
  public $purchaseToken;

  /**
   * Childs
   */
  public $Service;

  public function __construct($Service, $purchaseToken = NULL) {
    $this->Service = $Service;
    $this->purchaseToken = $purchaseToken;
  }

  public function isRequestClass() {
    return true;
  }

  public function getRQElementMethodName() {
    return $this->methodName;
  }

  public function getRQElementParams() {
    return array('echoToken', 'purchaseToken');
  }

  public function getRQElementChilds() {
    return array('Service');
  }
}
?>