<?php
namespace HotelBeds;

include_once('TransferZone.php');

class TransferLocation extends HotelBedsRQElement {

  /**
   * Params
   */
  public $xsi__type;

  /**
   * Childs
   */
  public $Code;
  public $DateTime;
  public $TransferZone;
  public $elementName;

  public function __construct($xsi__type, $Code, $DateTime, $TransferZone, $elementName = 'PickupLocation') {
    $this->xsi__type = $xsi__type;
    $this->Code = $Code;
    $this->DateTime = NULL;
    $this->TransferZone = NULL;
    $this->elementName = $elementName;

    if ($xsi__type == 'ProductTransferHotel') {
      $this->TransferZone = $TransferZone;
    }
    if ($xsi__type == 'ProductTransferTerminal') {
      $this->DateTime = $DateTime;
    }
  }

  public function setClassName($name) {
    $this->elementName = $name;
  }

  public function getRQElementName() {
    return $this->elementName;
  }

  public function getRQElementParams() {
    return array('xsi__type');
  }

  public function getRQElementChilds() {
    return array('Code', 'DateTime', 'TransferZone');
  }

}

?>