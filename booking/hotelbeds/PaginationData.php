<?php
namespace HotelBeds;

class PaginationData extends HotelBedsRQElement {

  /**
   * Params
   */
  public $pageNumber;
  public $itemsPerPage;

  public function __construct($pageNumber = 900, $itemsPerPage = 999) {
    $this->pageNumber = $pageNumber;
    $this->itemsPerPage = $itemsPerPage;
  }

  public function getRQElementParams() {
    return array('pageNumber', 'itemsPerPage');
  }

}

?>
