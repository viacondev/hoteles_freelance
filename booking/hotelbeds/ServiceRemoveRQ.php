<?php
namespace HotelBeds;

class ServiceRemoveRQ extends HotelBedsRQElement {

  private $methodName = 'serviceRemove';

  /**
   * Params
   */
  public $purchaseToken;
  public $SPUI;

  public function __construct($purchaseToken, $SPUI) {
    $this->purchaseToken = $purchaseToken;
    $this->SPUI = $SPUI;
  }

  public function isRequestClass() {
    return true;
  }

  public function getRQElementMethodName() {
    return $this->methodName;
  }

  public function getRQElementParams() {
    return array('purchaseToken', 'SPUI');
  }
}
?>