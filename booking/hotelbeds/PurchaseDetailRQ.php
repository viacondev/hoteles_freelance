<?php
namespace HotelBeds;

include_once('PurchaseReference.php');

class PurchaseDetailRQ extends HotelBedsRQElement {

  private $methodName = 'getPurchaseDetail';

  /**
   * Params
   */
  public $echoToken = 'DummyEchoToken';

  /**
   * Childs
   */
  public $PurchaseToken;
  public $PurchaseReference;

  public function __construct($purchaseToken = NULL, $PurchaseReference = NULL) {
    $this->PurchaseToken = $purchaseToken;
    $this->PurchaseReference = $PurchaseReference;
  }

  public function isRequestClass() {
    return true;
  }

  public function getRQElementMethodName() {
    return $this->methodName;
  }

  public function getRQElementParams() {
    return array('echoToken');
  }

  public function getRQElementChilds() {
    return array('PurchaseToken', 'PurchaseReference');
  }
}
?>