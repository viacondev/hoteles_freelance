<?php
namespace HotelBeds;

include_once('model/HotelInfoModel.php');
include_once('AvailableRoomRS.php');

class ServiceHotelRS extends \HotelAvail {

  public  $element;
  private $hotelInfo;
  private $availableRooms;
  private $roomCount;
  public  $factorFee;
  public  $hotelZionCode;

  public function __construct($serviceHotelElement, $board_filter = '', $hotelInfo, $roomCount = 0, $factorFee = 0.79, $extraFee = 0) {
    $this->element    = $serviceHotelElement;
    $this->roomCount  = $roomCount;

    // $this->hotelInfo = HotelInfoModel::buildClass($this->getCode());
    $this->hotelInfo = $hotelInfo;
    $this->factorFee = $factorFee;
    $this->availableRooms = array();
    $rooms = $this->element->AvailableRoom;
    foreach ($rooms as $room) {
      if($board_filter != '') {
        $attr = $room->HotelRoom->Board->attributes();
        if(in_array(substr($attr['code'], 0, 2), $board_filter)) {
          $this->availableRooms[] = new AvailableRoomRS($room, $this, $factorFee, $extraFee, $this->element->HotelInfo->Code);
        }
      }
      else{
        $this->availableRooms[] = new AvailableRoomRS($room, $this, $factorFee, $extraFee, $this->element->HotelInfo->Code);
      }
    }
    if(count($this->availableRooms) > 0)
      $this->createGroupedRooms();
  }

  public function getProvider() {
    return \BookingProvider::HOTELBEDS;
  }

  public function getProviderName() {
    return 'HOTELBEDS';
  }

  public function getAvailToken() {
    $attrs = $this->element->attributes();
    return strval($attrs['availToken']);
  }

  public function getCode() {
    return strval($this->element->HotelInfo->Code);
  }

  public function getHotelZionCode() {
    $this->hotelZionCode = strval($this->element->HotelInfo->Code);
    return $this->hotelZionCode;
  }

  public function getName() {
    return strval(htmlspecialchars($this->element->HotelInfo->Name, ENT_QUOTES, 'UTF-8'));
  }

  public function getDateFrom() {
    $attrs = $this->element->DateFrom->attributes();
    return strval($attrs['date']);
  }

  public function getDateTo() {
    $attrs = $this->element->DateTo->attributes();
    return strval($attrs['date']);
  }

  public function getDestinationCode() {
    $attrs = $this->element->HotelInfo->Destination->attributes();
    return strval($attrs['code']);
  }

  public function getDestinationType() {
    $attrs = $this->element->HotelInfo->Destination->attributes();
    return strval($attrs['type']);
  }

  public function getMainImage() {
    if (isset($this->element->HotelInfo->ImageList)) {
      $images = $this->element->HotelInfo->ImageList->Image;
      return str_replace('/small', '', strval($images[0]->Url));
    }

    return 'assets/images/no-photo.jpg';
  }

  public function getDescription() {
    if (method_exists($this->hotelInfo, "getDescription")) {
      return strval($this->hotelInfo->getDescription());
    }
    return "";
  }

  public function getLatitude() {
    if(isset($this->element->HotelInfo->Position)) {
      $location = $this->element->HotelInfo->Position->attributes();
      return strval($location['latitude']);
    }
    return '';
  }

  public function getLongitude() {
    if($this->element->HotelInfo->Position) {
      $location = $this->element->HotelInfo->Position->attributes();
      return strval($location['longitude']);
    }
    return '';
  }

  public function getZone() {
    if (method_exists($this->hotelInfo, "getZone")) {
      return strval($this->hotelInfo->getZone());
    }
    return "";
  }

  public function getLocation() {
    if (method_exists($this->hotelInfo, "getLocation")) {
      return strval($this->hotelInfo->getLocation());
    }
    return "";
  }

  public function getCategory() {
    return strval($this->element->HotelInfo->Category);
  }

  public function getCategoryType() {
    $cat = $this->element->HotelInfo->Category->attributes();
    return strval($cat['type']);
  }

  public function getCategoryCode() {
    $cat = $this->element->HotelInfo->Category->attributes();
    return strval($cat['code']);
  }

  public function getRoomsAvail() {
    return $this->availableRooms;
  }

  public function getCurrency() {
    $currency = $this->element->Currency->attributes();
    return strval($currency['code']);
  }

  public function getContractName() {
    return strval($this->element->ContractList->Contract->Name);
  }

  public function getContractIncomingOfficeCode() {
    $attrs = $this->element->ContractList->Contract->IncomingOffice;
    return strval($attrs['code']);
  }

  public function getContractComment() {
    if ($this->element->ContractList->Contract->CommentList) {
      return strval($this->element->ContractList->Contract->CommentList->Comment[0]);
    }
    else {
      return '';
    }
  }

  public function getClientComment() {
    if ($this->element->CommentList) {
      foreach ($this->element->CommentList->Comment as $comment) {
        $attrs = $comment->attributes();
        if ($attrs['type'] == 'INCOMING') {
          return strval($comment);
        }
      }
    }
    else {
      return '';
    }
  }

  public function getSupplier() {
    $attrs = $this->element->Supplier;
    return strval($attrs['name']);
  }

  public function getSupplierVatNumber() {
    $attrs = $this->element->Supplier;
    return strval($attrs['vatNumber']);
  }

  public function getDiscountList() {
    $discounts = array();
    if (isset($this->element->DiscountList)) {
      foreach ($this->element->DiscountList->Price as $price) {
        $commissionedPrice = round(floatval($price->Amount), 2);
        $discounts[] = new \BookingPrice($commissionedPrice, strval($price->Description), $this->factorFee);
      }
    }
    return $discounts;
  }

  private function createGroupedRooms() {
    $rooms = $this->getRoomsAvail();

    // Move the rooms to another array that contains the rooms according to the accommodation
    // i.e. $multiRooms['1_2_0'] contains all rooms with 1 Room, 2 adults and 0 childs.
    $multiRooms = array();
    $multiRoomsKey = array();
    foreach ($rooms as $room) {
      $key = $room->getRoomCount() . '_' . $room->getAdultCount() . '_' . $room->getChildCount();
      if (!isset($multiRooms[$key])) {
        $multiRooms[$key] = array();
        $multiRoomsKey[] = $key;
      }
      $multiRooms[$key][] = $room;
    }
    // Contain the room's groups that will be displayed to the user.
    // Groups are formed looking for the best matching across all rooms
    // If we only request for one room then all groups will have one item,
    // if we request for two rooms then all groups will have two items and so on.
    $this->groupedRooms = array();
    foreach ($multiRooms[$multiRoomsKey[0]] as $room) {
      $group = array();
      $group[] = $room;
      for ($i = 1; $i < count($multiRoomsKey); $i++) {
        $bestMatch = NULL;
        $bestMatchPoints = -1;
        foreach ($multiRooms[$multiRoomsKey[$i]] as $nextRoom) {
          $points = 0;
          if ($room->getBoard() == $nextRoom->getBoard()) {
            $points++;
            if ($room->getRoomTypeCode() == $nextRoom->getRoomTypeCode()) {
              $points++;
              if ($room->getRoomTypeCharacteristic() == $nextRoom->getRoomTypeCharacteristic()) {
                $points++;
              }
            }
          }
          if ($points > $bestMatchPoints) {
            $bestMatch = $nextRoom;
            $bestMatchPoints = $points;
          }
        }
        $group[] = $bestMatch;
      }
      // __logarr($group);
      if ($this->roomCount != 0 && $this->roomCount == $group[0]->getRoomCount() || $this->roomCount != 0 && $this->roomCount == count($group)) {
        $this->insertGroupedRoom($group);
      }
    }
  }

  public function getAllPaxRequired() {
    return false;
  }

  public function getXtraDataRequired() {
    return false;
  }

  public function isDataRequired() {
   return false;
  }
}

?>
