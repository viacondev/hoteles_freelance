<?php
namespace HotelBeds;

class TransferZone extends HotelBedsRQElement {

  /**
   * Params
   */
  public $xsi__type = "ProductZone";
  /**
   * Childs
   */
  public $Code;

  public function __construct($Code) {
    $this->Code = $Code;
  }

  public function getRQElementParams() {
    return array('xsi__type');
  }

  public function getRQElementChilds() {
    return array('Code');
  }

}

?>