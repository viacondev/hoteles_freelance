<?php
namespace HotelBeds;

class HotelInfo extends HotelBedsRQElement {

  /**
   * Params
   */
  public $xsi__type = 'ProductHotel';

  /**
   * Childs
   */
  public $Code;
  public $Destination;

  public function __construct($code, $destCode, $destType) {
    $this->Code = $code;
    $this->Destination = new Destination($destCode, $destType);
  }

  public function getRQElementParams() {
    return array('xsi__type');
  }

  public function getRQElementChilds() {
    return array('Code', 'Destination');
  }

}

?>