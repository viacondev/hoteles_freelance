<?php
namespace HotelBeds;

include_once('Contract.php');
include_once('DateElement.php');
include_once('TransferInfo.php');
include_once('Paxes.php');
include_once('TransferLocation.php');
include_once('TravelInfo.php');
include_once('ArrivalTravelInfo.php');
include_once('DepartureTravelInfo.php');

class ServiceTransfer extends HotelBedsRQElement {

  /**
   * Params
   */
  public $xsi__type = 'ServiceTransfer';
  public $availToken;
  public $transferType;

  /**
   * Childs
   */
  public $ContractList;
  public $DateFrom;
  public $DateTo;
  public $TransferInfo;
  public $Paxes;
  public $PickupLocation;
  public $DestinationLocation;
  public $TravelInfo;
  public $ArrivalTravelInfo;
  public $DepartureTravelInfo;

  public function __construct($availToken, $transferType, $ContractList, $DateFrom, $TransferInfo, $Paxes, $PickupLocation, $DestinationLocation, $TravelInfo, $ArrivalTravelInfo, $DepartureTravelInfo) {
    $this->availToken           = $availToken;
    $this->transferType         = $transferType;
    $this->ContractList         = $ContractList;
    $this->DateFrom             = $DateFrom;
    $this->TransferInfo         = $TransferInfo;
    $this->Paxes                = $Paxes;
    $this->PickupLocation       = $PickupLocation;
    $this->DestinationLocation  = $DestinationLocation;
    $this->TravelInfo           = $TravelInfo;
    $this->ArrivalTravelInfo    = $ArrivalTravelInfo;
    $this->DepartureTravelInfo  = $DepartureTravelInfo;
  }

  public function getRQElementName() {
    return 'Service';
  }

  public function getRQElementParams() {
    return array('availToken', 'transferType', 'xsi__type');
  }

  public function getRQElementChilds() {
    return array('ContractList', 'DateFrom', 'TransferInfo', 'Paxes', 'PickupLocation', 'DestinationLocation', 'ArrivalTravelInfo', 'DepartureTravelInfo'/*, 'TravelInfo'*/);
  }

}

?>