<?php
namespace HotelBeds;

include_once('Holder.php');
include_once('ElementsList.php');
include_once('ServiceData.php');

class ConfirmationData extends HotelBedsRQElement {

  /**
   * Params
   */
  public $purchaseToken;

  /**
   * Childs
   */
  public $Holder;
  public $AgencyReference;
  public $ConfirmationServiceDataList;

  public function __construct($purchaseToken, $Holder, $agencyReference, $ConfirmationServiceDataList) {
    $this->purchaseToken = $purchaseToken;
    $this->Holder = $Holder;
    $this->AgencyReference = $agencyReference;
    $this->ConfirmationServiceDataList = $ConfirmationServiceDataList;
  }

  public function getRQElementParams() {
    return array('purchaseToken');
  }

  public function getRQElementChilds() {
    return array('Holder', 'AgencyReference', 'ConfirmationServiceDataList');
  }

}

?>