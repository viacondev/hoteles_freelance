<?php
namespace HotelBeds;

include_once('ServiceTicketRS.php');

class TicketAvailRS extends \BookingTicketAvailList {

  private $elements;
  private $attributes;
  private $paginationData;
  public $tickets;

  public function __construct(HotelBedsRS $elements) {
    $this->elements = $elements;

    $this->paginationData = $this->elements->getResponse()->xpath('//ns1:PaginationData');
    if (isset($this->paginationData[0])) {
      $this->paginationData = $this->paginationData[0]->attributes();
    }

    $this->tickets = array();
    $tickets = $this->elements->getResponse()->xpath('//ns1:ServiceTicket');
    foreach ($tickets as $ticket) {
      $this->tickets[] = new ServiceTicketRS($ticket);
    }
  }

  public function getCurrentPage() {
    return strval($this->paginationData['currentPage']);
  }

  public function getTotalPages() {
    return strval($this->paginationData['totalPages']);
  }

  public function getTotalItems() {
    $attrs = $this->elements->attributes();
    return strval($attrs['totalItems']);
  }

  public function getTicketsAvail() {
    return $this->tickets;
  }

}

?>
