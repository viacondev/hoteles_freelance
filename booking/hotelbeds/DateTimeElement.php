<?php
namespace HotelBeds;

class DateTimeElement extends HotelBedsRQElement {

  /**
   * Params
   */
  public $date;
  public $time;
  public $elementName;

  public function __construct($date, $time, $elementName = 'DateTime') {
    $this->date = $date;
    $this->time = $time;
    $this->elementName = $elementName;
  }

  public function getRQElementParams() {
    return array('date', 'time');
  }

  public function getRQElementName() {
    return $this->elementName;
  }

}

?>