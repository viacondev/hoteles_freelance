<?php
namespace HotelBeds;

include_once('Customer.php');

class Occupancy extends HotelBedsRQElement {

  /**
   * Childs
   */
  public $AdultCount;
  public $ChildCount;
  public $GuestList;

  public function __construct($AdultCount = 2, $ChildCount = 0) {
    $this->AdultCount = $AdultCount;
    $this->ChildCount = $ChildCount;
  }

  public function getRQElementChilds() {
    return array('AdultCount', 'ChildCount', 'GuestList');
  }

  public function AddCustomer($type, $name, $lastname, $age = NULL) {
    $customer = new Customer($type, $name, $lastname, $age);
    if (!isset($this->GuestList)) {
      $this->GuestList = new ElementsList('GuestList');
    }

    $this->GuestList->addItem($customer);
  }

}

?>