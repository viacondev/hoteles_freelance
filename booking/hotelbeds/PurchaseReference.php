<?php
namespace HotelBeds;

include_once('IncomingOffice.php');

class PurchaseReference extends HotelBedsRQElement {

  /**
   * Childs
   */
  public $FileNumber;
  public $IncomingOffice;

  public function __construct($fileNumber, $IncomingOffice) {
    $this->FileNumber = $fileNumber;
    $this->IncomingOffice = $IncomingOffice;
  }

  public function getRQElementChilds() {
    return array('FileNumber', 'IncomingOffice');
  }

}

?>