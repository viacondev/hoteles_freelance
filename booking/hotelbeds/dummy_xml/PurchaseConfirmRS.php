<?php

$response_xml = '<?xml version="1.0" encoding="utf-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body><ns1:purchaseConfirm xsi:type="xsd:string" xmlns:ns1="http://axis.frontend.hydra.hotelbeds.com"><PurchaseConfirmRS xsi:schemaLocation="http://www.hotelbeds.com/schemas/2005/06/messages PurchaseConfirmRS.xsd" echoToken="DummyEchoToken">

<AuditData>
<ProcessTime>4952</ProcessTime>
<Timestamp>2012-03-05 14:54:02.025</Timestamp>
<RequestHost>194.224.22.9</RequestHost>
<ServerName>FORM</ServerName>
<ServerId>FO</ServerId>
<SchemaRelease>2005/06</SchemaRelease>
<HydraCoreRelease>2.0.201201041744</HydraCoreRelease>
<HydraEnumerationsRelease>1.0.201201041744</HydraEnumerationsRelease>
<MerlinRelease>N/A</MerlinRelease>
</AuditData>

<Purchase purchaseToken="FO025050986" timeToExpiration="1799994">

<Reference>
<FileNumber>983863</FileNumber>
<IncomingOffice code="1"/>
</Reference>
<Status>BOOKING</Status>

<Agency>
<Code>81331</Code>
<Branch>1</Branch>
</Agency>
<Language>ENG</Language>
<CreationDate date="20120305"/>
<CreationUser>XXXX</CreationUser>

<Holder type="AD">
<Age>0</Age>
<Name>TESTA</Name>
<LastName>TESTA</LastName>
</Holder>
<AgencyReference>TEST AGENCYREF</AgencyReference>

<ServiceList>

<Service xsi:type="ServiceHotel" SPUI="1#H#1">

<Reference>
<FileNumber>983863-H1</FileNumber>
<IncomingOffice code="1"/>
</Reference>
<Status>CONFIRMED</Status>

<ContractList>

<Contract>
<Name>CG-TODOS</Name>
<IncomingOffice code="1"/>
</Contract>
</ContractList>
<Supplier name="HOTELBEDS PRODUCT,S.L.U." vatNumber="B38877676"/>

<CommentList>
<Comment type="SERVICE">
</Comment>
</CommentList>
<DateFrom date="20120505"/>
<DateTo date="20120507"/>
<Currency code="EUR">Euro</Currency>
<TotalAmount>34.660</TotalAmount>

<DiscountList>

<Price unitCount="2" paxCount="2">
<Amount>-8.660</Amount>
<DateTimeFrom date="20120505"/>
<DateTimeTo date="20120507"/>
<Description>EARLY BOOKING </Description>
</Price>
</DiscountList>

<AdditionalCostList>

<AdditionalCost type="AG_COMMISSION">

<Price>
<Amount>0.000</Amount>
</Price>
</AdditionalCost>

<AdditionalCost type="COMMISSION_VAT">

<Price>
<Amount>0.000</Amount>
</Price>
</AdditionalCost>
</AdditionalCostList>

<ModificationPolicyList>
<ModificationPolicy>Cancellation</ModificationPolicy>
<ModificationPolicy>Confirmation</ModificationPolicy>
<ModificationPolicy>Modification</ModificationPolicy>
</ModificationPolicyList>

<HotelInfo xsi:type="ProductHotel">
<Code>8468</Code>
<Name>Deya</Name>
<Category type="SIMPLE" code="2LL">2 KEYS</Category>

<Destination type="SIMPLE" code="PMI">
<Name>Majorca</Name>

<ZoneList>
<Zone type="SIMPLE" code="31">Santa Ponsa</Zone>
</ZoneList>
</Destination>
</HotelInfo>

<AvailableRoom>

<HotelOccupancy>
<RoomCount>1</RoomCount>

<Occupancy>
<AdultCount>2</AdultCount>
<ChildCount>0</ChildCount>

<GuestList>

<Customer type="AD">
<CustomerId>2</CustomerId>
<Age>45</Age>
<Name>TestB</Name>
<LastName>TestB</LastName>
</Customer>

<Customer type="AD">
<CustomerId>1</CustomerId>
<Age>45</Age>
<Name>TestA</Name>
<LastName>TestA</LastName>
</Customer>
</GuestList>
</Occupancy>
</HotelOccupancy>

<HotelRoom SHRUI="96KNnzjStxKzIGrdULiTQQ==" availCount="1" status="CONFIRMED">
<Board type="SIMPLE" code="SC-E10">SELF CATERING</Board>
<RoomType type="SIMPLE" code="APT-E10" characteristic="1B-ST">APARTMENT ONE BEDROOM-STANDARD</RoomType>

<Price>
<Amount>43.320</Amount>
</Price>

<CancellationPolicy>

<Price>
<Amount>17.330</Amount>
<DateTimeFrom date="20120503" time="2359"/>
<DateTimeTo date="20120505"/>
</Price>
</CancellationPolicy>

<HotelRoomExtraInfo>

<ExtendedData>
<Name>INFO_ROOM_AGENCY_BOOKING_STATUS</Name>
<Value>O</Value>
</ExtendedData>

<ExtendedData>
<Name>INFO_ROOM_INCOMING_BOOKING_STATUS</Name>
<Value>O</Value>
</ExtendedData>
</HotelRoomExtraInfo>
</HotelRoom>
</AvailableRoom>
</Service>
</ServiceList>
<Currency code="EUR"/>

<PaymentData>
<PaymentType code="P"/>

<Description>
The total amount for this pro-forma invoice should be made in full to Hotelbeds S.L.U., Bank: BBVA(Plaza del Olivar, s/n - CP 07002 - Palma de Mallorca - BALEARES) Account:ES73 0182-4899-14-0200712789,  SWIFT:BBVAESMMXXX,  7 days prior to clients arrival (except group bookings with fixed days in advance at the time of the confirmation) . Please indicate our reference number when making payment. Thank you for your cooperation.
</Description>
</PaymentData>
<TotalPrice>34.660</TotalPrice>
</Purchase>
</PurchaseConfirmRS></ns1:purchaseConfirm></soapenv:Body></soapenv:Envelope>';
?>