<?php
namespace HotelBeds;

class AvailableModalityRS extends \BookingAvailableModality {

  private $element;
  // private $priceList;
  // private $operationDateList;

  public function __construct($element, $factorFee = 0.1) {
    $this->element = $element;

    $this->priceList = array();
    foreach ($element->PriceList->Price as $price) {
      $commissionedPrice = round(floatval($price->Amount), 2);
      $this->priceList[] = new \BookingPrice($commissionedPrice, strval($price->Description), $factorFee);
    }

    $this->operationDateList = array();
    if (isset($element->OperationDateList)) {
      foreach ($element->OperationDateList->OperationDate as $operationDate) {
        $attrs = $operationDate->attributes();
        $date = strval($attrs['date']);
        $minimumDuration = strval($attrs['minimumDuration']);
        $maximumDuration = strval($attrs['maximumDuration']);
        $this->operationDateList[] = new \BookingOperationDate($date, $minimumDuration, $maximumDuration);
      }
    }
  }

  public function getCode() {
    $attrs = $this->element->attributes();
    return strval($attrs['code']);
  }

  public function getName() {
    return strval($this->element->Name);
  }

  public function getContractName() {
    return strval($this->element->Contract->Name);
  }

  public function getContractIncomingOfficeCode() {
    $attrs = $this->element->Contract->IncomingOffice->attributes();
    return strval($attrs['code']);
  }

  public function getType() {
    return strval($this->element->Type);
  }

  public function getTypeCode() {
    $attrs = isset($this->element->Type) ? $this->element->Type->attributes() : '';
    return ($attrs != '') ? strval($attrs['code']) : '';
  }

  public function getMode() {
    return strval($this->element->Mode);
  }

  public function getModeCode() {
    $attrs = isset($this->element->Mode) ? $this->element->Mode->attributes() : '';
    return $attrs != '' ? strval($attrs['code']) : '';
  }

  public function getChildAgeFrom() {
    $attrs = isset($this->element->ChildAge) ? $this->element->ChildAge->attributes() : '';
    return $attrs != '' ? strval($attrs['ageFrom']) : '';
  }

  public function getChildAgeTo() {
    $attrs = isset($this->element->ChildAge) ? $this->element->ChildAge->attributes() : '';
    return $attrs != '' ? strval($attrs['ageTo']) : '';
  }

  public function getContentSequence() {
    return strval($this->element->ContentSequence);
  }

  public function getPriceList() {
    return $this->priceList;
  }

  public function getOperationDateList() {
    return $this->operationDateList;
  }

}

?>
