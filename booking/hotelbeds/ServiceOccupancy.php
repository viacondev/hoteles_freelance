<?php
namespace HotelBeds;

class ServiceOccupancy extends HotelBedsRQElement {

  /**
   * Childs
   */
  public $AdultCount;
  public $ChildCount;
  public $GuestList;

  public function __construct($AdultCount = 2, $ChildCount = 0, $GuestList = NULL) {
    $this->AdultCount = $AdultCount;
    $this->ChildCount = $ChildCount;
    $this->GuestList = $GuestList;
  }

  public function getRQElementChilds() {
    return array('AdultCount', 'ChildCount', 'GuestList');
  }

}

?>