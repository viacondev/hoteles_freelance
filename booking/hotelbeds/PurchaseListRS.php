<?php
namespace HotelBeds;

class PurchaseListRS extends \BookingPurchaseList {

  private $elements;
  private $purchases;

  public function __construct(HotelBedsRS $elements) {
    $this->elements = $elements;
    $this->purchases = array();

    $purchaseList = $this->elements->getResponse()->xpath('//ns1:Purchase');

    for ($i = 0; $i < count($purchaseList); $i++) {
      $this->purchases[] = new PurchaseRS($elements, $i);
    }
  }

  public function getCurrentPage() {
  }

  public function getTotalPages() {
  }

  public function getTotalItems() {
  }

  public function getPurchaseList() {
    return $this->purchases;
  }

}

?>