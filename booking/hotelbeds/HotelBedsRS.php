<?php
namespace HotelBeds;

class HotelBedsRS {

  private $response;

  public function __construct($xml, $solicitud = "", $type ="") {
    $xml = htmlspecialchars_decode($xml);
    $this->response = simplexml_load_string($xml);
    $this->response->registerXPathNamespace('ns1', 'http://www.hotelbeds.com/schemas/2005/06/messages');
    if($solicitud == 'XML_RS') {
      $descrip = $type;
      \Log::__log('INFO', $descrip, $xml);
    }
    //\Log::debug($xml, 'XML RS');
    // __logtxt($xml);
  }

  public function getResponse() {
    return $this->response;
  }

  public function attributes() {
    $attributes = $this->response->xpath('//ns1:AuditData/..');
    return $attributes[0]->attributes();
  }

}

?>
