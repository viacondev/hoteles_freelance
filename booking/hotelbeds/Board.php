<?php
namespace HotelBeds;

class Board extends HotelBedsRQElement {

  /**
   * Params
   */
  public $code;
  public $type;

  public function __construct($code, $type) {
    $this->code = $code;
    $this->type = $type;
  }

  public function getRQElementParams() {
    return array('code', 'type');
  }

  public function getRQElementChilds() {
    return array();
  }

}

?>