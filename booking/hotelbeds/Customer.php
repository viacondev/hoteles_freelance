<?php
namespace HotelBeds;

class Customer extends HotelBedsRQElement {

  /**
   * Params
   */
  public $type;

  /**
   * Childs
   */
  public $CustomerId;
  public $Age;
  public $Name;
  public $LastName;

  public function __construct($type, $name = NULL, $lastName = NULL, $age = NULL, $customerId = NULL) {
    $this->type = $type;
    $this->CustomerId = $customerId;
    $this->Age = $age;
    $this->Name = $name;
    $this->LastName = $lastName;
  }

  public function getRQElementParams() {
    return array('type');
  }

  public function getRQElementChilds() {
    return array('CustomerId', 'Age', 'Name', 'LastName');
  }

}

?>