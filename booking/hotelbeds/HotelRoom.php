<?php
namespace HotelBeds;

include_once('Board.php');
include_once('RoomType.php');

class HotelRoom extends HotelBedsRQElement {

  /**
   * Params
   */
  public $SHRUI;
  public $onRequest;

  /**
   * Childs
   */
  public $Board;
  public $RoomType;

  public function __construct($SHRUI, $onRequest, $Board = NULL, $RoomType = NULL) {
    $this->SHRUI = $SHRUI;
    $this->onRequest = $onRequest;
    $this->Board = $Board;
    $this->RoomType = $RoomType;
  }

  public function getRQElementParams() {
    return array('SHRUI', 'onRequest');
  }

  public function getRQElementChilds() {
    return array('Board', 'RoomType');
  }

}

?>