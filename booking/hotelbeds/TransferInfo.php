<?php
namespace HotelBeds;

include_once('CodeElement.php');

class TransferInfo extends HotelBedsRQElement {

  /**
   * Params
   */
  public $xsi__type = 'ProductTransfer';

  /**
   * Childs
   */
  public $Code;
  public $Type;
  public $VehicleType;

  public function __construct($code, $typeCode, $vehicleTypeCode) {
    $this->Code = $code;
    $this->Type = new CodeElement($typeCode, 'Type');
    $this->VehicleType = new CodeElement($vehicleTypeCode, 'VehicleType');
  }

  public function getRQElementParams() {
    return array('xsi__type');
  }

  public function getRQElementChilds() {
    return array('Code', 'Type', 'VehicleType');
  }

}

?>