<?php
namespace HotelBeds;

include_once('config/config.php');
include_once('config/database.php');
include_once('HotelBedsRQ.php');
include_once('HotelBedsRS.php');
include_once('model/Model.php');
include_once('HotelBedsRQElement.php');
include_once('HotelValuedAvailRQ.php');
include_once('HotelValuedAvailRS.php');
include_once('ServiceAddRQ.php');
include_once('PurchaseRS.php');
include_once('PurchaseConfirmRQ.php');
include_once('PurchaseDetailRQ.php');
include_once('PurchaseListRQ.php');
include_once('PurchaseListRS.php');
include_once('TicketAvailRQ.php');
include_once('TicketAvailRS.php');
include_once('TicketValuationRQ.php');
include_once('TransferValuedAvailRQ.php');
include_once('TransferValuedAvailRS.php');
include_once('HotelDetailRQ.php');
include_once('HotelDetailRS.php');
include_once('ServiceRemoveRQ.php');
include_once('PurchaseFlushRQ.php');
include_once('PurchaseCancelRQ.php');
//include_once('model/ServiceStatus.php');
class HotelBedsBooking {

  private $request;

  public function __construct() {
    $this->request = new HotelBedsRQ();
  }

  public function execRequest($params) {
    switch ($params['requestType']) {
      case \BookingRequestTypes::SearchHotelAvail:
        return $this->execHotelValuedAvailRQ($params);
        break;

      case \BookingRequestTypes::AddHotelService:
        return $this->execHotelServiceAddRQ($params);
        break;

      case \BookingRequestTypes::ConfirmPurchase:
        return $this->execPurchaseConfirmRQ($params);
        break;

      case \BookingRequestTypes::PurchaseDetail:
        return $this->execPurchaseDetailRQ($params);
        break;

      case \BookingRequestTypes::PurchaseList:
        return $this->execPurchaseListRQ($params);
        break;

      case \BookingRequestTypes::SearchTicketAvail:
        return $this->execTicketAvailRQ($params);
        break;

      case \BookingRequestTypes::TicketValuation:
        return $this->execTicketValuationRQ($params);
        break;

      case \BookingRequestTypes::AddTicketService:
        return $this->execAddTicketServiceRQ($params);
        break;

      case \BookingRequestTypes::SearchTransferAvail:
        return $this->execTransferValuedAvailRQ($params);
        break;

      case \BookingRequestTypes::AddTransferService:
        return $this->execAddTransferServiceRQ($params);
        break;

      case \BookingRequestTypes::HotelDetail:
        return $this->execHotelDetailRQ($params);
        break;

      case \BookingRequestTypes::RemoveService:
        return $this->execServiceRemoveRQ($params);
        break;

      case \BookingRequestTypes::FlushPurchase:
        return $this->execPurchaseFlushRQ($params);
        break;

      case \BookingRequestTypes::CancelPurchase:
        return $this->execPurchaseCancelRQ($params);
        break;

      default:
        return NULL;
        break;
    }
  }

  public function execHotelValuedAvailRQ($p) {
    $CheckinDate = new DateElement($p['checkin'], 'CheckInDate');
    $CheckOutDate = new DateElement($p['checkout'], 'CheckOutDate');
    $roomCount = intval($p['roomCount']);
    $occupancies = array();
    for ($i = 1; $i <= $roomCount; $i++) {
      $adultCount = $p['adultCount_' . $i];
      $childCount = $p['childCount_' . $i];
      // If there is a room with the same Occupancy then only increment the room count
      // instead of creating a new Occupancy
      $occupancyFound = false;
      foreach ($occupancies as $occupancy) {
        if ($occupancy->Occupancy->AdultCount == $adultCount && $occupancy->Occupancy->ChildCount == $childCount) {
          $occupancy->RoomCount++;
          $occupancyFound = true;
          break;
        }
      }
      if (!$occupancyFound) {
        $occupancies[] = new HotelOccupancy(1, $adultCount, $childCount);
      }
    }
    // Set child ages
    $currentChild = 1;
    foreach ($occupancies as $occupancy) {
      for ($i = 0; $i < $occupancy->Occupancy->AdultCount * $occupancy->RoomCount; $i++) {
        $occupancy->Occupancy->AddCustomer('AD', '', '', 30);
      }
      for ($i = 0; $i < $occupancy->Occupancy->ChildCount * $occupancy->RoomCount; $i++) {
        $occupancy->Occupancy->AddCustomer('CH', '', '', $p['childAge_' . $currentChild]);
        $currentChild++;
      }
    }

    // Search for Hotels by name
    $HotelCodeList = NULL;
    $ProductCode = array();
    if (isset($p['hotelName']) && $p['hotelName'] != '') {
      $hotels = array();
      $hotel = str_replace(" ", ',', $p['hotelName']);
      $hotelNames = explode(',', $hotel);
      // $hotelNames = explode(',', $p['hotelName']);
      foreach ($hotelNames as $hotelName) {
        $hotels = array_merge($hotels, HotelInfoModel::findHotelsByCode(trim($hotelName), $p['destCode']));
      }

      if (count($hotels) == 0) {
        return new HotelValuedAvailRS(NULL);
      }

      foreach ($hotels as $hotel) {
        $ProductCode[] = $hotel->getCode();
      }
    }

    if(isset($p['stars']) && $p['stars'] != ''){
      $star =  $p['stars'];
      $code = array();
      for ($i = 0; $i < count($star); $i++) {
        $code = array_merge($code, HotelInfoModel::findHotelByStar($star[$i], $p['destCode'], trim($p['hotelName'])));
      }
      if (count($code) != 0) {
        foreach ($code as $value) {
          if(!in_array($value->getCode(), $ProductCode)){
            $ProductCode[] = $value->getCode();
          }
        }
      }

    }

    if(count($ProductCode) != 0)
      $HotelCodeList = new HotelCodeList($ProductCode);

    // Search specific hotel
    if (isset($p['hotelCode']) && $p['hotelCode'] != '') {
      $ProductCode[] = $p['hotelCode'];
      $HotelCodeList = new HotelCodeList($ProductCode);
    }

    // Search only on a give Zone
    $ZoneList = NULL;
    if (isset($p['zoneCode']) && $p['zoneCode'] != '') {
      $Zone = new Zone($p['zoneCode'], 'SIMPLE');
      $ZoneList = new ElementsList('ZoneList', array($Zone));
    }
    $Destination = new Destination($p['destCode'], $p['destType'], $ZoneList);

    $OccupancyList = new ElementsList('OccupancyList', $occupancies);
    // $PaginationData = new PaginationData($p['page']);
    $PaginationData = new PaginationData(1);

    // Order by price
    $ExtraParamList = NULL;
    if (isset($p['orderPrice']) && $p['orderPrice'] != '') {
      $ExtendedData = new ExtendedData('EXT_ORDER', 'ORDER_CONTRACT_PRICE', $p['orderPrice']);
      $ExtraParamList = new ElementsList('ExtraParamList', array($ExtendedData));
    }

    $hotelRQ = new HotelValuedAvailRQ($Destination, $CheckinDate, $CheckOutDate, $OccupancyList, $PaginationData, $HotelCodeList, $ExtraParamList);

    $response_xml = $this->request->execRequest($hotelRQ);

    if ($response_xml == "") {
        return "";
    }

    #include('dummy_xml/HotelValuedAvailRS2.php');
    $response_elements = new HotelBedsRS($response_xml);
    $regimen = '';
    if (isset($p['regimen']) && $p['regimen'] != '') {
      $regimen = explode(',', implode(',', $p['regimen']));
    }
    $profiling = new \Profiling('hotelAvail read', 'HotelbedsBooking');
    $profiling->init();
    $response = new HotelValuedAvailRS($response_elements, $regimen, $roomCount, $p['factorFee'], $p['extraFee']);
    $profiling->end(strlen(json_encode($response)));
    return $response;
  }

  public function prepararConsulta($p) {
    $cant_search = array('HAV','VRA','SMA','CCO','LAR','CIE','STG');
    if (in_array($p['destCode'], $cant_search)) {
      return array();
    }
    
    $CheckinDate = new DateElement($p['checkin'], 'CheckInDate');
    $CheckOutDate = new DateElement($p['checkout'], 'CheckOutDate');
    $roomCount = intval($p['roomCount']);
    $occupancies = array();
    for ($i = 1; $i <= $roomCount; $i++) {
      $adultCount = $p['adultCount_' . $i];
      $childCount = $p['childCount_' . $i];
      // If there is a room with the same Occupancy then only increment the room count
      // instead of creating a new Occupancy
      $occupancyFound = false;
      foreach ($occupancies as $occupancy) {
        if ($occupancy->Occupancy->AdultCount == $adultCount && $occupancy->Occupancy->ChildCount == $childCount) {
          $occupancy->RoomCount++;
          $occupancyFound = true;
          break;
        }
      }
      if (!$occupancyFound) {
        $occupancies[] = new HotelOccupancy(1, $adultCount, $childCount);
      }
    }
    // Set child ages
    $currentChild = 1;
    foreach ($occupancies as $occupancy) {
      for ($i = 0; $i < $occupancy->Occupancy->AdultCount * $occupancy->RoomCount; $i++) {
        $occupancy->Occupancy->AddCustomer('AD', '', '', 30);
      }
      for ($i = 0; $i < $occupancy->Occupancy->ChildCount * $occupancy->RoomCount; $i++) {
        $occupancy->Occupancy->AddCustomer('CH', '', '', $p['childAge_' . $currentChild]);
        $currentChild++;
      }
    }

    // Search for Hotels by name
    $HotelCodeList = NULL;
    $ProductCode = array();
    // if (isset($p['hotelName']) && $p['hotelName'] != '') {
      // $hotels = array();
      // $hotel = str_replace(" ", ',', $p['hotelName']);
      // $hotelNames = explode(',', $hotel);
      // // $hotelNames = explode(',', $p['hotelName']);
      // foreach ($hotelNames as $hotelName) {
      //   $hotels = array_merge($hotels, HotelInfoModel::findHotelsByCode(trim($hotelName), $p['destCode']));
      // }

      // if (count($hotels) == 0) {
      //   return NULL;
      // }

      // foreach ($hotels as $hotel) {
      //   $ProductCode[] = $hotel->getCode();
      // }
    // }

    // if(isset($p['stars']) && $p['stars'] != '') {
    //   $star =  $p['stars'];
    //   $code = array();
    //   for ($i = 0; $i < count($star); $i++) {
    //     $code = array_merge($code, HotelInfoModel::findHotelByStar($star[$i], $p['destCode'], trim($p['hotelName'])));
    //   }
    //   if (count($code) != 0) {
    //     foreach ($code as $value) {
    //       if(!in_array($value->getCode(), $ProductCode)){
    //         $ProductCode[] = $value->getCode();
    //       }
    //     }
    //   }
    // }

    if(count($ProductCode) != 0)
      $HotelCodeList = new HotelCodeList($ProductCode);

    // Search specific hotel
    if (isset($p['hotelCode']) && $p['hotelCode'] != '') {
      $ProductCode = array();
      $ProductCode[] = $p['hotelCode'];
      $HotelCodeList = new HotelCodeList($ProductCode);
    }

    // Search only on a give Zone
    $ZoneList = NULL;
    if (isset($p['zoneCode']) && $p['zoneCode'] != '') {
      $Zone = new Zone($p['zoneCode'], 'SIMPLE');
      $ZoneList = new ElementsList('ZoneList', array($Zone));
    }
    $Destination = new Destination($p['destCode'], $p['destType'], $ZoneList);

    $OccupancyList = new ElementsList('OccupancyList', $occupancies);
    // $PaginationData = new PaginationData($p['page']);
    $PaginationData = new PaginationData(1);

    // Order by price
    $ExtraParamList = NULL;
    if (isset($p['orderPrice']) && $p['orderPrice'] != '') {
      $ExtendedData = new ExtendedData('EXT_ORDER', 'ORDER_CONTRACT_PRICE', $p['orderPrice']);
      $ExtraParamList = new ElementsList('ExtraParamList', array($ExtendedData));
    }

    $hotelRQ = new HotelValuedAvailRQ($Destination, $CheckinDate, $CheckOutDate, $OccupancyList, $PaginationData, $HotelCodeList, $ExtraParamList);

    return $this->request->allOptions($hotelRQ);
  }

  public function obtenerResultado($response_xml, $p) {
    if ($response_xml == "") {
        return NULL;
    }
    $roomCount = intval($p['roomCount']);
    #include('dummy_xml/HotelValuedAvailRS2.php');
    $response_elements = new HotelBedsRS($response_xml);
    $regimen = '';
    // if (isset($p['regimen']) && $p['regimen'] != '') {
    //   $regimen = explode(',', implode(',', $p['regimen']));
    // }

    $response = new HotelValuedAvailRS($response_elements, $regimen, $roomCount, $p['factorFee'], $p['extraFee']);

    return $response;
  }

  public function execHotelServiceAddRQ($p) {
    $AvailableRooms = array();
    $currentChild = 0;
    foreach ($p['rooms'] as $room) {
      $room = (array) $room;
      $HotelOccupancy = new HotelOccupancy($room['roomCount'], $room['adultCount'], $room['childCount']);
      for ($i = 0; $i < intval($room['adultCount']) * $room['roomCount']; $i++) {
        $HotelOccupancy->Occupancy->AddCustomer('AD', '', '', 30);
      }
      for ($i = 0; $i < intval($room['childCount']) * $room['roomCount']; $i++) {
        $HotelOccupancy->Occupancy->AddCustomer('CH', '', '', $p['childAges'][$currentChild]);
        $currentChild++;
      }
      $Board      = new Board($room['boardCode'], $room['boardType']);
      $RoomType   = new RoomType($room['roomTypeCode'], $room['roomTypeType'], $room['roomTypeCharacteristic']);
      $HotelRoom  = new HotelRoom($room['SHRUI'], $room['onRequest'], $Board, $RoomType);
      $AvailableRooms[] = new AvailableRoom($HotelOccupancy, $HotelRoom);
    }

    $Contract     = new Contract($p['contractName'], $p['contractIncomingOfficeCode']);
    $HotelInfo    = new HotelInfo($p['hotelCode'], $p['destCode'], $p['destType']);
    $ServiceHotel = new ServiceHotel($p['availToken'], $Contract, $p['dateFrom'], $p['dateTo'], $HotelInfo, $AvailableRooms);
    $purchaseToken = isset($p['purchaseToken']) ? $p['purchaseToken'] : NULL;
    $ServiceAddRQ = new ServiceAddRQ($ServiceHotel, $purchaseToken);

    $response_xml = $this->request->execRequest($ServiceAddRQ, 'XML_RQ', 'BB_HB_RQ_ServiceAdd');
    #include('dummy_xml/ServiceAddRS3.php');
    $response_elements = new HotelBedsRS($response_xml, 'XML_RS', 'BB_HB_RS_ServiceAdd');
    $response = new PurchaseRS($response_elements);

    return $response;
  }

  public function execPurchaseConfirmRQ($p) {
    $Holder = new Holder($p['holderType'], $p['holderName'], $p['holderLastName']);

    $services_count = intval($p['servicesCount']);
    $services = array();
    for ($i = 1; $i <= $services_count; $i++) {
      // $item = \ShoppingCart::getItem($p["scartId_$i"]);

      $book_params = (array)json_decode($p['book_params']);
      if ($book_params['provider'] != \BookingProvider::HOTELBEDS) { continue; }
      $purchaseToken = $book_params['purchaseToken'];
      $customer_count = $p['customerCount_' . $i];
      $customers = array();
      for ($j = 1; $j <= $customer_count; $j++) {
        $customers[] = new Customer($p['customerType_' . $i . '_' .$j],
                          $p['customerName_' . $i . '_' .$j],
                          $p['customerLastName_' . $i . '_' .$j],
                          $p['customerAge_' . $i . '_' .$j],
                          $p['CustomerId_' . $i . '_' .$j]);
      }

      $CustomerList = new ElementsList('CustomerList', $customers);

      if (isset($p['serviceDetailCount_' . $i]) && $p['serviceDetailCount_' . $i] > 0) {
        $services_detail = array();
        $detail_count = $p['serviceDetailCount_' . $i];
        for ($j = 1; $j <= $detail_count; $j++) {
          $detail_name = $p['serviceDetailName_' . $i . '_' . $j];
          $detail_code = $p['serviceDetailCode_' . $i . '_' . $j];
          $detail_desc = $p['serviceDetail_' . $i . '_' . $j];
          $services_detail[] = new ServiceDetail($detail_code, $detail_name, $detail_desc);
        }
      }

      $serviceTypes = array(
        'ServiceHotel' => 'ConfirmationServiceDataHotel',
        'ServiceTicket' => 'ConfirmationServiceDataTicket',
        'ServiceTransfer' => 'ConfirmationServiceDataTransfer'
      );
      $serviceType = $serviceTypes[$p['serviceType_' . $i]];

      // Extra params for services
      $CommentList = NULL;
      if ($p['serviceType_' . $i] = 'ServiceHotel' && isset($p['comments_' . $i]) && $p['comments_' . $i] != '') {
        $CommentList = new ElementsList('CommentList', array(new Comment('INCOMING', $p['comments_' . $i])));
      }

      $services[] = new ServiceData($serviceType, $p['SPUI_' . $i], $CustomerList, $CommentList);
    }

    // Check if a reservation was done or if all items are from another Provider.
    if (count($services) == 0) return NULL;

    $ConfirmationServiceDataList = new ElementsList('ConfirmationServiceDataList', $services);
    $ConfirmationData = new ConfirmationData($purchaseToken, $Holder, $p['agencyReference'], $ConfirmationServiceDataList);
    $PurchaseConfirmRQ = new PurchaseConfirmRQ($ConfirmationData);

    $response_xml = $this->request->execRequest($PurchaseConfirmRQ, 'XML_RQ', 'BB_HB_RQ_CONFIRM');
 
    #include('dummy_xml/PurchaseConfirmRS1.php');
    $response_elements = new HotelBedsRS($response_xml, 'XML_RS', 'BB_HB_RS_CONFIRM');
    $response = new PurchaseRS($response_elements);

    return $response;
  }

  public function execPurchaseDetailRQ($p) {
    $purchaseToken = isset($p['purchaseToken']) ? $p['purchaseToken'] : NULL;
    $PurchaseReference = NULL;
    if (isset($p['fileNumber']) && $p['incomingOffice']) {
      $IncomingOffice = new IncomingOffice($p['incomingOffice']);
      $PurchaseReference = new PurchaseReference($p['fileNumber'], $IncomingOffice);
    }

    $PurchaseDetailRQ = new PurchaseDetailRQ($purchaseToken, $PurchaseReference);

    $response_xml = $this->request->execRequest($PurchaseDetailRQ);
    #include('dummy_xml/PurchaseConfirmRS1.php');
    $response_elements = new HotelBedsRS($response_xml, 'XML RS');
    $factorFee = (isset($p['factorFee'])) ? $p['factorFee'] : 0.1;
    $response = new PurchaseRS($response_elements, 0, $factorFee);

    return $response;
  }

  public function execPurchaseListRQ($p) {
    $PaginationData = new PaginationData($p['page'], 999);
    $DateFrom = new DateElement($p['dateFrom'], 'DateFrom');
    $DateTo = new DateElement($p['dateTo'], 'DateTo');
    $IncludeCancelled = $p['includeCancelled'];
    $FilterType = $p['filterType'];

    $PurchaseListRQ = new PurchaseListRQ($PaginationData, $DateFrom, $DateTo, $IncludeCancelled, $FilterType);

    $response_xml = $this->request->execRequest($PurchaseListRQ);

    $response_elements = new HotelBedsRS($response_xml);
    // $response = new PurchaseListRS($response_elements);
    $rs = $response_elements->getResponse()->xpath('//ns1:Purchase');
    $book_list_arr = array();
    foreach ($rs as $value) {
      if (isset($value->Status)) {
        $attr = $value->Status;
        $status = ($attr == "BOOKING") ? \ServiceStatus::CONFIRMED : $attr;
      }

      $fileNumber     = $value->Reference->FileNumber;
      $incomingOffice = $value->Reference->IncomingOffice->attributes();
      $localizer      = $incomingOffice . '-' . $fileNumber;
      $holder         = $value->Holder->AdditionalInfo->ExtendedData->Value;
      $book_list_arr[] = array('localizer' => $localizer, 'holder' => array($holder), 'status_provider' => $status);
    }

    return $book_list_arr;
  }

  public function execTicketAvailRQ($p) {
    $PaginationData = new PaginationData($p['page']);
    $Destination = new Destination($p['destCode'], $p['destType']);
    $DateFrom = new DateElement($p['dateFrom'], 'DateFrom');
    $DateTo = new DateElement($p['dateTo'], 'DateTo');
    $GuestList = NULL;
    $guests = array();
    for ($i = 1; $i <= intval($p['adultCount']); $i++) {
      $guests[] = new Customer('AD', NULL, NULL, 30, NULL);
    }
    for ($i = 1; $i <= intval($p['childCount']); $i++) {
      $guests[] = new Customer('CH', NULL, NULL, $p['childAge_' . $i], NULL);
    }
    $GuestList = new ElementsList('GuestList', $guests);
    $ServiceOccupancy = new ServiceOccupancy($p['adultCount'], $p['childCount'], $GuestList);

    // Order by price
    $ExtraParamList = NULL;

    $TicketAvailRQ = new TicketAvailRQ($PaginationData, $ServiceOccupancy, $Destination, $DateFrom, $DateTo, $ExtraParamList);

    $response_xml = $this->request->execRequest($TicketAvailRQ);
    #include('dummy_xml/TicketAvailRS.php');
    $response_elements = new HotelBedsRS($response_xml);
    $response = new TicketAvailRS($response_elements);

    return $response;
  }

  public function execTicketValuationRQ($p) {
    $DateFrom = new DateElement($p['dateFrom'], 'DateFrom');
    $DateTo = new DateElement($p['dateTo'], 'DateTo');
    $GuestList = NULL;
    $guests = array();
    for ($i = 1; $i <= intval($p['adultCount']); $i++) {
      $guests[] = new Customer('AD', NULL, NULL, 30, NULL);
    }
    $childAges = (array) $p['childAges'];
    foreach ($childAges as $childAge) {
      $guests[] = new Customer('CH', NULL, NULL, $childAge, NULL);
    }
    $GuestList = new ElementsList('GuestList', $guests);
    $ServiceOccupancy = new ServiceOccupancy($p['adultCount'], $p['childCount'], $GuestList);

    $TicketValuationRQ = new TicketValuationRQ($p['availToken'], $ServiceOccupancy, $DateFrom, $DateTo, $p['modalityCode'], $p['ticketCode']);

    $response_xml = $this->request->execRequest($TicketValuationRQ);
    $response_elements = new HotelBedsRS($response_xml);
    $response = new TicketAvailRS($response_elements);
    $services = $response->getTicketsAvail();

    return $services[0];
  }

  public function execAddTicketServiceRQ($p) {
    $Contract = new Contract($p['contractName'], $p['contractIncomingOfficeCode']);
    $ContractList = new ElementsList('ContractList', array($Contract));
    $DateFrom = new DateElement($p['dateFrom'], 'DateFrom');
    $DateTo = new DateElement($p['dateTo'], 'DateTo');
    $TicketInfo = new TicketInfo($p['code'], $p['destCode'], $p['destType']);
    $AvailModalityContract = new Contract($p['availContractName'], $p['availContractIncomingOfficeCode']);
    $AvailableModality = new AvailableModality($p['availCode'], $p['availName'], $AvailModalityContract);
    $GuestList = NULL;
    $guests = array();
    for ($i = 1; $i <= intval($p['adultCount']); $i++) {
      $guests[] = new Customer('AD', NULL, NULL, 30, NULL);
    }
    $childAges = (array) $p['childAges'];
    foreach ($childAges as $childAge) {
      $guests[] = new Customer('CH', NULL, NULL, $childAge, NULL);
    }
    $GuestList = new ElementsList('GuestList', $guests);
    $Paxes = new Paxes($p['adultCount'], $p['childCount'], $GuestList);

    $ServiceTicket = new ServiceTicket($p['availToken'], $ContractList, $DateFrom, $DateTo, $TicketInfo, $AvailableModality, $Paxes);
    $purchaseToken = isset($p['purchaseToken']) ? $p['purchaseToken'] : NULL;
    $ServiceAddRQ = new ServiceAddRQ($ServiceTicket, $purchaseToken);

    $response_xml = $this->request->execRequest($ServiceAddRQ);
    $response_elements = new HotelBedsRS($response_xml);
    $response = new PurchaseRS($response_elements);

    return $response;
  }

  public function execTransferValuedAvailRQ($p) {
    $types = array(
      'H' => 'ProductTransferHotel',
      'A' => 'ProductTransferTerminal',
      'T' => 'ProductTransferTerminal',
      'P' => 'ProductTransferTerminal',
      'E' => 'ProductTransferTerminal',
      'Z' => 'ProductZone'
      );

    $AvailDataIn = NULL;
    $AvailDataOut = NULL;

    $ServiceDateIn = new DateTimeElement($p['dateIn'], $p['timeIn'], 'ServiceDate');
    $typeIn = $types[$p['typeIn']];
    $codeIn = $p['codeIn'];
    $DateTimeIn = new DateTimeElement($p['dateIn'], $p['timeIn']);
    $TransferZoneIn = new TransferZone($p['transferZoneCodeIn']);
    $LocationIn = new TransferLocation($typeIn, $codeIn, $DateTimeIn, $TransferZoneIn);

    $ServiceDateOut = new DateTimeElement($p['dateOut'], $p['timeOut'], 'ServiceDate');
    $typeOut = $types[$p['typeOut']];
    $codeOut = $p['codeOut'];
    $DateTimeOut = new DateTimeElement($p['dateOut'], $p['timeOut']);
    $TransferZoneOut = new TransferZone($p['transferZoneCodeOut']);
    $LocationOut = new TransferLocation($typeOut, $codeOut, $DateTimeOut, $TransferZoneOut);

    $Occupancy = new Occupancy($p['adultCount'], $p['childCount']);
    for ($i = 1; $i <= $p['childCount']; $i++) {
      $Occupancy->AddCustomer('CH', '', '', $p['childAge_' . $i]);
    }

    if ($typeIn == 'ProductTransferHotel') {
      // Hotel - Terminal
      $AvailDataOut = new AvailData('OUT', $ServiceDateOut, $Occupancy, $LocationIn, $LocationOut);
    }
    else if ($typeOut == 'ProductTransferHotel') {
      // Terminal - Hotel
      $AvailDataIn = new AvailData('IN', $ServiceDateIn, $Occupancy, $LocationIn, $LocationOut);

      if ($p['roundTrip']) {
        // Terminal - Hotel - Terminal
        $LocationInn = clone $LocationIn;
        $LocationInn->DateTime = clone $DateTimeOut;
        //$AvailDataOut = new AvailData('OUT', $ServiceDateOut, $Occupancy, clone $LocationOut, clone $LocationIn);
        $AvailDataOut = new AvailData('OUT', $ServiceDateOut, $Occupancy, clone $LocationOut, $LocationInn);
      }
    }
    else {
      // Terminal - Terminal - Terminal
      if ($p['roundTrip']) {
        $AvailDataIn = new AvailData('IN', $ServiceDateIn, $Occupancy, $LocationIn, $LocationOut);

        $LocationInn = clone $LocationIn;
        $LocationInn->DateTime = clone $DateTimeOut;
        //$AvailDataOut = new AvailData('OUT', $ServiceDateOut, $Occupancy, clone $LocationOut, clone $LocationIn);
        $AvailDataOut = new AvailData('OUT', $ServiceDateOut, $Occupancy, clone $LocationOut, $LocationInn);
      }
      else {
        // Terminal - Terminal
        $AvailDataOut = new AvailData('OUT', $ServiceDateOut, $Occupancy, $LocationIn, $LocationOut);
      }
    }

    $TransferValuedAvailRQ = new TransferValuedAvailRQ($AvailDataIn, $AvailDataOut);

    $response_xml = $this->request->execRequest($TransferValuedAvailRQ);
    $response_elements = new HotelBedsRS($response_xml);
    $response = new TransferValuedAvailRS($response_elements, $p['factorFee']);

    return $response;
  }

  public function execAddTransferServiceRQ($p) {
    $Contract     = new Contract($p['contractName'], $p['contractIncomingOfficeCode']);
    $ContractList = new ElementsList('ContractList', array($Contract));
    $DateFrom     = new DateTimeElement($p['dateFrom'], $p['timeFrom'], 'DateFrom');
    $TransferInfo = new TransferInfo($p['transferCode'], $p['transferTypeCode'], $p['vehicleTypeCode']);
    $GuestList    = NULL;
    if (intval($p['childCount']) > 0) {
      $childs = array();
      $childAges = (array) $p['childAges'];
      foreach ($childAges as $childAge) {
        $childs[] = new Customer('CH', NULL, NULL, $childAge, NULL);
      }
      $GuestList = new ElementsList('GuestList', $childs);
    }
    $Paxes                = new Paxes($p['adultCount'], $p['childCount'], $GuestList);
    $TransferZone         = new TransferZone(NULL);
    $PickupLocation       = new TransferLocation($p['pickupType'], $p['pickupCode'], NULL, $TransferZone, 'PickupLocation');
    $DestinationLocation  = new TransferLocation($p['destinationType'], $p['destinationCode'], NULL, $TransferZone, 'DestinationLocation');
    $DepartInfo           = NULL;
    $ArrivalInfo          = NULL;
    $DepartureTravelInfo  = "";
    $ArrivalTravelInfo    = "";
    if (isset($p['departType'])) {
      $DepartDateTime = new DateTimeElement($p['departDate'], $p['departTime']);
      $DepartInfo     = new TransferLocation($p['departType'], $p['departCode'], $DepartDateTime, NULL, 'DepartInfo');
      $ArrivalInfo    = new TransferLocation($p['departType'], 'VVI', $DepartDateTime, NULL, 'ArrivalInfo');

      $DepartureTravelInfo = new DepartureTravelInfo($DepartInfo, $ArrivalInfo, $p['travel_number']);
    }
    if (isset($p['arrivalType'])) {
      $ArrivalDateTime  = new DateTimeElement($p['arrivalDate'], $p['arrivalTime']);
      $ArrivalInfo      = new TransferLocation($p['arrivalType'], $p['arrivalCode'], $ArrivalDateTime, NULL, 'ArrivalInfo');
      $DepartInfo       = new TransferLocation($p['arrivalType'], 'VVI', $ArrivalDateTime, NULL, 'DepartInfo');

      $ArrivalTravelInfo = new ArrivalTravelInfo($DepartInfo, $ArrivalInfo, $p['travel_number']);
    }
    $TravelInfo = new TravelInfo($DepartInfo, $ArrivalInfo, $p['travel_number']);

    $ServiceTransfer  = new ServiceTransfer($p['availToken'], $p['transferType'], $ContractList, $DateFrom, $TransferInfo, $Paxes, $PickupLocation, $DestinationLocation, $TravelInfo, $ArrivalTravelInfo, $DepartureTravelInfo);
    $purchaseToken    = isset($p['purchaseToken']) ? $p['purchaseToken'] : NULL;
    $ServiceAddRQ     = new ServiceAddRQ($ServiceTransfer, $purchaseToken);

    $response_xml       = $this->request->execRequest($ServiceAddRQ);
    \Log::info($response_xml, 'BB_HB_RS_SERVICEADD');
    $response_elements  = new HotelBedsRS($response_xml);
    $response           = new PurchaseRS($response_elements);

    return $response;
  }

  function execHotelDetailRQ($p) {
    $HotelDetailRQ = new HotelDetailRQ($p['hotelCode']);

    $response_xml = $this->request->execRequest($HotelDetailRQ);
    #include('dummy_xml/HotelDetail1.php');
    $response_elements = new HotelBedsRS($response_xml);
    $response = new HotelDetailRS($response_elements);

    return $response;
  }

  function execServiceRemoveRQ($p) {
    $ServiceRemoveRQ = new ServiceRemoveRQ($p['purchaseToken'], $p['SPUI']);

    $response_xml = $this->request->execRequest($ServiceRemoveRQ);
    $response_elements = new HotelBedsRS($response_xml);
    $response = new PurchaseRS($response_elements);

    return $response;
  }

  function execPurchaseFlushRQ($p) {
    $PurchaseFlushRQ = new PurchaseFlushRQ($p['purchaseToken']);

    $response_xml = $this->request->execRequest($PurchaseFlushRQ);
    $response_elements = new HotelBedsRS($response_xml);
  }

  function execPurchaseCancelRQ($p) {
    $IncomingOffice = new IncomingOffice($p['incomingOffice']);
    $PurchaseReference = new PurchaseReference($p['fileNumber'], $IncomingOffice);

    $PurchaseCancelRQ = new PurchaseCancelRQ($PurchaseReference);

    $response_xml = $this->request->execRequest($PurchaseCancelRQ, 'XML_RQ', 'BB_HB_RQ_CANCEL');
    $response_elements = new HotelBedsRS($response_xml, 'XML_RS', 'BB_HB_RS_CANCEL');
    $response = new PurchaseRS($response_elements);

    return $response;
  }
}

?>
