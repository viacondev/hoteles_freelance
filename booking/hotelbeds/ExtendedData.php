<?php
namespace HotelBeds;

class ExtendedData extends HotelBedsRQElement {

  /**
   * Params
   */
  public $type;

  /**
   * Childs
   */
  public $Name;
  public $Value;

  public function __construct($type, $Name, $Value) {
    $this->type = $type;
    $this->Name = $Name;
    $this->Value = $Value;
  }

  public function getRQElementParams() {
    return array('type');
  }

  public function getRQElementChilds() {
    return array('Name', 'Value');
  }

}

?>