<?php
namespace HotelBeds;

class AvailableRoomRS extends \RoomAvail {

  private $element;
  public $priceWithFee;
  public $hotelCode;
  public $hotel;
  public $factorFee;
  public function __construct($availableRoomElement, $hotel, $factorFee = 1, $extraFee = 0, $hotelCode = 0) {
    $this->element    = $availableRoomElement;
    $this->hotelCode  = $hotelCode;
    $this->hotel      = $hotel;
    $this->factorFee  = $factorFee;
    $this->hotel      = json_encode($this->buildObjecArray());
    $this->priceWithFee = floatval($this->element->HotelRoom->Price->Amount)/ $factorFee;
    if ($extraFee != 0) {
      $this->priceWithFee += $this->priceWithFee * ($extraFee / 100);
    }
  }

  public function getProvider() {
    return \BookingProvider::HOTELBEDS;
  }

  public function getHotel() {
    return $this->hotel;
  }

  public function getProviderName() {
    return 'HOTELBEDS';
  }

  public function getHotelCode() {
    return strval($this->hotelCode);
  }

  public function getRoomCount() {
    return strval($this->element->HotelOccupancy->RoomCount);
  }

  public function getAdultCount() {
    return strval($this->element->HotelOccupancy->Occupancy->AdultCount);
  }

  public function getChildCount() {
    return strval($this->element->HotelOccupancy->Occupancy->ChildCount);
  }

  public function getChildAges() {
    return array();
  }
  
  public function getPrice() {
    return round(floatval($this->element->HotelRoom->Price->Amount), 2);
  }

  public function getPriceWithFee() {
    return round($this->priceWithFee, 2);
  }

  public function getSHRUI() {
    $attrs = $this->element->HotelRoom->attributes();
    return strval($attrs['SHRUI']);
  }

  public function getOnRequest() {
    $attrs = $this->element->HotelRoom->attributes();
    return strval($attrs['onRequest']);
  }

  public function getBoard() {
    return strval($this->element->HotelRoom->Board);
  }

  public function getBoardCode() {
    $attrs = $this->element->HotelRoom->Board->attributes();
    return strval($attrs['code']);
  }

  public function getBoardType() {
    $attrs = $this->element->HotelRoom->Board->attributes();
    return strval($attrs['type']);
  }

  public function getRoomType() {
    return strval($this->element->HotelRoom->RoomType);
  }

  public function getRoomTypeType() {
    $attrs = $this->element->HotelRoom->RoomType->attributes();
    return strval($attrs['type']);
  }

  public function getRoomTypeCode() {
    $attrs = $this->element->HotelRoom->RoomType->attributes();
    return strval($attrs['code']);
  }

  public function getRoomTypeCharacteristic() {
    $attrs = $this->element->HotelRoom->RoomType->attributes();
    return strval($attrs['characteristic']);
  }

  public function getCancellationPolicies() {
    $policies_arr = array();
    if ($this->element->HotelRoom->CancellationPolicies) {
      foreach ($this->element->HotelRoom->CancellationPolicies->CancellationPolicy as $policy) {
        $attrs    = $policy->attributes();
        // $amount   = $attrs['amount'];
        $amount   = intval($attrs['amount']) !== 0 ? $attrs['amount'] : floatval($this->element->HotelRoom->Price->Amount);
        $dateFrom = new \DateTime(dateFromYmdToDB($attrs['dateFrom']));
        $time     = substr($attrs['time'], 0, 2) . ':' . substr($attrs['time'], 2, 2);
        $cancellation = new \BookingCancellationPolicy();
        $cancellation->build($amount, $dateFrom, $time, true, $this->factorFee);
        $policies_arr[] = $cancellation;
      }
    }

    return $policies_arr;
  }

  public function getGuestList() {
    $guests = array();
    if (!isset($this->element->HotelOccupancy->Occupancy->GuestList)) {
      return $guests;
    }
    foreach ($this->element->HotelOccupancy->Occupancy->GuestList->Customer as $customer) {
      $attrs      = $customer->attributes();
      $type       = $attrs['type'];
      $customerId = strval($customer->CustomerId);
      $age        = strval($customer->Age);
      $name       = isset($customer->Name) ? strval($customer->Name) : NULL;
      $lastName   = isset($customer->LastName) ? strval($customer->LastName) : NULL;

      $guests[]   = new \BookingCustomer($type, $customerId, $age, $name, $lastName);
    }

    return $guests;
  }

  public function buildObjecArray() {
    $hotel = $this->hotel;
    $methods = get_class_methods($hotel);
    $objArray = array();
    foreach ($methods as $method) {
      if (substr($method, 0, 2) == '__') continue;
      if (substr($method, 0, 3) === 'get'&& $method != 'getGroupedRooms' && $method != 'getRoomsAvail') {
        $key = lcfirst(substr($method, 3));
        $objArray[$key] = $hotel->$method();
      }
    }
    return $objArray;
  }
}

?>
