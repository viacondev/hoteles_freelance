<?php
namespace HotelBeds;

include_once('ServiceHotelRS.php');

class HotelValuedAvailRS extends \HotelAvailList {

  private $elements;
  public $serviceHotels;
  private $paginationData;

  public function __construct($elements, $board_filter = '', $roomCount = 0, $factorFee = 1, $extraFee = 0.79) {
    $this->elements = $elements;

    if (isset($elements)) {
      $hotels = $this->elements->getResponse()->xpath('//ns1:ServiceHotel');
      $arr = array();
      foreach ($hotels as $value) {
        $hotelCode = strval($value->HotelInfo->Code);
        $arr[] = '"' . $hotelCode . '"';
      }
      $listCodes  = implode(',', $arr);
      $result     = array();
      if($listCodes != '') {
        $result   = HotelInfoModel::buildClassListHotelCode($listCodes);  
      }
      $arr        = array();
      foreach ($result as $hotel) {
        if(isset($hotel->HotelCode)) {
          $arr[$hotel->HotelCode] = $hotel;
        }
      }

      $this->paginationData = $this->elements->getResponse()->xpath('//ns1:PaginationData');
      if ($this->paginationData) {
        $this->paginationData = $this->paginationData[0]->attributes();
      }

      $this->serviceHotels = array();
      $arrayInsertedCodes = array();
      $con = 0;
      foreach ($hotels as $hotel) {
        $hotel_code = strval($value->HotelInfo->Code);
        $hotInfo = isset($arr[$hotel_code]) ? $arr[$hotel_code] : NULL;
        if ($hotInfo == NULL) {
          $hotInfo = new HotelInfoModel();
          $hotInfo->HotelFacilities = "M&aacute;s informaci&oacute;n de los hoteles dentro de poco tiempo...";
        }
        $availableroom = new ServiceHotelRS($hotel, $board_filter, $hotInfo, $roomCount, $factorFee, $extraFee);
        /*if(count($availableroom->getGroupedRooms()) > 0) {
          $this->serviceHotels[] = $availableroom;
          $arrayInsertedCodes[$hCode] = $con;
          $con++;
        }*/
        $hCode = $availableroom->getCode();
        if(count($availableroom->getGroupedRooms()) > 0 && !array_key_exists($hCode, $arrayInsertedCodes)) {
          $this->serviceHotels[] = $availableroom;
          $arrayInsertedCodes[$hCode] = $con;
          $con++;
        }
        else if (count($availableroom->getGroupedRooms()) > 0) {
          $index = $arrayInsertedCodes[$hCode]; 
          foreach ($availableroom->getGroupedRooms() as $group) {
              $this->serviceHotels[$index]->insertGroupedRoom($group);
          }
          // $this->serviceHotels[$index]->insertGroupedRoom($availableroom->getRoomsAvail());
        }
      }
    }
    else {
      $this->serviceHotels = array();
      $this->paginationData = array('currentPage' => 0, 'totalPages' => 0);
    }
  }

  public function getHotelsAvail() {
    return $this->serviceHotels;
  }

  public function getCurrentPage() {
    return strval($this->paginationData['currentPage']);
  }

  public function getTotalPages() {
    return strval($this->paginationData['totalPages']);
  }
}

?>
