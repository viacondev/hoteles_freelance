<?php
namespace HotelBeds;

class IncomingOffice extends HotelBedsRQElement {

  /**
   * Params
   */
  public $code;

  public function __construct($code) {
    $this->code = $code;
  }

  public function getRQElementParams() {
    return array('code');
  }

  public function getRQElementChilds() {
    return array();
  }

}

?>