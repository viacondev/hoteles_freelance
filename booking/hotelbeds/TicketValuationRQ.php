<?php
namespace HotelBeds;

include_once('ServiceOccupancy.php');
include_once('DateElement.php');

class TicketValuationRQ extends HotelBedsRQElement {

  private $methodName = 'getTicketValuation';

  /**
   * Params
   */
  public $echoToken = 'DummyEchoToken';

  /**
   * Childs
   */
  public $AvailToken;
  public $ServiceOccupancy;
  public $DateFrom;
  public $DateTo;
  public $ModalityCode;
  public $TicketCode;

  public function __construct($AvailToken, $ServiceOccupancy, $DateFrom, $DateTo, $ModalityCode, $TicketCode) {
    $this->AvailToken = $AvailToken;
    $this->ServiceOccupancy = $ServiceOccupancy;
    $this->DateFrom = $DateFrom;
    $this->DateTo = $DateTo;
    $this->ModalityCode = $ModalityCode;
    $this->TicketCode = $TicketCode;
  }

  public function isRequestClass() {
    return true;
  }

  public function getRQElementMethodName() {
    return $this->methodName;
  }

  public function getRQElementParams() {
    return array('echoToken');
  }

  public function getRQElementChilds() {
    return array('AvailToken', 'ServiceOccupancy', 'DateFrom', 'DateTo', 'ModalityCode', 'TicketCode');
  }
}
?>