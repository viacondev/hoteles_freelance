<?php
namespace HotelBeds;

class HotelDetailRS extends \BookingHotelDetail {

  private $elements;
  private $hotel;
  // private $buildingFacilities;
  // private $hotelTypeFacility;
  // private $credCardsFacilities;
  // private $roomFacilities;
  // private $servicesFacilities;
  // private $cateringFacilities;
  // private $businessFacilities;
  // private $healthBeautyFacilities;
  // private $entertainmentFacilities;
  // private $distancesFacilities;
  // private $highLightFacilities;

  public function __construct(HotelBedsRS $elements) {
    $this->elements = $elements;
    $this->hotel = $this->elements->getResponse()->xpath('//ns1:Hotel');
    $this->hotel = $this->hotel[0];
    $this->buildFacilitiesList();
  }

  public function buildFacilitiesList() {
    $this->buildingFacilities = array();
    $this->hotelTypeFacility = array();
    $this->credCardsFacilities = array();
    $this->roomFacilities = array();
    $this->servicesFacilities = array();
    $this->cateringFacilities = array();
    $this->businessFacilities = array();
    $this->healthBeautyFacilities = array();
    $this->entertainmentFacilities = array();
    $this->distancesFacilities = array();
    $this->highLightFacilities = array();

    if (isset($this->hotel->FacilityList->Feature)) {
      foreach ($this->hotel->FacilityList->Feature as $feature) {
        $attrs = $feature->attributes();
        $code = strval($feature->Code);
        $value = strval($feature->Description);
        if (isset($feature->Value)) {
          $value .= ': ' . strval($feature->Value);
        }
        if (isset($attrs['fee']) && $attrs['fee'] == 'Y') {
          $value .= '*';
        }

        if ($attrs['group'] == '10') { $this->buildingFacilities[$code] = $value; }
        if ($attrs['group'] == '20') { $this->hotelTypeFacility[$code] = $value; }
        if ($attrs['group'] == '30') { $this->credCardsFacilities[$code] = $value; }
        if ($attrs['group'] == '60') { $this->roomFacilities[$code] = $value; }
        if ($attrs['group'] == '70') { $this->servicesFacilities[$code] = $value; }
        if ($attrs['group'] == '71') { $this->cateringFacilities[$code] = $value; }
        if ($attrs['group'] == '72') { $this->businessFacilities[$code] = $value; }
        if ($attrs['group'] == '73') { $this->entertainmentFacilities[$code] = $value; }
        if ($attrs['group'] == '74') { $this->healthBeautyFacilities[$code] = $value; }

        if ($attrs['group'] == '40' && isset($feature->DistanceList)) {
          $d_attrs = $feature->DistanceList->Distance[0]->attributes();
          $distance = strval($feature->DistanceList->Distance[0]);
          $distance .= ' ' . $d_attrs->unit;
          $this->distancesFacilities[$value] = $distance;
        }
        if ($attrs['group'] == '100' && isset($feature->DistanceList)) {
          $d_attrs = $feature->DistanceList->Distance[0]->attributes();
          $distance = strval($feature->DistanceList->Distance[0]);
          $distance .= ' ' . $d_attrs->unit;
          $value = strval($feature->Remark);
          $this->highLightFacilities[$value] = $distance;
        }
      }
    }
  }

  public function getCode() {
    return strval($this->hotel->Code);
  }

  public function getName() {
    return strval($this->hotel->Name);
  }

  public function getDescription() {
    return strval($this->hotel->DescriptionList->Description[0]);
  }

  public function getImageList() {
    $images = array();
    if (isset($this->hotel->ImageList->Image)) {
      foreach ($this->hotel->ImageList->Image as $image) {
        $images[] = strval($image->Url);
      }
    }

    return $images;
  }

  public function getAddressStreetName() {
    return strval($this->hotel->Contact->Address->StreetName);
  }
  public function getAddressNumber() {
    return strval($this->hotel->Contact->Address->Number);
  }

  public function getAddressPostalCode() {
    return strval($this->hotel->Contact->Address->PostalCode);
  }

  public function getEmailList() {
    $mails = array();
    if (isset($this->hotel->Contact->EmailList)) {
      foreach ($this->hotel->Contact->EmailList->Email as $mail) {
        $mails[] = strval($mail);
      }
    }

    return $mails;
  }

  public function getPhoneList() {
    $arr = array();
    if (isset($this->hotel->Contact->PhoneList)) {
      foreach ($this->hotel->Contact->PhoneList->ContactNumber as $value) {
        $arr[] = strval($value);
      }
    }

    return $arr;
  }

  public function getFaxList() {
    $arr = array();
    if (isset($this->hotel->Contact->FaxList)) {
      foreach ($this->hotel->Contact->FaxList->ContactNumber as $value) {
        $arr[] = strval($value);
      }
    }

    return $arr;
  }

  public function getWebList() {
    $arr = array();
    if (isset($this->hotel->Contact->WebList)) {
      foreach ($this->hotel->Contact->WebList->Web as $value) {
        $arr[] = strval($value);
      }
    }

    return $arr;
  }

  public function getLocation() {
    return '';
  }

  public function getLatitude() {
    $attrs = $this->hotel->Position->attributes();
    return strval($attrs['latitude']);
  }

  public function getLongitude() {
    $attrs = $this->hotel->Position->attributes();
    return strval($attrs['longitude']);
  }

  public function getDestinationCode() {
    $attrs = $this->hotel->Destination->attributes();
    return strval($attrs['code']);
  }

  public function getDestinationName() {
    return strval($this->hotel->Destination->Name);
  }

  public function getDestinationZone() {
    return strval($this->hotel->Destination->ZoneList->Zone[0]);
  }

  public function getCategory() {
    return strval($this->hotel->Category);
  }

  public function getCategoryCode() {
    $attrs = $this->hotel->Category->attributes();
    return strval($attrs['code']);
  }

  public function getBuildingFacilities() {
    return $this->buildingFacilities;
  }

  public function getHotelTypeFacilities() {
    return $this->hotelTypeFacility;
  }

  public function getCredCardsFacilities() {
    return $this->credCardsFacilities;
  }

  public function getRoomFacilities() {
    return $this->roomFacilities;
  }

  public function getServicesFacilities() {
    return $this->servicesFacilities;
  }

  public function getCateringFacilities() {
    return $this->cateringFacilities;
  }

  public function getBusinessFacilities() {
    return $this->businessFacilities;
  }

  public function getHealthBeautyFacilities() {
    return $this->healthBeautyFacilities;
  }

  public function getEntertainmentFacilities() {
    return $this->entertainmentFacilities;
  }

  public function getDistancesFacilities() {
    return $this->distancesFacilities;
  }

  public function getHighLightFacilities() {
    return $this->highLightFacilities;
  }

}

?>
