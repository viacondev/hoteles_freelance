<?php
namespace HotelBeds;

include_once('IncomingOffice.php');

class Contract extends HotelBedsRQElement {

  /**
   * Childs
   */
  public $Name;
  public $IncomingOffice;

  public function __construct($name, $incomingOfficeCode) {
    $this->Name = $name;
    $this->IncomingOffice = new IncomingOffice($incomingOfficeCode);
  }

  public function getRQElementParams() {
    return array();
  }

  public function getRQElementChilds() {
    return array('Name', 'IncomingOffice');
  }

}

?>