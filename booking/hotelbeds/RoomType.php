<?php
namespace HotelBeds;

class RoomType extends HotelBedsRQElement {

  /**
   * Params
   */  
  public $code;
  public $type;
  public $characteristic;

  public function __construct($code, $type, $characteristic) {
    $this->code = $code;
    $this->type = $type;
    $this->characteristic = $characteristic;
  }

  public function getRQElementParams() {
    return array('code', 'type', 'characteristic');
  }

  public function getRQElementChilds() {
    return array();
  }

}

?>