<?php
namespace HotelBeds;

include_once('ConfirmationData.php');

class PurchaseConfirmRQ extends HotelBedsRQElement {

  private $methodName = 'purchaseConfirm';

  /**
   * Params
   */
  public $echoToken = 'DummyEchoToken';

  /**
   * Childs
   */
  public $ConfirmationData;

  public function __construct($ConfirmationData) {
    $this->ConfirmationData = $ConfirmationData;
  }

  public function isRequestClass() {
    return true;
  }

  public function getRQElementMethodName() {
    return $this->methodName;
  }

  public function getRQElementParams() {
    return array('echoToken');
  }

  public function getRQElementChilds() {
    return array('ConfirmationData');
  }
}
?>