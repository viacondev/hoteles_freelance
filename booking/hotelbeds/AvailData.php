<?php
namespace HotelBeds;

include_once('DateTimeElement.php');
include_once('Occupancy.php');
include_once('TransferLocation.php');

class AvailData extends HotelBedsRQElement {

  /**
   * Params
   */
  public $type;

  /**
   * Childs
   */
  public $ServiceDate;
  public $Occupancy;
  public $PickupLocation;
  public $DestinationLocation;

  public function __construct($type, $ServiceDate, $Occupancy, $PickupLocation, $DestinationLocation) {
    $this->type = $type;
    $this->ServiceDate = $ServiceDate;
    $this->Occupancy = $Occupancy;
    $this->PickupLocation = $PickupLocation;
    $this->PickupLocation->setClassName('PickupLocation');
    $this->DestinationLocation = $DestinationLocation;
    $this->DestinationLocation->setClassName('DestinationLocation');
  }

  public function getRQElementParams() {
    return array('type');
  }

  public function getRQElementChilds() {
    return array('ServiceDate', 'Occupancy', 'PickupLocation', 'DestinationLocation');
  }

}

?>