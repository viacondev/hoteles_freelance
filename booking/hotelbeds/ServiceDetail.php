<?php
namespace HotelBeds;

class ServiceDetail extends HotelBedsRQElement {

  /**
   * Params
   */
  public $code;

  /**
   * Childs
   */
  public $Name;
  public $Description;

  public function __construct($code, $Name, $Description) {
    $this->code = $code;
    $this->Name = $Name;
    $this->Description = $Description;
  }

  public function getRQElementParams() {
    return array('code');
  }

  public function getRQElementChilds() {
    return array('Name', 'Description');
  }

}

?>