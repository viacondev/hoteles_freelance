<?php
namespace HotelBeds;

class HotelOccupancy extends HotelBedsRQElement {

  /**
   * Childs
   */
  public $RoomCount;
  public $Occupancy;

  public function __construct($RoomCount = 1, $AdultCount = 2, $ChildCount = 0) {
    $this->RoomCount = $RoomCount;
    $this->Occupancy = new Occupancy($AdultCount, $ChildCount);
  }

  public function getRQElementChilds() {
    return array('RoomCount', 'Occupancy');
  }

}

?>