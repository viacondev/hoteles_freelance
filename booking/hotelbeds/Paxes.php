<?php
namespace HotelBeds;

include_once('Customer.php');

class Paxes extends HotelBedsRQElement {

  /**
   * Params
   */
  public $xsi__type;

  /**
   * Childs
   */
  public $AdultCount;
  public $ChildCount;
  public $GuestList;

  public function __construct($AdultCount = 2, $ChildCount = 0, $GuestList = NULL, $xsi__type = 'ServiceOccupancy') {
    $this->xsi__type = $xsi__type;
    $this->AdultCount = $AdultCount;
    $this->ChildCount = $ChildCount;
    $this->GuestList = $GuestList;
  }

  public function getRQElementParams() {
    return array('xsi__type');
  }

  public function getRQElementChilds() {
    return array('AdultCount', 'ChildCount', 'GuestList');
  }
}

?>