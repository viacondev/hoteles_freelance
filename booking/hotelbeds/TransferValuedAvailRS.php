<?php
namespace HotelBeds;

include_once('ServiceTransferRS.php');

class TransferValuedAvailRS extends \BookingTransferAvailList {

  private $elements;
  private $transfers;
  private $transfersIn;
  private $transfersOut;

  public function __construct(HotelBedsRS $elements, $factorFee = 0.1) {
    $this->elements = $elements;

    $this->transfers = array();
    $this->transfersIn = array();
    $this->transfersOut = array();
    $transfers = $this->elements->getResponse()->xpath('//ns1:ServiceTransfer');

    foreach ($transfers as $transfer) {
      $serviceTransfer = new ServiceTransferRS($transfer, $factorFee, "TransferValuedAvailRS");

      $this->transfers[] = $serviceTransfer;
      if ($serviceTransfer->getTransferType() == 'IN') {
        $this->transfersIn[] = $serviceTransfer;
      }
      else if ($serviceTransfer->getTransferType() == 'OUT') {
        $this->transfersOut[] = $serviceTransfer;
      }
    }
  }

  public function getTransferType() {
    if (count($this->transfersIn) > 0 && count($this->transfersOut) > 0) {
      return 'IN_OUT';
    }
    if (count($this->transfersIn) > 0) {
      return 'IN';
    }
    return 'OUT';
  }

  public function getTransfersAvail() {
    return $this->transfers;
  }

  public function getTransfersAvailIn() {
    return $this->transfersIn;
  }

  public function getTransfersAvailOut() {
    return $this->transfersOut;
  }

  // public function getOutTransferForInTransfer($transferIn) {
  //   $bestMatch = NULL;
  //   $level1 = true;
  //
  //   // TODO: Improve matching algorithm. In ROME (ROE) Transfer has different contract names
  //   // for IN and OUT even on the same transfers.
  //   foreach ($this->transfersOut as $transferOut) {
  //     if ($transferIn->getContractName() == $transferOut->getContractName()) {
  //       if ($level1) $bestMatch = $transferOut;
  //
  //       if ($transferIn->getTransferInfoVehicleTypeCode() == $transferOut->getTransferInfoVehicleTypeCode()) {
  //         $level1 = false;
  //         $bestMatch = $transferOut;
  //
  //         if ($transferIn->getTotalAmount() == $transferOut->getTotalAmount()) {
  //           $bestMatch = $transferOut;
  //           break;
  //         }
  //       }
  //     }
  //   }
  //
  //   if ($bestMatch == NULL && count($this->transfersOut) > 0) {
  //     $bestMatch = $this->transfersOut[0];
  //   }
  //
  //   return $bestMatch;
  // }
}

?>
