<?php
namespace HotelBeds;

include_once('TransferLocation.php');

class ArrivalTravelInfo extends HotelBedsRQElement {

  /**
   * Childs
   */
  public $DepartInfo;
  public $ArrivalInfo;
  public $TravelNumber;

  public function __construct($DepartInfo, $ArrivalInfo, $TravelNumber = '000') {
    $this->DepartInfo = $DepartInfo;
    $this->ArrivalInfo = $ArrivalInfo;
    $this->TravelNumber = $TravelNumber;
  }

  public function getRQElementChilds() {
    return array('DepartInfo', 'ArrivalInfo', 'TravelNumber');
  }

}

?>