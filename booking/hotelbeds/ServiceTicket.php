<?php
namespace HotelBeds;

include_once('Contract.php');
include_once('DateElement.php');
include_once('TicketInfo.php');
include_once('AvailableModality.php');
include_once('Paxes.php');

class ServiceTicket extends HotelBedsRQElement {

  /**
   * Params
   */
  public $availToken;
  public $xsi__type = 'ServiceTicket';

  /**
   * Childs
   */
  public $ContractList;
  public $DateFrom;
  public $DateTo;
  public $TicketInfo;
  public $AvailableModality;
  public $Paxes;

  public function __construct($availToken, $ContractList, $DateFrom, $DateTo, $TicketInfo, $AvailableModality, $Paxes) {
    $this->availToken = $availToken;
    $this->ContractList = $ContractList;
    $this->DateFrom = $DateFrom;
    $this->DateTo = $DateTo;
    $this->TicketInfo = $TicketInfo;
    $this->AvailableModality = $AvailableModality;
    $this->Paxes = $Paxes;
  }

  public function getRQElementName() {
    return 'Service';
  }

  public function getRQElementParams() {
    return array('availToken', 'xsi__type');
  }

  public function getRQElementChilds() {
    return array('ContractList', 'DateFrom', 'DateTo', 'TicketInfo', 'AvailableModality', 'Paxes');
  }

}

?>