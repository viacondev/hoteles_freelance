<?php
namespace HotelBeds;

include_once('ServiceRS.php');

class PurchaseRS extends \BookingPurchase {

  private $elements;
  private $Purchase;
  public $Services;

  public function __construct(HotelBedsRS $elements, $index = 0, $factorFee = 0.1) {
    $this->elements = $elements;
    $this->Purchase = $this->elements->getResponse()->xpath('//ns1:Purchase');
    $this->Services = array();
    $error_list = $this->elements->getResponse()->xpath('//ns1:ErrorList');
    $arr_errors = array();
    if(count($error_list) > 0) {
      $response_error = $error_list[0];
      foreach ($response_error as $error) {
        $code = strval($error->Code);
        $msg  = strval($error->DetailedMessage);
        $provider = \BookingProvider::HOTELBEDS;
        $arr_errors[] = array('code' => $code, 'msg' => $msg,'provider' => $provider);
      }
      $this->Services[] = new ServiceRS(array(), '', $arr_errors);
    }
    // Maybe an error has ocurred
    if (!isset($this->Purchase[$index])) {
      return;
    }
    $this->Purchase = $this->Purchase[$index];

    if (isset($this->Purchase->ServiceList) && count($arr_errors) == 0) {
      foreach ($this->Purchase->ServiceList->Service as $service) {
        $localizer = '';
        if ($this->getStatus() == 'BOOKING') {
          $localizer = $this->getReferenceIncomingOffice() . '-' . $this->getReferenceFileNumber();
        }
        $this->Services[] = new ServiceRS($service, $localizer, array(), $factorFee);
      }
    }
  }

  public function getPurchaseToken() {
    $attrs = $this->Purchase->attributes();
    return strval($attrs['purchaseToken']);
  }

  public function getStatus() {
    if (isset($this->Purchase) && isset($this->Purchase->Status)) {
      return strval($this->Purchase->Status);
    }
    return 'NOT_FOUND';
  }

  public function getHolder() {
    $attrs = $this->Purchase->Holder;
    $type = $attrs['type'];
    $name = strval($this->Purchase->Holder->Name);
    $lastname = strval($this->Purchase->Holder->LastName);
    $age = strval($this->Purchase->Holder->Age);
    return new \BookingCustomer($type, NULL, $age, $name, $lastname);
  }

  public function getBookingServices() {
    return $this->Services;
  }

  public function getTotalPrice() {
    return round(floatval($this->Purchase->TotalPrice), 2);
  }

  public function getCurrency() {
    $attrs = $this->Purchase->Currency->attributes();
    return strval($attrs['code']);
  }

  public function getCreationDate() {
    $attrs = $this->Purchase->CreationDate->attributes();
    return strval($attrs['date']);
  }

  public function getReferenceFileNumber() {
    return strval($this->Purchase->Reference->FileNumber);
  }

  public function getReferenceIncomingOffice() {
    $attrs = $this->Purchase->Reference->IncomingOffice->attributes();
    return strval($attrs['code']);
  }

  public function getAgencyReference() {
    return strval($this->Purchase->AgencyReference);
  }

  public function getPaymentDataDescription() {
    return strval($this->Purchase->PaymentData->Description);
  }
}

?>
