<?php
namespace HotelBeds;

include_once('ConfirmationData.php');

class PurchaseListRQ extends HotelBedsRQElement {

  private $methodName = 'getPurchaseList';

  /**
   * Params
   */
  public $echoToken = 'DummyEchoToken';
  public $sessionId = 'DummySessionId';

  /**
   * Childs
   */
  public $PaginationData;
  public $DateFrom;
  public $DateTo;
  public $IncludeCancelled;
  // C = by creation date; E = by checkin date
  public $FilterType;

  public function __construct($PaginationData, $DateFrom, $DateTo, $IncludeCancelled, $FilterType) {
    $this->PaginationData = $PaginationData;
    $this->DateFrom = $DateFrom;
    $this->DateTo = $DateTo;
    $this->IncludeCancelled = $IncludeCancelled;
    $this->FilterType = $FilterType;
  }

  public function isRequestClass() {
    return true;
  }

  public function getRQElementMethodName() {
    return $this->methodName;
  }

  public function getRQElementParams() {
    return array('echoToken', 'sessionId');
  }

  public function getRQElementChilds() {
    return array('PaginationData', 'DateFrom', 'DateTo', 'IncludeCancelled', 'FilterType');
  }
}
?>