<?php
namespace HotelBeds;

class Holder extends HotelBedsRQElement {

  /**
   * Params
   */
  public $type;

  /**
   * Childs
   */
  public $Name;
  public $LastName;

  public function __construct($type, $Name, $LastName) {
    $this->type = $type;
    $this->Name = $Name;
    $this->LastName = $LastName;
  }

  public function getRQElementParams() {
    return array('type');
  }

  public function getRQElementChilds() {
    return array('Name', 'LastName');
  }

}

?>