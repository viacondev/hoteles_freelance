<?php
namespace HotelBeds;

class DateElement extends HotelBedsRQElement {

  /**
   * Params
   */
  public $date;
  public $elementName;

  public function __construct($date, $elementName = 'Date') {
    $this->date = $date;
    $this->elementName = $elementName;
  }

  public function getRQElementParams() {
    return array('date');
  }

  public function getRQElementName() {
    return $this->elementName;
  }

}

?>