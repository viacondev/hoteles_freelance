<?php

ini_set('soap.wsdl_cache_enabled', '0');
ini_set('soap.wsdl_cache_ttl', '0');

$location = $_GET['location'];
$wsdl = $_GET['wsdl'];
$method = $_GET['method'];
$rq = $_GET['request'];

$client = new SoapClient($wsdl, array('compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP, "trace" => 0));
echo $client->__doRequest($rq, $location, $method, SOAP_1_1);

?>