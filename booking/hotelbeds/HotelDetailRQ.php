<?php
namespace HotelBeds;

include_once('ConfirmationData.php');

class HotelDetailRQ extends HotelBedsRQElement {

  private $methodName = 'getHotelDetail';

  /**
   * Params
   */
  public $echoToken = 'DummyEchoToken';

  /**
   * Childs
   */
  public $HotelCode;

  public function __construct($HotelCode) {
    $this->HotelCode = $HotelCode;
  }

  public function isRequestClass() {
    return true;
  }

  public function getRQElementMethodName() {
    return $this->methodName;
  }

  public function getRQElementParams() {
    return array('echoToken');
  }

  public function getRQElementChilds() {
    return array('HotelCode');
  }
}
?>