<?php
namespace HotelBeds;

include_once('AvailableModalityRS.php');

class ServiceTicketRS extends \BookingTicketAvail {

  private $element;
  public $availableModalityList;

  public function __construct($element, $factorFee = 0.1) {
    $this->element = $element;
    $this->availableModalityList = array();
    foreach ($element->AvailableModality as $availableModality) {
      $this->availableModalityList[] = new AvailableModalityRS($availableModality, $factorFee);
    }
  }

  public function getProvider() {
    return \BookingProvider::HOTELBEDS;
  }

  public function getAvailToken() {
    $attrs = $this->element->attributes();
    return strval($attrs['availToken']);
  }

  public function getCode() {
    return strval($this->element->TicketInfo->Code);
  }

  public function getName() {
    return strval($this->element->TicketInfo->Name);
  }

  public function getDateFrom() {
    $attrs = $this->element->DateFrom->attributes();
    return strval($attrs['date']);
  }

  public function getDateTo() {
    $attrs = $this->element->DateTo->attributes();
    return strval($attrs['date']);
  }

  public function getCurrency() {
    $currency = $this->element->Currency->attributes();
    return strval($currency['code']);
  }

  public function getDescription() {
    return strval($this->element->TicketInfo->DescriptionList->Description);
  }

  public function getMainImage() {
    if (isset($this->element->TicketInfo->ImageList)) {
      $images = $this->element->TicketInfo->ImageList->Image;
      return str_replace('/small', '', strval($images[0]->Url));
    }

    return 'assets/images/no-photo.jpg';
  }

  public function getCompanyCode() {
    return strval($this->element->TicketInfo->CompanyCode);
  }

  public function getTicketClass() {
    return strval($this->element->TicketInfo->TicketClass);
  }

  public function getDestinationCode() {
    $attrs = $this->element->TicketInfo->Destination->attributes();
    return strval($attrs['code']);
  }

  public function getDestinationType() {
    $attrs = $this->element->TicketInfo->Destination->attributes();
    return strval($attrs['type']);
  }

  public function getClassification() {
    return strval($this->element->TicketInfo->Classification);
  }

  public function getClassificationCode() {
    $attrs = isset($this->element->TicketInfo->Classification) ? $this->element->TicketInfo->Classification->attributes() : '';
    return ($attrs != '') ? strval($attrs['code']) : '';
  }

  public function getAvailableModalityList() {
    return $this->availableModalityList;
  }

  public function getAdultCount() {
    return strval($this->element->Paxes->AdultCount);
  }

  public function getChildCount() {
    return strval($this->element->Paxes->ChildCount);
  }

  public function getContractName() {
    return strval($this->element->ContractList->Contract->Name);
  }

  public function getContractIncomingOfficeCode() {
    $attrs = $this->element->ContractList->Contract->IncomingOffice;
    return strval($attrs['code']);
  }

  public function getContractComment() {
    if ($this->element->CommentList) {
      return strval($this->element->CommentList->Comment[0]);
    }
    else {
      return '';
    }
  }

  public function getCancellationPolicies() {
    $policies_arr = array();
    foreach ($this->element->CancellationPolicyList->Price as $policy) {
      $amount = $policy->Amount;
      $attrs = $policy->DateTimeFrom->attributes();
      $dateFrom = new \DateTime(dateFromYmdToDB($attrs['date']));
      $time = '00:00';
      $cancellation = new \BookingCancellationPolicy();
      $cancellation->build($amount, $dateFrom, $time, true);
      $policies_arr[] = $cancellation;
    }

    return $policies_arr;
  }

  public function getGuestList() {
    $guests = array();
    foreach ($this->element->Paxes->GuestList->Customer as $customer) {
      $attrs = $customer->attributes();
      $type = $attrs['type'];
      $customerId = strval($customer->CustomerId);
      $age = strval($customer->Age);
      $name = isset($customer->Name) ? strval($customer->Name) : NULL;
      $lastName = isset($customer->LastName) ? strval($customer->LastName) : NULL;

      $guests[] = new \BookingCustomer($type, $customerId, $age, $name, $lastName);
    }

    return $guests;
  }

  public function getServiceDetailList() {
    if (!isset($this->element->ServiceDetailList->ServiceDetail)) {
      return array();
    }
    foreach ($this->element->ServiceDetailList->ServiceDetail as $service) {
      $attrs = $service->attributes();
      $serviceDetail = new \BookingServiceDetail();
      $serviceDetail->build($attrs['code'], $service->Name);
      $services[] = $serviceDetail;
    }

    return $services;
  }

  public function getSupplier() {
    $attrs = $this->element->Supplier;
    return strval($attrs['name']);
  }

  public function getSupplierVatNumber() {
    $attrs = $this->element->Supplier;
    return strval($attrs['vatNumber']) != 'UNKNOWN' ? strval($attrs['vatNumber']) : '';
  }
}

?>
