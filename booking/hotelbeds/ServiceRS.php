<?php
namespace HotelBeds;

include_once('AvailableRoomRS.php');
include_once('ServiceHotelRS.php');

class ServiceRS extends \BookingService {

  private $element;
  private $errors;
  private $factorFee;
  public $service;
  public $totalAmountWithFee;
  // private $serviceType;

  public function __construct($serviceElement, $localizer, $errors = array(), $factorFee = 0.1) {
    $this->element = $serviceElement;
    $this->localizer = $localizer;
    $this->factorFee = $factorFee;
    $this->errors = $errors;
    if(!is_array($serviceElement) && count($serviceElement) != 0) {
      $this->totalAmountWithFee = floatval($this->element->TotalAmount) / $factorFee;
      $namespaces = $this->element->getNameSpaces(true);
      $xsi = $this->element->attributes($namespaces['xsi']);
      $this->serviceType = strval($xsi['type']);

      if ($this->serviceType == 'ServiceHotel') {
        $this->service = new ServiceHotelRS($serviceElement, '', '', 0, $factorFee, 0);
      }
      else if ($this->serviceType == 'ServiceTicket') {
        $this->service = new ServiceTicketRS($serviceElement, $factorFee);
      }
      else if ($this->serviceType == 'ServiceTransfer') {
        $this->service = new ServiceTransferRS($serviceElement, $factorFee);
      }
    }
  }

  public function getLocalizer() {
    return $this->localizer;
  }

  public function getSPUI() {
    $attrs = $this->element->attributes();
    return strval($attrs['SPUI']);
  }

  public function getServiceType() {
    return strval($this->serviceType);
  }

  public function getServiceStatus() {
    return strval($this->element->Status);
  }

  public function getDirectPayment() {
    return strval($this->element->DirectPayment);
  }

  public function getDateFrom() {
    $attrs = $this->element->DateFrom->attributes();
    return strval($attrs['date']);
  }

  public function getDateTo() {
    if (isset($this->element->DateTo)) {
      $attrs = $this->element->DateTo->attributes();
      return strval($attrs['date']);
    }
    return strval($this->getDateFrom());
  }

  public function getCurrency() {
    $attrs = $this->element->Currency->attributes();
    return strval($attrs['code']);
  }

  public function getTotalAmount() {
    return round(floatval($this->element->TotalAmount), 2);
  }

  public function getTotalAmountWithFee() {
    return round($this->totalAmountWithFee, 2);
  }

  public function getAdditionalCosts() {
    $additional_costs = array();
    foreach ($this->element->AdditionalCostList->AdditionalCost as $cost) {
      $attrs = $cost->attributes();
      $type = strval($attrs['type']);
      $additional_costs[$type] = (float) $cost->Price->Amount;
    }

    return $additional_costs;
  }

  public function getSupplementList() {
    $Supplement_List = array();
    if (isset($this->element->SupplementList)) {
      foreach ($this->element->SupplementList->Price as $Suplement) {
        $Supplement_List[] = new \BookingPrice(floatval($Suplement->Amount), strval($Suplement->Description), $this->factorFee);
      }
    }
    return $Supplement_List;
  }

  public function getServiceInfo() {
    return $this->service;
  }

  public function getStatus() {
    // TODO: Check the result of PurchaseRS and return according to that
    return \ServiceStatus::CONFIRMED;
  }

  public function getErrors() {
    return $this->errors;
  }
}

?>
