<?php
namespace HotelBeds;

class HotelBedsRQ {

  private $client;
  private $location;
  private $wsdl;

  public function __construct() {
    ini_set('soap.wsdl_cache_enabled', '0');
    ini_set('soap.wsdl_cache_ttl', '0');

    $this->location = \HBConfig::$XMLLocation;
    $this->wsdl = \HBConfig::$WSDLLocation;
    //$this->client = new \SoapClient($this->wsdl, array('compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP, "trace" => 0));
  }

  public function getRequestXML($RQElement) {
    return '<?xml version="1.0" encoding="UTF-8"?>
            <soapenv:Envelope soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
                xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
              <soapenv:Body>
                <hb:' . $RQElement->getRQElementMethodName() . ' xmlns:hb="http://axis.frontend.hydra.hotelbeds.com" xsi:type="xsd:anyType">
                  ' . $RQElement->getRQElementXML() . '
                </hb:' . $RQElement->getRQElementMethodName() . '>
              </soapenv:Body>
            </soapenv:Envelope>';
  }

  public function execRequest($RQElement, $solicitud = "", $type = "") {
    if (\HBConfig::$DirectXMLAccess) {
      // __logtxt($this->getRequestXML($RQElement));
      //\Log::debug($this->getRequestXML($RQElement), 'XML RQ');
      // $header = array('compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP);
      $header[] = 'SOAPAction: ' . $this->wsdl . $RQElement->getRQElementMethodName();
      $ch = curl_init();
      curl_setopt_array($ch, array(
                                CURLOPT_URL => $this->location,
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => "",
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "POST",
                                CURLOPT_POSTFIELDS => $this->getRequestXML($RQElement),
                                CURLOPT_HTTPHEADER => $header,
                                CURLOPT_USERAGENT => $_SERVER['HTTP_USER_AGENT'])
                      );
      $xml = curl_exec($ch);
      if($solicitud != '') {
        $descrip = $type;
        \Log::__log('INFO', $descrip, $this->getRequestXML($RQElement));
      }
      if($xml ==  "") {
          return "";
      }
      return $xml;
    }
    else {
      // Call to a remote request wrapper because our current dev IP doesn't have access to HB.
      // This should be changed due security issues.
      $rq = 'http://barrybolivia.com/booking/hotelbeds/RemoteRequestWrapper.php';
      $rq .= '?location=' . urlencode($this->location);
      $rq .= '&wsdl=' . urlencode($this->wsdl);
      $rq .= '&method=' . urlencode($RQElement->getRQElementMethodName());
      $rq .= '&request=' . urlencode($this->getRequestXML($RQElement));

      // __logtxt($this->getRequestXML($RQElement));
      if($solicitud == 'XML_RQ') {
        $descrip = $type;
        \Log::__log('INFO', $descrip, $this->getRequestXML($RQElement));
      }
      $context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));
      return file_get_contents($rq, false, $context);
    }
  }

  public function allOptions($RQElement)
  {
    if (\HBConfig::$DirectXMLAccess) {
      $header = array('compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP);
      $header[] = 'SOAPAction: ' . $this->wsdl . $RQElement->getRQElementMethodName();
      $options = array(CURLOPT_URL => $this->location,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => $this->getRequestXML($RQElement),
                        CURLOPT_HTTPHEADER => $header,
                        CURLOPT_USERAGENT => $_SERVER['HTTP_USER_AGENT']);
    } 
    else {
      $rq = 'http://barrybolivia.com/booking/hotelbeds/RemoteRequestWrapper.php';
      $rq .= '?location=' . urlencode($this->location);
      $rq .= '&wsdl=' . urlencode($this->wsdl);
      $rq .= '&method=' . urlencode($RQElement->getRQElementMethodName());
      $rq .= '&request=' . urlencode($this->getRequestXML($RQElement));
      $options  = array(CURLOPT_AUTOREFERER => true, 
                        CURLOPT_HEADER => 0, 
                        CURLOPT_RETURNTRANSFER => 1, 
                        CURLOPT_URL             => $rq);
    }
    return $options;
  }
}

?>
