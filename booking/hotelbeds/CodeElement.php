<?php
namespace HotelBeds;

class CodeElement extends HotelBedsRQElement {

  /**
   * Params
   */
  public $code;

  public $elementName;

  public function __construct($code, $elementName) {
    $this->code = $code;
    $this->elementName = $elementName;
  }

  public function getRQElementParams() {
    return array('code');
  }

  public function getRQElementName() {
    return $this->elementName;
  }

}

?>