<?php

class BookingSerializable {

  public function serialize($obj = null) {

    if ($obj == null) {
      if (!isset($this)) {
        return null;
      }

      if(is_null($obj)) {
        $obj = $this;
      }
    }

    $attrs = array();

    if (is_object($obj)) {
      // If inherts from BookingSerializable then serialize all 'get' methods
      if (BookingSerializable::objIsBookingSerializable($obj)) {
        $methods = get_class_methods($obj);
        foreach ($methods as $method) {

          if (substr($method, 0, 3) === 'get') {
            $attrName = lcfirst(substr($method, 3, strlen($method) - 3));
            $data = $obj->$method();
            // If array then serialize all of its items

            if (is_array($data)) {
              $item_arr = array();
              foreach ($data as $key => $item) {
                $item_arr[$key] = BookingSerializable::serialize($item);
              }

              $attrs[$attrName] = $item_arr;
            }
            // If object then serialize recursively (in case it is a BookingSerializable)
            else if (BookingSerializable::objIsBookingSerializable($data)) {
              if ($data != null) {

                $attrs[$attrName] = BookingSerializable::serialize($data);
              }
            }
            // Else, we assume it is a primitive type
            else {

              $attrs[$attrName] = $data;
            }
          }
        }
      }
      // Normal object, what should we do here? Are there normal objects? Maybe not right now.
      else {

        return $obj;
      }

      $attrs['__isBookingSerializable'] = BookingSerializable::objIsBookingSerializable($obj);
      $attrs['__className'] = get_class($obj);
      $attrs['__parents'] = BookingSerializable::objParents($obj);
    }
    else if (is_array($obj)) {
      $item_arr = array();
      foreach ($obj as $key => $item) {

        $item_arr[$key] = BookingSerializable::serialize($item);
      }

      $attrs = $item_arr;
    }
    else {

      $attrs = $obj;

    }
    return $attrs;
  }

  /**
   * Deserialize array to BookingSerializable object
   * @params $arr: array to deserialize. $root: true for the first call, false if recursive call (method internal only)
   */
  public static function deserialize($serializable, $root = true) {
    $arr = $serializable;
    if (gettype($serializable) == 'object' && get_class($serializable) == 'stdClass') {
      $arr = (array) $serializable;
    }

    if (BookingSerializable::arrIsBookingSerializable($arr)) {
      if (count($arr['__parents']) == 1) {
        $className = $arr['__className'];
      }
      else {
        $className = $arr['__parents'][count($arr['__parents']) - 2]; // Lastet parent before BookingSerializable
      }

      // $class = new ReflectionClass($arr['__className']);
      $class = new $className();

      foreach ($arr as $key => $value) {
        if (substr($key, 0, 2) == '__') continue; // Avoid internal items

        if (BookingSerializable::arrIsBookingSerializable($value)) {
          $class->$key = BookingSerializable::deserialize($value, false);
        }
        else if (is_array($value)) {
          $value_arr = array();
          foreach ($value as $k => $v) {
            $value_arr[$k] = BookingSerializable::deserialize($v, false);
          }
          $class->$key = $value_arr;
        }
        else {
          $class->$key = $value;
        }
      }

      return $class;
    }
    else if (!$root && is_array($arr)) {
      $item_arr = array();
      foreach ($arr as $key => $value) {
        $item_arr[$key] = BookingSerializable::deserialize($value, false);
      }
      return $item_arr;
    }
  }

  public static function objIsBookingSerializable($obj) {
    $parents = BookingSerializable::objParents($obj);
    if (end($parents) == 'BookingSerializable') {
      return true;
    }
    return false;
  }

  public static function arrIsBookingSerializable($serializable) {
    $arr = $serializable;
    if (gettype($serializable) == 'object' && get_class($serializable) == 'stdClass') {
      $arr = (array) $serializable;
    }

    return is_array($arr) && isset($arr['__isBookingSerializable']) && $arr['__isBookingSerializable'];
  }

  public static function objParents($class, $plist = array()) {
    $parent = get_parent_class($class);
    if ($parent) {
      $plist[] = $parent;
      /* Do not use $this. Use 'self' here instead, or you
       * will get an infinite loop. */
      $plist = BookingSerializable::objParents($parent, $plist);
    }
    return $plist;
  }

}

?>
