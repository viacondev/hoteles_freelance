<?php

/**
 * Simulates an enum
 */
abstract class BookingRequestTypes {
    const SearchHotelAvail = 0;
    const AddHotelService = 1;
    const ConfirmPurchase = 2;
    const PurchaseDetail = 3;
    const PurchaseList = 4;
    const SearchTicketAvail = 5;
    const TicketValuation = 6;
    const AddTicketService = 7;
    const SearchTransferAvail = 8;
    const AddTransferService = 9;
    const HotelDetail = 10;
    const RemoveService = 11;
    const FlushPurchase = 12;
    const CancelPurchase = 13;
    const PurchaseDetailList = 14;
    const SearchHotelsByDest = 15;
    const SearchZones        = 16;
}

?>
