<?php

class BookingCustomer extends BookingSerializable {

  public $type;
  public $customerId;
  public $age;
  public $name;
  public $lastName;

  public function __construct($type = NULL, $customerId = NULL, $age = NULL, $name = NULL, $lastName = NULL) {
    $this->type = strval($type);
    $this->customerId = $customerId;
    $this->age = $age;
    $this->name = $name;
    $this->lastName = $lastName;
  }

  public function getType() {
    return strval($this->type);
  }

  public function getCustomerId() {
    return strval($this->customerId);
  }

  public function getAge() {
    return strval($this->age);
  }

  public function getName() {
    return strval($this->name);
  }

  public function getLastName() {
    return strval($this->lastName);
  }

}

?>
