<?php
namespace Methabookk;

class HotelValuationRQ
{
  private $params;
  function __construct($params) {
    $this->params = $params;
  }

  function getType() {
    return "bookingCancellationPolicy";
  }

  function getMethod() {
    return 'Accommodation/v1.0/Valuation';
  }

  function getParams() {
    return $this->params;
  }
}

?>
