<?php
namespace Methabookk;

class AvailableHotelsRQ {

  public $params;
  function __construct($params = array()) {
    $this->params = $params;
  }

  function getType() {
    return "searchHotelAvail";
  }

  function getMethod() {
    if (isset($this->params['accommodationCodes'])) {
      return 'Accommodation/v1.0/Availability/Accommodation';//busqueda por hotel
    }
    return 'Accommodation/v1.0/Availability/Zone'; // busqueda por zona
  }

  function getParams() {
    return $this->params;
  }
}

?>
