<?php
namespace Methabookk;

class RoomAvail extends \RoomAvail {

  private $room;
  public $priceWithFee;
  public $hotelCode;
  public $hotel;
  public $board;
  public $price;
  public $roomCount;
  private $availabilitiCode;
  public $adultCount;
  public $childCount;
  public function __construct($hotel, $room, $board, $price, $availabilitiCode, $roomCount, $adultCount, $childCount, $hotelCode, $factorFee) {
    $this->room = $room;
    $this->hotel = $hotel;
    $this->hotel = json_encode($this->buildObjecArray());
    $this->board = $board;
    $this->price = $price;
    $this->priceWithFee = ($price / $factorFee);
    $this->roomCount = $roomCount;
    $this->availabilitiCode = $availabilitiCode;
    $this->adultCount = $adultCount;
    $this->childCount = $childCount;
    $this->hotelCode  = $hotelCode;
  }

  public function getProvider() {
    return \BookingProvider::METHABOOK2;
  }

  public function getHotel() {
    return $this->hotel;
  }

  public function getProviderName() {
    return 'METHABOOK2';
  }

  public function getHotelCode() {
    return strval($this->hotelCode);
  }

  public function getRoomCount() {
    return 1;
  }

  public function getAdultCount() {
    return $this->adultCount;
  }

  public function getChildCount() {
    return $this->childCount;
  }

  public function getChildAges() {
    return array();
  }

  public function getPrice() {

    return $this->price;
  }

  public function getPriceWithFee() {
    return round($this->priceWithFee, 2);
  }

  public function getSHRUI() {
    return $this->availabilitiCode;
  }

  public function getOnRequest() {
    return '';
  }

  public function getBoard() {
    return $this->board->name;
  }

  public function getBoardCode() {
    // $boards = array(
    //   'AI'  => 'TI', //Todo Incluido Codigo MBB=>GENERAL
    //   'BB'  => 'AD',  //Alojamiento y desayuno
    //   'OR'  => 'SA', //Solo Alojamiento
    //   'RO'  => 'SA',
    //   'NO'  => 'SA',
    //   'PC'  => 'FB', //Pension Completa
    //   'FB'  => 'FB',
    //   'MP'  => 'HB', //Media Pension
    //   'HB'  => 'HB'
    // );
    // if (isset($boards[strval($this->board->code)])) {
    //   return $boards[strval($this->board->code)];
    // }
    return $this->board->code;
  }

  public function getBoardType() {
    return '';
  }

  public function getRoomType() {
    return strval($this->room->name);
  }

  public function getRoomTypeType() {
    return strval($this->room->code);
  }

  public function getRoomTypeCode() {
    return '';
  }

  public function getRoomTypeCharacteristic() {
    return '';
  }

  public function getCancellationPolicies() {
    return array();
  }

  public function getGuestList() {
    return array();
  }

  public function buildObjecArray() {
    $con = 0;
    $hotel = $this->hotel;
    $methods = get_class_methods($hotel);
    $objArray = array();
    foreach ($methods as $method) {
      if (substr($method, 0, 2) == '__') continue;
      if (substr($method, 0, 3) === 'get' && $method != 'getGroupedRooms' && $method != 'getRoomsAvail') {
        $key = lcfirst(substr($method, 3));
        $objArray[$key] = $hotel->$method();
        $con++;
      }
    }
    return $objArray;
  }
}
?>
