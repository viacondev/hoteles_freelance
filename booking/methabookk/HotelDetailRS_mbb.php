<?php
namespace Methabookk;

class HotelDetailRS extends \BookingHotelDetail {

  public $elements;
  // private $servicesFacilities;
  // private $roomFacilities;
  // private $cateringFacilities;

  public function __construct($rs) {
    $this->elements = $rs[0];
  }

  private function build() {

  }

  public function getCode() {
    $hotelCode = $this->elements->hotel_code;
    return $hotelCode;
  }

  public function getName() {
    $hotelName = $this->elements->hotel_name;
    return $hotelName;
  }

  public function getDescription() {
    return $this->elements->hotel_description;
  }

  public function getImageList() {
    $img_array = array();
    $img = json_decode($this->elements->images);
    foreach ($img as $value) {
      $img_array[] = $value->Url;
    }
    return $img_array;
  }

  public function getAddressStreetName() {
    return $this->elements->address;
  }
  public function getAddressNumber() {
    return '';
  }

  public function getAddressPostalCode() {
    return '';
  }

  public function getEmailList() {
    return array();
  }

  public function getPhoneList() {

    return array();
  }

  public function getFaxList() {
    return array();
  }

  public function getWebList() {
    return array();
  }

  public function getLocation() {
    return '';
  }

  public function getLatitude() {
    $latitude = $this->elements->latitude;
    return $latitude;
  }

  public function getLongitude() {
    $longitude = $this->elements->longitude;
    return $longitude;
  }

  public function getDestinationCode() {
    // TODO: return dest code
    return '';
  }

  public function getDestinationName() {
    // Commented because direction already inludes de city & country
    // return strval($this->contact->Addresses->Address->CityName);
    return '';
  }

  public function getDestinationZone() {
    return '';
  }

  public function getCategory() {
    $category = $this->elements->category_code;
    return strval(substr($category, 0, 1) . 'EST');
  }

  public function getCategoryCode() {
    $category = $this->elements->category_code;
    return strval(substr($category, 0, 1) . 'EST');
  }

  public function getBuildingFacilities() {
    // return $this->buildingFacilities;
    return array();
  }

  public function getHotelTypeFacilities() {
    // return $this->hotelTypeFacility;
    return array();
  }

  public function getCredCardsFacilities() {
    // return $this->credCardsFacilities;
    return array();
  }

  public function getRoomFacilities() {
    return array();
  }

  public function getServicesFacilities() {
    return array();
  }

  public function getCateringFacilities() {
    return array();
  }

  public function getBusinessFacilities() {
    // return $this->businessFacilities;
    return array();
  }

  public function getHealthBeautyFacilities() {
    // return $this->healthBeautyFacilities;
    return array();
  }

  public function getEntertainmentFacilities() {
    // return $this->entertainmentFacilities;
    return array();
  }

  public function getDistancesFacilities() {
    return array();
  }

  public function getHighLightFacilities() {
    // return $this->highLightFacilities;
    return array();
  }

}

?>
