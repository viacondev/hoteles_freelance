<?php
namespace Methabookk;

class PurchasedRoomRS extends \RoomAvail {

  private $elements;
  private $guests;

  public function __construct($elements, $hotel, $board, $paxs, $paxes) {
    $this->elements = $elements;
    $this->board = $board;

    $this->adultCount = 0;
    $this->childCount = 0;
    $this->guests = array();
    $i = 0;

    foreach ($paxs as $guest) {
      $i++;
      $is_pax = false;
      $paxcurrent = '';
      $indexPax = 1;
      foreach ($paxes as $value) {
        if ($guest->paxId == $indexPax) {
          $is_pax = true;
          $paxcurrent = $value;
          break;
        }
        $indexPax++;
      }

      if (!$is_pax) continue;
      $type = ($paxcurrent->age > 18) ? 'AD' : 'CH';
      $this->guests[] = new \BookingCustomer($type, $i, $paxcurrent->age, $paxcurrent->name, $paxcurrent->surname);
      if ($type == 'AD') {
        $this->adultCount++;
      }
      else if ($type == 'CH') {
        $this->childCount++;
      }

    }
  }

  public function getProvider() {
    return \BookingProvider::METHABOOK;
  }

  public function getRoomCount() {
    return 1;
  }

  public function getAdultCount() {
    return $this->adultCount;
  }

  public function getChildCount() {
    return $this->childCount;
  }

  public function getPrice() {
    // TODO: return price
    return 0;
  }

  public function getSHRUI() {
    return '';
  }

  public function getOnRequest() {
    return '';
  }

  public function getBoard() {
    return strval($this->board->name);
  }

  public function getBoardCode() {
    return $this->board->code;
  }

  public function getBoardType() {
    return '';
  }

  public function getRoomType() {
    return $this->elements->name;
  }

  public function getRoomTypeType() {
    return '';
  }

  public function getRoomTypeCode() {
    return '';
  }

  public function getRoomTypeCharacteristic() {
    return '';
  }

  public function getCancellationPolicies() {
    return array();
  }

  public function getGuestList() {
    return $this->guests;
  }
}

?>
