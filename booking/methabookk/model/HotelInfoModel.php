<?php
namespace Methabookk;

class HotelInfoModel {

  public $HotelCode;
  public $Hotel_Name;
  public $Zone_Code;
  public $Zone_Name;
  public $Destination_Code;
  public $Country_Code;
  public $Longitude;
  public $Latitude;
  public $Address;
  public $Hotel_Description;
  public $images;

  public function getDescription() {
    return $this->HotelFacilities;
  }

  public function getZone() {
    return $this->ZoneName;
  }

  public function getLocation() {
    return $this->ZoneName . ', ' . $this->DestinationName;
  }

  public function getCode() {
    return $this->HotelCode;
  }

  public static function buildClass($code) {

  }

  /**
   * Returns an array of HotelInfoModel that only contains the Code
   */
  public static function findHotelsByCode($name, $hotelCode) {
    $query = "SELECT hotel_code, hotel_name, category_code, hotel_description, address, images, latitude, longitude FROM HotelMb
                WHERE HOTEL_CODE = '$hotelCode'";

    return Database::getInstance()->fetchAll($query, 'Methabookk\HotelInfoModel');
  }
  
  public static function findHotelsByListHotelCode($dest) {
    $query = "SELECT hotel_code, hotel_name, category_code, images, latitude, longitude, zone_code FROM HotelMb
                WHERE hotel_code IN ($dest)";

    return Database::getInstance()->fetchAll($query, 'HotelBeds\HotelInfoModel');
  }
}

?>
