<?php
namespace Methabookk;
use Rest\ClientRest;
class MethabookkRQ {

  private $location;

  public function __construct() {
    $this->location = \MBBConfig::$url;
  }

  public function getRequestXML($RQElement) {

  }

  public function execRequest($RQElement, $solicitud = "", $type = "") {
    $url = \MBBConfig::$url;
    $method = $RQElement->getMethod();
    $function = $RQElement->getType();
    $params   = $RQElement->getParams();
    $key      = base64_encode(\MBBConfig::$agencyName . ":" . \MBBConfig::$agencyCode);
    $header   = array("accept-charset" =>  "UTF-8", "Content-Type" => "application/json",
                    "Authorization" => "Basic " . $key , "cache-control" => "no-cache");
    // $params = json_encode(array('language' => "es"));
    if (isset(\MBBConfig::$DirectXMLAccess) && \MBBConfig::$DirectXMLAccess) { //falta incluir el archivo rest para que funcione
      include_once(__DIR__ . '/../rest/ClientRest.php');
      $rest = new ClientRest($url);
      $result = $rest->$function($method, $header, $params);
      $xml = $result->response;
    }
    else {
      // Call to a remote request wrapper because our current dev IP doesn't have access to MB.
      // This should be changed due security issues.
      $rq = 'http://test.boliviabooking.com/hotelesboliviabooking/booking/methabookk/RemoteRequestWrapper_mbb.php';
      $rq .= '?url=' . urlencode($url);
      $rq .= '&header=' . urlencode(json_encode($header));
      $rq .= '&params=' . urlencode(json_encode($params));
      $rq .= '&method=' . urlencode($method);
      $rq .= '&type=' . urlencode($function);

      $context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));
      $xml = file_get_contents($rq);

    }
    if($solicitud == 'XML_RQ') {
      $descrip = $type;
      \Log::info(json_encode($params), $descrip);
    }

    // __logtxt($xml); // RS
    return json_decode($xml);
  }
  public function allOptions($RQElement) {
    $url = \MBBConfig::$url;
    $method = $RQElement->getMethod();
    $function = $RQElement->getType();
    $params   = $RQElement->getParams();
    $key      = base64_encode(\MBBConfig::$agencyName . ":" . \MBBConfig::$agencyCode);
    $header   = array("accept-charset" =>  "UTF-8", "Content-Type" => "application/json",
                    "Authorization" => "Basic " . $key , "cache-control" => "no-cache");
    if(false)
    {
      include_once(__DIR__ . '/../rest/ClientRest.php');
      $rest = new ClientRest($url);
      $options = $rest->getOptionsApi($method, $header, $params);
    }
    else
    {
      $rq = 'http://test.boliviabooking.com/hotelesboliviabooking/booking/methabookk/RemoteRequestWrapper_mbb.php';
      $rq .= '?url=' . urlencode($url);
      $rq .= '&header=' . urlencode(json_encode($header));
      $rq .= '&params=' . urlencode(json_encode($params));
      $rq .= '&method=' . urlencode($method);
      $rq .= '&type=' . urlencode($function);
      $options  = array(CURLOPT_AUTOREFERER => true, 
                        CURLOPT_HEADER => 0, 
                        CURLOPT_RETURNTRANSFER => 1, 
                        CURLOPT_URL             => $rq);
    }
    //echo "<pre>"; print_r($options); echo "</pre>";
    return $options;
  }

  public function getRequestXML__soapCall($RQElement) {

  }
  /**
   * Even when this way of requesting works, it fails while reading the data under CDATA tag.
   * So, it is not used but left here just in case.
   * To make it work, $this->getRequestXML() should not include the soap:Envelope and soap:Body tags.
   */
  public function execRequestUsing__soapCall($RQElement) {

  }
}

?>
