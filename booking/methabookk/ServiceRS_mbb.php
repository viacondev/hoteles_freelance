<?php
namespace Methabookk;

/**
 * Class used for confirmed Services
 */
class ServiceRS extends \BookingService {

  private $elements;
  private $item;
  private $errors;
  public function __construct($rs, $scItem, $errors = array()) {
    $this->elements = $rs;
    $this->errors = $errors;
    $this->item = json_decode(json_encode($scItem), FALSE);
  }

  public function getLocalizer() {
    $localizer = $this->elements->locator;
    return $localizer;
  }

  public function getSPUI() {
    return '';
  }

  public function getServiceType() {
    return 'ServiceHotel';
  }

  public function getServiceStatus() {
    return '';
  }

  public function getDirectPayment() {
    return '';
  }

  public function getDateFrom() {
    $checkIn = explode('-', $this->elements->services->accommodationServices[0]->checkInDate);
    $checkIn = $checkIn[0] . $checkIn[1] . $checkIn[2];
    return $checkIn;
  }

  public function getDateTo() {
    $checkOut = explode('-', $this->elements->services->accommodationServices[0]->checkOutDate);
    $checkOut = $checkOut[0] . $checkOut[1] . $checkOut[2];
    return $checkOut;
  }

  public function getCurrency() {
    $currency = $this->elements->services->accommodationServices[0]->price->currencyCode;
    return $currency;
  }

  public function getTotalAmount() {
    $totalAmount = $this->elements->services->accommodationServices[0]->price->gross;
    return $totalAmount;
  }

  public function getAdditionalCosts() {
    return array();
  }

  public function getServiceInfo() {
    $accommodations = $this->elements->services->accommodationServices[0];
    $paxes          = $this->elements->paxes;
    $totalAmount    = $this->getTotalAmount();
    return new ConfirmHotelAvail($accommodations, $this->item, $paxes, $totalAmount);
  }

  public function getSupplementList() {
    return array();
  }

  public function getStatus() {
    $status = $this->elements->status;

    if ($status == 'Confirmed') {
      return \ServiceStatus::CONFIRMED;
    }
  }

  public function getErrors() {
    return $this->errors;
  }

}

?>
