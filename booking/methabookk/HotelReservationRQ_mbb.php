<?php
namespace Methabookk;

class HotelReservationRQ {

  private $params;
  private $item;
  private $serviceIndex;

  public function __construct($params, $item, $serviceIndex) {
    $this->params = $params;
    $this->item = $item;
    $this->serviceIndex = $serviceIndex;
  }

  function getType() {
    return "purchase";
  }

  function getMethod() {
    return 'Bookings/v1.0';
  }

  public function getParams() {
    $request = array();
    $paxes = array();
    $p = $this->params;
    $i = $this->serviceIndex;
    $item = $this->item;
    $numberOfRooms = count($item['rooms']);
    $currentPax = 1;
    $roomsRQ = array();
    $roomCount = 1;
    $paxCount = 0;
    $totalPrice = 0;
    for ($d = 0; $d < $numberOfRooms; $d++) {
      $room = $item['rooms'][$d];
      $roomRQ = array();
      $roomRQ['RoomType'] = $room->roomTypeType;
      $roomRQ['RoomRefId'] = $roomCount;
      $paxesRQ = array();
      $adultCount = 0;
      $totalPrice += $room->price;
      for ($r = 0; $r < $room->adultCount; $r++) {
        $pax = array();
        $pax['PaxId'] = $currentPax;
        $pax['Name']  = $p['customerName_' . $i . '_' . $currentPax];
        $pax['SurName'] = $p['customerLastName_' . $i . '_' . $currentPax];
        $pax['Holder'] = false;
        if ($currentPax == 1) {
          $pax['Holder'] = true;
          $pax['Email'] = \Config::findById(1)->mail2;
          $pax['PhoneNumber'] = $p['holderPhone'];
          $pax['Address'] = $p['holderAddress'];
          $pax['City'] = $p['holderCity'];
          $pax['Document'] = 6279510;
          $pax['PostalCode'] = $p['holderPostalCode'];
        }
        $pax['Age'] = 30;
        $currentPax++;
        $adultCount++;
        $paxCount++;
        $paxes[] = $pax;
        $paxesRQ[] = array('PaxId' => $paxCount);
      }

      for ($r = 0; $r < $room->childCount; $r++) {
        $pax = array();
        $pax['PaxId'] = $currentPax;
        $pax['Name'] = $p['customerName_' . $i . '_' . $currentPax];
        $pax['SurName'] = $p['customerLastName_' . $i . '_' . $currentPax];
        $pax['Holder'] = false;
        $pax['Age']     = $p['customerAge_' . $i . '_' . $currentPax];
        $currentPax++;
        $paxCount++;
        $paxes[] = $pax;
        $paxesRQ[] = array('PaxId' => $paxCount);
      }
      $roomRQ['Paxes'] = $paxesRQ;
      $roomsRQ[] = $roomRQ;
      $roomCount++;
    }
    $services = array();
    $rooms = $item['rooms'][0];
    $accommodationService = array();
    $accommodationService['BookingCode'] = $p['dataRequired_' . $this->serviceIndex];
    $accommodationService['AccommodationCode'] = $item['availToken'];
    $accommodationService['CheckInDate'] = dateFromYmdToDB($item['dateFrom']);
    $accommodationService['CheckOutDate'] = dateFromYmdToDB($item['dateTo']);
    $accommodationService['MealPlanCode'] = $rooms->boardCode;
    $accommodationService['Price'] = array('gross' => $totalPrice, 'currencyCode' => 'USD');
    $accommodationService['Rooms'] = $roomsRQ;
    $bookingRq = array();
    $bookingRq['language'] = "es";
    $bookingRq['ClientReference'] = str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789".uniqid());
    $bookingRq['Remarks'] = "";
    $bookingRq['Paxes'] = $paxes;
    $bookingRq['Services'] = array('AccommodationServices' => array($accommodationService));
    $request['bookingRq'] = $bookingRq;
    // __logarr(json_encode($request));
    return $request;
  }
}
?>
