<?php
namespace Methabookk;

include_once('SCartHotelAvailRS_mbb.php');

class SCartServiceRS extends \BookingService {

  private $data;
  public $errors;
  // private $localizer;
  public $serviceInfo;
  public function __construct($data, $localizer = '', $total_price = 0, $errors = array()) {
    $this->data = json_decode(json_encode($data), FALSE);
    $this->localizer = $localizer;
    $this->$total_price = $total_price;
    $from = $this->data->dateFrom;
    $to = $this->data->dateTo;
    $checkin = substr($from, 0, 4) . '-' . substr($from, 4, 2) . '-' . substr($from, 6, 4);
    $checkout = substr($to, 0, 4) . '-' . substr($to, 4, 2) . '-' . substr($to, 6, 4);
    $room = $this->data->rooms[0];
    $mealplanCode = $room->boardCode;
    $accommodationCode = $room->SHRUI;
    $availabilityCode = $this->data->availToken;
    $params = array('language'          => "es",
                    'nationality'       => 'ES',
                    'availabilityCode'  => $accommodationCode,
                    'accommodationCode' => $availabilityCode,
                    'mealplanCode'      => $mealplanCode,
                    'CheckInDate'       => $checkin,
                    'CheckOutDate'      => $checkout
                    );
    $valuationRQ = new HotelValuationRQ($params);
    $rq = new MethabookKRQ();
    $xml = $rq->execRequest($valuationRQ, 'XML_RQ', 'BB_MBB_RQ_VALUATION');
    \Log::info(json_encode($xml), 'BB_MBB_RS_VALUATION');
    $this->serviceInfo = new SCartHotelAvailRS($this->data, $this->getTotalAmount(), $xml);

  }

  public function getLocalizer() {
    return $this->localizer;
  }

  public function getSPUI() {
    return '';
  }

  public function getServiceType() {
    return 'ServiceHotel';
  }

  public function getServiceStatus() {
    return '';
  }

  public function getDirectPayment() {
    return '';
  }

  public function getDateFrom() {
    return $this->data->dateFrom;
  }

  public function getDateTo() {
    return $this->data->dateTo;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getTotalAmount() {
    if (isset($this->total_price) && $this->total_price != 0) {
      return $this->total_price;
    }

    $price = 0;
    foreach ($this->data->rooms as $room) {
      $price += $room->price;
    }
    return $price;
  }

  public function getTotalAmountWithFee() {
    if (isset($this->total_price) && $this->total_price != 0) {
      return round($this->total_price / $this->data->factorFee, 2);
    }

    $price = 0;
    foreach ($this->data->rooms as $room) {
      $price += $room->price;
    }
    return round($price / $this->data->factorFee, 2);
  }

  public function getAdditionalCosts() {
    return array();
  }

  public function getServiceInfo() {
    return $this->serviceInfo;
  }

  public function getSupplementList() {
    return array();
  }

  public function getStatus() {
    return \ServiceStatus::PENDING;
  }

  public function getErrors() {
    return $this->errors;
  }

}

?>
