<?php
namespace Methabookk;

class Room
{
  private $adultCount;
  private $childCount;
  private $childAges;
  function __construct($adultCount, $childCount, $childAges) {
    $this->adultCount = $adultCount;
    $this->childCount = $childCount;
    $this->childAges = $childAges;
  }

  function getAdultCount() {
    return $this->adultCount;
  }

  function getChildCount() {
    return $this->childCount;
  }

  function getChilddAges() {
    return $this->childAges;
  }
}

?>
