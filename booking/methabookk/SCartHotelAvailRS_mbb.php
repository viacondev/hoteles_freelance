<?php
namespace Methabookk;

include_once('SCartRoomAvailRS_mbb.php');

class SCartHotelAvailRS extends \HotelAvail {

  private $data;
  private $rules;
  public $availToken;
  public function __construct($data, $totalAmountService, $response) {
    $this->data = $data;
    $this->rules = $response;
    $this->availToken = $response->valuationRS->availability->booking->bookingCode;
    $this->roomsAvail = array();
    $indexChild = 0;
    foreach ($this->data->rooms as $room) {
      $this->roomsAvail[] = new SCartRoomAvailRS($room, $this, $this->getDateFrom(), $this->getDateTo(), $this->data->childAges, $this->rules, $totalAmountService, $this->data->factorFee, $indexChild);
      $indexChild += $room->childCount;
    }
  }

  public function getProvider() {
    return \BookingProvider::METHABOOK2;
  }

  public function getProviderName() {
    return 'METHABOOK';
  }

  public function getAvailToken() {
    return $this->availToken;
  }

  public function getCode() {
    return $this->data->hotelCode;
  }

  public function getName() {
    return $this->data->hotelName;
  }

  public function getDateFrom() {
    return $this->data->dateFrom;
  }

  public function getDateTo() {
    return $this->data->dateTo;
  }

  public function getDestinationCode() {
    return $this->data->destCode;
  }

  public function getDestinationType() {
    return '';
  }

  public function getMainImage() {
    return '';
  }

  public function getDescription() {
    return '';
  }

  public function getLatitude() {
    return '';
  }

  public function getLongitude() {
    return '';
  }

  public function getZone() {
    return '';
  }

  public function getLocation() {
    return '';
  }

  public function getCategory() {
    return '';
  }

  public function getCategoryType() {
    return '';
  }

  public function getCategoryCode() {
    return strval($this->data->category);
  }

  public function getRoomsAvail() {
    return $this->roomsAvail;
  }

  public function getGroupedRooms() {
    return $this->roomsAvail;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getContractName() {
    return '';
  }

  public function getContractIncomingOfficeCode() {
    return '';
  }

  public function getContractComment() {
    return '';
  }

  public function getClientComment() {
    return '';
  }

  public function getSupplier() {
    return '';
  }

  public function getSupplierVatNumber() {
    return '';
  }

  public function getDiscountList() {
    return array();
  }

  public function getAllPaxRequired() {
    return true;
  }

  public function getXtraDataRequired() {
    return true;
  }

  public function isDataRequired() {
   return true;
  }
}

?>
