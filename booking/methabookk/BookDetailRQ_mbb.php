<?php
namespace Methabookk;

class BookDetailRQ {

  private $params;

  public function __construct($params) {
    $this->params = $params;
  }

  function getType() {
    return "searchHotelAvail";
  }

  function getMethod() {
    return 'Bookings/v1.0/';
  }

  public function getParams() {
    return $this->params;
  }
}
?>
