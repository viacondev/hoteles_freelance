<?php
namespace Methabookk;

class ReadServiceRQ {

  private $params;

  public function __construct($params = array()) {
    $this->params = $params;
  }

  function getType() {
    return "purchase_detail";
  }

  function getMethod() {
    return 'Bookings/v1.0';
  }

  function getParams() {
    return $this->params;
  }

}
?>
