<?php
namespace Methabookk;

class Database {
  private $dbname = 'barrybol_zion_engine';
  private $dbuser = 'barrybol_zion';
  private $dbpwd = 'zionshopdbpwd';

  private $db;
  static private $instan = NULL;

  private function __construct() {
    $this->db = new \PDO("mysql:host=localhost;dbname=" . $this->dbname . ';charset=utf8', $this->dbuser, $this->dbpwd);
  }

  public static function getInstance() {
    if (is_null(self::$instan)) {
      self::$instan = new Database();
    }
    return self::$instan;
  }

  public function fetchClass($query, $className) {
    $result = $this->db->query($query);
    $result->setFetchMode(\PDO::FETCH_CLASS, $className);

    return $result->fetch();
  }

  public function fetchAll($query, $className) {
    $result = $this->db->query($query);
    $result->setFetchMode(\PDO::FETCH_CLASS, $className);

    return $result->fetchAll();
  }
}


?>
