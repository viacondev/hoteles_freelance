<?php
namespace Methabookk;

include_once('HotelAvailRS_mbb.php');

class HotelAvailListRS extends \HotelAvailList {

  private $elements;
  public $serviceHotels;
  public function __construct($rs, $searchHotelName, $checkin, $checkout, $destCode, $rooms_numbers, $regimen, $factorFee) {
    $this->elements = $rs;
    $arr = array();
    foreach ($this->elements as $value) {
      $arr[] = '"' . $value->code . '"';
    }
    $listCodes    = implode(',', $arr);
    $result       = HotelInfoModel::findHotelsByListHotelCode($listCodes);
    $arr          = array();
    $arr_acentos  = array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢",
                                          "ê","Ã®","Ã´","Ã»","Ã","ÃŠ","ÃŽ","Ã","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã","Ã‹","Ñ","*","%");
    $permitidas   = array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e",
                                "i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E","N",".",".");
    foreach ($result as $hotel) {
      $arr[$hotel->hotel_code] = $hotel;
    }
    foreach ($this->elements as $value) {
      $hotelName         = str_replace($arr_acentos, $permitidas, $value->name);
      $accommodationCode = $value->code;
      $availabilities    = $value->availabilities;
      $hotelinfo         = $value->info;
      $hoteldesc         = isset($arr[$value->code]) ? $arr[$value->code] : array();
      $params            = array( 'availabilities'  => $availabilities, 'checkin'       => $checkin, 'checkout' => $checkout,
                                  'destCode'        => $destCode,       'hotelName'     => $hotelName, 'accommodationCode' => $accommodationCode,
                                  'hotelinfo'       => $hotelinfo,      'rooms_numbers' => $rooms_numbers, 'hoteldesc' => $hoteldesc,
                                  'regimen'         => $regimen,        'factorFee'     => $factorFee);
      $hotelAvail = new HotelAvailRS($params);
      $count      = count($hotelAvail->getGroupedRooms());
      if ($searchHotelName != '' && $count > 0) {
        if (preg_match("/$searchHotelName/i", $hotelName)) {
          $this->serviceHotels[] = $hotelAvail;
        }
      }
      else {
        if ($count > 0) {
          $this->serviceHotels[] = $hotelAvail;
        }
      }

    }
  }

  public function getHotelsAvail() {
    return $this->serviceHotels;
  }

  public function getCurrentPage() {
    return 1;
  }

  public function getTotalPages() {
    return 1;
  }

}
?>
