<?php
namespace Methabookk;

class CancellBookRQ{

  private $params;

  public function __construct($localizer) {
    $this->params = array($localizer);
  }

  function getType() {
    return "cancelPurchase";
  }

  function getMethod() {
    return 'Bookings/v1.0/Cancel';
  }

  function getParams() {
    return $this->params;
  }
}
?>
