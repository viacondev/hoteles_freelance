<?php
namespace Methabookk;

include_once('PurchasedHotelRS_mbb.php');

/**
 * Class used for purchased Services (used for vouchers view)
 */
class PurchasedServiceRS extends \BookingService {

  private $elements;

  public function __construct($rs) {
    $this->elements = $rs;
  }

  public function getLocalizer() {
    $localizer = $this->elements->locator;
    return $localizer;
  }

  public function getSPUI() {
    return '';
  }

  public function getServiceType() {
    return 'ServiceHotel';
  }

  public function getServiceStatus() {
    return '';
  }

  public function getDirectPayment() {
    return '';
  }

  public function getDateFrom() {
    $checkIn = explode('-', $this->elements->services->accommodationServices[0]->checkInDate);
    $checkIn = $checkIn[0] . $checkIn[1] . $checkIn[2];
    return $checkIn;
  }

  public function getDateTo() {
    $checkOut = explode('-', $this->elements->services->accommodationServices[0]->checkOutDate);
    $checkOut = $checkOut[0] . $checkOut[1] . $checkOut[2];
    return $checkOut;
  }

  public function getCurrency() {
    $currency = $this->elements->services->accommodationServices[0]->price->currencyCode;
    return $currency;
  }

  public function getTotalAmount() {
    $totalAmount = $this->elements->services->accommodationServices[0]->price->gross;
    return $totalAmount;
  }

  public function getAdditionalCosts() {
    return array();
  }

  public function getServiceInfo() {
    return new PurchasedHotelRS($this->elements->services->accommodationServices[0], $this->elements->paxes);
  }

  public function getSupplementList() {
    return array();
  }

  public function getStatus() {
    $status = $this->elements->status;

    if ($status == 'Confirmed') {
      return \ServiceStatus::CONFIRMED;
    }
  }

}

?>
