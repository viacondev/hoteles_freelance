<?php
namespace Methabookk;
// error_reporting(0);
ini_set('memory_limit', -1);
include_once('MethabookkRQ.php');
include_once('AvailableHotelsRQ_mbb.php');
include_once('HotelAvailListRS_mbb.php');
include_once('PurchaseRS_mbb.php');
include_once('SCartServiceRS_mbb.php');
include_once('HotelValuationRQ.php');
include_once('Room_mbb.php');
include_once('ServiceRS_mbb.php');
include_once('HotelReservationRQ_mbb.php');
include_once('ReadServiceRQ_mbb.php');
include_once('PurchasedServiceRS_mbb.php');
include_once('CancellBookRQ_mbb.php');
include_once('ConfirmHotelAvail_mbb.php');
include_once('ConfirmRoomAvail_mbb.php');
include_once('DestinationCodeMap_mbb.php');
include_once('config/database.php');
include_once('model/HotelInfoModel.php');
include_once('HotelDetailRS_mbb.php');
include_once('BookDetailRQ_mbb.php');

class MethabookkBooking {
  private $request;

  public function __construct() {
    $this->request = new MethaBookkRQ();
  }

  public function execRequest($params) {
    switch ($params['requestType']) {
      case \BookingRequestTypes::SearchHotelAvail:
        return $this->execSearchHotelAvailRQ($params);
        break;

      case \BookingRequestTypes::AddHotelService:
        return $this->execHotelServiceAddRQ($params);
        break;

      case \BookingRequestTypes::ConfirmPurchase:
        return $this->execPurchaseConfirmRQ($params);
        break;

      case \BookingRequestTypes::PurchaseDetail:
        return $this->execPurchaseDetailRQ($params);
        break;

      case \BookingRequestTypes::HotelDetail:
        return $this->execHotelDetailRQ($params);
        break;

      case \BookingRequestTypes::CancelPurchase:
        return $this->execPurchaseCancelRQ($params);
        break;

      case \BookingRequestTypes::PurchaseDetailList:
        return $this->execPurchaseDetailList($params);
        break;

      default:
        return NULL;
        break;
    }
  }

  public function execSearchHotelAvailRQ($p) {
    // if ($p['hotelCode'] != '' && substr($p['hotelCode'], 0, 2) != 'JP') return NULL;
    $destCode = DestinationCodeMap::getDestCode($p['destCode']);
    if ($destCode == NULL || intval($p['roomCount']) > 3) return NULL;
    // Only search for page=1 since there is no pagination on MB
    // if ($p['page'] != 1) return NULL;
    $roomCount      = intval($p['roomCount']);
    $rooms          = array();
    $rooms_numbers  = array();
    $childIndex = 1;
    for ($i = 1;$i <= $roomCount; $i++) {
      $adultCount = intval($p["adultCount_$i"]);
      $childCount = intval($p["childCount_$i"]);
      $occupancy  = array();
      $childAges  = array();
      for  ($d = 1; $d <= intval($p["adultCount_$i"]); $d++) {
        $occupancy[] = 30;
      }

      for ($c = 1; $c <= intval($p["childCount_$i"]); $c++) {
        $occupancy[] = $p["childAge_$childIndex"];
        $childAges[] = $p["childAge_$childIndex"];
        $childIndex++;
      }
      $rooms_numbers[] = new Room($adultCount, $childCount, $childAges);
      $rooms[] = $occupancy;
    }
    $checkin  = substr($p['checkin'], 0, 4) . '-' . substr($p['checkin'], 4, 2) . '-' . substr($p['checkin'], 6, 4);
    $checkout = substr($p['checkout'], 0, 4) . '-' . substr($p['checkout'], 4, 2) . '-' . substr($p['checkout'], 6, 4);

    // $p['destCode'] = DestinationCodeMap::getCode($p['destCode']);
    // $p['destCode'] = $destCode;

    $params = array('language'              => "es",
                    'nationality'           => 'ES',
                    'showExtendedHotelInfo' => 'true',
                    'CheckInDate'           => $checkin,
                    'CheckOutDate'          => $checkout,
                    );
    //$r = \Hotel_Mapping::findAll(array('dest_code_provider' => $p['destCode']));
    $r = \Hotel_Mapping::getHotelByLimit($p['destCode'], \BookingProvider::METHABOOK2);
    if (!empty($p['hotelCode'])) {
      $zion = \HotelMappingProvider::getHotelByZionCode(array('hotel_code' => $p['hotelCode'], 'provider' => \BookingProvider::METHABOOK2));
      $params['accommodationCodes'] = $zion;
    }
    else if (!empty($p['zoneCode'])) {
      $hotelsCode = '';
      $res        = \HotelMappingProvider::getHotelByZoneCode(array('destCode' => $p['destCode'], 'zone_code' => $p['zoneCode'], 'provider' => \BookingProvider::METHABOOK2));
      foreach ($res as $hotel) {
        $hotelsCode .= $hotel->code . ",";
      }
      $hotelsCode = substr($hotelsCode, 0, -1);
      $params['accommodationCodes'] = $hotelsCode;
    }
    else if (count($r) > 2 && empty($p['zoneCode'])) {
      $hotelsCode = '';$con = 0;
      foreach ($r as $value) {
        $hotelsCode .= $value->hotel_code . ",";
      }
      $hotelsCode = substr($hotelsCode, 0, -1);
      $params['accommodationCodes'] = $hotelsCode;
    }
    else {
      $params['ZoneCode'] = $destCode;
     }
    $count = 0;
    for ($i = 1;$i <= $roomCount; $i++) {
      $params["room$i"] = implode(',', $rooms[$count]);
      $count++;
    }

    $request = new AvailableHotelsRQ($params);

    $profiling = new \Profiling('hotelAvail query', 'MethabookBooking');
    $profiling->init();

    $xml = $this->request->execRequest($request);

    $profiling->end(strlen(json_encode($xml)));
    if (!isset($xml->availabilityRs) ||count($xml->availabilityRs->accommodations) == 0) {
      return NULL;
    }
    $regimen = '';
    if(isset($p['regimen']) && $p['regimen'] != '') {
      $regimen = explode(',', implode(',', $p['regimen']));
    }
    $rs = $xml->availabilityRs->accommodations;
    $hotelName = '';
    if (isset($p['hotelName']) && $p['hotelName'] != '') {
      $hotel = str_replace(" ", ',', $p['hotelName']);
      $hotel = explode(',', $hotel);

      foreach ($hotel as $value) {
        $hotelName .= trim($value) . "|";
      }
      $hotelName = substr($hotelName, 0, -1);
    }
    $result = new HotelAvailListRS($rs, $hotelName, $p['checkin'], $p['checkout'], $p['destCode'], $rooms_numbers, $regimen, $p['factorFee']);

    if (isset($p['stars']) && $p['stars'] != '') {
      $star = explode(',', implode(',', $p['stars']));
      $hotel = $result->getHotelsAvail();
      for ($i = 0; $i < count($hotel); $i++) {
          $service = $hotel[$i];
          if (!in_array("'" . $service->getCategoryCode() . "'", $star)) {
            unset($result->serviceHotels[$i]);
          }
      }
    }
    //
    // if(isset($p['zone_name']) && $p['zone_name'] != '') {
    //   $zone_name = $p['zone_name'];
    //   $hotel = $result->getHotelsAvail();
    //   for ($i = 0; $i < count($hotel); $i++) {
    //       if(!preg_match("/$zone_name/i", $hotel[$i]->getLocation())) {
    //         unset($result->serviceHotels[$i]);
    //       }
    //   }
    // }
    
    return $result;
  }

  public function prepararConsulta($p) {
    $destCode = DestinationCodeMap::getDestCode($p['destCode']);
    if ($destCode == NULL || intval($p['roomCount']) > 3) return NULL;
    // Only search for page=1 since there is no pagination on MB
    // if ($p['page'] != 1) return NULL;
    $roomCount      = intval($p['roomCount']);
    $rooms          = array();
    $rooms_numbers  = array();
    $childIndex = 1;
    for ($i = 1;$i <= $roomCount; $i++) {
      $adultCount = intval($p["adultCount_$i"]);
      $childCount = intval($p["childCount_$i"]);
      $occupancy  = array();
      $childAges  = array();
      for  ($d = 1; $d <= intval($p["adultCount_$i"]); $d++) {
        $occupancy[] = 30;
      }

      for ($c = 1; $c <= intval($p["childCount_$i"]); $c++) {
        $occupancy[] = $p["childAge_$childIndex"];
        $childAges[] = $p["childAge_$childIndex"];
        $childIndex++;
      }
      $rooms_numbers[] = new Room($adultCount, $childCount, $childAges);
      $rooms[] = $occupancy;
    }
    $checkin  = substr($p['checkin'], 0, 4) . '-' . substr($p['checkin'], 4, 2) . '-' . substr($p['checkin'], 6, 4);
    $checkout = substr($p['checkout'], 0, 4) . '-' . substr($p['checkout'], 4, 2) . '-' . substr($p['checkout'], 6, 4);

    // $p['destCode'] = DestinationCodeMap::getCode($p['destCode']);
    // $p['destCode'] = $destCode;

    $params = array('language'              => "es",
                    'nationality'           => 'ES',
                    'showExtendedHotelInfo' => 'true',
                    'CheckInDate'           => $checkin,
                    'CheckOutDate'          => $checkout,
                    );
    //$r = \Hotel_Mapping::findAll(array('dest_code_provider' => $p['destCode']));
    $r = \Hotel_Mapping::getHotelByLimit($p['destCode'], \BookingProvider::METHABOOK2);
    if (!empty($p['hotelCode'])) {
      $zion = \HotelMappingProvider::getHotelByZionCode(array('hotel_code' => $p['hotelCode'], 'provider' => \BookingProvider::METHABOOK2));
      $params['accommodationCodes'] = $zion;
    }
    else if (!empty($p['zoneCode'])) {
      $hotelsCode = '';
      $res        = \HotelMappingProvider::getHotelByZoneCode(array('destCode' => $p['destCode'], 'zone_code' => $p['zoneCode'], 'provider' => \BookingProvider::METHABOOK2));
      foreach ($res as $hotel) {
        $hotelsCode .= $hotel->code . ",";
      }
      $hotelsCode = substr($hotelsCode, 0, -1);
      $params['accommodationCodes'] = $hotelsCode;
    }
    else if (count($r) > 2 && empty($p['zoneCode'])) {
      $hotelsCode = '';$con = 0;
      foreach ($r as $value) {
        $hotelsCode .= $value->hotel_code . ",";
      }
      $hotelsCode = substr($hotelsCode, 0, -1);
      $params['accommodationCodes'] = $hotelsCode;
    }
    else {
      $params['ZoneCode'] = $destCode;
     }
    $count = 0;
    for ($i = 1;$i <= $roomCount; $i++) {
      $params["room$i"] = implode(',', $rooms[$count]);
      $count++;
    }

    $request = new AvailableHotelsRQ($params);

    return $this->request->allOptions($request);    
  }
  public function obtenerResultado($xml, $p) {
    $xml = json_decode($xml);
    if (!isset($xml->availabilityRs) ||count($xml->availabilityRs->accommodations) == 0) {
      return NULL;
    }
    $roomCount      = intval($p['roomCount']);
    $rooms_numbers  = array();
    $childIndex = 1;
    for ($i = 1;$i <= $roomCount; $i++) {
      $adultCount = intval($p["adultCount_$i"]);
      $childCount = intval($p["childCount_$i"]);
      $occupancy  = array();
      $childAges  = array();
      for  ($d = 1; $d <= intval($p["adultCount_$i"]); $d++) {
        $occupancy[] = 30;
      }

      for ($c = 1; $c <= intval($p["childCount_$i"]); $c++) {
        $occupancy[] = $p["childAge_$childIndex"];
        $childAges[] = $p["childAge_$childIndex"];
        $childIndex++;
      }
      $rooms_numbers[] = new Room($adultCount, $childCount, $childAges);
    }
    $regimen    = '';
    $rs         = $xml->availabilityRs->accommodations;
    $hotelName  = '';
    $result     = new HotelAvailListRS($rs, $hotelName, $p['checkin'], $p['checkout'], $p['destCode'], $rooms_numbers, $regimen, $p['factorFee']);
    return $result;
  }

  public function execHotelServiceAddRQ($p) {
    $service = new SCartServiceRS($p);

    return new PurchaseRS(array($service));
  }

  public function execPurchaseConfirmRQ($p) {
    $services_count = intval($p['servicesCount']);
    $services = array();
    $errors = array();
    for ($i = 1; $i <= $services_count; $i++) {
      $book_params = (array)json_decode($p['book_params']);
      $item = $book_params;
      if ($item['provider'] != \BookingProvider::METHABOOK2) { continue; }
      $HotelReservationRQ = new HotelReservationRQ($p, $item, $i);
      $xml = $this->request->execRequest($HotelReservationRQ, 'XML_RQ', 'BB_MBB_RQ_PURCHASE');
      \Log::info(json_encode($xml), 'BB_MBB_RS_PURCHASE');
      $result = $xml;

      if (isset($result->exceptions)) {
        $error = $result->exceptions[0];
        $code = strval($error->code);
        $msg  = strval($error->message);
        $provider = \BookingProvider::METHABOOK2;
        $errors[] = array('code' => $code, 'msg' => $msg,'provider' => $provider);
        $services[] = new ServiceRS(array(), array(), $errors);
      }
      if (count($errors) == 0) {
          $services[] = new ServiceRS($result->bookingRs->bookings[0], $item, $errors);
      }

    }

    return new PurchaseRS($services);
  }

  function execPurchaseCancelRQ($p) {
    $CancellBookRQ = new CancellBookRQ($p['localizer']);
    $rs = $this->request->execRequest($CancellBookRQ, 'XML_RQ', 'MB_RQ_CANCEL');

    \Log::info(json_encode($rs), 'MB_RS_CANCEL');
    if (isset($rs->exceptions)) {
      return NULL;
    }
    $totalCancellationFee = floatval($rs->bookingRs->bookings[0]->services->accommodationServices[0]->price->gross);
    return new PurchaseRS(array(), $totalCancellationFee);
  }

  public function execPurchaseDetailRQ($p) {
    $ReadServiceRQ = new ReadServiceRQ(array('locator' => $p['localizer']));

    $rs = $this->request->execRequest($ReadServiceRQ);
    // \Log::info(json_encode($rs), 'MB_RS_INFOBOOK');

    $service = new PurchasedServiceRS($rs->bookingRs->bookings[0]);

    return new PurchaseRS(array($service));
  }

  public function execHotelDetailRQ($p) {
    $hotelInfo = new HotelInfoModel();
    $hotels = $hotelInfo->findHotelsByCode('', $p['hotelCode']);
    if(count($hotels) == 0) {
      return NULL;
    }
    return new HotelDetailRS($hotels);
  }

  public function execPurchaseDetailList($p) {
    $from = substr($p['dateFrom'], 0, 4) . '-' . substr($p['dateFrom'], 4, 2) . '-' . substr($p['dateFrom'], 6, 4);
    $to = substr($p['dateTo'], 0, 4) . '-' . substr($p['dateTo'], 4, 2) . '-' . substr($p['dateTo'], 6, 4);
    $params = array('creationDateFrom' => $from, 'creationDateTo' => $to);
    $result = new BookDetailRQ($params);
    $list_books = array();
    $rs = $this->request->execRequest($result,'XML_RQ', 'MB');
    $bookings = $rs->bookingRs->bookings;
    foreach ($bookings as $book) {
      $status = $book->status;
      if($status == 'Confirmed') {
        $status = \ServiceStatus::CONFIRMED;
      }
      else if($status == 'CancelledByCustomer') {
        $status = \ServiceStatus::CANCELLED;
      }
      $paxs_arr = array();
      foreach ($book->paxes as $pax) {
        $holder = $pax->name . ' ' . $pax->surname;
        $paxs_arr[] = strval($holder);
      }
      $list_books[] = array('localizer' => $book->locator, 'holder' => $paxs_arr, 'status_provider' => $status);
    }
    return $list_books;
  }
}
?>
