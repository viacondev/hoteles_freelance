<?php
namespace Methabookk;

class DestinationCodeMap {

  function __construct() {
  }

  public static function getDestCode($destCode) {
    $destinos = array('CUN' => '055458',
                      'BUE' => '016921',//Buenos Aires
                      'PCM' => '055459',// Riviera Maya017611
                      'HAV' => '067118', //HABANA
                      'MIA' => '056085',
                      'MCO' => '056109',//Orlando
                      'BCN' => '005015',//Barcelona
                      'NYC' => '063642',
                      'PUJ' => '020292',
                      'SMC' => '063528', //SANTA MARTA
                      'ADZ' => '111093', //SAN ANDRES
                      'VRA' => '109211', //varadero
                      'CTG' => '062799', //cartagena
                      'LAX' => '055690', //lOS ANGELES
                      'LVS' => '057423',
                      'PVR' => '018219',
                      'ACA' => '017988',//Acapulco
                      'WAS' => '062486',//Washington
                      'BOG' => '062542',
                      'MDQ' => '016966',
                      'RIO' => '065812',
                      'SAO' => '066779',//Bogota
                      'BCH' => '017570',
                      'IQQ' => '059642',
                      'MDE' => '062727',
                      'VRA' => '109211',//VARADERO
                      'PTY' => '111092',//PANAMA
                      'PAR' => '040682',//PARIS
                      'LIM' => '082497',//LIMA
                      'CUZ' => '082022',//Cusco
                      'PDG' => '064682',// Porto de Galinhas  
                      'FDI' => '063957',//Foz de Iguasu
                      'RUJ' => '066410',//GUARUJA
                      'RCF' => '064735',//Recife
                      'MVD' => '113144',//MONTEVIDEO
                      'PDP' => '113221',
                      'ASU' => '096265',//Asuncion
                      'MAD' => '006962',//MADRID
                      'SLT' => '113241',//SALTA
                      'CRD' => '113234',
                      'BZI' => '065749',
                      'SCL' => '059600',
                      'ARI' => '059381',
                      'KNA' => '059685',
                      'ROM' => '020298', //La Romana
                      'MEZ' => '016906',
                      'FLO' => '065922',
                      'BLN' => '065860',
                      'GOI' => '061236'
                      );
    if (isset($destinos[$destCode])) {
      return $destinos[$destCode];
    }
    return NULL;
  }
}

?>
