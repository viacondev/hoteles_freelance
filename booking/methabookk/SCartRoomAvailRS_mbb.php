<?php
namespace Methabookk;

class SCartRoomAvailRS extends \RoomAvail {

  private $data;
  private $date_in;
  private $date_out;
  public $childAges;
  private $rules;
  private $factorFee;
  private $totalAmountService;
  private $indexChild;
  public $hotel;
  public function __construct($data, $hotel, $date_in, $date_out, $childAges, $rules, $totalAmountService, $factorFee = 0.79, $indexChild) {
    $this->totalAmountService = $totalAmountService;
    $this->data = $data;
    $this->date_in = $date_in;
    $this->date_out = $date_out;
    $this->childAges = $childAges;
    $this->rules = $rules;
    $this->factorFee = $factorFee;
    $this->indexChild = $indexChild;
    $this->hotel = $hotel;
  }

  public function getProvider() {
    return \BookingProvider::METHABOOK2;
  }

  public function getHotel() {
    return $this->hotel;
  }

  public function getProviderName() {
    return 'METHABOOK2';
  }

  public function getHotelCode() {
    return '';
  }

  public function getRoomCount() {
    return $this->data->roomCount;
  }

  public function getAdultCount() {
    return $this->data->adultCount;
  }

  public function getChildCount() {
    return $this->data->childCount;
  }

  public function getPrice() {
    return $this->data->price;
  }

  public function getPriceWithFee() {
    return round($this->data->price / $this->factorFee, 2);
  }

  public function getSHRUI() {
    return $this->data->SHRUI;
  }

  public function getOnRequest() {
    return '';
  }

  public function getBoard() {
    return $this->data->board;
  }

  public function getBoardCode() {
    return $this->data->boardCode;
  }

  public function getBoardType() {
    return $this->data->boardType;
  }

  public function getRoomType() {
    return $this->data->roomType;
  }

  public function getRoomTypeType() {
    return $this->data->roomTypeType;
  }

  public function getRoomTypeCode() {
    return $this->data->roomTypeCode;
  }

  public function getRoomTypeCharacteristic() {
    return $this->data->roomTypeCharacteristic;
  }

  public function getCancellationPolicies() {
    $policies = array();
    $rules = $this->rules->valuationRS->availability->cancellationPolicy;
    $bookAmount = $this->totalAmountService;
    foreach ($rules->penaltyRules as $key => $value) {
      $type = $value->type;
      if ($type == 'NonRefundable') {
        $dateFrom = new \DateTime('now');
        $price    = $this->totalAmountService;
      }
      else if ($type == 'Amount') {
        $dateFrom = new \DateTime($value->dateFrom);
        $price    = $value->price->net;
      }
      else if ($type == 'Night') {
        $dateFrom = new \DateTime($value->dateFrom);
        $price    = ($bookAmount / $this->hotel->calculateNights()) * $value->nights;
      }
      else if ($type == 'Percentage') {
        $dateFrom = new \DateTime($value->dateFrom);
        $price    = ($bookAmount / 100) * $value->percentage;
      }
      else if ($type == 'NoShow') {
        $dateFrom = new \DateTime($value->dateFrom);
        $price = ($bookAmount / $this->hotel->calculateNights()) * $value->nights;
      }
      $cancellation = new \BookingCancellationPolicy();
      $cancellation->build($price, $dateFrom, '00:00', true, '', $this->factorFee);
      $policies[] = $cancellation;
    }

    return $policies;
  }

  public function getGuestList() {
    $guests = array();

    $customer_counter = 0;
    for ($i = 0; $i < $this->getAdultCount(); $i++) {
      $customer_counter++;
      $guests[] = new \BookingCustomer('AD', $customer_counter, '30', '', '');
    }

    $index_child = $this->indexChild;
    for ($i = 0; $i < $this->getChildCount(); $i++) {
      $customer_counter++;
      $guests[] = new \BookingCustomer('CH', $customer_counter, $this->childAges[$index_child], '', '');
      $index_child++;
    }

    return $guests;
  }

  public function buildObjecArray() {
    return array();
  }
}

?>
