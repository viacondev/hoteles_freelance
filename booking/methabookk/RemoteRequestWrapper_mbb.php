<?php
  include_once('../rest/ClientRest.php');

  $url = $_GET['url'];
  $header= (array)json_decode($_GET['header']);
  $method = $_GET['method'];
  $function = $_GET['type'];
  $params = (array)json_decode($_GET['params']);

  $rest = new Rest\ClientRest($url);
  $result = $rest->$function($method, $header, $params);
  echo $result->response;
?>
