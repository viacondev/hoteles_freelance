<?php
namespace Methabookk;


class ConfirmHotelAvail extends \HotelAvail {

  public $data;
  public $rules;
  public $roomsAvail;
  public $item;

  public function __construct($data, $item, $paxes, $totalAmount) {

    $this->item = $item;
    $this->data = $data;
    $this->rules = $data->cancellationPolicy;

    $from = $this->getDateFrom();
    $to = $this->getDateTo();

    $child_position = 0;
    $this->roomsAvail = array();

    foreach ($item->rooms as $room) {
      $arr_child = array();
      for ($i = 0; $i < $room->childCount; $i++) {
        $arr_child[] = $this->item->childAges[$child_position];
        $child_position++;
      }
      $this->roomsAvail[] = new ConfirmRoomAvail($room, $this, $this->getDateFrom(), $this->getDateTo(), $arr_child, $this->rules, $paxes, $totalAmount);
    }
  }

  public function getProvider() {
    return \BookingProvider::METHABOOK2;
  }

  public function getAvailToken() {
    return '';
  }

  public function getCode() {
    $hotelCode = $this->data->accommodationCode;
    return $hotelCode;
  }

  public function getName() {
    $hotelName = $this->data->description;
    return $hotelName;
  }

  public function getDateFrom() {
    $checkIn = explode('-', $this->data->checkInDate);
    $checkIn = $checkIn[2] . '-' . $checkIn[1] . '-' . $checkIn[0]; //Fromat YYYYMMDD
    return $checkIn;
  }

  public function getDateTo() {
    $checkOut = explode('-', $this->data->checkOutDate);
    $checkOut = $checkOut[2] . '-' . $checkOut[1] . '-' . $checkOut[0];
    return $checkOut;
  }

  public function getDestinationCode() {
    return $this->item->destCode;
  }

  public function getDestinationType() {
    return '';
  }

  public function getMainImage() {
    return '';
  }

  public function getDescription() {
    return '';
  }

  public function getLatitude() {
    return '';
  }

  public function getLongitude() {
    return '';
  }

  public function getZone() {
    return '';
  }

  public function getLocation() {
    return '';
  }

  public function getCategory() {
    return $this->item->category;
  }

  public function getCategoryType() {
    return '';
  }

  public function getCategoryCode() {
    return $this->item->category;//falta esta parte
  }

  public function getRoomsAvail() {
    return $this->roomsAvail;
  }

  public function getGroupedRooms() {
    return $this->roomsAvail;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getContractName() {
    return '';
  }

  public function getContractIncomingOfficeCode() {
    return '';
  }

  public function getContractComment() {
    // return $this->rules->getBookingRulesDesc();
    //revisar los comentario
    return '';
  }

  public function getClientComment() {
    return '';
  }

  public function getSupplier() {
    return '';
  }

  public function getSupplierVatNumber() {
    return '';
  }

  public function getDiscountList() {
    return array();
  }

  public function getAllPaxRequired() {
    return true;
  }

  public function getXtraDataRequired() {
    return true;
  }
}

?>
