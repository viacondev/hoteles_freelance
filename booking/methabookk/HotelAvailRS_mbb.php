<?php
namespace Methabookk;

include_once('RoomAvail_mbb.php');
include_once('model/HotelInfoModel.php');
include_once('model/Model.php');

class HotelAvailRS extends \HotelAvail {

  private $element;
  private $destCode;
  private $rooms;
  private $checkin;
  private $checkout;
  private $hotelName;
  private $accommodationCode;
  private $hotelinfo;
  public  $currency;
  public  $hotelZionCode;
  public  $hotelbd;
  public function __construct($params) {
    $this->hotelbd   = $params['hoteldesc'];
    $this->element   = $params['availabilities'];
    $this->destCode  = $params['destCode'];
    $this->checkin   = $params['checkin'];
    $this->checkout  = $params['checkout'];
    $this->hotelName = $params['hotelName'];
    $this->accommodationCode  = $params['accommodationCode'];
    $this->hotelZionCode      = $params['accommodationCode'];
    $this->currency   = $this->element[0]->price->currencyCode;
    $this->hotelinfo  = $params['hotelinfo'];
    $roomCount        = count($this->element[0]->rooms);
    $factorFee        = $params['factorFee'];
    $regimen          = $params['regimen'];
    $this->roomsAvail = array();
    $rooms_numbers    = $params['rooms_numbers'];
    $arrBoards = array(
      'AI'  => 'TI', //Todo Incluido Codigo MBB=>GENERAL
      'BB'  => 'AD',  //Alojamiento y desayuno
      'OR'  => 'SA', //Solo Alojamiento
      'RO'  => 'SA',
      'NO'  => 'SA',
      'PC'  => 'FB', //Pension Completa
      'FB'  => 'FB',
      'MP'  => 'HB', //Media Pension
      'HB'  => 'HB'
    );
    foreach ($this->element as $value) {
      $groupedRooms = array();
      $cont = 0;
      $price_net = $value->price->gross / $roomCount;
      foreach ($value->rooms as $room) {
        $availabilitiCode = $value->code;
        $r = $rooms_numbers[$cont];
        $adultCount =  $r->getadultCount();
        $childCount = $r->getchildCount();
        $rooms_group = new RoomAvail($this, $room, $value->mealPlan, $price_net, $availabilitiCode, $roomCount, $adultCount, $childCount, $this->accommodationCode, $factorFee);
        if($regimen != '') {
          $boardCode = $rooms_group->getBoardCode();
          if(in_array($arrBoards[$boardCode], $regimen)) {
            $groupedRooms[] = $rooms_group;
            $cont++;
          }
        }
        else {
          $groupedRooms[] = $rooms_group;
          $cont++;
        }
      }
      if (count($groupedRooms) > 0) {
        $this->roomsAvail[] = $groupedRooms;
        $this->insertGroupedRoom($groupedRooms);
      }
    }


  }

  public function getProvider() {
    return \BookingProvider::METHABOOK2;
  }

  public function getProviderName() {
    return 'METHABOOK2';
  }

  public function getAvailToken() {
    return $this->accommodationCode;
  }

  public function getCode() {
    //el accommodationCode es el codigo de Hotel
    return $this->accommodationCode;
  }

  public function getHotelZionCode() {
    return $this->hotelZionCode;
  }
  
  public function getName() {
    return htmlspecialchars($this->hotelName, ENT_QUOTES, 'UTF-8');
  }

  public function getDateFrom() {
    return $this->checkin;
  }

  public function getDateTo() {
    return $this->checkout;
  }

  public function getDestinationCode() {
    return $this->destCode;
  }

  public function getDestinationType() {
    return '';
  }

  public function getMainImage() {
    $images = isset($this->hotelbd->images) ? $this->hotelbd->images : "";
    $img = json_decode($images);
    if(isset($img[0])) {
      return $img[0]->Url;
    }
    return 'assets/images/no-photo.jpg';
  }

  public function getDescription() {
    $msg = "informaci&oacute;n dentro de poco";
    if (isset($this->hotelbd->hotel_description)) {
      return $this->hotelbd->hotel_description;
    }
    return $msg;
  }

  public function getLatitude() {
    if ($this->hotelinfo->coordinates->latitude) {
      return $this->hotelinfo->coordinates->latitude;
    }
    return NULL;
  }

  public function getLongitude() {
    if ($this->hotelinfo->coordinates->longitude) {
      return $this->hotelinfo->coordinates->longitude;
    }
    return NULL;
  }

  public function getZone() {
    return '';
  }

  public function getLocation() {
    return isset($this->hotelbd->address) ? $this->hotelbd->address : '';
  }

  public function getCategory() {
    $cat = explode('*', $this->hotelinfo->category->code);
    return $cat[0] . 'EST';
  }

  public function getCategoryType() {
    return '';
  }

  public function getCategoryCode() {
    $cat = explode('*', $this->hotelinfo->category->code);
    return $cat[0] . 'EST';
  }

  public function getRoomsAvail() {
    return $this->roomsAvail;
  }

  public function getCurrency() {
    return $this->currency;
  }

  public function getContractName() {
    return '';
  }

  public function getContractIncomingOfficeCode() {
    return '';
  }

  public function getContractComment() {
    return '';
  }

  public function getClientComment() {
    return '';
  }

  public function getSupplier() {
    return '';
  }

  public function getSupplierVatNumber() {
    return '';
  }

  public function getDiscountList() {
    return array();
  }

  public function getAllPaxRequired() {
    return true;
  }

  public function getXtraDataRequired() {
    return true;
  }
}
?>
