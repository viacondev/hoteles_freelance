<?php
namespace Methabookk;

include_once('PurchasedRoomRS_mbb.php');

class PurchasedHotelRS extends \HotelAvail {

  private $element;
  private $rooms;

  public function __construct($element, $paxes) {
    $this->element = $element;

    $this->rooms = array();
    foreach ($this->element->rooms as $room) {
      $board = $this->element->mealPlan;
      $paxs = $room->paxes;
      $this->rooms[] = new PurchasedRoomRS($room, $this, $board, $paxs, $paxes);
    }
  }

  public function getProvider() {
    return \BookingProvider::METHABOOK;
  }

  public function getProviderName() {
    return 'METHABOOK';
  }

  public function getAvailToken() {
    return '';
  }

  public function getCode() {
    $hotelCode = $this->element->accommodationCode;
    return $hotelCode;
  }

  public function getName() {
    $hotelName = $this->element->description;
    return $hotelName;
  }

  public function getDateFrom() {
    $checkIn = explode('-', $this->element->checkInDate);
    $checkIn = $checkIn[2] . $checkIn[1] . $checkIn[0]; //Fromat YYYYMMDD
    return $checkIn;
  }

  public function getDateTo() {
    $checkOut = explode('-', $this->element->checkOutDate);
    $checkOut = $checkOut[2] . $checkOut[1] . $checkOut[0];
    return $checkOut;
  }

  public function getDestinationCode() {
    // TODO
    return '';
  }

  public function getDestinationType() {
    return '';
  }

  public function getMainImage() {
    return '';
  }

  public function getDescription() {
    return '';
  }

  public function getLatitude() {
    return '';
  }

  public function getLongitude() {
    return '';
  }

  public function getZone() {
    return '';
  }

  public function getLocation() {
    return '';
  }

  public function getCategory() {
    return '';
  }

  public function getCategoryType() {
    return '';
  }

  public function getCategoryCode() {
    // TODO
    return '';
  }

  public function getRoomsAvail() {
    return $this->rooms;
  }

  public function getGroupedRooms() {
    return $this->rooms;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getContractName() {
    return '';
  }

  public function getContractIncomingOfficeCode() {
    return '';
  }

  public function getContractComment() {
    return '';
  }

  public function getClientComment() {
    return '';
  }

  public function getSupplier() {
    return 'Methabook';
  }

  public function getSupplierVatNumber() {
    return '';
  }

  public function getDiscountList() {
    return array();
  }

  public function getAllPaxRequired() {
    return true;
  }

  public function getXtraDataRequired() {
    return true;
  }
}

?>
