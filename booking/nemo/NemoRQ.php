<?php

namespace Nemo;
// ini_set('display_errors', 1);
class NemoRQ
{

  private $location;
  private $client;
  private $user;
  private $pass;
  private $wsdl;
  public function __construct()
  {
    // ini_set('soap.wsdl_cache_enabled', '0');
    // ini_set('soap.wsdl_cache_ttl', '0');

    // $this->location  = \THConfig::$location;
    // $this->user = \THConfig::$agencyName;
    // $this->pass = \THConfig::$agencyCode;
    // $this->wsdl = \THConfig::$wsdl;

  }

  public function getRequestXML($RQElement)
  {

    //$res = $RQElement->getParams();
    //$xml = str_replace('__POS__', $autentication, $res);
    $xml =  $RQElement->getParams();
    return $xml;
  }

  public function execRequest($RQElement, $solicitud = "", $type = "")
  {
    // $request = $this->getRequestXML($RQElement->getParams());
    $url = \NMConfig::$url . $RQElement->getMethodName();

    $header = array();
    // $header[] = 'Cache-Control: no-cache';
    // $header[] = 'Content-Type: text/xml';
    $header[] = 'Accept: application/xml';
    // $header[] = 'Accept-Encoding: gzip,deflate';
    $header[] = 'Cache-Control: no-cache';
    $header[] = 'Connection: Keep-Alive';
    $header[] = 'Content-Type: application/xml';
    $header[] = 'User-Agent: Apache-HttpClient/4.1.1 (java 1.5)';
    $header[] = 'X-PS-AUTHTOKEN: ' . \NMConfig::$token;
    //$header[] = 'SOAPAction: ' . $wsdl . $RQElement->getMethodName();

    if (isset(\NMConfig::$DirectXMLAccess) && \NMConfig::$DirectXMLAccess) { //falta incluir el archivo rest para que funcione
      $ch = curl_init();
      curl_setopt_array(
        $ch,
        array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          /*CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 30,*/
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $this->getRequestXML($RQElement),
          CURLOPT_HTTPHEADER => $header,
          CURLOPT_USERAGENT => $_SERVER['HTTP_USER_AGENT']
        )
      );
      $xml = curl_exec($ch);
      curl_close($ch);
    } else {
      $urlWrapper =  'https://agente.barrybolivia.com/hotel-ws-v2/app/Http/Controllers/booking/nemo/RemoteRequestWrapper_nm.php';
      $data = [
        'url' => $url, // URL de destino
        'header' => json_encode($header),
        'request' => $this->getRequestXML($RQElement),
        'apiKey' => 'b@rr1b0l',
      ];

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $urlWrapper);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data)); // Convertir datos a JSON
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, [
        'Content-Type: application/json',
      ]);
      $xml = curl_exec($ch);
      curl_close($ch);
    }
    if ($solicitud == 'XML_RQ') {
      $descrip = $type;
      \Log::info($this->getRequestXML($RQElement), $descrip);
    }
    // __logtxt($this->getRequestXML($RQElement));
    // __logtxt($xml); // RS
    return $xml;
  }
  public function allOptions($RQElement)
  {
    $url = \NMConfig::$url . $RQElement->getMethodName();

    $header = array();
    // $header[] = 'Cache-Control: no-cache';
    // $header[] = 'Content-Type: text/xml';
    $header[] = 'Accept: application/xml';
    $header[] = 'Accept-Encoding: gzip,deflate';
    $header[] = 'Cache-Control: no-cache';
    $header[] = 'Connection: Keep-Alive';
    $header[] = 'Content-Type: application/xml';
    $header[] = 'User-Agent: Apache-HttpClient/4.1.1 (java 1.5)';
    $header[] = 'X-PS-AUTHTOKEN: ' . \NMConfig::$token;


    if (isset(\NMConfig::$DirectXMLAccess) && \NMConfig::$DirectXMLAccess) {
      $options  = array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $this->getRequestXML($RQElement),
        CURLOPT_HTTPHEADER => $header,
        CURLOPT_USERAGENT => $_SERVER['HTTP_USER_AGENT']
      );
    } else {
      $rq = 'http://test.barrybolivia.com/booking/tourico/RemoteRequestWrapper_th.php';
      $rq .= '?url=' . urlencode($url);
      $rq .= '&header=' . urlencode(json_encode($header));
      $rq .= '&request=' . urlencode($this->getRequestXML($RQElement));
      $options  = array(
        CURLOPT_AUTOREFERER => true,
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL             => $rq
      );
    }
    return $options;
  }
}
