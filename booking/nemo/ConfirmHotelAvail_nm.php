<?php
namespace Nemo;


class ConfirmHotelAvail extends \HotelAvail {

  public $data;
  public $rules;
  public $roomsAvail;
  public $item;

  public function __construct($data, $item, $totalAmount) {
    $this->item = $item;
    $this->data = $data->Products->Hotels->Hotel;
    
    $this->rules = $data->Products->Hotels->Hotel->Rate->ChargeConditions;
    $deadLine    = $data->Products->Hotels->Hotel->Rate;
    $rooms       = $data->Products->Hotels->Hotel->Rate;
    $roomCount   = count($rooms->Rooms->Room);
    $con = 0;
    foreach ($rooms->Rooms->Room as $room) {
      // $res        = $room->attributes();
      // $totalRoom  = floatval($res['price']);
      // $arr_child  = array();
      // for ($i = 0; $i < $room->childCount; $i++) {
      //   $arr_child[] = $this->item->childAges[$child_position];
      //   $child_position++;
      // }
      $roomsItem  = $this->item->rooms[$con];
      $checkin    = $this->data->CheckIn;
      $board      = strval($rooms->Board);
      $totalPrice = floatval($rooms->RatePrices->RatePrice) / $roomCount;
      $this->roomsAvail[] = new ConfirmRoomAvail($room, $this, $totalPrice, $board, $roomsItem, $checkin, $this->rules, $deadLine);
      $con++;
    }

  }

  public function getProvider() {
    return \BookingProvider::NEMO;
  }

  public function getProviderName() {
    return \BookingProvider::getProviderName(\BookingProvider::NEMO);
  }
  
  public function getAvailToken() {
    return '';
  }

  public function getCode() {
    // $hotelCode = $this->data->Hotel->attributes();
    $hotelCode = $this->item->hotelCode;
    return strval($hotelCode);
  }

  public function getName() {
    $hotelName = $this->data->Hotel->HotelName;
    return strval($hotelName);
  }

  public function getDateFrom() {
    $res = $this->data->CheckIn;
    $checkin = explode('-', $res[0]);
    $checkin = $checkin[0] . $checkin[1] . $checkin[2];
    return $checkin;
  }

  public function getDateTo() {
    $res = $this->data->CheckOut;
    $checkOut = explode('-', $res[0]);
    $checkOut = $checkOut[0] . $checkOut[1] . $checkOut[2];
    return $checkOut;
  }

  public function getDestinationCode() {
    return $this->item->destCode;
  }

  public function getDestinationType() {
    return '';
  }

  public function getMainImage() {
    return '';
  }

  public function getDescription() {
    return '';
  }

  public function getLatitude() {
    return '';
  }

  public function getLongitude() {
    return '';
  }

  public function getZone() {
    return '';
  }

  public function getLocation() {
    return '';
  }

  public function getCategory() {
    $category = intval($this->data->Hotel->HotelRating->RatingValue);

    return $category . 'EST';
  }

  public function getCategoryType() {
    $categoryType = $this->data->Reservations->Reservation->ProductInfo->attributes();
    return strval($categoryType['roomTypeCategory']);
  }

  public function getCategoryCode() {
    $category = intval($this->data->Hotel->HotelRating->RatingValue);

    return $category . 'EST';
  }

  public function getRoomsAvail() {
    return $this->roomsAvail;
  }

  public function getGroupedRooms() {
    return $this->roomsAvail;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getContractName() {
    return '';
  }

  public function getContractIncomingOfficeCode() {
    return '';
  }

  public function getContractComment() {
    return '';
  }

  public function getClientComment() {
    return '';
  }

  public function getSupplier() {
    return '';
  }

  public function getSupplierVatNumber() {
    return '';
  }

  public function getDiscountList() {
    return array();
  }

  public function areAllPaxRequired() {
    return true;
  }

  public function isExtraDataRequired() {
    return true;
  }

  public function isDataRequired() {
    return false;
  }

  public function getIsWithPromotion() {
    return array();
  }
}

?>
