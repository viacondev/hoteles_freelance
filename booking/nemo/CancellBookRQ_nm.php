<?php
namespace Nemo;

class CancellBookRQ {

  private $localizer;

  public function __construct($localizer) {
    $this->localizer = $localizer;
  }

  public function getType() {
    return 'reservationService';
  }

  public function getMethodName() {
    return 'booking/cancel';
  }

  public function getParams() {
    $xml = '
        <BookingCancellationRQ>
          <GeneralParameters>
            <PreferedLanguage LanguageCode="es"/>
            <PreferedCurrency CurrencyCode="USD"/>
          </GeneralParameters>
          <Bookings>
            <Booking BookingReference="' . $this->localizer . '"/>
          </Bookings>
        </BookingCancellationRQ>';

    return $xml;
  }
}
?>
