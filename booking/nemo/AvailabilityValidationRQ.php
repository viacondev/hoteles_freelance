<?php
namespace Nemo;

class AvailabilityValidationRQ {

  public $tripProduductId;
  public function __construct($tripProduductId) {
    $this->tripProduductId  = $tripProduductId;
  }

  public function getType() {
    return 'hotelService';
  }

  public function getMethodName() {
    return 'catalog/product/validate';
  }

  public function getParams() {
    
    $xml = '
      <AvailabilityValidationRQ>
        <GeneralParameters>
          <PreferedLanguage LanguageCode="es"/>
          <PreferedCurrency CurrencyCode="ARS"/>
        </GeneralParameters>
        <Products>
          <Hotels>
            <Hotel TripProductID="' . $this->tripProduductId . '"/>
          </Hotels>
        </Products>
      </AvailabilityValidationRQ>';

    return $xml;
  }
}

?>
