<?php
namespace Nemo;

class PurchaseRS extends \BookingPurchase {

  public $services;
  private $total_price;

  /**
   * @total_price is used for cancellation fees. For a Purchase it is ok to leave it as 0.
   */
  public function __construct($services, $total_price = 0) {
    $this->services = $services;
    $this->total_price = $total_price;
  }

  public function getPurchaseToken() {
    return '';
  }

  public function getStatus() {
    return '';
  }

  public function getHolder() {
    return '';
  }

  public function getBookingServices() {
    return $this->services;
  }

  public function getTotalPrice() {
    return $this->total_price;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getCreationDate() {
    return '00000000';
  }

  public function getReferenceFileNumber() {
    return '';
  }

  public function getReferenceIncomingOffice() {
    return '';
  }

  public function getAgencyReference() {
    return '';
  }

  public function getPaymentDataDescription() {
    return '';
  }
}

?>
