<?php
namespace Nemo;

include_once('PurchasedRoomRS_nm.php');

class PurchasedHotelRS extends \HotelAvail {

  private $element;
  private $rooms;

  public function __construct($element) {
    $this->element = $element;
    $this->rooms = array();
    $rooms = $element->Products->Hotels->Hotel->Rate->Rooms->Room;
    $board = strval($element->Products->Hotels->Hotel->Rate->Board);
    foreach ($rooms as $value) {
      $roomType = strval($value->RoomDescription);
      $this->rooms[] = new PurchasedRoomRS($value, $board, $roomType, $this);
    }

  }

  public function getProvider() {
    return \BookingProvider::TAILORBEDS;
  }

  public function getProviderName() {
    return 'TAILORBEDS';
  }

  public function getAvailToken() {
    return '';
  }

  public function getCode() {
    $attrs = $this->element->Products->Hotels->Hotel->attributes();
    return strval($attrs['HotelCode']);
  }

  public function getName() {
    $attrs = $this->element->Products->Hotels->Hotel->Hotel->HotelName;
    return strval($attrs);
  }

  public function getDateFrom() {
    $res = $this->elements->Products->Hotels->Hotel->CheckIn;
    $checkin = explode('-', $res);
    $checkin = $checkin[0] . $checkin[1] . $checkin[2];
    return $checkin;
  }

  public function getDateTo() {
    $res = $res = $this->elements->Products->Hotels->Hotel->CheckOut;
    $checkOut = explode('-', $res);
    $checkOut = $checkOut[0] . $checkOut[1] . $checkOut[2];
    return $checkOut;
  }

  public function getDestinationCode() {
    // TODO
    return '';
  }

  public function getDestinationType() {
    return '';
  }

  public function getMainImage() {
    return '';
  }

  public function getDescription() {
    return '';
  }

  public function getLatitude() {
    return '';
  }

  public function getLongitude() {
    return '';
  }

  public function getZone() {
    return '';
  }

  public function getLocation() {
    return '';
  }

  public function getCategory() {
    return '';
  }

  public function getCategoryType() {
    return '';
  }

  public function getCategoryCode() {
    // TODO
    return '';
  }

  public function getRoomsAvail() {
    return $this->rooms;
  }

  public function getGroupedRooms() {
    return $this->rooms;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getContractName() {
    return '';
  }

  public function getContractIncomingOfficeCode() {
    return '';
  }

  public function getContractComment() {
    return strval("");
  }

  public function getClientComment() {
    return '';
  }

  public function getSupplier() {
    return 'TailorBeds';
  }

  public function getSupplierVatNumber() {
    return '';
  }

  public function getDiscountList() {
    return array();
  }

  public function areAllPaxRequired() {
    return true;
  }

  public function isExtraDataRequired() {
    return true;
  }

  public function isDataRequired() {
    return false;
  }

  public function getIsWithPromotion() {
    return array();
  }
}

?>
