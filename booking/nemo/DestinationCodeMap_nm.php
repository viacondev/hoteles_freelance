<?php
namespace Nemo;

class DestinationCodeMap {

  function __construct() {
  }

  public static function getDestCode($destCode) {
    $destinos = array('PUJ' => '6128',
                      'CUN' => '3703',
                      'BUE' => '5694',
                      'MIA' => '4490',
                      'CTG' => '6061',
                      'MDE' => '6071',
                      'NYC' => '4584',
                      'MCO' => '4619',
                      'VRA' => '6119',
                      'SMA' => '22560',
                      'BZI' => '5798',
                      'FLO' => '5826',
                      'FDI' => '5837',
                      'RIO' => '5948',
                      'HAV' => '6110',
                      'RCF' => '5945',
                      'NYC' => '44199',
                      'PTY' => '6222',
                      'ADZ' => '6049',
                      'PVR' => '3745', //PUERTO VALLARTA
                      'IQQ' => '6030',
                      'CAB' => '3752',//LOS CABOS
                      'PAR' => '2574',
                      'MVD' => '6268',
                      'AUA' => '5752',// ARUBA
                      'ARI' => '6021',//ARICA
                      'SMA' => '22560',//CAYO SANTA MARIA
                      'PCM' => '22506'// RIVIERA MAYA
                      );
    if (isset($destinos[$destCode])) {
      return $destinos[$destCode];
    }
    return NULL;
  }
}

?>
