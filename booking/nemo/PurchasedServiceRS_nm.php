<?php
namespace Nemo;

/**
 * Class used for confirmed Services
 */
class PurchasedServiceRS extends \BookingService {

  private $elements;

  public function __construct($rs) {
    $this->elements = $rs->Details->Bookings->Booking;
  }

  public function getLocalizer() {
    $localizer = $this->elements->BookingReference;
    return strval($localizer);
  }

  public function getSPUI() {
    return '';
  }

  public function getServiceType() {
    return 'ServiceHotel';
  }

  public function getServiceStatus() {
    return '';
  }

  public function getDirectPayment() {
    return '';
  }

  public function getDateFrom() {
    $res = $this->elements->Products->Hotels->Hotel->CheckIn;
    $checkin = explode('-', $res);
    $checkin = $checkin[0] . $checkin[1] . $checkin[2];
    return $checkin;
  }

  public function getDateTo() {
    $res = $res = $this->elements->Products->Hotels->Hotel->CheckOut;
    $checkOut = explode('-', $res);
    $checkOut = $checkOut[0] . $checkOut[1] . $checkOut[2];
    return $checkOut;
  }

  public function getCurrency() {
    return strval('USD');
  }

  public function getTotalAmount() {
    $totalAmount = $this->elements->ResGroup->attributes();
    return floatval($totalAmount['totalPrice']);
  }

  public function getAdditionalCosts() {
    return array();
  }

  public function getServiceInfo() {
    $accommodations = $this->elements;
    return new PurchasedHotelRS($accommodations);
  }

  public function getSupplementList() {
    return array();
  }

  public function getStatus() {
    $status = $this->elements->Products->Hotels->Hotel->BookingStatusList->BookingStatus->BookingStatusDescription->attributes();
    if (strval($status['BookingStatusCode']) == "NMO.HTL.BST.CNF") {
      return \ServiceStatus::CONFIRMED;
    }
    else {
     return \ServiceStatus::CANCELLED; 
    }
  }

  public function getErrors() {
    return $this->errors;
  }

}

?>
