<?php
namespace Nemo;

class RoomAvail extends \RoomAvail {

  private $element;
  public $hotel;
  public $hotelCode;
  public $adultCount;
  public $childCount;
  public $roomCount;
  public $price;
  public $childAges;
  public $board;
  public $boardCode;
  public $roomType;
  public $SHRUI;
  public $roomTypeCode;
  public $priceWithFee;

  public function __construct($element, $price, $params, $hotel, $factorFee = 0.79) {
    $this->element    = $element;
    $this->price      = $price;
    $this->priceWithFee = round($price / $factorFee, 2);
    $this->board      = $params['board'];
    $this->boardCode  = $params['boardCode'];
    $this->hotel      = $hotel;
    $this->hotelCode  = $params['hotelCode'];
    $this->hotel      = json_encode($this->buildObjecArray());
    $this->roomType   = $params['roomType'];
    $this->SHRUI      = $params['SHRUI'];
    $this->roomTypeCode = $params['roomTypeCode'];
    $this->adultCount = $params['adultCount'];
    $this->childCount = $params['childCount'];
    $childAges = $params['childAges'];
    $this->childAges = $childAges->getChildAges();
  }

  public function getProvider() {
    return \BookingProvider::NEMO;
  }

  public function getHotel() {
    return $this->hotel;
  }

  public function getProviderName() {
    return 'NEMO';
  }

  public function getHotelCode() {
    // return strval($this->hotelCode);
    return strval($this->hotelCode);
  }

  public function getRoomCount() {
    // return $this->roomCount;
    return 1;
  }

  public function getAdultCount() {
    return $this->adultCount;
  }

  public function getChildCount() {
    return $this->childCount;
  }

  public function getChildAges() {
    return $this->childAges;
  }

  public function getPrice() {
    return round($this->price, 2);
  }

  public function getPriceWithFee() {
    return $this->priceWithFee;
  }

  public function getSHRUI() {
    return $this->SHRUI;
  }

  public function getOnRequest() {
    return '';
  }

  public function getBoard() {
    return $this->board == 'None' ? 'Solo Alojamiento' : $this->board;
  }

  public function getBoardCode() {
    $arrCode = array('7'       => 'TI', //Todo ICLUIDO
                     '5'       => 'HB',//MEDIA PENSION
                     'HB'      => 'HB',
                     '1'       => 'SA', //Solo Habitacion
                     '8'       => 'SA', //Solo Habitacion
                     'SA'      => 'SA',
                     '2'       => 'BB', //Alojamiento y Desayuno
                     'BB'      => 'BB',
                     '6'       => 'FB' //Pension Completa
                     );
    if (isset($arrCode[$this->boardCode])) {
      return $arrCode[$this->boardCode];
    }
    return $this->boardCode;
  }

  public function getBoardType() {
    return '';
  }

  public function getRoomType() {
    return $this->roomType;
  }

  public function getRoomTypeType() {
    return "";
  }

  public function getRoomTypeCode() {
    return $this->roomTypeCode;
  }

  public function getRoomTypeCharacteristic() {
    return '';
  }

  public function getCancellationPolicies() {
    return array();
  }

  public function getGuestList() {
    return array();
  }

  public function buildObjecArray() {
    $con = 0;
    $hotel = $this->hotel;
    $methods = get_class_methods($hotel);
    $objArray = array();
    foreach ($methods as $method) {
      if (substr($method, 0, 2) == '__') continue;
      if (substr($method, 0, 3) === 'get' && $method != 'getGroupedRooms' && $method != 'getRoomsAvail') {
        $key = lcfirst(substr($method, 3));
        $objArray[$key] = $hotel->$method();
        $con++;
      }
    }
    return $objArray;
  }

}

?>
