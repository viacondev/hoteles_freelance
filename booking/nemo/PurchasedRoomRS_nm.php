<?php
namespace Nemo;

class PurchasedRoomRS extends \RoomAvail {

  private $elements;
  public $board;
  private $guests;
  public $roomType;

  public function __construct($elements, $board, $roomType, $hotel) {
    $this->elements   = $elements;
    $this->board      = $board;
    $this->roomType   = $roomType;
    $this->adultCount = 0;
    $this->childCount = 0;
    $this->guests     = array();
    // $room = $this->elements->ProductInfo->RoomExtraInfo->RoomInfo;
    $i = 0;
    // $pax = $this->elements->ProductInfo->Passenger->attributes();
    // $i++;
    // $this->guests[] = new \BookingCustomer('AD', $i, 30, strval($pax['firstName']), strval($pax['lastName']));
    // foreach ($room->ChildAges->ChildAge as $key => $value) {
    //   $age = $value->attributes();
    //   $i++;
    //   $type = 'CH';
    //   $this->guests[] = new \BookingCustomer($type, $i, strval($age['age']), '', '');
    // }
    foreach ($this->elements->Occupancy->Passengers->Passenger as $guest) {
      $i++;
      $attrs    = $guest->attributes();
      $type     = $attrs['AgeType'] == 'NMO.GBL.AGT.ADT' ? 'AD' : 'CH';
      $age      = isset($guest->Age) ? $guest->Age : '30';
      $name     = $guest->PersonNames->PersonName[0];
      $lastName = /*$guest->PersonNames->PersonName[1]*/"";
      $this->guests[] = new \BookingCustomer($type, $i, $age, $name, $lastName);
      if ($type == 'AD') {
        $this->adultCount++;
      }
      else if ($type == 'CH') {
        $this->childCount++;
      }
    }
  }

  public function getProvider() {
    return \BookingProvider::TAILORBEDS;
  }

  public function getHotel() {
    return NULL;
  }

  public function getRoomCount() {
    return 1;
  }

  public function getAdultCount() {
    return intval($this->elements->Occupancy->AdultsCount);
  }

  public function getChildCount() {
    return intval($this->elements->Occupancy->ChildrenCount);
  }

  public function getPrice() {
    // TODO: return price
    return 0;
  }

  public function getSHRUI() {
    return '';
  }

  public function getOnRequest() {
    return '';
  }

  public function getBoard() {
    return $this->board;
  }

  public function getBoardCode() {
    return '';
  }

  public function getBoardType() {
    return '';
  }

  public function getRoomType() {
    return $this->roomType;
  }

  public function getRoomTypeType() {
    return '';
  }

  public function getRoomTypeCode() {
    return '';
  }

  public function getRoomTypeCharacteristic() {
    return '';
  }

  public function getCancellationPolicies() {
    return array();
  }

  public function getGuestList() {
    return $this->guests;
  }
}

?>
