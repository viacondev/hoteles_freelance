<?php
namespace Nemo;

include_once('NemoRQ.php');
include_once('NemoRS.php');
include_once('Room_nm.php');
include_once('AvailableHotelsRQ_nm.php');
include_once('HotelAvailListRS_nm.php');
include_once('PurchaseRS_nm.php');
include_once('SCartServiceRS_nm.php');
include_once('HotelBookingRuleRQ_nm.php');
include_once('HotelReservationRQ_nm.php');
include_once('AvailabilityValidationRQ.php');
include_once('ReadServiceRQ_nm.php');
include_once('PurchasedServiceRS_nm.php');
include_once('PurchasedHotelRS_nm.php');
include_once('PurchasedRoomRS_nm.php');
include_once('HotelDetailRQ_nm.php');
include_once('HotelDetailRS_nm.php');
include_once('CancellBookRQ_nm.php');
include_once('ServiceRS_nm.php');
include_once('ConfirmHotelAvail_nm.php');
include_once('ConfirmRoomAvail_nm.php');
include_once('DestinationCodeMap_nm.php');

class NemoBooking {

  private $request;

  public function __construct() {
    $this->request = new NemoRQ();
  }

  public function execRequest($params) {
    switch ($params['requestType']) {
      case \BookingRequestTypes::SearchHotelAvail:
        return $this->execHotelValuedAvailRQ($params);
        break;

      case \BookingRequestTypes::AddHotelService:
        return $this->execHotelServiceAddRQ($params);
        break;

      case \BookingRequestTypes::ConfirmPurchase:
        return $this->execPurchaseConfirmRQ($params);
        break;

      case \BookingRequestTypes::PurchaseDetail:
        return $this->execPurchaseDetailRQ($params);
        break;

      case \BookingRequestTypes::PurchaseList:
        return $this->execPurchaseListRQ($params);
        break;

      case \BookingRequestTypes::SearchTicketAvail:
        return $this->execTicketAvailRQ($params);
        break;

      case \BookingRequestTypes::HotelDetail:
        return $this->execHotelDetailRQ($params);
        break;

      case \BookingRequestTypes::CancelPurchase:
        return $this->execPurchaseCancelRQ($params);
        break;

      case \BookingRequestTypes::SearchHotelsByDest:
        return $this->execSearchHotelsByDestRQ($params);
        break;

      case \BookingRequestTypes::SearchZones:
        return $this->execSearchZonesRQ($params);
        break;

      default:
        return NULL;
        break;
    }
  }

  public function execHotelValuedAvailRQ($p) {
    $destCode = DestinationCodeMap::getDestCode($p['destCode']);
    if ($destCode == NULL /*|| intval($p['roomCount']) > 2*/) return array();

    $roomCount      = intval($p['roomCount']);
    $rooms          = array();
    $rooms_numbers  = array();
    $childIndex = 1;
    for ($i = 1;$i <= $roomCount; $i++) {
      $adultCount = intval($p["adultCount_$i"]);
      $childCount = intval($p["childCount_$i"]);
      $occupancy  = array();
      $childAges  = array();
      for  ($d = 1; $d <= intval($p["adultCount_$i"]); $d++) {
        $occupancy[] = 30;
      }

      for ($c = 1; $c <= intval($p["childCount_$i"]); $c++) {
        $occupancy[] = $p["childAge_$childIndex"];
        $childAges[] = $p["childAge_$childIndex"];
        $childIndex++;
      }
      $rooms_numbers[]  = new Room($adultCount, $childCount, $childAges);
      $rooms[]          = $occupancy;
    }

    $checkin    = substr($p['checkin'], 0, 4) . '-' . substr($p['checkin'], 4, 2) . '-' . substr($p['checkin'], 6, 4);
    $checkout   = substr($p['checkout'], 0, 4) . '-' . substr($p['checkout'], 4, 2) . '-' . substr($p['checkout'], 6, 4);

    $hotelAvailRQ = new AvailableHotelsRQ($destCode, $checkin, $checkout, $rooms_numbers);
    $xml          = $this->request->execRequest($hotelAvailRQ);
    $res          = new NemoRS($xml);

    $hotelList = new HotelAvailListRS($res, $p['checkin'], $p['checkout'], $roomCount, $rooms_numbers, $p['factorFee']);
    
    return $hotelList;
  }

  public function prepararConsulta($p) {
    $destCode = DestinationCodeMap::getDestCode($p['destCode']);
    if ($destCode == NULL /*|| $p['hotelCode'] != ""*/ || !empty($p['zoneCode'])) {
      return NULL;
    }
    $roomCount      = intval($p['roomCount']);
    $rooms          = array();
    $rooms_numbers  = array();
    $hotelCode      = $p['hotelCode'];
    // if ($p['hotelCode'] != '') {
      // $hotelCode = explode('_', $p['hotelCode']);
      // $hotelCode = $hotelCode[0];
    // }
    
    $childIndex     = 1;
    for ($i = 1;$i <= $roomCount; $i++) {
      $adultCount = intval($p["adultCount_$i"]);
      $childCount = intval($p["childCount_$i"]);
      $occupancy  = array();
      $childAges  = array();
      for  ($d = 1; $d <= intval($p["adultCount_$i"]); $d++) {
        $occupancy[] = 30;
      }

      for ($c = 1; $c <= intval($p["childCount_$i"]); $c++) {
        $occupancy[] = $p["childAge_$childIndex"];
        $childAges[] = $p["childAge_$childIndex"];
        $childIndex++;
      }
      $rooms_numbers[] = new Room($adultCount, $childCount, $childAges);
      $rooms[] = $occupancy;
    }

    $checkin    = substr($p['checkin'], 0, 4) . '-' . substr($p['checkin'], 4, 2) . '-' . substr($p['checkin'], 6, 4);
    $checkout   = substr($p['checkout'], 0, 4) . '-' . substr($p['checkout'], 4, 2) . '-' . substr($p['checkout'], 6, 4);

    $hotelAvailRQ = new AvailableHotelsRQ($destCode, $checkin, $checkout, $rooms_numbers, $hotelCode);
    
    return $this->request->allOptions($hotelAvailRQ);
  }

  public function obtenerResultado($xml, $p) {
    // \Log::info($xml, 'NEMO AVAIL RS');
    // __logarr($xml);
    $res  = new NemoRS($xml);
    $res_result = $res->getResponse();
    if (!isset($res_result->Details->Trips->Trip->HotelsAvailability->Hotels->Hotel)) {
      return array();
    }
    $roomCount      = intval($p['roomCount']);
    $rooms_numbers  = array();
    $childIndex     = 1;
    for ($i = 1;$i <= $roomCount; $i++) {
      $adultCount   = intval($p["adultCount_$i"]);
      $childCount   = intval($p["childCount_$i"]);
      $occupancy    = array();
      $childAges    = array();
      for  ($d = 1; $d <= intval($p["adultCount_$i"]); $d++) {
        $occupancy[]  = 30;
      }

      for ($c = 1; $c <= intval($p["childCount_$i"]); $c++) {
        $occupancy[] = $p["childAge_$childIndex"];
        $childAges[] = $p["childAge_$childIndex"];
        $childIndex++;
      }
      $rooms_numbers[] = new Room($adultCount, $childCount, $childAges);
    }
    $hotelName = '';
    $regimen  = '';
    if(isset($p['regimen']) && $p['regimen'] != '') {
      $regimen = explode(',', implode(',', $p['regimen']));
    }

    // $hotelList = new HotelAvailListRS($res, $p['destCode'], $hotelName, $p['checkin'], $p['checkout'], $roomCount, $rooms_numbers, $regimen, $p['factorFee']);
    $factorFee = $p['factorFee'];
    $hotelList = new HotelAvailListRS($res, $p['destCode'], $hotelName, $p['checkin'], $p['checkout'], $roomCount, $rooms_numbers, $regimen, $factorFee);

    if (isset($p['stars']) && $p['stars'] != '') {
      $star      = explode(',', implode(',', $p['stars']));
      $hotel     = $hotelList->getHotelsAvail();
      $can_hotel = count($hotel);
      for ($i = 0; $i < $can_hotel; $i++) {
          $service = $hotel[$i];
          if (!in_array("'" . $service->getCategoryCode() . "'", $star)) {
            unset($hotelList->serviceHotels[$i]);
          }
      }
    }
    $arrNames   = array();
    $hotel      = $hotelList->getHotelsAvail();
    $can_hotel  = count($hotel);

    foreach ($hotel as $key => $value) {
      $i = $key;
      $h = $hotel[$i]->getName();
      if (!array_key_exists($h, $arrNames)) {
        $name             = $hotel[$i]->getName();
        $arrNames[$name]  = $i;
      }
      else {
        $index  = $arrNames[$h];
        $hs     = $hotelList->serviceHotels[$i]->getGroupedRooms();
        foreach ($hs as $group) {
          $hotelList->serviceHotels[$index]->insertGroupedRoom($group);
        }
        unset($hotelList->serviceHotels[$i]);
      }
    }

    return $hotelList;
  }

  public function execHotelServiceAddRQ($p) {
    $service = new SCartServiceRS($p);

    return new PurchaseRS(array($service));
  }

  public function execPurchaseConfirmRQ($p) {
    $services_count = intval($p['servicesCount']);
    $services       = array();
    $errors         = array();
    for ($i = 1; $i <= $services_count; $i++) {
      $book_params  = (array)json_decode($p['book_params']);
      $item         = $book_params;
      if ($item['provider'] != \BookingProvider::NEMO) { continue; }

      $resRQ  = new HotelReservationRQ($p, $item, $i);
      $resRQ->getParams();
      $xml    = $this->request->execRequest($resRQ, 'XML_RQ', 'BB_NM_RQ_PURCHASE');
      \Log::info($xml, 'BB_NM_RS_PURCHASE');
      
      $result     =  new NemoRS($xml);
      $result     = $result->getResponse();
      $status     = '';
      if (isset($result->Details->Products->Hotels->Hotel->BookingStatusList->BookingStatus)) {
        $status = $result->Details->Products->Hotels->Hotel->BookingStatusList->BookingStatus;
      }

      if (isset($result->Exceptions->Notification) && $result->Exceptions->Notification->NotificationType == 'NMO.HTL.NFT.ERR' || 
          $status != '' && strval($status['BookingStatusCode']) != "NMO.HTL.BST.CNF") {
        $error    = isset($result->Exceptions->Notification) ? $result->Exceptions->Notification : 'Error al confirmar Reserva';
        $code     = isset($result->Exceptions->Notification) ? strval($error->NotificationId) : '500';
        $msg      = isset($result->Exceptions->Notification) ? strval($error->NotificationMessage) : 'Error al confirmar Reserva';
        $provider = \BookingProvider::NEMO;
        $errors[] = array('code' => $code, 'msg' => $msg,'provider' => $provider);
        $services[] = new ServiceRS(array(), array(), $errors);
      }
      else {
        $services[] = new ServiceRS($result, $item, $errors);  
      }
    }
    return new PurchaseRS($services);
  }

  public function execPurchaseCancelRQ($p) {
    $localizer            = $p['localizer'];
    $totalCancellationFee = 0;
    $CancellBookRQ        = new CancellBookRQ($localizer);
    $rs = $this->request->execRequest($CancellBookRQ, 'XML_RQ', 'BB_NM_RQ_CANCELBOOK');
    
    \Log::info($rs, 'BB_NM_RS_CANCELBOOK');
    $cancellBookRS  = new NemoRS($rs);

    if (isset($cancellBookRS->Body->GetCancellationFeeResponse->GetCancellationFeeResult)) {
      $totalCancellationFee += floatval($cancellBookRS->Body->GetCancellationFeeResponse->GetCancellationFeeResult->CancellationFeeValue);
    }

    return new PurchaseRS(array(), $totalCancellationFee);
  }

  public function execPurchaseDetailRQ($p) {
    $resRQ      = new ReadServiceRQ($p['localizer']);
    $xml        = $this->request->execRequest($resRQ);
    $serviceRS  = new NemoRS($xml);
    $service    = new PurchasedServiceRS($serviceRS->getResponse());
    return new PurchaseRS(array($service));
  }

  public function execHotelDetailRQ($p) {
    $hotelCode = explode('_', $p['hotelCode']);
    $rq   = new HotelDetailRQ($hotelCode[1]);
    $xml  = $this->request->execRequest($rq);
    $rs   = new NemoRS($xml);
    return new HotelDetailRS($rs->getResponse());
  }

  public function execSearchZonesRQ($p) {
    $destCode = DestinationCodeMap::getDestCode($p['destCode']);
    if ($destCode == NULL) return array();


     $rooms_numbers[] = new Room(2, 0, array());

    $checkin    = '2020-10-01';
    $checkout   = '2020-10-05';

    $hotelAvailRQ = new AvailableHotelsRQ($destCode, $checkin, $checkout, $rooms_numbers, $hotelCode);
    $xml          = $this->request->execRequest($hotelAvailRQ);
    $res          = new NemoRS($xml);
    $res          = $res->getResponse();
    $res          = $res->Details->Trips->Trip->HotelsAvailability->Hotels->Hotel;
    $hotelsList   = array();
    foreach ($res as $key => $hotel) {
      $exist      = false;
      $attr       = $hotel->attributes();
      $hotelCode  = strval($attr['Code']).'_'.strval($attr['HotelDetailId']);
      $r          = \HotelMappingProvider::getMappedCodeId(array('hotel_code' => strval($hotelCode), 'provider' => \BookingProvider::TAILORBEDS));
      $idMapping  = 0;
      if(count($r)) {
        $exist      = true;
        $idMapping  = $r;
      }
      
      $hotelsList[] = array('destCode'  => strval($p['destCode']),                  'hotelCode'     => strval($attr['Code']).'_'.strval($attr['HotelDetailId']), 
                            'hotelName' => addslashes(strval($attr['Name'])),       'longitude'       => strval($attr['Longitude']), 
                            'latitude'  => strval($attr['Latitude']),               'category_code'   => strval($hotel->Rating->Value),
                            'dest_code' => $p['destCode'], 'provider' => 136,       'zoneName'        => '','exist' => $exist,
                            'idMapping' => $idMapping);  
    }
    
    return $hotelsList;
  }

}

?>
