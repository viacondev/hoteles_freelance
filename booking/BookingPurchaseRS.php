<?php

class BookingPurchaseRS extends BookingPurchase {

  private $services;

  public function __construct($services) {
    $this->services = $services;
  }

  public function getPurchaseToken() {
    return '';
  }

  public function getStatus() {
    return '';
  }

  public function getHolder() {
    return '';
  }

  public function getBookingServices() {
    return $this->services;
  }

  public function getTotalPrice() {
    return '';
  }

  public function getCurrency() {
    return '';
  }

  public function getCreationDate() {
    return '';
  }

  public function getReferenceFileNumber() {
    return '';
  }

  public function getReferenceIncomingOffice() {
    return '';
  }

  public function getAgencyReference() {
    return '';
  }

  public function getPaymentDataDescription() {
    return '';
  }
}

?>