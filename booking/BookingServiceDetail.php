<?php

class BookingServiceDetail extends BookingSerializable {

  public $code;
  public $name;

  public function build($code, $name) {
    $this->code = $code;
    $this->name = $name;
  }

  public function getCode() {
    return strval($this->code);
  }

  public function getName() {
    return strval($this->name);
  }

}

?>
