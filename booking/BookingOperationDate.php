<?php

class BookingOperationDate {

  private $date;
  private $minimumDuration;
  private $maximumDuration;

  public function __construct($date, $minimumDuration, $maximumDuration) {
    $this->date = $date;
    $this->minimumDuration = $minimumDuration;
    $this->maximumDuration = $maximumDuration;
  }

  public function getDate() {
    return $this->date;
  }

  public function getMinimumDuration() {
    return $this->minimumDuration;
  }

  public function getMaximumDuration() {
    return $this->maximumDuration;
  }

}

?>