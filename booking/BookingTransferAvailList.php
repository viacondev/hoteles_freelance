<?php

include_once('BookingTransferAvail.php');

class BookingTransferAvailList extends BookingSerializable {

  public $transferType;
  public $transfersAvail;
  public $transfersAvailIn;
  public $transfersAvailOut;

  /**
   * @return: IN, OUT or IN_OUT
   */
  public function getTransferType() {
    return $this->transferType;
  }

  public function getTransfersAvail() {
    return $this->transfersAvail;
  }

  public function getTransfersAvailIn() {
    return $this->transfersAvailIn;
  }

  public function getTransfersAvailOut() {
    return $this->transfersAvailOut;
  }

  public function calculateOutTransferForInTransfer($transferIn) {
    $bestMatch = NULL;
    $level1 = true;

    // TODO: Improve matching algorithm. In ROME (ROE) Transfer has different contract names
    // for IN and OUT even on the same transfers.
    foreach ($this->getTransfersAvailOut() as $transferOut) {
      if ($transferIn->getContractName() == $transferOut->getContractName()) {
        if ($level1) $bestMatch = $transferOut;

        if ($transferIn->getTransferInfoVehicleTypeCode() == $transferOut->getTransferInfoVehicleTypeCode()) {
          $level1 = false;
          $bestMatch = $transferOut;

          if ($transferIn->getTotalAmount() == $transferOut->getTotalAmount()) {
            $bestMatch = $transferOut;
            break;
          }
        }
      }
    }

    if ($bestMatch == NULL && count($this->getTransfersAvailOut()) > 0) {
      $allOut = $this->getTransfersAvailOut();
      $bestMatch = $this->allOut[0];
    }

    return $bestMatch;
  }

}

?>
