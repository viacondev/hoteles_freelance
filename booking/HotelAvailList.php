<?php

include_once('HotelAvail.php');

class HotelAvailList extends BookingSerializable {

  public $serviceHotels;
  public $currentPage;
  public $totalPages;

  public function getHotelsAvail() {
    return $this->serviceHotels;
  }

  public function getCurrentPage() {
    return $this->currentPage;
  }

  public function getTotalPages() {
    return $this->totalPages;
  }

}

?>
