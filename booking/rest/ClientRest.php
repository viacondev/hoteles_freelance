<?php
namespace Rest;

require __DIR__ . '/vendor/autoload.php';
// require __DIR__ . '/vendor';
// require __DIR__ . '/config.php';

class ClientRest {

  private $api;
  private $book;
  private $hotel;
  private $rooms;

  function __construct($url) {
    $this->api = new \RestClient(array(
      'base_url' => $url
    ));
  }

  public function searchHotelAvail($method, $header, $params) {
    $result = $this->api->get($method, $params, $header);
    return $result;
  }

  public function bookingCancellationPolicy($method, $header, $params) {
    $result = $this->api->get($method, $params, $header);
    return $result;
  }

  public function purchase($method, $header, $params) {
    $params = json_encode($params);
    $result = $this->api->post($method, $params, $header);
    return $result;
  }

  public function purchase_detail($method, $header, $params) {
    $result = $this->api->get($method, $params, $header);
    return $result;
  }

  public function cancelPurchase($method, $header, $params) {
    $params = $params[0];
    $result = $this->api->put($method, $params, $header);
    return $result;
  }
  public function getOptionsApi($method, $header, $params) {
    return $this->api->getOptions($method, 'GET', $params, $header);
  }

}
?>
