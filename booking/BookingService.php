<?php

class BookingService extends BookingSerializable {

  public $scartId;
  public $localizer;
  public $sPUI;
  public $serviceType;
  public $serviceStatus;
  public $directPayment;
  public $dateFrom;
  public $dateTo;
  public $currency;
  public $totalAmount;
  public $totalAmountWithFee;
  public $supplementList;
  public $additionalCosts;
  public $serviceInfo;
  public $status;

  public function getLocalizer() {
    return $this->localizer;
  }

  public function getSPUI() {
    return $this->sPUI;
  }

  public function getServiceType() {
    return $this->serviceType;
  }

  public function getServiceStatus() {
    return $this->serviceStatus;
  }

  public function getDirectPayment() {
    return $this->directPayment;
  }

  public function getDateFrom() {
    return $this->dateFrom;
  }

  public function getDateTo() {
    return $this->dateTo;
  }

  public function getCurrency() {
    return $this->currency;
  }

  public function getTotalAmount() {
    return $this->totalAmount;
  }

  public function getTotalAmountWithFee() {
    return $this->totalAmountWithFee;
  }

  public function getSupplementList() {
    return $this->supplementList;
  }

  public function getAdditionalCosts() {
    return $this->additionalCosts;
  }

  public function getServiceInfo() {
    return $this->serviceInfo;
  }

  public function getStatus() {
    return $this->status;
  }

}

?>
