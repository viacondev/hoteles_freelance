<?php
namespace Domitur;

abstract class RQOperation {
  abstract public function getType();
  abstract public function getName();
  abstract public function getParams();
}


?>