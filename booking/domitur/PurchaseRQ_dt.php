<?php
namespace Domitur;

class PurchaseRQ extends RQOperation {

  private $hotel_code;
  private $holder_name;
  private $holder_lastname;
  private $date_in;
  private $nights;
  private $client_comments;
  private $smoker;
  private $contact;
  private $numberOfRooms;
  private $room;

  public function __construct($hotel_code, $holder_name, $holder_lastname, $date_in, $nights, $client_comments, $contact, $numberOfRooms, $rooms) {
    $this->hotel_code = $hotel_code;
    $this->holder_name = $holder_name;
    $this->holder_lastname = $holder_lastname;
    $this->date_in = $date_in;
    $this->nights = $nights;
    $this->client_comments = $client_comments;
    $this->smoker = 0;
    $this->contact = $contact;
    $this->numberOfRooms = $numberOfRooms;
    $this->rooms = $rooms;
  }

  public function getType() {
    return 40;
  }

  public function getName() {
    return 'Booking Confirmation';
  }

  public function getParams() {
    $xml = '
      <hotel_code>' . $this->hotel_code . '</hotel_code>
      <client_name>' . $this->holder_name . '</client_name>
      <client_surname>' . $this->holder_lastname . '</client_surname>
      <date_in>' . $this->date_in . '</date_in>
      <nights>' . $this->nights . '</nights>
      <hotelinstructions>' . $this->client_comments . '</hotelinstructions>
      <smoker>' . $this->smoker . '</smoker>
      <contact_name>' . $this->contact . '</contact_name>
      <language_code>1</language_code>
      <rooms>
        <numberofrooms>' . $this->numberOfRooms . '</numberofrooms>';

    foreach ($this->rooms as $room) {
      $xml .= $room->getParams();
    }
    $xml .= '</rooms>';

    return $xml;
  }
}

?>