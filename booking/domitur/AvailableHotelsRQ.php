<?php
namespace Domitur;

include_once('Room.php');

class AvailableHotelsRQ extends RQOperation {

  public $name;
  public $country;
  public $city;
  public $date_in;
  public $nights;
  public $rooms;
  public $numberOfRooms;

  public function __construct($name, $city, $date_in, $nights, $rooms) {
    $this->name = isset($name) ? $name : '';
    $this->country = '';
    $this->city = $city;
    $this->date_in = $date_in;
    $this->nights = $nights;
    $this->numberOfRooms = count($rooms);
    $this->rooms = $rooms;
  }

  public function getType() {
    return 30;
  }

  public function getName() {
    return 'Available Hotels List';
  }

  public function getParams() {
    $xml = '
      <hotel>' . $this->name . '</hotel>
      <country></country>
      <city>' . $this->city . '</city>
      <date_in>' . $this->date_in . '</date_in>
      <nights>' . $this->nights . '</nights>
      <language_code>1</language_code>
      <rooms>
        <numberofrooms>' . $this->numberOfRooms . '</numberofrooms>';

    foreach ($this->rooms as $room) {
      $xml .= $room->getParams();
    }
    $xml .= '</rooms>';

    return $xml;
  }
}

?>