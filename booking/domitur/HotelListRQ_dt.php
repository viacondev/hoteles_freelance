<?php
namespace Domitur;

class HotelListRQ extends RQOperation {

  public $country;

  public function __construct($country) {
    $this->country = $country;
  }

  public function getType() {
    return 80;
  }

  public function getName() {
    return 'Hotels List';
  }

  public function getParams() {
    return '<country>' . $this->country . '</country>';    
  }
}

?>