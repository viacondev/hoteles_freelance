<?php
namespace Domitur;

include_once('SCartRoomAvail_dt.php');

class SCartHotelAvail extends \HotelAvail {

  private $data;

  public function __construct($data) {
    $this->data = $data;

    $this->roomsAvail = array();
    foreach ($this->data->rooms as $room) {
      $this->roomsAvail[] = new SCartRoomAvail($room, $this, $this->getDateFrom(), $this->getDateTo(), $data->childAges, $this->data->hotelCode, $this->data->factorFee);
    }
  }

  public function getProvider() {
    return \BookingProvider::DOMITUR;
  }

  public function getProviderName() {
    return 'DOMITUR';
  }

  public function getAvailToken() {
    return '';
  }

  public function getCode() {
    return $this->data->hotelCode;
  }

  public function getName() {
    return $this->data->hotelName;
  }

  public function getDateFrom() {
    return $this->data->dateFrom;
  }

  public function getDateTo() {
    return $this->data->dateTo;
  }

  public function getDestinationCode() {
    return $this->data->destCode;
  }

  public function getDestinationType() {
    return '';
  }

  public function getMainImage() {
    return '';
  }

  public function getDescription() {
    return '';
  }

  public function getLatitude() {
    return '';
  }

  public function getLongitude() {
    return '';
  }

  public function getZone() {
    return '';
  }

  public function getLocation() {
    return '';
  }

  public function getCategory() {
    return '';
  }

  public function getCategoryType() {
    return '';
  }

  public function getCategoryCode() {
    return strval($this->data->category);
  }

  public function getRoomsAvail() {
    return $this->roomsAvail;
  }

  public function getGroupedRooms() {
    return $this->roomsAvail;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getContractName() {
    return '';
  }

  public function getContractIncomingOfficeCode() {
    return '';
  }

  public function getContractComment() {
    return '';
  }

  public function getClientComment() {
    return '';
  }

  public function getSupplier() {
    return '';
  }

  public function getSupplierVatNumber() {
    return '';
  }

  public function getDiscountList() {
    return array();
  }

  public function getAllPaxRequired() {
    return false;
  }

  public function getXtraDataRequired() {
    return false;
  }

  public function isDataRequired() {
   return false;
  }
}

?>
