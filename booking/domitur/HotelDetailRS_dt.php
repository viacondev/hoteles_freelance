<?php
namespace Domitur;

class HotelDetailRS extends \BookingHotelDetail {

  private $elements;
  private $hotel;
  // private $buildingFacilities;
  // private $hotelTypeFacility;
  // private $credCardsFacilities;
  // private $roomFacilities;
  // private $servicesFacilities;
  // private $cateringFacilities;
  // private $businessFacilities;
  // private $healthBeautyFacilities;
  // private $entertainmentFacilities;
  // private $distancesFacilities;
  // private $highLightFacilities;

  public function __construct($elements, $destCode) {
    //$this->elements = $elements;
    $this->hotel = $elements->parameters->available_hotels->hotel;

  }

  public function buildFacilitiesList() {
    return '';
  }

  public function getCode() {
    return strval($this->hotel->code);
  }

  public function getName() {
    $name = explode('-', $this->hotel->name);
    return $name[0];
    //return strval($this->hotel->name);
  }

  public function getDescription() {
    return strval($this->hotel->general_information);
  }

  public function getImageList() {
    $images = array();
    if (isset($this->hotel->picture)) {
        $images[] = strval(str_replace('+', ' ', $this->hotel->picture));
    }

    return $images;
  }

  public function getAddressStreetName() {
    return '';
  }
  public function getAddressNumber() {
    return '';
  }

  public function getAddressPostalCode() {
    return str_replace('-', ' ', $this->hotel->address);
  }

  public function getEmailList() {
    return array();
  }

  public function getPhoneList() {
    $arr = array();
    if (isset($this->hotel->telephone)) {
      $arr[] = strval($this->hotel->telephone);
    }

    return $arr;
  }

  public function getFaxList() {
    return array();
  }

  public function getWebList() {
    return array();
  }

  public function getLocation() {
    return $this->hotel->localization;
  }

  public function getLatitude() {
    return '';
    //return $this->hotel->localization;
  }

  public function getLongitude() {
    return '';
    // return $this->hotel->localization;
  }

  public function getDestinationCode() {
    return '';
  }

  public function getDestinationName() {
    return '';
  }

  public function getDestinationZone() {
    return '';
  }

  public function getCategory() {
    return strval($this->hotel->category) . 'ESTRELLAS';
  }

  public function getCategoryCode() {
    $cat = strval($this->hotel->category);
    if ($cat == '6') return '5LUX';

    return $cat . 'EST';
  }

  public function getBuildingFacilities() {
    return array();
  }

  public function getHotelTypeFacilities() {
    return array();
  }

  public function getCredCardsFacilities() {
    return array();
  }

  public function getRoomFacilities() {
    return array();
  }

  public function getServicesFacilities() {
    return array();
  }

  public function getCateringFacilities() {
    return array();
  }

  public function getBusinessFacilities() {
    return array();
  }

  public function getHealthBeautyFacilities() {
    return array();
  }

  public function getEntertainmentFacilities() {
    return array();
  }

  public function getDistancesFacilities() {
    return array();
  }

  public function getHighLightFacilities() {
    return array();
  }

}

?>
