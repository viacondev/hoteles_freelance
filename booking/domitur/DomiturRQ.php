<?php
namespace Domitur;

class DomiturRQ {

  private $agencyName;
  private $agencyCode;
  private $url;

  public function __construct() {
    $this->agencyName = \DTConfig::$agencyName;
    $this->agencyCode = \DTConfig::$agencyCode;
    $this->url = \DTConfig::$url;
  }

  public function getRequest($operation) {
    return '<?xml version="1.0"?>
            <request>
              <type>' . $operation->getType() . '</type>
              <name>' . $operation->getName() . '</name>
              <agency>' . $this->agencyName . '</agency>
              <parameters>' . $operation->getParams() . '</parameters>
            </request>';
  }

  public function execRequest($operation, $solicitud = "", $type = "") {
    // __logtxt($this->getRequest($operation));
    if($solicitud == 'XML_RQ') {
      $descrip = $type;
      \Log::info($this->getRequest($operation), $descrip);
    }
    $rq = $this->url;
    $rq .= '?agencycode=' . urlencode($this->agencyCode);
    $rq .= '&agencyname=' . urlencode($this->agencyName);
    $rq .= '&reqname=' . urlencode($operation->getName());
    $rq .= '&request=' . urlencode($this->getRequest($operation));
    $context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));
    return file_get_contents($rq, false, $context);
  }
}


?>
