<?php
namespace Domitur;

include_once('RoomAvail.php');

class HotelAvailRS extends \HotelAvail {

  private $element;
  private $destCode;

  public function __construct($element, $destCode, $factorFee = 1, $extraFee = 0) {
    $this->element = $element;
    $this->destCode = $destCode;

    $name = explode('-', strval($this->element->name));
    $this->name = $name[0];

    $rooms = $this->element->values->rooms;
    $this->roomsAvail = array();
    for ($i = 0; $i < $rooms->numberofrooms; $i++) {
      $room = new RoomAvail($rooms->room[$i], $this, $factorFee, $extraFee, $this->element->code);
      if (isset($name[1])) {
        $room->name = $name[1];
      }
      for ($j = $i; $j < count($rooms->room); $j += $rooms->numberofrooms) {
        $room->price += floatval(str_replace(',', '', $rooms->room[$j]->net_rate_to_pay));
      }
      $room->priceWithFee = floatval($room->price) / $factorFee;
      if ($extraFee != 0) {
        $room->priceWithFee += $room->priceWithFee * ($extraFee / 100);
      }
      $this->roomsAvail[] = $room;
    }

    // Domitur only returns 1 result per hotel, so all rooms will match the groupedRooms
    $this->insertGroupedRoom($this->roomsAvail);
  }

  public function getProvider() {
    return \BookingProvider::DOMITUR;
  }

  public function getProviderName() {
    return 'DOMITUR';
  }

  public function getAvailToken() {
    return '';
  }

  public function getCode() {
    return strval($this->element->code);
  }

  public function getName() {
    return $this->name;
  }

  public function getDateFrom() {
    return strval($this->element->values->date_in);
  }

  public function getDateTo() {
    $date_in = new \DateTime($this->element->values->date_in);
    $nights = $this->element->values->nights;
    $dateTo = date_add($date_in, new \DateInterval('P' . $nights . 'D'));
    return $dateTo->format('Ymd');
  }

  public function getDestinationCode() {
    return $this->destCode;
  }

  public function getDestinationType() {
    return '';
  }

  public function getMainImage() {
    return strval(str_replace('+', ' ', $this->element->picture));
  }

  public function getDescription() {
    return strval($this->element->general_information);
  }

  public function getLatitude() {
    return '';
  }

  public function getLongitude() {
    return '';
  }

  public function getZone() {
    return '';
  }

  public function getLocation() {
    return strval($this->element->localization);
  }

  public function getCategory() {
    return strval($this->element->category) . 'ESTRELLAS';
  }

  public function getCategoryType() {
    return '';
  }

  public function getCategoryCode() {
    $cat = strval($this->element->category);
    if ($cat == '6') return '5LUX';

    return $cat . 'EST';
  }

  public function getRoomsAvail() {
    return $this->roomsAvail;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getContractName() {
    return '';
  }

  public function getContractIncomingOfficeCode() {
    return '';
  }

  public function getContractComment() {
    return '';
  }

  public function getClientComment() {
    return '';
  }

  public function getSupplier() {
    return '';
  }

  public function getSupplierVatNumber() {
    return '';
  }

  public function getDiscountList() {
    return array();
  }

  public function getAllPaxRequired() {
    return false;
  }

  public function getXtraDataRequired() {
    return false;
  }
}

?>
