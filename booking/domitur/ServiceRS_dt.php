<?php
namespace Domitur;

include_once('Purchase_HotelRS_dt.php');

class ServiceRS extends \BookingService {

  private $element;
  private $service;
  // private $serviceType;

  public function __construct($serviceElement, $localizer) {
    $this->element = $serviceElement->parameters->booking;

    $this->service = new Purchase_HotelRS($this->element);
    $this->localizer = $localizer;
  }

  public function getLocalizer() {
    return strval($this->element->code);
  }

  public function getSPUI() {
    return '';
  }

  public function getServiceType() {
    return 'ServiceHotel';
  }

  public function getServiceStatus() {
    return strval($this->element->booking_head->booking_status);
  }

  public function getDirectPayment() {
    return '';
  }

  public function getDateFrom() {
    $date_arr = explode('/', $this->element->booking_head->date_in);
    if (count($date_arr) >= 3) {
      return $date_arr[2] . $date_arr[1] . $date_arr[0];
    }
    return '00000000';
  }

  public function getDateTo() {
    $date_arr = explode('/', $this->element->booking_head->date_out);
    if (count($date_arr) >= 3) {
      return $date_arr[2] . $date_arr[1] . $date_arr[0];
    }
    return '00000000';
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getTotalAmount() {
    return round(floatval($this->element->booking_head->total_price), 2);
  }

  public function getAdditionalCosts() {
    return array();
  }

  public function getSupplementList() {
    return array();
  }

  public function getServiceInfo() {
    return $this->service;
  }

  public function getStatus() {
    if(isset($this->elements->booking_code)) {
      $attrs = $this->elements->booking_code;
      if($attrs == 1) {
        return \ServiceStatus::CANCELLED;
      }
    }
    return '';
  }
}

?>
