<?php
namespace Domitur;

class BookingCancellationRQ extends RQOperation {

  public $localizer;

  public function __construct($localizer) {
    $this->localizer = $localizer;
  }

  public function getType() {
    return 70;
  }

  public function getName() {
    return 'Booking Cancellation';
  }

  public function getParams() {
    return '<booking_code>' . $this->localizer . '</booking_code>';
  }
}

?>