<?php
namespace Domitur;

class PurchaseRoomsRS_dt extends \RoomAvail {

  private $element;
  private $room_type;

  public function __construct($availableRoomElement, $room_typ = '') {
    $this->element = $availableRoomElement;
    $this->room_type = $room_typ;
    //$this->hotel = $hotel;
  }

  public function getProvider() {
    return \BookingProvider::DOMITUR;
  }

  public function getRoomCount() {
    return 1;
  }

  public function getAdultCount() {
    return '';
  }

  public function getChildCount() {
    return '';
  }

  public function getPrice() {
    return round(floatval($this->element->total_price), 2);
  }

  public function getSHRUI() {
    return '';
  }

  public function getOnRequest() {
    return '';
  }

  public function getBoard() {
    $boards = array(
      'TI' => 'Todo incluido',
      'AD' => 'Alojamiento y desayuno buffet',
      'SA' => 'Solo alojamiento',
      'MP' => 'Media Pensi&oacute;n'
      );

    if (array_key_exists(strval($this->element->service), $boards)) {
      return $boards[strval($this->element->service)];
    }
    return strval($this->element->service);
  }

  public function getBoardCode() {
    return strval($this->element->service);
  }

  public function getBoardType() {
    return '';
  }

  public function getRoomType() {
    return  strval($this->room_type);
  }

  public function getRoomTypeType() {
    return '';
  }

  public function getRoomTypeCode() {
    return '';
  }

  public function getRoomTypeCharacteristic() {
    return '';
  }

  public function getCancellationPolicies() {
    $policies_arr = array();
    $policies_arr[] = $this->element->cancellation_penalty;
    return $policies_arr;
  }

  public function getGuestList() {
    $guests = array();
    if (!isset($this->element->passenger)) {
      return $guests;
    }
    $name = $this->element->passenger->passenger_name;
    $lastName = $this->element->passenger->passenger_surname;
    $guests[] = new \BookingCustomer(0, 0, 0, $name, $lastName);
    return $guests;
  }

}

?>
