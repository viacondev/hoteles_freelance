<?php
namespace Domitur;

class PurchaseDetailRQ extends RQOperation {

  private $booking_localizer;

  public function __construct($localizer) {
    $this->booking_localizer = $localizer;
  }
  
  public function getType() {
    return 50;
  }

  public function getName() {
    return 'Booking Confirmation';
  }

  public function getBookingLocalizer() {
    return $this->booking_localizer;
  }

  public function getParams() {
    $xml = '
      <booking_code>' . $this->booking_localizer . '</booking_code>
      <language_code>1</language_code>';

    return $xml;
  }
}

?>