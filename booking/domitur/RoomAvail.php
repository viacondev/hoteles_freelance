<?php
namespace Domitur;

class RoomAvail extends \RoomAvail {

  private $element;
  public $name;
  public $price;
  public $priceWithFee;
  public $hotelCode;
  public function __construct($element, $hotel, $factorFee = 1, $extraFee = 0, $hotelCode = 0) {
    $this->element = $element;
    $name = '';
    $price = 0;
    $this->hotelCode = $hotelCode;
    $this->hotel = $hotel;
    $this->hotel = json_encode($this->buildObjecArray());
  }

  public function getProvider() {
    return \BookingProvider::DOMITUR;
  }

  public function getHotel() {
    return $this->hotel;
  }

  public function getProviderName() {
    return 'DOMITUR';
  }

  public function getHotelCode() {
    return strval($this->hotelCode);
  }

  public function getRoomCount() {
    return 1;
  }

  public function getAdultCount() {
    return intval($this->element->adults);
  }

  public function getChildCount() {
    return intval($this->element->children);
  }

  public function getChildAges() {
    return array();
  }

  public function getPrice() {
    return $this->price;
  }

  public function getPriceWithFee() {
    return round($this->priceWithFee, 2);
  }

  public function getSHRUI() {
    return '';
  }

  public function getOnRequest() {
    return '';
  }

  public function getBoard() {
    $boards = array(
      'TI' => 'Todo incluido',
      'AD' => 'Alojamiento y desayuno buffet',
      'SA' => 'Solo alojamiento',
      'MP' => 'Media Pensi&oacute;n'
      );

    if (array_key_exists(strval($this->element->service), $boards)) {
      return $boards[strval($this->element->service)];
    }
    return strval($this->element->service);
  }

  public function getBoardCode() {
    return strval($this->element->service);
  }

  public function getBoardType() {
    return '';
  }

  public function getRoomType() {
    return $this->name . ' - ' . strval($this->element->type_room);
  }

  public function getRoomTypeType() {
    return '';
  }

  public function getRoomTypeCode() {
    return strval($this->element->type_room);
  }

  public function getRoomTypeCharacteristic() {
    return '';
  }

  public function getCancellationPolicies() {
    return array();
  }

  public function getGuestList() {
    return array();
  }

  public function buildObjecArray() {
    $hotel = $this->hotel;
    $methods = get_class_methods($hotel);
    $objArray = array();
    foreach ($methods as $method) {
      if (substr($method, 0, 2) == '__') continue;
      if (substr($method, 0, 3) === 'get' && $method != 'getGroupedRooms' && $method != 'getRoomsAvail') {
        $key = lcfirst(substr($method, 3));
        $objArray[$key] = $hotel->$method();
      }
    }
    return $objArray;
  }

}

?>
