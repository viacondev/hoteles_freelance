<?php
namespace Domitur;

class Room {

  public $adults;
  public $childrens;
  public $childrenAges;

  public function __construct($adults, $childrens, $childrenAges) {
    $this->adults = $adults;
    $this->childrens = $childrens;
    $this->childrenAges = $childrenAges;
  }

  public function getParams() {
    $xml = '<room>
              <adults>' . $this->adults . '</adults>
              <children>' . $this->childrens . '</children>';
    for ($i = 1; $i <= $this->childrens; $i++) {
      $age = $this->childrenAges[$i];
      $xml .= "<childrenage$i>$age</childrenage$i>";
    }
    $xml .= '</room>';

    return $xml;
  }
}

?>