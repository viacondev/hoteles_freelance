<?php
namespace Domitur;

class SCartRoomAvail extends \RoomAvail {

  private $data;
  public $hotel;
  private $date_in;
  private $date_out;
  public $childAges;
  public $priceWithFee;
  public $hotelCode;

  public function __construct($data, $hotel, $date_in, $date_out, $childAges, $hotelCode, $factorFee) {
    $this->data = $data;
    $this->date_in = $date_in;
    // $this->hotel = $hotel;
    $this->date_out = $date_out;
    $this->childAges = $childAges;
    $this->hotelCode = $hotelCode;
    $this->priceWithFee = round($this->data->price / $factorFee, 2);
  }

  public function getProvider() {
    return \BookingProvider::DOMITUR;
  }

  public function getProviderName() {
    return 'DOMITUR';
  }

  public function getHotelCode() {
    return '';
  }

  public function getRoomCount() {
    return $this->data->roomCount;
  }

  public function getAdultCount() {
    return $this->data->adultCount;
  }

  public function getChildCount() {
    return $this->data->childCount;
  }

  public function getChildAges() {
    return array();
  }

  public function getPrice() {
    return $this->data->price;
  }

  public function getPriceWithFee() {
    return $this->priceWithFee;
  }

  public function getSHRUI() {
    return '';
  }

  public function getOnRequest() {
    return '';
  }

  public function getBoard() {
    return $this->data->board;
  }

  public function getBoardCode() {
    return $this->data->boardCode;
  }

  public function getBoardType() {
    return $this->data->boardType;
  }

  public function getRoomType() {
    return $this->data->roomType;
  }

  public function getRoomTypeType() {
    return $this->data->roomTypeType;
  }

  public function getRoomTypeCode() {
    return $this->data->roomTypeCode;
  }

  public function getRoomTypeCharacteristic() {
    return $this->data->roomTypeCharacteristic;
  }

  public function getCancellationPolicies() {
    $policies = array();

    $policiesRQ = new CancellationPoliciesRQ($this->hotelCode, $this->date_in);
    $rq = new DomiturRQ();
    $xml = $rq->execRequest($policiesRQ, 'XML_RQ', 'BB_RQ_DT_POLICY');
    \Log::info($xml, 'BB_RS_DT_POLICY');
    $rs = simplexml_load_string(utf8_encode($xml));
    if(!isset($rs->parameters->pcs)) {
      return $policies;
    }
    foreach ($rs->parameters->pcs->pc as $policy) {
      $from = new \DateTime(dateFromYmdToDB($this->date_in));
      $from = date_sub($from, new \DateInterval('P' . strval($policy->da) . 'D'));

      $amount = 0;
      if (intval($policy->ng) != 0) {
        $dateIn = new \DateTime(dateFromYmdToDB($this->date_in));
        $dateOut = new \DateTime(dateFromYmdToDB($this->date_out));
        $nights = $dateOut->diff($dateIn)->days;
        $amount = $this->getPrice() * intval($policy->ng) / $nights;
      }
      if (intval($policy->pg) != 0) {
        $amount = ($this->getPrice() * intval($policy->pg)) / 100;
      }
      $cancellation = new \BookingCancellationPolicy();
      $cancellation->build($amount, $from, '00:00', true);
      $policies[] = $cancellation;
    }

    return $policies;
  }

  public function getGuestList() {
    $guests = array();

    $customer_counter = 0;
    for ($i = 0; $i < $this->getAdultCount(); $i++) {
      $customer_counter++;
      $guests[] = new \BookingCustomer('AD', $customer_counter, '30', '', '');
    }

    for ($i = 0; $i < $this->getChildCount(); $i++) {
      $customer_counter++;
      $guests[] = new \BookingCustomer('CH', $customer_counter, $this->childAges[$i], '', '');
    }

    return $guests;
  }

  public function buildObjecArray() {
    return array();
  }
}

?>
