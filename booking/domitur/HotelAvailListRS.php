<?php
namespace Domitur;

include_once('HotelAvailRS.php');

class HotelAvailListRS extends \HotelAvailList {

  private $elements;
  public $serviceHotels;

  public function __construct($elements, $destCode, $board_descrip = '', $factorFee = 1, $extraFee = 0) {
    $this->elements = $elements;

    $this->serviceHotels = array();
    if (isset($this->elements->parameters->available_hotels)) {
      foreach ($this->elements->parameters->available_hotels->hotel as $hotel) {
        if($board_descrip != '') {
          $rooms_ava = new HotelAvailRS($hotel, $destCode, $factorFee, $extraFee);
          $room = $rooms_ava->getRoomsAvail();
          foreach ($room as $key => $value) {
            if(in_array($value->getBoardCode(), $board_descrip)) {
              $this->serviceHotels[] = new HotelAvailRS($hotel, $destCode, $factorFee, $extraFee);
            }
          }
        }
        else {
          $this->serviceHotels[] = new HotelAvailRS($hotel, $destCode, $factorFee, $extraFee);
        }
      }
    }

  }

  public function getHotelsAvail() {
    return $this->serviceHotels;
  }

  public function getCurrentPage() {
    return 1;
  }

  public function getTotalPages() {
    return 1;
  }
}

?>
