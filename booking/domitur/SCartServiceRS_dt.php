<?php
namespace Domitur;

include_once('SCartHotelAvail_dt.php');

class SCartServiceRS extends \BookingService {

  private $data;
  private $errors;
  // private $localizer;

  public function __construct($data, $localizer = '', $total_price = 0, $errors = array()) {
    $this->data = json_decode(json_encode($data), FALSE);
    $this->localizer = $localizer;
    $this->$total_price = $total_price;
    $this->errors = $errors;
  }

  public function getLocalizer() {
    return $this->localizer;
  }

  public function getSPUI() {
    return '';
  }

  public function getServiceType() {
    return 'ServiceHotel';
  }

  public function getServiceStatus() {
    return '';
  }

  public function getDirectPayment() {
    return '';
  }

  public function getDateFrom() {
    return $this->data->dateFrom;
  }

  public function getDateTo() {
    return $this->data->dateTo;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getTotalAmount() {
    if (isset($this->total_price) && $this->total_price != 0) {
      return $this->total_price;
    }

    $price = 0;
    foreach ($this->data->rooms as $room) {
      $price += $room->price;
    }
    return $price;
  }

  public function getTotalAmountWithFee() {
    if (isset($this->total_price) && $this->total_price != 0) {
      return round($this->total_price / $this->data->factorFee, 2);
    }

    $price = 0;
    foreach ($this->data->rooms as $room) {
      $price += $room->price;
    }
    return round($price / $this->data->factorFee, 2);
  }

  public function getAdditionalCosts() {
    return array();
  }

  public function getServiceInfo() {
    return new SCartHotelAvail($this->data);
  }

  public function getSupplementList() {
    return array();
  }

  public function getStatus() {
    return $this->localizer == '' ? \ServiceStatus::PENDING : \ServiceStatus::CONFIRMED;
  }

  public function getErrors() {
    return $this->errors;
  }

}

?>
