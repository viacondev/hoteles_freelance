<?php
namespace Domitur;

class DestinationCodeMap {

  public static function getCode($c) {
    $map = array(
      'PUJ' => 'PTC',
      'PTY' => 'PAN',
      'CUN' => 'CUNN-ZHT',
      'PCM' => 'cun',
      'DOM' => 'SDQ'
      );

    if (isset($map[$c])) {
      return $map[$c];
    }

    return '';
  }

}

?>