<?php
namespace Domitur;

include_once('DomiturRQ.php');
include_once('RQOperation.php');
include_once('AvailableHotelsRQ.php');
include_once('HotelAvailListRS.php');
include_once('SCartServiceRS_dt.php');
include_once('CancellationPoliciesRQ_dt.php');
include_once('PurchaseRQ_dt.php');
include_once('PurchaseRS_dt.php');
include_once('DestinationCodeMap_dt.php');
include_once('BookingCancellationRQ_dt.php');
include_once('HotelDetailRS_dt.php');
include_once('ServiceRS_dt.php');
include_once('PurchaseDetailRQ_dt.php');

class DomiturBooking {

  private $request;

  public function __construct() {
    $this->request = new DomiturRQ();
  }

  public function execRequest($params) {
    switch ($params['requestType']) {
      case \BookingRequestTypes::SearchHotelAvail:
        return $this->execSearchHotelAvailRQ($params);
        break;

      case \BookingRequestTypes::AddHotelService:
        return $this->execHotelServiceAddRQ($params);
        break;

      case \BookingRequestTypes::ConfirmPurchase:
        return $this->execPurchaseConfirmRQ($params);
        break;

      case \BookingRequestTypes::CancelPurchase:
        return $this->execPurchaseCancelRQ($params);
        break;

       case \BookingRequestTypes::HotelDetail:
        return $this->execHotelDetailRQ($params);
        break;

      case \BookingRequestTypes::PurchaseDetail:
        return $this->execPurchaseDetail($params);
        break;

      default:
        return NULL;
        break;
    }
  }

  public function execSearchHotelAvailRQ($p) {
    $p['destCode'] = DestinationCodeMap::getCode($p['destCode']);
    // if ($p['destCode'] == '' || $p['page'] != 1) return NULL;

    $from = new \DateTime($p['checkin']);
    $to = new \DateTime($p['checkout']);
    $nights = $to->diff($from)->days;

    $rooms = array();
    $currentChild = 0;
    for ($i = 1; $i <= $p['roomCount']; $i++) {
      $childrenAges = array();
      if ($p["childCount_$i"] > 0) {
        for ($j = 1; $j <= $p["childCount_$i"]; $j++) {
          $currentChild++;
          $childrenAges[$j] = $p["childAge_$j"];
        }
      }
      $rooms[] = new Room($p["adultCount_$i"], $p["childCount_$i"], $childrenAges);
    }

    $hotelName = $p['hotelName'];
    if (isset($p['hotelCode']) && $p['hotelCode'] != '') {
      if(in_array(1, $p['providers']))
      {
        $hotelsMap = \DomiturHotelMapping::findAll(array('zion_code' => $p['hotelCode']));
        if (count($hotelsMap) == 0) {
          return NULL;
        }

        $hotelName = '';
        foreach ($hotelsMap as $map) {
          $hotelName .= $map->code . '#';
        }
      }
      else{
        $hotelName = $p['hotelCode'] . "#";
      }
    }

    $AvailableHotelsRQ = new AvailableHotelsRQ($hotelName, $p['destCode'], $p['checkin'], $nights, $rooms);

    $profiling = new \Profiling('hotelAvail query', 'DomiturBooking');
    $profiling->init();
    $xml = $this->request->execRequest($AvailableHotelsRQ);
    $profiling->end(strlen($xml));

    //__logtxt($xml);
    $rs = simplexml_load_string(utf8_encode($xml));
    $regimen = '';
    if(isset($p['regimen']) && $p['regimen'] != '') {
      $regimen = explode(',', implode(',', $p['regimen']));
    }
    $ls = new HotelAvailListRS($rs, $p['destCode'], $regimen, $p['factorFee'], $p['extraFee']);

    if (isset($p['stars']) && $p['stars'] != '') {
      $star = explode(',', implode(',', $p['stars']));
      $hotel = $ls->getHotelsAvail();
      for ($i = 0; $i < count($hotel); $i++) {
          $service = $hotel[$i];
          if (!in_array("'" . $service->getCategoryCode() . "'", $star)) {
            unset($ls->serviceHotels[$i]);
          }
      }
    }

    if(isset($p['zone_name']) && $p['zone_name'] != '') {
      $zone_name = $p['zone_name'];
      $hotel = $ls->getHotelsAvail();
      for ($i = 0; $i < count($hotel); $i++) {
          if(!preg_match("/$zone_name/i", $hotel[$i]->getLocation())) {
            unset($ls->serviceHotels[$i]);
          }
      }
    }

    return $ls;
  }

  public function execHotelServiceAddRQ($p) {
    $service = new SCartServiceRS($p);
    return new PurchaseRS(array($service));
  }

  public function execPurchaseConfirmRQ($p) {
    $services_count = intval($p['servicesCount']);
    $services = array();
    for ($i = 1; $i <= $services_count; $i++) {

      // $item = \ShoppingCart::getItem($p["scartId_$i"]);
      $book_params = (array)json_decode($p['book_params']);
      $item = $book_params;
      if ($item['provider'] != \BookingProvider::DOMITUR) { continue; }

      $hotel_code = $item['hotelCode'];
      $holder_name = $p['customerName_' . $i . '_1'];
      $holder_lastname = $p['customerLastName_' . $i . '_1'];
      $date_in = $item['dateFrom'];
      $from = new \DateTime($item['dateFrom']);
      $to = new \DateTime($item['dateTo']);
      $nights = $to->diff($from)->days;
      $client_comments = $p["comments_$i"];
      $contact = $p['agencyReference'];
      $numberOfRooms = count($item['rooms']);
      $rooms = array();
      for ($j = 0; $j < $numberOfRooms; $j++) {
        $room = (object)$item['rooms'][$j];
        $childAges = array();
        for ($r = 1; $r <= $room->childCount; $r++) {
          $childAges[$r] = $item['childAges'][$r - 1];
        }
        $rooms[] = new Room($room->adultCount, $room->childCount, $childAges);
      }

      $PurchaseRQ = new PurchaseRQ($hotel_code, $holder_name, $holder_lastname, $date_in, $nights, $client_comments, $contact, $numberOfRooms, $rooms);
      $xml = $this->request->execRequest($PurchaseRQ, 'XML_RQ', 'BB_DT_RQ_CONFIRM');
      \Log::info($xml, 'BB_DT_RS_CONFIRM');
      $rs = simplexml_load_string(utf8_encode($xml));

      if ($rs->parameters->booking->error_code == 0) {
        $localizer = $rs->parameters->booking->nr_booking;
        $quitar = array(',');
        $total_price = str_replace($quitar, '', $rs->parameters->booking->booking_net_total);
        $serviceRS = new SCartServiceRS($item, $localizer, $total_price);
        $services[] = $serviceRS;
      }
      else {
        $msg = '[' . $rs->parameters->booking->error_code . '] ' . $rs->parameters->booking->error_description;
        \Log::error($msg, 'BOOK DT');
        $arr_errors = array();
        $arr_errors['code'] = strval($rs->parameters->booking->error_code);
        $arr_errors['msg'] = strval($rs->parameters->booking->error_description);
        $arr_errors['provider'] = \BookingProvider::DOMITUR;

        $serviceRS = new SCartServiceRS($item, '', 0, $arr_errors);
        $services[] = $serviceRS;
      }
    }

    // Check if a reservation was done or if all items are from another Provider.
    if (count($services) == 0) return NULL;

    return new PurchaseRS($services);
  }

  function execPurchaseCancelRQ($p) {
    $BookingCancellationRQ = new BookingCancellationRQ($p['localizer']);

    $xml = $this->request->execRequest($BookingCancellationRQ, 'XML_RQ', 'CANCEL');
    \Log::info($xml, 'XML_RS_CANCEL');
    $rs = simplexml_load_string(utf8_encode($xml));

    if ($rs->parameters->bookingcancelation->error_code != 0) {
      return NULL;
    }

    $cancel_penalty = floatval($rs->parameters->bookingcancelation->booking->cancel_penalty);
    return new PurchaseRS(array(), $cancel_penalty);
  }

  function execHotelDetailRQ($p) {
    $hotelName = $p['hotelCode']. '#';
    $from = new \DateTime(dateFormatForDB($p['params']['checkin']));
    $to = new \DateTime(dateFormatForDB($p['params']['checkout']));

    $nights = $to->diff($from)->days;

    $numberOfRooms = $p['params']['habitaciones'];
    $rooms = array();
    $con=1;
    for ($j = 1; $j <= $numberOfRooms; $j++) {
      $adultCount = $p['params']['adultos'.$j];
      $childAges = array();
      for ($r = 1; $r <= $p['params']['ninos'.$j]; $r++) {
        $childAges[$r] = $p['params']['childAge'.$con];
        $con++;
        if ($r == $p['params']['ninos'.$j]) {
          break;
        }
      }
      $rooms[] = new Room($adultCount, $p['params']['ninos'.$j], $childAges);
    }
    $fech = explode('/', $p['params']['checkin']);
    $fecha = $fech[2] . $fech[1] . $fech[0];

    $destCode = DestinationCodeMap::getCode($p['params']['destination_code']);
    $AvailableHotelsRQ = new AvailableHotelsRQ($hotelName, $destCode, $fecha, $nights, $rooms);

    $xml = $this->request->execRequest($AvailableHotelsRQ);

    $rs = simplexml_load_string(utf8_encode($xml));

    $response = new HotelDetailRS($rs, $p['params']['destination_code']);

    return $response;

  }

  function execPurchaseDetail($p) {
    $Booking_PurchaseRQ = new PurchaseDetailRQ($p['localizer']);

    $xml = $this->request->execRequest($Booking_PurchaseRQ);

    $rs = simplexml_load_string(utf8_encode($xml));

    $service = new ServiceRS($rs, $p['localizer']);

    $response = new PurchaseRS(array($service));
    return $response;
  }
}

?>
