<?php
namespace Domitur;

class PurchaseRS extends \BookingPurchase {

  public $services;
  public $totalPrice;

  public function __construct($services, $totalPrice = 0) {
    $this->services = $services;
    $this->totalPrice = $totalPrice;
  }

  public function getPurchaseToken() {
    return '';
  }

  public function getStatus() {
    return '';
  }

  public function getHolder() {
    return '';
  }

  public function getBookingServices() {
    return $this->services;
  }

  public function getTotalPrice() {
    return $this->totalPrice;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getCreationDate() {
    return '00000000';
  }

  public function getReferenceFileNumber() {
    return '';
  }

  public function getReferenceIncomingOffice() {
    return '';
  }

  public function getAgencyReference() {
    return '';
  }

  public function getPaymentDataDescription() {
    return '';
  }
}

?>