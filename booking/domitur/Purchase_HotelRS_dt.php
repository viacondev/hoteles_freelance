<?php
namespace Domitur;

include_once('PurchaseRoomsRS_dt.php');

class Purchase_HotelRS extends \HotelAvail {

  private $element;
  private $hotelInfo;
  private $availableRooms;

  public function __construct($serviceHotelElement) {
    $this->element = $serviceHotelElement->booking_head;
    $this->availableRooms = array();
    $date_prub = '';
    foreach ($serviceHotelElement->booking_lines->line as  $room) {

      if(($room->date) != ($date_prub) && $date_prub != '') {
        break;
      }
      $this->availableRooms[] = new PurchaseRoomsRS_dt($serviceHotelElement->booking_head, $room->room);
      $date_prub = $room->date;
    }

  }

  public function getProvider() {
    return \BookingProvider::DOMITUR;
  }

  public function getProviderName() {
    return 'DOMITUR';
  }

  public function getAvailToken() {
    return '';
  }

  public function getCode() {
    return strval($this->element->hotel->code);
  }

  public function getName() {
    return strval($this->element->hotel->hotel_name);
  }

  public function getDateFrom() {
    $date_arr = explode('/', $this->element->date_in);
    if (count($date_arr) >= 3) {
      return $date_arr[2] . '-' . $date_arr[1] . '-' . $date_arr[0];
    }
    return '00000000';
  }

  public function getDateTo() {
    $date_arr = explode('/', $this->element->date_out);
    if (count($date_arr) >= 3) {
      return $date_arr[2] . '-' . $date_arr[1] . '-' . $date_arr[0];
    }
    return '00000000';
  }

  public function getDestinationCode() {
    return strval($this->element->hotel->hotel_city);
  }

  public function getDestinationType() {
    return strval($this->element->hotel->hotel_province);
  }

  public function getMainImage() {
    return 'assets/images/no-photo.jpg';
  }

  public function getDescription() {
    return "";
  }

  public function getLatitude() {
    return '';
  }

  public function getLongitude() {
    return '';
  }

  public function getZone() {
    return '';
  }

  public function getLocation() {
    return '';
  }

  public function getCategory() {
    return strval($this->element->hotel->hotel_category);
  }

  public function getCategoryType() {
   return '';
  }

  public function getCategoryCode() {
    $this->element->hotel->hotel_category;
  }

  public function getRoomsAvail() {
    return $this->availableRooms;
  }

  public function getCurrency() {
    return strval('USD');
  }

  public function getContractName() {
    return strval('');
  }

  public function getContractIncomingOfficeCode() {
    return strval('');
  }

  public function getContractComment() {
    return '';
  }

  public function getClientComment() {
    return '';
  }

  public function getSupplier() {
    return '';
  }

  public function getSupplierVatNumber() {
    return '';
  }

  public function getDiscountList() {
    return array();
  }

  public function createGroupedRooms() {
    return '';
  }

  public function getAllPaxRequired() {
    return false;
  }

  public function getXtraDataRequired() {
    return false;
  }
}

?>
