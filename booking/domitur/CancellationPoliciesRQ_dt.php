<?php
namespace Domitur;

class CancellationPoliciesRQ extends RQOperation {

  public $code;
  public $date_in;

  public function __construct($code, $date_in) {
    $this->code = $code;
    $this->date_in = $date_in;
  }

  public function getType() {
    return 85;
  }

  public function getName() {
    return 'Cancellation Policies';
  }

  public function getParams() {
    return '
      <hotel_code>' . $this->code . '</hotel_code>
      <date_in>' . $this->date_in . '</date_in>';
  }
}

?>