<?php 
set_time_limit(0);
class MultiCUrl {

    private $urls       = array();
    private $options    = array();
    private $results    = array();
    private $providers  = array();
    private $lengths    = array();

    public function pushData($cod_provider, $options)
    {
        $this->urls[]         = '';
        $this->options[]      = $options;
        $this->results[]      = null;
        $this->lengths[]      = 0;
        $this->providers[]    = $cod_provider;
    }

    public function getResults()
    {
        return array_combine($this->providers, $this->results);
    }

    function rolling_curl() {

        // make sure the rolling window isn't greater than the # of urls
        $rolling_window = 6;
        $rolling_window = (sizeof($this->options) < $rolling_window) ? sizeof($this->options) : $rolling_window;

        $master = curl_multi_init();
        $curl_arr = array();

        // add additional curl options here
        $std_options = array(CURLOPT_FOLLOWLOCATION => true);
        /**foreach($this->options as $i => $opt)
        {
            $this->options[$i] += $std_options;
        }*/
        //$options = ($custom_options) ? ($std_options + $custom_options) : $std_options;

        // start the first batch of requests
        for ($i = 0; ($i < $rolling_window && $i < count($this->options)); $i++) {
            $ch = curl_init();
            $callback                                   = $this->get_write_function($i);
            //$this->options[$i][CURLOPT_URL]             = $this->urls[$i];
            $this->options[$i] += $std_options;
            $this->options[$i][CURLOPT_WRITEFUNCTION]   = $callback;
            curl_setopt_array($ch,$this->options[$i]);
            curl_multi_add_handle($master, $ch);
        }

        do {
            while(($execrun = curl_multi_exec($master, $running)) == CURLM_CALL_MULTI_PERFORM);
            if($execrun != CURLM_OK)
                break;
            // a request was just completed -- find out which one
            while($done = curl_multi_info_read($master)) {
                $info = curl_getinfo($done['handle']);
                if ($info['http_code'] == 200)  {
                    //$output = curl_multi_getcontent($done['handle']);

                    // request successful.  process output using the callback function.
                    //$callback($output);

                    // start a new request (it's important to do this before removing the old one)
                    if($i < count($this->options))
                    {
                        $ch = curl_init();
                        $i++;
                        $callback                                   = $this->get_write_function($i);
                        //$this->options[$i][CURLOPT_URL]             = $this->urls[$i];
                        $this->options[$i]                          += $std_options;
                        $this->options[$i][CURLOPT_WRITEFUNCTION]   = $callback;
                        curl_setopt_array($ch, $this->options[$i]);
                        curl_multi_add_handle($master, $ch);
                    }

                    // remove the curl handle that just completed
                    curl_multi_remove_handle($master, $done['handle']);
                } else {
                    // request failed.  add error handling.
                }
            }
        } while ($running);
        
        curl_multi_close($master);
        return true;
    }
    function get_write_function($key) {
        $this->lengths[$key] = 0;
        $this->results[$key] = '';
        $obj    = $this;
        $funky  = function ($ch, $str) use ($obj, $key) {
            $obj->results[$key]     .= $str;
            $length                 = strlen($str);
            $obj->lengths[$key] += $length;
            /*if($obj->lengths[$key] >= 5000) {
                return -1;
            }*/
            return $length;
        };
        return $funky;
    }
}
?>