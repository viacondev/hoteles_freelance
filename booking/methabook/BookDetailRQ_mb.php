<?php
namespace Methabook;

class BookDetailRQ extends RQOperation {

  private $from;
  private $to;

  public function __construct($from, $to) {
    $this->from = $from;
    $this->to = $to;
  }

  public function getWSDL() {
    return 'OTA_Read.asmx?wsdl';
  }

  public function getMethodName() {
    return 'OTA_ReadService';
  }

  public function getXML() {
    $xml = '
      <OTA_ReadService xmlns="http://www.opentravel.org/OTA/2003/05">
        <OTA_ReadRQ PrimaryLangID="es">
          __POS__
          <ReadRequests>
            <ReadRequest>
              <Verification>
                <TPA_Extensions>
                <TravelFrom>2016-09-01</TravelFrom>
                <ResStatus>Can,CaC,Con,Con*,Pag,OK,Tar</ResStatus>
                <ReservationFrom>' . $this->from . '</ReservationFrom>
                <ReservationTo>' . $this->to . '</ReservationTo>
                </TPA_Extensions>
              </Verification>
            </ReadRequest>
          </ReadRequests>
        </OTA_ReadRQ>
      </OTA_ReadService>';

    return $xml;
  }
}
?>
