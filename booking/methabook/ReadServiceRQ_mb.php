<?php
namespace Methabook;

class ReadServiceRQ extends RQOperation {

  private $localizer;

  public function __construct($localizer) {
    $this->localizer = $localizer;
  }

  public function getWSDL() {
    return 'OTA_Read.asmx?wsdl';
  }

  public function getMethodName() {
    return 'OTA_ReadService';
  }

  public function getXML() {
    $xml = '
      <OTA_ReadService xmlns="http://www.opentravel.org/OTA/2003/05">
        <OTA_ReadRQ PrimaryLangID="es">
          __POS__
          <ReadRequests>
            <ReadRequest>
              <UniqueID ID="' . $this->localizer . '"/>
            </ReadRequest>
          </ReadRequests>
        </OTA_ReadRQ>
      </OTA_ReadService>';

    return $xml;
  }
}
?>