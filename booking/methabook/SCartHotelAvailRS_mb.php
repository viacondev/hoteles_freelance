<?php
namespace Methabook;

include_once('SCartRoomAvailRS_mb.php');

class SCartHotelAvailRS extends \HotelAvail {

  private $data;
  private $rules;
  private $currentProvider;

  public function __construct($data) {
    $this->data = $data;
    $this->currentProvider = $data->provider;

     // Build Booking Rules
    $from = $this->getDateFrom();
    $to = $this->getDateTo();
    $from = substr($from, 0, 4) . '-' . substr($from, 4, 2) . '-' . substr($from, 6, 4);
    $to = substr($to, 0, 4) . '-' . substr($to, 4, 2) . '-' . substr($to, 6, 4);

    $params = array(
      'availToken' => $this->getAvailToken(),
      'hotelCode' => $this->getCode(),
      'SHRUI' => $this->data->rooms[0]->SHRUI,
      'checkin' => $from,
      'checkout' => $to
      );
    $rules = new HotelBookingRuleRQ($params);
    $rq = new MethabookRQ();
    $xml = $rq->execRequest($rules, 'XML_RQ', 'BB_DT2_RQ_POLICY');
    $this->rules = new BookingRuleRS(new MethabookRS($xml, 'XML_RS', 'BB_DT2_RS_POLICY'), $this, $this->data->factorFee);

    $this->roomsAvail = array();
    foreach ($this->data->rooms as $room) {
      $this->roomsAvail[] = new SCartRoomAvailRS($room, $this, $this->getDateFrom(), $this->getDateTo(), $this->data->childAges, $this->rules, $this->data->factorFee);
    }
  }

  public function getProvider() {
    return $this->currentProvider;
  }

  public function getProviderName() {
    return 'METHABOOK';
  }

  public function getAvailToken() {
    return $this->data->availToken;
  }

  public function getCode() {
    return $this->data->hotelCode;
  }

  public function getName() {
    return $this->data->hotelName;
  }

  public function getDateFrom() {
    return $this->data->dateFrom;
  }

  public function getDateTo() {
    return $this->data->dateTo;
  }

  public function getDestinationCode() {
    return $this->data->destCode;
  }

  public function getDestinationType() {
    return '';
  }

  public function getMainImage() {
    return '';
  }

  public function getDescription() {
    return '';
  }

  public function getLatitude() {
    return '';
  }

  public function getLongitude() {
    return '';
  }

  public function getZone() {
    return '';
  }

  public function getLocation() {
    return '';
  }

  public function getCategory() {
    return '';
  }

  public function getCategoryType() {
    return '';
  }

  public function getCategoryCode() {
    return strval($this->data->category);
  }

  public function getRoomsAvail() {
    return $this->roomsAvail;
  }

  public function getGroupedRooms() {
    return $this->roomsAvail;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getContractName() {
    return '';
  }

  public function getContractIncomingOfficeCode() {
    return '';
  }

  public function getContractComment() {
    return $this->rules->getBookingRulesDesc();
  }

  public function getClientComment() {
    return '';
  }

  public function getSupplier() {
    return '';
  }

  public function getSupplierVatNumber() {
    return '';
  }

  public function getDiscountList() {
    return array();
  }

  public function getAllPaxRequired() {
    return true;
  }

  public function getXtraDataRequired() {
    return true;
  }

  public function isDataRequired() {
   return false;
  }

  public function calculateNights() {
    $from = new \DateTime($this->getDateFrom());
    $to = new \DateTime($this->getDateTo());
    return $to->diff($from)->days;
  }
}

?>
