<?php
namespace Methabook;

class RoomStayCandidateRQ extends RQOperation {

  private $params;

  public function __construct($params = array()) {
    $this->params = $params;
  }

  public function getXML() {
    $p = $this->params;
    $childAges = array();
    foreach ($p['childAges'] as $age) {
      if (!isset($childAges[$age])) {
        $childAges[$age] = 0;
      }
      $childAges[$age]++;
    }

    $adtGuest = '<GuestCount Count="' . $p['adultCount'] . '" />';
    $chdGuest = '';
    foreach ($childAges as $key => $value) {
      $chdGuest .= '<GuestCount Age="' . $key . '" Count="' . $value . '"/>';
    }

    $xml = '
            <RoomStayCandidate Quantity="' . $p['roomCount'] . '">
              <GuestCounts>
                ' . $adtGuest . $chdGuest . '  
              </GuestCounts>
            </RoomStayCandidate>';

    return $xml;
  }
}
?>