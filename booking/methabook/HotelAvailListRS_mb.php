<?php
namespace Methabook;

include_once('HotelAvailRS_mb.php');

class HotelAvailListRS extends \HotelAvailList {

  private $elements;
  public $serviceHotels;

  public function __construct($rs, $destCode, $rooms, $hotelName = '', $board_descrip = '', $currentProvider, $factorFee = 1, $extraFee = 0) {
    $this->elements = $rs->getResponse()->xpath('//ns1:OTA_HotelAvailRS');
    $this->elements = $this->elements[0];
    $attrs          = $this->elements->attributes();
    $sequenceNmbr   = strval($attrs['SequenceNmbr']);
    $this->serviceHotels = array();
    if (isset($this->elements->RoomStays)) {
      foreach ($this->elements->RoomStays->RoomStay as $hotel) {
        $hotelAvail = new HotelAvailRS($hotel, $destCode, $rooms, $sequenceNmbr, $board_descrip, $currentProvider, $factorFee, $extraFee);
        $count      = count($hotelAvail->getGroupedRooms());
        if ($hotelName != '' && $count > 0) {
          $name = $hotelAvail->getName();
          if (preg_match("/$hotelName/i", $name)) {
            $this->serviceHotels[] = $hotelAvail;
          }
        }
        else {
          if($count > 0) {
            $this->serviceHotels[] = $hotelAvail;
          }
        }
      }
    }
  }

  public function getHotelsAvail() {
    return $this->serviceHotels;
  }

  public function getCurrentPage() {
    return 1;
  }

  public function getTotalPages() {
    return 1;
  }
}

?>
