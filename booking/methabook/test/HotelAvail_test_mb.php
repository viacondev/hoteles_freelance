<?
namespace Methabook;

include_once('../../BookingProvider.php');
include_once('../../BookingRequestTypes.php');
include_once('../../HotelAvailList.php');
include_once('../../BookingPurchase.php');
include_once('../../BookingPurchaseList.php');
include_once('../../BookingCustomer.php');
include_once('../../BookingCancellationPolicy.php');
include_once('../../BookingTicketAvailList.php');
include_once('../../BookingPrice.php');
include_once('../../BookingOperationDate.php');
include_once('../../BookingServiceDetail.php');
include_once('../../BookingTransferAvailList.php');
include_once('../../BookingHotelDetail.php');
include_once('../../../zion/helpers/logger.php');
include_once('../MethabookBooking.php');

$params = array(
  'requestType' => 0,
  'destCode' => 'PMI',
  'destType' => 'SIMPLE',
  'checkin' => '20150416',
  'checkout' => '20150420',
  'roomCount' => '1',
  'page' => '1',
  'hotelName' => '',
  'hotelCode' => '',
  'zoneCode' => '',
  'orderPrice' => '',
  'providers' => array(1, 23),
  'adultCount_1' => '2',
  'childCount_1' => '0',
  'totalChildCount' => '0'
);

$rq = new MethabookBooking();
$rs = $rq->execRequest($params);
__logarr($rs);

?>