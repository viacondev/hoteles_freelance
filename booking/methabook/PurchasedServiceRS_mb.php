<?php
namespace Methabook;

include_once('PurchasedHotelRS_mb.php');

/**
 * Class used for purchased Services (used for vouchers view)
 */
class PurchasedServiceRS extends \BookingService {

  private $elements;

  public function __construct($rs) {
    $this->elements = $rs->getResponse()->xpath('//ns1:OTA_ResRetrieveRS');
    $this->elements = $this->elements[0];
  }

  public function getLocalizer() {
    $attrs = $this->elements->ReservationsList->HotelReservation->ResGlobalInfo->HotelReservationIDs->HotelReservationID->attributes();
    return strval($attrs['ResID_Source']);
  }

  public function getSPUI() {
    $attrs = $this->elements->ReservationsList->HotelReservation->UniqueID->attributes();
    return strval($attrs['ID']);
  }

  public function getServiceType() {
    return 'ServiceHotel';
  }

  public function getServiceStatus() {
    return '';
  }

  public function getDirectPayment() {
    return '';
  }

  public function getDateFrom() {
    $attrs = $this->elements->ReservationsList->HotelReservation->RoomStays->RoomStay->TimeSpan->attributes();
    $date = strval($attrs['Start']); 
    return str_replace('-', '', $date);
  }

  public function getDateTo() {
    $attrs = $this->elements->ReservationsList->HotelReservation->RoomStays->RoomStay->TimeSpan->attributes();
    $date = strval($attrs['End']); 
    return str_replace('-', '', $date);
  }

  public function getCurrency() {
    $attrs = $this->elements->ReservationsList->HotelReservation->RoomStays->RoomStay->Total->attributes();
    return strval($attrs['CurrencyCode']);
  }

  public function getTotalAmount() {
    $attrs = $this->elements->ReservationsList->HotelReservation->RoomStays->RoomStay->Total->attributes();
    return floatval($attrs['AmountAfterTax']);
  }

  public function getAdditionalCosts() {
    return array();
  }

  public function getServiceInfo() {
    return new PurchasedHotelRS($this->elements->ReservationsList->HotelReservation);
  }
  
  public function getSupplementList() {
    return array();
  }

  public function getStatus() {
    $attrs = $this->elements->ReservationsList->HotelReservation->attributes();
    $status = $attrs['ResStatus'];

    if ($status == 'Pag' || $status == 'OK' || $status == 'Con' || $status == 'Con*' || $status == 'Tar') {
      return \ServiceStatus::CONFIRMED;
    }
    if ($status == 'Can' || $status == 'CaC') {
      return \ServiceStatus::CANCELLED;
    }
    if ($status == 'PRe' || $status == 'PDi' || $status == 'PDI' || $status == 'Ini') {
      return \ServiceStatus::PENDING;
    }
  }
  
}

?>