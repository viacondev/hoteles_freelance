<?php
namespace Methabook;

class DestinationCodeMap {

  public static function getCode($c) {
    $map = array(
      'SLT' => 'SLA',
      'CAB' => 'SJD',
      'MDF' => 'MEX',
      'LVS' => 'LAS', // Las Vegas, NV
      'PCM' => 'JPD036980',
      'CUN' => 'CUN',
      'PUJ' => 'PUJ',
      'VRA' => 'VRA',
      'HAV' => 'HAV',
      'IQQ' => 'JPD037941',
      'CTG' => 'JPD037019',
      'SMA' => 'JPD051530'
      );

    if (isset($map[$c])) {
      return $map[$c];
    }

    return NULL;
  }

}

?>
