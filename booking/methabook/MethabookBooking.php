<?php
namespace Methabook;
ini_set('memory_limit', -1);
include_once('MethabookRQ.php');
include_once('MethabookRS_mb.php');
include_once('RQOperation_mb.php');
include_once('HotelAvailRQ_mb.php');
include_once('HotelAvailListRS_mb.php');
include_once('HotelBookingRuleRQ_mb.php');
include_once('PurchaseRS_mb.php');
include_once('SCartServiceRS_mb.php');
include_once('HotelBookingRuleRQ_mb.php');
include_once('BookingRuleRS_mb.php');
include_once('HotelReservationRQ_mb.php');
include_once('ServiceRS_mb.php');
include_once('CancellBookRQ_mb.php');
include_once('CancellBookRS_mb.php');
include_once('ReadServiceRQ_mb.php');
include_once('PurchasedServiceRS_mb.php');
include_once('HotelDetailRQ_mb.php');
include_once('HotelDetailRS_mb.php');
include_once('DestinationCodeMap_mb.php');
include_once('ConfirmHotelAvail_mb.php');
include_once('ConfirmRoomAvail_mb.php');
include_once('BookDetailRQ_mb.php');
class MethabookBooking {
  private $request;
  private $currentProvider;
  public function __construct($provider = 0) {
    $this->request = new MethaBookRQ();
    $this->currentProvider = $provider;
  }

  public function execRequest($params) {
    switch ($params['requestType']) {
      case \BookingRequestTypes::SearchHotelAvail:
        return $this->execSearchHotelAvailRQ($params);
        break;

      case \BookingRequestTypes::AddHotelService:
        return $this->execHotelServiceAddRQ($params);
        break;

      case \BookingRequestTypes::ConfirmPurchase:
        return $this->execPurchaseConfirmRQ($params);
        break;

      case \BookingRequestTypes::CancelPurchase:
        return $this->execPurchaseCancelRQ($params);
        break;

      case \BookingRequestTypes::PurchaseDetail:
        return $this->execPurchaseDetailRQ($params);
        break;

      case \BookingRequestTypes::HotelDetail:
        return $this->execHotelDetailRQ($params);
        break;

      case \BookingRequestTypes::PurchaseDetailList:
        return $this->execPurchaseDetailList($params);
        break;

      default:
        return NULL;
        break;
    }
  }

  public function execSearchHotelAvailRQ($p) {
    $destCode = DestinationCodeMap::getCode($p['destCode']);
    if ($p['hotelCode'] != '' && substr($p['hotelCode'], 0, 2) != 'JP' || $destCode == NULL) return NULL;
    // Only search for page=1 since there is no pagination on MB
    // if ($p['page'] != 1) return NULL;

    $roomCount = intval($p['roomCount']);

    // Used to return the original requested accomodancy
    $roomsUngrouped = array();
    // Group rooms with the same occupancy
    $roomsGroups = array();
    for ($i = 1; $i <= $roomCount; $i++) {
      $accomodation = $p["adultCount_$i"] . '_' . $p["childCount_$i"];
      if (!isset($roomsGroups[$accomodation])) {
        $roomsGroups[$accomodation] = 0;
      }
      $roomsGroups[$accomodation]++;
      $roomsUngrouped[] = array('adultCount' => $p["adultCount_$i"], 'childCount' => $p["childCount_$i"]);
    }

    $rooms = array();
    $currentChild = 1;
    foreach ($roomsGroups as $key => $value) {
      $accomodation = explode('_', $key);
      $adt = intval($accomodation[0]);
      $chd = intval($accomodation[1]);

      $childAges = array();
      for ($i = 1; $i <= $chd; $i++) {
        $childAges[] = $p["childAge_$currentChild"];
        $currentChild++;
      }

      $rooms[] = new RoomStayCandidateRQ(array(
        'roomCount' => $value,
        'adultCount' => $adt,
        'childAges' => $childAges
      ));

      $checkin = substr($p['checkin'], 0, 4) . '-' . substr($p['checkin'], 4, 2) . '-' . substr($p['checkin'], 6, 4);
      $checkout = substr($p['checkout'], 0, 4) . '-' . substr($p['checkout'], 4, 2) . '-' . substr($p['checkout'], 6, 4);

      $p['destCode'] = DestinationCodeMap::getCode($p['destCode']);
      $HotelAvailRQ = new HotelAvailRQ(array(
        'destCode' => $p['destCode'],
        'hotelCode' => $p['hotelCode'],
        'checkin' => $checkin,
        'checkout' => $checkout,
        'rooms' => $rooms
      ));
    }

    // $profiling = new \Profiling('hotelAvail query', 'Domitur2');
    // $profiling->init();
    $xml = $this->request->execRequest($HotelAvailRQ);
    // $profiling->end(strlen($xml));
    // include('dummy/OTA_HotelAvailService.php');
    // $profiling = new \Profiling('hotelAvail parseRS', 'MethabookBooking');
    // $profiling->init();
    $rs = new MethabookRS($xml);
    $regimen = '';
    if(isset($p['regimen']) && $p['regimen'] != '') {
      $regimen = explode(',', implode(',', $p['regimen']));
    }
    $result = new HotelAvailListRS($rs, $p['destCode'], $roomsUngrouped, $p['hotelName'], $regimen, $this->currentProvider, $p['factorFee']);

    if (isset($p['stars']) && $p['stars'] != '') {
      $star = explode(',', implode(',', $p['stars']));
      $hotel = $result->getHotelsAvail();
      for ($i = 0; $i < count($hotel); $i++) {
          $service = $hotel[$i];
          if (!in_array("'" . $service->getCategoryCode() . "'", $star)) {
            unset($result->serviceHotels[$i]);
          }
      }
    }

    if(isset($p['zone_name']) && $p['zone_name'] != '') {
      $zone_name = $p['zone_name'];
      $hotel = $result->getHotelsAvail();
      for ($i = 0; $i < count($hotel); $i++) {
          if(!preg_match("/$zone_name/i", $hotel[$i]->getLocation())) {
            unset($result->serviceHotels[$i]);
          }
      }
    }
    // $profiling->end();
    return $result;
  }

  public function prepararConsulta($p) {
    if ($p['hotelCode'] != '' && substr($p['hotelCode'], 0, 2) != 'JP') return NULL;
    // Only search for page=1 since there is no pagination on MB
    // if ($p['page'] != 1) return NULL;

    $roomCount = intval($p['roomCount']);

    // Used to return the original requested accomodancy
    $roomsUngrouped = array();
    // Group rooms with the same occupancy
    $roomsGroups = array();
    for ($i = 1; $i <= $roomCount; $i++) {
      $accomodation = $p["adultCount_$i"] . '_' . $p["childCount_$i"];
      if (!isset($roomsGroups[$accomodation])) {
        $roomsGroups[$accomodation] = 0;
      }
      $roomsGroups[$accomodation]++;
      $roomsUngrouped[] = array('adultCount' => $p["adultCount_$i"], 'childCount' => $p["childCount_$i"]);
    }

    $rooms = array();
    $currentChild = 1;
    foreach ($roomsGroups as $key => $value) {
      $accomodation = explode('_', $key);
      $adt = intval($accomodation[0]);
      $chd = intval($accomodation[1]);

      $childAges = array();
      for ($i = 1; $i <= $chd; $i++) {
        $childAges[] = $p["childAge_$currentChild"];
        $currentChild++;
      }

      $rooms[] = new RoomStayCandidateRQ(array(
        'roomCount' => $value,
        'adultCount' => $adt,
        'childAges' => $childAges
      ));

      $checkin = substr($p['checkin'], 0, 4) . '-' . substr($p['checkin'], 4, 2) . '-' . substr($p['checkin'], 6, 4);
      $checkout = substr($p['checkout'], 0, 4) . '-' . substr($p['checkout'], 4, 2) . '-' . substr($p['checkout'], 6, 4);

      $p['destCode'] = DestinationCodeMap::getCode($p['destCode']);
      $HotelAvailRQ = new HotelAvailRQ(array(
        'destCode' => $p['destCode'],
        'hotelCode' => $p['hotelCode'],
        'checkin' => $checkin,
        'checkout' => $checkout,
        'rooms' => $rooms
      ));
    }

    /*$profiling = new \Profiling('hotelAvail query', 'Domitur2');
    $profiling->init();
    $xml = $this->request->execRequest($HotelAvailRQ);
    $profiling->end(strlen($xml));*/

    return $this->request->allOptions($HotelAvailRQ);
  }

  public function obtenerResultado($xml, $p) {
    

    $xml = gzinflate(substr($xml, 10, -8));
    if ($xml === false) {
      return array();
    }
    $roomCount = intval($p['roomCount']);
    // Used to return the original requested accomodancy
    $roomsUngrouped = array();
    // Group rooms with the same occupancy
    $roomsGroups = array();
    for ($i = 1; $i <= $roomCount; $i++) {
      $accomodation = $p["adultCount_$i"] . '_' . $p["childCount_$i"];
      if (!isset($roomsGroups[$accomodation])) {
        $roomsGroups[$accomodation] = 0;
      }
      $roomsGroups[$accomodation]++;
      $roomsUngrouped[] = array('adultCount' => $p["adultCount_$i"], 'childCount' => $p["childCount_$i"]);
    }

    $rs         = new MethabookRS($xml);
    $regimen    = '';
    $hotelName  = '';
    $result     = new HotelAvailListRS($rs, $p['destCode'], $roomsUngrouped, $hotelName, $regimen, $this->currentProvider, $p['factorFee']);

    if(isset($p['zone_name']) && $p['zone_name'] != '') {
      $zone_name = $p['zone_name'];
      $hotel = $result->getHotelsAvail();
      for ($i = 0; $i < count($hotel); $i++) {
          if(!preg_match("/$zone_name/i", $hotel[$i]->getLocation())) {
            unset($result->serviceHotels[$i]);
          }
      }
    }
    // $profiling->end();
    return $result;
  }

  public function execHotelServiceAddRQ($p) {
    $service = new SCartServiceRS($p);

    return new PurchaseRS(array($service));
  }

  public function execPurchaseConfirmRQ($p) {
    $arr_provider   = array(159); 
    $services_count = intval($p['servicesCount']);
    $services = array();
    $errors   = array();
    for ($i = 1; $i <= $services_count; $i++) {
      $book_params = (array)json_decode($p['book_params']);
      $item = $book_params;
      if (!in_array($item['provider'], $arr_provider)) { continue; }

      $HotelReservationRQ = new HotelReservationRQ($p, $item, $i);
      $xml = $this->request->execRequest($HotelReservationRQ, 'XML_RQ', 'BB_DT2_RQ_CONFIRM');
 
      $rs       = new MethabookRS($xml, 'XML_RS', 'BB_DT2_RS_CONFIRM');
      $result   = $rs->getResponse();
      $response = $result->Body->OTA_HotelResV2ServiceResponse->OTA_HotelResRS;
      if(isset($response->Errors)) {
        $result = $response->Errors->ErrorType->attributes();
        $error  = array();
        $error['code'] = strval($result['Code']);
        $error['msg']  = strval($result['ShortText']);
        $error['provider']  = \BookingProvider::DOMITUR2;
        $errors[] = $error;
      }
      $services[] = new ServiceRS($response, $item, $errors);
    }

    return new PurchaseRS($services);
  }

  function execPurchaseCancelRQ($p) {
    $CancellBookRQ = new CancellBookRQ($p['localizer']);

    $xml = $this->request->execRequest($CancellBookRQ, 'XML_RQ', 'BB_DT2_RQ_CANCEL');

    $rs = new MethabookRS($xml, 'XML_RS', 'BB_DT2_RS_CANCEL');
    $res = $rs->getResponse();
    $result = $res->Body->OTA_CancelServiceResponse->OTA_CancelRS;
    if (isset($result->Errors->ErrorType)) {
      return NULL;
    }
    $CancellBookRS = new CancellBookRS($rs);

    return new PurchaseRS(array(), $CancellBookRS->getCancellationFee());
  }

  public function execPurchaseDetailRQ($p) {
    $ReadServiceRQ = new ReadServiceRQ($p['localizer']);

    $xml = $this->request->execRequest($ReadServiceRQ);
    // include('dummy/OTA_ReadServiceResponse.php');
    $rs = new MethabookRS($xml);

    $service = new PurchasedServiceRS($rs);

    return new PurchaseRS(array($service));
  }

  public function execHotelDetailRQ($p) {
    $HotelDetailRQ = new HotelDetailRQ($p['hotelCode']);

    $xml = $this->request->execRequest($HotelDetailRQ);
    // include('dummy/OTA_HotelDescriptiveInfoServiceResponse.php');
    $rs = new MethabookRS($xml);

    try {
      $details = new HotelDetailRS($rs);
      return $details;
    } catch (\Exception $e) {
      return null;
    }
  }

  public function execPurchaseDetailList($p) {
    $from           = substr($p['dateFrom'], 0, 4) . '-' . substr($p['dateFrom'], 4, 2) . '-' . substr($p['dateFrom'], 6, 4);
    $to             = substr($p['dateTo'], 0, 4) . '-' . substr($p['dateTo'], 4, 2) . '-' . substr($p['dateTo'], 6, 4);
    $ReadServiceRQ  = new BookDetailRQ($from, $to);
    $xml            = $this->request->execRequest($ReadServiceRQ);

    $rs = new MethabookRS($xml);
    $list_books = array();
    $res = $rs->getResponse()->xpath('//ns1:OTA_ReadServiceResponse');
    $res = $res[0]->OTA_ResRetrieveRS->ReservationsList->HotelReservation;

    foreach ($res as $value) {
      $attrs  = $value->attributes();
      $status = $attrs['ResStatus'];
      if ($status == 'Pag' || $status == 'OK' || $status == 'Con' || $status == 'Con*' || $status == 'Tar') {
        $status = \ServiceStatus::CONFIRMED;
      }
      if ($status == 'Can' || $status == 'CaC') {
        $status = \ServiceStatus::CANCELLED;
      }
      if ($status == 'PRe' || $status == 'PDi' || $status == 'PDI' || $status == 'Ini') {
        $status = \ServiceStatus::PENDING;
      }
      $paxs     = $value->RoomStays->RoomStay->RoomTypes->RoomType->TPA_Extensions->Guests->Guest;
      $paxs_arr = array();
      foreach ($paxs as $pax) {
        $holder     = $pax->attributes();
        $paxs_arr[] = strval($holder['Name']) . ' ' . strval($holder['Surname']);
      }
      $valId        = $value->ResGlobalInfo->HotelReservationIDs->HotelReservationID->attributes();
      $list_books[] = array('localizer' => strval($valId['ResID_Source']), 'holder' => $paxs_arr, 'status_provider' => $status);
    }

    return $list_books;
  }
}
?>
