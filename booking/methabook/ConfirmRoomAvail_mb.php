<?php
namespace Methabook;

class ConfirmRoomAvail extends \RoomAvail {

  public $data;
  public $hotel;
  public $date_in;
  public $date_out;
  public $childAges;
  public $rules;

  public function __construct($data, $hotel, $date_in, $date_out, $childAges, $rules) {
    $this->data = $data;
    $this->hotel = $hotel;
    $this->date_in = $date_in;
    $this->date_out = $date_out;
    $this->childAges = $childAges;
    $this->rules = $rules;
  }

  public function getProvider() {
    return \BookingProvider::Methabook;
  }

  public function getHotel() {
    return $this->hotel;
  }

  public function getRoomCount() {
    return $this->data->roomCount;
  }

  public function getAdultCount() {
    return $this->data->adultCount;
  }

  public function getChildCount() {
    return $this->data->childCount;
  }

  public function getPrice() {
    return $this->data->price;
  }

  public function getSHRUI() {
    return $this->data->SHRUI;
  }

  public function getOnRequest() {
    return '';
  }

  public function getBoard() {
    return $this->data->board;
  }

  public function getBoardCode() {
    return $this->data->boardCode;
  }

  public function getBoardType() {
    return $this->data->boardType;
  }

  public function getRoomType() {
    return $this->data->roomType;
  }

  public function getRoomTypeType() {
    return $this->data->roomTypeType;
  }

  public function getRoomTypeCode() {
    return $this->data->roomTypeCode;
  }

  public function getRoomTypeCharacteristic() {
    return $this->data->roomTypeCharacteristic;
  }

  public function getCancellationPolicies() {
    $policies = array();
    try{
      $extensions = $this->rules->ResGlobalInfo->Total->attributes();
      $extensions = $extensions['AmountAfterTax'];
      foreach ($this->rules->TPA_Extensions->CancellationPolicy->CancellationPolicyRules->Rule as $r) {
        $rule = new BookingRule($r);
        $bookAmount = floatval($extensions);
        $amount = 0;
        if ($rule->FixedPrice != 0) {
          $amount = $rule->FixedPrice;
        }
        else if ($rule->PercentPrice != 0) {
          $amount = ($bookAmount / 100) * $rule->PercentPrice;
        }
        else if ($rule->Nights != 0) {
          if ($rule->ApplicationTypeNights == 'Average') {
            $amount = ($bookAmount / $this->hotel->getNights()) * $rule->Nights;
          }
          else if ($rule->ApplicationTypeNights == 'FirstNight') {
            $amount = $rule->FirstNightPrice * $rule->Nights;
          }
          else if($rule->ApplicationTypeNights == '') {
            $amount = ($bookAmount / $this->hotel->getNights()) * $rule->Nights;
          }
        }
        if ($amount != 0) {
          $dateFrom = new \DateTime('0000-00-00');
          if ($rule->Type == 'S') {
            $dateFrom = new \DateTime($this->hotel->getDateFrom());
          }
          else if ($rule->Type == 'V') {
            if ($rule->DateFrom != '') {
              $dateFrom_arr = explode('T', $rule->DateFrom);
              $dateFrom = new \DateTime($dateFrom_arr[0]);
            }
            else if ($rule->From != '' && $rule->To != '') {
              $dateFrom = new \DateTime($this->hotel->getDateFrom());
              $dateFrom = date_sub($dateFrom, new \DateInterval('P' . $rule->To . 'D'));
            }
            else if ($rule->From != '' && $rule->To == '') {
              $dateFrom = new \DateTime('now');
            }
          }
          else if ($rule->Type == 'R' && $rule->From != '') {
            $dateFrom = new \DateTime('now');
            $dateFrom = date_add($dateFrom, new \DateInterval('P' . $rule->From . 'D'));
          }
          $cancellation = new \BookingCancellationPolicy();
          $cancellation->build($amount, $dateFrom, '00:00', true);
          $policies[] = $cancellation;
        }
      }
    }
    catch(Exception $ex) {
      sendNotificationErrorMail('Error BookinC', $msg, $p['current_user']);$ex->getMessage();
    }
    return $policies;
  }

  public function getGuestList() {
    $guests = array();

    $customer_counter = 0;
    for ($i = 0; $i < $this->getAdultCount(); $i++) {
      $customer_counter++;
      $guests[] = new \BookingCustomer('AD', $customer_counter, '30', '', '');
    }

    for ($i = 0; $i < $this->getChildCount(); $i++) {
      $customer_counter++;
      $guests[] = new \BookingCustomer('CH', $customer_counter, $this->childAges[$i], '', '');
    }

    return $guests;
  }
}

?>
