<?php
$url = $_GET['url'];
$header = (array) json_decode($_GET['header']);
$request = $_GET['request'];

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url); // Set url to post to
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // Allow redirects
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Return into a variable
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
$xml = curl_exec($ch);
curl_close($ch);

echo $xml;
?>