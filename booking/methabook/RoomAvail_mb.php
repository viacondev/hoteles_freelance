<?php
namespace Methabook;

class RoomAvail extends \RoomAvail {

  private $rate;
  private $ratePlanCode;
  public $priceWithFee;
  public $hotelCode;
  public $childAges;
  public $hotel;
  public $currentProvider;
  public function __construct($rate, $board, $ratePlanCode, $hotel, $adultCount, $childCount, $currentProvider, $factorFee = 1, $extraFee = 0, $hotelCode = 0) {
    $this->rate         = $rate;
    $this->board        = $board;
    $this->ratePlanCode = $ratePlanCode;
    $this->adultCount   = $adultCount;
    $this->childCount   = $childCount;
    $this->childAges    = array();
    $this->hotelCode    = $hotelCode;
    $this->hotel        = $hotel;
    $this->currentProvider = $currentProvider;
    $this->hotel        = json_encode($this->buildObjecArray());
    $price = $this->rate->Total->attributes();
    $this->priceWithFee = (floatval($price['AmountAfterTax']) * $this->getRoomCount()) / $factorFee;
    if ($extraFee != 0) {
      $this->priceWithFee += $this->priceWithFee * ($extraFee / 100);
    }
  }

  public function getProvider() {
    return $this->currentProvider;
  }

  public function getHotel() {
    return $this->hotel;
  }

  public function getProviderName() {
    return \BookingProvider::getProviderName($this->currentProvider);
  }

  public function getHotelCode() {
    return strval($this->hotelCode);
  }

  public function getRoomCount() {
    $attrs = $this->rate->attributes();
    return intval($attrs['NumberOfUnits']);
  }

  public function getAdultCount() {
    return $this->adultCount;
  }

  public function getChildCount() {
    return $this->childCount;
  }

  public function getChildAges() {
    return $this->childAges;
  }

  public function getPrice() {
    $attrs = $this->rate->Total->attributes();
    return floatval($attrs['AmountAfterTax']) * $this->getRoomCount();
  }

  public function getPriceWithFee() {
    return round($this->priceWithFee, 2);
  }

  public function getSHRUI() {
    return $this->ratePlanCode;
  }

  public function getOnRequest() {
    return '';
  }

  public function getBoard() {
    return $this->board;
  }

  public function getBoardCode() {
    $boards = array(
      'TI'  => 'Todo incluido',
      'TI'  => 'All Inclusive',
      'AD'  => 'Alojamiento y desayuno',
      'AD'  => 'Breakfast',
      'AD'  => 'Room and Board',
      'AD-1' => 'Desayuno buffet',
      'AD-2' => 'Desayuno continental',
      'SA'   => 'Solo alojamiento',
      'SA-1' => 'Solo habitacion',
      'SA-2' => 'Room Only',
      'MP'   => 'Media pension',
      'MP'   => 'Half board'
      );
    foreach ($boards as $key => $value) {
      $boar =  str_replace('ó', 'o', $this->board);
      $boardd =  str_replace('Ó', 'o', $boar);
      if(preg_match("/$value/i", strval($boardd))) {
        $var = explode('-', $key);
        if($var[0] !='')
          return $var[0];
        else
          return '';
      }
    }
    return '';
  }

  public function getBoardType() {
    return '';
  }

  public function getRoomType() {
    return strval($this->rate->RateDescription->Text);
  }

  public function getRoomTypeType() {
    return '';
  }

  public function getRoomTypeCode() {
    return '';
  }

  public function getRoomTypeCharacteristic() {
    return '';
  }

  public function getCancellationPolicies() {
    return array();
  }

  public function getGuestList() {
    return array();
  }

  public function buildObjecArray() {
    $con = 0;
    $hotel = $this->hotel;
    $methods = get_class_methods($hotel);
    $objArray = array();
    foreach ($methods as $method) {
      if (substr($method, 0, 2) == '__') continue;
      if (substr($method, 0, 3) === 'get' && $method != 'getGroupedRooms' && $method != 'getRoomsAvail') {
        $key = lcfirst(substr($method, 3));
        $objArray[$key] = $hotel->$method();
        $con++;
      }
    }
    return $objArray;
  }
}

?>
