<?php
namespace Methabook;

class HotelDetailRS extends \BookingHotelDetail {

  public $elements;
  private $hotel;
  private $desc;
  private $images;
  private $contact;
  // private $servicesFacilities;
  // private $roomFacilities;
  // private $cateringFacilities;

  public function __construct($rs) {
    $this->elements = $rs->getResponse()->xpath('//ns1:OTA_HotelDescriptiveInfoRS');
    $this->elements = $this->elements[0];
    if (!isset($this->elements->HotelDescriptiveContents)) {
      throw new \Exception("Hotel not found", 1);
    }
    $this->hotel = $this->elements->HotelDescriptiveContents->HotelDescriptiveContent->HotelInfo;
    $this->contact = $this->elements->HotelDescriptiveContents->HotelDescriptiveContent->ContactInfos->ContactInfo;

    $this->build();
  }

  private function build() {
    foreach ($this->hotel->Descriptions->MultimediaDescriptions->MultimediaDescription as $item) {
      if (isset($item->ImageItems)) {
        $this->images = array();
        foreach ($item->ImageItems->ImageItemsTypeImageItem as $img) {
          $attrs = $img->ImageFormat->attributes();
          $this->images[] = $attrs['FileName'];
        }
      }
      else if (isset($item->TextItems)) {
        $this->desc = '';
        foreach ($item->TextItems->TextItemsTypeTextItem as $desc) {
          $attrs = $desc->attributes();
          if ($attrs['Title'] == 'LongDescription' || $attrs['Title'] == 'RoomDescription') {
            $this->desc .= '<p>' . strval($desc->Description) . '</p>';
          }
        }
      }
    }

    // Facilities
    $this->roomFacilities = array();
    $this->servicesFacilities = array();
    $this->cateringFacilities = array();
    foreach ($this->hotel->Services->Service as $service) {
      $attrs = $service->attributes();
      $txt = strval($service->DescriptiveText);
      if ($attrs['Sort'] == 'Hotel') {
        $this->servicesFacilities[] = $txt;
      }
      else if ($attrs['Sort'] == 'Room') {
        $this->roomFacilities[] = $txt;
      }
      else if ($attrs['Sort'] == 'Catering') {
        $this->cateringFacilities[] = $txt;
      }
    }
  }

  public function getCode() {
    $attrs = $this->elements->HotelDescriptiveContents->attributes();
    return strval($attrs['HotelCode']);
  }

  public function getName() {
    $attrs = $this->elements->HotelDescriptiveContents->attributes();
    return strval($attrs['HotelName']);
  }

  public function getDescription() {
    return $this->desc;
  }

  public function getImageList() {
    return $this->images;
  }

  public function getAddressStreetName() {
    return strval($this->contact->Addresses->Address->AddressLine);
  }
  public function getAddressNumber() {
    return '';
  }

  public function getAddressPostalCode() {
    return '';
  }

  public function getEmailList() {
    $mails = array();
    if (isset($this->contact->Emails->Email)) {
      foreach ($this->contact->Emails->Email as $mail) {
        $mails[] = strval($mail);
      }
    }

    return $mails;
  }

  public function getPhoneList() {
    $phones = array();
    if (isset($this->contact->Phones->Phone)) {
      foreach ($this->contact->Phones->Phone as $phone) {
        $attrs = $phone->attributes();
        $phones[] = strval($attrs['ID']);
      }
    }

    return $phones;
  }

  public function getFaxList() {
    return array();
  }

  public function getWebList() {
    return array();
  }

  public function getLocation() {
    return '';
  }

  public function getLatitude() {
    $attrs = $this->hotel->Position->attributes();
    return strval($attrs['Latitude']);
  }

  public function getLongitude() {
    $attrs = $this->hotel->Position->attributes();
    return strval($attrs['Longitude']);
  }

  public function getDestinationCode() {
    // TODO: return dest code
    return '';
  }

  public function getDestinationName() {
    // Commented because direction already inludes de city & country
    // return strval($this->contact->Addresses->Address->CityName);
    return '';
  }

  public function getDestinationZone() {
    return '';
  }

  public function getCategory() {
    $attrs = $this->hotel->CategoryCodes->HotelCategory->attributes();
    if ($attrs['Code'] != 0)
      return strval($attrs['Code'] . 'EST');
    return strval(substr($attrs['CodeDetail'], 0, 1) . 'EST');
  }

  public function getCategoryCode() {
    if (isset($this->hotel->CategoryCodes->HotelCategory)) {
      $attrs = $this->hotel->CategoryCodes->HotelCategory->attributes();
      if ($attrs['Code'] != 0)
        return strval($attrs['Code'] . 'EST');
      return strval(substr($attrs['CodeDetail'], 0, 1) . 'EST');
    }
    return '0';
  }

  public function getBuildingFacilities() {
    // return $this->buildingFacilities;
    return array();
  }

  public function getHotelTypeFacilities() {
    // return $this->hotelTypeFacility;
    return array();
  }

  public function getCredCardsFacilities() {
    // return $this->credCardsFacilities;
    return array();
  }

  public function getRoomFacilities() {
    return $this->roomFacilities;
  }

  public function getServicesFacilities() {
    return $this->servicesFacilities;
  }

  public function getCateringFacilities() {
    return $this->cateringFacilities;
  }

  public function getBusinessFacilities() {
    // return $this->businessFacilities;
    return array();
  }

  public function getHealthBeautyFacilities() {
    // return $this->healthBeautyFacilities;
    return array();
  }

  public function getEntertainmentFacilities() {
    // return $this->entertainmentFacilities;
    return array();
  }

  public function getDistancesFacilities() {
    $arr = array();
    if (isset($this->elements->HotelDescriptiveContents->HotelDescriptiveContent->AreaInfo->RefPoints)) {
      foreach ($this->elements->HotelDescriptiveContents->HotelDescriptiveContent->AreaInfo->RefPoints->RefPoint as $point) {
        $attrs = $point->attributes();
        $name = strval($attrs['Name']);
        $distance = strval($attrs['Distance']);
        // If distance and name are equal then only send the name as key to avoid showing duplicated data.
        $arr[$name] = $distance != $name ? $distance : '';
      }
    }
    return $arr;
  }

  public function getHighLightFacilities() {
    // return $this->highLightFacilities;
    return array();
  }

}

?>
