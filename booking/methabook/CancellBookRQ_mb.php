<?php
namespace Methabook;

class CancellBookRQ extends RQOperation {

  private $localizer;

  public function __construct($localizer) {
    $this->localizer = $localizer;
  }

  public function getWSDL() {
    return 'OTA_Cancel.asmx?wsdl';
  }

  public function getMethodName() {
    return 'OTA_CancelService';
  }

  public function getXML() {
    $xml = '
      <OTA_CancelService xmlns="http://www.opentravel.org/OTA/2003/05">
        <OTA_CancelRQ PrimaryLangID="es">
          __POS__
          <UniqueID ID="' . $this->localizer . '"/>
        </OTA_CancelRQ>
        <TPA_Extensions>
          <MsgUser>1</MsgUser>
        </TPA_Extensions>
      </OTA_CancelService>';

    return $xml;
  }
}
?>