<?php
namespace Methabook;

class HotelReservationRQ extends RQOperation {

  private $params;
  private $item;
  private $serviceIndex;

  public function __construct($params, $item, $serviceIndex) {
    $this->params = $params;
    $this->item = $item;
    $this->serviceIndex = $serviceIndex;
  }

  public function getWSDL() {
    return 'OTA_HotelRes.asmx?wsdl';
  }

  public function getMethodName() {
    return 'OTA_HotelResV2Service';
  }

  public function getXML() {
    $p = $this->params;
    $i = $this->serviceIndex;
    $item = $this->item;

    $price = 0;
    $roomsXml = '';
    $numberOfRooms = count($item['rooms']);
    $currentPax = 1;
    for ($j = 0; $j < $numberOfRooms; $j++) {
      $roomsXml .= '<RoomType><TPA_Extensions><Guests>';

      $room = $item['rooms'][$j];
      for ($r = 0; $r < $room->adultCount; $r++) {
        $name = $p['customerName_' . $i . '_' . $currentPax];
        $surname = $p['customerLastName_' . $i . '_' . $currentPax];
        $roomsXml .= '<Guest Name="' . $name . '" Surname="' . $surname . '"/>';
        $currentPax++;
      }
      for ($r = 0; $r < $room->childCount; $r++) {
        $name = $p['customerName_' . $i . '_' . $currentPax];
        $surname = $p['customerLastName_' . $i . '_' . $currentPax];
        $age     = $p['customerAge_' . $i . '_' . $currentPax];
        // TODO: Set real child age
        //$roomsXml .= '<Guest Age="8" Name="' . $name . '" Surname="' . $surname . '"/>';
        $roomsXml .= '<Guest Age="'. $age .'" Name="' . $name . '" Surname="' . $surname . '"/>';
        $currentPax++;
      }

      $roomsXml .= '</Guests></TPA_Extensions></RoomType>';
      $price += $room->price;
    }

    $holderXml = '<PersonName>';
    $holderXml .= '<GivenName>' . $p['holderName'] . '</GivenName>';
    $holderXml .= '<Surname>' . $p['holderLastName'] . '</Surname>';
    $holderXml .= '</PersonName>';
    $holderXml .= '<Email>' . \Config::findById(1)->mail2 . '</Email>';
    if ($p['holderPhone'] != '') {
      $holderXml .= '<Telephone PhoneNumber="' . $p['holderPhone'] . '" />';
    }
    $holderXml .= '<Address>';
    if ($p['holderAddress'] != '') {
      $holderXml .= '<AddressLine>' . $p['holderAddress'] . '</AddressLine>';
    }
    if ($p['holderPostalCode'] != '') {
      $holderXml .= '<PostalCode>' . $p['holderPostalCode'] . '</PostalCode>';
    }
    if ($p['holderCity'] != '') {
      $holderXml .= '<CityName>' . $p['holderCity'] . '</CityName>';
    }
    if ($p['holderCountry'] != '') {
      $holderXml .= '<CountryName Code="' . $p['holderCountryCode'] . '">' . $p['holderCountry'] . '</CountryName>';
    }
    $holderXml .= '</Address>';

    $firstRoom = $item['rooms'][0];
    // TODO: CurrencyCode
    $xml = '
      <OTA_HotelResV2Service xmlns="http://www.opentravel.org/OTA/2003/05">
        <OTA_HotelResRQ PrimaryLangID="es" SequenceNmbr="' . $item['availToken'] . '">
          __POS__
          <HotelReservations>
            <HotelReservation>
              <RoomStays>
                <RoomStay>
                  <RatePlans>
                    <RatePlan RatePlanCode="' . $firstRoom->SHRUI . '"/>
                  </RatePlans>
                  <RoomTypes>
                    ' . $roomsXml . '
                  </RoomTypes>
                  <TimeSpan
                    Start="' . dateFromYmdToDB($item['dateFrom']) . '"
                    End="' . dateFromYmdToDB($item['dateTo']) . '" />
                  <BasicPropertyInfo HotelCode="' . $item['hotelCode'] . '" />
                  <Total CurrencyCode="USD" AmountAfterTax="' . $price . '"/>
                  <TPA_Extensions>
                    <ExpectedPriceRange min="0" max="' . $price . '" />
                  </TPA_Extensions>
                  <Comments>
                    <Comment>
                      <Text>' . $p["comments_$i"] . '</Text>
                    </Comment>
                  </Comments>
                </RoomStay>
              </RoomStays>
              <ResGuests>
                <ResGuest>
                  <Profiles>
                    <ProfileInfo>
                      <Profile ProfileType="1">
                        <Customer>
                          ' . $holderXml . '
                        </Customer>
                      </Profile>
                    </ProfileInfo>
                  </Profiles>
                </ResGuest>
              </ResGuests>
              <TPA_Extensions>
                <MsgUser>1</MsgUser>
                <VoucherUser>1</VoucherUser>
                <Agent>' . $p['agencyReference'] . '</Agent>
                <AcceptOnly>OK</AcceptOnly>
                <ForceCurrency>USD</ForceCurrency>
              </TPA_Extensions>
            </HotelReservation>
          </HotelReservations>
        </OTA_HotelResRQ>
      </OTA_HotelResV2Service>';

    return $xml;
  }
}
?>