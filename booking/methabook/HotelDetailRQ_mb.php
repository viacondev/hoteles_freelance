<?php
namespace Methabook;

class HotelDetailRQ extends RQOperation {

  private $code;

  public function __construct($code) {
    $this->code = $code;
  }

  public function getWSDL() {
    return 'OTA_HotelDescriptiveInfo.asmx?wsdl';
  }

  public function getMethodName() {
    return 'OTA_HotelDescriptiveInfoService';
  }

  public function getXML() {
    $xml = '
      <OTA_HotelDescriptiveInfoService xmlns="http://www.opentravel.org/OTA/2003/05">
        <OTA_HotelDescriptiveInfoRQ PrimaryLangID="en">
          __POS__
          <HotelDescriptiveInfos>
          <HotelDescriptiveInfo HotelCode="' . $this->code . '"/>
          </HotelDescriptiveInfos>
        </OTA_HotelDescriptiveInfoRQ>
      </OTA_HotelDescriptiveInfoService>';

    return $xml;
  }
}
?>