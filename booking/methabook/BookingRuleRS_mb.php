<?php
namespace Methabook;

class BookingRuleRS {

  private $elements;
  private $hotel;
  public $factorFee;

  public function __construct($rs, $hotel, $factorFee) {
    $this->elements = $rs->getResponse()->xpath('//ns1:OTA_HotelBookingRuleRS');
    $this->elements = $this->elements[0];
    $this->hotel = $hotel;
    $this->factorFee = $factorFee;
  }

  public function getPenaltyDescription() {
    try {
      return strval($this->elements->RuleMessage->BookingRules->BookingRule->CancelPenalties->CancelPenalty->PenaltyDescription->Text);
    }
    catch (Exception $e) { }

    return '';
  }

  public function getCancellationPolicies() {
    $policies = array();

    try {
      $extensions = $this->elements->RuleMessage->BookingRules->BookingRule->TPA_Extensions;
      if (isset($extensions->CancellationPolicyRules)) {
        foreach ($extensions->CancellationPolicyRules->Rule as $r) {
          $rule = new BookingRule($r);
          $bookAmount = floatval($extensions->TotalPrice);
          $amount = 0;
          if ($rule->FixedPrice != 0) {
            $amount = $rule->FixedPrice;
          }
          else if ($rule->PercentPrice != 0) {
            $amount = ($bookAmount / 100) * $rule->PercentPrice;
          }
          else if ($rule->Nights != 0) {
            if ($rule->ApplicationTypeNights == 'Average') {
              $amount = ($bookAmount / $this->hotel->calculateNights()) * $rule->Nights;
            }
            else if ($rule->ApplicationTypeNights == 'FirstNight') {
              $amount = $rule->FirstNightPrice * $rule->Nights;
            }
            else if($rule->ApplicationTypeNights == '') {
              $amount = ($bookAmount / $this->hotel->calculateNights()) * $rule->Nights;
            }
          }

          if ($amount != 0) {
            $dateFrom = new \DateTime('0000-00-00');
            if ($rule->Type == 'S') {
              $dateFrom = new \DateTime(dateFromYmdToDB($this->hotel->getDateFrom()));
            }
            else if ($rule->Type == 'V') {
              if ($rule->DateFrom != '') {
                $dateFrom_arr = explode('T', $rule->DateFrom);
                $dateFrom = new \DateTime($dateFrom_arr[0]);
              }
              else if ($rule->From != '' && $rule->To != '') {
                $dateFrom = new \DateTime(dateFromYmdToDB($this->hotel->getDateFrom()));
                $dateFrom = date_sub($dateFrom, new \DateInterval('P' . $rule->To . 'D'));
              }
              else if ($rule->From != '' && $rule->To == '') {
                $dateFrom = new \DateTime('now');
              }
            }
            else if ($rule->Type == 'R' && $rule->From != '') {
              $dateFrom = new \DateTime('now');
              $dateFrom = date_add($dateFrom, new \DateInterval('P' . $rule->From . 'D'));
            }

            $cancellation = new \BookingCancellationPolicy();
            $cancellation->build($amount, $dateFrom, '00:00', true, $this->factorFee);
            $policies[] = $cancellation;
          }
        }
        if (count($policies) == 0) {
          \Log::warning('BookingRulesParsing', 'No policies:' . json_encode($this->elements->RuleMessage->BookingRules->BookingRule->TPA_Extensions));
        }
      }
      else{
        $cancell_string  = $this->elements->RuleMessage->BookingRules->BookingRule->CancelPenalty->PenaltyDescription->Text;
        $attrs           = $this->element->TimeSpan->attributes();
        $date_checkin    = strval($attrs_['Start']);

        $patrones = explode("*", $cad_date);
        $monto    = 0;
        $fecha    = '';
        $no_show  = false;
        foreach ($patrones as $value) {
          $posicion    = strripos($value, ':');
          $monto_text  = substr($value, $posicion+1);

          //verifica la fecha
          if(preg_match("/[0-9]+[.]?[,]?[0-9]*/", $value)) {
            preg_match_all("/[0-9]{2}[\/][0-9]{2}[\/][0-9]{4}/", $value, $fecha_text);
            $fecha = $fecha_text[0][0];
          }
          // caso de encontrar la cadena noche
          if(preg_match("/noche/i", $monto_text) && !preg_match("/no show/i", $value)) {
            continue;
          }
         //en caso de encontrar el texto (no show) se introduce la fecha de entrada
          if(preg_match("/no show/i", $value)) {
            $no_show = true;
            break;
          }
          //verific todos los montos encontrados en la cadeda
          if(preg_match("/[0-9]+[.]?[,]?[0-9]*/", $monto_text)) {
            $monto_cadena = '';
            preg_match_all("/[0-9]+[.]?[,]?[0-9]*/", $monto_text, $monto_cadena);
            $monto = floatval($monto_cadena[0][0]);
            if($monto > 0) {
             break;
            }
          }
        }
        if($no_show || $fecha == '') {
          $dateFrom = new \DateTime($date_checkin);
        }
        if($fecha != '') {
          $dateFrom = new \DateTime($fecha);
        }
        $cancellation = new BookingCancellationPolicy();
        $cancellation->build($monto, $dateFrom, '00:00', true, $this->factorFee);
        $policies[] = $cancellation;
        \Log::info('BookingRulesTextParsing', $cancell_string . "=>" . $dateFom . " " . $monto);
      }
    }
    catch (Exception $e) {
      \Log::__log('INFO', 'CancelPenalty', $this->elements->RuleMessage->BookingRules->BookingRule->CancelPenalty->PenaltyDescription->Text);
    }

    return $policies;
  }

  public function getBookingRulesDesc() {
    $desc = '';
    try {
      if (isset($this->elements->RuleMessage->BookingRules->BookingRule->Description->Text)) {
        $desc = strval($this->elements->RuleMessage->BookingRules->BookingRule->Description->Text);
      }
    }
    catch (Exception $e) { }

    return $desc;
  }
}

class BookingRule {
  public $From;
  public $To;
  public $DateFrom;
  public $DateTo;
  public $Type;
  public $FixedPrice;
  public $PercentPrice;
  public $Nights;
  public $ApplicationTypeNights;
  public $FirstNightPrice;

  public function __construct($rule) {
    $attrs = $rule->attributes();

    $this->From = $attrs['From'];
    $this->To = $attrs['To'];
    $this->DateFrom = strval($attrs['DateFrom']);
    $this->DateTo = strval($attrs['DateTo']);
    $this->Type = strval($attrs['Type']);
    $this->FixedPrice = floatval($attrs['FixedPrice']);
    $this->PercentPrice = floatval($attrs['PercentPrice']);
    $this->Nights = intval($attrs['Nights']);
    $this->ApplicationTypeNights = strval($attrs['ApplicationTypeNights']);
    $this->FirstNightPrice = floatval($attrs['FirstNightPrice']);
  }
}

?>
