<?php
namespace Methabook;


class ConfirmHotelAvail extends \HotelAvail {

  public $data;
  public $rules;
  public $roomsAvail;
  public $item;

  public function __construct($data, $item) {

    $this->item   = $item;
    $this->data   = $data->HotelReservations->HotelReservation;
    $this->rules  = $data->HotelReservations->HotelReservation;
     // Build Booking Rules
    $from = $this->getDateFrom();
    $to = $this->getDateTo();
    $from = substr($from, 0, 4) . '-' . substr($from, 4, 2) . '-' . substr($from, 6, 4);
    $to = substr($to, 0, 4) . '-' . substr($to, 4, 2) . '-' . substr($to, 6, 4);

    $child_position = 0;
    $this->roomsAvail = array();
    foreach ($item->rooms as $room) {
      $arr_child = array();
      for ($i=0; $i < $room->childCount; $i++) {
        $arr_child[] = $this->data->childAges[$child_position];
        $child_position++;
      }
      $this->roomsAvail[] = new ConfirmRoomAvail($room, $this, $this->getDateFrom(), $this->getDateTo(), $arr_child, $this->rules);
    }
  }

  public function getProvider() {
    return $this->item->provider;
  }

  public function getAvailToken() {
    return '';
  }

  public function getCode() {
    $code = $this->data->RoomStays->RoomStay->BasicPropertyInfo->attributes();
    return strval($code['HotelCode']);
  }

  public function getName() {
    $value = $this->data->RoomStays->RoomStay->BasicPropertyInfo->attributes();
    return strval($value['HotelName']);
  }

  public function getDateFrom() {
    $from = $this->data->RoomStays->RoomStay->TimeSpan->attributes();
    return strval($from['Start']);
  }

  public function getDateTo() {
    $to = $this->data->RoomStays->RoomStay->TimeSpan->attributes();
    return strval($to['End']);
  }

  public function getDestinationCode() {
    return $this->item->destCode;
  }

  public function getDestinationType() {
    return '';
  }

  public function getMainImage() {
    return '';
  }

  public function getDescription() {
    return '';
  }

  public function getLatitude() {
    return '';
  }

  public function getLongitude() {
    return '';
  }

  public function getZone() {
    return '';
  }

  public function getLocation() {
    return '';
  }

  public function getCategory() {
    return '';
  }

  public function getCategoryType() {
    return '';
  }

  public function getCategoryCode() {
    return strval($this->data->category);
  }

  public function getRoomsAvail() {
    return $this->roomsAvail;
  }

  public function getGroupedRooms() {
    return $this->roomsAvail;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getContractName() {
    return '';
  }

  public function getContractIncomingOfficeCode() {
    return '';
  }

  public function getContractComment() {
    // return $this->rules->getBookingRulesDesc();
    //revisar los comentario
    return '';
  }

  public function getClientComment() {
    return '';
  }

  public function getSupplier() {
    return '';
  }

  public function getSupplierVatNumber() {
    return '';
  }

  public function getDiscountList() {
    return array();
  }

  public function getAllPaxRequired() {
    return true;
  }

  public function getXtraDataRequired() {
    return true;
  }
}

?>
