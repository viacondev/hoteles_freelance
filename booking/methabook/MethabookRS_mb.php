<?php
namespace Methabook;

class MethabookRS {

  private $response;

  public function __construct($xml, $solicitud = "", $type = "") {
    $xml = str_replace(array('<soap:','<s:'), "<", $xml);
    $xml = str_replace(array('</soap:','</s:'), "</", $xml);
    $this->response = simplexml_load_string($xml);
    $this->response->registerXPathNamespace('ns1', 'http://www.opentravel.org/OTA/2003/05');
    if($solicitud == 'XML_RS') {
      $descrip = $type;
      \Log::info($xml, $descrip);
    }
    //\Log::debug($xml, 'XML RS');
    // __logtxt($xml);
  }

  public function getResponse() {
    return $this->response;
  }
}

?>