<?php
namespace Methabook;

class ZoneListRQ extends RQOperation {

  public function __construct() {
  }

  public function getWSDL() {
    return 'JP_ZoneList.asmx?wsdl';
  }

  public function getMethodName() {
    return 'JP_ZoneListService';
  }

  public function getXML() {
    $xml = '
      <JP_ZoneListService xmlns="http://www.juniper.es/webservice/2007/">
        <JP_ZoneListRQ PrimaryLangID="en">
          __POS__
          <TPA_Extensions>
            <ShowHotels>1</ShowHotels>
            <OnlyZonesWithOwnHotels>1</OnlyZonesWithOwnHotels>
          </TPA_Extensions>
        </JP_ZoneListRQ>
      </JP_ZoneListService>';

    return $xml;
  }
}
?>