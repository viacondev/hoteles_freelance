<?php
namespace Methabook;

class PurchasedRoomRS extends \RoomAvail {

  private $elements;
  private $guests;

  public function __construct($elements, $hotel, $board) {
    $this->elements = $elements;
    $this->board = $board;

    $this->adultCount = 0;
    $this->childCount = 0;
    $this->guests = array();
    $i = 0;
    foreach ($elements->TPA_Extensions->Guests->Guest as $guest) {
      $i++;
      $attrs = $guest->attributes();
      $type = $attrs['Age'] > 18 ? 'AD' : 'CH';
      $this->guests[] = new \BookingCustomer($type, $i, $attrs['Age'], $attrs['Name'], $attrs['Surname']);
      if ($type == 'AD') {
        $this->adultCount++;
      }
      else if ($type == 'CH') {
        $this->childCount++;
      }
    }
  }

  public function getProvider() {
    return \BookingProvider::DOMITUR2;
  }

  public function getRoomCount() {
    return 1;
  }

  public function getAdultCount() {
    return $this->adultCount;
  }

  public function getChildCount() {
    return $this->childCount;
  }

  public function getPrice() {
    // TODO: return price
    return 0;
  }

  public function getSHRUI() {
    return '';
  }

  public function getOnRequest() {
    return '';
  }

  public function getBoard() {
    return strval($this->board);
  }

  public function getBoardCode() {
    return '';
  }

  public function getBoardType() {
    return '';
  }

  public function getRoomType() {
    return strval($this->elements->RoomDescription->Text);
  }

  public function getRoomTypeType() {
    return '';
  }

  public function getRoomTypeCode() {
    return '';
  }

  public function getRoomTypeCharacteristic() {
    return '';
  }

  public function getCancellationPolicies() {
    return array();
  }

  public function getGuestList() {
    return $this->guests;
  }
}

?>
