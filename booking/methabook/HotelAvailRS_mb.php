<?php
namespace Methabook;

include_once('RoomAvail_mb.php');

class HotelAvailRS extends \HotelAvail {

  private $element;
  private $destCode;
  private $rooms;
  private $sequenceNmbr;
  private $currentProvider;
  public $hotelZionCode;

  public function __construct($element, $destCode, $rooms, $sequenceNmbr, $board_descrcip = '', $currentProvider = 0, $factorFee = 1, $extraFee = 0) {
    $this->element          = $element;
    $this->destCode         = $destCode;
    $this->rooms            = $rooms;
    $this->sequenceNmbr     = $sequenceNmbr;
    $this->currentProvider  = $currentProvider;
    $this->roomsAvail       = array();
    $attrs =$this->element->BasicPropertyInfo->attributes();
    $hotelCode = strval($attrs['HotelCode']);
    foreach ($this->element->RoomRates->RoomRate as $group) {
      $groupedRooms = array();
      $attrs        = $group->attributes();
      $board        = strval($attrs['RatePlanCategory']);
      $ratePlanCode = strval($attrs['RatePlanCode']);
      foreach ($group->Rates->Rate as $rate) {
        $attrs = $rate->attributes();
        $roomsSource  = explode(',', $attrs['RateSource']);
        $adultCount   = 0;
        $childCount   = 0;
        foreach ($roomsSource as $source) {
          $roomIndex = intval($source) - 1;

          $adultCount += $this->rooms[$roomIndex]['adultCount'];
          $childCount += $this->rooms[$roomIndex]['childCount'];
        }
        $room = new RoomAvail($rate, $board, $ratePlanCode, $this, $adultCount, $childCount, $currentProvider, $factorFee, $extraFee, $hotelCode);
        if($board_descrcip != '') {
          if(in_array($room->getBoardCode(), $board_descrcip)) {
            $groupedRooms[] = $room;
            $this->roomsAvail[] = $room;
          }
        }
        else {
          $groupedRooms[] = $room;
          $this->roomsAvail[] = $room;
        }
      }
      if(count($groupedRooms) > 0)
        $this->insertGroupedRoom($groupedRooms);
    }
  }

  public function getProvider() {
    return $this->currentProvider;
  }

  public function getProviderName() {
    return \BookingProvider::getProviderName($this->currentProvider);
  }

  public function getAvailToken() {
    return $this->sequenceNmbr;
  }

  public function getCode() {
    $attrs =$this->element->BasicPropertyInfo->attributes();
    return strval($attrs['HotelCode']);
  }

  public function getHotelZionCode() {
    $attrs                = $this->element->BasicPropertyInfo->attributes();
    $this->hotelZionCode  = strval($attrs['HotelCode']);
    return $this->hotelZionCode;
  }

  public function getName() {
    $attrs  = $this->element->BasicPropertyInfo->attributes();
    $name   = strval(htmlspecialchars($attrs['HotelName'], ENT_QUOTES, 'UTF-8'));
    return ucwords(strtolower($name));
  }

  public function getDateFrom() {
    $attrs =$this->element->TimeSpan->attributes();
    $date = strval($attrs['Start']);
    return str_replace('-', '', $date);
  }

  public function getDateTo() {
    $attrs =$this->element->TimeSpan->attributes();
    $date = strval($attrs['End']);
    return str_replace('-', '', $date);
  }

  public function getDestinationCode() {
    return $this->destCode;
  }

  public function getDestinationType() {
    return '';
  }

  public function getMainImage() {
    if (isset($this->element->TPA_Extensions->HotelInfo->Thumb)) {
      $img = strval($this->element->TPA_Extensions->HotelInfo->Thumb);

      if ($img == 'http://www.touricoholidays.com/en/Modules/customizable/images/ESES/photo-not-available-small.gif') {
        $img = 'assets/images/no-photo.jpg';
      }

      // Look for a better sized image
      if (strpos($img, 'hotelbeds.com') !== false) {
        $img = str_replace('/small', '', $img);
      }
      else if (strpos($img, 'urlforimages.com') !== false) {
        $img = str_replace('100x100', '200x200', $img);
      }

      return $img;
    }

    return 'assets/images/no-photo.jpg';
  }

  public function getDescription() {
    return strval($this->element->TPA_Extensions->HotelInfo->Description);
  }

  public function getLatitude() {
    return strval($this->element->TPA_Extensions->HotelInfo->Latitude);
  }

  public function getLongitude() {
    return strval($this->element->TPA_Extensions->HotelInfo->Longitude);
  }

  public function getZone() {
    return strval($this->element->TPA_Extensions->HotelInfo->Zone);
  }

  public function getLocation() {
    return $this->getZone();
  }

  public function getCategory() {
    $attrs = $this->element->TPA_Extensions->HotelInfo->Category->attributes();
    $cat = $attrs['Code'];
    return $cat != 6 ? $cat . 'EST' : '5LUX';
  }

  public function getCategoryType() {
    return '';
  }

  public function getCategoryCode() {
    $attrs = $this->element->TPA_Extensions->HotelInfo->Category->attributes();
    $cat = $attrs['Code'];
    return $cat != 6 ? $cat . 'EST' : '5LUX';
  }

  public function getRoomsAvail() {
    return $this->roomsAvail;
  }

  public function getCurrency() {
    $attrs = $this->element->Total->attributes();
    return strval($attrs['CurrencyCode']);
  }

  public function getContractName() {
    return '';
  }

  public function getContractIncomingOfficeCode() {
    return '';
  }

  public function getContractComment() {
    return '';
  }

  public function getClientComment() {
    return '';
  }

  public function getSupplier() {
    return '';
  }

  public function getSupplierVatNumber() {
    return '';
  }

  public function getDiscountList() {
    return array();
  }

  public function getAllPaxRequired() {
    return true;
  }

  public function getXtraDataRequired() {
    return true;
  }

  public function getIsWithPromotion() {
    return false;
  }
}

?>
