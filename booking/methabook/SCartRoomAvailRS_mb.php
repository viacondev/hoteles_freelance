<?php
namespace Methabook;

class SCartRoomAvailRS extends \RoomAvail {

  private $data;
  private $date_in;
  private $date_out;
  public $childAges;
  private $rules;
  private $factorFee;

  public function __construct($data, $hotel, $date_in, $date_out, $childAges, $rules, $factorFee = 0.79) {
    $this->data = $data;
    $this->date_in = $date_in;
    $this->date_out = $date_out;
    $this->childAges = $childAges;
    $this->rules = $rules;
    $this->factorFee = $factorFee;
  }

  public function getProvider() {
    return \BookingProvider::METHABOOK;
  }

  public function getHotel() {
    return $this->hotel;
  }

  public function getProviderName() {
    return \BookingProvider::getProviderName($this->currentProvider);
  }

  public function getHotelCode() {
    return '';
  }

  public function getRoomCount() {
    return $this->data->roomCount;
  }

  public function getAdultCount() {
    return $this->data->adultCount;
  }

  public function getChildCount() {
    return $this->data->childCount;
  }

  public function getPrice() {
    return $this->data->price;
  }

  public function getPriceWithFee() {
    return round($this->data->price / $this->factorFee, 2);
  }

  public function getSHRUI() {
    return $this->data->SHRUI;
  }

  public function getOnRequest() {
    return '';
  }

  public function getBoard() {
    return $this->data->board;
  }

  public function getBoardCode() {
    return $this->data->boardCode;
  }

  public function getBoardType() {
    return $this->data->boardType;
  }

  public function getRoomType() {
    return $this->data->roomType;
  }

  public function getRoomTypeType() {
    return $this->data->roomTypeType;
  }

  public function getRoomTypeCode() {
    return $this->data->roomTypeCode;
  }

  public function getRoomTypeCharacteristic() {
    return $this->data->roomTypeCharacteristic;
  }

  public function getCancellationPolicies() {
    $policies = array();

    $policies = $this->rules->getCancellationPolicies();
    // If no policies then read the a posible cancellation policy description
    if (count($policies) == 0 && $this->rules->getPenaltyDescription() != '') {
      // $dateFrom = new \DateTime(dateFromYmdToDB($this->hotel->getDateFrom()));
      $dateFrom = new \DateTime(dateFromYmdToDB(date("Ymd")));
      $policyDesc = $this->rules->getPenaltyDescription();
      $cancellation = new \BookingCancellationPolicy();
      $cancellation->build(0, $dateFrom, '00:00', true, $policyDesc);
      $policies[] = $cancellation;
    }

    return $policies;
  }

  public function getGuestList() {
    $guests = array();

    $customer_counter = 0;
    for ($i = 0; $i < $this->getAdultCount(); $i++) {
      $customer_counter++;
      $guests[] = new \BookingCustomer('AD', $customer_counter, '30', '', '');
    }

    for ($i = 0; $i < $this->getChildCount(); $i++) {
      $customer_counter++;
      $guests[] = new \BookingCustomer('CH', $customer_counter, $this->childAges[$i], '', '');
    }

    return $guests;
  }

  public function buildObjecArray() {
    return array();
  }
}

?>
