<?php
namespace Methabook;

class HotelBookingRuleRQ extends RQOperation {

  private $params;

  public function __construct($params = array()) {
    $this->params = $params;
  }

  public function getWSDL() {
    return 'OTA_HotelBookingRule.asmx?wsdl';
  }

  public function getMethodName() {
    return 'OTA_HotelBookingRuleService';
  }

  public function getXML() {
    $p = $this->params;

    $xml = '
      <OTA_HotelBookingRuleService xmlns="http://www.opentravel.org/OTA/2003/05">
        <OTA_HotelBookingRuleRQ PrimaryLangID="es" SequenceNmbr="' . $p['availToken'] . '">
          __POS__
          <RuleMessage HotelCode="' . $p['hotelCode'] . '">
            <StatusApplication RatePlanCode="' . $p['SHRUI'] . '" 
              Start="' . $p['checkin'] . '" 
              End="' . $p['checkout'] . '" />
            <TPA_Extensions>
              <ShowBasicInfo>1</ShowBasicInfo>
              <ShowCatalogueData>1</ShowCatalogueData>
              <ForceCurrency>USD</ForceCurrency>
            </TPA_Extensions>
          </RuleMessage>
        </OTA_HotelBookingRuleRQ>
      </OTA_HotelBookingRuleService>';

    return $xml;
  }
}
?>