<?php
namespace Methabook;

include_once('RoomStayCandidateRQ_mb.php');

class HotelAvailRQ extends RQOperation {

  private $params;

  public function __construct($params = array()) {
    $this->params = $params;
  }

  public function getWSDL() {
    return 'OTA_HotelAvail.asmx?wsdl';
  }

  public function getMethodName() {
    return 'OTA_HotelAvailService';
  }

  public function getXML() {
    $p = $this->params;

    $roomsStr = '';
    foreach ($p['rooms'] as $room) {
      $roomsStr .= $room->getXML();
    }

    $hotelRef = empty($p['hotelCode']) ? 'HotelCityCode="' . $p['destCode'] . '"' : 'HotelCode="' . $p['hotelCode'] . '"';
    $xml = '
      <OTA_HotelAvailService xmlns="http://www.opentravel.org/OTA/2003/05">
        <OTA_HotelAvailRQ PrimaryLangID="es">
          __POS__
          <AvailRequestSegments>
            <AvailRequestSegment>
              <StayDateRange Start="' . $p['checkin'] . '" End="' . $p['checkout'] . '"/>
              <RoomStayCandidates>
                ' . $roomsStr . '
              </RoomStayCandidates>
              <HotelSearchCriteria>
                <Criterion>
                  <HotelRef ' . $hotelRef . '/>
                  <TPA_Extensions>
                    <ShowBasicInfo>1</ShowBasicInfo>
                    <ShowCatalogueData>1</ShowCatalogueData>
                    <ShowOnlyAvailable>1</ShowOnlyAvailable>
                    <ForceCurrency>USD</ForceCurrency>
                  </TPA_Extensions>
                </Criterion>
              </HotelSearchCriteria>
            </AvailRequestSegment>
          </AvailRequestSegments>
        </OTA_HotelAvailRQ>
      </OTA_HotelAvailService>';

    return $xml;
  }
}
?>