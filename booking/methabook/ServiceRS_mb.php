<?php
namespace Methabook;

/**
 * Class used for confirmed Services
 */
class ServiceRS extends \BookingService {

  private $elements;
  private $item;
  private $errors;
  public function __construct($rs, $scItem, $errors = array()) {
    // $this->elements = $rs->getResponse()->xpath('//ns1:OTA_HotelResRS');
    // $this->elements = $this->elements[0];
    $this->elements = $rs;
    $this->errors = $errors;
    $this->item = json_decode(json_encode($scItem), FALSE);
  }

  public function getLocalizer() {
    $attrs = $this->elements->HotelReservations->HotelReservation->ResGlobalInfo->HotelReservationIDs->HotelReservationID->attributes();
    return strval($attrs['ResID_Source']);
  }

  public function getSPUI() {
    $attrs = $this->elements->HotelReservations->HotelReservation->UniqueID->attributes();
    return strval($attrs['ID']);
  }

  public function getServiceType() {
    return 'ServiceHotel';
  }

  public function getServiceStatus() {
    return '';
  }

  public function getDirectPayment() {
    return '';
  }

  public function getDateFrom() {
    return $this->item->dateFrom;
  }

  public function getDateTo() {
    return $this->item->dateTo;
  }

  public function getCurrency() {
    $attrs = $this->elements->HotelReservations->HotelReservation->RoomStays->RoomStay->Total->attributes();
    return strval($attrs['CurrencyCode']);
  }

  public function getTotalAmount() {
    $attrs = $this->elements->HotelReservations->HotelReservation->RoomStays->RoomStay->Total->attributes();
    return floatval($attrs['AmountAfterTax']);
  }

  public function getAdditionalCosts() {
    return array();
  }

  public function getServiceInfo() {
    return new ConfirmHotelAvail($this->elements, $this->item);
  }

  public function getSupplementList() {
    return array();
  }

  public function getStatus() {
    $attrs = $this->elements->HotelReservations->HotelReservation->attributes();
    $status = $attrs['ResStatus'];

    if ($status == 'Pag' || $status == 'OK' || $status == 'Con' || $status == 'Con*' || $status == 'Tar') {
      return ServiceStatus::CONFIRMED;
    }
    if ($status == 'Can' || $status == 'CaC') {
      return ServiceStatus::CANCELLED;
    }
    if ($status == 'PRe' || $status == 'PDi' || $status == 'PDI' || $status == 'Ini') {
      return ServiceStatus::PENDING;
    }
  }

  public function getErrors() {
    return $this->errors;
  }

}

?>
