<?php
namespace Methabook;

abstract class RQOperation {

  abstract public function getXML();

  public function getWSDL() {
    return '';
  }

  public function getMethodName() {
    return '';
  }
  
}
?>