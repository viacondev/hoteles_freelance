<?php
namespace Methabook;

class CancellBookRS {

  private $elements;

  public function __construct($rs) {
    $this->elements = $rs->getResponse()->xpath('//ns1:OTA_CancelRS');
    $this->elements = $this->elements[0];
  }

  public function getCancellationFee() {
    $price = strval($this->elements->Warnings->WarningType);
    $price_arr = explode(' ', $price);
    if (count($price_arr) == 2) {
      return $price_arr[0];
    }

    // TODO: If cancellation couldn't be calculed then show it to the user.
    // return '77000000';
  }
}
?>
