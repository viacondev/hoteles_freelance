<?php
namespace Methabook;

class MethabookRQ {

  private $location;

  public function __construct() {
    ini_set('soap.wsdl_cache_enabled', '0');
    ini_set('soap.wsdl_cache_ttl', '0');

    $this->location = \MBConfig::$url;
  }

  public function getRequestXML($RQElement) {
    $POS = '
      <POS>
        <Source AgentDutyCode="' . \MBConfig::$agencyName . '">
          <RequestorID Type="1" MessagePassword="' . \MBConfig::$agencyCode . '"/>
        </Source>
      </POS>';

    $xml = '<?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema">
              <soap:Body>
                ' . $RQElement->getXML() . '
              </soap:Body>
            </soap:Envelope>';

    $xml = str_replace('__POS__', $POS, $xml);

    return $xml;
  }

  public function execRequest($RQElement, $solicitud = "", $type = "") {
    $url = \MBConfig::$url . $RQElement->getWSDL();
    $header = array();
    $header[] = 'Content-Type: text/xml;charset=UTF-8';
    $header[] = 'SOAPAction: "http://www.opentravel.org/OTA/2003/05/' . $RQElement->getMethodName() . '"';
    // $header[] = 'SOAPAction: "http://www.juniper.es/webservice/2007/' . $RQElement->getMethodName() . '"';
    $header[] = 'Accept-Encoding: gzip,deflate';

    if (isset(\MBConfig::$DirectXMLAccess) && \MBConfig::$DirectXMLAccess) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url); // Set url to post to
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // Allow redirects
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Return into a variable
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getRequestXML($RQElement));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
      $xml = curl_exec($ch);
      curl_close($ch);
    }
    else {
      // Call to a remote request wrapper because our current dev IP doesn't have access to MB.
      // This should be changed due security issues.
      $rq = 'http://methabook.barrybolivia.com/booking/methabook/RemoteRequestWrapper_mb.php';
      $rq .= '?url=' . urlencode($url);
      $rq .= '&header=' . urlencode(json_encode($header));
      $rq .= '&request=' . urlencode($this->getRequestXML($RQElement));

      // __logtxt($this->getRequestXML($RQElement));
      $context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));
      $xml = file_get_contents($rq, false, $context);

    }
    if($solicitud == 'XML_RQ') {
      $descrip = $type;
      \Log::info($this->getRequestXML($RQElement), $descrip);
    }
    $xml = gzinflate(substr($xml, 10, -8));
    // __logtxt($this->getRequestXML($RQElement)); // RQ
    // __logtxt($xml); // RS

    return $xml;
  }


  public function getRequestXML__soapCall($RQElement) {
    $POS = '
      <POS>
        <Source AgentDutyCode="' . \MBConfig::$agencyName . '">
          <RequestorID Type="1" MessagePassword="' . \MBConfig::$agencyCode . '"/>
        </Source>
      </POS>';

    $xml = str_replace('__POS__', $POS, $RQElement->getXML());

    return $xml;
  }
  /**
   * Even when this way of requesting works, it fails while reading the data under CDATA tag.
   * So, it is not used but left here just in case.
   * To make it work, $this->getRequestXML() should not include the soap:Envelope and soap:Body tags.
   */
  public function execRequestUsing__soapCall($RQElement) {
    ini_set('soap.wsdl_cache_enabled', '0');
    ini_set('soap.wsdl_cache_ttl', '0');

    $client = new \SoapClient(\MBConfig::$url . $RQElement->getWSDL(), array('compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP, 'trace' => 1));
    $params = new \SoapVar($this->getRequestXML__soapCall($RQElement), XSD_ANYXML);

    $rs = $client->__soapCall($RQElement->getMethodName(), array($params));

    return $rs;
  }

  public function allOptions($RQElement)
  {
    $url = \MBConfig::$url . $RQElement->getWSDL();
    $header = array();
    $header[] = 'Content-Type: text/xml;charset=UTF-8';
    $header[] = 'SOAPAction: "http://www.opentravel.org/OTA/2003/05/' . $RQElement->getMethodName() . '"';
    // $header[] = 'SOAPAction: "http://www.juniper.es/webservice/2007/' . $RQElement->getMethodName() . '"';
    $header[] = 'Accept-Encoding: gzip,deflate';

    if (isset(\MBConfig::$DirectXMLAccess) && \MBConfig::$DirectXMLAccess) {
      $options  = array(CURLOPT_URL => $url, 
                        CURLOPT_FOLLOWLOCATION => 1, 
                        CURLOPT_RETURNTRANSFER => 1, 
                        CURLOPT_SSL_VERIFYHOST => 0, 
                        CURLOPT_SSL_VERIFYPEER => 0, 
                        CURLOPT_HTTPHEADER => $header, 
                        CURLOPT_POST => 1, 
                        CURLOPT_POSTFIELDS => $this->getRequestXML($RQElement), 
                        CURLOPT_RETURNTRANSFER => 1, 
                        CURLOPT_USERAGENT => $_SERVER['HTTP_USER_AGENT']);
    }
    else {
      $rq = 'http://methabook.barrybolivia.com/booking/methabook/RemoteRequestWrapper_mb.php';
      $rq .= '?url=' . urlencode($url);
      $rq .= '&header=' . urlencode(json_encode($header));
      $rq .= '&request=' . urlencode($this->getRequestXML($RQElement));
      $options  = array(CURLOPT_AUTOREFERER => true, 
                        CURLOPT_HEADER => 0, 
                        CURLOPT_RETURNTRANSFER => 1, 
                        CURLOPT_URL             => $rq);
    }
    return $options;
  }
}
?>
