<?php
namespace Methabook;

include_once('PurchasedRoomRS_mb.php');

class PurchasedHotelRS extends \HotelAvail {

  private $element;
  private $rooms;

  public function __construct($element) {
    $this->element = $element;

    $this->rooms = array();
    foreach ($this->element->RoomStays->RoomStay->RoomTypes->RoomType as $room) {
      $attrs = $this->element->RoomStays->RoomStay->RatePlans->RatePlan->attributes();
      $this->rooms[] = new PurchasedRoomRS($room, $this, $attrs['RatePlanName']);
    }
  }

  public function getProvider() {
    return \BookingProvider::DOMITUR2;
  }

  public function getProviderName() {
    return 'METHABOOK';
  }

  public function getAvailToken() {
    return '';
  }

  public function getCode() {
    $attrs = $this->element->RoomStays->RoomStay->BasicPropertyInfo->attributes();
    return strval($attrs['HotelCode']);
  }

  public function getName() {
    $attrs = $this->element->RoomStays->RoomStay->BasicPropertyInfo->attributes();
    return strval($attrs['HotelName']);
  }

  public function getDateFrom() {
    $attrs = $this->element->RoomStays->RoomStay->TimeSpan->attributes();
    $date = strval($attrs['Start']);
    return str_replace('-', '', $date);
  }

  public function getDateTo() {
    $attrs = $this->element->RoomStays->RoomStay->TimeSpan->attributes();
    $date = strval($attrs['End']);
    return str_replace('-', '', $date);
  }

  public function getDestinationCode() {
    // TODO
    return '';
  }

  public function getDestinationType() {
    return '';
  }

  public function getMainImage() {
    return '';
  }

  public function getDescription() {
    return '';
  }

  public function getLatitude() {
    return '';
  }

  public function getLongitude() {
    return '';
  }

  public function getZone() {
    return '';
  }

  public function getLocation() {
    return '';
  }

  public function getCategory() {
    return '';
  }

  public function getCategoryType() {
    return '';
  }

  public function getCategoryCode() {
    // TODO
    return '';
  }

  public function getRoomsAvail() {
    return $this->rooms;
  }

  public function getGroupedRooms() {
    return $this->rooms;
  }

  public function getCurrency() {
    return 'USD';
  }

  public function getContractName() {
    return '';
  }

  public function getContractIncomingOfficeCode() {
    return '';
  }

  public function getContractComment() {
    try {
      if (isset($this->element->RoomStays->RoomStay->BasicPropertyInfo->VendorMessages->VendorMessage)) {
        return strval($this->element->RoomStays->RoomStay->BasicPropertyInfo->VendorMessages->VendorMessage->SubSection->Paragraph->Text);
      }
    }
    catch (Exception $e) { }
    return '';
  }

  public function getClientComment() {
    return '';
  }

  public function getSupplier() {
    if (isset($this->element->TPA_Extensions->Payable)) {
      $payable = strval($this->element->TPA_Extensions->Payable);
      return str_replace('<br/>', '. ', $payable);
    }
    return 'Methabook';
  }

  public function getSupplierVatNumber() {
    return '';
  }

  public function getDiscountList() {
    return array();
  }

  public function getAllPaxRequired() {
    return true;
  }

  public function getXtraDataRequired() {
    return true;
  }
}

?>
